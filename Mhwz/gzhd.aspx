﻿<%@ Page Language="C#" MasterPageFile="~/GzhdMasterPage.master" AutoEventWireup="true" CodeFile="gzhd.aspx.cs" Inherits="gzhd" Title="南京市国土资源局六合分局" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div style="width: 100%; height: 31px; text-align: left; background: url(images/index_15.jpg)">
        <div style="text-align: left; float: left;">
            <img src="images/index_14.jpg" width="31" height="31" />
        </div>
        <div style="text-align: left; float: left; width: 641px; margin-top: 5px;" class="font3">
              
        </div>
        <div style="width: 64px; float: left; text-align: right"><a href="gzhd_hyb.aspx?jg=1" class="blue">更多&gt;&gt;</a></div>
        <div style="width: 5px; float: left; text-align: right">
            <img src="images/index_17.jpg" width="5" height="31" />
        </div>
    </div>
    <asp:DataList ID="hyb" runat="server" Width="100%">
        <HeaderTemplate>
            <table width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#d9d9d9">
                <tr>
                    <td width="32%" height="22" align="center" valign="middle" bgcolor="#def2fb"><span class="STYLE1">标题</span></td>
                    <td width="30%" height="22" align="center" valign="middle" bgcolor="#def2fb" class="STYLE1">受理单位</td>
                    <td width="13%" height="22" align="center" valign="middle" bgcolor="#def2fb" class="STYLE1">状态</td>
                    <td width="25%" height="22" align="center" valign="middle" bgcolor="#def2fb" class="STYLE1">日期</td>
                </tr>
            </table>
        </HeaderTemplate>
        <ItemTemplate>
            <table width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#d9d9d9">
                <tr>
                    <td width="32%" height="22" align="center" valign="middle" bgcolor="#FFFFFF"><a href="gzhd_hyb.aspx?jg=1"><%#CommonOperation.Left(Eval("title").ToString(),10) %></a></td>
                    <td width="30%" height="22" align="center" valign="middle" bgcolor="#FFFFFF"><%#Eval("r_name") %></td>
                    <td width="13%" height="22" align="center" valign="middle" bgcolor="#FFFFFF">已处理</td>
                    <td width="25%" height="22" align="center" valign="middle" bgcolor="#FFFFFF"><%#Convert.ToDateTime(Eval("tjrq")).GetDateTimeFormats('D')[1].ToString() %></td>
                </tr>
            </table>
        </ItemTemplate>
    </asp:DataList>



</asp:Content>

