﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="xxgkMore.aspx.cs" Inherits="xxgkMore" %>

<%@ Register Src="~/UserControl/head.ascx" TagPrefix="uc1" TagName="head" %>
<%@ Register Src="~/UserControl/foot.ascx" TagPrefix="uc1" TagName="foot" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="css/main.css" rel="stylesheet" />
    <link href="css/top_css.css" rel="stylesheet" />
    <script src="js/jquery.js"></script>
    <script src="js/Jquery.Global.js"></script>
    <title></title>
    <script type="text/javascript">
       function hideDiv() {
            if (request.QueryString("art_id") != null) {
                $("#iframeContent").empty();
                $("#iframeContent").hide();
            }
            else {
                $("#artContent").empty();
                $("#artContent").hide();
            }
        }

       function AddxxgkList(obj) {
           $("#artContent").empty();
           $("#artContent").hide();
           $("#xxgkmore_content").attr("src", obj);
         
       }

        //用于从首页业务公告下内容区链接过来，并且页面还有生成iframe，所以特添加固定隐藏iframe
       function AddxxgkList2(obj) {
           $("#artContent").empty();
           $("#artContent").hide();
           $("#iframeContent").empty();
           $("#iframeContent").hide();
           $("#Content").show();
           $("#iframe_content").attr("src", obj);
       }


        function iFrameHeight3() {
            var ifm = document.getElementById("xxgkmore_content");
            var subWeb = document.frames ? document.frames["xxgkmore_content"].document :
    ifm.contentDocument;
            if (ifm != null && subWeb != null) {
                ifm.height = subWeb.body.scrollHeight;
            }
        }
        function iFrameHeight4() {
            var ifm = document.getElementById("iframe_content");
            var subWeb = document.frames ? document.frames["iframe_content"].document :
    ifm.contentDocument;
            if (ifm != null && subWeb != null) {
                ifm.height = subWeb.body.scrollHeight;
            }
        }
    </script>
</head>
<body onload="hideDiv()">
    <form id="form1" runat="server">
        <div style="text-align: center; width: 993px; margin: 0px auto;">
            <div id="tdTop">
                <uc1:head runat="server" ID="head" />
            </div>
            <div style="width: 100%; margin: 0px auto;">
                <div style="width: 100%; background-color: #FFFFFF; margin-top: 5px;">

                    <div style="width: 232px; float: left;">
                        <%=LeftMenu %>
                    </div>
                    <div style="float: left; margin-left: 10px; width: 749px;" id="iframeContent">
                        <%=IframeContent %>
                    </div>

                    <div style="float: left; margin-left: 10px; width: 749px; overflow:auto; margin-bottom:5px;" id="artContent" class="table1Copy" >
                         <div style="width: 750px; background: url(images/index_15.jpg); overflow:hidden">

                            <div style="text-align: left; width: 42px; height: 31px; float: left;">
                                <img src="images/index_14.jpg" width="31" height="31" />
                            </div>
                            <div style="text-align: left; padding-top:5px; width: 353px; float: left; vertical-align: middle; background: url(images/index_15.jpg);" class="font3">
                                <asp:Label ID="strclassname" runat="server"></asp:Label>
                            </div>
                            <div style="text-align: right;font-size:12px;padding-top:5px ;width: 337px; float: left; vertical-align: middle;">
                                当前位置：首页 &gt;&nbsp;<asp:Label ID="strclass" runat="server"></asp:Label>
                                &gt;
                            <asp:Label ID="strsclass" runat="server"></asp:Label>
                            </div>
                            <div style="text-align: right; width: 10px; float: left;">
                                <img src="images/index_17.jpg" width="5" height="31" />
                            </div>

                        </div>


                        <div style="height: 60px; text-align: center;" class="news_title">
                            <asp:Label ID="lbtitle" runat="server"></asp:Label>
                        </div>

                        <div style="height: 30px; text-align: center; background-color: #EDEDEB">
                            来源:<asp:Label ID="lbfrom" runat="server"></asp:Label>
                             日期:(<asp:Label ID="lbdate" runat="server"></asp:Label>)　【<span onclick="window.print();" style="text-decoration: underline; cursor: pointer">打印</span>】【<a href="" onclick="window.close();">关闭窗口</a>】
                        </div>

                        <div style="text-align: left; width: 100%;" class="news_read">
                            <%=strContent %>
                        </div>
                    </div>
                    <div style="float: left;display:none; margin-left: 10px; width: 749px; overflow:auto; margin-bottom:5px;" id="Content"    >
                    <iframe id='iframe_content' src='#' name='iframe_content' marginheight='0' marginwidth='0' frameborder='0' scrolling='no' height='100%' width='100%' onload='iFrameHeight4()' ></iframe>
                        </div>
                    <div id="divBottom" style="margin-top: 5px; width: 100%">
                        <uc1:foot runat="server" ID="foot" />
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
