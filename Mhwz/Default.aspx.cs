﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Default: System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //更新访问量
            BusinessOperation.UpdateFwl();
        }
    }

    /// <summary>
    /// 更新访问量
    /// </summary>
    protected string GetFwl()
    {
        return BusinessOperation.SelectFwl();
    }
   
}