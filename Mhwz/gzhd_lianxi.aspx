﻿<%@ Page Language="C#" MasterPageFile="~/GzhdMasterPage.master" AutoEventWireup="true" CodeFile="gzhd_lianxi.aspx.cs" Inherits="gzhd_lianxi" Title="南京市国土资源局六合分局－联系我们" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table width="80%" height="108" border="0" cellpadding="0" cellspacing="1">
        <tr>
            <td align="center">单位名称:</td>
            <td>南京市国土资源局六合分局</td>
        </tr>
        <tr>
            <td align="center">热线电话:</td>
            <td>0716-3267474</td>
        </tr>
        <tr>
            <td align="center">电子邮箱:</td>
            <td>www.jlgtj.gov.cn</td>
        </tr>
        <tr>
            <td align="center">传 真:</td>
            <td>0716-3262949</td>
        </tr>
        <tr>
            <td align="center">地 址:</td>
            <td>监利县容城镇天府大道123号</td>
        </tr>
        <tr>
            <td align="center">举报电话:</td>
            <td>12336</td>
        </tr>
    </table>

</asp:Content>

