﻿<%@ WebHandler Language="C#" Class="ZtAndTcHandle" %>

using System;
using System.Web;

public class ZtAndTcHandle : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/plain";
        context.Response.Write(GetJson(context));
    }

    public string GetJson(HttpContext context)
    {
        string strContent = string.Empty;
        string sql = "select * from TB_ZTFL t where pid=-1";
        if (!string.IsNullOrEmpty(context.Request["type"]))
        {
            sql += string.Format(" and type='{0}'", context.Request["type"]);
        }
        System.Data.DataTable dtParent = DbOperation.QueryBySql(sql);

        if (dtParent.Rows.Count > 0)
        {
            strContent = "[";
            for (int i = 0; i < dtParent.Rows.Count; i++)
            {
                System.Data.DataRow drParent = dtParent.Rows[i];
                strContent += "{";
                string strId = drParent["id"].ToString();

                strContent += string.Format("\"id\":{0},", strId);
                strContent += string.Format("\"text\":\"{0}\",", drParent["name"]);

                sql = string.Format("select * from TB_ZTFL t where pid='{0}'", strId);
                if (!string.IsNullOrEmpty(context.Request["type"]))
                {
                    sql += string.Format(" and type='{0}'", context.Request["type"]);
                }
                System.Data.DataTable dtChild = DbOperation.QueryBySql(sql);

                if (dtChild.Rows.Count > 0)
                {
                    //strContent += "\"state\":\"closed\",";
                    strContent += "\"children\":[";
                    foreach (System.Data.DataRow drChild in dtChild.Rows)
                    {
                        strContent += "{";

                        strContent += string.Format("\"id\":{0},", drChild["id"]);
                        strContent += string.Format("\"text\":\"{0}\"", drChild["name"]);
                        strContent += "},";
                    }
                    strContent = strContent.Substring(0, strContent.Length - 1);
                    strContent += "]";
                }
                else//没有子集则要把text或state后面的逗号去掉
                {
                    strContent = strContent.Substring(0, strContent.Length - 1);
                }
                strContent += "},";
            }
            strContent = strContent.Substring(0, strContent.Length - 1);
            strContent += "]";
        }
        return strContent;
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}