﻿<%@ WebHandler Language="C#" Class="ZpTreeHandle" %>

using System;
using System.Web;

public class ZpTreeHandle: IHttpHandler
{
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/plain";
        context.Response.Write(GetJson());
    }
    
    public string GetJson()
    {
        string strContent = string.Empty;
        string sql = "select * from TB_CLASS t";
        System.Data.DataTable dtParent = DbOperation.QueryBySql(sql);

        if (dtParent.Rows.Count > 0)
        {
            strContent = "[";
            for (int i = 0; i < dtParent.Rows.Count; i++)
            {
                System.Data.DataRow drParent = dtParent.Rows[i];
                strContent += "{";
                string strId = drParent["classid"].ToString();

                strContent += string.Format("\"id\":{0},", strId);
                strContent += string.Format("\"text\":\"{0}\",", drParent["c_name"]);

                sql = string.Format("select * from TB_SCLASS t where classid='{0}'", strId);
                System.Data.DataTable dtChild = DbOperation.QueryBySql(sql);

                if (dtChild.Rows.Count > 0)
                {
                    strContent += "\"state\":\"closed\",";
                    strContent += "\"children\":[";
                    foreach (System.Data.DataRow drChild in dtChild.Rows)
                    {
                        strContent += "{";

                        strContent += string.Format("\"id\":{0},", drChild["s_id"]);
                        strContent += string.Format("\"text\":\"{0}\"", drChild["s_name"]);
                        strContent += "},";
                    }
                    strContent = strContent.Substring(0, strContent.Length - 1);
                    strContent += "]";
                }
                else//没有子集则要把text或state后面的逗号去掉
                {
                    strContent = strContent.Substring(0, strContent.Length - 1);
                }

                strContent += "},";
            }
            strContent = strContent.Substring(0, strContent.Length - 1);
            strContent += "]";
        }
        return strContent;
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}