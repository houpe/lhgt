﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class bsdzContent : System.Web.UI.Page
{
    public string strContent = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            AddClickCount();//浏览次数
            strread();//内容
        }
    }

  
    private void strread()  //读取内容
    {
        string sql = string.Format("select art_id,title,author,WZNR,wfrom,hits,to_char(tjrq,'yyyy-mm-dd') as tjrq from tb_article where art_id='{0}'", Request["art_id"]);
        DataTable dtReturn = DbOperation.QueryBySql(sql);

        if (dtReturn.Rows.Count > 0)
        {
            lbtitle.Text = dtReturn.Rows[0]["title"].ToString();
            //lbauthor.Text = dtReturn.Rows[0]["author"].ToString();
            strContent = dtReturn.Rows[0]["WZNR"].ToString();
            lbfrom.Text = dtReturn.Rows[0]["wfrom"].ToString();
            //lbhit.Text = dtReturn.Rows[0]["hits"].ToString();
            lbdate.Text = dtReturn.Rows[0]["tjrq"].ToString();

        }
        else
        {
           // strclass.Text = "暂无栏目";
        }

    }

    private void AddClickCount()  //浏览次数
    {
        string strUserHostIp = Session["userhostip"] == null ? "" : Session["userhostip"].ToString();

        string strArtId = Request["art_id"];
        if (strUserHostIp != Request.UserHostAddress || Session["RequestId"] != strArtId)
        {
            string sql = string.Format("update tb_article set hits=hits+1 where art_id='{0}'", strArtId);
            DbOperation.ExecuteSql(sql);
            Session["userhostip"] = Request.UserHostAddress;
            Session["RequestId"] = strArtId;
        }
    }
}