﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;

public partial class manage_firmpwd : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Button1_Click(object sender, EventArgs e)
    {

        string sql = string.Format("select id,pwd from tb_admin where pwd='{0}' and id='{1}'", jiami.Encrypting(tbpwd.Text), Request["id"]);
        DataTable dtReturn = DbOperation.QueryBySql(sql);
        if (dtReturn.Rows.Count != 0)
        {
            Panel1.Visible = true;
            Panel2.Visible = false;

        }
        else
        {
            Panel1.Visible = false;
            Response.Write("<script>alert('密码错误');location='javascript:history.go(-1)';</script>");
        }
    }

    protected void Button2_Click(object sender, EventArgs e)
    {

        string sql = string.Format("update tb_admin set name='{0}',pwd='{1}' where id='{2}'", tb_name.Text, jiami.Encrypting(tb_newpwd.Text), Request["id"]);
        DbOperation.ExecuteSql(sql);
        Response.Write("<script>alert('修改成功');location='user.aspx';</script>");
    }
}
