﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="submitEditMenu.aspx.cs" Inherits="manage_submitEditMenu" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>修改页面</title>
    <script src="../js/jquery.js" type="text/javascript"></script>
    <script src="../js/Jquery.Global.js" type="text/javascript"></script>
    <script src="../js/Jquery.Common.js"></script>

    <script type="text/javascript">
        var serviceUrl = Common.GetLocalPath() + "<%=ConfigurationManager.AppSettings["moaServiceUrl"]%>";
        var strServicePath = serviceUrl+"/ExSql";
        String.prototype.getQuery = function (name) {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
            var r = this.substr(this.indexOf("\?") + 1).match(reg);
            if (r != null) return unescape(r[2]); return null;
        }

        function LoadData() {
            var url = window.location.href;
            var Type = url.getQuery("type");
            var level = url.getQuery("level");
            try {
                if (Type == "0" & level == "0") {
                    $("#subEdit_Content").append("<tr style='width:100%; text-align:center;background-color:#0C90FF'><td>添加一级目录</td></tr><tr></tr><tr><tr/><tr><td>一级目录名称:<input type='text' id='mulu'/></td></tr><tr></tr><tr><tr/><tr style='text-align:center'><td><input type='button' value='添加' onclick='Submit()'> <input type='button' value='取消'></td></tr>");
                }
                else if (Type == "0" & level == "1") {
                    $("#subEdit_Content").append("<div>添加二级目录</div><tr><td><input type='text' id='mulu'/><td/> <tr/> <tr><td><input type='button' value='添加' onclick='Submit2()'> <input type='button' value='取消'>  </td></tr>");
                }
                else if (Type == "1" && level == "1") {
                    var id = url.getQuery("editId");
                    WebService(strServicePath,BackEditName,'{"type":"3","level":"1","par":"' + id + '","oldpar":"","newpar":""}');
                }
                else if (Type == "1" && level == "0") {
                    var nameId = url.getQuery("editId");
                    WebService(strServicePath, BackEditName2, '{"type":"3","level":"0","par":"' + nameId + '","oldpar":"","newpar":""}');
                }
            }
            catch (ex)
            {
                alert(ex.message);
            }
        }

        function Submit() {
            var mulu = $("#mulu").val();
            if (mulu != "" && mulu != null) {
                WebService(strServicePath, Backline, '{"type":"0","level":"0","par":"' + mulu + '","oldpar":"","newpar":""}');
            }
        }

        function Submit2() {
            var url = window.location.href;
          
            var nameId = url.getQuery("editId");
            var mulu = $("#mulu").val();
            if (mulu != "" && mulu != null) {
                WebService(strServicePath, Backline, '{"type":"0","level":"1","par":"' + mulu + '","oldpar":"","newpar":"' + nameId + '"}');
            }
        }

        function SubmitEdit(obj) {
            var url = window.location.href;
            var oldpar = url.getQuery("editId");
            var newpar = $("#mulu").val();
            if (newpar != "" && newpar != null) {
                WebService(strServicePath, Backline, '{"type":"1","level":"1","par":"","oldpar":"' + oldpar + '","newpar":"' + newpar + '"}');
            }
        }
        function SubmitEdit2() {
            var url = window.location.href;
            var oldpar = url.getQuery("editId");
            var newpar = $("#mulu").val();
            if (newpar != "" && newpar != null) {
                WebService(strServicePath, Backline, '{"type":"1","level":"0","par":"","oldpar":"' + oldpar + '","newpar":"' + newpar + '"}');
            }

        }


        function Backline(result) {
            if (result != "" && result != null)
            {
                window.opener.location.href = window.opener.location.href;
                alert("添加成功");
            }
        }

        function BackEditName(result) {
            var name = $.parseJSON(result);
            $(name).each(function (index,obj) {
               // $("#subEdit_Content").append(" <span style='width:100%; text-align:center'</span><span style='width:100%; text-align:center'> </span>");
                $("#subEdit_Content").append("<div>上级目录:" + obj.c_name + "<div/><div>编辑二级目录</div><tr><td>目录名称:<input type='text' id='mulu' /><td/> <tr/> <tr><td><input type='button' name='" + obj.s_id + "' value='确定' onclick='SubmitEdit(this)'> <input type='button' value='取消'>  </td></tr>");
                $("#mulu").val(obj.s_name);
            })
            
        }

        //返回第一级目录依据ID返回的CNAME并显示
        function BackEditName2(result) {
            var strName = $.parseJSON(result);
            $(strName).each(function (index,obj) {
                //$("#subEdit_Content").append("<span> </span>");
                $("#subEdit_Content").append("<div>编辑一级目录</div><tr><td><input type='text' id='mulu' /><td/> <tr/> <tr><td><input type='button' value='确定' onclick='SubmitEdit2()'> <input type='button' value='取消'>  </td></tr>"); 
                $("#mulu").val(obj.c_name);
            })
         
        }
    </script>
</head>
<body onload="LoadData()">
    <form id="form1" runat="server">
        <div id="subEdit_Content" style="width:80%">
       
        </div>
    </form>
</body>
</html>
