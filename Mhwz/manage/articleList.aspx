﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="articleList.aspx.cs" Inherits="manage_articleList" %>

<%@ Register Src="~/UserControl/Calendar.ascx" TagPrefix="uc1" TagName="Calendar" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>编辑与删除</title>
    <style type="text/css">
        td {
            text-align: center;
            font-size: 13px;
            background-color: #FFFFFF;
        }

        table {
            background-color: #87CEFA;
        }

        .auto-style1 {
            height: 20px;
        }
    </style>
    <script type="text/javascript">
        function CheckAll(Obj) {
            var AllObj = document.all;
            if (Obj.checked) {
                for (var i = 0; i < AllObj.length; i++) {
                    if (AllObj[i].type == "checkbox") {
                        AllObj[i].checked = true;
                    }
                }
            }
            else {
                for (var i = 0; i < AllObj.length; i++) {
                    if (AllObj[i].type == "checkbox") {
                        AllObj[i].checked = false;
                    }
                }
            }
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div style="margin: 0px auto; text-align: center; width: 970px;">
            <table border="0" cellpadding="0" cellspacing="1" style="width: 100%">
                <tr>
                    <td class="auto-style1">文章搜索:</td>
                    <td class="auto-style1">
                        <asp:DropDownList ID="select1" runat="server">
                            <asp:ListItem Value="strtitle">标题</asp:ListItem>
                            <asp:ListItem Value="strcontent">内容</asp:ListItem>
                        </asp:DropDownList>

                    <td class="auto-style1" colspan="2">
                        <asp:TextBox ID="tb_search" runat="server" Width="186px" Text=""></asp:TextBox></td>
                </tr>
                <tr>
                    <td>发布时间：</td>
                    <td>
                       从<uc1:calendar runat="server" id="cdStart" width="153" />
                    </td>
                    <td>至</td>
                    <td>
                        <uc1:calendar runat="server" id="cdEnd" width="153"/>
                    </td>
                </tr>
                <tr>
                    <td>组配分类:</td>
                    <td>
                        <asp:DropDownList ID="dlclass" runat="server" DataTextField="c_name" DataValueField="classid" AutoPostBack="true" OnSelectedIndexChanged="dlclass_SelectedIndexChanged" Width="100px">
                        </asp:DropDownList>

                    </td>
                    <td>
                        <asp:DropDownList ID="dlsclass" runat="server" DataTextField="s_name" DataValueField="s_id" Width="186px">
                        </asp:DropDownList>
                    <td>
                        <asp:Button ID="Button1" runat="server" Text="搜索" OnClick="bt_search_Click" Width="46px" /></td>
                </tr>

            </table>
            <asp:DataList ID="dlart" runat="server" Width="100%" DataKeyField="art_id" OnItemCommand="dlart_ItemCommand" OnItemDataBound="dlart_ItemDataBound">
                <HeaderTemplate>
                    <table border="0" cellpadding="0" cellspacing="1" style="width: 100%">
                        <tr>
                            <td width="73">
                                <asp:Button ID="btnPLDelete" runat="server" Text="批量删除" CommandName="pldelete" Width="68px" /></td>
                            <td width="100">ID编号</td>
                            <td>文章标题</td>
                            <td width="100">栏目类别</td>
                            <td width="123">子类别</td>
                            <td width="73">发布时间</td>
                            <td width="180">操作</td>
                        </tr>
                    </table>
                </HeaderTemplate>
                <ItemTemplate>
                    <table border="0" cellpadding="0" cellspacing="1" style="width: 100%">
                        <tr>
                            <td width="73">
                                <asp:CheckBox ID="cbox" runat="server" /></td>
                            <td width="100"><%#Eval("art_id") %></td>
                            <td><%#Eval("title") %></td>
                            <td width="100"><%#Eval("c_name") %></td>
                            <td width="123"><%#Eval("s_name") %></td>
                            <td width="73"><%#Eval("tjrq") %></td>
                            <td width="180"><a href="javascript:window.location.href = 'articleList.aspx?type=1&art_id=<%#Eval("art_id") %>'">取消发布</a>&nbsp;&nbsp;<a href='articleEdit.aspx?art_id=<%#Eval("art_id") %>' target="_blank">编辑</a>&nbsp;&nbsp;<asp:LinkButton ID="dldelete" runat="server" CommandName="delete">删除</asp:LinkButton>
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>

                <FooterTemplate>
                    <table border="0" cellpadding="0" cellspacing="1" style="width: 100%">
                        <tr>
                            <td width="73px">
                                <input id="Checkbox1" name="全选" onclick="return CheckAll(this);" title="全选" type="checkbox" value="全选" />全选
                            </td>
                            <td align="center">
                                <asp:Label ID="labCurrentPage" runat="server"></asp:Label>/
                <asp:Label ID="labPageCount" runat="server"></asp:Label>
                                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="first" Font-Underline="False" ForeColor="Black">首页</asp:LinkButton>
                                <asp:LinkButton ID="LinkButton2" runat="server" CommandName="pre" Font-Underline="False" ForeColor="Black">上一页</asp:LinkButton>
                                <asp:LinkButton ID="LinkButton3" runat="server" CommandName="next" Font-Underline="False" ForeColor="Black">下一页</asp:LinkButton>
                                <asp:LinkButton ID="LinkButton4" runat="server" CommandName="last" Font-Underline="False" ForeColor="Black">尾页</asp:LinkButton>
                            </td>
                        </tr>
                    </table>
                </FooterTemplate>
                <SelectedItemStyle ForeColor="GhostWhite" />
                <HeaderStyle Font-Bold="True" />
            </asp:DataList>
        </div>
    </form>
</body>
</html>
