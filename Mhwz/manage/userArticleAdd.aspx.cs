﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using System.Data.OracleClient;
using WF_Business;

public partial class manage_articleAdd : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["AdminName"] != null)
        { 
            if (!IsPostBack)
            {
                //Calendar.Value = DateTime.Now.ToString("d");
                Calendar.Value = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                 string strTemp = "select QX from tb_admin where name='" + Session["AdminName"] + "'";
                  DataTable dtRole = DbOperation.QueryBySql(strTemp);
                  DataTable resultTable = new DataTable();
                  resultTable.Columns.Add("s_id", typeof(string));
                  resultTable.Columns.Add("s_name", typeof(string));
                if (dtRole.Rows.Count > 0)
                {
                    if (dtRole.Rows[0]["QX"].ToString() == "1")
                    {
                        
                        string strdl1 = "select classid,c_name from tb_class order by classid asc ";
                        DataTable dtReturn = DbOperation.QueryBySql(strdl1);
                        if (dtReturn.Rows.Count > 0)
                        {
                            dlclass.DataSource = dtReturn;
                            dlclass.DataBind();
                        }
                      
                    }
                    else
                    {
                        
                        string strSql = "select 组配权限 from TB_POWERMANAGE where 用户 like '%" + Session["AdminName"] + "%'";
                      //  DataRow dr;
                        DataTable dtReturn = DbOperation.QueryBySql(strSql);
                        if (dtReturn.Rows.Count > 0)
                        {
                            string[] power = (dtReturn.Rows[0]["组配权限"].ToString()).Split('|');
                            for (int i = 0; i < power.Length; i++)
                            {
                                dlclass.Items.Add(new ListItem(power[i], power[i]));

                            }
  
                        }
                    
                    }
                }                 
                ListItem item = new ListItem("==文章类型==", "==文章类型==");
                dlclass.Items.Insert(0, item);
                ListItem item2 = new ListItem("==选择分类==", "==选择分类==");
                dlsclass.Items.Insert(0, item2);

               
                ListItem item4 = new ListItem("==办事名称==", "==办事名称==");
                BindDropdownData("指南分类", dlbsznlist);
                dlbsznlist.Items.Insert(0, item4);
                //string strdl2 = "select name from tb_ztfl";
                //DataTable dtReturn2 = DbOperation.QueryBySql(strdl2);
                //foreach (DataRow drTemp in dtReturn2.Rows)
                //{
                //    dlbsznlist.Items.Add(drTemp["name"].ToString());
                //}
                BindDropdownData("体裁分类", ddlTcfl);
                ddlTcfl.Items.FindByValue("其他").Selected = true;
                BindDropdownData("主题分类", ddlZtfl);
            }
        }
        else
        {

            Response.Write("<script language='javascript'>window.open('login-admin.aspx','_top');</script>");

        }
    }

    public void BindDropdownData(string strType, DropDownList ddlTemp)
    {

        string sql = string.Format("select * from TB_ZTFL t where pid=-1 and type='{0}'",strType);
        DataTable dtParent = DbOperation.QueryBySql(sql);
        ddlTemp.Items.Clear();
        foreach (DataRow drTemp in dtParent.Rows)
        {
            ddlTemp.Items.Add(drTemp["name"].ToString());
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        if (dlclass.SelectedItem.ToString() == "==文章类型==" ||dlsclass.SelectedValue == "")
        {
            Response.Write("<script>alert('请检查文章类型和文章子类型是否选择!');</script>");
        }
        else if (string.IsNullOrEmpty(Calendar.Value) ||string.IsNullOrEmpty(txtContent.Value))
        {
            Response.Write("<script>alert('请检查日期和文章内容是否输入!');</script>");
        }
        else if (CheckBox1.Checked == true && string.IsNullOrEmpty(tboxtu.Text))
        {
            Response.Write("<script>alert('请选择上传附件!');</script>");
        }
        else
        {
            RunSQL();
        }

   
    }

    public void RunSQL()
    {
        string cid = string.Empty;
        string sqlClass = "select classid from tb_class where c_name='" + dlclass.SelectedItem.ToString() + "'";
        DataTable dtclass = DbOperation.QueryBySql(sqlClass);
        if (dtclass.Rows.Count > 0)
        {
            cid = dtclass.Rows[0]["classid"].ToString();
        }
        string sid = dlsclass.SelectedValue;
        //  string strtxt = txtContent.Text.Trim(); 
        string strtxt = txtContent.Value.Trim();
        string sql = string.Empty;
        if (CheckBox1.Checked == false)
        {
            if (dlbsznlist.SelectedItem.ToString() != "==办事名称==")
            {
                sql = string.Format("insert into TB_ARTICLESUBMIT (classid,s_id,title,wznr,author,wfrom,tcfl,ztfl,bszn_fl,tjrq,tupian,tu) values ('{0}','{1}','{2}',:txtWznr,'{3}','{4}','{5}','{6}','{7}',To_date('{8}','yyyy-mm-dd hh24-mi-ss'),'{9}','0')", cid, sid, strtitle.Text.Trim().Replace("'", "''"), strauthor.Text.Trim().Replace("'", "''"), strfrom.Text.Trim().Replace("'", "''"), ddlTcfl.Text, ddlZtfl.Text, dlbsznlist.Text, Calendar.Value, tboxtu.Text);
            }
            else
            {
                sql = string.Format("insert into TB_ARTICLESUBMIT (classid,s_id,title,wznr,author,wfrom,tcfl,ztfl,tjrq,tupian,tu) values ('{0}','{1}','{2}',:txtWznr,'{3}','{4}','{5}','{6}',To_date('{7}','yyyy-mm-dd hh24-mi-ss'),'{8}','0')", cid, sid, strtitle.Text.Trim().Replace("'", "''"), strauthor.Text.Trim().Replace("'", "''"), strfrom.Text.Trim().Replace("'", "''"), ddlTcfl.Text, ddlZtfl.Text, Calendar.Value, tboxtu.Text);
            }
        }
        else if (CheckBox1.Checked != false && tboxtu.Text != null && tboxtu.Text != "")
        {
            if (dlbsznlist.SelectedItem.ToString() != "==办事名称==")
            {
                sql = string.Format("insert into TB_ARTICLESUBMIT (classid,s_id,title,wznr,author,wfrom,tupian,tu,tcfl,ztfl,bszn_fl,tjrq) values ('{0}','{1}','{2}',:txtWznr,'{3}','{4}','{5}','1','{6}','{7}','{8}',To_date('{9}','yyyy-mm-dd hh24-mi-ss'))", cid, sid, strtitle.Text.Trim().Replace("'", "''"), strauthor.Text.Trim().Replace("'", "''"), strfrom.Text.Trim().Replace("'", "''"), tboxtu.Text.Trim().Replace("'", "''"), ddlTcfl.Text, ddlZtfl.Text, dlbsznlist.Text, Calendar.Value);
            }
            else
            {
                sql = string.Format("insert into TB_ARTICLESUBMIT (classid,s_id,title,wznr,author,wfrom,tupian,tu,tcfl,ztfl,tjrq) values ('{0}','{1}','{2}',:txtWznr,'{3}','{4}','{5}','1','{6}','{7}',To_date('{8}','yyyy-mm-dd hh24-mi-ss'))", cid, sid, strtitle.Text.Trim().Replace("'", "''"), strauthor.Text.Trim().Replace("'", "''"), strfrom.Text.Trim().Replace("'", "''"), tboxtu.Text.Trim().Replace("'", "''"), ddlTcfl.Text, ddlZtfl.Text, Calendar.Value);
            }
        }

        IDataParameter[] idp = new OracleParameter[1];
        idp[0] = new OracleParameter(":txtWznr", OracleType.Clob);
        idp[0].Value = txtContent.Value.Trim();
        SysParams.OAConnection().RunSql(sql, ref idp);
        //  DbOperation.ExecuteSql(sql);
        //Response.Write("<script>alert('文章已添加成功，但需向办公室提供保密审查表方能正式发布!');location=location;</script>");
        Response.Write("<script>alert('文章已添加成功，但需向办公室提供保密审查表方能正式发布!')</script>");
        //strtitle.Text = "";
        //strauthor.Text = "";
        //strfrom.Text = "";
        txtContent.Value = "";
    
    
    }
    protected void dlclass_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (dlclass.SelectedItem.ToString() != "==文章类型==")
        {
            string strdl2 = string.Format("select s_id,s_name,classid from tb_sclass where classid=(select classid from tb_class where c_name='{0}')", dlclass.SelectedItem.ToString());
            DataTable dtReturn = DbOperation.QueryBySql(strdl2);
            dlsclass.DataSource = dtReturn;
            dlsclass.DataBind();
        }

    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        strtitle.Text = "";
        strauthor.Text = "";
        strfrom.Text = "";
        txtContent.Value = "";
    }
    protected void strupload_Click(object sender, EventArgs e)
    {

        if (f_pic.HasFile)
        {
            if (f_pic.PostedFile.ContentLength < 10485760)
            {
                try
                {
                    Boolean fileOk = false;
                    string fileExt, path;
                    fileExt = f_pic.FileName.ToLower();//文件名
                    //int i = fileExt.LastIndexOf(".");
                   // fileExt = fileExt.Substring(i, fileExt.Length - i);
                    String fileExtension = System.IO.Path.GetExtension(f_pic.FileName).ToLower();

                    String[] allowedExtensions = { ".gif", ".png", ".jpeg", ".jpg", ".bmp" };
                    for (int p = 0; p < allowedExtensions.Length; p++)
                    {
                        if (fileExtension == allowedExtensions[p])
                        {
                            fileOk = true;
                        }
                    }
                    if (fileOk)
                    {
                        try
                        {//文件另存在服务器的指定目录下
                            string strFileName = Guid.NewGuid().ToString();
                          //  path = "~/pic/upload/" + strFileName + fileExt;
                            path = "~/pic/upload/" + fileExt;
                            this.f_pic.PostedFile.SaveAs(HttpContext.Current.Server.MapPath(path));
                            Response.Write("<script>alert('上传成功！');</script>");
                            tboxtu.Text = "pic/upload/" + strFileName + fileExtension;
                        }
                        catch
                        {
                            Response.Write("<script>alert('文件上传失败！');</script>");
                        }
                    }
                    else
                    {
                        Response.Write("<script>alert('只能上传gif,png,jpeg,jpg,bmp图象文件！');</script>");
                    }
                

                   
                }
                catch
                {
                    Response.Write("<script>alert('上传失败！');</script>");

                }
            }
            else
            {
                Response.Write("<script>alert('上传文件不能大于10MB！');</script>");

            }
        }
        else
        {
            Response.Write("<script>alert('请选择文件!');</script>");

        }
    }
    //protected void DDL_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    string sql = "select name from TB_ZTFL where type=" + DDL.SelectedValue;
    //    DataTable dtParent = DbOperation.QueryBySql(sql);
    //    dlbsznlist.Items.Clear();
    //    foreach (DataRow drTemp in dtParent.Rows)
    //    {
    //        dlbsznlist.Items.Add(drTemp["name"].ToString());
    //    }
    //}
    //protected void DDL_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    string sql = "select name from TB_ZTFL where type=" + DDL.SelectedValue;
    //    DataTable dtParent = DbOperation.QueryBySql(sql);
    //    dlbsznlist.Items.Clear();
    //    foreach (DataRow drTemp in dtParent.Rows)
    //    {
    //        dlbsznlist.Items.Add(drTemp["name"].ToString());
    //    }

    //}
}
