﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class manage_articleSubmit : System.Web.UI.Page
{
    private  PagedDataSource pd = new PagedDataSource();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["AdminName"] != null)
        {
            if (!IsPostBack)
            {
                string strcmd = "select classid,c_name from  tb_class";
                DataTable dtReturn = DbOperation.QueryBySql(strcmd);
                if (dtReturn.Rows.Count > 0)
                {
                    dlclass.DataSource = dtReturn;
                    dlclass.DataBind();
                }
                ListItem item = new ListItem("==文章类型==", "==文章类型==");
                dlclass.Items.Insert(0, item);
                ListItem item2 = new ListItem("==选择分类==", "==选择分类==");
                dlsclass.Items.Insert(0, item2);

                getData(1);
            }

        }
        else
        {
            Response.Write("<script language='javascript'>window.open('login-admin.aspx','_top');</script>");
        }
    }

    static int i;  
    private DataTable getData_artitlesub()
    {
        string strSql = "select art_id,title,author, to_char(tjrq,'yyyy-mm-dd') as tjrq ,tjrq as tt from TB_ARTICLESUBMIT order by tt desc";
        DataTable resultDt=new DataTable();
        DataTable dtSubART = DbOperation.QueryBySql(strSql);
        if (dtSubART.Rows.Count > 0)
        {
           resultDt= dtSubART;
        }
        return resultDt;
    }
    private void getData(int pageIndex)
    {
        
        pd.AllowPaging = true;
        pd.PageSize = 8;
        pd.CurrentPageIndex = pageIndex - 1;
        pd.DataSource = getData_artitlesub().DefaultView;
        Datalist1.DataSource = pd;
        Datalist1.DataBind();
        lblCurrent.Text = Convert.ToString(pd.CurrentPageIndex + 1);
        lblCount.Text = pd.PageCount.ToString();
        i = pd.PageCount;
        if (pd.IsFirstPage)
        {
            lbtnFirst.Enabled = false;
            lbtnPrev.Enabled = false;
            lbtnNext.Enabled = true;
            lbtnLast.Enabled = true;
        }
        else
        {
            lbtnFirst.Enabled = true;
            lbtnPrev.Enabled = true;
        }
        if (pd.IsLastPage)
        {
            lbtnLast.Enabled = false;
            lbtnNext.Enabled = false;
        }
        else
        {
            lbtnLast.Enabled = true;
            lbtnNext.Enabled = true;
        }
    }

    protected void fabu_Command(object sender, CommandEventArgs e)
    {
        if (!string.IsNullOrEmpty(e.CommandArgument.ToString()))
        {
            switch (e.CommandName)
            {
                case "lkFb":

                    string strSetSql = string.Format("delete from tb_articlesubmit where art_id='{0}'", e.CommandArgument.ToString());
                    string strSql = string.Format(" insert into tb_article(classid,s_id,title,wznr,author,wfrom,tupian,tu,tcfl,ztfl,bszn_fl,tjrq,art_id) (select classid,s_id,title,wznr,author,wfrom,tupian,tu,tcfl,ztfl,bszn_fl,tjrq ,art_id from tb_articlesubmit where art_id='{0}')", e.CommandArgument.ToString().Trim());
                    ArrayList myAL = new ArrayList();
                    myAL.Add(strSql);
                    myAL.Add(strSetSql);
                    DbOperation.ExecuteSqlTran(myAL);
                    Response.Write("<script>alert('发布成功!')</script>");
                    keepSearch();

                    break;
                case "lkDelete":
                    string strDelSql = string.Format("delete tb_articlesubmit  where art_id='{0}'", e.CommandArgument.ToString());
                    int i = DbOperation.ExecuteSql(strDelSql);
                    if (i > 0)
                    {
                        Response.Write("<script>alert('删除成功!')</script>");
                    }
                    keepSearch();
                    break;

            }
        }
    }

    /// <summary>
    /// 保留搜索前的数据条件数据和更新操作后的数据
    /// </summary>
    private void keepSearch()
    {
        if (strSelect1 != null || strClass != null || strSclass != null)
        {
            ListItem item5 = select1.Items.FindByText(strSelect1);
            if (item5 != null)
            {
                item5.Selected = true;
            }
            ListItem item6 = dlclass.Items.FindByText(strClass);
            if (item6 != null)
            {
                item6.Selected = true;
            }
            ListItem item7 = dlsclass.Items.FindByText(strSclass);
            if (item7 != null)
            {
                item7.Selected = true;
            }
            tb_search.Text = strSearch;
            bindSearchData();
        }
    
    }

    protected void lbtnFirst_Command(object sender, CommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "First":
                getData(1);
                break;
            case "Next":
                getData(Convert.ToInt32(lblCurrent.Text) + 1);
                break;
            case "Prev":
                getData(Convert.ToInt32(lblCurrent.Text) - 1);
                break;
            case "Last":
                getData(i);
                break;
            default:
                getData(1);
                break;
        }
    }

    string strSelect1 =string.Empty;
    string strSearch = string.Empty;
    string strClass = string.Empty;
    string strSclass = string.Empty;
    protected void Datalist1_ItemCommand(object source, DataListCommandEventArgs e)
    {
        if (e.CommandName == "subok")
        {
             

            string strSetSql = string.Format("update tb_articlesubmit set isup='1' where art_id='{0}'", e.CommandArgument);
            //Response.Write("<script>"+strSetSql+"</script>");
            string strSql = string.Format(" insert into tb_article  select classid,s_id,title,wznr,author,wfrom,tupian,tu,tcfl,ztfl,bszn_fl from tb_articlesubmit where art_id='{0}'", e.CommandArgument);
            int i = DbOperation.ExecuteSql(strSetSql);
            if (i > 0)
            {
               int backLind= DbOperation.ExecuteSql(strSql);
               if (backLind > 0)
               {
                   Response.Write("<script>alert('提交成功!');location=location</script>");
                  
               }
            }
        }
    }
    protected void bt_search_Click(object sender, EventArgs e)
    {
        strSelect1 = select1.SelectedItem.ToString();
        strSearch = tb_search.Text;
        strClass = dlclass.SelectedItem.ToString();
        strSclass = dlsclass.SelectedItem.ToString();
        bindSearchData();
    }

    protected void bindSearchData()
    {
        string sql = string.Empty;
        string tbox = tb_search.Text.Trim();

        sql = "select title,author,to_char(tjrq,'yyyy-mm-dd') as tjrq,tjrq as tt,art_id from tb_articlesubmit where 1=1 ";
        if (select1.SelectedItem.ToString() == "标题")
        {
            sql += " and title like '%" + tbox + "%' ";//定义一条SQL语句
        }
        if (select1.SelectedValue == "strcontent")
        {
            sql += " and WZNR like '%" + tbox + "%'";//定义一条SQL语句
        }
        if (dlclass.SelectedItem.ToString() != "==文章类型==")
        {
            sql += " and s_id='" + dlsclass.SelectedValue + "'";//定义一条SQL语句
        }
        sql += "order by tt desc";

        DataTable dtSource = DbOperation.QueryBySql(sql);

        pd.DataSource = dtSource.DefaultView;//把数据集中的数据放入分页数据源中
        pd.PageSize = 10;
        Datalist1.DataSource = pd;//绑定Datalist
        Datalist1.DataBind();
    
    }


    protected void dlclass_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (dlclass.SelectedItem.ToString() != "==文章类型==")
        {
            string strdl2 = string.Format("select s_id,s_name,classid from tb_sclass where classid='{0}'", dlclass.SelectedValue);
            DataTable dtReturn = DbOperation.QueryBySql(strdl2);
            dlsclass.DataSource = dtReturn;
            dlsclass.DataBind();
        }
    }
}