﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="toushu.aspx.cs" Inherits="manage_toushu" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>信访信箱</title>
    <style type="text/css">
        body, div, a, span {
        margin:0 auto;padding:0;
        }
         #headFont a {
            color: #808080;
            text-decoration: none;
            font-weight: bolder;
            font-size: 20px;
        }
        #headFont {
         margin-bottom:20px;
         margin-top:20px;
        }
        #mainContent {
           
        
        }

    </style>
    <link href="../css/main.css" rel="stylesheet" />
</head>
<body style="width:100%">
    <form id="form1" runat="server">
        <div id="mainContent" >
            <div style="text-align: center; width: 100%;" id="headFont">
                <a>信访信箱</a>                        
            </div>
            <asp:DataList  ID="DataList1" runat="server" DataKeyField="id" OnItemCommand="DataList1_ItemCommand" OnItemDataBound="DataList1_ItemDataBound" HorizontalAlign="Center" OnSelectedIndexChanged="DataList1_SelectedIndexChanged" Width="900px">
                <ItemTemplate>
                    <table width="100%" border="0" class="table05" cellpadding="0" cellspacing="1">
                        <tr> <td width="60" align="center">标题：</td><td colspan="9"><%#Eval("title") %></td></tr>
                        <tr>
                            <td  align="center" style="width:60px" >编号：</td>
                            <td align="center"  ><%#Eval("id") %></td>
                            <td width="45" align="center">Email：</td>
                            <td width="90" align="center"><%#Eval("email") %></td>
                            <td width="40" align="center">电话：</td>
                            <td width="80" align="center"><%#Eval("tel") %></td>
                            <td width="45" align="center">时间:</td>
                            <td width="150" align="center"><%#Eval("tjrq") %></td>
                            <td width="45" align="center">
                                <asp:LinkButton ID="dldelete" runat="server" CommandName="delete" ForeColor="Red">删除</asp:LinkButton></td>
                            <td width="62"  align="center"><a href='<%#ConfigurationManager.AppSettings["webPageUrl"]%>?iid=<%#Eval("iid")%>&readonly=false&UserId=-1&FormName=<%#ConfigurationManager.AppSettings["xinfangMailReply"]%>&input_index=0' target="_self" style="color:green">回复</a>
                            </td>

                        </tr>
                        <tr>
                            <td height="50" align="center">内容：</td>
                            <td colspan="9"><%#Eval("wznr") %></td>
                        </tr>
                        <tr>
                            <td height="37" align="center">回复：</td>
                            <td align="center" colspan="7"><%#Eval("reply") %></td>
                            <td colspan="2" width="100"><%#Eval("r_name") %></td>
                        </tr>
                    </table>
                </ItemTemplate>
                <FooterTemplate>
                    <table border="0" style="width:100%;" class="table05" cellpadding="0" cellspacing="1">
                        <tr>
                            <td>当前页:<asp:Label ID="labCurrentPage" runat="server"></asp:Label>/
                <asp:Label ID="labPageCount" runat="server"></asp:Label>
                                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="first" Font-Underline="False" ForeColor="Black">首页</asp:LinkButton>
                                <asp:LinkButton ID="LinkButton2" runat="server" CommandName="pre" Font-Underline="False" ForeColor="Black">上一页</asp:LinkButton>
                                <asp:LinkButton ID="LinkButton3" runat="server" CommandName="next" Font-Underline="False" ForeColor="Black">下一页</asp:LinkButton>
                                <asp:LinkButton ID="LinkButton4" runat="server" CommandName="last" Font-Underline="False" ForeColor="Black">尾页</asp:LinkButton></td>
                        </tr>
                    </table>

                </FooterTemplate>
            </asp:DataList>
        </div>
    </form>
</body>
</html>
