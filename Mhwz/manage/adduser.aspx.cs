﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;

public partial class manage_adduser : System.Web.UI.Page
{
     
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["AdminName"] == null)
        {
            Response.Write("<script>alert('当前身份普通用户！您没有访问权限!');location='javascript:history.go(-1)';</script>");

        }
    }
    protected void btadd_Click(object sender, EventArgs e)
    {
        
        if (rbadmin.Checked == false && rbuser.Checked == false)
        {
            Response.Write("<script>alert('请选择身份');location='javascript:history.go(-1)';</script>");
        }
        else
        {
            if (rbadmin.Checked == true)
            {
                BusinessOperation.addadmin(txtname.Text.Trim(), jiami.Encrypting(txtpwd.Text.Trim()),"1");
                Response.Write("<script>alert('添加成功');location='javascript:history.go(-1)';</script>");

            }
            else
            {
                BusinessOperation.addadmin(txtname.Text.Trim(), jiami.Encrypting(txtpwd.Text.Trim()),"0");
                Response.Write("<script>alert('添加成功');location='javascript:history.go(-1)';</script>");

            }
        }
    }
}
