﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;

public partial class manage_user : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["AdminName"] != null)
        {
            look_user.DataSource = BusinessOperation.userbind();
            look_user.DataBind();
        }
        else
        {
            Response.Write("<script>alert('当前身份为普通用户！您没有访问权限!');location='javascript:history.go(-1)'</script>");

        }
    }
    protected void look_user_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        look_user.PageIndex = e.NewPageIndex;
        look_user.DataSource = BusinessOperation.userbind();
        look_user.DataBind();

    }
    protected void look_user_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

        BusinessOperation.deleteuser(look_user.DataKeys[e.RowIndex].Value.ToString());
        look_user.DataSource = BusinessOperation.userbind();
        look_user.DataBind();

    }
}