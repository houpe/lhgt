﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="mainPage.aspx.cs" Inherits="manage_mainPage" %>

<%@ Register Src="~/UserControl/LeftManageMenu.ascx" TagPrefix="uc1" TagName="LeftManageMenu" %>



<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>南京市国土资源局六合分局后台管理系统</title>
    <link href="../css/top_css.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" id="skinCss" type="text/css" href="../css/default/easyui.css" />
    <link rel="stylesheet" type="text/css" href="../css/icon.css" />
    <script type="text/javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" src="../js/jquery.easyui.min.js"></script>

    <script type="text/javascript">
        //设置标题头
        function loadTitle(strId, strTile) {
            $("#" + strId).panel({ title: strTile });
        }

        //显示进度条
        function ShowLoading() {
            //定时器2秒
            $.messager.progress({
                title: '请稍候',
                msg: '数据加载中...'
            });

        }

        //隐藏进度条
        function HideLoading() {
            $.messager.progress('close');

            $("#loadbox").hide();
        }

        //消息框
        function loadMsg(strMsg) {
            $("#divMsgContent").html(strMsg);
        }
        </script>
</head>

<body class="easyui-layout" id="divBody" onload="initWinInfo();HideLoading();" style="height: 100%;">
    <script type="text/javascript">
        ShowLoading();
    </script>
    <div id="divNorth" region="north" split="false" style="height: 47px; margin: 0px; padding: 0px" runat="server">
        <table style="background-image: url(../images/top_bg.gif); height: 44px" border="0"
            cellspacing="0" cellpadding="0" width="100%">
            <tbody>
                <tr>
                    <td style="color: #e04f22; font-size: 18pt" height="44">门户网站管理系统
                        
                    </td>
                    
                    <td height="44">
                       
                        <a style="float: right;display:inline-block;width:60px" href="loginout.aspx" target="_parent" >退出登录</a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div id="divwest" region="west" title='功能导航' split="true" style="width: 155px;">

        <div class="easyui-layout" fit="true" style="background: #ccc;">
            <div id="divMenu" region="center" split="true">
                 <uc1:LeftManageMenu runat="server" ID="LeftManageMenu" />
              
            </div>
            <div  region="south" style="height:150px;" title="待办事项">
                <div id="divMsgContent">消息提醒</div>
            </div>
        </div>

       
    </div>
    <div id="divsouth" region="south" split="false" style="overflow: hidden; height: 28px;">
        <div class="easyui-layout" fit="true" style="background: #ccc;">
            <div id="starusscale" region="west" split="true" style="width: 139px; z-index: -10;">
                <div id="skin">
                    皮肤：<a id="default" style="cursor: pointer; color: Blue;" class="selected" title="">蓝</a>
                    <a id="gray" style="cursor: pointer; color: Gray;" title="">灰</a>
                </div>
            </div>
            <div id="statusbar" region="center">
            </div>
        </div>
    </div>
    <div region="center" title="业务办公区" style="overflow: hidden;" id="divCenter">
        <div class="easyui-tabs" id="divMain" fit="true" border="true">
            <div id="divIndex" title="登录情况" icon="icon-search" closable="false" style="padding: 2px; overflow: hidden;">
                <iframe src="showlogin.aspx" width="100%" height="100%" frameboder="0" id="登录情况"></iframe>                
            </div>
        </div>
    </div>
</body>
</html>
