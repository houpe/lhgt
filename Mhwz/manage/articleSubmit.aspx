﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="articleSubmit.aspx.cs" Inherits="manage_articleSubmit" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <style type="text/css">
        #tableSearch td {
            border: 1px solid #87CEFA;
            height: 22px;
            empty-cells: show;
            border-collapse: collapse;
        }

        .auto-style1 {
            width: 260px;
        }

        .auto-style2 {
            width: 186px;
        }

        #tableSearch {
            float: left;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div style="margin: 0px auto; text-align: center; width: 970px;">
            <table border="0" cellpadding="0" cellspacing="1" style="width: 100%;margin:0 auto" id="tableSearch">
                <tr style="font-size:14px;">
                    <td>文章搜索:</td>
                    <td class="auto-style2">
                        <asp:DropDownList ID="select1" runat="server" Style="margin-left: 0px">
                            <asp:ListItem Value="strtitle">标题</asp:ListItem>
                            <asp:ListItem Value="strcontent">内容</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td class="auto-style1">
                        <asp:TextBox ID="tb_search" runat="server" Width="186px" Text=""></asp:TextBox></td>
                    <td></td>
                </tr>
                <tr style="font-size:14px;">
                    <td>组配分类：</td>
                    <td class="auto-style2">

                        <asp:DropDownList ID="dlclass" runat="server" DataTextField="c_name" DataValueField="classid" AutoPostBack="true" OnSelectedIndexChanged="dlclass_SelectedIndexChanged" Width="100px">
                        </asp:DropDownList>

                    </td>
                    <td class="auto-style1">
                        <asp:DropDownList ID="dlsclass" runat="server" DataTextField="s_name" DataValueField="s_id" Width="186px">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:Button ID="Button1" runat="server" Text="搜索" OnClick="bt_search_Click" Width="86px" /></td>
                </tr>
            </table>
            <asp:DataList ID="Datalist1" Width="100%" runat="server" OnItemCommand="Datalist1_ItemCommand" Font-Size="14px">
                <HeaderTemplate>
                    <table style="margin: 0 auto; padding: 0; border: 1px solid #87CEFA; width: 100%; font-size:14px;font-weight:bold;color:#646464" cellpadding="0" cellspacing="0" id="tbhead">
                        <tr>
                            <td style="width: 265px; text-align: center; border-left: 1px solid #87CEFA">文章ID</td>
                            <td style="width: 150px; text-align: center; border-left: 1px solid #87CEFA">文章标题</td>
                            <td style="width: 150px; text-align: center; border-left: 1px solid #87CEFA">审核文章</td>
                            <td style="width: 155px; text-align: center; border-left: 1px solid #87CEFA">添加日期</td>
                            <td style="width: 50px; text-align: center; border-left: 1px solid #87CEFA">删除</td>
                            <td style="width: 50px; text-align: center; border-left: 1px solid #87CEFA">编辑</td>
                            <td style="width: 150px; text-align: center; border-left: 1px solid #87CEFA; border-right: 1px solid #87CEFA">文章作者</td>
                        </tr>
                    </table>
                </HeaderTemplate>
                <ItemTemplate>
                    <table id="tbcontent" runat="server" cellpadding="0" cellspacing="0" style="padding: 0; border-collapse: collapse; empty-cells: show; border: 1px solid #87CEFA; width: 100%; font-size:13px; margin:0 auto;">
                        <tr>
                            <td style="width: 265px; text-align: center; border-left: 1px solid #87CEFA">
                                <asp:Label ID="art_id" runat="server" Text='<%#Eval("art_id") %>'>  
                                </asp:Label>
                            </td>
                            <td style="width: 150px; text-align: center; border-left: 1px solid #87CEFA">
                                <asp:Label ID="lbltitle" runat="server" Text='<%#Eval("title") %>'>  
                                </asp:Label>
                            </td>

                            <td style="width: 155px; text-align: center; border-left: 1px solid #87CEFA">
                                <asp:LinkButton runat="server" OnCommand="fabu_Command"  ID="fabu" CommandName="lkFb" CommandArgument='<%# Eval("art_id")%>'>同意发布</asp:LinkButton>
                               <%-- <a href="javascript:window.location.href='articleSubmit.aspx?art_id=<%#Eval("art_id") %>&type=2'">同意发布</a>--%>

                            </td>

                            <td style="width: 150px; text-align: center; border-left: 1px solid #87CEFA">
                                <asp:Label ID="lbldate" runat="server" Text='<%#Eval("tjrq") %>'>  
                                </asp:Label>
                            </td>
                            <td style="width: 50px; text-align: center; border-left: 1px solid #87CEFA">
                         <%--       <a href="javascript:window.location.href='articleSubmit.aspx?art_id=<%#Eval("art_id") %>&type=0'">删除</a>--%>
                                <asp:LinkButton runat="server" OnCommand="fabu_Command"  ID="LinkButton1" CommandName="lkDelete" CommandArgument='<%# Eval("art_id")%>'>删除</asp:LinkButton>
                            </td>
                            <td style="width: 50px; text-align: center; border-left: 1px solid #87CEFA">
                                <a href='userArticleEdit.aspx?art_id=<%#Eval("art_id") %>&type=1' target="_blank">编辑</a>
                            </td>

                            <td style="width: 150px; text-align: center; border-left: 1px solid #87CEFA; border-right: 1px solid #87CEFA">
                                <asp:Label ID="lblauthor" runat="server" Text='<%#Eval("author") %>'>  
                                </asp:Label>

                            </td>
                        </tr>
                    </table>
                </ItemTemplate>
            </asp:DataList>
            <div style="margin: 0 auto; display: inline-block; width: 100%; font-size: 13px">
                第<asp:Label ID="lblCurrent" runat="server"></asp:Label>页/
    共<asp:Label ID="lblCount" runat="server" Text=""></asp:Label>页
        <asp:LinkButton ID="lbtnFirst" runat="server" CommandName="First" OnCommand="lbtnFirst_Command">首页</asp:LinkButton>
                <asp:LinkButton ID="lbtnPrev" runat="server" CommandName="Prev"
                    OnCommand="lbtnFirst_Command">上一页</asp:LinkButton>
                <asp:LinkButton ID="lbtnNext" runat="server" CommandName="Next"
                    OnCommand="lbtnFirst_Command">下一页</asp:LinkButton>
                <asp:LinkButton ID="lbtnLast" runat="server" CommandName="Last"
                    OnCommand="lbtnFirst_Command">末页</asp:LinkButton>
            </div>
        </div>

    </form>
</body>
</html>
