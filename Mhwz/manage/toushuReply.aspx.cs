﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
public partial class manage_toushuReply : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string strSql = string.Format("select id,name,email,tel,wznr,tjrq,r_name,reply from tb_tousu where id='{0}'",Request.QueryString["id"]);//定义一条SQL语句
            DataTable dtReturn = DbOperation.QueryBySql(strSql);           
            DataList1.DataSource = dtReturn;//绑定Datalist
            DataList1.DataBind();
        }
        
    }
    protected void bttj_Click(object sender, EventArgs e)
    {
        bt(txtboxname.Text.Trim(),txtboxcontent.Text.Trim());
        Response.Write("<script>alert('回复成功');location='toushu.aspx';</script>");
    }
    protected void btqx_Click(object sender, EventArgs e)
    {
        this.txtboxname.Text ="";
        this.txtboxcontent.Text ="";
    }
    public void bt(string btname,string btcontent)
    {
        string strSql = string.Format("update tb_tousu set r_name='{0}',reply='{1}' where id='{2}'",btname,btcontent,Request.QueryString["id"]);//定义一条SQL语句
        DbOperation.ExecuteSql(strSql);
       
    }
   
}
