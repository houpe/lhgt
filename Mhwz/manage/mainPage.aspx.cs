﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class manage_mainPage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["AdminName"] == null)
        {
            Response.Write("<script>window.location.href='login-admin.aspx';</script>");

        }

        if (!IsPostBack)
        {
            //获取设置标题头
            string strMsg = "<script>function initWinInfo(){";

            strMsg += string.Format("loadTitle('divCenter','业务办公区【欢迎您，{0}，今天是{1}】');HideLoading();", Session["AdminName"], DateTime.Now.ToShortDateString());

            //获取待办
            string strPopContent = "<table>";
            strPopContent += string.Format("<tr><td><a href=javascript:addTab('liuyanList.aspx?type=1','局长信箱')>局长信箱中有{0}条消息</a></td></tr>", BusinessOperation.GetTiXing(1));

            strPopContent += string.Format("<tr><td><a href=javascript:addTab('toushu.aspx?type=1','信访信箱')>信访信箱中有{0}条消息</a></td></tr>", BusinessOperation.GetTiXing(2));

            string strWebPageUrl = ConfigurationManager.AppSettings["webPageUrl"];
            strPopContent += string.Format("<tr><td><a href=javascript:addTab('{1}?iid=-1&readonly=false&UserId=-1&FormName=1db5d436-0e9f-4637-9ddf-f98849797e5f&input_index=0&type=1','政务咨询')>有{0}条政务咨询信息</a></td></tr>", BusinessOperation.GetTiXing(3), strWebPageUrl);

            strPopContent += string.Format("<tr><td><a href=javascript:addTab('{1}?iid=-1&readonly=false&UserId=-1&FormName=705e6eb8-e053-4e35-b061-2ec4eaa5e6f8&input_index=0','依申请公开')>有{0}条依申请公开信息</a></td></tr>", BusinessOperation.GetTiXing(4), strWebPageUrl);

            strPopContent += string.Format("<tr><td><a href=javascript:addTab('articleSubmit.aspx','文章审核')>有{0}条新增文章信息</a></td></tr>", BusinessOperation.GetTiXing(5), strWebPageUrl);

            strPopContent += "</table>";
            strMsg += string.Format("loadMsg(\"{0}\");", strPopContent);

            //针对
            //strMsg += "addTab('MyWindow/TaskInStepList.aspx', '我的待办');";
            strMsg += "}</script>";

            Response.Write(strMsg);
        }
    }

    protected void lnkbtnOut_Click(object sender, EventArgs e)
    {
        if (Session["AdminName"] != null)
        {
            Session["AdminName"] = null;
            Response.Cookies["AdminName"].Expires = DateTime.Now.AddYears(-1);
            Response.Redirect("login-admin.aspx");
            Response.End();
        }
    }
}
