﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;

public partial class manage_toushu : System.Web.UI.Page
{
    protected PagedDataSource pds = new PagedDataSource();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["AdminName"] != null)
        {
            if (!IsPostBack)
            {
                BindDataList(0);
            }
        }
        else
        {
            Response.Write("<script>alert('您没有访问权限!');top.location='login-admin.aspx';</script>");

        }
    }
    private void BindDataList(int currentpage)
    {
        pds.AllowPaging = true;//允许分页
        pds.PageSize = 3;//每页显示9条数据
        pds.CurrentPageIndex = currentpage;//当前页为传入的一个int型值
        string strSql = string.Empty;
        if (!string.IsNullOrEmpty(Request["type"]))
        {
            if (Request["type"] == "1")
            {
                strSql = "select * from tb_tousu where reply is null order by tjrq desc";

            }
        }
        else 
        {
            strSql = "select * from tb_tousu order by tjrq desc";//定义一条SQL语句
        }
        //打开与DB的连接 
        DataTable dtReturn = DbOperation.QueryBySql(strSql);
        pds.DataSource = dtReturn.DefaultView;//把数据集中的数据放入分页数据源中
        DataList1.DataSource = pds;//绑定Datalist
        DataList1.DataBind();
        ViewState["pageCount"] = pds.PageCount;
    }
    protected void DataList1_ItemCommand(object source, DataListCommandEventArgs e)
    {
            switch (e.CommandName)
            {
                //以下四个为 捕获用户点击 上一页 下一页等时发生的事件
                case "first":
                    ViewState["currPageIndex"] = "0";
                    pds.CurrentPageIndex = Convert.ToInt32(ViewState["currPageIndex"]);
                    BindDataList(pds.CurrentPageIndex);
                    break;
                case "pre":
                    ViewState["currPageIndex"] = Convert.ToInt32(ViewState["currPageIndex"]) - 1;
                    pds.CurrentPageIndex = Convert.ToInt32(ViewState["currPageIndex"]);
                    BindDataList(pds.CurrentPageIndex);
                    break;
                case "next":
                    ViewState["currPageIndex"] = Convert.ToInt32(ViewState["currPageIndex"]) + 1;
                    pds.CurrentPageIndex = Convert.ToInt32(ViewState["currPageIndex"]);
                    BindDataList(pds.CurrentPageIndex);
                    break;
                case "last":
                    ViewState["currPageIndex"] = Convert.ToInt32(ViewState["pageCount"]) - 1;
                    pds.CurrentPageIndex = Convert.ToInt32(ViewState["currPageIndex"]);
                    BindDataList(pds.CurrentPageIndex);
                    break;
                case "delete":
                    //删除用户选中的单条信息
                    string id = (DataList1.DataKeys[e.Item.ItemIndex]).ToString();
                    if (DbOperation.ExecuteSql("delete from tb_tousu WHERE id='" + id+"'") > 0)
                    {
                        Response.Write("<script>alert('删除成功!');window.location.href=window.location.href</script>");
                    }
                    
                    BindDataList(0);
                    break;
            }
   

        }
    protected void DataList1_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Footer)
        {
            //以下六个为得到脚模板中的控件,并创建变量.
            Label CurrentPage = e.Item.FindControl("labCurrentPage") as Label;
            Label PageCount = e.Item.FindControl("labPageCount") as Label;
            LinkButton FirstPage = e.Item.FindControl("LinkButton1") as LinkButton;
            LinkButton PrePage = e.Item.FindControl("LinkButton2") as LinkButton;
            LinkButton NextPage = e.Item.FindControl("LinkButton3") as LinkButton;
            LinkButton LastPage = e.Item.FindControl("LinkButton4") as LinkButton;
            CurrentPage.Text = (pds.CurrentPageIndex + 1).ToString();//绑定显示当前页
            PageCount.Text = pds.PageCount.ToString();//绑定显示总页数
            if (pds.IsFirstPage)//如果是第一页,首页和上一页不能用
            {
                FirstPage.Enabled = false;
                PrePage.Enabled = false;
            }
            if (pds.IsLastPage)//如果是最后一页"下一页"和"尾页"按钮不能用
            {
                NextPage.Enabled = false;
                LastPage.Enabled = false;
            }
        }
    }
    protected void DataList1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}
