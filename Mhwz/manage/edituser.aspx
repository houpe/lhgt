﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="edituser.aspx.cs" Inherits="manage_edituser" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>编辑与修改</title>
    <style type="text/css">
        td {
            text-align: center;
            height: 20px;
            width: 60px;
            font-size: 15px;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div style="text-align: center">
                <table border="1" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>ID编号</td>
                        <td>用户名</td>

                        <td>修改</td>
                        <td>删除</td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <asp:DataList ID="dluser" runat="server" DataKeyField="id">
                                <ItemTemplate>
                                    <table border="1" cellpadding="0" cellspacing="0" width="400px">
                                        <tr>
                                            <td><%#Eval("id") %></td>
                                            <td><%#Eval("name") %></td>
                                            <td><a href='firmpwd.aspx?id=<%#Eval("id") %>' target="main">修改</a></td>
                                            <td>
                                                <a href="edituser.aspx" onclick="if(confirm('你确定删除吗?'))window.open('deleteuser.aspx?id=<%#Eval("id") %>','','width=20,height=20')" target="main">删除</a></td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:DataList></td>
                    </tr>
                </table>
            </div>
        </div>


    </form>
</body>
</html>
