﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="adduser.aspx.cs" Inherits="manage_adduser" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>无标题页</title>
    <style type="text/css">
        td {
            font-size: 14px;
            background-color: #FFFFFF;
        }

        table {
            background-color: #87CEFA;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div align="center">
            <table border="0" cellpadding="0" cellspacing="1">
                <tr>
                    <td colspan="3" style="height: 26px">请输入用户名和密码</td>
                </tr>
                <tr>
                    <td style="width: 75px; height: 24px;">用户名:</td>
                    <td colspan="2" style="height: 24px">
                        <asp:TextBox ID="txtname" runat="server" Width="113px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" Display="dynamic" ControlToValidate="txtname"></asp:RequiredFieldValidator></td>
                </tr>
                <tr>
                    <td style="width: 75px; height: 19px;">密 码:</td>
                    <td colspan="2" style="height: 19px">

                        <asp:TextBox ID="txtpwd" runat="server" TextMode="password" Width="113px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" Display="dynamic" ControlToValidate="txtpwd"></asp:RequiredFieldValidator></td>
                </tr>
                <tr>
                    <td style="width: 75px; height: 22px;">权限:</td>
                    <td style="width: 55px; height: 22px;">
                        <asp:RadioButton ID="rbadmin" runat="server" GroupName="rb" Text="管理员" Width="109px" />
                    </td>
                    <td style="width: 100px; height: 22px">
                        <asp:RadioButton ID="rbuser" GroupName="rb" runat="server" Text="普通用户" Width="101px" /></td>

                </tr>
                <tr>
                    <td align="center" colspan="3" style="height: 26px">
                        <asp:Button ID="btadd" runat="server" Height="23px" Text="新增" Width="58px" OnClick="btadd_Click" /></td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
