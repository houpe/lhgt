﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;

public partial class manage_lanmu : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["AdminName"] != null)
        {

            if (!IsPostBack)
            {
             
                lmlb1.DataSource = dlbind();
                lmlb1.DataBind();
                lmlb2.DataSource = dlbind();
                lmlb2.DataBind();
                zlm.DataSource = zlmbind();
                zlm.DataBind();
            
            }
       }
       else
       {
           Response.Write("<script>alert('您没有访问权限!');location='login-admin.aspx';</script>");

       }
    }
    protected void Button1_Click(object sender, EventArgs e)  //删除一级栏目
    {
      BusinessOperation.deletelm(lmlb1.SelectedValue);
      Response.Write("<script>alert('删除成功');location='lanmu.aspx';</script>");
    }
    protected void Button2_Click(object sender, EventArgs e)//更改一级栏目名称
    {
     BusinessOperation.updatelm(lm_txtname.Text.Trim().Replace("'","''"),lmlb1.SelectedValue);
     Response.Write("<script>alert('更改成功');location='lanmu.aspx';</script>");

    }
    protected void Button3_Click(object sender, EventArgs e)
    {
        BusinessOperation.insertlm(lm_strname.Text.Trim().Replace("'","''"));
        Response.Write("<script>alert('添加成功');location='lanmu.aspx';</script>");

    }
    protected void Button4_Click(object sender, EventArgs e)  // 二级栏目  删除
    {
     BusinessOperation.zlmdelete(zlm.SelectedValue);
     Response.Write("<script>alert('删除成功');location='lanmu.aspx';</script>");

    }
    protected void Button5_Click(object sender, EventArgs e)   //更改
    {
     BusinessOperation.zlmupdate(zlm_txtname.Text.Trim().Replace("'","''"),zlm.SelectedValue);
     Response.Write("<script>alert('更改成功');location='lanmu.aspx';</script>");

    }
    protected void Button6_Click(object sender, EventArgs e)  //添加
    {
     BusinessOperation.insertzlm(zlm_add.Text.Trim().Replace("'","''"),lmlb2.SelectedValue);
     Response.Write("<script>alert('添加成功');location='lanmu.aspx';</script>");

    }
    public  DataTable dlbind()     //后台一级栏目
    {
        string sql = "select classid,c_name from tb_class order by classid asc ";
        return DbOperation.QueryBySql(sql);
    }
    public  DataTable zlmbind()   //子栏目
    {
        string sql = "select s_id,s_name from tb_sclass order by s_id asc";
        return DbOperation.QueryBySql(sql);

    }
 
}
