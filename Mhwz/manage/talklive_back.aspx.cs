﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data.OleDb;
using System.Web.UI.WebControls;
using WF_Business;
using System.Configuration;
using System.Data;

public partial class tablelive_back : System.Web.UI.Page
{
    
   // public string webmaxid;
  
    protected void Page_Load(object sender, EventArgs e)
    { 
        string IID = Request["iid"].ToString(); 

        string TYPE = Request["type"];
        if (!IsPostBack)
        {
            //判断TYPE为3时表示为编辑
            if (TYPE == "3" && IID != null)
            {
                DataTable dtReturn = DbOperation.QueryBySql("select 访谈名称,访谈嘉宾,访谈主持,标题图片,to_char(开始时间,'yyyy-mm-dd') as 开始时间,嘉宾介绍,访谈说明,文字实录 from TB_TALKLIVE  where iid=" + IID);
                talkName.Text = dtReturn.Rows[0]["访谈名称"].ToString();
                talkPerson.Text = dtReturn.Rows[0]["访谈嘉宾"].ToString();
                talkHost.Text = dtReturn.Rows[0]["访谈主持"].ToString();
                tboxtu.Text = dtReturn.Rows[0]["标题图片"].ToString();
                talkAction.Text = dtReturn.Rows[0]["开始时间"].ToString();


                talkGuest.Text = dtReturn.Rows[0]["嘉宾介绍"].ToString();
                talkInstruction.Text = dtReturn.Rows[0]["访谈说明"].ToString();
                vedioAddress.Text = dtReturn.Rows[0]["文字实录"].ToString();

            }

        }
    }


    protected void btnUpload_Click(object sender, EventArgs e)
    {
    
        if (FileUploadPic.HasFile)
        {
            if (FileUploadPic.PostedFile.ContentLength < 10485760)
            {
                try
                {
                    string fileExt, path;
                    fileExt = FileUploadPic.FileName.ToLower();//文件名
                    int i = fileExt.LastIndexOf(".");
                    fileExt = fileExt.Substring(i, fileExt.Length - i);

                     string strFileName = Guid.NewGuid().ToString();
                    path = "~/pic/upload/" + strFileName + fileExt;
                    this.FileUploadPic.PostedFile.SaveAs(HttpContext.Current.Server.MapPath(path));
                    Response.Write("<script>alert('上传成功！');</script>");
                    tboxtu.Text= "pic/upload/" + strFileName + fileExt;
                }
                catch
                {
                    Response.Write("<script>alert('上传失败！');</script>");

                }
            }
            else
            {
                Response.Write("<script>alert('上传文件不能大于10MB！');</script>");

            }
        }
        else
        {
            Response.Write("<script>alert('请选择文件!');</script>");

        }
    }


    protected void btnOK_Click(object sender, EventArgs e)
    {
        string btIID = Request["iid"].ToString(); 
        var i = 0;
        string Talkperson="";
        string Talkhost="";
        string Talkpic ="";
        string Talktime="";
         string Talkguest="";
        string TalkIns="";
        string Vedioaddress="";
            if (talkPerson.Text != "" && talkPerson.Text != null)
            {
                Talkperson = talkPerson.Text;
            }
            if (talkHost.Text != "" && talkHost.Text != null)
            {
                Talkhost = talkHost.Text;
            }
            if (tboxtu.Text != "" && tboxtu.Text != null)
            {
                Talkpic = tboxtu.Text;
            }
            if (talkGuest.Text != null && talkGuest.Text != "")
            {
                Talkguest = talkGuest.Text;
            }
            if (talkInstruction.Text != null && talkInstruction.Text != "")
            {
                TalkIns = talkInstruction.Text;
            }
            if (vedioAddress.Text != null && vedioAddress.Text != "")
            {
                Vedioaddress = vedioAddress.Text;
            }
            if (talkAction.Value != null && talkAction.Value != "")
            {
                Talktime = talkAction.Value;
            }
            if(Request["type"]=="1"&&btIID!=null)
            {
                string strSql = string.Format("insert into tb_talklive (访谈名称,开始时间,访谈嘉宾,访谈主持,标题图片,嘉宾介绍,访谈说明,文字实录,iid) values('{0}',To_date('{1}','yyyy-mm-dd hh24-mi-ss'),'{2}','{3}','{4}','{5}','{6}','{7}','{8}')", talkName.Text, Talktime, Talkperson, Talkhost, Talkpic, Talkguest, TalkIns, Vedioaddress, btIID);
           i = DbOperation.ExecuteSql(strSql);
           if (i > 0)
           {
               Response.Write("<script>alert('添加成功!')</script>");
           }
         }
            if (Request["type"] == "3" && btIID != null)
         {
             string strSql = string.Format("update TB_TALKLIVE set 访谈名称='{0}',开始时间=To_date('{1}','yyyy-mm-dd hh24-mi-ss'), 访谈嘉宾='{2}',访谈主持='{3}',标题图片='{4}',嘉宾介绍='{5}',访谈说明='{6}',文字实录='{7}' where iid='" + btIID + "'", talkName.Text, Talktime, Talkperson, Talkhost, Talkpic, Talkguest, TalkIns, Vedioaddress);
            i = DbOperation.ExecuteSql(strSql);
           if (i > 0)
           {
               Response.Write("<script>alert('修改成功!')</script>");
           }
         }
        
        
        }
  
 
       
    
    protected void btnUpvedio_Click(object sender, EventArgs e)
    {

        if (FileUploadVedio.HasFile)
        {
            if (FileUploadVedio.PostedFile.ContentLength < 100485760)
            {
                try
                {
                    string fileExt, path;
                    fileExt = FileUploadVedio.FileName.ToLower();//文件名
                    int i = fileExt.LastIndexOf(".");
                    fileExt = fileExt.Substring(i, fileExt.Length - i);
                    string strFileName = Guid.NewGuid().ToString();
                    path = "~/pic/upload/" + strFileName + fileExt;
                    this.FileUploadVedio.PostedFile.SaveAs(HttpContext.Current.Server.MapPath(path));
                    Response.Write("<script>alert('上传成功！');</script>");
                    vedioAddress.Text = "pic/upload/" + strFileName + fileExt;
                }
                catch
                {
                    Response.Write("<script>alert('上传失败！');</script>");

                }
            }
            else
            {
                Response.Write("<script>alert('上传文件不能大于100MB！');</script>");

            }
        }
        else
        {
            Response.Write("<script>alert('请选择文件!');</script>");

        }
    }
 
}
