﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="editMenu.aspx.cs" Inherits="manage_editMenu" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style type="text/css">
        div, body, span, a, img {
            margin: 0 auto;
            padding: 0;
        }

        #content {
            width: 98%;
        }

            #content table {
                border-collapse: collapse;
                font-size: 12px;
                color: #666;
                background: #fff;
            }

                #content table tr td {
                    height: 30px;
                    line-height: 30px;
                    padding: 0 10px;
                    border: 1px solid #ccc;
                }
    </style>
    <script src="../js/jquery.js" type="text/javascript"></script>
    <script src="../js/Jquery.Global.js" type="text/javascript"></script>
    <script src="../js/Jquery.Common.js"></script>

    <script type="text/javascript">
        var serviceUrl = Common.GetLocalPath()+"<%=ConfigurationManager.AppSettings["moaServiceUrl"]%>";
        var strServicePath = serviceUrl+"/GetFirstMenu";
        var strServicePath2 = serviceUrl + "/GettwoMenu";
        var strServicePathExSql = serviceUrl + "/ExSql";

        //获取第一级菜单调用方法
        function pageLoad() {
            WebService(strServicePath, GetMenu, "{}");
        }
        //获取第一级菜单
        function GetMenu(result) {
            if (result != null) {
                var JsonResult = $.parseJSON(result);
                $("#content").html("");
                $("#content").append("<table cellspacing='0' style='width:100%;' cellpadding='1' class='tab' id='tab'></table>");
                $("#content table").append("<tr ><td style='text-align:right' colspan='3' ><input type='button' value='添加一级菜单' onclick='AddMenu()'/></td> </tr>");

                $(JsonResult).each(function (index, obj) {
                    var row = "<tr style='background-color:#DFDFD9'><td id='row1_" + index + "' > " + obj.c_name + "</td> <td id='row2_" + index + "' > </td>";
                    row += "<td id='row3_" + index + " '><input name='" + obj.classid + "' type='button' value='增加子目录' onclick='addMenu2(this)'  /><input type='button' name='" + obj.classid + "' value='编辑' onclick='editParMenu(this)' /><input type='button' value='删除' title='" + obj.classid + "' onclick='delMenu1(this)'/></td></tr>";
                    $("#tab").append(row);
                })

                //获取第二级菜单调用方法
                WebService(strServicePath2, GetMenu2, "{}");
            }            
        }

        function GetMenu2(result) {
            if (result != null) {
                var JsonResult = $.parseJSON(result);
                var trlength = $("#tab tr").length;
                for (var i = 0; i < trlength - 1; i++) {
                    $(JsonResult).each(function (index, obj) {
                        if ($.trim($("#row1_" + i + "").text()) == $.trim(obj.c_name)) {
                            $("#row1_" + i).parent().after("<tr onmousemove='moveColor(this)' onmouseout='outColor(this)'><td></td><td>" + obj.s_name + "</td><td><input title='" + obj.s_id + "' type='button' value='编辑' onclick='editChMenu(this)'/> <input type='button' value='删除' title='" + obj.s_id + "' onclick='delMenu(this)'/></td></tr>");

                        }
                    })
                }
            }
        }

        function AddMenu() {
            var nLeft = (screen.width - 490) / 2;
            var nTop = (screen.height - 320) / 2;
            window.open("submitEditMenu.aspx?type=0&level=0", "SUBMIT", "toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=no,width=490,height=320,left=" + nLeft + ",top=" + nTop, true);

        }
        function addMenu2(obj) {

            var parMenu = $(obj).attr("name");
            var nLeft = (screen.width - 490) / 2;
            var nTop = (screen.height - 320) / 2; 
            window.open("submitEditMenu.aspx?type=0&level=1&editId=" + parMenu, "SUBMIT", "toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=no,width=490,height=320,left=" + nLeft + ",top=" + nTop, true);

        }
        //编辑子目录
        function editChMenu(obj) {
            //var Menuname = $(obj).parent().parent().find("td:eq(1)").text();
            var editId = $(obj).attr("title");
            var nLeft = (screen.width - 490) / 2;
            var nTop = (screen.height - 320) / 2;
            //WebService(strServicePath, BackEditName, '{"type":"3","level":"1","par":"' + editId + '","oldpar":"","newpar":""}');
            window.open("submitEditMenu.aspx?type=1&level=1&editId=" + editId + "", "SUBMIT", "toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=no,width=490,height=320,left=" + nLeft + ",top=" + nTop, true);
        }

        function editParMenu(obj) {
            var Menuname = $(obj).attr("name");
            var nLeft = (screen.width - 490) / 2;
            var nTop = (screen.height - 320) / 2;
            window.open("submitEditMenu.aspx?type=1&level=0&editId=" + Menuname + "", "SUBMIT", "toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=no,width=490,height=320,left=" + nLeft + ",top=" + nTop, true);

        }


        function delMenu(obj) {

            WebService(strServicePathExSql, Backline, '{"type":"2","level":"1","par":"' + $(obj).attr("title") + '","oldpar":"","newpar":""}');

        }

        //调用删除第一级目录
        function delMenu1(obj) {
            WebService(strServicePathExSql, Backline, '{"type":"2","level":"0","par":"' + $(obj).attr("title") + '","oldpar":"","newpar":""}');

        }

        function Backline(result) {
            if (result != "" && result != null) {
                window.location.href = window.location.href;
                alert("操作成功");

            }
        }

        function moveColor(obj) {
            $(obj).css("background-color", "#18E56B");
        }
        function outColor(obj) {

            $(obj).css("background-color", "");
        }
        // function BackEditName(result) {

        //     alert(result);
        //  }
    </script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>栏目管理</title>
</head>
<body onload="pageLoad();">
    <form id="form1" runat="server">
        <div style="width: 100%" id="content">
        </div>
    </form>
</body>
</html>
