﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="talklive_back.aspx.cs" Inherits="tablelive_back" %>

<%@ Register Src="~/UserControl/Calendar.ascx" TagPrefix="uc1" TagName="Calendar" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>访谈直播维护</title>
    <script src="../js/jquery.js"></script>
    <script src="../js/Jquery.Global.js" type="text/javascript"></script>
    <script type="text/javascript">
        
        function onLoad2() {
            $("#btnOK").bind("click", function checkInput() {
                if ($("#talkName").val() == null || $("#talkName").val() == "") {
                    $("#talkName")[0].focus();
                    return false;
                }
                if ($("#talkPerson").val() == null || $("#talkPerson").val() == "") {
                    $("#talkPerson")[0].focus();
                    return false;
                }
                if ($("#talkAction").val() == null || $("#talkAction").val() == "") {
                    $("#talkAction")[0].focus();
                    return false;
                }

            })

        }
    </script>


    <style type="text/css">
        table, div, body, span {
            margin: 0 auto;
            padding: 0;
        }

        /*#contenttb {
            border-right: 2px solid #999999;
            border-bottom: 2px solid #999999;
        }

            #contenttb td {
                height: 35px;
                border-left: 2px solid #999999;
                border-top: 2px solid #999999;
            }

            #contenttb tr td:first-child {
                width: 188px;
                border-right: 2px  solid #CCCCCC;
                /*background-color:#BFDBFF;*/

        /*color:#1E5494;
            }*/
        #headFont a {
            color: #808080;
            text-decoration: none;
            font-weight: bolder;
            font-size: 20px;
        }

        #tboxtu {
            width: 182px;
        }

        #maindiv {
            width: 100%;
            height: 100%;
            padding-top: 50px;
        }

        body {
            /*background-color:#E3EEFB;*/
        }

        #btnDiv {
            margin-top: 18px;
        }
    </style>
</head>
<body onload="onLoad2()">

    <form id="form1" runat="server">

        <div style="text-align: center;" id="maindiv">
            <p id="headFont"><a>访谈管理</a></p>
            <table style="width: 900px; background-color: #FFFFFF; border: solid 1px  #d9d9d9;" border="1" id="contenttb" cellpadding="0" cellspacing="1">
                <tr>
                    <td>专访名称:</td>
                    <td style="text-align: left">

                        <asp:TextBox ID="talkName" runat="server" Height="20px" Width="300px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>访谈嘉宾:</td>
                    <td style="text-align: left">
                        <asp:TextBox ID="talkPerson" runat="server" Height="20px" Width="300px"></asp:TextBox>
                    </td>
                </tr>

                <tr>
                    <td>访谈主持:</td>
                    <td style="text-align: left">
                        <asp:TextBox ID="talkHost" runat="server" Height="20px" Width="300px"></asp:TextBox></td>
                </tr>

                <tr>
                    <td>标题图片:</td>
                    <td style="text-align: left">

                        <asp:FileUpload ID="FileUploadPic" runat="server" Height="24px" />
                        <asp:Button ID="btnUpload" runat="server" Text=" 上传 " Width="73px" Height="24px" OnClick="btnUpload_Click" />
                        <div id="Msg">图片地址:<asp:TextBox ID="tboxtu" runat="server" Height="18px" Width="163px" ReadOnly="true"></asp:TextBox></div>
                    </td>
                </tr>

                <tr>
                    <td>开始时间:</td>
                    <td style="text-align: left">
                        <uc1:Calendar runat="server" DisappearTime="false" ID="talkAction" />
                        <br />
                    </td>
                </tr>

                <tr>
                    <td>嘉宾介绍:</td>
                    <td style="text-align: left">

                        <asp:TextBox ID="talkGuest" runat="server" Height="20px" Width="300px"></asp:TextBox>
                    </td>
                </tr>

                <tr>
                    <td>访谈说明:</td>
                    <td style="text-align: left">

                        <asp:TextBox ID="talkInstruction" runat="server" Height="20px" Width="300px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>文字实录:</td>
                    <td style="text-align: left">
                        <asp:FileUpload ID="FileUploadVedio" runat="server" Width="228px" Height="20px" /><asp:Button ID="btnUpvedio" runat="server" Text="上传" OnClick="btnUpvedio_Click" Width="70px" />
                        <div>视频地址:<asp:TextBox ID="vedioAddress" runat="server" Height="18px" Width="163px" ReadOnly="true"></asp:TextBox></div>
                    </td>
                </tr>
            </table>
            <div id="btnDiv">
                <asp:Button ID="btnOK" runat="server" Text="确定" OnClick="btnOK_Click" Style="height: 21px" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnCancel" runat="server" Text="取消" />
            </div>

        </div>
    </form>
</body>
</html>
