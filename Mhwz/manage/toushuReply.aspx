﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="toushuReply.aspx.cs" Inherits="manage_toushuReply" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>回复</title>
    <link href="../css/main.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div style="text-align: center; width: 900px; margin: 0px auto;">
            <asp:DataList ID="DataList1" runat="server" DataKeyField="id">
                <HeaderTemplate>
                    <table width="100%" class="table05" height="25" border="0" cellpadding="0" cellspacing="1" style="text-align: center">
                        <tr>
                            <td>投诉回复</td>
                        </tr>
                    </table>
                </HeaderTemplate>
                <ItemTemplate>
                    <table width="100%" class="table05" height="146" border="0" cellpadding="0" cellspacing="1">
                        <tr>
                            <td width="51" align="center">编号：</td>
                            <td width="48" align="center"><%#Eval("id") %></td>
                            <td width="60" align="center">用户名：</td>
                            <td width="98" align="center"><%#Eval("name") %></td>
                            <td width="52" align="center">Email：</td>
                            <td width="91" align="center"><%#Eval("email") %></td>
                            <td width="41" align="center">电话：</td>
                            <td width="77" align="center"><%#Eval("tel") %></td>
                            <td width="37" align="center">时间</td>
                            <td width="54" align="center"><%#Eval("tjrq") %></td>

                        </tr>
                        <tr>
                            <td height="50" align="center">内容：</td>
                            <td colspan="9"><%#Eval("wznr") %></td>
                        </tr>
                        <tr>
                            <td height="37" align="center">回复：</td>
                            <td colspan="8"><%#Eval("reply") %></td>
                            <td><%#Eval("r_name") %></td>
                        </tr>
                    </table>
                </ItemTemplate>
            </asp:DataList>
            <table width="100%" class="table05" height="125" border="0" cellpadding="0" cellspacing="1">
                <tr>
                    <td colspan="2" align="center">回复</td>
                </tr>
                <tr>
                    <td align="center" style="width: 91px; height: 21px">回复人:</td>
                    <td width="584" style="height: 21px">
                        <asp:TextBox ID="txtboxname" runat="server">南京市国土资源局六合分局</asp:TextBox></td>
                </tr>
                <tr>
                    <td align="center" style="width: 91px">内容:</td>
                    <td>
                        <asp:TextBox ID="txtboxcontent" TextMode="multiLine" runat="server" Height="130px" Width="488px"></asp:TextBox></td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <asp:Button ID="bttj" runat="server" Text="提交" Width="46px" OnClick="bttj_Click" />
                        <asp:Button ID="btqx" runat="server" Text="取消" Width="43px" OnClick="btqx_Click" /></td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
