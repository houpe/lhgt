﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WF_Business;
using WF_DataAccess;
using System.Data.OracleClient;

public partial class manage_articleEdit : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["AdminName"] != null)
        {
            if (!IsPostBack)
            {
                string strTemp = "select QX from tb_admin where name='" + Session["AdminName"] + "'";
                DataTable dtRole = DbOperation.QueryBySql(strTemp);
                if (dtRole.Rows.Count > 0)
                {
                    if (dtRole.Rows[0]["QX"].ToString() == "1")
                    {
                        string sql1 = "select classid,c_name from tb_class ";
                        DataTable dtReturn1 = DbOperation.QueryBySql(sql1);
                        dlclass.DataSource = dtReturn1;
                    }
                    else
                    {
                        string strSql = "select 组配权限 from TB_POWERMANAGE where 用户 like '%" + Session["AdminName"] + "%'";
                        DataTable dtReturn = DbOperation.QueryBySql(strSql);
                        if (dtReturn.Rows.Count > 0)
                        {
                            string[] power = (dtReturn.Rows[0]["组配权限"].ToString()).Split(',');
                            for (int i = 0; i < power.Length; i++)
                            {
                                dlclass.Items.Add(new ListItem(power[i], power[i]));
                            }
                        }
                    }
                }


                string str_Art_Id = Request["art_id"];
                string sql2 = "select s_id,s_name from tb_sclass";

                string sql4 = string.Format("select  c_name from tb_class where classid=(select classid from tb_article where art_id='{0}')", str_Art_Id);
                string sql5 = string.Format("select  s_name from tb_sclass where s_id=(select s_id from tb_article where art_id='{0}')",str_Art_Id);
                string sql6 = string.Format("select  bszn_fl from TB_ARTICLE  where art_id='{0}'", str_Art_Id);
                //string bszlSql = "select Name from TB_ZTFL ";
                string selectedZTFL = string.Format("select ztfl from  TB_ARTICLE  where art_id='{0}' ", str_Art_Id);
                string selectedTCFL = string.Format("select tcfl from  TB_ARTICLE  where art_id='{0}' ", str_Art_Id);


                DataTable dtReturn2 = DbOperation.QueryBySql(sql2);
               // DataTable dtReturn3 = DbOperation.QueryBySql(bszlSql);
               // DrlZl.DataSource = dtReturn3;
               // DrlZl.DataBind();
                BindDropdownData("体裁分类", DrlZl);
                DataTable DrlZlTable = DbOperation.QueryBySql(sql6);
                ///加载办事指南默认选中项
                if (DrlZlTable != null)
                {
                    string SelectText = DrlZlTable.Rows[0]["bszn_fl"].ToString();
                    ListItem liTemp = DrlZl.Items.FindByText(SelectText);
                    if (liTemp != null)
                    {
                        liTemp.Selected = true;
                    }
                    else
                    {
                        ListItem item4 = new ListItem("==办事名称==", "==办事名称==");
                        DrlZl.Items.Insert(0, item4);
                    }
                }

                dlclass.DataBind();
                string strTempName = DbOperation.GetSingle(sql4);
                //加载文章分类父类默认选中项
                if (!string.IsNullOrEmpty(strTempName))
                {
                    ListItem liTemp = dlclass.Items.FindByText(strTempName);
                    if (liTemp != null)
                    {
                        liTemp.Selected = true;
                    }
                }


                dlsclass.DataSource = dtReturn2;
                dlsclass.DataBind();
                strTempName = DbOperation.GetSingle(sql5);
                if (!string.IsNullOrEmpty(strTempName))
                {
                    ListItem liTemp = dlsclass.Items.FindByText(strTempName);
                    if (liTemp != null)
                    {
                        liTemp.Selected = true;
                    }
                }

                BindDropdownData("体裁分类", ddlTcfl);
                BindDropdownData("主题分类", ddlZtfl);
                //加载默认选中的体裁分类
                strTempName = DbOperation.GetSingle(selectedTCFL);
                if (!string.IsNullOrEmpty(strTempName))
                {
                    ListItem liTemp = ddlTcfl.Items.FindByText(strTempName);
                    if (liTemp != null)
                    {
                        liTemp.Selected = true;
                    }
                }
                //加载默认选中的主题分类
                strTempName = DbOperation.GetSingle(selectedZTFL);
                if (!string.IsNullOrEmpty(strTempName))
                {
                    ListItem liTemp = ddlZtfl.Items.FindByText(strTempName);
                    if (liTemp != null)
                    {
                        liTemp.Selected = true;
                    }
                }

                string sql3 = string.Format("select art_id, title,author,WZNR,wfrom,tupian,tu,tjrq from tb_article where art_id='{0}'", str_Art_Id);
                IDataReader sdr = DbOperation.ExecuteReader(sql3);
                while (sdr.Read())
                {
                    strtitle.Text = sdr["title"].ToString();
                    strauthor.Text = sdr["author"].ToString();
                    txtContent.Value = sdr["WZNR"].ToString();
                    strfrom.Text = sdr["wfrom"].ToString();
                    tboxtu.Text = sdr["tupian"].ToString();
                    //if (sdr["tjrq"].ToString()!=null)
                    try
                    {
                        Calendar.Value = Convert.ToDateTime(sdr["tjrq"]).ToString("yyyy-MM-dd HH:mm:ss");
                    }
                    catch
                    {
                       
                    }
           
                    if (sdr["tu"].ToString() == "1")
                    {
                        CheckBox1.Checked = true;
                    }
                }
                sdr.Close();
              
            }

        }
        else
        {
            Response.Write("<script>alert('您没有访问权限!');top.location='login-admin.aspx';</script>");
        }
    }

    public void BindDropdownData(string strType, DropDownList ddlTemp)
    {
        string sql = string.Format("select * from TB_ZTFL t where pid=-1 and type='{0}'", strType);
        DataTable dtParent = DbOperation.QueryBySql(sql);
        ddlTemp.Items.Clear();
        foreach (DataRow drTemp in dtParent.Rows)
        {
            ddlTemp.Items.Add(drTemp["name"].ToString());
        }
    }


    protected void Button1_Click(object sender, EventArgs e)
    {

        if (dlclass.SelectedItem.ToString() == "==文章类型=="||string.IsNullOrEmpty(dlsclass.SelectedValue))
        {
            Response.Write("<script>alert('请检查文章类型和文章子类型是否选择!');</script>");
        }
        else if( string.IsNullOrEmpty(Calendar.Value)||string.IsNullOrEmpty(txtContent.Value))
        {
            Response.Write("<script>alert('请检查日期和文章内容是否输入!');</script>");
        }
        else if(CheckBox1.Checked==true&&string.IsNullOrEmpty(tboxtu.Text))
        {
            Response.Write("<script>alert('请选择上传附件!');</script>");
        }
        else
        {
            RunSQL();
        }
    }

    public void RunSQL()
    {

        string sql;
        string cid = string.Empty;
        string sqlClass = "select classid from tb_class where c_name='" + dlclass.SelectedItem.ToString() + "'";
        string drlBSZL = DrlZl.SelectedItem.ToString();
        DataTable dtclass = DbOperation.QueryBySql(sqlClass);
        if (drlBSZL == "==办事名称==")
        {
            drlBSZL = "";
        }
        if (dtclass.Rows.Count > 0)
        {
            cid = dtclass.Rows[0]["classid"].ToString();
        }
        if (CheckBox1.Checked == false)
        {
            sql = string.Format("update tb_article set classid='{0}',s_id='{1}',title='{2}',wznr=:content,author='{3}',wfrom='{4}',tcfl='{6}',ztfl='{7}',tu=0 ,tjrq=To_date('{8}','yyyy-mm-dd hh24:mi:ss') ,bszn_fl='{9}' where art_id='{5}'",
cid, dlsclass.SelectedValue, strtitle.Text.Trim().Replace("'", "''"), strauthor.Text.Trim().Replace("'", "''"), strfrom.Text.Trim().Replace("'", "''"), Request["art_id"], ddlTcfl.Text, ddlZtfl.Text, Calendar.Value, drlBSZL);
        }
        else
        {
            sql = string.Format("update tb_article set classid='{0}',s_id='{1}',title='{2}',wznr=:content,author='{3}',wfrom='{4}',tcfl='{6}',ztfl='{7}',tupian='{8}',tjrq=To_date('{9}','yyyy-mm-dd hh24:mi:ss'),tu=1,bszn_fl='{10}' where art_id='{5}'",
cid, dlsclass.SelectedValue, strtitle.Text.Trim().Replace("'", "''"), strauthor.Text.Trim().Replace("'", "''"), strfrom.Text.Trim().Replace("'", "''"), Request["art_id"], ddlTcfl.Text, ddlZtfl.Text, tboxtu.Text.Trim().Replace("'", "''"), Calendar.Value, drlBSZL);

        }

        IDataParameter[] idp = new OracleParameter[1];
        idp[0] = new OracleParameter(":content", OracleType.Clob);
        idp[0].Value = txtContent.Value.Trim();
        SysParams.OAConnection().RunSql(sql, ref idp);

        Response.Write("<script>alert('修改成功');</script>");
    
    
    }


 
    protected void dlclass_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (dlclass.SelectedItem.ToString() != "==文章分类==")
        {
            string strdl2 = string.Format("select s_id,s_name,classid from tb_sclass where classid=(select classid from tb_class where c_name='{0}')", dlclass.SelectedItem.ToString());
         //   string strdl2 = "select s_id,s_name,classid from tb_sclass where classid=" + dlclass.SelectedValue;
            DataTable dtReturn = DbOperation.QueryBySql(strdl2);
            dlsclass.DataSource = dtReturn;
            dlsclass.DataBind();
        }
        if (dlclass.SelectedItem.ToString() == "==文章分类==")
        {
            dlsclass.Items.Clear();
        }
    }
   

    protected void strupload_Click(object sender, EventArgs e)
    {
        if (f_pic.HasFile)
        {
            if (f_pic.PostedFile.ContentLength < 10485760)
            {
                try
                {
                    string fileExt, path;
                    fileExt = f_pic.FileName.ToLower();//文件名
                    int i = fileExt.LastIndexOf(".");
                    fileExt = fileExt.Substring(i, fileExt.Length - i);

                    //DateTime oDateTime = new DateTime();
                    //oDateTime = DateTime.Now;
                    //string oStringTime = oDateTime.ToString();
                    //oStringTime = oStringTime.Replace("-", "");
                    //oStringTime = oStringTime.Replace(" ", "");
                    //oStringTime = oStringTime.Replace(":", "");

                    string strFileName = Guid.NewGuid().ToString();
                    path = "~/pic/upload/" + strFileName + fileExt;
                    this.f_pic.PostedFile.SaveAs(HttpContext.Current.Server.MapPath(path));
                    Response.Write("<script>alert('上传成功！');</script>");
                    tboxtu.Text = "pic/upload/" + strFileName + fileExt;
                }
                catch
                {
                    Response.Write("<script>alert('上传失败！');</script>");

                }
            }
            else
            {
                Response.Write("<script>alert('上传文件不能大于10MB！');</script>");

            }
        }
        else
        {
            Response.Write("<script>alert('请选择文件!');</script>");

        }
    }
}
