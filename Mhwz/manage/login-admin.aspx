<%@ Page Language="C#" AutoEventWireup="true" CodeFile="login-admin.aspx.cs" Inherits="manage_login_admin" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>电子政务系统登录</title>
    <style type="text/css">
        td {
            font-size: 12px;
            color: #007AB5;
        }

        input {
            border: 0px;
            height: 26px;
            color: #007AB5;
        }

        .login {
            background: url(../images/login/login_5.gif) no-repeat;
        }
    </style>

    <script src="../js/jquery.js" type="text/javascript"></script>

    <script type="text/javascript">
        function EnterKeyClick() {
            if (event.keyCode == 13) {
                event.keyCode = 9;
                event.returnValue = false;
                var btnLogin = document.getElementById('btnLogin');
                btnLogin.click();
            }
        }

        function AntiSqlValid(strValue) {
            re = /select|update|delete|exec|count|or|and|'|"|=|;|>|<|%/i;
            //alert(re.test(strValue));
            if (re.test(strValue)) {
                //alert("请您不要在参数中输入特殊字符和SQL关键字！"); //注意中文乱码
                return false;
            }
            return true;
        }

        //密码验证回调
        function validate() {
            $("#ImageButton1").bind("click", function () {
                var context = "";
                var user = $('#txtUser').val();
                var password = $('#txtpwd').val();
                var checkcode = $('#txtCheckCode').val();

                //防sql注入
                if (!AntiSqlValid(user) || !AntiSqlValid(password)) {
                    alert("请勿输入非法字符");
                    return;
                }

            })
        }
    </script>
</head>
<body bgcolor="#9CDCF9" onkeydown="EnterKeyClick()" onload="validate()">
    <form id="form1" runat="server">
        <table width="681" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-top: 200px">
            <tr>
                <td width="353" height="259" valign="bottom" background="../images/login/login_1.gif">
                    <table width="90%" border="0" cellspacing="3" cellpadding="0">
                        <tr>
                            <td align="right" valign="bottom" style="color: #05B8E4"></td>
                        </tr>
                    </table>
                </td>
                <td width="195" background="../images/login/login_2.gif">
                    <table width="100%" height="106" border="0" align="center" cellpadding="2" cellspacing="0">
                        <tr>
                            <td style="height: 30px" colspan="2" align="left">&nbsp;
                            </td>
                        </tr>
                        <tr style="height: 30px">

                            <td style="font-size: 13px;">用户名:</td>
                            <td>
                                <asp:TextBox ID="txtUser" runat="server" Height="25px" Style="border-right: #000000 1px solid; border-top: #000000 1px solid; border-left: #000000 1px solid; border-bottom: #000000 1px solid" Width="100px"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtUser" runat="server" ErrorMessage="*" Display="dynamic"></asp:RequiredFieldValidator></td>
                        </tr>
                        <tr style="height: 25px">

                            <td style="font-size: 13px;">口　令:</td>
                            <td>

                                <asp:TextBox ID="txtpwd" TextMode="password" Width="100" runat="server" Height="25px" Style="border-right: #000000 1px solid; border-top: #000000 1px solid; border-left: #000000 1px solid; border-bottom: #000000 1px solid"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtpwd" runat="server" ErrorMessage="*" Display="dynamic"></asp:RequiredFieldValidator></td>
                        </tr>

                        <tr>
                            <td>验证码:</td>
                            <td style="vertical-align: middle">
                                <asp:TextBox ID="txtCheckCode" Width="80" runat="server" Height="25px" Style="border-right: #000000 1px solid; border-top: #000000 1px solid; border-left: #000000 1px solid; border-bottom: #000000 1px solid"></asp:TextBox>
                                <img alt="img" style="height: 22px; vertical-align: middle" src="ValidateNum.aspx" /></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:Button ID="btnLogin" CssClass="login" Width="74" runat="server" Text="登录" OnClick="btnLogin_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
                <td width="133" background="../images/login/login_3.gif">&nbsp;
                </td>
            </tr>
            <tr>
                <td height="161" colspan="3" background="../images/login/login_4.gif"></td>
            </tr>
        </table>

    </form>
</body>
</html>
