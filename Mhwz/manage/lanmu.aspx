﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="lanmu.aspx.cs" Inherits="manage_lanmu" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>栏目管理</title>
    <style type="text/css">
        td {
            font-size: 14px;
            background-color: #FFFFFF;
        }

        table {
            background-color: #87CEFA;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div align="center">
            <table border="0" cellpadding="0" cellspacing="1" style="width: 610px; height: 276px">
                <tr>
                    <td colspan="3" style="height: 35px">一级栏目管理:</td>
                </tr>
                <tr>
                    <td colspan="3" style="height: 29px">栏目类别:<asp:DropDownList ID="lmlb1" runat="server" DataTextField="c_name" DataValueField="classid">
                    </asp:DropDownList>
                        <asp:Button ID="Button1" runat="server" Text="删除" Width="50px" Height="23px" OnClick="Button1_Click" /></td>
                </tr>
                <tr>
                    <td colspan="3" style="height: 29px"></td>
                </tr>
                <tr>
                    <td colspan="3" style="height: 29px">新名字:<asp:TextBox ID="lm_txtname" runat="server"></asp:TextBox>
                        <asp:Button ID="Button2" runat="server" Text="更名" Width="50px" Height="23px" OnClick="Button2_Click" /></td>
                </tr>
                <tr>
                    <td colspan="3" style="height: 29px"></td>
                </tr>
                <tr>
                    <td colspan="3" style="height: 29px">新类别:<asp:TextBox ID="lm_strname" runat="server"></asp:TextBox>
                        <asp:Button ID="Button3" runat="server" Text="新增" Width="50px" Height="23px" OnClick="Button3_Click" /></td>
                </tr>
                <tr>
                    <td colspan="3" style="height: 55px"></td>
                </tr>
                <tr>
                    <td colspan="3" style="height: 24px">子栏目管理:</td>
                </tr>
                <tr>
                    <td colspan="3" style="height: 36px">栏目名称:<asp:DropDownList ID="zlm" runat="server" DataTextField="s_name" DataValueField="s_id">
                    </asp:DropDownList>
                        <asp:Button ID="Button4" runat="server" Text="删除" Width="50px" OnClick="Button4_Click" Height="23px" /></td>
                </tr>
                <tr>
                    <td colspan="3" style="height: 19px"></td>
                </tr>
                <tr>
                    <td colspan="3" style="height: 27px">新名称:<asp:TextBox ID="zlm_txtname" runat="server"></asp:TextBox>
                        <asp:Button ID="Button5" runat="server" Text="更名" Width="50px" OnClick="Button5_Click" Height="23px" /></td>
                </tr>
                <tr>
                    <td colspan="3" style="height: 27px"></td>
                </tr>
                <tr>
                    <td colspan="3" style="height: 43px">子栏目:<asp:TextBox ID="zlm_add" runat="server"></asp:TextBox>&nbsp; 在一级栏目<asp:DropDownList
                        ID="lmlb2" runat="server" DataTextField="c_name" DataValueField="classid">
                    </asp:DropDownList>中<asp:Button ID="Button6" runat="server" Height="23px" Text="新增"
                        Width="50px" OnClick="Button6_Click" /></td>
                </tr>
            </table>

        </div>
    </form>
</body>
</html>
