﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;

public partial class manage_articleList : System.Web.UI.Page
{
  protected  string strSelect1 = string.Empty;
  protected string strSearch = string.Empty;
  protected string strClass = string.Empty;
  protected string strSclass = string.Empty;
  protected PagedDataSource pds = new PagedDataSource();
  protected void Page_Load(object sender, EventArgs e)
  {
      Session["AdminName"] = "admin";
      if (Session["AdminName"] != null)
      {
          if (!IsPostBack)
          {
              if (Request["type"] == "1" && !string.IsNullOrEmpty(Request["art_id"]))
              {
                  string strId = Request["art_id"];
                  string strSetSql = string.Format("delete from tb_article where art_id='{0}'", strId);
                  string strSql = string.Format(" insert into tb_articlesubmit(classid,s_id,title,wznr,author,wfrom,tupian,tu,tcfl,ztfl,bszn_fl,tjrq,art_id) (select classid,s_id,title,wznr,author,wfrom,tupian,tu,tcfl,ztfl,bszn_fl,tjrq ,art_id from tb_article where art_id='{0}')", strId);
                  ArrayList myAL = new ArrayList();
                  myAL.Add(strSql);
                  myAL.Add(strSetSql);
                  DbOperation.ExecuteSqlTran(myAL);
              }

              InitData();
              BindDataList(0);
          }
      }
      else
      {
          Response.Write("<script>alert('您没有访问权限!');top.location='login-admin.aspx';</script>");
      }
  }

    private void InitData()
    {
        string strSql = "";

        string strTemp = string.Format("select QX from tb_admin where name='{0}'",Session["AdminName"]);
        DataTable dtRole = DbOperation.QueryBySql(strTemp);
        if (dtRole.Rows.Count > 0)
        {
            if (dtRole.Rows[0]["QX"].ToString() == "1")
            {
                strSql = "select tb_article.art_id,tb_article.title,to_char(tb_article.tjrq,'yyyy-mm-dd') as tjrq,tb_article.tjrq as tt,tb_class.classid,tb_class.c_name,tb_sclass.s_id,tb_sclass.s_name  from tb_article,tb_class,tb_sclass where tb_class.classid=tb_article.classid  and tb_sclass.s_id=tb_article.s_id order by tt desc";
                string strdl1 = "select classid,c_name from tb_class order by tjrq asc ";
                DataTable dtReturn = DbOperation.QueryBySql(strdl1);
                if (dtReturn.Rows.Count > 0)
                {
                    dlclass.DataSource = dtReturn;
                    dlclass.DataBind();
                }
            }
            else
            {
                string sqlCmd = "select 组配权限 from TB_POWERMANAGE where 用户 like '%" + Session["AdminName"] + "%'";
                string tempcmd = " select tb_article.art_id,tb_article.title,tb_class.classid,tb_class.c_name,tb_sclass.s_id,tb_sclass.s_name  from tb_article,tb_class,tb_sclass where tb_class.classid=tb_article.classid  and tb_sclass.s_id=tb_article.s_id ";
                string tempEndCmd = "";
                DataTable dtReturn = DbOperation.QueryBySql(sqlCmd);
                if (dtReturn.Rows.Count > 0)
                {
                    int tmp = 0;
                    string[] power = (dtReturn.Rows[0]["组配权限"].ToString()).Split(',');
                    for (int i = 0; i < power.Length; i++)
                    {
                        dlclass.Items.Add(new ListItem(power[i], power[i]));
                        if (tmp == 0)
                        {
                            tempEndCmd += "'" + power[i] + "'";
                            tmp++;
                        }
                        else
                        {
                            tempEndCmd += " , '" + power[i] + "'";
                        }
                    }
                    tempcmd += "and tb_class.c_name in (" + tempEndCmd + ")";
                }
                strSql = tempcmd + " order by tb_article.tjrq asc";
            }
        }


        //定义一条SQL语句

        DataTable dtSource = DbOperation.QueryBySql(strSql);
        ViewState["dtSource"] = dtSource;

        ListItem item = new ListItem("==文章类型==", "==文章类型==");
        dlclass.Items.Insert(0, item);
        ListItem item2 = new ListItem("==选择分类==", "==选择分类==");
        dlsclass.Items.Insert(0, item2);
    }

    private void BindDataList(int currentpage)
    {
        DataTable dtSource = ViewState["dtSource"] as DataTable;
        if (dtSource.Rows.Count>0)
        {   pds.AllowPaging = true;//允许分页
            pds.PageSize = 14;//每页显示9条数据    
            pds.DataSource = dtSource.DefaultView;//把数据集中的数据放入分页数据源中
            pds.CurrentPageIndex = currentpage;//当前页为传入的一个int型值
            dlart.DataSource = pds;//绑定Datalist
            dlart.DataKeyField = "art_id";
            ViewState["pageCount"] = pds.PageCount;
            dlart.DataBind();
        }
    }

    protected void dlart_ItemCommand(object source, DataListCommandEventArgs e)
    {
       
        switch (e.CommandName)
        {
            //以下四个为 捕获用户点击 上一页 下一页等时发生的事件
            case "first":
                ViewState["currPageIndex"] = "0";
                pds.CurrentPageIndex = Convert.ToInt32(ViewState["currPageIndex"]);
                BindDataList(pds.CurrentPageIndex);
                break;
            case "pre":
                ViewState["currPageIndex"] = Convert.ToInt32(ViewState["currPageIndex"]) - 1;
                pds.CurrentPageIndex = Convert.ToInt32(ViewState["currPageIndex"]);
                BindDataList(pds.CurrentPageIndex);
                break;
            case "next":
                ViewState["currPageIndex"] = Convert.ToInt32(ViewState["currPageIndex"]) + 1;
                pds.CurrentPageIndex = Convert.ToInt32(ViewState["currPageIndex"]);
                BindDataList(pds.CurrentPageIndex);
                break;
            case "last":
                int tempPageCount = Convert.ToInt32(ViewState["pageCount"]);
                ViewState["currPageIndex"] = tempPageCount - 1;
                 BindDataList(tempPageCount-1);
                   break;
            case "delete":
                //删除用户选中的单条信息
                string id = (dlart.DataKeys[e.Item.ItemIndex]).ToString();
                DbOperation.ExecuteSql("delete from tb_article WHERE art_id='"+id+"'");
                Response.Write("<script>alert('删除成功!')</script>");
                //InitData();
                keepSearch();
                break;
            case "pldelete":
               
                DataListItemCollection dlic = dlart.Items;
                //执行一个循环,删除所有用户选中的信息
                int resultLineNum = 0;
                for (int i = 0; i < dlic.Count; i++)
                {
                    if ((dlic[i].ItemType == ListItemType.AlternatingItem) || (dlic[i].ItemType == ListItemType.Item))
                    {
                        CheckBox CBox = dlic[i].FindControl("cbox") as CheckBox;
                        if (CBox != null)
                        {
                            if (CBox.Checked)
                            {
                                string deleteid = dlart.DataKeys[dlic[i].ItemIndex].ToString();
                                resultLineNum = DbOperation.ExecuteSql("DELETE FROM tb_article WHERE art_id='"+ deleteid + "'");
                           
                            }
                        }
                    }
                }
                if (resultLineNum > 0)
                {
                    Response.Write("<script>alert('删除成功!')</script>");
                    keepSearch();

                }
                //InitData();
                //BindDataList(0);
                break;

        }
    }

    protected void dlart_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Footer)
        {
            //以下六个为得到脚模板中的控件,并创建变量.
            Label CurrentPage = e.Item.FindControl("labCurrentPage") as Label;
            Label PageCount = e.Item.FindControl("labPageCount") as Label;
            LinkButton FirstPage = e.Item.FindControl("LinkButton1") as LinkButton;
            LinkButton PrePage = e.Item.FindControl("LinkButton2") as LinkButton;
            LinkButton NextPage = e.Item.FindControl("LinkButton3") as LinkButton;
            LinkButton LastPage = e.Item.FindControl("LinkButton4") as LinkButton;
            CurrentPage.Text = (pds.CurrentPageIndex + 1).ToString();//绑定显示当前页

            PageCount.Text = pds.PageCount.ToString();//绑定显示总页数
            PageCount.Text += string.Format("页(当前页共{0}条)", pds.Count);

            if (pds.IsFirstPage)//如果是第一页,首页和上一页不能用
            {
                FirstPage.Enabled = false;
                PrePage.Enabled = false;
            }
            if (pds.IsLastPage)//如果是最后一页"下一页"和"尾页"按钮不能用
            {
                NextPage.Enabled = false;
                LastPage.Enabled = false;
            }
        }
    }
    protected void bt_search_Click(object sender, EventArgs e)
    {
        strSelect1 = select1.SelectedItem.ToString();
        strClass = dlclass.SelectedItem.ToString();
        strSclass=dlsclass.SelectedItem.ToString();
        bindData();
        
    }

    protected void bindData() 
    {
        string sql = string.Empty;
        string tbox = tb_search.Text.Trim().Replace("'", "''");

        sql = @"select a.art_id,a.title,to_char(a.tjrq,'yyyy-mm-dd') as tjrq,a.tjrq as tt ,b.classid,b.c_name,c.s_id,c.s_name from tb_article a, tb_class b, tb_sclass c where b.classid = a.classid and c.s_id = a.s_id";

        if (!string.IsNullOrEmpty(tbox))
        {
            if (select1.SelectedItem.ToString() == "标题")
            {
                sql += " and title like '%" + tbox + "%'";//定义一条SQL语句
            }
            else if (select1.SelectedValue == "strcontent")
            {
                sql += " and WZNR like '%" + tbox + "%'";//定义一条SQL语句
            }
        }
        if (dlclass.SelectedItem.ToString() != "==文章类型==")
        {
            sql += " and a.s_id='" + dlsclass.SelectedValue + "'";//定义一条SQL语句
        }

        if (!string.IsNullOrEmpty(cdStart.Value))
        {
            sql += string.Format(" and a.tjrq>=to_date('{0}','yyyy-mm-dd')", cdStart.Value);//定义一条SQL语句
        }
        if (!string.IsNullOrEmpty(cdEnd.Value))
        {
            sql += string.Format(" and a.tjrq<=to_date('{0}','yyyy-mm-dd')", cdEnd.Value);//定义一条SQL语句
        }
        sql += " order by tt desc";


        DataTable dtSource = DbOperation.QueryBySql(sql);
        pds.DataSource = dtSource.DefaultView;//把数据集中的数据放入分页数据源中
        dlart.DataSource = pds;//绑定Datalist
        dlart.DataBind();
    }


    protected void dlclass_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (dlclass.SelectedItem.ToString() != "==文章类型==")
        {
            string strdl2 = string.Format("select s_id,s_name,classid from tb_sclass where classid=(select classid from tb_class where c_name='{0}')", dlclass.SelectedItem.ToString());
            DataTable dtReturn = DbOperation.QueryBySql(strdl2);
            dlsclass.DataSource = dtReturn;
            dlsclass.DataBind();
        }
    }


    /// <summary>
    /// 保留搜索前的数据条件数据和更新操作后的数据
    /// </summary>
    private void keepSearch()
    {
        if (strSelect1 != null || strClass != null || strSclass != null)
        {
            ListItem item5 = select1.Items.FindByText(strSelect1);
            if (item5 != null)
            {
                item5.Selected = true;
            }
            ListItem item6 = dlclass.Items.FindByText(strClass);
            if (item6 != null)
            {
                item6.Selected = true;
            }
            ListItem item7 = dlsclass.Items.FindByText(strSclass);
            if (item7 != null)
            {
                item7.Selected = true;
            }
            tb_search.Text = strSearch;
            bindData();
        }

    }

}
