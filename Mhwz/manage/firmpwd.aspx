﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="firmpwd.aspx.cs" Inherits="manage_firmpwd" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>请输入原始密码</title>
    <link href="../css/main.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div align="center" style="margin-top:25px;">
            <asp:Panel ID="Panel2" runat="server" Height="65px" Width="300px">
                <table class="table03" width="300">
                    <tr>
                        <td>请输入原始密码：</td>
                        <td style="width: 148px">
                            <asp:TextBox ID="tbpwd" TextMode="password" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="height: 39px; text-align:center">
                            <asp:Button ID="Button1" runat="server" Text="确定 " Height="29px" Width="61px" OnClick="Button1_Click" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel ID="Panel1" runat="server" Height="200px" Width="300px" valign="middle" Visible="False">
                <table  class="table03" style="width: 300px; height: 125px">
                    <tr>
                        <td align="left" colspan="2">请输入要修改的用户名和密码</td>
                    </tr>
                    <tr>
                        <td style="width: 56px; height: 10px">用户名:</td>
                        <td align="left" style="width: 100px; height: 10px">
                            <asp:TextBox ID="tb_name" runat="server" Width="137px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 56px; height: 13px">密 码:</td>
                        <td align="left" style="width: 100px; height: 13px">
                            <asp:TextBox ID="tb_newpwd" TextMode="password" runat="server" Width="137px"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="height: 34px">
                            <asp:Button ID="Button2" runat="server" Height="28px" Text="确定 " Width="67px" OnClick="Button2_Click" /></td>
                    </tr>
                </table>
            </asp:Panel>

        </div>
    </form>
</body>
</html>
