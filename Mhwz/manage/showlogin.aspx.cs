﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class manage_showlogin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["AdminName"] != null)
        {
            strname.Text = Session["AdminName"].ToString();
            strsf.Text = "后台管理员";
           
            strtime.Text = System.DateTime.Now.ToString();
            string result = String.Empty;
            if (Context.Request.ServerVariables["HTTP_VIA"] != null)
            {//获取代理
                result = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            }
            else
            {//获取真实
                if (string.IsNullOrEmpty(result))
                {
                    result = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                }
                if (string.IsNullOrEmpty(result))
                {
                    result = HttpContext.Current.Request.UserHostAddress;
                }
                if (string.IsNullOrEmpty(result))
                {
                    result = "127.0.0.1";
                }
            }
            this.strIP.Text = result;
        }
    }
}
