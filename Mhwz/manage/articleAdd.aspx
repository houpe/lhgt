﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="articleAdd.aspx.cs" Inherits="manage_articleAdd" ValidateRequest="false" %>

<%@ Register Src="~/UserControl/Calendar.ascx" TagPrefix="uc1" TagName="Calendar" %>



<%--<%@ Register TagPrefix="ftb" Namespace="FreeTextBoxControls" Assembly="FreeTextBox" %>--%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
 <title> 添加文章</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
   <%-- 发布到服务器后需要将ckeditor的引入路径改为./开头--%>
    <script src="../ckeditor/ckeditor.js" type="text/javascript"></script>
    <script src="../ckeditor/config.js" type="text/javascript"></script>
    <script src="../js/jquery.js"></script>
    <script src="../js/Regex.js"></script>
    <style type="text/css">
        table {
            font-size: 13px;
        }
        #txtContent {
            width: 516px;
            height: 65px;
        }
        .auto-style1 {
            height: 16px;
        }
   </style>
    <script type="text/javascript">
       
        window.onload = function() {
            CKEDITOR.replace('txtContent')
           
           // CKEDITOR.replace('txtContent', { language: 'zh-cn', skin: 'kama' });
        }


        function checkType() {
            var fileName = document.getElementById("f_pic").value;
            var seat = fileName.lastIndexOf(".");
            var extension = fileName.substring(seat).toLowerCase();
            var allowed = [".jpg", ".gif", ".png", ".bmp", ".jpeg", ".doc", ".xls"];
            for (var i = 0; i < allowed.length; i++) {
                if (!(allowed[i] != extension)) {
                    return true;
                }
            }
            alert("不支持" + extension + "格式");
            return false;
        }
    </script>
</head>
<body >
    <form id="form1" runat="server" enctype="multipart/form-data">
        <div align="center">
            <table border="1" cellpadding="0" cellspacing="1" style="width: 100%; height: 75%; background-color: #FFFFFF; border: solid 1px  #d9d9d9;" id="txtTable">
                <tr>
                    <td style="width: 15%" align="center">文章类型（组配分类）：</td>
                    <td>
                        <%--<asp:DropDownList ID="dlclass" runat="server" DataTextField="c_name" DataValueField="classid" AutoPostBack="true" OnSelectedIndexChanged="dlclass_SelectedIndexChanged">--%>
                            <asp:DropDownList ID="dlclass" runat="server" DataTextField="c_name" DataValueField="classid" AutoPostBack="true" OnSelectedIndexChanged="dlclass_SelectedIndexChanged">
                        </asp:DropDownList>
                        <asp:DropDownList ID="dlsclass" runat="server" DataTextField="s_name" DataValueField="s_id">
                        </asp:DropDownList>

                    </td>
                    <td>办事指南</td>
                    <td>
                        <asp:DropDownList ID="dlbsznlist" runat="server"></asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>体裁分类：</td>
                    <td>
                        <asp:DropDownList ID="ddlTcfl" runat="server" DataTextField="c_name" DataValueField="classid" AutoPostBack="true" >
                        </asp:DropDownList>
                    <td style="width: 10%">主题分类：</td>
                    <td>
                        <asp:DropDownList ID="ddlZtfl" runat="server" DataTextField="c_name" DataValueField="classid" AutoPostBack="true" OnSelectedIndexChanged="dlclass_SelectedIndexChanged">
                        </asp:DropDownList>
                </tr>
                <tr>
                    <td>文章标题：</td>
                    <td colspan="3">
                        <asp:TextBox ID="strtitle" runat="server" Width="510px"></asp:TextBox><asp:RequiredFieldValidator
                            ID="RequiredFieldValidator1" runat="server" ErrorMessage="请输入标题" Display="dynamic" ControlToValidate="strtitle"></asp:RequiredFieldValidator></td>
                </tr>
                <tr>
                    <td>文章内容：</td>
                    <td colspan="3">
                        <textarea rows="30" cols="50" name="txtContent" id="txtContent" runat="server" class="ckeditor"></textarea>
                    
                    </td>
                </tr>
                <tr> <td>发布日期:</td><td colspan="3" id="valDate">
                 <uc1:Calendar runat="server" ID="Calendar" Width="153" TimeUsing="true"  ValidatorTime="true"  />
                                   </td>

                </tr>
                <tr>
                    <%--<td class="auto-style1"><td>发布日期:</td><tr>--%>
                    <td >文章作者：</td>
                    <td colspan="3">
                        <asp:TextBox ID="strauthor" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>文章来源：</td>
                    <td colspan="3">
                        <asp:TextBox ID="strfrom" runat="server" Text="南京市国土资源局六合分局"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>首页图片：</td>
                    <td colspan="3">
                        <asp:CheckBox ID="CheckBox1" runat="server" Text="是否在首页显示图片" />
                    </td>
                </tr>
                <tr>
                    <td>上传图片：</td>
                    <td colspan="3">
                        <asp:FileUpload ID="f_pic" runat="server" />
                        <asp:Button ID="strupload" runat="server" Text="上传" OnClick="strupload_Click" CausesValidation="false" OnClientClick="return checkType()" />
                    </td>
                </tr>
                 <tr>
                    <td>图片地址：
                    </td>
                    <td colspan="3">
                        <asp:TextBox ID="tboxtu" runat="server" ReadOnly="true"></asp:TextBox></td>
                </tr>
                <tr>
                    <td align="center" colspan="4">
                        <asp:Button ID="Button1" runat="server" Text="提交" Width="47px" OnClick="Button1_Click" />
                        <asp:Button ID="Button2" runat="server" Text="清空" Width="47px" OnClick="Button2_Click" /></td>
                </tr>
            </table>

        </div>
    </form>
</body>
</html>
