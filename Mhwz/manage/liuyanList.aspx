﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="liuyanList.aspx.cs" Inherits="manage_liuyanList" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>领导信箱</title>
    <link href="../css/main.css" rel="stylesheet" />
    <style type="text/css">
        #headFont a {
            color: #808080;
            text-decoration: none;
            font-weight: bolder;
            font-size: 20px;
        }

        #headFont {
            margin-bottom: 20px;
             margin-top:20px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div style="text-align: center;">
            <div style="text-align: center; width: 100%;" id="headFont">
                <a>领导信箱</a>
            </div>
            <asp:DataList ID="DataList1" runat="server" DataKeyField="id" OnItemCommand="DataList1_ItemCommand" OnItemDataBound="DataList1_ItemDataBound" HorizontalAlign="Center" Width="900px">
                <ItemTemplate>
                    <table width="100%" class="table05" border="0" cellpadding="0" cellspacing="1">
                        <tr>
                            <td width="60" align="center">标题：</td>
                            <td width="100" align="center" colspan="12"><%#Eval("title") %></td>

                        </tr>
                        <tr>
                            <td width="50" height="23" align="center">编号：</td>
                            <td width="25" align="center"><%#Eval("id") %></td>

                            <td width="60" align="center">姓名：</td>
                            <td width="100" align="center"><%#Eval("name") %></td>
                            <td width="45" align="center">Email：</td>
                            <td width="90" align="center"><%#Eval("email") %></td>
                            <td width="40" align="center">电话：</td>
                            <td width="80" align="center"><%#Eval("tel") %></td>
                            <td width="45" align="center">时间:</td>
                            <td width="150" align="center"><%#Eval("tjrq") %></td>
                            <td width="45" align="center">
                                <asp:LinkButton ID="dldelete" runat="server" CommandName="delete" ForeColor="Red">删除</asp:LinkButton></td>
                            
                            <td width="62" align="center" ><a href='<%#ConfigurationManager.AppSettings["webPageUrl"]%>?iid=<%#Eval("iid")%>&readonly=false&UserId=-1&FormName=<%#ConfigurationManager.AppSettings["lingDaoReplyAdress"]%>&input_index=0' target="_self" style="color:green">回复</a></td>

                            <td width="62" align="center" ><a href='<%#ConfigurationManager.AppSettings["webPageUrl"]%>?iid=<%#Eval("iid")%>&readonly=false&UserId=-1&FormName=<%#ConfigurationManager.AppSettings["lingDaoReplyAdress"]%>&act=Print' target="_blank" style="color:green">打印</a></td>
                        </tr>
                        <tr>
                            <td height="50" align="center">内容：</td>
                            <td colspan="12"><%#Eval("wznr") %></td>
                        </tr>
                        <tr>
                            <td height="37" align="center">回复：</td>
                            <td colspan="11"><%#Eval("reply") %></td>
                            <td align="center" width="70"><%#Eval("r_name") %></td>
                        </tr>
                    </table>
                </ItemTemplate>
                <FooterTemplate>
                    <div align="center">
                        <table class="table05" border="0" cellpadding="0" cellspacing="1" style="border-style: none">
                            <tr>
                                <td>当前页:<asp:Label ID="labCurrentPage" runat="server"></asp:Label>/
                <asp:Label ID="labPageCount" runat="server"></asp:Label>
                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="first" Font-Underline="False" ForeColor="Black">首页</asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton2" runat="server" CommandName="pre" Font-Underline="False" ForeColor="Black">上一页</asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton3" runat="server" CommandName="next" Font-Underline="False" ForeColor="Black">下一页</asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton4" runat="server" CommandName="last" Font-Underline="False" ForeColor="Black">尾页</asp:LinkButton></td>
                            </tr>
                        </table>
                    </div>
                </FooterTemplate>
            </asp:DataList>
        </div>
    </form>
</body>
</html>
