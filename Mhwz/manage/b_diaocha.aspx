﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="b_diaocha.aspx.cs" Inherits="manage_bdiaocha" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>调查</title>
    <link href="../css/main.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div style="text-align: center; width: 550px; margin: 0px auto; margin-top:25px;">

            <table border="0" class="table05" cellpadding="0" cellspacing="1" style="width: 550px; height: 157px">
                <tr>
                    <td align="center" colspan="2" style="font-size: 16px; background-color: #87CEFA">网上调查</td>
                </tr>
                <tr>
                    <td width="50">问 题:</td>
                    <td>
                        <asp:TextBox ID="wenti" runat="server" Width="195px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" Display="dynamic" ControlToValidate="wenti"></asp:RequiredFieldValidator></td>
                </tr>
                <tr>
                    <td>答案A:</td>
                    <td>
                        <asp:TextBox ID="tba" runat="server" Width="195px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" Display="dynamic" ControlToValidate="tba"></asp:RequiredFieldValidator></td>
                </tr>
                <tr>
                    <td>答案B:</td>
                    <td>
                        <asp:TextBox ID="tbb" runat="server" Width="195px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*" Display="dynamic" ControlToValidate="tbb"></asp:RequiredFieldValidator></td>
                </tr>
                <tr>
                    <td>答案C:</td>
                    <td>
                        <asp:TextBox ID="tbc" runat="server" Width="195px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*" Display="dynamic" ControlToValidate="tbc"></asp:RequiredFieldValidator></td>
                </tr>
                <tr>
                    <td>答案D:</td>
                    <td>
                        <asp:TextBox ID="tbd" runat="server" Width="195px"></asp:TextBox></td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <asp:Button ID="bt1" runat="server" Text="创建" OnClick="bt1_Click" Width="51px" />&nbsp;<asp:Button ID="bt2"
                            runat="server" Text="取消" Width="43px" OnClick="bt2_Click" /></td>
                </tr>
            </table>
            <br />
            <asp:GridView ID="diaocha" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="id" PageSize="5" PagerStyle-Font-Size="13px" OnPageIndexChanging="diaocha_PageIndexChanging" Width="550px" OnRowDeleting="diaocha_RowDeleting" OnRowEditing="diaocha_RowEditing">
                <Columns>
                    <asp:BoundField DataField="id" HeaderText="ID编号" ItemStyle-Width="50px" />
                    <asp:BoundField DataField="question" HeaderText="问题" ItemStyle-Width="400px" />
                    <asp:ButtonField Text="&lt;span onclick=&quot;JavaScript:return confirm('确定删除吗？')&quot;&gt;删除&lt;/span&gt;" CommandName="delete" HeaderText="删除" ItemStyle-Width="50px" />
                </Columns>
                <PagerSettings Mode="NextPreviousFirstLast" FirstPageText="首页" PreviousPageText="上一页" NextPageText="下一页" LastPageText="尾页" />
                <HeaderStyle HorizontalAlign="Center" Font-Size="13px" />
                <RowStyle Font-Size="13px" />
                <FooterStyle HorizontalAlign="Center" Font-Size="10px" />
                <PagerStyle HorizontalAlign="Center" Font-Size="10pt" />
                <SelectedRowStyle BackColor="#FFC080" />
            </asp:GridView>

        </div>
    </form>
</body>
</html>
