﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;

public partial class manage_login_admin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //Response.Write(jiami.Decrypting("On8U4+dy1Rs="));
            this.txtUser.Focus();

        }
    }
    protected void btnLogin_Click(object sender, EventArgs e)
    {
        string validate = "";
        string chekcode = "-1";
        try
        {
            validate = Session["ValidateNum"].ToString();
            chekcode = txtCheckCode.Text.Trim().ToUpper();
        }
        catch
        {

        }
        if (validate != chekcode)    //判断验证码
        {
            // Response.Write("<script>alert('验证码错误!');windows.location=windows.location </script>");
            Response.Write("<script>alert('验证码错误!');window.location.href=window.location.href </script>");
            txtUser.Text = "";
            txtpwd.Text = "";
            txtCheckCode.Text = "";
        }
        else
        {
            DataTable dtUsers = GetAdminInfo();
            if (dtUsers.Rows.Count > 0)
            {
                Session.Add("AdminName", txtUser.Text.ToString().Trim());  //将用户名称存储在Session中,用于判断非法用户进行后台。
                Response.Write("<script>location='mainPage.aspx';</script>");

            }
            else
            {
                Response.Write("<script>alert('用户名称或密码不正确或权限不对！');windows.location=windows.location</script>");
                txtUser.Text = "";
                txtpwd.Text = "";
                txtCheckCode.Text = "";
            }
        }
    }

    public DataTable GetAdminInfo()
    {
        string str = string.Format("select name,pwd,qx from tb_admin where name='{0}' and pwd='{1}'",txtUser.Text.ToString().Trim(),jiami.Encrypting(txtpwd.Text.ToString().Trim()));
        return DbOperation.QueryBySql(str);

    }
}
