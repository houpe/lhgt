﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
public partial class manage_bdiaocha : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["AdminName"] != null)
        {
            diaocha.DataSource = BusinessOperation.mydc();
            diaocha.DataBind();
        }
        else
        {

            Response.Write("<script>alert('您没有访问权限!');top.location='login-admin.aspx';</script>");

        }
    }
    protected void bt1_Click(object sender, EventArgs e)
    {
        string sql = "insert into  tb_myddc (question,da_a,da_b,da_c,da_d)values('" + wenti.Text.Trim() + "','" + tba.Text.Trim() + "','" + tbb.Text.Trim() + "','" + tbc.Text.Trim() + "','" + tbd.Text.Trim() + "')";
        DbOperation.ExecuteSql(sql);
        Response.Write("<script>alert('创建成功');</script>");

        this.wenti.Text = "";
        this.tba.Text = "";
        this.tbb.Text = "";
        this.tbc.Text = "";
        this.tbd.Text = "";
        
    }
    protected void bt2_Click(object sender, EventArgs e)
    {
        this.wenti.Text = "";
        this.tba.Text = "";
        this.tbb.Text = "";
        this.tbc.Text = "";
        this.tbd.Text = "";
    }

 
    protected void diaocha_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        diaocha.PageIndex = e.NewPageIndex;
        diaocha.DataSource = BusinessOperation.mydc();
        diaocha.DataBind();
   
    }
   protected void diaocha_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        BusinessOperation.deletedc(diaocha.DataKeys[e.RowIndex].Value.ToString());
        diaocha.DataSource = BusinessOperation.mydc();
        diaocha.DataBind();
    }

   protected void diaocha_RowEditing(object sender, GridViewEditEventArgs e)
   {
       diaocha.EditIndex = e.NewEditIndex;
   }
}
