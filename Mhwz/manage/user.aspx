﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="user.aspx.cs" Inherits="manage_user" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>用户修改删除</title>
    <link href="../css/main.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div align="center">
            <asp:GridView ID="look_user" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="id" PageSize="5" PagerStyle-Font-Size="13px" OnPageIndexChanging="look_user_PageIndexChanging" Width="90%" OnRowDeleting="look_user_RowDeleting">
                <Columns>
                    <asp:BoundField DataField="id" HeaderText="ID编号" />
                    <asp:BoundField DataField="name" HeaderText="用户名" />
                    <asp:BoundField DataField="tjrq" DataFormatString="{0:d}" HeaderText="注册时间" HtmlEncode="False" />
                    <asp:HyperLinkField DataNavigateUrlFields="id" DataNavigateUrlFormatString="firmpwd.aspx?id={0}" HeaderText="修改" Text="修改" />
                    <asp:ButtonField Text="&lt;span onclick=&quot;JavaScript:return confirm('确定删除吗？')&quot;&gt;删除&lt;/span&gt;" CommandName="delete" HeaderText="删除" />
                </Columns>
                <PagerSettings Mode="NextPreviousFirstLast" FirstPageText="首页" PreviousPageText="上一页" NextPageText="下一页" LastPageText="尾页" />
                <HeaderStyle HorizontalAlign="Center" Font-Size="13px" />
                <RowStyle HorizontalAlign="Center" Font-Size="13px" />
                <FooterStyle HorizontalAlign="Center" Font-Size="10px" />
                <SelectedRowStyle BackColor="#FFC080" />
            </asp:GridView>
        </div>
    </form>
</body>
</html>
