﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using System.Text;

public partial class gzhd : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            readhyb();
        }

    }

    private void readhyb()  //回音壁
    {
        string sql = "select id,title,r_name,tjrq from tb_reply where jg=1 and rownum<=4 order by id desc"; //jg即已处理
        hyb.DataSource = DbOperation.QueryBySql(sql);
        hyb.DataBind();

    }
}
