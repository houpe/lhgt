﻿<%@ Page Language="C#" MasterPageFile="~/GzhdMasterPage.master" AutoEventWireup="true" CodeFile="gzhd_hyb.aspx.cs" Inherits="gzhd_hyb" Title="南京市国土资源局六合分局-回音壁" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        #divMain table {
            background-color: #d9d9d9;
        }

        #divMain table td {
            background-color: #FFFFFF;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="divMain">
        <asp:DataList ID="DataList1" runat="server" DataKeyField="id" OnItemCommand="DataList1_ItemCommand" OnItemDataBound="DataList1_ItemDataBound">
            <HeaderTemplate>
                <table width="99%" height="25" border="0" cellpadding="1" cellspacing="0" style="text-align: center">
                    <tr>
                        <td>领导信箱</td>
                    </tr>
                </table>
            </HeaderTemplate>
            <ItemTemplate>
                <table width="99%" border="0" cellpadding="0" cellspacing="1">
                    <tr>
                        <td width="40" height="23" align="center">编号：</td>
                        <td width="25" align="center"><%#Eval("id") %></td>
                        <td width="60" align="center">标题：</td>
                        <td width="100" align="center"><%#Eval("title") %></td>
                        <td width="45" align="center">Email：</td>
                        <td width="90" align="center"><%#Eval("email") %></td>
                        <td width="40" align="center">电话：</td>
                        <td width="80" align="center"><%#Eval("tel") %></td>
                        <td width="45" align="center">时间:</td>
                        <td width="150" align="center"><%#Eval("tjrq") %></td>
                    </tr>
                    <tr>
                        <td height="50" align="center">内容：</td>
                        <td align="left" colspan="9"><%#Eval("wznr") %></td>
                    </tr>
                    <tr>
                        <td height="37" align="center">回复：</td>
                        <td colspan="8"><%#Eval("reply") %></td>
                        <td width="110"><%#Eval("r_name") %></td>
                    </tr>
                </table>
            </ItemTemplate>
            <FooterTemplate>
                <div align="center">
                    <table border="0" cellpadding="0" cellspacing="0" style="border-style: none">
                        <tr>
                            <td>当前页:<asp:Label ID="labCurrentPage" runat="server"></asp:Label>/
                <asp:Label ID="labPageCount" runat="server"></asp:Label>
                                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="first" Font-Underline="False" ForeColor="Black">首页</asp:LinkButton>
                                <asp:LinkButton ID="LinkButton2" runat="server" CommandName="pre" Font-Underline="False" ForeColor="Black">上一页</asp:LinkButton>
                                <asp:LinkButton ID="LinkButton3" runat="server" CommandName="next" Font-Underline="False" ForeColor="Black">下一页</asp:LinkButton>
                                <asp:LinkButton ID="LinkButton4" runat="server" CommandName="last" Font-Underline="False" ForeColor="Black">尾页</asp:LinkButton></td>
                        </tr>
                    </table>
                </div>
            </FooterTemplate>
        </asp:DataList>
    </div>
</asp:Content>


