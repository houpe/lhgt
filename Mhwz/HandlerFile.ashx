﻿<%@ WebHandler Language="C#" Class="HandlerImage" %>

using System;
using System.Web;
using System.Configuration;
public class HandlerImage : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {

        try
        {
            string path = System.Web.HttpContext.Current.Server.MapPath("UploadFiles");

            if (!System.IO.Directory.Exists(path))
            {
                System.IO.Directory.CreateDirectory(path);
            }
            String callback = context.Request["CKEditorFuncNum"];
            HttpPostedFile file = context.Request.Files[0];
            string filename = file.FileName;
            
            string filetype = file.ContentType;
            int index = filetype.IndexOf("/");
            string isFiletype = filetype.Substring(0, index);
            string isApplication = filename.Substring(filename.LastIndexOf(".") + 1, filename.Length - filename.LastIndexOf(".") - 1);
            string key = Guid.NewGuid().ToString();
            if (isFiletype == "image" || isApplication == "pdf" || isApplication == "doc" || isApplication == "docx" || isApplication == "xls" || isApplication == "xlsx")
            {
                int indext = filename.LastIndexOf("\\");
                string name = file.FileName.Substring(indext + 1);
                string pathname = path + "\\" + name;
                if (!System.IO.File.Exists(pathname))
                {
                    file.SaveAs(pathname);
                    string imageurl = ConfigurationManager.AppSettings["imageUrl"];
                    context.Response.ContentType = "text/html";
                    context.Response.Write("<script type=\"text/javascript\">");
                    context.Response.Write("window.parent.CKEDITOR.tools.callFunction(" + callback + ",'" + "" + imageurl + "/" + name + "','')");
                    context.Response.Write("</script>");
                }
                else
                {
                    int indexChangeName = name.IndexOf('.');
                    string lastName = name.Substring(0, indexChangeName) + key;
                    string lastType = name.Substring(indexChangeName, name.Length - indexChangeName);
                    string allName = lastName + lastType;
                    pathname = path + "\\" + allName;
                    file.SaveAs(pathname);
                    string imageurl = ConfigurationManager.AppSettings["imageUrl"];
                    context.Response.ContentType = "text/html";
                    context.Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>");
                    context.Response.Write("<script type=\"text/javascript\">");
                    context.Response.Write("window.parent.CKEDITOR.tools.callFunction(" + callback + ",'" + "" + imageurl + "/" + allName + "','')");
                    context.Response.Write("</script>");

                }
            }
            else
            {
                string resultTxt = "<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>";
               resultTxt += "<script type=\"text/javascript\">";
                resultTxt += "window.parent.CKEDITOR.tools.callFunction(" + callback + ",''," + "'文件格式不正确（必须为.jpg/.gif/.bmp/.png/.doc/.docx/.pdf/.xls/.xlsx文件）');";
                resultTxt += "</script>";
                context.Response.Write(resultTxt);

            }

        }
        catch (System.IO.IOException e)
        {
            context.Response.Write(e.Message);
        }



    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}