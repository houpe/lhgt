﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using System.Collections;

/// <summary>
/// Operation 的摘要说明
/// </summary>
public class BusinessOperation
{
    public BusinessOperation()
    {
    }

    #region 一级栏目

    public static void deletelm(string strquery) //删除一级栏目
    {
        string sql1 = string.Format("delete from tb_class where classid='{0}'",strquery);
        string sql2 = string.Format("delete from tb_sclass where classid='{0}'",strquery);
        string sql3 = string.Format("delete from tb_article where classid='{0}'", strquery);

        ArrayList myAL = new ArrayList();
        myAL.Add(sql1);
        myAL.Add(sql2);
        myAL.Add(sql3);
        DbOperation.ExecuteSqlTran(myAL);

    }
    public static void updatelm(string txt, string strquery)  //更新一级栏目
    {
        string sql = string.Format("update tb_class set c_name='{0}'where classid='{1}'" , txt,strquery);
        DbOperation.ExecuteSql(sql);
    }
    public static void insertlm(string txt)
    {
        string sql = string.Format("insert into tb_class (c_name) values('{0}')",txt);
        DbOperation.ExecuteSql(sql);
    }
    #endregion

    public static void zlmdelete(string strquery)  //删除子栏目
    {
        string sql1 = string.Format("delete from tb_sclass where s_id='{0}'",strquery);
        string sql2 = string.Format("delete from tb_article where s_id='{0}'", strquery);
        ArrayList myAL = new ArrayList();
        myAL.Add(sql1);
        myAL.Add(sql2);
        DbOperation.ExecuteSqlTran(myAL);
    }

    public static void zlmupdate(string txt, string strquery)//更改子栏目
    {
        string sql = string.Format("update tb_sclass set s_name='{0}' where s_id='{1}'",txt,strquery);
        DbOperation.ExecuteSql(sql);
    }

    public static void insertzlm(string txt, string strquery)  //添加子栏目
    {
        string sql = string.Format("insert into tb_sclass (s_name,classid) values('{0}','{1}')",txt,strquery);
        DbOperation.ExecuteSql(sql);
    }

    public static DataTable userbind()  //用户绑定
    {
        string sql = "select * from tb_admin ";

        return DbOperation.QueryBySql(sql);
    }

    public static void addadmin(string user, string pwd, string i)  //添加用户
    {
        string sql = string.Format("insert into tb_admin (name,pwd,qx) values('{0}','{1}','{2}')",user,pwd,i);
        DbOperation.ExecuteSql(sql);
    }

    public static void deleteuser(string str)
    {
        string sql = string.Format("delete from tb_admin where id='{0}'",str);
        DbOperation.ExecuteSql(sql);
    }

    public static DataTable mydc()  //tb_myddc
    {
        string sql = "select id,question from tb_myddc ";
        return DbOperation.QueryBySql(sql);
    }

    public static void deletedc(string str)
    {
        string sql = "delete from tb_myddc where id=" + str;
        DbOperation.ExecuteSql(sql);
    }

    /// <summary>
    /// 读取数据
    /// </summary>
    /// <param name="i">条数</param>
    /// <param name="Sid">二级栏目id</param>
    /// <returns></returns>
    public static DataTable readdatabind(int i, string Sid)
    {
        string sql = string.Format("select art_id,classid,s_id,title,to_char(tjrq,'yyyy-mm-dd') as tjrq,tjrq as tt  from tb_article where s_id='{0}' and rownum<={1} order by tt desc", Sid, i);
        return DbOperation.QueryBySql(sql);
    }

    /// <summary>
    /// 读取信息公开数据
    /// </summary>
    /// <param name="i">显示条数</param>
    /// <param name="strBid">大类id</param>
    /// <param name="Sid">小类id</param>
    /// <param name="strWhere">其他条件</param>
    /// <returns></returns>
    public static DataTable GetList(int i, string strBid, string Sid, string strWhere)
    {
        int ii = 0;
        // string sql = string.Format("select * from (select art_id,classid,s_id,title,tjrq from (select * from tb_article where bszn_fl='{0}' ) order by tjrq desc) where rownum<={0}",strWhere,i);
        //select * from (select art_id, classid, s_id, title, tjrq from tb_article where bszn_fl = '1' and classid = 6 and s_id = 25 order by tjrq desc) where rownum <= 8
        string sql = "select * from (select art_id,classid,s_id,title,to_char(tjrq,'yyyy-mm-dd') as tjrq,tjrq as tt from tb_article  where 1=1 ";

        if (!string.IsNullOrEmpty(strBid))
        {
            sql += string.Format(" and classid={0} ", strBid);
        }
        if (!string.IsNullOrEmpty(Sid))
        {
            sql += string.Format(" and s_id={0} ", Sid);
        }

        if (!string.IsNullOrEmpty(strWhere))
        {
            sql += strWhere;
        } 
        sql += "order by tt desc )";
        if (i != null&&i>0)
        {
            sql += string.Format(" where rownum<={0}", i);
        }
        return DbOperation.QueryBySql(sql);
    }

    /// <summary>
    /// 读取信息便民服务公开数据（信息公开页面用）
    /// </summary>
    /// <param name="i">显示条数</param>
    /// <param name="strBid">大类id</param>
    /// <param name="Sid">小类id</param>
    /// <param name="strWhere">其他条件</param>
    /// <returns></returns>
    public static DataTable GetListBianMin(int i, string strBid, string Sid, string strWhere)
    {
        string sql = " select * from (select id,name,to_char(tjrq,'yyyy-mm-dd') as tjrq ,tjrq as tt from tb_tousu  where 1=1 ";
        
        //if (!string.IsNullOrEmpty(strBid))
        //{

        //    sql += string.Format(" and classid={0} ", strBid);
        //}
        //if (!string.IsNullOrEmpty(Sid))
        //{
        //    sql += string.Format(" and s_id={0} ", Sid);
        //}

        if (!string.IsNullOrEmpty(strWhere))
        {
            sql += strWhere;
        }
        sql += "order by tt desc )";
        if (i != null && i > 0)
        {
            sql += string.Format(" where rownum<={0}", i);
        }
        return DbOperation.QueryBySql(sql);
    }



    /// <summary>
    /// 信访信箱
    /// </summary>
    /// <returns></returns>
    public static DataTable readhyb()  
    {
        string sql = "select id,title,r_name,tjrq from tb_reply where jg=1 and rownum<=4 order by tjrq desc"; //jg即已处理
        return DbOperation.QueryBySql(sql);
    }

    /// <summary>
    /// 投票
    /// </summary>
    /// <param name="da"></param>
    /// <param name="i"></param>
    public static void toupiao(string da, int i) ///第几个
    {
        string sql = string.Format("update tb_myddc set {0}={0}+1 where id='{1}'" + da,i);
        DbOperation.ExecuteSql(sql);
    }

    /// <summary>
    /// 更新访问量
    /// </summary>
    public static void UpdateFwl()
    {
        string sql ="update TB_SITE_COUNT set FWS=FWS+1,FWSJ=sysdate";
        DbOperation.ExecuteSql(sql);
    }

    /// <summary>
    /// 获取最新访问量
    /// </summary>
    public static string SelectFwl()
    {
        string sql = "select fws from TB_SITE_COUNT";
        return DbOperation.GetSingle(sql);
    }

    /// <summary>
    /// 根据类别获取提醒信息
    /// </summary>
    /// <param name="nType"></param>
    /// <returns></returns>
    public static string GetTiXing(int nType)
    {
        string sql = string.Empty;
        switch (nType)
        {
            case 1://局长信箱
                sql = "select count(*) from  tb_reply where reply is null";
                break;
            case 2://信访信箱
                sql = "select count(*) from  tb_tousu where reply is null";
                break;
            case 3://政务咨询
                sql = "select count(*) from  tb_ut_zwzx where zxhf is null ";
                break;
            case 4://依信息公开
                sql = "select count(*) from  UT_CM_MEASSGE where XXHF is null";
                break;
            case 5://待审核文章
                sql = "select count(*) from  TB_ARTICLESUBMIT ";
                break;
        }

        return DbOperation.GetSingle(sql);
    }
}
