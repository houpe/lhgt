﻿
/// <summary>
/// SqlInterlgeHelper 的摘要说明
/// </summary>	 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
/// <summary> 
///SQLInjectionHelper 的摘要说明 
/// </summary> 
public class SQLInjectionHelper
{
    /// <summary> 
    /// 获取Post的数据 
    /// </summary> 
    /// <param name="request"></param> 
    /// <returns></returns> 
    public static bool ValidUrlData(string request)
    {
        bool result = false;
        if (request == "POST")
        {
            //for (int i = 0; i < HttpContext.Current.Request.Form.Count; i++)
            //{
            //    result = ValidData(0, HttpContext.Current.Request.Form[i].ToString());
            //    if (result)
            //    {
            //        break;
            //    }
            //}
        }
        else
        {
            for (int i = 0; i < HttpContext.Current.Request.QueryString.Count; i++)
            {
                result = ValidData(1,HttpContext.Current.Request.QueryString[i].ToString());
                if (result)
                {
                    break;
                }
            }
        }
        return result;
    }
    /// <summary> 
    /// 验证是否存在注入代码 
    /// </summary> 
    /// <param name="inputData"></param> 
    /// <returns></returns> 
    private static bool ValidData(int nFlag,string inputData)
    {
        //验证inputData是否包含恶意集合 
        string strRegex = GetRegexStringForGet();
        if(nFlag==0)//post方式采用简化版的防sql注入
        {
            strRegex = GetRegexStringForPost();
        }

        if (Regex.IsMatch(inputData, strRegex))
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    /// <summary> 
    /// 获取http get方式正则表达式 
    /// </summary> 
    /// <returns></returns> 
    private static string GetRegexStringForGet()
    {
        //构造SQL的注入关键字符 【 "and", "asc", "or", ";",":","\'", "\"", "%", "mid","char", "chr", "count",, "net user", "net localgroup administrators", "exec master.dbo.xp_cmdshell"】
        string[] strChar = { "exec", "drop", "insert", "select", "update", "delete", "from", "master", "truncate", "declare", "SiteName", "/add", "xp_cmdshell", "and", "chr","DBMS_PIPE","DUAL","union"};
        string str_Regex = ".*(";
        for (int i = 0; i < strChar.Length - 1; i++)
        {
            str_Regex += strChar[i] + "|";
        }
        str_Regex += strChar[strChar.Length - 1] + ").*";
        return str_Regex;
    }

    /// <summary> 
    /// 获取http post方式正则表达式 
    /// </summary> 
    /// <returns></returns> 
    private static string GetRegexStringForPost()
    {
        //构造SQL的注入关键字符 【"exec","drop", "and", "asc", "or", ";",":","\'", "\"", "%", "mid","char", "chr", "count",, "net user", "net localgroup administrators", "exec master.dbo.xp_cmdshell","declare", "SiteName", "/add", "xp_cmdshell"】
        string[] strChar = { "insert", "select", "update", "delete", "from", "master","truncate"};
        string str_Regex = ".*(";
        for (int i = 0; i < strChar.Length - 1; i++)
        {
            str_Regex += strChar[i] + "|";
        }
        str_Regex += strChar[strChar.Length - 1] + ").*";
        return str_Regex;
    }
}

