﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Text;
using System.Configuration;

/// <summary>
/// ClsWzflOperation 的摘要说明
/// </summary>
public class ClsWzflOperation
{
	public ClsWzflOperation()
	{
		//
		// TODO: 在此处添加构造函数逻辑
		//
	}

    /// <summary>
    /// 获取组配分类信息
    /// </summary>
    /// <param name="strUlId"></param>
    /// <param name="strUrl"></param>
    /// <returns></returns>
    public static string GetZpfl(string strUlId, string strUrl)
    {
        string hidedata = "";
        hidedata = "'"+ConfigurationManager.AppSettings["hideData"].Replace(",","','")+"'";
         
        string strContent = string.Empty;
        string sql = string.Format("select * from TB_CLASS t where c_name not in({0})",hidedata);
        System.Data.DataTable dtParent = DbOperation.QueryBySql(sql);

        if (dtParent.Rows.Count > 0)
        {
            strContent += "<ul id='ulZpfl' class='easyui-tree' data-options='animate:true'> ";
            for (int i = 0; i < dtParent.Rows.Count; i++)
            {
                System.Data.DataRow drParent = dtParent.Rows[i];

                string strId = drParent["classid"].ToString();
                sql = string.Format("select * from TB_SCLASS t where classid='{0}'", strId);
                System.Data.DataTable dtChild = DbOperation.QueryBySql(sql);

                strContent += string.Format("<li data-options=\"state:'closed'\"><span id='{0}'>{1}({2})</span>", strId, drParent["c_name"], dtChild.Rows.Count);

                if (dtChild.Rows.Count > 0)
                {
                    strContent += "<ul>";
                    foreach (System.Data.DataRow drChild in dtChild.Rows)
                    {
                      
                      // strContent += string.Format("<li id='{0}'><a href='{2}?s_id={0}' target='xxgk_content'>{1}</a>" , drChild["s_id"], drChild["s_name"], strUrl);
                       strContent += string.Format("<li id='{0}'><a href=javascript:AddTab('{2}?s_id={0}')>{1}</a>", drChild["s_id"], drChild["s_name"], strUrl);
                        strContent += "</li>";
                    }
                    strContent += "</ul>";
                }

                strContent += "</li>";
            }
            strContent += "</ul> ";
        }
        return strContent;
    }

    /// <summary>
    /// 获取主题或题材分类信息
    /// </summary>
    /// <param name="strUlId"></param>
    /// <param name="strUrl"></param>
    /// <param name="strType">类别</param>
    /// <returns></returns>
    public static string GetZtOrTcfl(string strUlId, string strUrl,string strType)
    {
        string zttfl = "";//存放主题分类和体裁分类的SQL字段;
        string strContent = string.Empty;
        string sql = "select * from TB_ZTFL t where pid=-1";
        if (!string.IsNullOrEmpty(strType))
        {
            sql += string.Format(" and type='{0}'", strType);
            if (strType == "主题分类")
            {
                zttfl = "ztfl";
            }
            else if(strType == "体裁分类")
            {
                zttfl = "tcfl";
            }
        }
        System.Data.DataTable dtParent = DbOperation.QueryBySql(sql);

        if (dtParent.Rows.Count > 0)
        {
            strContent += "<ul id='ulZpfl' class='easyui-tree' data-options='animate:true'> ";
            for (int i = 0; i < dtParent.Rows.Count; i++)
            {
                System.Data.DataRow drParent = dtParent.Rows[i];

                string strId = drParent["id"].ToString();
                sql = string.Format("select * from TB_ZTFL t where pid='{0}'", strId);
                if (!string.IsNullOrEmpty(strType))
                {
                    sql += string.Format(" and type='{0}'", strType);
                }
                System.Data.DataTable dtChild = DbOperation.QueryBySql(sql);

               // strContent += string.Format("<li><span id='{0}'><a href=javascript:AddTab('{1}?type=" + System.Web.HttpUtility.UrlEncode(drParent["name"].ToString(), Encoding.GetEncoding("utf-8")) + "&zttfl={2},')>{3}</a></span>", strId, strUrl, zttfl, drParent["name"]);
                strContent += string.Format("<li><span id='{0}'><a href=javascript:AddTab('{1}?type=" + drParent["name"].ToString() + "&zttfl={2}')>{3}</a></span>", strId, strUrl, zttfl, drParent["name"]);
                //strContent += string.Format("<li><span id='{0}'><a href=javascript:AddTab('{1}?zttfl={2}','" + drParent["name"].ToString()+"')>{3}</a></span>", strId, strUrl, zttfl, drParent["name"]);

                if (dtChild.Rows.Count > 0)
                {
                    strContent += "<ul>";
                    foreach (System.Data.DataRow drChild in dtChild.Rows)
                    {
                        strContent += string.Format("<li id='{0}'><a href=javascript:AddTab('{2}?s_id={0}')>{1}</a>", drChild["id"], drChild["name"], strUrl);
                        strContent += "</li>";
                    }
                    strContent += "</ul>";
                }

                strContent += "</li>";
            }
            strContent += "</ul> ";
        }
        return strContent;
    }
}