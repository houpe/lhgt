﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections;
using Business;
using WF_DataAccess;
using WF_Business;

/// <summary>
/// DB 的摘要说明
/// </summary>
public class DbOperation
{
    public DbOperation()
    {
    }

    /// <summary>
    /// 执行OleDb语句，返回影响的记录数  执行简单sql语句
    /// </summary>
    /// <param name="OleDbString">sql语句</param>
    /// <returns>影响的记录数</returns>
    public static int ExecuteSql(string SQLString)
    {
        return SysParams.OAConnection().RunSql(SQLString);
    }

    /// <summary>
    /// 执行多条SQL语句，实现数据库事务。
    /// </summary>
    /// <param name="OleDbStringList">多条OleDb语句</param>		
    public static void ExecuteSqlTran(ArrayList SQLStringList)
    {
        IDataAccess idaTemp = SysParams.OAConnection(true);
        try
        {
            for (int n = 0; n < SQLStringList.Count; n++)
            {
                string strsql = SQLStringList[n].ToString();
                if (strsql.Trim().Length > 1)
                {
                    idaTemp.RunSql(strsql);
                }
            }
            idaTemp.Close(true);
        }
        catch
        {
            idaTemp.Close(false);
            throw;
        }
    }


    /// <summary>
    /// execute multipul SQL commands 
    /// </summary>
    /// <param name="strSQLs">string</param>
    /// <returns>int</returns>
    protected static int ExecuteSqls(string[] strSQLs)
    {
        IDataAccess idaTemp = SysParams.OAConnection(true);
        int nReturn = 1;
        try
        {
            foreach (string str in strSQLs)
            {
                idaTemp.RunSql(str);
            }
            idaTemp.Close(true);
            nReturn = 0;
        }
        catch
        {
            throw;
        }
        finally
        {
            idaTemp.Close(false);
        }
        return nReturn;
    }

    /// <summary>
    /// 执行一条计算查询结果语句，返回查询结果（object）。
    /// </summary>
    /// <param name="SQLString">计算查询结果语句</param>
    /// <returns>查询结果（object）</returns>
    public static string GetSingle(string SQLString)
    {
        return SysParams.OAConnection().GetValue(SQLString);
    }



    /// <summary>
    /// 执行查询语句，返回SqlDataReader
    /// </summary>
    /// <param name="strSQL">查询语句</param>
    /// <returns>SqlDataReader</returns>
    public static IDataReader ExecuteReader(string strSQL)
    {
        IDataReader idrTemp = SysParams.OAConnection().GetDataReader(strSQL);
        return idrTemp;
    }

    /// <summary>
    /// 执行查询语句，返回DataSet
    /// </summary>
    /// <param name="SQLString">查询语句</param>
    /// <returns>DataSet</returns>
    public static DataTable QueryBySql(string SQLString)
    {
        DataSet dsReturn = new DataSet();
        SysParams.OAConnection().RunSql(SQLString, out dsReturn);
        DataTable dtReturn = new DataTable();

        if (dsReturn.Tables.Count > 0)
        {
            dtReturn = dsReturn.Tables[0];
        }

        return dtReturn;
    }

}



