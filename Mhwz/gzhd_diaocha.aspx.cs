﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;

public partial class gzhd_diaocha : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string sql = "select id from tb_myddc"; //jg即已处理
            DataList1.DataSource = DbOperation.QueryBySql(sql);
            DataList1.DataBind();

        }
    }


    protected void DataList1_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        DataRowView dtr = (DataRowView)e.Item.DataItem;
        if (dtr != null)
        {
            string strId = dtr["id"].ToString();

            UserControl_ucDiaoCha ucTemp = e.Item.FindControl("ucDiaoCha") as UserControl_ucDiaoCha;
            if (null != ucTemp)
            {
                ucTemp.TpId = strId;
            }
        }
    }
}
