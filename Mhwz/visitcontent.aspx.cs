﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class visitcontent : System.Web.UI.Page
{
    public string strWzsl = "flvPath=";
    protected void Page_Load(object sender, EventArgs e)
    {
        string vid = Request["iid"];
        if (vid != null)
        {
            DataTable dtReturn = DbOperation.QueryBySql("select * from TB_TALKLIVE  where iid=" + vid);
            if (dtReturn.Rows.Count > 0)
            {
                lb_visitName.Text = dtReturn.Rows[0]["访谈名称"].ToString();
                lbGuestIns.Text = dtReturn.Rows[0]["嘉宾介绍"].ToString();
                lb_visitGuest.Text = dtReturn.Rows[0]["访谈嘉宾"].ToString();
                lb_visitHost.Text = dtReturn.Rows[0]["访谈主持"].ToString();
                lbInstruct.Text = dtReturn.Rows[0]["访谈说明"].ToString();
                lbTime.Text = dtReturn.Rows[0]["开始时间"].ToString();
                Image1.ImageUrl = dtReturn.Rows[0]["标题图片"].ToString();

                string strTemp = dtReturn.Rows[0]["文字实录"].ToString();
                if(!string.IsNullOrEmpty(strTemp))
                {
                    strWzsl = string.Format("flvPath={0}/{1}&isAutoPlay=0&isShowList=0", Request.ApplicationPath,strTemp);
                }
            }
        }
    }
}