﻿<%@ Page Language="C#" MasterPageFile="~/GzhdMasterPage.master" AutoEventWireup="true" CodeFile="gzhd_diaocha.aspx.cs" Inherits="gzhd_diaocha" Title="南京市国土资源局六合分局－－网站调查" %>

<%@ Register Src="~/UserControl/ucDiaoCha.ascx" TagPrefix="uc1" TagName="ucDiaoCha" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        #divMain table {
            background-color: #d9d9d9;
        }

        #divMain table td {
            background-color: #FFFFFF;
        }

        .tabCenter{
            text-align:center;
            width:100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="divMain" style="text-align: center;">
         <div style="text-align:center">民意调查</div>
        <asp:DataList ID="DataList1" runat="server" CssClass="tabCenter" OnItemDataBound="DataList1_ItemDataBound">
            <ItemTemplate>
                <uc1:ucDiaoCha runat="server" ID="ucDiaoCha" />
            </ItemTemplate>
        </asp:DataList>
    </div>
</asp:Content>

