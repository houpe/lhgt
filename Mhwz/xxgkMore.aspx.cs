﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class xxgkMore : System.Web.UI.Page
{
    public string LeftMenu=string.Empty;
    public string IframeContent = string.Empty;
    public string strContent = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        getLeftMenu();
    }

  /// <summary>
  /// 显示左侧导航和右侧对应List
  /// </summary>
    protected void getLeftMenu() 
    {
        if (!string.IsNullOrEmpty(Request["classid"]))
        {
            string strParen = string.Format("select c_name from tb_class where classid='{0}'", Request["classid"]);
            string strChil = string.Format("select s_name,classid,s_id from tb_sclass where classid='{0}'", Request["classid"]);
             
            System.Data.DataTable dtParent = DbOperation.QueryBySql(strParen);
            if (dtParent.Rows.Count > 0)
            {
                LeftMenu += " <div style='text-align: left; width: 100%; vertical-align: middle ' class='news_leftbg'>" + dtParent.Rows[0]["c_name"] + "</div>";
                if (Request["classid"] == "4")
                {
                    strChil += " and s_name in('土地证初始登记公告','废止土地证公告','其他通知公告')";
                }
                System.Data.DataTable dtChil = DbOperation.QueryBySql(strChil);
                if (dtChil.Rows.Count > 0)
                {
                    LeftMenu += "<div style='text-align: center; width: 230px;' class='gg_table'>";
                    if (!string.IsNullOrEmpty(Request["flag"]) && string.IsNullOrEmpty(Request["s_id"]))
                    {
                        if (Request["flag"] == "1")
                        {
                            for (int i = 0; i < dtChil.Rows.Count; i++)
                            {
                                LeftMenu += string.Format("<div style='width: 100%; height: 38px; text-align: left;' class='menu_nei_bg'><a href=javascript:AddxxgkList('xxgkSingle.aspx?classid={0}&s_id={1}') class='menu_nei'>{2}</a></div>", dtChil.Rows[i]["classid"], dtChil.Rows[i]["s_id"], dtChil.Rows[i]["s_name"]);
                                if (i == 0)
                                {
                                    IframeContent += string.Format(" <iframe id='xxgkmore_content' src='xxgkSingle.aspx?classid={0}&s_id={1}' name='xxgkmore_content' marginheight='0' marginwidth='0' frameborder='0' scrolling='no' height='100%' width='100%' onload='iFrameHeight3()' ></iframe>", dtChil.Rows[0]["classid"], dtChil.Rows[0]["s_id"]);
                                }
                            }
                        }
                    }
                    else if (string.IsNullOrEmpty(Request["flag"]) && !string.IsNullOrEmpty(Request["s_id"]))
                    {

                        for (int i = 0; i < dtChil.Rows.Count; i++)
                        {
                            LeftMenu += string.Format("<div style='width: 100%; height: 38px; text-align: left;' class='menu_nei_bg'><a href=javascript:AddxxgkList('xxgkSingle.aspx?classid={0}&s_id={1}') class='menu_nei'>{2}</a></div>", dtChil.Rows[i]["classid"], dtChil.Rows[i]["s_id"], dtChil.Rows[i]["s_name"]);

                        }

                        IframeContent += string.Format(" <iframe id='xxgkmore_content' src='xxgkSingle.aspx?classid={0}&s_id={1}' name='xxgkmore_content' marginheight='0' marginwidth='0' frameborder='0' scrolling='no' height='100%' width='100%' onload='iFrameHeight3()' ></iframe>", Request["classid"], Request["s_id"]);

                    }
                    else if (!string.IsNullOrEmpty(Request["art_id"]))
                    {
                        AppearBiaoTi();
                        for (int i = 0; i < dtChil.Rows.Count; i++)
                        {
                            LeftMenu += string.Format("<div style='width: 100%; height: 38px; text-align: left;' class='menu_nei_bg'><a href=javascript:AddxxgkList2('xxgkSingle.aspx?classid={0}&s_id={1}') class='menu_nei'>{2}</a></div>", dtChil.Rows[i]["classid"], dtChil.Rows[i]["s_id"], dtChil.Rows[i]["s_name"]);

                        }
                        getArtConent();

                    }
              
                }
                LeftMenu += "</div>";

            }
        }
    }


    /// <summary>
    /// 显示文章内容
    /// </summary>
    protected void getArtConent() 
    {
        string sql = string.Format("select art_id,title,author,WZNR,wfrom,hits,to_char(tjrq,'yyyy-mm-dd') as tjrq  from tb_article where art_id='{0}'", Request["art_id"]);
        DataTable dtReturn = DbOperation.QueryBySql(sql);

        if (dtReturn.Rows.Count > 0)
        {
            lbtitle.Text = dtReturn.Rows[0]["title"].ToString();
            //lbauthor.Text = dtReturn.Rows[0]["author"].ToString();
           // strContent = dtReturn.Rows[0]["WZNR"].ToString();
            strContent = dtReturn.Rows[0]["WZNR"].ToString();
            lbfrom.Text = dtReturn.Rows[0]["wfrom"].ToString();
            //lbhit.Text = dtReturn.Rows[0]["hits"].ToString();
            lbdate.Text = dtReturn.Rows[0]["tjrq"].ToString();
        }
    }

    private void AppearBiaoTi()   //显示一、二级导航标题
    {
        string strSql = "select s_id,s_name,c_name from tb_class a,tb_sclass b where a.classid=b.classid";

        if (!string.IsNullOrEmpty(Request["classid"]))
        {
            strSql += string.Format(" and b.classid='{0}'", Request["classid"]);
        }

        bool bHavaSmallClass = false;
        if (!string.IsNullOrEmpty(Request["s_id"]))
        {
            strSql += string.Format(" and s_id='{0}'", Request["s_id"]);
            bHavaSmallClass = true;
        }

        DataTable dtReturn = DbOperation.QueryBySql(strSql);
        if (dtReturn.Rows.Count > 0)
        {
            strclass.Text = dtReturn.Rows[0]["c_name"].ToString();

            if (bHavaSmallClass)
            {
                strsclass.Text = dtReturn.Rows[0]["s_name"].ToString();
                strclassname.Text = dtReturn.Rows[0]["s_name"].ToString();
            }
            else
            {
                strclassname.Text = dtReturn.Rows[0]["c_name"].ToString();
            }
        }
        else
        {
            strclass.Text = "暂无栏目";
            strsclass.Text = "暂无栏目";
            strclassname.Text = "暂无栏目";
        }

    }
}