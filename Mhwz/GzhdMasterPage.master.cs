﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class GzhdMasterPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            AppearBiaoTi();   //二级导航标题
        }
    }

    protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
    {
        if (RadioButton1.Checked == true)
        {
            toupiao("a_count", "DBEF6E1A0ACC49CF9E119B582FA370CA");
        }
        if (RadioButton2.Checked == true)
        {
            toupiao("b_count", "DBEF6E1A0ACC49CF9E119B582FA370CA");
        }
        if (RadioButton3.Checked == true)
        {
            toupiao("c_count", "DBEF6E1A0ACC49CF9E119B582FA370CA");
        }
        if (RadioButton4.Checked == true)
        {
            toupiao("d_count", "DBEF6E1A0ACC49CF9E119B582FA370CA");
        }
    }

    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("gzhd_diaocha.aspx");
    }

    private void toupiao(string da, string strId) ///第几个
    {
        string sql = string.Format("update tb_myddc set {0}={0}+1 where id='{1}'",da,strId);
        DbOperation.ExecuteSql(sql);
        Response.Write("<script>alert('感谢您的支持!投票成功!');location='javascript:history.go(-1)';</script>");
    }

    private void AppearBiaoTi()   //显示一、二级导航标题
    {
        if (!string.IsNullOrEmpty(Request["classid"]) || !string.IsNullOrEmpty(Request["s_id"]))
        {
            string strSql = "select s_id,s_name,c_name from tb_class a,tb_sclass b where a.classid=b.classid";

            if (!string.IsNullOrEmpty(Request["classid"]))
            {
                strSql += string.Format(" and b.classid='{0}'", Request["classid"]);
            }

            bool bHavaSmallClass = false;
            if (!string.IsNullOrEmpty(Request["s_id"]))
            {
                strSql += string.Format(" and s_id='{0}'", Request["s_id"]);
                bHavaSmallClass = true;
            }

            DataTable dtReturn = DbOperation.QueryBySql(strSql);
            if (dtReturn.Rows.Count > 0)
            {
                strclass.Text = dtReturn.Rows[0]["c_name"].ToString();

                if (bHavaSmallClass)
                {
                    strsclass.Text = dtReturn.Rows[0]["s_name"].ToString();
                    strclassname.Text = dtReturn.Rows[0]["s_name"].ToString();
                }
                else
                {
                    strclassname.Text = dtReturn.Rows[0]["c_name"].ToString();
                }
            }
        }
        else
        {
            strclass.Text = "公众互动";
            strsclass.Text = "";
            strclassname.Text = "";
        }

    }

    private string readcount(string str)  //读取标题总数
    {
        string sql = "select count(*) from tb_article where s_id=" + str;
        string count =DbOperation.GetSingle(sql);
        return count;

    }
}
