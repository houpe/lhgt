﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Default" %>

<!DOCTYPE html>

<%@ Register Src="~/UserControl/head.ascx" TagName="mhead" TagPrefix="uc1" %>
<%@ Register Src="~/UserControl/foot.ascx" TagName="m_foot" TagPrefix="uc2" %>
<%@ Register Src="UserControl/ucMsgPart.ascx" TagName="ucMsgPart" TagPrefix="uc3" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html;" />
    <title>南京市国土资源局六合分局</title>
    <link href="css/main.css" rel="stylesheet" type="text/css" />
    <link href="css/ad.css" rel="stylesheet" />
    <script src="js/main.js"></script>

    <script src="js/jquery.js"></script>
    <script src="js/jquery.floatingAd.js"></script>
    <script type="text/javascript">
        $(function () {
            $.floatingAd({
                //频率
                delay: 60,
                //超链接后是否关闭漂浮
                isLinkClosed: true,
                //漂浮内容
                ad: [{
                    //关闭区域背景透明度(0.1-1)
                    headFilter: 0.3,

                    'title': '网站关闭通知',
                    //图片
                    'img': 'images/closeNote.png',//网上公开检查
                    //图片高度
                    'imgHeight': 146,
                    //图片宽度
                    'imgWidth': 230,

                    //标题
                    //'title': '政务信息网上公开检查内容表',
                    //图片
                    //'img': 'images/guanggao.jpg',//网上公开检查
                    //图片高度
                    //'imgHeight': 146,
                    //图片宽度
                    //'imgWidth': 230,
                    //图片链接
                    //'linkUrl': 'xxgkContent.aspx?classid=4&art_id=EBCB8676A8BA4F759C3D7338E9194B1F',//对应的公开检查内容

                    //'title': '第47个世界地球日',
                    //'img': 'images/47dqr.jpg',//地球日
                    //'imgHeight': 153,
                    //'imgWidth': 230,                    
                    //'linkUrl': 'xxgkContent.aspx?classid=7&art_id=868221DB958A4D2FAF5352FACD622BF8',//地球日
                    //浮动层级别

                    'z-index': 100,                   
                    'closed-icon': 'images/close.png'
                }],
                //关闭事件
                onClose: function (elem) {
                    $("#floatingAd").hide();
                }
            });
        });

      
	</script>
</head>
<body>
    <form id="form1" runat="server">
        <div style="text-align: center; width: 993px; margin: 0px auto;">
            <div id="tdTop">
                <uc1:mhead ID="mhead" runat="server" />
            </div>
            <div style="width: 100%; margin: 0px auto;">
                <div style="width: 742px; float: left;">
                    <div style="width: 100%; height: 9px; background-image: url(images/index_08.jpg)"></div>

                    <div style="width: 100%; height: 265px;">
                        <div style="width: 281px; float: left;">
                            <iframe id="iframe1" src="flashtu.aspx" scrolling="no" frameborder="0" width="282" height="241" marginheight="0" marginwidth="0"></iframe>
                        </div>
                        <div style="width: 6px; float: left;">&nbsp;</div>
                        <div style="float: left;">
                            <div style="width: 100%; height: 31px; background: url(images/index_15.jpg)">

                                <div style="width: 31px; float: left;">
                                    <img src="images/index_14.jpg" width="31" height="31" />
                                </div>
                                <div style="float: left; width: 350px; text-align: left"><a href="xxgk.aspx?classid=2" class="f14" id="gztdTabTop1"><strong>国土资讯</strong></a> </div>
                                <div style="width: 62px; float: left; text-align: right"><a href="xxgk.aspx?classid=2" class="blue" target="_blank">更多&gt;&gt;</a></div>
                                <div style="width: 5px; float: left; text-align: right;">
                                    <img src="images/index_17.jpg" width="5" height="31" />
                                </div>

                            </div>

                            <div class="table1" id="gzdtTabBtm1" style="height: 200px;">
                                <uc3:ucMsgPart ID="gzdt" BigClass="2" RecordCount="8" HideLength="30" AppearMore="false" runat="server" />
                            </div>
                        </div>
                    </div>

                    <div style="width: 100%; height: 95px; border: solid 1px #cccccc;">
                        <%--<a href="xxgkContent.aspx?classid=13&art_id=433">
                            <img src="images/banner.jpg" width="738" height="79" style="border-style: none" /></a>--%>

                        <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0" width="742" height="87">
                            <param name="movie" value="images/banner_mid1.swf" />
                            <param name="quality" value="high" />
                            <embed src="images/banner_mid1.swf" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="742" height="87"></embed>
                        </object>

                    </div>

                    <div style="width: 100%">

                        <div style="width: 100%; height: 31px; text-align: left; background: url(images/index_15.jpg)">
                            <div style="text-align: left; float: left;">
                                <img src="images/index_14.jpg" width="31" height="31" />
                            </div>
                            <div style="text-align: left; float: left; width: 641px; margin-top: 5px;" class="font3">
                                征地信息公开
                            </div>
                            <div style="width: 64px; float: left; text-align: right"><a href="xxgkMore.aspx?classid=10&flag=1" class="blue" target="_blank">更多&gt;&gt;</a></div>
                            <div style="width: 5px; float: left; text-align: right">
                                <img src="images/index_17.jpg" width="5" height="31" />
                            </div>
                        </div>
                        <div style="width: 739px;" class="table1Copy">

                            <div style="height: 27px; text-align: left;" class="firstPage_tab">
                                <span id="xf1TabTop4" onmouseover="setTab('xf1', '1', 6, '', '');"><a href="xxgkMore.aspx?classid=10&s_id=72" target="_blank">一书四方案 </a></span>
                                <span id="xf1TabTop1" onmouseover="setTab('xf1', '2', 6, '', '');"><a href="xxgkMore.aspx?classid=10&s_id=16" target="_blank">征地听证公告</a></span>
                                <span id="xf1TabTop2" onmouseover="setTab('xf1', '3', 6, '', '');"><a href="xxgkMore.aspx?classid=10&s_id=17" target="_blank">征收土地公告</a></span>
                                <span id="xf1TabTop3" onmouseover="setTab('xf1', '4', 6, '', '');"><a href="bsdt_bgxz.aspx?classid=10&s_id=18" target="_blank">征地补偿公告</a></span>
                                <span id="xf1TabTop5" onmouseover="setTab('xf1', '5', 6, '', '');"><a href="bsdt_bgxz.aspx?classid=10&s_id=101" target="_blank">征地政策</a></span>
                                <span id="xf1TabTop6" onmouseover="setTab('xf1', '6', 6, '', '');"><a href="bsdt_bgxz.aspx?classid=10&s_id=102" target="_blank">征地批复</a></span>
                            </div>

                            <div id="xf1TabBtm1">
                                <div style="height: 5px; text-align: left;">
                                </div>
                                <div style="height: 130px; text-align: left; vertical-align: middle;">
                                    <uc3:ucMsgPart ID="UcMsgPart11" BigClass="10" HideLength="50" AppearMore="false" RecordCount="5" SmallClass="72" runat="server" Url="xxgkMore.aspx" />
                                </div>
                            </div>
                            <div style="display: none;" id="xf1TabBtm2">
                                <div style="height: 5px; text-align: left;">
                                </div>
                                <div style="height: 130px; text-align: left; vertical-align: middle;">

                                    <uc3:ucMsgPart ID="UcMsgPart8" BigClass="10" AppearMore="false" RecordCount="5" SmallClass="16" runat="server" HideLength="50" Url="xxgkMore.aspx" />
                                </div>
                            </div>
                            <div style="display: none;" id="xf1TabBtm3">
                                <div style="height: 5px; text-align: left;">
                                </div>
                                <div style="height: 130px; text-align: left; vertical-align: middle;">
                                    <uc3:ucMsgPart ID="UcMsgPart9" BigClass="10" AppearMore="false" RecordCount="5" SmallClass="17" runat="server" HideLength="50" Url="xxgkMore.aspx" />

                                </div>
                            </div>

                            <div style="display: none;" id="xf1TabBtm4">
                                <div style="height: 5px; text-align: left;">
                                </div>
                                <div style="height: 130px; text-align: left; vertical-align: middle;">
                                    <uc3:ucMsgPart ID="UcMsgPart10" BigClass="10" AppearMore="false" RecordCount="5" SmallClass="18" runat="server" HideLength="50" Url="xxgkMore.aspx" />
                                </div>
                            </div>
                            <div style="display: none;" id="xf1TabBtm5">
                                <div style="height: 5px; text-align: left;">
                                </div>
                                <div style="height: 130px; text-align: left; vertical-align: middle;">
                                    <uc3:ucMsgPart ID="UcMsgPart2" BigClass="10" AppearMore="false" RecordCount="5" SmallClass="101" runat="server" HideLength="50" Url="xxgkMore.aspx" />
                                </div>
                            </div>
                            <div style="display: none;" id="xf1TabBtm6">
                                <div style="height: 5px; text-align: left;">
                                </div>
                                <div style="height: 130px; text-align: left; vertical-align: middle;">
                                    <uc3:ucMsgPart ID="UcMsgPart3" BigClass="10" AppearMore="false" RecordCount="5" SmallClass="102" runat="server" HideLength="50" Url="xxgkMore.aspx" />
                                </div>
                            </div>

                        </div>

                        <div style="width: 100%; height: 31px; text-align: left; background: url(images/index_15.jpg)">
                            <div style="text-align: left; float: left;">
                                <img src="images/index_14.jpg" width="31" height="31" />
                            </div>
                            <div style="text-align: left; float: left; width: 641px; margin-top: 5px;" class="font3">
                                业务公告
                            </div>
                            <div style="width: 64px; float: left; text-align: right"><a href="xxgkMore.aspx?classid=4&flag=1" class="blue" target="_blank">更多&gt;&gt;</a></div>
                            <div style="width: 5px; float: left; text-align: right">
                                <img src="images/index_17.jpg" width="5" height="31" />
                            </div>
                        </div>
                        <div style="width: 739px;" class="table1Copy">

                            <div style="height: 27px; text-align: left;" class="firstPage_tab">

                                <span id="xf2TabTop1" onmouseover="setTab('xf2', '1', 5, '', '');"><a href="xxgkMore.aspx?classid=4&s_id=19" class="firstPage_tab" target="_blank">初始登记公告
                                </a></span><span style="display: none" id="xf2TabTop2" onmouseover="setTab('xf2', '2', 5, '', '');"><a href="xxgkMore.aspx?classid=4&s_id=20">
                                    <img src="images/xxk_tdzbggg.jpg" name="Image17" height="22" border="0" id="Image17" /></a></span><span style="display: none" id="xf2TabTop3" onmouseover="setTab('xf2', '3', 5, '', '');"><a href="xxgkMore.aspx?classid=4&s_id=21">
                                        <img src="images/xxk_zxtdzgg.jpg" name="Image18" height="22" border="0" id="Image18" /></a></span><span id="xf2TabTop4" class="firstPage_tab" onmouseover="setTab('xf2', '4', 5, '', '');"><a href="xxgkMore.aspx?classid=4&s_id=37" target="_blank">废止土地证公告</a></span><span id="xf2TabTop5" class="firstPage_tab" onmouseover="setTab('xf2', '5', 5, '', '');"><a href="xxgkMore.aspx?classid=4&s_id=42" target="_blank">其他通知公告</a></span>
                            </div>

                            <div id="xf2TabBtm1">
                                <div style="height: 5px; text-align: left;">
                                </div>
                                <div style="height: 130px; text-align: left; vertical-align: middle;">
                                    <uc3:ucMsgPart ID="ysbz" BigClass="4" AppearMore="false" RecordCount="5" HideLength="40" SmallClass="19" runat="server" Url="xxgkMore.aspx" />

                                </div>
                            </div>

                            <div style="display: none;" id="xf2TabBtm2">
                                <div style="height: 5px; text-align: left;">
                                </div>
                                <div style="height: 130px; text-align: left; vertical-align: middle;">
                                    <uc3:ucMsgPart ID="zxgg" BigClass="4" AppearMore="false" RecordCount="5" SmallClass="20" runat="server" HideLength="40" Url="xxgkMore.aspx" />
                                </div>
                            </div>

                            <div style="display: none;" id="xf2TabBtm3">
                                <div style="height: 5px; text-align: left;">
                                </div>
                                <div style="height: 130px; text-align: left; vertical-align: middle;">
                                    <uc3:ucMsgPart ID="dl_tdly" BigClass="4" AppearMore="false" RecordCount="5" SmallClass="21" runat="server" HideLength="40" Url="xxgkMore.aspx" />
                                </div>
                            </div>

                            <div style="display: none;" id="xf2TabBtm4">
                                <div style="height: 5px; text-align: left;">
                                </div>
                                <div style="height: 130px; text-align: left; vertical-align: middle;">
                                    <uc3:ucMsgPart ID="jsyd" BigClass="4" AppearMore="false" RecordCount="5" SmallClass="37" runat="server" HideLength="40" Url="xxgkMore.aspx" />
                                </div>
                            </div>

                            <div style="display: none;" id="xf2TabBtm5">
                                <div style="height: 5px; text-align: left;">
                                </div>
                                <div style="height: 130px; text-align: left; vertical-align: middle;">
                                    <uc3:ucMsgPart ID="UcMsgPart1" BigClass="4" AppearMore="false" RecordCount="5" SmallClass="42" runat="server" HideLength="40" Url="xxgkMore.aspx" />
                                </div>
                            </div>
                        </div>

                        <div style="height: 5px; text-align: left;">
                        </div>
                        <div style="width: 100%; height: 95px; border: solid 1px #cccccc;">
                            <img src="images/bhjyzt.jpg" width="742" height="87" />

                        </div>



                        <div style="width: 100%; height: 31px; text-align: left; vertical-align: middle; background: url(images/index_15.jpg)">
                            <div style="text-align: left; float: left;">
                                <img src="images/index_14.jpg" width="31" height="31" />
                            </div>
                            <div style="text-align: left; float: left; width: 642px; margin-top: 5px;" class="font3">
                                便民服务
                            </div>

                            <div style="width: 64px; float: left; text-align: right">&nbsp;&nbsp;</div>
                            <div style="width: 5px; float: left; text-align: right">
                                <img src="images/index_17.jpg" width="5" height="31" />
                            </div>
                        </div>

                        <div style="width: 739px" class="table1Copy">
                            <div>
                                <span><a href="xxgkLdxx.aspx?classid=13&s_id=49" target="_blank">
                                    <img src="images/ldxx.jpg" width="130" height="48" border="0" /></a></span>

                                <span style="padding-left: 50px;"><a href="xxgkList.aspx?classid=13&s_id=50" target="_blank">
                                    <img src="images/xfxx.jpg" width="130" height="48" border="0" /></a></span>

                                <span style="padding-left: 50px;"><a href="gzhd_diaocha.aspx?classid=13&s_id=52" target="_blank">
                                    <img src="images/mydc.jpg" width="130" height="48" border="0" /></a></span>
                                <span style="padding-left: 50px;"><a href="gzhd_xfxx.aspx?classid=13&s_id=53&tag=2" target="_blank">
                                    <img src="images/zwzx.jpg" width="130" height="48" border="0" /></a></span>
                            </div>
                        </div>

                        <div style="width: 100%; height: 6px; border: 0px;">
                        </div>

                    </div>

                </div>
                <div style="float: left; width: 6px;">

                    <div style="width: 100%; height: 9px; border: 0px; background-image: url(images/index_08.jpg)">
                    </div>
                </div>
                <div style="float: left; width: 245px; background-image: url(images/index_13.jpg)">

                    <div style="width: 100%; height: 10px; border: 0px; background-image: url(images/index_10.jpg)">
                    </div>

                    <div style="width: 100%; height: 220px;">
                        <div style="width: 99%; height: 26px;" class="banner1">
                            <div style="text-align: left; float: left; width: 40%; margin-top: 2px;" class="font4">最新更新</div>
                            <div style="text-align: right; float: left; width: 60%"><a href="xxgk.aspx?flag=1" class="blue" target="_blank">更多&gt;&gt;</a></div>
                        </div>
                        <div style="width: 100%">
                            <div id="demo" style="overflow: hidden; height: 194px; margin-top: 2px; width: 98%" class="table04">
                                <div id="demo1">
                                    <uc3:ucMsgPart ID="UcMsgPart12" RecordCount="20" AppearMore="false" HideLength="11" runat="server" BigClass="" SmallClass="" />
                                </div>
                                <div id="demo2"></div>
                            </div>

                            <script type="text/javascript">
                                scrollText();
                            </script>
                        </div>
                    </div>

                    <div style="width: 100%; height: 6px; border: 0px;"></div>
                    <div style="width: 100%; height: 26px; text-align: left;" class="banner1">
                        信息公开
                    </div>
                    <div style="width: 100%" class="table02">

                        <div style="height: 42px; padding-top: 10px;">
                            <a href="xxgk.aspx?classid=3&s_id=9" target="_blank">
                                <img src="images/xxgk_ml.jpg" width="201" height="33" border="0" /></a>
                        </div>
                        <div style="height: 42px;">
                            <a href="xxgk.aspx?classid=3&s_id=10" target="_blank">
                                <img src="images/xxgk_zn.jpg" width="201" height="33" border="0" /></a>
                        </div>
                        <div style="height: 42px;">
                            <a href="xxgk.aspx?classid=3&s_id=11" target="_blank">
                                <img src="images/xxgk_gzzd.jpg" width="201" height="33" border="0" /></a>
                        </div>
                        <div style="height: 42px;">
                            <a href="xxgk.aspx?classid=3&s_id=12" target="_blank">
                                <img src="images/xxgk_ndbg.jpg" width="201" height="33" border="0" /></a>
                        </div>
                        <div style="height: 42px;">

                            <a href="ysqgk.aspx" target="_blank">
                                <img src="images/xxgk_jg.jpg" width="201" height="33" border="0" /></a>
                        </div>
                    </div>
                    <div style="width: 100%; height: 6px; border: 0px;"></div>
                    <div style="width: 100%; height: 26px; text-align: left;" class="banner1">
                        <span class="font4">服务电话</span>
                    </div>
                    <div style="width: 100%; height: 90px; padding-top: 2px; text-align: center;">
                        <img src="images/jbdh.jpg" border="0" />
                    </div>

                    <div style="width: 100%; height: 6px; border: 0px;"></div>
                    <div style="width: 100%; height: 26px; text-align: left;" class="banner1">
                        <span class="font4">专项工作</span>
                    </div>

                    <div style="width: 100%">
                        <div style="padding-top: 9px;">
                            <a href="xxgk.aspx?classid=7&s_id=29" target="_blank">
                                <img src="images/dqr.jpg" width="234" height="61" border="0" />
                                <%-- <img src="images/earthday.jpg" width="232" height="61" border="0" />--%></a>
                        </div>
                        <div>
                            <a href="xxgk.aspx?classid=7&s_id=31" target="_blank">
                                <img src="images/tudiri.jpg" width="232" height="59" border="0" /></a>
                        </div>
                        <div>
                            <a href="xxgk.aspx?classid=7&s_id=40" target="_blank">
                                <img src="images/mingzhu.jpg" width="232" height="61" border="0" /></a>
                        </div>
                        <div>
                            <a href="xxgk.aspx?classid=7&s_id=48" target="_blank">
                                <img src="images/dangfeng.jpg" width="232" height="56" border="0" /></a>
                        </div>

                    </div>

                    <div style="width: 100%; height: 6px; border: 0px;"></div>
                    <div style="width: 100%; height: 26px; text-align: left;" class="banner1">
                        <span class="font4">网站计数：<%=GetFwl()%>人次</span>
                    </div>
                </div>
            </div>

            <div id="divBottom" style="margin-top: 3px;">
                <uc2:m_foot ID="foot" runat="server" />
            </div>
        </div>
    </form>
</body>
</html>
