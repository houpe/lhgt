﻿<%@ Page Language="C#" MasterPageFile="~/GzhdMasterPage.master" AutoEventWireup="true" CodeFile="gzhd_ftzb.aspx.cs" Inherits="gzhd_ftzb" Title="南京市国土资源局六合分局-访谈直播" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:DataList ID="dltitle" runat="server" Width="95%" DataKeyField="iid" OnItemCommand="dltitle_ItemCommand" OnItemDataBound="dltitle_ItemDataBound">
        <ItemTemplate>
            <table style="width: 100%; border-collapse: collapse; height: 187px" border="1" class="s5x5">
                <tbody>
                    <tr> 
                         <%--<a href='xxgkContent.aspx?classid=<%#Eval("classid")%>&art_id=<%#Eval("art_id") %>'--%>
                        <td style="width: 80px" rowspan="5"><a href='visitcontent.aspx?iid=<%#Eval("iid") %>' target="_blank" class="f14">
                            <img style="width: 176px; height: 167px" src="<%#Eval("标题图片")%>" width="70" height="59" setdata="ivpic"/></a></td>
                    </tr>
                    <tr>
                        <td style="text-align: left">&nbsp;主题：<%#CommonOperation.Left(Eval("访谈名称").ToString(),40) %></td>
                    </tr>
                    <tr>
                        <td style="text-align: left">&nbsp;日期：<%# Eval("开始时间")%></td>
                    </tr>
                    <tr>
                        <td style="text-align: left">&nbsp;访谈嘉宾：<%# Eval("访谈嘉宾")%></td>
                    </tr>
                    <tr>
                        <td style="text-align: left">&nbsp;访谈内容：<%# Eval("访谈说明")%></td>
                    </tr>
                </tbody>
            </table>

        </ItemTemplate>
        <FooterTemplate>
            <table style="text-align: center; width: 100%;">
                <tr>
                    <td height="50" colspan="2" align="center" valign="middle">共<asp:Label ID="titlecount" runat="server"></asp:Label>篇
                        <asp:LinkButton ID="LinkButton1" runat="server" CommandName="first">首页</asp:LinkButton><asp:LinkButton ID="LinkButton2"
                            runat="server" CommandName="pre">上一页</asp:LinkButton>
                        <asp:LinkButton ID="LinkButton3" runat="server" CommandName="next">下一页</asp:LinkButton><asp:LinkButton
                            ID="LinkButton4" runat="server" CommandName="last">尾页</asp:LinkButton>
                        页次:<asp:Label ID="labCurrentPage" runat="server"></asp:Label>/<asp:Label
                            ID="labPageCount" runat="server"></asp:Label>页 20篇/页 转:
                     
                          <input name="textfield2" type="text" id="textfield2" size="3" />

                    </td>
                </tr>
            </table>
        </FooterTemplate>
    </asp:DataList>



</asp:Content>

