﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;

public partial class searchlist : System.Web.UI.Page
{
    protected PagedDataSource pds = new PagedDataSource();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
             ddldatabind();
             search(0); 
         }

    }

    //执行搜索
    private void search(int currentpage)
    {
        pds.AllowPaging = true;//允许分页
        pds.PageSize =20;//每页显示9条数据
        pds.CurrentPageIndex = currentpage;//当前页为传入的一个int型值
        string sql =Session["SearchSql"].ToString();
        DataTable dtReturn = DbOperation.QueryBySql(sql);
        pds.DataSource = dtReturn.DefaultView;//把数据集中的数据放入分页数据源中
        dlsearch.DataSource =pds;
        ViewState["pageCount"] = pds.PageCount;
        dlsearch.DataBind();
             
    }


    protected void dlsearch_ItemCommand(object source, DataListCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            //以下四个为 捕获用户点击 上一页 下一页等时发生的事件
            case "first":
                ViewState["currPageIndex"] = "0";
                break;
            case "pre":
                ViewState["currPageIndex"] = Convert.ToInt32(ViewState["currPageIndex"]) - 1;
                break;
            case "next":
                ViewState["currPageIndex"] = Convert.ToInt32(ViewState["currPageIndex"]) + 1;
                break;
            case "last":
                ViewState["currPageIndex"] = Convert.ToInt32(ViewState["pageCount"])- 1;
                break;
        }
        pds.CurrentPageIndex = Convert.ToInt32(ViewState["currPageIndex"]);
        search(pds.CurrentPageIndex);

      }

    protected void dlsearch_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Footer)
        {
            //以下六个为得到脚模板中的控件,并创建变量.
            Label CurrentPage = e.Item.FindControl("labCurrentPage") as Label;
            Label PageCount = e.Item.FindControl("labPageCount") as Label;
            LinkButton FirstPage = e.Item.FindControl("LinkButton1") as LinkButton;
            LinkButton PrePage = e.Item.FindControl("LinkButton2") as LinkButton;
            LinkButton NextPage = e.Item.FindControl("LinkButton3") as LinkButton;
            LinkButton LastPage = e.Item.FindControl("LinkButton4") as LinkButton;
            CurrentPage.Text = (pds.CurrentPageIndex + 1).ToString();//绑定显示当前页
            PageCount.Text = pds.PageCount.ToString();//绑定显示总页数
            if (pds.IsFirstPage)//如果是第一页,首页和上一页不能用
            {
                FirstPage.Enabled = false;
                PrePage.Enabled = false;
            }
            if (pds.IsLastPage)//如果是最后一页"下一页"和"尾页"按钮不能用
            {
                NextPage.Enabled = false;
                LastPage.Enabled = false;
            }
        }
    }
    protected void ddlclass_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (ddlclass.SelectedItem.ToString()!= "未指定条件")
        {
            string strdl2 = "select s_id,s_name,classid from tb_sclass where classid=" + ddlclass.SelectedValue;
            DataTable dtReturn = DbOperation.QueryBySql(strdl2);
            ddlsclass.DataSource = dtReturn;
            ddlsclass.DataBind();
        }

    }

    protected void btsubmit_Click(object sender, EventArgs e)
    {
        string strclassid = ddlclass.SelectedValue.ToString();
        string strs_id = ddlsclass.SelectedValue.ToString();
        string strbox = tbox.Text.Trim().Replace("'","''");
        string sql;
        if (tbox.Text.Trim() == "" || tbox.Text.Trim() == "关键字")
        {
            Response.Write("<script>alert('请输入关键字!');location='javascript:history.go(-1)';</script>");
        }
        else
        {
            if (select1.Value == "strtitle")
            {
                if (strs_id != null)
                {
                    sql = "select art_id,classid,s_id,title from tb_article where title like '%" + strbox + "%' and classid='" + strclassid + "' and s_id=" + strs_id;
                }

                if (strs_id == null)
                {
                    sql = "select art_id,classid,s_id,title tb_article from where title like '%" + strbox + "%' and classid='" + strclassid;

                }
                else
                {
                    sql = "select art_id,classid,s_id,title from tb_article  where title like '%" + strbox + "%'";

                }

            }
            else
            {
                if (strs_id != null)
                {
                    sql = "select art_id,classid,s_id,title from tb_article  where WZNR like '%" + strbox + "%' and classid='" + strclassid + "' and s_id=" + strs_id;
                }

                if (strs_id == null)
                {
                    sql = "select art_id,classid,s_id,title from  tb_article  where WZNR like '%" + strbox + "%' and classid='" + strclassid;

                }
                else
                {
                    sql = "select art_id,classid,s_id,title from  tb_article  where WZNR like '%" + strbox + "%'";

                }

            }
            Session["SearchSql"] = sql;
            Response.Redirect("searchlist.aspx");
        }


    }


    private void ddldatabind()
    {
        string sql = "select classid,c_name from tb_class order by classid asc ";
        DataTable dtReturn = DbOperation.QueryBySql(sql);
        ddlclass.DataSource = dtReturn;
        ddlclass.DataBind();
        ListItem item = new ListItem("未指定条件", "未指定条件");
        ddlclass.Items.Insert(0, item);
        ListItem item2 = new ListItem("未指定条件", "未指定条件");
        ddlsclass.Items.Insert(0, item2);
    }

   
}
