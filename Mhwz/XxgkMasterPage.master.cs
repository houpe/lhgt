﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class XxgkMasterPage : System.Web.UI.MasterPage
{
    public string strZpfl = string.Empty;
    public string strZtfl = string.Empty;
    public string strTcfl = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            strZpfl = ClsWzflOperation.GetZpfl("ulZpfl", "xxgkSingle.aspx");
            strZtfl = ClsWzflOperation.GetZtOrTcfl("ulZtfl", "xxgkSingle.aspx", "主题分类");
            strTcfl = ClsWzflOperation.GetZtOrTcfl("ulTcfl", "xxgkSingle.aspx", "体裁分类");
        }
    }

   
}
