﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using System.Text;

public partial class gzhd_ftzb : System.Web.UI.Page
{
    protected PagedDataSource pds = new PagedDataSource();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindDataList(0);
        }

    }

    private void BindDataList(int currentpage)
    {
        pds.AllowPaging = true;//允许分页
        pds.PageSize = 2;//每页显示2条数据
        pds.CurrentPageIndex = currentpage;//当前页为传入的一个int型值

       // string strSql = "select * from tb_article where tu=1 ";
        string strSql = "select * from tb_talklive";
     
        strSql += " order by '开始日期' desc";

        DataTable dtReturn = DbOperation.QueryBySql(strSql);

        pds.DataSource = dtReturn.DefaultView;//把数据集中的数据放入分页数据源中
        dltitle.DataSource = pds;//绑定Datalist
        dltitle.DataBind();
    }


    protected void dltitle_ItemCommand(object source, DataListCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            //以下四个为 捕获用户点击 上一页 下一页等时发生的事件
            case "first":
                ViewState["currPageIndex"] = "0";
                break;
            case "pre":
                ViewState["currPageIndex"] = Convert.ToInt32(ViewState["currPageIndex"]) - 1;
                break;
            case "next":
                ViewState["currPageIndex"] = Convert.ToInt32(ViewState["currPageIndex"]) + 1;
                break;
            case "last":
                ViewState["currPageIndex"] = pds.PageCount - 1;
                break;
        }
        pds.CurrentPageIndex = Convert.ToInt32(ViewState["currPageIndex"]);
        BindDataList(pds.CurrentPageIndex);
    }
    protected void dltitle_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Footer)
        {
            //以下六个为得到脚模板中的控件,并创建变量.
            Label CurrentPage = e.Item.FindControl("labCurrentPage") as Label;
            Label PageCount = e.Item.FindControl("labPageCount") as Label;
            Label countbiaoti = e.Item.FindControl("titlecount") as Label;
            LinkButton FirstPage = e.Item.FindControl("LinkButton1") as LinkButton;
            LinkButton PrePage = e.Item.FindControl("LinkButton2") as LinkButton;
            LinkButton NextPage = e.Item.FindControl("LinkButton3") as LinkButton;
            LinkButton LastPage = e.Item.FindControl("LinkButton4") as LinkButton;
            CurrentPage.Text = (pds.CurrentPageIndex + 1).ToString();//绑定显示当前页
            PageCount.Text = pds.PageCount.ToString();//绑定显示总页数            

            if (pds.IsFirstPage)//如果是第一页,首页和上一页不能用
            {
                FirstPage.Enabled = false;
                PrePage.Enabled = false;
            }
            if (pds.IsLastPage)//如果是最后一页"下一页"和"尾页"按钮不能用
            {
                NextPage.Enabled = false;
                LastPage.Enabled = false;
            }
        }
    }
}
