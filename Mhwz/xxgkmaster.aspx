<%@ Page Language="C#" MasterPageFile="~/XxgkMasterPage.master" AutoEventWireup="true" CodeFile="xxgkmaster.aspx.cs" Inherits="xxgk" Title="南京市国土资源局六合分局-专题列表" %>
<%@ Register Src="UserControl/ucMsgPart.ascx" TagName="ucMsgPart" TagPrefix="uc3" %>
<%@ Register Src="~/UserControl/ucMsgPartBianM.ascx" TagPrefix="uc3" TagName="ucMsgPartBianM" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server" >
    <div id="xxgk_default"  >
        <div style="width: 367px; height: 200px; float: left; text-align: left;" class="table1Copy">
            <uc3:ucmsgpart id="kczy" appearmore="true" title="机构设置" bigclass="1" HideLength="20" recordcount="4" runat="server" />
        </div>
        <div style="width: 367px; height: 200px; margin-left: 10px; float: left; text-align: left;" class="table1Copy">
            <uc3:ucmsgpart id="UcMsgPart1" HideLength="20" appearmore="true" title="国土新闻" bigclass="2" recordcount="4" runat="server" />
        </div>
        <div style="width: 367px; height: 200px; float: left; text-align: left;" class="table1Copy">
            <uc3:ucmsgpart id="UcMsgPart2" appearmore="true" hidelength="20" title="政务公开" bigclass="3" recordcount="4" runat="server" />
        </div>
        <div style="width: 367px; height: 200px; margin-left: 10px; float: left; text-align: left;" class="table1Copy">
            <uc3:ucmsgpart id="UcMsgPart3" HideLength="20" appearmore="true" title="业务公告" bigclass="4" recordcount="4" runat="server" />
        </div>
        <div style="width: 367px; height: 200px; float: left; text-align: left;" class="table1Copy">
            <uc3:ucmsgpart id="UcMsgPart4"  appearmore="true" HideLength="20" title="国土规划" bigclass="5" recordcount="4" runat="server" />
        </div>
        <div style="width: 367px; height: 200px; margin-left: 10px; float: left; text-align: left;" class="table1Copy">
            <uc3:ucmsgpart id="UcMsgPart5"  HideLength="20" appearmore="true" title="办事指南" bigclass="6" recordcount="4" runat="server" />
        </div>
        <div style="width: 367px; height: 200px; float: left; text-align: left;" class="table1Copy">
            <uc3:ucmsgpart id="UcMsgPart6"  HideLength="20" appearmore="true" title="执法监察" bigclass="9" recordcount="4" runat="server" />
        </div>
        <div style="width: 367px; height: 200px; margin-left: 10px; float: left; text-align: left;" class="table1Copy">
            <uc3:ucMsgPartBianM runat="server" ID="ucMsgPartBianM" Url="xxgkXfxxReply.aspx" BigClass="13" SmallClass="50" appearmore="true" title="便民服务"  HideLength="20" recordcount="4"  />
        </div>
    </div>
    
</asp:Content>

