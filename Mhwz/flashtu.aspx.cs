﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;

public partial class flashtu : System.Web.UI.Page
{
    public int Pics_Width = 280;  //宽度
    public int Pics_Height = 222;//高度
    public int Texts_Height = 18;
    public int Swf_Height;
    public string Pics;
    public string Links;
    public string Texts;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string sql = "select * from (select art_id,title,tupian,tu from tb_article where tu=1 order by tjrq desc) a where  rownum<=5 ";
            DataTable dtReturn = DbOperation.QueryBySql(sql);
            string[] imgPics = new string[5];
            string[] imgTexts = new string[5];
            string[] imgLinks = new string[5];
            int i;
            for (i = 0; i < dtReturn.Rows.Count; i++)
            {
                imgPics[i] = Convert.ToString(dtReturn.Rows[i]["tupian"].ToString());
                imgTexts[i] = Convert.ToString(dtReturn.Rows[i]["title"].ToString());
                imgLinks[i] = "xxgkContent.aspx?art_id=" + Convert.ToString(dtReturn.Rows[i]["art_id"].ToString());
                Pics = i == 4 ? Pics = Pics + imgPics[i] : Pics + imgPics[i] + "|";
                Links = i == 4 ? Links = Links + imgLinks[i] : Links + imgLinks[i] + "|";
                Texts = i == 4 ? Texts = Texts + imgTexts[i] : Texts + imgTexts[i] + "|";
            }

            Swf_Height = Pics_Height + Texts_Height;

        }

    }
}

