﻿<%@ Page Title="" Language="C#" MasterPageFile="~/GzhdMasterPage.master" AutoEventWireup="true" CodeFile="visitcontent.aspx.cs" Inherits="visitcontent" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
   <%-- <script src="js/swfobjectopt.js"></script>--%>

    <style type="text/css">
        table, div, body, span, embed {
            margin: 0 auto;
            padding: 0;
        }

        .td1 {
            display: inline-block;
            width: 185px;
            line-height: 25px;
            overflow: hidden;
            height: 30px;
            float: left;
            vertical-align: middle;

            border-right: 1px dotted #808080 ;

        }

            .td1 a {
                line-height: 30px;
                display: block;
                height: 30px;
            }

        .td2 label, .td2 span {
            height: 30px;
            line-height: 30px;
        }

        .td2 {
            display: inline-block;
            width: 550px;
            height: 30px;
            float: left;
            vertical-align: middle;
            overflow: hidden;
            /*border-bottom: 1px dotted #808080;*/
            border-bottom: 1px dotted #808080;
        }

        .td1{
            border-bottom: 1px dotted #808080;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div style="width:100%; height:730px;">
        <div class="td1">访谈名称:</div>
        <div class="td2">
            <asp:Label ID="lb_visitName" runat="server" Text=""></asp:Label>
        </div>
        <div class="td1">
            访谈嘉宾:
        </div>
        <div class="td2">
            <asp:Label ID="lb_visitGuest" runat="server" Text=""></asp:Label>
        </div>
        <div class="td1">访谈主持:</div>
        <div class="td2">
            <asp:Label ID="lb_visitHost" runat="server" Text=""></asp:Label>
        </div>
        <div class="td1" style="height:70px; text-align:center;line-height:70px;">标题图片:</div>
        <div class="td2" style="height: 70px; text-align:center">
            <asp:Image ID="Image1" runat="server" Height="68px" Width="82px"/>
        </div>
        <div class="td1">开始时间:</div>
        <div class="td2">
            <asp:Label ID="lbTime" runat="server" Text=""></asp:Label>
        </div>
        <div class="td1">嘉宾介绍:</div>
        <div class="td2">
            <asp:Label ID="lbGuestIns" runat="server" Text=""></asp:Label>
        </div>
        <div class="td1">访谈说明:</div>
        <div class="td2">
            <asp:Label ID="lbInstruct" runat="server" Text=""></asp:Label>
        </div>
        <div class="td1">当前用户:</div>
        <div class="td2"></div>
        <div class="td1"  style="width:735px; text-align:left; border:""" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 文字实录:</div>
        <div  style="height: 420px; text-align: center;width:735px">

            <embed id="embedswf" src="images/player1.swf" type="application/x-shockwave-flash" style="width:500; height: 400px; margin:0 auto;" quality="1" flashvars="<%=strWzsl%>" loop="true" play="true" scale="showall" allowfullscreen="true"></embed>

        </div>

    </div>

</asp:Content>