﻿/*创 建 人：LiuY
创建时间：2013-3-14
案例说明：获取网站后台配置
function GetConfig(name) {
var str = GetServerData( "SystemConfig", "GetConfig", name,"c");
return str;
}

参数说明：类,方法,参数,命名空间（可选：默认:b:SbBusiness；c:Common）,回调函数（可选）
*/
function GetServerData(clssname, method, parameters, assembly, callback) {
    var _val = "";
    if (!assembly) {
        assembly = "b";
    }
    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: Common.GetServicePath() + "WebService/JqueryService.asmx/AjaxMethod",
        data: '{ asb: "' + assembly + '", cls: "' + clssname + '", mth: "' + method + '", param: "' + parameters + '" }',
        dataType: 'json',
        async: false,
        error: function (message) {
            alert(message.responseText);
        },
        success: function (data) {
            if (!callback || callback == undefined || callback == "") {
                _val = data.d;
            }
            else {
                callback(data.d);
            }
        }
    });
    return _val;
};

function GetServerDataJson(clssname, method, parameters, assembly, callback) {
    var _val = "";
    if (!assembly) {
        assembly = "b";
    }
    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: Common.GetServicePath() + "WebService/JqueryService.asmx/AjaxMethodJson",
        data: '{ asb: "' + assembly + '", cls: "' + clssname + '", mth: "' + method + '", param:"' + parameters + '"}',
        dataType: 'json',
        async: false,
        error: function (message) {
            alert(message.responseText);
        },
        success: function (data) {
            if (!callback || callback == undefined || callback == "") {
                _val = data.d;
            }
            else {
                callback(data.d);
            }
        }
    });
    return _val;
};

//请求service通用方法
function GetServiceData(method, parameters, callback, loadItemId) {
    var _val = "";
    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: Common.GetServicePath() + "WebService/SbxtService.asmx/" + method,
        data: parameters,
        dataType: 'json',
        async: false,
        error: function (message) {
            alert(message.responseText);
        },
        success: function (data) {
            if (!callback || callback == undefined || callback == "") {
                _val = data.d;
            }
            else {
                callback(loadItemId, data.d);
            }
        }
    });
    return _val;
};

//jquery实现对webservice的调用
function WebService(url, callback, pars) {
    $.ajax({
        data: pars,
        url: url,
        type: "POST",
        contentType: "application/json;utf-8",
        dataType: 'json',
        cache: false,
        success: function (json) {
            callback(json.d);
        },
        error: function (xml, status) {
            if (status == 'error') {
                try {
                    var json = eval('(' + xml.responseText + ')');
                    alert(json.Message + '\n' + json.StackTrace);
                } catch (e) { }
            } else {
                alert(status);
            }
        },
        beforeSend: function (xml) {
            if (!pars) xml.setRequestHeader("Content-Type", "application/json;utf-8")
        }
    });
}


//获取dataTable并返回json
function CommonData(clssname, method, parameters) {
    var json = GetServerData(clssname, method, parameters, "c");
    return json;
}

//解析Json格式并构建table数据(行政许可事项列表)
function createCommonTable(strDivId, json) {
    if (json != "") {
        json = json.toJson(); //格式化
        var table = $("<table class='window_tab_list_all'></table>");
        for (var i = 0; i < json.length; i++) {
            o1 = json[i];
            var row = $("<tr></tr>");
            for (key in o1) {
                var td = $("<td></td>");
                td.text(o1[key].toString());
                td.appendTo(row);
            }
            row.appendTo(table);
        }
        table.appendTo($("#" + strDivId));
    }
}

//解析Json格式并构建table数据(行政许可审批公示列表)
function createXzxkTable(strDivId, json) {
    if (json == "") {
        return "";
    }
    json = json.toJson(); //格式化

    var table = $("<table class='window_tab_list' style='width: 100%; border-collapse: collapse' border='0' cellspacing='0'></table>");
    for (var i = 0; i < json.length; i++) {
        o1 = json[i];
        var row = $("<tr style='background-color: #eff8fc' onmouseover=\"this.style.backgroundColor='#b5e0f3'\" onmouseout=\"this.style.backgroundColor='#eff8fc'\"></tr>");
        for (key in o1) {
            if (key != "序号") {
                var td = "";
                if (key == "审图号") {
                    td = $("<td style='width:25%;height:25px;'></td>");
                }
                else if (key == "申请单位") {
                    td = $("<td style='width:25%'></td>");
                }
                else if (key == "图名") {
                    td = $("<td style='width:25%'></td>");
                }
                else if (key == "批准日期") {
                    td = $("<td style='width:25%'></td>");
                }

                if (td != "") {
                    td.text(o1[key].toString());
                    td.appendTo(row);
                }
            }
        }
        row.appendTo(table);
    }
    $("#" + strDivId).html("");
    table.appendTo($("#" + strDivId));
}

//显示信息公示数据
function createXxgsTable(strDivId, json) {
    if (json != "") {
        json = json.toJson(); //格式化
        var table = $("<ul class='page_bar_ul' style='margin-left: 0px; padding-left: 0px;'></ul>");
        for (var i = 0; i < json.length; i++) {
            o1 = json[i];

            var row = $("<li><a href='Wsbs/detail.aspx?id=" + o1["id"].toString() + "'>" + o1["title"].toString() + "</a></li>");
            row.appendTo(table);
        }
        table.appendTo($("#" + strDivId));
    }
}

//显示留言咨询数据
function createLyzxTable(strDivId, json) {
    if (json != "") {
        json = json.toJson(); //格式化
        var ulLyzx = $("#" + strDivId);

        for (var i = 0; i < json.length; i++) {
            o1 = json[i];
            var row = $("<li></li>");
            var td = $("<a href='Wsbs/LyzxMessage.aspx?id=" + o1["id"].toString() + "'>" + o1["title"].toString() + "</a>");
            td.appendTo(row);
            row.appendTo(ulLyzx);
        }
    }
}

//显示常见问题信息
function creatCjwtTable(strDivId, json) {
    if (json != "") {
        json = json.toJson(); //格式化
        var ulLyzx = $("#" + strDivId);

        for (var i = 0; i < json.length; i++) {
            o1 = json[i];
            var row = $("<li></li>");
            var td = $("<a href='Wsbs/detail.aspx?id=" + o1["id"].toString() + "'>" + o1["title"].toString() + "</a>");
            td.appendTo(row);
            row.appendTo(ulLyzx);
        }
    }
}

//显示行政许可事项
function createXzxkInfoTable(strDivId, json) {
    if (json != "") {
        json = json.toJson(); //格式化
        var divContent = $("#" + strDivId);
        var loginUserId = getCookie("login_user");

        for (var i = 0; i < json.length; i++) {
            o1 = json[i];
            var div = $("<div id='div_xksx" + i + "'></div>");

            // 获取标题头和内容信息
            var parms = "{strFlowName:'" + o1["flowname"].toString() + "',strFlowType:'" + o1["flowtype"].toString() + "',strUserId:'" +loginUserId + "'}";

            var strContent = GetServiceData("GetXzxkInfoContent", parms, "", "");

            var table = $("<table style='margin-top: 2px;width:100%' class='page_c_box' border='0' cellspacing='6'>" + strContent + "</table>");

            table.appendTo(div);
            div.appendTo(divContent);
        }
    }
}

//绑定信息公示列表
function BindXxgsList(strType, strSmallType) {
    var _val = GetServiceData("GetInfoType", '{ Type: "' + strType + '", strSmalType: "' + strSmallType + '" }', createXxgsTable, "divXxgg");
    return _val;
}

//绑定留言咨询列表
function BindLyzxList() {
    var _val = GetServiceData("GetLyzx", '{}', createLyzxTable, "ulLyzx");
    return _val;
}

//绑定常见问题列表
function BindCjwtList() {
    var _val = GetServiceData("GetCjwt", '{}', creatCjwtTable, "ulCjwt");
    return _val;
}

//获取行政许可审批公示列表
function BindXzxkList(strWhere) {
    //默认strWhere = '{}'
    if (strWhere == "") {
        strWhere = '{strIID:"",strName:""}';
    }
    var _val = GetServiceData("GetXzxkjgList",strWhere, createXzxkTable, "demo1");
    return _val;
}

//获取行政许可事项
function GetXzxkInfoTitle() {
    var _val = GetServiceData("GetXzxkInfoTitle", '{}', createXzxkInfoTable, "dragdiv3");
    return _val;
}

function BussinessHandle(clssname, method, parameters) {
    var str = GetServerData("Handle." + clssname, method, parameters, "s");
    return str;
}
function BussinessTools(clssname, method, parameters) {
    var str = GetServerData("Tools." + clssname, method, parameters, "s");
    return str;
}

//获取网站配置文件中的信息
function GetConfig(name) {
    var json = GetServerDataJson("ConfigHelper", "GetConfig", "{ configKey:'" + name + "'}", "c");
    return json;
}



//数据字典翻译成中文
function fieldCodeDataRender(r, n, value) {
    for (var i = 0, l = dictionaryCodeData.length; i < l; i++) {
        var o = dictionaryCodeData[i];
        if (o.value == value) {
            return o.text;
        }
    }
    return value;
}

//表格滚动方法
function scrollText() {
    var speed = 50; //数字越大速度越慢
    var tab = document.getElementById("demo");
    var tab1 = document.getElementById("demo1");
    var tab2 = document.getElementById("demo2");
    tab2.innerHTML = tab1.innerHTML; //克隆demo1为demo2
    function Marquee() {
        if (tab2.offsetTop - tab.scrollTop <= 0)//当滚动至demo1与demo2交界时
            tab.scrollTop -= tab1.offsetHeight //demo跳到最顶端
        else {
            tab.scrollTop++
        }
    }
    var MyMar = setInterval(Marquee, speed);
    tab.onmouseover = function () { clearInterval(MyMar) }; //鼠标移上时清除定时器达到滚动停止的目的
    tab.onmouseout = function () { MyMar = setInterval(Marquee, speed) }; //鼠标移开时重设定时器
}

//设置相应html元素的内容
function LoadMsgText(loadItemId, strInfo) {
    if (strInfo == "") {//session过期则退出
        //LoginOut();
    }
    else {
        if (loadItemId.indexOf("|") == -1) {
            $("#" + loadItemId).html(strInfo);
        }
        else {
            var arrIds = loadItemId.split("|");
            for (var i = 0; i < arrIds.length; i++) {
                $("#" + arrIds[i]).html(strInfo);
            }
        }
    }
}

//设置相应html元素的内容
function IsLogining(loadItemId, strInfo) {
    if (strInfo == "") {//session过期则退出
        LoginOut();
    }
}

//注销
function LoginOut() {
    //清除session
    GetServiceData("ClearSession", "{}", ReturnMain, "lblMessage");
}

//返回首页
function ReturnMain(strTemp) {
    var loginPage = getCookie("loginPageUrl");
    window.document.location.href = loginPage;
}

//显示进度条
function ShowLoading() {
    //定时器2秒
    $.messager.progress({
        title: '请稍候',
        msg: '数据加载中...'
        });
    //$("#loading div").animate({ width: "16px" }); //动态加载进度条
}

//隐藏进度条
function HideLoading() {
    $.messager.progress('close');
   // $("#loading").fadeOut();
}
//设置为主页
function SetPageHome() {
    this.style.behavior = 'url(#default#homepage)';
    this.setHomePage('http://shenpi.sbsm.gov.cn');
}

//打开弹出框
function clickUrl(strUrl) {
    window.open(strUrl);
}

//获取当前日期
function GetCurrentDate() {
    var date = new Date();
    this.year = date.getFullYear();
    this.month = date.getMonth() + 1;
    this.date = date.getDate();
    this.day = new Array("星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六")[date.getDay()];
    this.hour = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
    this.minute = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
    this.second = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();
    var currentTime = "今天是:" + this.year + "年" + this.month + "月" + this.date + "日 " + this.hour + ":" + this.minute + ":" + this.second + " " + this.day;
    return currentTime;
}

var request = {
    QueryString: function (val) {
        var uri = window.location.search;
        var re = new RegExp("" + val + "=([^&?]*)", "ig");
        return ((uri.match(re)) ? (uri.match(re)[0].substr(val.length + 1)) : null);
    }
}