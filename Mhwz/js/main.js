﻿//Tab多个切换
//TabTop TabBtm 固定不变
//tabkey /*关键标识*/, me /*本元素*/, num /*tab总个数*/
function setTab(tabkey, me, num, topStyleName1, topStyleName2) {
    var tabkey = tabkey;//关键标识
    var me = me;//本元素
    var num = num;//tab总个数
    var topStyleName1 = topStyleName1;//标题切换区样式名
    var topStyleName2 = topStyleName2;//标题其他样式切换
    for (i = 1; i <= num; i++) {
        document.getElementById(tabkey + "TabTop" + i).className = topStyleName2;
        document.getElementById(tabkey + "TabBtm" + i).style.display = 'none';
    }
    document.getElementById(tabkey + "TabTop" + me).className = topStyleName1;
    document.getElementById(tabkey + "TabBtm" + me).style.display = 'block';
}

//表格滚动方法
function scrollText() {
    var speed = 50; //数字越大速度越慢
    var tab = document.getElementById("demo");
    var tab1 = document.getElementById("demo1");
    var tab2 = document.getElementById("demo2");
    tab2.innerHTML = tab1.innerHTML; //克隆demo1为demo2
    function Marquee() {
        if (tab2.offsetTop - tab.scrollTop <= 0)//当滚动至demo1与demo2交界时
            tab.scrollTop -= tab1.offsetHeight //demo跳到最顶端
        else {
            tab.scrollTop++
        }
    }
    var MyMar = setInterval(Marquee, speed);
    tab.onmouseover = function () { clearInterval(MyMar) }; //鼠标移上时清除定时器达到滚动停止的目的
    tab.onmouseout = function () { MyMar = setInterval(Marquee, speed) }; //鼠标移开时重设定时器
}