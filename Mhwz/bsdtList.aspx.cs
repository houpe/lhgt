﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class bsdtList : System.Web.UI.Page
{
    protected PagedDataSource pds = new PagedDataSource();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //dldatabind(strid); //左边列表
            BindDataList(0);//右边文章标题
        }
    }
    private void BindDataList(int currentpage)
    {
        pds.AllowPaging = true;//允许分页
        pds.PageSize = 20;//每页显示9条数据
        pds.CurrentPageIndex = currentpage;//当前页为传入的一个int型值

        string strSql = "select art_id,classid,s_id,title,to_char(tjrq,'yyyy-mm-dd') as tjrq from tb_article where 1=1 ";
        if (!string.IsNullOrEmpty(Request["flag"]))
        {
            if (Request["flag"].ToString() == "1")
            {
                strSql = "select art_id,classid,s_id,title,to_char(tjrq,'yyyy-mm-dd') as tjrq from tb_article where tjrq > (select trunc(sysdate,'y') FROM DUAL) order by tjrq desc";
            }
        }
        else if (!string.IsNullOrEmpty(Request["type"]) && !string.IsNullOrEmpty(Request["zttfl"]))
        {
            string strFlName = Request["zttfl"].ToString();
            string strFl = System.Web.HttpUtility.UrlDecode(Request["type"].ToString());
            strSql = "select art_id,classid,s_id,title,to_char(tjrq,'yyyy-mm-dd') as tjrq from tb_article where " + strFlName + "='" + strFl + "' order by tjrq desc";

        }
        else
        {

            if (!string.IsNullOrEmpty(Request["classid"]))
            {
                strSql += string.Format(" and classid='{0}'", Request["classid"]);
            }
            if (!string.IsNullOrEmpty(Request["s_id"]))
            {
                strSql += string.Format(" and s_id='{0}'", Request["s_id"]);
            }
            if (!string.IsNullOrEmpty(Request["bsfl"]))
            {
                strSql += string.Format(" and bszn_fl='{0}'", Request["bsfl"]);
            }
            strSql += " order by tjrq desc";

        }
        DataTable dtReturn = DbOperation.QueryBySql(strSql);

        pds.DataSource = dtReturn.DefaultView;//把数据集中的数据放入分页数据源中
        dltitle.DataSource = pds;//绑定Datalist
        dltitle.DataBind();
        ViewState["pageCount"] = pds.PageCount;
        if (!string.IsNullOrEmpty(Request["s_id"]))
        {
            while (Equals(Convert.ToInt32(readcount(Request["s_id"])), 1))  //实现页面跳转
            {
                int i = Convert.ToInt32(dtReturn.Rows[0]["art_id"]);
                Response.Redirect("xxgkContent.aspx?art_id=" + i);
            }
        }
    }


    protected void dltitle_ItemCommand(object source, DataListCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            //以下四个为 捕获用户点击 上一页 下一页等时发生的事件
            case "first":
                ViewState["currPageIndex"] = "0";
                break;
            case "pre":
                ViewState["currPageIndex"] = Convert.ToInt32(ViewState["currPageIndex"]) - 1;
                break;
            case "next":
                ViewState["currPageIndex"] = Convert.ToInt32(ViewState["currPageIndex"]) + 1;
                break;
            case "last":
                ViewState["currPageIndex"] = Convert.ToInt32(ViewState["pageCount"]) - 1;
                break;
        }
        pds.CurrentPageIndex = Convert.ToInt32(ViewState["currPageIndex"]);
        BindDataList(pds.CurrentPageIndex);
    }
    protected void dltitle_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        string str = "";
        try
        {
            str = Request.QueryString["s_id"].ToString();
        }
        catch
        {
        }
        if (e.Item.ItemType == ListItemType.Footer)
        {
            //以下六个为得到脚模板中的控件,并创建变量.
            Label CurrentPage = e.Item.FindControl("labCurrentPage") as Label;
            Label PageCount = e.Item.FindControl("labPageCount") as Label;
            Label countbiaoti = e.Item.FindControl("titlecount") as Label;
            LinkButton FirstPage = e.Item.FindControl("LinkButton1") as LinkButton;
            LinkButton PrePage = e.Item.FindControl("LinkButton2") as LinkButton;
            LinkButton NextPage = e.Item.FindControl("LinkButton3") as LinkButton;
            LinkButton LastPage = e.Item.FindControl("LinkButton4") as LinkButton;
            CurrentPage.Text = (pds.CurrentPageIndex + 1).ToString();//绑定显示当前页
            PageCount.Text = pds.PageCount.ToString();//绑定显示总页数            

            if (pds.IsFirstPage)//如果是第一页,首页和上一页不能用
            {
                FirstPage.Enabled = false;
                PrePage.Enabled = false;
            }
            if (pds.IsLastPage)//如果是最后一页"下一页"和"尾页"按钮不能用
            {
                NextPage.Enabled = false;
                LastPage.Enabled = false;
            }
            countbiaoti.Text = readcount(str);//标题总数

        }
    }

    private string readcount(string str)  //读取标题总数
    {
        string sql = string.Format("select count(*) from tb_article where s_id='{0}'", str);
        string count = DbOperation.GetSingle(sql);
        return count;

    }
}