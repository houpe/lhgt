﻿<%@ Page Language="C#" MasterPageFile="~/BsdtMasterPage.master" AutoEventWireup="true" CodeFile="searchlist.aspx.cs" Inherits="searchlist" Title="南京市国土资源局六合分局-全站检索" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        span table {
            background-color: #d9d9d9;
        }

        span table td {
            background-color: #FFFFFF;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td height="6"></td>
        </tr>
    </table>
    <table width="98%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td align="center" valign="middle">全站搜索：
                <select id="select1" runat="server">
                    <option selected="selected" value="strtitle">标题</option>
                    <option value="strcontent">内容</option>
                </select>
                <asp:DropDownList ID="ddlclass" runat="server" OnSelectedIndexChanged="ddlclass_SelectedIndexChanged" AutoPostBack="true" DataTextField="c_name" DataValueField="classid">
                </asp:DropDownList>
                <asp:DropDownList ID="ddlsclass" runat="server" DataTextField="s_name" DataValueField="s_id">
                </asp:DropDownList><asp:TextBox ID="tbox" runat="server" Text="关键字"></asp:TextBox><asp:Button ID="btsubmit" runat="server" Text="搜索" OnClick="btsubmit_Click" /></td>
        </tr>
    </table>
    <span>
        <asp:DataList ID="dlsearch" runat="server" DataKeyField="art_id" Width="98%" Style="margin-top: 10px;" OnItemCommand="dlsearch_ItemCommand" OnItemDataBound="dlsearch_ItemDataBound">
            <ItemTemplate>
                <table width="80%" border="0" cellpadding="0" cellspacing="1" align="center">
                    <tr>
                        <td align="left" valign="middle">
                            <a href='xxgkContent.aspx?classid=<%#Eval("classid") %>&art_id=<%#Eval("art_id") %>'><span style="font-size: 14px; margin-left: 10px;"><%#((DataListItem)Container).ItemIndex+1%>.<%#CommonOperation.Left(Eval("title").ToString(),50) %></span></a></td>
                    </tr>

                </table>
            </ItemTemplate>
            <FooterTemplate>
                <table width="80%" border="0" cellpadding="0" cellspacing="1" align="center">
                    <tr>
                        <td align="center" valign="middle">
                            <asp:Label ID="labCurrentPage" runat="server"></asp:Label>/
                <asp:Label ID="labPageCount" runat="server"></asp:Label>
                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="first" Font-Underline="False" ForeColor="Black">首页</asp:LinkButton>
                            <asp:LinkButton ID="LinkButton2" runat="server" CommandName="pre" Font-Underline="False" ForeColor="Black">上一页</asp:LinkButton>
                            <asp:LinkButton ID="LinkButton3" runat="server" CommandName="next" Font-Underline="False" ForeColor="Black">下一页</asp:LinkButton>
                            <asp:LinkButton ID="LinkButton4" runat="server" CommandName="last" Font-Underline="False" ForeColor="Black">尾页</asp:LinkButton>
                        </td>
                    </tr>
                </table>
            </FooterTemplate>
        </asp:DataList></span>

</asp:Content>

