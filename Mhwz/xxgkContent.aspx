﻿<%@ Page Language="C#" MasterPageFile="~/XxgkMasterPage.master" AutoEventWireup="true" CodeFile="xxgkContent.aspx.cs" Inherits="xxgkContent" Title="南京市国土资源局六合分局-信息公开内容" %>

<%@ Register Src="~/UserControl/search.ascx" TagName="search" TagPrefix="uc1" %>

<asp:Content ID="header" ContentPlaceHolderID="head" runat="Server">
    <link href="css/main.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div style="width: 100%; background: url(images/index_15.jpg); overflow: hidden;">

        <div style="text-align: left; width: 42px; height: 31px; float: left;">
            <img src="images/index_14.jpg" width="31" height="31" alt="" />
        </div>
        <div style="text-align: left; width: 353px; float: left; vertical-align: middle; background: url(images/index_15.jpg); padding-top: 5px;" class="font3">
            <asp:Label ID="strclassname" runat="server"></asp:Label>
        </div>
        <div style="text-align: right; width: 337px; float: left; vertical-align: middle; padding-top: 5px; font-size: 12px;">
            当前位置：首页 &gt;&nbsp;<asp:Label ID="strclass" runat="server"></asp:Label>
            &gt;
                           
                <asp:Label ID="strsclass" runat="server"></asp:Label>
        </div>
        <div style="text-align: right; width: 10px; float: left;">
            <img src="images/index_17.jpg" width="5" height="31" />
        </div>

    </div>

    <div style="width: 100%; text-align: center; font-size: 12px; overflow:auto;" class="table1Copy">

        <div style="width: 100%; text-align: center;" class="s5x5">

            <div style="height: 60px; text-align: center;" class="news_title">
                <asp:Label ID="lbtitle" runat="server"></asp:Label>
            </div>

            <div style="height: 30px; text-align: center; background-color: #EDEDEB">
                <%--原作者:<asp:Label ID="lbauthor" runat="server"></asp:Label>,--%> 来源:<asp:Label ID="lbfrom" runat="server"></asp:Label>
                <%--, 被阅读<asp:Label ID="lbhit" runat="server"></asp:Label>次--%>  日期:(<asp:Label ID="lbdate" runat="server"></asp:Label>)　【<span onclick="window.print();" style="text-decoration: underline; cursor: pointer">打印</span>】【<a href="" onclick="window.close();">关闭窗口</a>】
           
            </div>

            <div style="text-align: left; width: 100%;" class="news_read" id="divContent">
                <%=strContent %>
            </div>

        </div>

    </div>

     <script type="text/javascript">

        //自动缩小字体
        function autoSizeDiv(objId) {
            var thisEle = $("#" + objId).css("font-size");
            var textFontSize = parseFloat(thisEle, 10);
            var unit = thisEle.slice(-2); //获取单位
            if (textFontSize >= 12) {
                textFontSize -= 2;
            }

            $("#" + objId).css("font-size", textFontSize + unit);

        }
        //autoSizeDiv("divContent");
    </script>

</asp:Content>
