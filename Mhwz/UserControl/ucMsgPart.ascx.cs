﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserControl_ucMsgPart : System.Web.UI.UserControl
{
    //默认展示记录条数
    private int nRecordCount = 5;
    public int RecordCount
    {
        set { nRecordCount = value; }
        get { return nRecordCount; }
    }

    //大类
    private string strBigClass = "";
    public string BigClass
    {
        set { strBigClass = value; }
        get { return strBigClass; }
    }

    //小类
    private string strSmallClass = "";
    public string SmallClass
    {
        set { strSmallClass = value; }
        get { return strSmallClass; }
    }

    //where条件
    private string strWhere = "";
    public string WhereCondition
    {
        set { strWhere = value; }
        get { return strWhere; }
    }

    //是否显示更多
    private bool bAppearMore = true;
    public bool AppearMore
    {
        set { bAppearMore = value; }
        get { return bAppearMore; }
    }

    //显示的标题
    private string strTitle = "";
    public string Title
    {
        set { strTitle = value; }
        get { return strTitle; }
    }

    /// <summary>
    /// 隐藏字符的长度
    /// </summary>
    private int nHideLength = 30;
    public int HideLength
    {
        set { nHideLength = value; }
        get { return nHideLength; }
    }

    ///// <summary>
    ///// 限制行显示字符数量
    ///// </summary>
    //private int LineNum = 30;

    //public int LineNum1
    //{
    //    get { return LineNum; }
    //    set { LineNum = value; }
    //}

    /// <summary>
    /// 判断控件的是办事大厅的界面还是信箱公开的界面,默认是信息公开
    /// </summary>
    private string strUrl = "xxgkContent.aspx";

    public string Url
    {
        get { return strUrl; }
        set { strUrl = value; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DataTable dtSource = BusinessOperation.GetList(nRecordCount, BigClass, SmallClass, strWhere);
            //if (dtSource.Rows.Count > 0)
            //{
            //    for (int i = 0; i < dtSource.Rows.Count; i++)
            //    {
            //        if (dtSource.Rows[i]["title"].ToString().Length > LineNum1)
            //        {
            //            dtSource.Rows[i]["title"] = dtSource.Rows[i]["title"].ToString().Substring(0, LineNum1)+"...";
            //        }
            //    }
            
            //}
            gzdt.DataSource = dtSource.DefaultView;
           gzdt.DataBind();

            divMore.Visible = AppearMore;

            if(!string.IsNullOrEmpty(Title))
            {
                labTitle.Text = Title;
            }
            else
            {
                msgTitle.Visible = false;
            }
        }
    }
}