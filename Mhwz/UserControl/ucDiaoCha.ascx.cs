﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserControl_ucDiaoCha : System.Web.UI.UserControl
{
    float poll1ratio = 0F;
    float poll2ratio = 0F;
    float poll3ratio = 0F;
    float poll4ratio = 0F;

    public string TpId
    {
        get
        {
            return ViewState["tpid"]==null?"":ViewState["tpid"].ToString();
        }
        set
        {
            ViewState["tpid"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            dataread(TpId);
        }
    }

    #region
    /// <summary>
    /// <param name="i">第几个</param>
    /// </summary>
    private void dataread(string strId)   ///第一个调查
    {
        string sql = string.Format("select id,question,da_a,a_count,da_b,b_count,da_c,c_count,da_d,d_count from tb_myddc where id='{0}'", strId);
        DataTable dtReturn = DbOperation.QueryBySql(sql);
        if (dtReturn.Rows.Count != 0)
        {
            lbquestion.Text = dtReturn.Rows[0]["question"].ToString();//问题
            rbt_a.Text = dtReturn.Rows[0]["da_a"].ToString(); //答案a
            int poll1 = Convert.ToInt32(dtReturn.Rows[0]["a_count"]);            //票数
            rbt_b.Text = dtReturn.Rows[0]["da_b"].ToString();  //答案b
            int poll2 = Convert.ToInt32(dtReturn.Rows[0]["b_count"]);             //票数
            rbt_c.Text = dtReturn.Rows[0]["da_c"].ToString();   //答案c
            int poll3 = Convert.ToInt32(dtReturn.Rows[0]["c_count"]);            //票数
            rbt_d.Text = dtReturn.Rows[0]["da_d"].ToString();    //答案d
            int poll4 = Convert.ToInt32(dtReturn.Rows[0]["d_count"]);           //票数
            int pollsum = poll1 + poll2 + poll3 + poll4;

            if (pollsum > 0)
            {
                poll1ratio = Convert.ToSingle((float)poll1 / pollsum);
                lb_a.Text = Math.Round((poll1ratio * 100), 2).ToString() + "%";
                poll2ratio = Convert.ToSingle((float)poll2 / pollsum);
                lb_b.Text = Math.Round((poll2ratio * 100), 2).ToString() + "%";
                poll3ratio = Convert.ToSingle((float)poll3 / pollsum);
                lb_c.Text = Math.Round((poll3ratio * 100), 2).ToString() + "%";
                poll4ratio = Convert.ToSingle((float)poll4 / pollsum);
                lb_d.Text = Math.Round((poll4ratio * 100), 2).ToString() + "%";
                lb_count.Text = pollsum.ToString();

                Image1.Width = (int)(poll1ratio * 100);
                Image2.Width = (int)(poll2ratio * 100);
                Image3.Width = (int)(poll3ratio * 100);
                Image4.Width = (int)(poll4ratio * 100);
                Image1.Height = 15;
                Image2.Height = 15;
                Image3.Height = 15;
                Image4.Height = 15;
            }

        }
        else
        {
            lberror.Visible = true;
            lberror.Text = "暂无内容";
        }
    }

    /// <summary>
    /// 投票
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        if (rbt_a.Checked == true)
        {
            toupiao("a_count", TpId);
        }
        if (rbt_b.Checked == true)
        {
            toupiao("b_count", TpId);
        }
        if (rbt_c.Checked == true)
        {
            toupiao("c_count", TpId);
        }
        if (rbt_d.Checked == true)
        {
            toupiao("d_count", TpId);
        }
    }
    private void toupiao(string da, string strId)  ///第几个
    {
        string sql = string.Format("update tb_myddc set {0}={0}+1 where id='{1}'", da, strId);
        DbOperation.ExecuteSql(sql);
        Response.Write("<script>alert('感谢您的支持!投票成功!');location=location;</script>");
    }

    #endregion
}