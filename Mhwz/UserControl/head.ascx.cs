﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class head : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btsearch_Click(object sender, EventArgs e)
    {
        if (tbsearch.Text.Trim() == "")
        {
            Response.Write("<script>alert('请输入关键字!');location='javascript:history.go(-1)';</script>");

        }
        else
        {
            string strsql = string.Format("select art_id,classid,s_id,title,WZNR from tb_article where WZNR like '%{0}%' or title like '%{0}%'", tbsearch.Text.Trim().ToString().Replace("'", "''"));
            Session["SearchSql"] = strsql;
            Response.Redirect("searchlist.aspx");
        }
    }
}
