﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;

public partial class search : System.Web.UI.UserControl
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
       {
             ddldatabind();  
       }

    }
    protected void ddlclass_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (ddlclass.SelectedItem.ToString() != "未指定条件")
        {
            string sql = "select s_id,s_name,classid from tb_sclass where classid=" + ddlclass.SelectedValue;
            DataTable dtReturn = DbOperation.QueryBySql(sql);
            ddlsclass.DataSource = dtReturn;
            ddlsclass.DataBind();

        }
    }

    protected void btsubmit_Click(object sender, EventArgs e)
    {
        string strclassid=ddlclass.SelectedValue.ToString();
        string strs_id=ddlsclass.SelectedValue.ToString();
        string strbox=tbox.Text.Trim().Replace("'","''");
        string sql;
        if (tbox.Text.Trim() == "" || tbox.Text.Trim() == "关键字")
        {
          Response.Write("<script>alert('请输入关键字!');location='javascript:history.go(-1)';</script>");
        }
        else
        {
            if (select1.Value == "strtitle")
            {
                if (strs_id != null)
                {
                    sql = "select art_id,classid,s_id,title from tb_article where title like '%" + strbox + "%' and classid='" + strclassid + "' and s_id=" + strs_id;
                }
              
                if(strs_id == null)
                {
                    sql = "select art_id,classid,s_id,title tb_article from where title like '%" + strbox + "%' and classid='" + strclassid;

                 }
                else
                {
                  sql = "select art_id,classid,s_id,title from tb_article  where title like '%" + strbox + "%'";

                }
           
             }
            else{
                if (strs_id != null)
                {
                    sql = "select art_id,classid,s_id,title from tb_article  where WZNR like '%" + strbox + "%' and classid='" + strclassid + "' and s_id=" + strs_id;
                }

                if(strs_id == null)
                {
                    sql = "select art_id,classid,s_id,title from  tb_article  where WZNR like '%" + strbox + "%' and classid='" + strclassid;

                }
                else
                {
                    sql = "select art_id,classid,s_id,title from  tb_article  where WZNR like '%" + strbox + "%'";

                 }
               
              }
              Session["SearchSql"] = sql;
              Response.Redirect("searchlist.aspx");
      }
        
      
    }


 private void ddldatabind()
{
    string sql = "select classid,c_name from tb_class order by classid asc ";
    DataTable dtReturn = DbOperation.QueryBySql(sql);
    ddlsclass.DataSource = dtReturn;
    ddlclass.DataBind();

    ListItem item = new ListItem("未指定条件", "未指定条件");
    ddlclass.Items.Insert(0, item);
    ListItem item2 = new ListItem("未指定条件", "未指定条件");
    ddlsclass.Items.Insert(0, item2);
}
}
