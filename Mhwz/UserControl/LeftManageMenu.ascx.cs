﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserControl_LeftManageMenu : System.Web.UI.UserControl
{
    public string strMenuWhere;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["AdminName"] == null)
        {
            Response.Write("<script>window.location.href='login-admin.aspx';</script>");
        }
        else
        {
            string userName = Session["AdminName"].ToString();
            string strSql = string.Format("select replace( wmsys.wm_concat(b.菜单权限),',','|')from tb_powermanage b where exists (select * from table (select split(t.用户)   from TB_POWERMANAGE t  where t.id = b.id ) where column_value = '{0}')", userName);

            strMenuWhere = DbOperation.GetSingle(strSql);
        }
    }
}