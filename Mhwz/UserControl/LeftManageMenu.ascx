﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LeftManageMenu.ascx.cs" Inherits="UserControl_LeftManageMenu" %>
<script src="../js/Jquery.Global.js"></script>
<script src="../js/Jquery.Common.js"></script>
<script type="text/javascript">
    var serviceUrl = Common.GetLocalPath() + "<%=ConfigurationManager.AppSettings["moaServiceUrl"]%>"; 
    var strServicePath = serviceUrl + "/GetSecondLeftMenu";
    var PathByUser = serviceUrl + "/GetMenuByUser";
    var roleByUser = serviceUrl+"/GetRoleByUser";
    var user = '<%=Session["AdminName"] %>';
    var StrMenuWhere = '<%=strMenuWhere %>';
    //页面加载时加载菜单栏目
    $(function () {
        LoadMenuData();
    })

    //加载菜单栏目数据
    function LoadMenuData() {
        WebService(roleByUser, BackRole, '{"user":"' + user + '"}');
    }
    var adminRole;//存储是否是管理员角色
    function BackRole(result) {
    
        if (result != null) {
            adminRole = result;
        }
        WebService(PathByUser, BackFirstMenu, '{"user":"' + user + '","menuClassID":"' + StrMenuWhere + '"}');
    }


    var objPub;
    var trid;
    var getFMenuByuser = null;//存储通过用户查找到的所有一级菜单
    //返回第一级菜单
    function BackFirstMenu(result) {
        getFMenuByuser = result;
        WebService(PathByUser, BackAllFirstMenu, '{"user":"admin","menuClassID":""}');
    }

    var arrpwer;//临时存储通过用户查找的所有存单
    var arrMenuAll ;//存储所有第一级目录
    function BackAllFirstMenu(resultAll) {
        arrpwer = new Array()
        arrMenuAll = new Array();
        if (resultAll != null && resultAll != "") {
            var menuAll = $.parseJSON(resultAll);
            $(menuAll).each(function (index, obj) {
                var power = obj.菜单权限;
                arrMenuAll = power.split(',');
            })
            trid = "";
            objPub = "";
            if (getFMenuByuser != null && getFMenuByuser != "") {
                resultmenu = "";
                $("#tableMenu").html("");
                var menu = $.parseJSON(getFMenuByuser);
                $(menu).each(function (index, obj) {
                    var power = obj.菜单权限;
                    arrpower = power.split(',');
                })

                for (var i = 0; i < arrpower.length; i++) {

                    $("#tableMenu").append("<tr id='tr" + i + "_1' style='cursor:hand;background: url(../Images/left_tt.gif) no-repeat;height:25px' align='center'><td id='td_" + i + "'>" + arrpower[i] + "</td></tr>");

                }

                var trlength = $("#tableMenu tr").length;
                for (var i = 0; i < trlength; i++) {

                    $("#tr" + i + "_1").bind("click", function (index, obj) {
                        objPub = $(this).attr("id");
                        trid = objPub.substring(0, objPub.length - 1) + "2";
                        if ($("." + trid).is(':visible')) {
                            $("." + trid).remove();
                        }
                        else {

                            WebService(strServicePath, BackSecondMenu, '{"con":"' + StrMenuWhere + '","menuName":"' + $(this).find("td").text() + '","role":"' + adminRole + '"}');

                        }

                    });
                }
                $("#tr0_1").trigger("click");
            }
        }
    }

    //返回二级菜单
    function BackSecondMenu(result) {
        if (result != null && result != "") {
            var secondMeun = $.parseJSON(result);
            if (adminRole == "1") {
                $(secondMeun).each(function (index, obj) {
                    $("#" + objPub + "").after('<tr class="' + trid + '" style="background:url(\'../images/closed.gif\') no-repeat ;background-position:10% 50%;height:25px;" align="center"><td><a  href="javascript:addTab(\'' + obj.navurl + '\',\'' + obj.menuname + '\')"> ' + obj.menuname + '</a></td></tr>');

                })
            }
            else {
                //通过一级菜单返回的二级菜单和权限表中比较
                $(secondMeun).each(function (index, obj) {
                    $("#" + objPub + "").after('<tr class="' + trid + '" style="background:url(\'../images/closed.gif\') no-repeat ;background-position:10% 50%;height:25px;" align="center"><td><a  href="javascript:addTab(\'' + obj.navurl + '\',\'' + obj.menuname + '\')"> ' + obj.menuname + '</a></td></tr>');
                })
            }
        }
       
    }

    function showsubmenu(sid) {
        whichEl = eval("submenu" + sid);
        if (whichEl.style.display == "none") {
            eval("submenu" + sid + ".style.display=\"\";");
        }
        else {
            eval("submenu" + sid + ".style.display=\"none\";");
        }
    }

    //子菜单点击
    function addTab(strUrl, strTitle) {
        var varTemp = $("#divMain").tabs("exists", strTitle);
        if (varTemp == false) {//如果选项卡不存在
            $('#divMain').tabs('add', {
                title: strTitle,
                content: '<iframe src="' + strUrl + '" width="100%" height="100%" frameboder="0" id="' + strTitle + '"></iframe>',
                iconCls: 'icon-save',
                closable: true
            });

            //为子框架添加加载事件
            $("#" + strTitle).load(function () {
                HideLoading();
            });
        }
        else {//如果已经存在则选中对应的选项卡并刷新
            if (strUrl.indexOf("?") > 0) {
                strUrl += "&rdm=" + Math.random();
            }
            else {
                strUrl += "?rdm=" + Math.random();
            }
            $("#" + strTitle + "").attr("src", strUrl);
            $('#divMain').tabs('select', strTitle);

        }
    }
</script>

<div id="MenuContent" >
    <table style="width: 100%; background: url('../Images/tablemde.jpg');" cellpadding="0" cellspacing="0" id='tableMenu'>
       
    </table>

</div>
