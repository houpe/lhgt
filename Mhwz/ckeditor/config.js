﻿/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function (config) {
    config.allowedContent = true;
    config.format_p = { element: 'p', attributes: { 'class': 'normalPara' } };
    //// config.forcePasteAsPlainText = false;
    config.pasteFromWordKeepsStructure = true;
    config.pasteFromWordIgnoreFontFace = false;
    config.pasteFromWordRemoveStyle = false;
    config.pasteFromWordRemoveFontStyles = false;

    config.removeDialogTabs = 'image:advanced;image:Link';
    config.filebrowserBrowseUrl ='/ckfinder/ckfinder.html';
    config.filebrowserImageBrowseUrl ='/ckfinder/ckfinder.html?Type=Images'; 
    filebrowserFlashBrowseUrl ='/ckfinder/ckfinder.html?Type=Flash'; 
    config.filebrowserUploadUrl = '../HandlerFile.ashx';
    config.filebrowserImageUploadUrl = '/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Images'; config.filebrowserFlashUploadUrl = '/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Flash'; config.filebrowserWindowWidth = '800';
    config.filebrowserWindowHeight = '500';
    //需要将ckeditor文件夹剪切到manage文件夹内,.filebrowserImageUploadUrl后的路径不动;另需将articleadd.aspx,articleedit.aspx,userarticleadd.aspx,userarticleeidt.aspx中的引入ckeditor文件路径改为"./"开头。
    config.filebrowserImageUploadUrl = "../HandlerImage.ashx";
    config.toolbarCanCollapse = true;
    config.toolbar = [
		{ name: 'document', items: ['Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates'] },
		{ name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'] },
		{ name: 'editing', items: ['Find', 'Replace', '-', 'SelectAll', '-', 'Scayt'] },
        { name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'] },
		'/',
		{ name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language'] },
		{ name: 'links', items: ['Link', 'Unlink', 'Anchor'] },
		{ name: 'insert', items: ['Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe'] },
		'/',
		{ name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize'] },
		{ name: 'colors', items: ['TextColor', 'BGColor'] },
		{ name: 'tools', items: ['Maximize', 'ShowBlocks'] },
		{ name: 'about', items: ['About'] }
    ];
    config.font_names = '宋体/宋体;黑体/黑体;仿宋/仿宋_GB2312;楷体/楷体_GB2312;隶书/隶书;幼圆/幼圆;微软雅黑/微软雅黑;' + config.font_names;



  
};
