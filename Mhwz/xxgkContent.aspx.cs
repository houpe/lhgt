﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;

public partial class xxgkContent : System.Web.UI.Page
{
   
    public string strContent = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
                AppearBiaoTi();
                AddClickCount();//浏览次数
                strread();//内容
           
        }
    }

    private void AppearBiaoTi()   //显示一、二级导航标题
    {
        string sql = string.Format("select b.s_id,s_name,c_name from tb_class a,tb_sclass b,tb_article c where a.classid=b.classid and a.classid=c.classid and b.s_id=c.s_id and art_id='{0}'", Request["art_id"]);
        DataTable dtReturn = DbOperation.QueryBySql(sql);

        if (dtReturn.Rows.Count > 0)
        {
            strclass.Text = dtReturn.Rows[0]["c_name"].ToString();
            strsclass.Text = dtReturn.Rows[0]["s_name"].ToString();
            strclassname.Text = dtReturn.Rows[0]["s_name"].ToString();
        }
        else
        {
            strclass.Text = "暂无栏目";
            strsclass.Text = "暂无栏目";
            strclassname.Text = "暂无栏目";
        }
;
    }
    
   private void strread()  //读取内容
   {
       string sql = string.Format("select art_id,title,author,WZNR,wfrom,hits,to_char(tjrq,'yyyy-mm-dd') as tjrq from tb_article where art_id='{0}'", Request["art_id"]);
       DataTable dtReturn = DbOperation.QueryBySql(sql);

       if (dtReturn.Rows.Count > 0)
       {
           lbtitle.Text = dtReturn.Rows[0]["title"].ToString();
          // lbauthor.Text = dtReturn.Rows[0]["author"].ToString();
           strContent = dtReturn.Rows[0]["WZNR"].ToString();
           lbfrom.Text = dtReturn.Rows[0]["wfrom"].ToString();
           //lbhit.Text = dtReturn.Rows[0]["hits"].ToString();
           lbdate.Text = dtReturn.Rows[0]["tjrq"].ToString();

           //if (!string.IsNullOrEmpty(strContent))
           //{
           //    strContent = Server.HtmlDecode(strContent);
           //}
       }
       else
       {
           strclass.Text = "暂无栏目";
       }
       
   }

    private void AddClickCount()  //浏览次数
    {
        string strUserHostIp = Session["userhostip"]==null ? "" : Session["userhostip"].ToString();

        string strArtId = Request["art_id"];
        if (strUserHostIp != Request.UserHostAddress || Session["RequestId"] != strArtId)
        {
            string sql = string.Format("update tb_article set hits=hits+1 where art_id='{0}'",strArtId);
            DbOperation.ExecuteSql(sql);
            Session["userhostip"] = Request.UserHostAddress;
            Session["RequestId"] = strArtId;
        }
    }

 

}
