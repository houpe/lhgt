﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class BsdtMasterPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            AppearBiaoTi();   //二级导航标题
        }
    }

    private void AppearBiaoTi()   //显示一、二级导航标题
    {
        string strSql = "select s_id,s_name,c_name from tb_class a,tb_sclass b where a.classid=b.classid";

        if (!string.IsNullOrEmpty(Request["classid"]))
        {
            strSql += string.Format(" and b.classid='{0}'", Request["classid"]);
        }

        bool bHavaSmallClass = false;
        if (!string.IsNullOrEmpty(Request["s_id"]))
        {
            strSql += string.Format(" and s_id='{0}'", Request["s_id"]);
            bHavaSmallClass = true;
        }

        DataTable dtReturn = DbOperation.QueryBySql(strSql);
        if (dtReturn.Rows.Count > 0)
        {
            strclass.Text = dtReturn.Rows[0]["c_name"].ToString();

            if (bHavaSmallClass)
            {
                strsclass.Text = dtReturn.Rows[0]["s_name"].ToString();
                strclassname.Text = dtReturn.Rows[0]["s_name"].ToString();
            }
            else
            {
                strclassname.Text = dtReturn.Rows[0]["c_name"].ToString();
            }
        }
        else
        {
            strclass.Text = "暂无栏目";
            strsclass.Text = "暂无栏目";
            strclassname.Text = "暂无栏目";
        }

    }

    private string readcount(string str)  //读取标题总数
    {
        string sql = "select count(*) from tb_article where s_id=" + str;
        string count = DbOperation.GetSingle(sql);
        return count;

    }
}
