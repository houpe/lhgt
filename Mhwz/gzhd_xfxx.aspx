﻿<%@ Page Title="" Language="C#" MasterPageFile="~/GzhdMasterPage.master" AutoEventWireup="true" CodeFile="gzhd_xfxx.aspx.cs" Inherits="gzhd_xfxx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="js/Jquery.Global.js"></script>
    <script src="js/jquery.js"></script>
    <script src="js/Jquery.Common.js"></script>

    <script type="text/javascript">
        $(function () {
            if (request.QueryString('tag') == "-1") {
                $("#iframepage").attr('src', "<%=ConfigurationManager.AppSettings["webPageUrl"] %>?iid=" + request.QueryString('iid') + "&FormName=<%=ConfigurationManager.AppSettings["lingDaoReplyAppearAdress"]%>");
            }
            else {
                var strServicePath = Common.GetLocalPath() + '<%=ConfigurationManager.AppSettings["webcMaxiid"]%>/GetNewIID';
                WebService(strServicePath, GetIIDValue, '{}');
                //加载办事大厅下的领导信箱、信访信箱、政务咨询三个页
                function GetIIDValue(result) {
                    var strType = request.QueryString('tag');
                    if (strType == "0") {
                        $("#iframepage").attr('src', "<%=ConfigurationManager.AppSettings["webPageUrl"] %>?iid=" + result + "&readonly=false&UserId=-1&input_index=0&FormName=<%=ConfigurationManager.AppSettings["ldxx"]%>");
                    }
                    else if (strType == "1") {
                        $("#iframepage").attr('src', "<%=ConfigurationManager.AppSettings["webPageUrl"] %>?iid=" + result + "&readonly=false&UserId=-1&input_index=0&FormName=<%=ConfigurationManager.AppSettings["xfxx"]%>");
                    }
                    else if (strType == "2") {
                        $("#iframepage").attr('src', "<%=ConfigurationManager.AppSettings["webPageUrl"] %>?iid=" + result + "&readonly=false&UserId=-1&input_index=0&FormName=<%=ConfigurationManager.AppSettings["zwzx"]%>");
                    }
                }
            }
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <iframe src="#" marginheight="0" marginwidth="0" frameborder="0" scrolling="no" width="765" height="100%" id="iframepage" name="iframepage" onload="iFrameHeight(this)"></iframe>
    <script type="text/javascript" language="javascript">
        
            function iFrameHeight(obj) {

                var ifm = document.getElementById("iframepage");

                var subWeb = document.frames ? document.frames["iframepage"].document : ifm.contentDocument;

                if (ifm != null && subWeb != null) {

                    if (subWeb.body.scrollHeight > 530) {
                        ifm.height = subWeb.body.scrollHeight;
                    }
                    else {
                        ifm.height = 530;
                    }
                }
            }
        
    </script>
</asp:Content>

