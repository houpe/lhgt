﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BsdtMasterPage.master" AutoEventWireup="true" CodeFile="bsdtList.aspx.cs" Inherits="bsdtList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
      <div style="width: 100%; margin-top: 0px; text-align: center; font-size: 12px; overflow: hidden;" class="table1Copy">

        <asp:DataList ID="dltitle" runat="server" Width="95%" DataKeyField="art_id" OnItemCommand="dltitle_ItemCommand" OnItemDataBound="dltitle_ItemDataBound" >
            <ItemTemplate>
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="s5x5">
                    <tr>
                        <td width="84%" height="30" align="left" valign="middle" class="xuxian"><a href='bsdzContent.aspx?classid=<%#Eval("classid")%>&art_id=<%#Eval("art_id") %>' target="_self" class="f14">·<%#CommonOperation.Left(Eval("title").ToString(),40) %></a></td>
                        <td width="16%" height="30" align="left" valign="middle" class="xuxian"><span class="font2"><%# Eval("tjrq")%></span></td>
                    </tr>
                </table>
            </ItemTemplate>
            <FooterTemplate>
                <table style="text-align: center; width: 100%;">
                    <tr>
                        <td height="50" colspan="2" align="center" valign="middle">共<asp:Label ID="titlecount" runat="server"></asp:Label>篇
                        <asp:LinkButton ID="LinkButton1" runat="server" CommandName="first">首页</asp:LinkButton><asp:LinkButton ID="LinkButton2"
                            runat="server" CommandName="pre">上一页</asp:LinkButton>
                            <asp:LinkButton ID="LinkButton3" runat="server" CommandName="next">下一页</asp:LinkButton><asp:LinkButton
                                ID="LinkButton4" runat="server" CommandName="last">尾页</asp:LinkButton>
                            页次:<asp:Label ID="labCurrentPage" runat="server"></asp:Label>/<asp:Label
                                ID="labPageCount" runat="server"></asp:Label>页 20篇/页 转:
                     
                          <input name="textfield2" type="text" id="textfield2" size="3" />

                        </td>
                    </tr>
                </table>
            </FooterTemplate>
        </asp:DataList>

    </div>

</asp:Content>

