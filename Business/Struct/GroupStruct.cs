﻿ 
// 创建人  ：LinJian
// 创建时间：2007年5月23日
using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Struct
{
    /// <!--
    /// 功能描述  : 存放组信息
    /// 创建人  : LinJian
    /// 创建时间: 2007年5月23日
    /// -->
    public struct GroupStruct
    {
        private string strGroupId;
        /// <summary>
        /// 组id
        /// </summary>
        public string GroupId
        {
            get
            {
                return strGroupId;
            }
            set
            {
                strGroupId = value;
            }
        }

        private string strGroupName;
        /// <summary>
        /// 组名
        /// </summary>
        public string GroupName
        {
            get
            {
                return strGroupName;
            }
            set
            {
                strGroupName = value;
            }
        }
        private string strOrderNum;
        /// <summary>
        /// 排序字段
        /// </summary>
        public string OrderNum
        {
            get
            {
                return strOrderNum;
            }
            set
            {
                strOrderNum = value;
            }
        }
        private string _strIsStop;
        /// <summary>
        /// 是否可中止
        /// </summary>
        public string IS_STOP
        {
            get
            {
                return _strIsStop;
            }
            set
            {
                _strIsStop = value;
            }
        }
    }
}
