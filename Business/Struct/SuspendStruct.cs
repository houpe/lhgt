using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Struct
{
    /// <summary>
    /// 挂起审批
    /// </summary>
    public struct SuspendStruct
    {
        private string StrFlowname;

        /// <summary>
        /// 流程名
        /// </summary>
        public string Flowname
        {
            get
            {
                return StrFlowname;
            }
            set
            {
                StrFlowname = value;
            }
        }
        private string StrFlowno;
        /// <summary>
        /// 
        /// 流程号
        /// </summary>
        public string Flowno
        {
            get 
            {
                return StrFlowno;
            }
            set
            {
                StrFlowno = value;
            }
        }
            
    }
}
