﻿ 
// 创建人  ：LinJian
// 创建时间：2007年5月23日
using System;
using System.Collections.Generic;
using System.Text;
using WF_DataAccess;

namespace Business.Struct
{
    /// <!--
    /// 功能描述  : 存放用户信息
    /// 创建人  : LinJian
    /// 创建时间: 2007年5月23日
    /// -->
    public struct UserStruct
    {
        private string strUserName;
        /// <summary>
        /// 用户名（唯一标识符）
        /// </summary>
        public string UserName
        {
            get
            {
                return strUserName;
            }
            set
            {
                strUserName = value;
            }
        }

        private string strUserRealName;
        /// <summary>
        /// 用户真实名字
        /// </summary>
        public string UserRealName
        {
            get
            {
                return strUserRealName;
            }
            set
            {
                strUserRealName = value;
            }
        }

        private string strUserPass;
        /// <summary>
        /// 用户密码
        /// </summary>
        public string UserPass
        {
            get
            {
                return strUserPass;
            }
            set
            {
                strUserPass = value;
            }
        }

        private string strUserId;
        /// <summary>
        /// 用户id
        /// </summary>
        public string UserId
        {
            get
            {
                return strUserId;
            }
            set
            {
                strUserId = value;
            }
        }

        private string strDepartmentId;
        /// <summary>
        /// 用户所属部门ID
        /// </summary>
        public string UserDepartmentId
        {
            get
            {
                return strDepartmentId;
            }
            set
            {
                strDepartmentId = value;
            }
        }
        private string strMobile;
        /// <summary>
        /// 用户所属部门ID
        /// </summary>
        public string UserMobile
        {
            get
            {
                return strMobile;
            }
            set
            {
                strMobile = value;
            }
        }
        private string strOrderbyid;
        /// <summary>
        /// 用户显示的排列顺序
        /// </summary>
        public string UserOderbyid
        {
            get
            {
                return strOrderbyid;
            }
            set
            {
                strOrderbyid = value;
            }
        }
        private string strInvalid;
        /// <summary>
        /// 用户显示的排列顺序
        /// </summary>
        public string UserInvalid
        {
            get
            {
                return strInvalid;
            }
            set
            {
                strInvalid = value;
            }
        }
        private string strDepart_Name;
        /// <summary>
        /// 用户显示的部门名称
        /// </summary>
        public string UserDepart_Name
        {
            get
            {
                return strDepart_Name;
            }
            set
            {
                strDepart_Name = value;
            }
        }
        private string strParent_Id;
        /// <summary>
        /// 用户显示的单位编号
        /// </summary>
        public string UserParent_Id
        {
            get
            {
                return strParent_Id;
            }
            set
            {
                strParent_Id = value;
            }
        }
        private string[] strDepartId;
        /// <summary>
        /// 用户显示的部门编号
        /// </summary>
        public string[] UserDepartId
        {
            get
            {
                return strDepartId;
            }
            set
            {
                strDepartId = value;
            }
        }
    

    }
}
