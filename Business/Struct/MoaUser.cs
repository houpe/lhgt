﻿ 
// 创建人  ：cd
// 创建时间：2010-04-14
using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq.Mapping;

namespace Business.Struct
{
    /// <summary>
    /// 数据库表“ST_USER”的ALinq映射类 User 。
    /// </summary>
    [Table(Name = "ST_USER")]
    public class MoaUser
    {
        //private string strTemp;

        /// <summary>
        /// 是否可用，0或Y为不可用。
        /// </summary>
        [Column(Name = "INVALID", UpdateCheck = UpdateCheck.Never)]
        //public string Invalid { get; set; }//南京市局为字符型
        //public string Invalid
        //{
        //    get { return strTemp; }
        //    set { strTemp = value.ToString(); }
        //}
        public decimal Invalid { get; set; }

        [Column(Name = "USERID", UpdateCheck = UpdateCheck.Never)]
        public string Userid { get; set; }

        [Column(Name = "LOGIN_NAME", UpdateCheck = UpdateCheck.Never)]
        public string Login_Name { get; set; }

        [Column(Name = "USER_NAME", UpdateCheck = UpdateCheck.Never)]
        public string User_Name { get; set; }

        [Column(Name = "PASSWORD", UpdateCheck = UpdateCheck.Never)]
        public string Password { get; set; }

        [Column(Name = "MEMO", UpdateCheck = UpdateCheck.Never)]
        public string Memo { get; set; }
        
        [Column(Name = "ORDERBYID", UpdateCheck = UpdateCheck.Never)]
        public decimal Orderbyid { get; set; }

        [Column(Name = "MOBILE", UpdateCheck = UpdateCheck.Never)]
        public string Mobile { get; set; }

        [Column(Name = "TEL", UpdateCheck = UpdateCheck.Never)]
        public string Tel { get; set; }

        [Column(Name = "MOBILEBAK", UpdateCheck = UpdateCheck.Never)]
        public string Mobilebak { get; set; }

        //[Column(Name = "OPERATION_FLAG", UpdateCheck = UpdateCheck.Never)]
        //public decimal Operation_Flag { get; set; }

        [Column(Name = "OFFICETEL", UpdateCheck = UpdateCheck.Never)]
        public string Officetel { get; set; }

        [Column(Name = "EXTNO", UpdateCheck = UpdateCheck.Never)]
        public string Extno { get; set; }

        [Column(Name = "HOUSETEL", UpdateCheck = UpdateCheck.Never)]
        public string Housetel { get; set; }

        //[Column(Name = "OA_USE", UpdateCheck = UpdateCheck.Never)]
        //public decimal Oa_Use { get; set; }

        //[Column(Name = "WHITEANT_USE", UpdateCheck = UpdateCheck.Never)]
        //public decimal Whiteant_Use { get; set; }

        [Column(Name = "DEPARTID", UpdateCheck = UpdateCheck.Never)]
        public string Departid { get; set; }

        //[Column(Name = "USERNUMBER", UpdateCheck = UpdateCheck.Never)]
        //public decimal Usernumber { get; set; }

        [Column(Name = "USERNAME", UpdateCheck = UpdateCheck.Never)]
        public string Username { get; set; }

        [Column(Name = "NOTES", UpdateCheck = UpdateCheck.Never)]
        public string Notes { get; set; }

        [Column(Name = "TYPE", UpdateCheck = UpdateCheck.Never)]
        public string Type { get; set; }

        [Column(Name = "USERIP", UpdateCheck = UpdateCheck.Never)]
        public string Userip { get; set; }

        [Column(Name = "IS_LEADER", UpdateCheck = UpdateCheck.Never)]
        public string Is_Leader { get; set; }

        [Column(Name = "CTN_CODE", UpdateCheck = UpdateCheck.Never)]
        public decimal Ctn_Code { get; set; }

        ///// <summary>
        ///// 用户Id。
        ///// </summary>
        //[Column(Name = "USERID", IsPrimaryKey = true)]
        //public string Id { get; set; }

        ///// <summary>
        ///// 登陆名。
        ///// </summary>
        //[Column(Name = "LOGIN_NAME",UpdateCheck = UpdateCheck.Never)]
        //public string Login_Name { get; set; }

        ///// <summary>
        ///// 用户名称。
        ///// </summary>
        //[Column(Name = "USER_NAME",UpdateCheck = UpdateCheck.Never)]
        //public string User_Name { get; set; }

        ///// <summary>
        ///// 密码。
        ///// </summary>
        //[Column(Name = "PASSWORD",UpdateCheck = UpdateCheck.Never)]
        //public string Password { get; set; }

        ///// <summary>
        ///// 备注。
        ///// </summary>
        //[Column(Name = "MEMO",UpdateCheck = UpdateCheck.Never)]
        //public string Memo { get; set; }

        ///// <summary>
        ///// 排序。
        ///// </summary>
        //[Column(Name = "ORDERBYID",UpdateCheck = UpdateCheck.Never)]
        //public decimal Orderbyid { get; set; }

        ///// <summary>
        ///// 手机号。
        ///// </summary>
        //[Column(Name = "MOBILE",UpdateCheck = UpdateCheck.Never)]
        //public string Mobile { get; set; }

        ///// <summary>
        ///// 电话。
        ///// </summary>
        //[Column(Name = "TEL",UpdateCheck = UpdateCheck.Never)]
        //public string Tel { get; set; }

        ///// <summary>
        ///// 备用手机。
        ///// </summary>
        //[Column(Name = "MOBILEBAK",UpdateCheck = UpdateCheck.Never)]
        //public string Mobilebak { get; set; }

        ///// <summary>
        ///// 用户所属行政区。
        ///// </summary>
        //[Column(Name = "CTN_CODE",UpdateCheck = UpdateCheck.Never)]
        //public decimal Ctn_Code { get; set; }

        ///// <summary>
        ///// 是否局领导。
        ///// </summary>
        //[Column(Name = "IS_LEADER",UpdateCheck = UpdateCheck.Never)]
        //public string Is_Leader { get; set; }

        ///// <summary>
        ///// 。
        ///// </summary>
        //[Column(Name = "S", UpdateCheck = UpdateCheck.Never)]
        //public string S { get; set; }

        ///// <summary>
        ///// 部门ID。
        ///// </summary>
        //[Column(Name = "DEPARTID", UpdateCheck = UpdateCheck.Never)]
        //public string Dept_Id { get; set; }

        ///// <summary>
        ///// 签名
        ///// </summary>
        //public string Sign { get; set; }

        ///// <summary>
        ///// 密码输入错误时间
        ///// </summary>
        //[Column(Name = "LAST_TIME", UpdateCheck = UpdateCheck.Never)]
        //public Nullable<DateTime> Last_Time { get; set; }

        ///// <summary>
        ///// 密码错误次数
        ///// </summary>
        //[Column(Name = "NUM", UpdateCheck = UpdateCheck.Never)]
        //public decimal Num { get; set; }

    }
}
