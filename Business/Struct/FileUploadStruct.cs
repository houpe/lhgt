﻿ 
// 创建人  ：LinJian
// 创建时间：2007-07-09
using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Struct
{
    /// <!--
    /// 功能描述  : 文件信息
    /// 创建人  : LinJian
    /// 创建时间: 2007-07-09
    /// -->
    public struct FileUploadStruct
    {
        #region 属性

        private string _keyId;
        /// <summary>
        /// 主键ID
        /// </summary>
        public string KeyId
        {
            set
            {
                _keyId = value;
            }
            get
            {
                return _keyId;
            }
        }

        private string _keyIdValue;
        /// <summary>
        /// 主键ID的值
        /// </summary>
        public string KeyIdValue
        {
            set
            {
                _keyIdValue = value;
            }
            get
            {
                return _keyIdValue;
            }
        }

        private string _tableName;
        /// <summary>
        /// 存放的表名
        /// </summary>
        public string TableName
        {
            set
            {
                _tableName = value;
            }
            get
            {
                return _tableName;
            }
        }

        private string _fileStoreFieldName;
        /// <summary>
        /// 文件存储的字段名
        /// </summary>
        public string FileStoreFieldName
        {
            set
            {
                _fileStoreFieldName = value;
            }
            get
            {
                return _fileStoreFieldName;
            }
        }

        private string _fileTypeFieldName;
        /// <summary>
        /// 文件类型存储的字段名
        /// </summary>
        public string FileTypeFieldName
        {
            set
            {
                _fileTypeFieldName = value;
            }
            get
            {
                return _fileTypeFieldName;
            }
        }

        private string _fileTypeFieldValue;
        /// <summary>
        /// 文件类型存储的字段值
        /// </summary>
        public string FileTypeFieldValue
        {
            set
            {
                _fileTypeFieldValue = value;
            }
            get
            {
                return _fileTypeFieldValue;
            }
        }

        private string _storeKeyFieldName;
        /// <summary>
        /// 存储的关键字段名
        /// </summary>
        public string StoreKeyFieldName
        {
            set
            {
                _storeKeyFieldName = value;
            }
            get
            {
                return _storeKeyFieldName;
            }
        }

        private string _storeKeyValue;
        /// <summary>
        /// 存储的关键字段值
        /// </summary>
        public string StoreKeyValue
        {
            set
            {
                _storeKeyValue = value;
            }
            get
            {
                return _storeKeyValue;
            }
        }

        private string _storeResourceFieldName;
        /// <summary>
        /// 存储资源的字段名
        /// </summary>
        public string StoreResourceFieldName
        {
            set
            {
                _storeResourceFieldName = value;
            }
            get
            {
                return _storeResourceFieldName;
            }
        }

        private string _storeResourceNameValue;
        /// <summary>
        /// 存储资源的字段值
        /// </summary>
        public string StoreResourceNameValue
        {
            set
            {
                _storeResourceNameValue = value;
            }
            get
            {
                return _storeResourceNameValue;
            }
        }

        private string _storeParamFieldName;
        /// <summary>
        /// 数据字典类别名
        /// </summary>
        public string StoreParamFieldName
        {
            set
            {
                _storeParamFieldName = value;
            }
            get
            {
                return _storeParamFieldName;
            }
        }

        private string _storeParamValue;
        /// <summary>
        /// 数据字典键值
        /// </summary>
        public string StoreParamValue
        {
            set
            {
                _storeParamValue = value;
            }
            get
            {
                return _storeParamValue;
            }
        }

        private byte[] _fileBytes;
        /// <summary>
        /// 文件转化后的字节数组
        /// </summary>
        public byte[] FileBytes
        {
            set
            {
                _fileBytes = value;
            }
            get
            {
                return _fileBytes;
            }
        }

        private System.IO.Stream _fileContent;
        /// <summary>
        /// 文件转化后的数据流
        /// </summary>
        public System.IO.Stream FileContent
        {
            set
            {
                _fileContent = value;
            }
            get
            {
                return _fileContent;
            }
        }

        #endregion
    }
}
