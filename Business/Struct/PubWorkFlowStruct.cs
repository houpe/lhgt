﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Struct
{
    /// <summary>
    /// 发布工作流信息结构
    /// </summary>
    public struct PubWorkFlowStruct
    {
        private string strFlowType;
        /// <summary>
        ///流程分类
        /// </summary>
        public string FlowType
        {
            get
            {
                return strFlowType;
            }
            set
            {
                strFlowType = value;
            }
        }

        private string strFlowName;
        /// <summary>
        ///流程名称
        /// </summary>
        public string FlowName
        {
            get
            {
                return strFlowName;
            }
            set
            {
                strFlowName = value;
            }
        }

        private string strFlowNum;
        /// <summary>
        ///流程排序
        /// </summary>
        public string FlowNum
        {
            get
            {
                return strFlowNum;
            }
            set
            {
                strFlowNum = value;
            }
        }

        private int bBszn;
        /// <summary>
        ///办事指南
        /// </summary>
        public int BSZN
        {
            get
            {
                return bBszn;
            }
            set
            {
                bBszn = value;
            }
        }

        private int bBllc;
        /// <summary>
        ///办事流程
        /// </summary>
        public int BLLC
        {
            get
            {
                return bBllc;
            }
            set
            {
                bBllc = value;
            }
        }

        private int bBgxz;
        /// <summary>
        ///表格下载
        /// </summary>
        public int BGXZ
        {
            get
            {
                return bBgxz;
            }
            set
            {
                bBgxz = value;
            }
        }

        private int bZxsb;
        /// <summary>
        ///在线申报
        /// </summary>
        public int ZXSB
        {
            get
            {
                return bZxsb;
            }
            set
            {
                bZxsb = value;
            }
        }

        private int bJggs;
        /// <summary>
        ///结果公示
        /// </summary>
        public int JGGS
        {
            get
            {
                return bJggs;
            }
            set
            {
                bJggs = value;
            }
        }

        private int bBlcx;
        /// <summary>
        ///办理查询
        /// </summary>
        public int BLCX
        {
            get
            {
                return bBlcx;
            }
            set
            {
                bBlcx = value;
            }
        }

        private int bXggd;
        /// <summary>
        ///相关规定
        /// </summary>
        public int XGGD
        {
            get
            {
                return bXggd;
            }
            set
            {
                bXggd = value;
            }
        }

        private int bZxts;
        /// <summary>
        ///在线投诉
        /// </summary>
        public int ZXTS
        {
            get
            {
                return bZxts;
            }
            set
            {
                bZxts = value;
            }
        }

        private int bJy;
        /// <summary>
        ///是否需要校验
        /// </summary>
        public int Jy
        {
            get
            {
                return bJy;
            }
            set
            {
                bJy = value;
            }
        }
    }
}
