﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Struct
{
    /// <summary>
    /// 数据存储方式。
    /// </summary>
    public enum DataStoreType
    {
        /// <summary>
        /// Oracle
        /// </summary>
        Select = 0,
        /// <summary>
        /// Ddc
        /// </summary>
        Update = 1,
        /// <summary>
        /// Access
        /// </summary>
        Insert = 2
    }
}
