﻿ 
// 创建人  ：LinJian
// 创建时间：2007年5月23日
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace Business.Common
{
    /// <!--
    /// 功能描述  : 类似datagrid控件操作类
    /// 创建人  : LinJian
    /// 创建时间: 2007年5月23日
    /// -->
    public class PageOperation
    {
        //记录集
        private DataTable dtGloab;

        //默认当前页码
        private int nPageNow;

        //默认每页显示记录数
        private int nPageSize;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="dtParam"></param>
        /// <param name="pageNow"></param>
        /// <param name="pageSize"></param>
        public PageOperation(DataTable dtParam, int pageNow, int pageSize)
        {
            dtGloab = dtParam;
            nPageNow = pageNow;
            nPageSize = pageSize;

            if (nPageNow < 1) nPageNow = 1;
            if (nPageSize < 1) nPageSize = 15;
        }

        /// <summary>
        /// 获取当前页的list
        /// </summary>
        public List<DataRow> CurrentPageList
        {
            get
            {
                int nIndexOfPage = nPageSize * (nPageNow-1);
                List<DataRow> lstDataRow = new List<DataRow>();

                for (int i = nIndexOfPage; i < nIndexOfPage+nPageSize; i++)
                {
                    if (i<RecordCount)
                    {
                        lstDataRow.Add(dtGloab.Rows[i]);
                    }
                }

                return lstDataRow;
            }
        }

        /// <summary>
        /// 获取页面总数
        /// </summary>
        public int PageCount
        {
            get
            {
                if (dtGloab.Rows.Count > 0)
                {
                    return dtGloab.Rows.Count % nPageSize == 0 ? dtGloab.Rows.Count / nPageSize : dtGloab.Rows.Count / nPageSize + 1;
                }
                else
                {
                    return 0;
                }
            }
        }
        
        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int RecordCount
        {
            get
            {
                return dtGloab.Rows.Count;
            }
        }

        /// <summary>
        /// 获取列名
        /// </summary>
        public string[] ColumnsName
        {
            get
            {
                string[] arrStrTemp = new string[dtGloab.Columns.Count];
                for (int i=0;i< dtGloab.Columns.Count;i++)
                {
                    arrStrTemp[i] = dtGloab.Columns[i].ColumnName;
                }

                return arrStrTemp;
            }
        }

        /// <summary>
        /// 获取当前页数
        /// </summary>
        /// <returns></returns>
        public int PageNow
        {
            get
            {
                return nPageNow;
            }
        }
        
        /// <summary>
        /// 获取每页的记录数
        /// </summary>
        public int EveryPageSize
        {
            get
            {
                return nPageSize;
            }
        }

        /// <summary>
        /// 创建翻页按钮
        /// </summary>
        /// <param name="mypage"></param>
        /// <param name="pagenow"></param>
        /// <param name="pageCount"></param>
        /// <param name="pagesize"></param>
        /// <returns></returns>
        public static StringBuilder CreatPageChanges(PageOperation mypage, int pagenow, int pageCount, int pagesize)
        {
            StringBuilder strPageHTML = new StringBuilder();
            string HeadShow = "";
            string FootShow = "";
            string GoPage = "转到第<select name='jumpPage' id='jumpPage' onchange='tosubmit()'>";
            if (pagenow == 1)
            {
                HeadShow = " 首页 上一页 ";
            }
            else
            {
                HeadShow = string.Format("<a href=\"javascript:tosubmit('1');\">首页</a> <a href=\"javascript:tosubmit('{0}');\">上一页</a> ", pagenow - 1);
            }
            if (pagenow == pageCount || pageCount == 0)
            {
                FootShow = " 下一页 尾页 ";
            }
            else
            {
                FootShow = string.Format("<a href=\"javascript:tosubmit('{0}');\">下一页</a> <a href=\"javascript:tosubmit('{1}');\">尾页</a> ", pagenow + 1, pageCount);
            }
            if (pageCount > 0)
            {
                for (int i = 1; i < pageCount + 1; i++)
                {
                    if (i == pagenow)
                    {

                        GoPage += string.Format("<option selected='selected' value='{0}'>{0}</option>", i);
                    }
                    else
                    {
                        GoPage += string.Format("<option value='{0}'>{0}</option>", i);
                    }
                }
            }
            GoPage += "</select>页";

            strPageHTML.Append(" <table width='100%' border='0' cellspacing='0' cellpadding='4'>");
            strPageHTML.AppendFormat("<tr><td style=\"text-align: right; height: 20px\">每页{0}行 共{1}行 当前{2}页 共{3}页 {4}{5}{6}</td></tr>",
                pagesize, mypage.RecordCount, pagenow, pageCount, HeadShow, FootShow, GoPage);

            return strPageHTML;

        }
    }
}
