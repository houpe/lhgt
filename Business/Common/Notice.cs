﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using WF_Business;

namespace Business.Common
{
    public class Notice
    {
        #region 设定弹出窗口接收人员(所有)
        /// <summary>
        /// 设定弹出窗口接收人员
        /// </summary>
        /// <param name="sID"></param>
        public void InsertNoticeUser(string sID)
        {
            string strSql = string.Format(@"insert into XT_NOTIFYUSER t (LOGIN_NAME,SID) select login_name,'{0}' from st_user t", sID);
            SysParams.OAConnection().RunSql(strSql);
        }
        #endregion

        /// <summary>
        /// 通知通告传送历史记录
        /// </summary>
        /// <param name="whereValue"></param>
        /// <returns></returns>
        public DataTable SendactListHistory(string whereValue)
        {
            string strSql = string.Format(@"select a.id, a.type,a.time,a.title,
                                   (select user_name from st_user where userid = a.userid) as owner,     
                                   (select t.user_name from st_user t where t.login_name= b.login_name and t.userid in 
                                   (select userid from st_user_department)) as 接收人,
                                   b.readflag,    
                                   case when b.readflag ='0' then '否' else '是' end 是否查看
                                   from xt_other a,  XT_NOTIFYUSER b,st_user u,st_user_department ud
                                  where b.login_name=u.login_name and u.userid=ud.userid and a.id=b.sid {0} order by ud.order_id,u.orderbyid", whereValue);
            DataTable dtTemp = new DataTable();
            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }


        #region 弹出窗口接收人员
        /// <summary>
        /// 弹出窗口接收人员
        /// </summary>
        /// <param name="sID"></param>
        public void InsertNoticeUser(string sID, int readflag)
        {
            string strSql = string.Format(@"select t.login_name from st_user t ");
            DataTable dt;
            SysParams.OAConnection().RunSql(strSql, out dt);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                strSql = string.Format(@"insert into XT_NOTIFYUSER t (LOGIN_NAME,SID,readflag) values ('{0}','{1}','{2}')", dt.Rows[i][0].ToString(), sID, readflag);
                SysParams.OAConnection().RunSql(strSql);
            }
        }
        #endregion

        #region 获取通知信息
        /// <summary>
        /// 获取通知信息
        /// 创 建 人:LiuY
        /// 创建时间:2009-04-15
        /// </summary>
        /// <param name="RealUserId"></param>
        /// <returns></returns>
        public DataTable GetNotiyUser(string RealUserId)
        {
            string strSql = string.Format(@"select * from XT_NOTIFYUSER t,xt_other a where t.sid=a.id and a.is_notice=1 and t.readflag = '0' and t.login_name='{0}'", RealUserId);
            DataTable dt;
            SysParams.OAConnection().RunSql(strSql, out dt);
            return dt;
        }
        #endregion

        #region 更新阅读标志位
        public void UpdateNotiyReadFlag(string RealUserId, string sid)
        {
            string strSql = string.Format(@"update XT_NOTIFYUSER t set t.readflag='1' where t.login_name='{0}' and t.sid = '{1}'", RealUserId, sid);
            SysParams.OAConnection().RunSql(strSql);
        }
        #endregion

    }
}
