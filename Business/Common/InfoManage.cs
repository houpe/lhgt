﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using WF_DataAccess;
using Microsoft.Practices.EnterpriseLibrary.Data;
using WF_Business;
namespace Business.Common
{
    /// <summary>
    /// 通知公告类信息操作
    /// </summary>
    public class InfoManage
    {
        public static DataTable GetNotice()
        {
            string strSql = "select * from xt_other t where t.is_notice='1'";
            DataTable dt;
            SysParams.OAConnection().RunSql(strSql, out dt);
            return dt;
        }

        /// <summary>
        /// Gets the type of the info.
        /// </summary>
        /// <param name="Type">消息类型</param>
        /// <param name="strSmalType">流程名称</param>
        /// <returns></returns>
        public DataTable GetInfoType(string Type, string strSmalType)
        {
            string strSql = @"select * from (select t.id as id,(case when length(t.title)>14 then 
                Substr(t.title,0,14) || '...' else t.title end ) as title,t.time,t.type  
                from xt_other t where 1 = 1  ";
            if (!string.IsNullOrEmpty(Type))
            {
                strSql += string.Format(" and type ='{0}'", Type);
            }
            if (!string.IsNullOrEmpty(strSmalType))
            {
                strSql += string.Format(" and SMALLTYPE ='{0}'", strSmalType);
            }
            strSql += " order by t.time desc ) where rownum<=5 ";

            DataTable dtOut;
            SysParams.OAConnection().RunSql(strSql, out dtOut);
            return dtOut;
        }

        /// <summary>
        /// 根据用户获取信息
        /// </summary>
        /// <param name="Type"></param>
        /// <param name="strUserid"></param>
        /// <returns></returns>
        public DataTable GetInfoByUser(string Type, string strUserid)
        {
            string strSql = @"select t.id as id,(case when length(t.title)>14 then 
                Substr(t.title,0,14) || '...' else t.title end ) as title,t.time,t.type  
                from xt_other t where 1 = 1  ";
            if (!string.IsNullOrEmpty(Type))
            {
                strSql += string.Format(" and type ='{0}'", Type);
            }
            if (!string.IsNullOrEmpty(strUserid))
            {
                strSql += string.Format(" and userid ='{0}'", strUserid);
            }
            strSql += " order by t.time desc";

            DataTable dtOut;
            SysParams.OAConnection().RunSql(strSql, out dtOut);
            return dtOut;
        }

        /// <summary>
        /// 读取办事指南内容
        /// </summary>
        /// <param name="strContentType">办事指南类别</param>
        /// <param name="strWid">获取流程id</param>
        /// <returns></returns>
        public DataTable GetPubContentInfo(string strContentType, string strWid)
        {
            string strSql = string.Format(@"select * from xt_other where type='{0}' and 
            SMALLTYPE=(select wname from st_workflow a where a.wid='{1}')",
                strContentType, strWid);

            DataTable dtOut;
            SysParams.OAConnection().RunSql(strSql, out dtOut);
            return dtOut;
        }

        /// <summary>
        /// Gets the pub content info.
        /// </summary>
        /// <param name="strContentType">Type of the STR content.</param>
        /// <param name="strWid">The STR wid.</param>
        /// <param name="strTitle">The STR title.</param>
        /// <param name="strQssj">The STR QSSJ.</param>
        /// <param name="strZzsj">The STR ZZSJ.</param>
        /// <returns></returns>
        public DataTable GetPubContentInfo(string strContentType, string strWid,
            string strTitle, string strQssj, string strZzsj)
        {
            string strSql = string.Format(@"select * from xt_other where type='{0}' and 
            SMALLTYPE=(select wname from st_workflow a where a.wid='{1}') ",
                strContentType, strWid);

            if (!string.IsNullOrEmpty(strTitle))
            {
                strSql += string.Format(" and title like '%{0}%'", strTitle);
            }
            if (!string.IsNullOrEmpty(strQssj))
            {
                strSql += string.Format(" and time>=to_date('{0}','yyyy-mm-dd')", strQssj);
            }
            if (!string.IsNullOrEmpty(strZzsj))
            {
                strSql += string.Format(" and time<=to_date('{0}','yyyy-mm-dd')", strZzsj);
            }

            DataTable dtOut;
            SysParams.OAConnection().RunSql(strSql, out dtOut);
            return dtOut;
        }

        /// <summary>
        /// Gets the pub info.
        /// </summary>
        /// <param name="strId">The STR id.</param>
        /// <returns></returns>
        public DataTable GetPubInfoById(string strId)
        {
            string strSql = @"select * from xt_other t where 1 = 1  ";
            if (!string.IsNullOrEmpty(strId))
            {
                strSql += string.Format(" and id ='{0}'", strId);
            }

            DataTable dtOut;
            SysParams.OAConnection().RunSql(strSql, out dtOut);
            return dtOut;
        }

        /// <summary>
        /// 读取办事指南内容
        /// </summary>
        /// <param name="strWid">流程名称</param>
        /// <returns></returns>
        public DataTable GetPubContentInfo(string strWid)
        {
            string strSql = string.Format(@"select id,filename,flowid,ftitle,fcontent,forder
                                  from xt_lawguide
                                 where filename = (select wname from st_workflow where wid='{0}') order by forder", strWid);

            DataTable dtOut;
            SysParams.OAConnection().RunSql(strSql, out dtOut);
            return dtOut;
        }

        /// <summary>
        /// 根据用户信息获取信息发布的内容2
        /// </summary>
        /// <param name="type"></param>
        /// <param name="login_name"></param>
        /// <returns></returns>
        public DataTable GetInfoTypeTop(string type, string login_name)
        {
            string strSql = "select * from (select t.id as id,(case when length(t.title)>30 then Substr(t.title,0,30) || '...' else t.title end ) as title,t.time as time,t.type as type from xt_other ,XT_NOTIFYUSER t1 where t.id=t1.sid and t1.login_name='" + login_name + "' and 1 = 1  ";
            if (!string.IsNullOrEmpty(type))
            {
                strSql += string.Format(" and type ='{0}'", type);
            }
            strSql += " order by t.time desc ) where rownum<=5 ";

            DataTable dtOut;
            SysParams.OAConnection().RunSql(strSql, out dtOut);
            return dtOut;
        }

        /// <summary>
        /// 删除文章
        /// </summary>
        /// <param name="id"></param>       
        public void DeleteArticle(string id)
        {
            IDataAccess idaTemp = SysParams.OAConnection(true);
            try
            {
                string strSql = string.Format("delete  xt_other t where t.id='{0}'", id);
                idaTemp.RunSql(strSql);
                strSql = string.Format("delete XT_NOTIFYUSER t where t.sid ='{0}'", id);
                idaTemp.RunSql(strSql);
                idaTemp.Close(true);
            }
            catch
            {
                idaTemp.Close(false);
            }
        }

    }
}
