using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using WF_Business;

namespace Business.Common
{
    /// <!--
    /// 功能描述  : oracle的系统表操作类
    /// 创建人  : LinJian
    /// 创建时间: 2007-07-09
    /// -->
    public class OracleSystemOperation
    {
        #region 获取指定表的所有列名及数据类型
        /// <summary>
        /// 获取指定表的所有列名及数据类型
        /// </summary>
        /// <param name="strDataSource"></param>
        /// <param name="strTableName"></param>
        /// <returns></returns>
        public DataTable GetFieldsNameOfTable(string strDataSource,string strTableName)
        {
            string strSql = string.Format("select * from all_tab_columns a where a.OWNER='{0}' and A.table_name='{1}'",strDataSource,strTableName);

            DataTable dtTemp = new DataTable();
            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }
        #endregion

        #region 获取所有表名
        /// <summary>
        /// 获取所有表名
        /// </summary>
        /// <returns></returns>
        public List<string> GetAllTablesName()
        {
            string strSql = "select table_name from user_tables";

            List<string> lstReturn = new List<string>();

            DataTable dtTemp = new DataTable();
            SysParams.OAConnection().RunSql(strSql, out dtTemp);

            foreach (DataRow drTemp in dtTemp.Rows)
            {
                lstReturn.Add(drTemp["table_name"].ToString());
            }

            return lstReturn;
        }
        #endregion

        #region 获取临时表数据
        /// <summary>
        /// 获取指定表的数据
        /// </summary>
        /// <param name="strTableName">数据表名</param>
        /// <returns></returns>
        public DataTable GetOneTableData(string strTableName)
        {
            string strSql = string.Format("select * from {0}", strTableName);
            DataTable dtSource = new DataTable();
            SysParams.OAConnection().RunSql(strSql, out dtSource);

            DataTable dtReturn = dtSource.Copy();
            dtReturn.TableName = strTableName;
            return dtReturn;
        }

        /// <summary>
        /// 获取指定表的数据
        /// </summary>
        /// <param name="dbParam">数据库操作类</param>
        /// <param name="dbTrans">事务对象</param>
        /// <param name="strTableName">数据表名</param>
        /// <returns></returns>
        public DataSet GetOneTableData(Database dbParam, DbTransaction dbTrans, string strTableName)
        {
            string strSql = string.Format("select * from {0}", strTableName);
            DataSet dsSource = new DataSet();

            using (DbCommand cmd = dbParam.GetSqlStringCommand(strSql))
            {
                if (dbTrans != null)
                {
                    dsSource = dbParam.ExecuteDataSet(cmd, dbTrans);
                }
                else
                {
                    dsSource = dbParam.ExecuteDataSet(cmd);
                }

                dsSource.Tables[0].TableName = strTableName;
            }

            return dsSource;
        }
        #endregion

        #region 删除临时表的数据
        /// <summary>
        /// 删除指定表的数据
        /// </summary>
        /// <param name="strTableName">数据表名</param>
        /// <returns></returns>
        public int DeleteOneTableData(string strTableName)
        {
            string strSql = string.Format("delete from {0}", strTableName);

            return SysParams.OAConnection().RunSql(strSql);
        }

        /// <summary>
        /// 删除指定表的数据
        /// </summary>
        /// <param name="dbParam">数据库操作类</param>
        /// <param name="dbTrans">事务对象</param>
        /// <param name="strTableName">数据表名</param>
        /// <returns></returns>
        public int DeleteOneTableData(Database dbParam, DbTransaction dbTrans, string strTableName)
        {
            string strSql = string.Format("delete from {0}", strTableName);

            if (dbTrans != null)
            {
                return dbParam.ExecuteNonQuery(dbTrans, CommandType.Text, strSql);
            }
            else
            {
                return dbParam.ExecuteNonQuery(CommandType.Text, strSql);
            }
        }
        #endregion

        #region 根据表集合获取dataset
        /// <summary>
        /// 根据表集合获取dataset
        /// </summary>
        /// <param name="lstTables">表名集合</param>
        /// <returns></returns>
        public  DataSet GetNeedExportData(List<string> lstTables)
        {
            DataSet dsReturn = new DataSet("ExportData");

            foreach (string strTableName in lstTables)
            {
                DataTable dtOne = GetOneTableData(strTableName);
                dsReturn.Tables.Add(dtOne);
            }

            return dsReturn;
        }
        #endregion

        /// <summary>
        /// 获取字段的注释
        /// </summary>
        /// <param name="strTableName">Name of the STR table.</param>
        /// <returns></returns>
        public DataTable GetFieldsComment(string strTableName)
        {
            string strSql = string.Format(@"select column_name,comments from user_col_comments t 
                where table_name='{0}'", strTableName);
            DataTable dtTemp = new DataTable();
            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }

        /// <summary>
        /// 获取用户表中字段内容列表
        /// </summary>
        /// <param name="strSouseTable">来源数据表名</param>
        /// <returns></returns>
        public DataTable GetSouseDataList(string strSouseTable)
        {
            string strSql = string.Format(@" select * from user_col_comments where table_name = '{0}' AND COMMENTS IS NOT NULL", strSouseTable);
            DataTable dtTemp = new DataTable();
            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }


        /// <summary>
        /// 将xml转化为dataset对象
        /// </summary>
        /// <param name="strXml">The STR XML.</param>
        /// <returns></returns>
        public DataSet GetDatasetFromXml(string strXml)
        {
            DataSet dsTemp = new DataSet();
            System.IO.StringReader reader = new System.IO.StringReader(strXml);
            dsTemp.ReadXml(reader);
            return dsTemp;
        }

        #region 获取指定表中类型不为TypeName的所有列名和注释
        /// <summary>
        /// 获取指定表中类型不为TypeName的所有列名和注释
        /// </summary>
        /// <param name="strTableName"></param>
        /// <param name="TypeName"></param>
        /// <returns></returns>
        public DataTable GetFieldsNameAndCommentFromTable(string strTableName,string TypeName)
        {
            string strSql=string.Format(@"select *
                                  from user_col_comments a, user_tab_columns b
                                 where a.table_name = b.TABLE_NAME and a.column_name=b.COLUMN_NAME 
                                   and b.DATA_TYPE <> '{0}'
                                   and a.table_Name = '{1}'",TypeName,strTableName);
            DataTable dtTemp = new DataTable();
            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }
        #endregion

        /// <summary>
        /// 根据列动态构建sql并执行
        /// </summary>
        /// <param name="lstFiledNames">列名集合</param>
        /// <param name="lstFieldValues">列值集合</param>
        /// <param name="strTableName">表名</param>
        /// <param name="strWhere">条件名</param>
        /// <param name="strUpdateFlag">更新命令</param>
        public void UpdateData(List<string> lstFiledNames, List<string> lstFieldValues, string strTableName, string strWhere, string strUpdateFlag)
        {
            string strSql = string.Empty;
            string strFieldName = string.Empty;
            string strFieldValue = string.Empty;
            if (strUpdateFlag == "insert")
            {
                //先执行删除
                for (int p = 0; p < lstFiledNames.Count; p++)
                {
                    strFieldName += string.Format("{0},", lstFiledNames[p]);
                    strFieldValue += string.Format("'{0}',", lstFieldValues[p]);
                }
                strFieldName = strFieldName.Substring(0, strFieldName.Length - 1);
                strFieldValue = strFieldValue.Substring(0, strFieldValue.Length - 1);

                strSql = string.Format("insert into {0}({1}) values({2})", strTableName, strFieldName, strFieldValue);
            }
            else if (strUpdateFlag == "update" && !string.IsNullOrEmpty(strWhere))
            {
                for (int p = 0; p < lstFiledNames.Count; p++)
                {
                    strFieldName += string.Format("{0}='{1}',", lstFiledNames[p], lstFieldValues[p]);
                }
                strFieldName = strFieldName.Substring(0, strFieldName.Length - 1);
                strSql = string.Format("update {0} set {1} where 1=1 {2}", strTableName, strFieldName, strWhere);
            }
            else if (strUpdateFlag == "delete" && !string.IsNullOrEmpty(strWhere))
            {
                strSql = string.Format("delete from {0} where 1=1 {1}", strTableName, strWhere);
            }

            SysParams.OAConnection().RunSql(strSql);
        }

        /// <summary>
        /// 根据表和条件获取字段的值
        /// </summary>
        /// <param name="strTabName"></param>
        /// <param name="strFieldName"></param>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public string GetTableFieldValue(string strTabName, string strFieldName, string strWhere)
        {
            string strSql = string.Format("select {0} from {1} where 1=1 and {2}", strFieldName, strTabName, strWhere);
            
            return SysParams.OAConnection().GetValue(strSql);
        }

        /// <summary>
        /// 根据通用参数获取datatable数据
        /// </summary>
        /// <param name="strTabName"></param>
        /// <param name="strFieldName"></param>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataTable GetDataByCommonParams(string strTabName, string strFieldName, string strWhere)
        {
            if (string.IsNullOrEmpty(strFieldName))
            {
                strFieldName = "*";
            }

            string strSql = string.Format("select {0} from {1}", strFieldName, strTabName);
            if (!string.IsNullOrEmpty(strWhere))
            {
                strSql += " where "+strWhere;
            }

            DataTable dtTemp = new DataTable();
            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }

        /// <summary>
        /// 动态复制表数据并执行sql
        /// </summary>
        /// <param name="strNewIid">新iid</param>
        /// <param name="strOldIid">关联iid</param>
        /// <param name="strTableName">表名</param>
        /// <param name="dataAccess">数据库操作类</param>
        /// <returns></returns>
        public string GetNewTaskSql(string strNewIid, string strOldIid, string strTableName)
        {
            string strSql = string.Format("select * from user_tab_cols t where table_name='{0}'", strTableName);
            DataTable dtReturn = new DataTable();
            SysParams.OAConnection().RunSql(strSql, out dtReturn);

            string strColumns = string.Empty;
            foreach (DataRow drOne in dtReturn.Rows)
            {
                if (string.IsNullOrEmpty(strColumns))
                {
                    strColumns = drOne["column_name"].ToString();
                }
                else
                {
                    strColumns += "," + drOne["column_name"].ToString();
                }
            }

            string strHouSql = strColumns.Replace("IID", "'" + strNewIid + "'");

            strSql = string.Format("insert into {0}({1}) select {2} from {0} where iid='{3}'", strTableName, strColumns, strHouSql, strOldIid);
            SysParams.OAConnection().RunSql(strSql);

            return strColumns;
        }

    }
}
