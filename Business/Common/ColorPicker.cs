﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business.Common
{
    public class ColorPicker
    {
        // Fields
        private string[] arr_FCColors = new string[] { 
        "1941A5", "AFD8F8", "F6BD0F", "8BBA00", "A66EDD", "F984A1", "CCCC00", "999999", "0099CC", "FF0000", "006F00", "0099FF", "FF66CC", "669966", "7C7CB4", "FF9933", 
        "9900FF", "99FFCC", "CCCCFF", "669900"
     };
        private int FC_ColorCounter = 0;

        // Methods
        public string getFCColor()
        {
            this.FC_ColorCounter++;
            return this.arr_FCColors[this.FC_ColorCounter % this.arr_FCColors.Length];
        }
    }

 

}
