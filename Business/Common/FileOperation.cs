﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Business.Common
{
    public class FileOperation
    {
        #region 获取指定路径下的所有文件名
        /// <summary>
        /// 获取指定路径下的所有文件名
        /// </summary>
        /// <param name="path"></param>
        public static List<string> GetDir(string path)
        {
            DirectoryInfo dires = new DirectoryInfo(path);
            FileInfo[] files = dires.GetFiles();

            List<string> lstReturn = new List<string>();
            foreach (FileInfo f in files)
            {
                lstReturn.Add(f.Name);
            }
            lstReturn.Remove("vssver2.scc");

            return lstReturn;
        }
        #endregion
    }
}
