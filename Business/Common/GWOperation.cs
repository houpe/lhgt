﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using WF_DataAccess;
using WF_Business;

namespace Business.Common
{
    /// <summary>
    /// 文件传送与接收类
    /// </summary>
    public class GWOperation : System.Web.UI.Page
    {
        #region 消息发送历史记录
        public DataTable SendMessageListHistory(string whereValue)
        {
            string strSql = string.Format(@"select a.id, a.FILENAME,
                                           a.UPLOADTIME,
                                           a.type,
                                           (select user_name from st_user where login_name = a.owner) as owner,
                                           a.owner as ownerid,       
                                           (select user_name from st_user t where t.login_name= b.userid) as 接收人,
                                           b.notes, 
                                           b.readflag,    
                                           case when b.readflag ='0' then '否' else '是' end 是否查看
                                           from xt_uploadfile a,  xt_browsefile b
                                         　where b.upload_id = a.id and a.flag='0' and b.flag='0'  {0}", whereValue);
            DataTable dtTemp = new DataTable();
            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }
        #endregion

        #region 通知通告传送历史记录
       
        /// <summary>
        /// 绑定部门
        /// </summary>
        /// <returns></returns>
        public DataTable Bindddl()
        {
            string strsql = "select distinct depart_name as name,departid as id,b.order_id from st_user_department a,st_department b where b.departid=a.order_id order by b.order_id";
            DataTable dtTemp = new DataTable();
            SysParams.OAConnection().RunSql(strsql, out dtTemp);
            return dtTemp;
        }
        #endregion

        #region 发送列表
        public DataTable SendMessageList(string whereValue)
        {
            string strSql = string.Format(@"select distinct(a.id),a.filename,a.uploadtime,
                                            (select user_name from st_user where login_name = a.owner) as owner,
                                            a.owner as ownerid 
                                            from xt_uploadfile a, xt_browsefile b 
                                            where b.upload_id = a.id
                                            and a.flag='0' and b.flag='0' 
                                            {0} order by a.uploadtime desc", whereValue);
            DataTable dtTemp = new DataTable();
            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }
        #endregion

        #region 更新阅读标志位
        /// <summary>
        /// 更新阅读标志位
        /// </summary>
        /// <param name="upload_id"></param>
        public void UpdateUpLoadFileReadFlag(string upload_id)
        {
            if (!string.IsNullOrEmpty(upload_id))
            {
                string strSql = string.Format(@"update xt_browsefile t set t.readflag='1' where t.upload_id='{0}' and userid=(select login_name from st_user where userid='{1}')", upload_id, Session["UserId"]);
                SysParams.OAConnection().RunSql(strSql);
            }
        }
        #endregion

        #region 获取公文发布列表
        /// <summary>
        /// 获取公文发布列表
        /// </summary>        
        /// <param name="whereValue"></param>
        /// <returns></returns>
        public DataTable GetUserUpLoadInfoArchives(string whereValue)
        {
            string strSql = string.Format(@"select t.*,a.user_name from xt_uploadfile t,st_user a where t.flag='1' and a.login_name = t.owner {0}", whereValue);
            DataTable dtTemp = new DataTable();
            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }
        #endregion

        #region 更新公文发布信息
        /// <summary>
        /// 更新公文发布信息
        /// </summary>
        /// <param name="archivesno"></param>
        /// <param name="archivesname"></param>
        /// <param name="id"></param>
        /// <param name="fileName"></param>
        public void UpdateUpLoadInfoArchives(string archivesno, string archivesname ,string id,string fileName)
        {
            string strSql = "";
            if (string.IsNullOrEmpty(fileName))
            {
                strSql = string.Format("update xt_uploadfile t set t.archivesno ='{0}',t.archivesname='{1}' where t.id='{2}'",archivesno,archivesname,id);
            }
            else
            {
                strSql = string.Format("update xt_uploadfile t set t.archivesno ='{0}',t.archivesname='{1}',t.filename = '{2}' where t.id='{3}'", archivesno, archivesname, fileName, id);
            }
            SysParams.OAConnection().RunSql(strSql);
        }
        #endregion

        #region 删除公文发布信息
        /// <summary>
        /// 删除公文发布信息
        /// </summary>
        /// <param name="xtID">ID</param>
        public void DeleteArchives(string xtID)
        {
            IDataAccess ida = SysParams.OAConnection(true);
            try
            {
                string strSql = string.Format("delete from xt_uploadfile t where t.id='{0}'", xtID);
                ida.RunSql(strSql);
                strSql = string.Format("delete from xt_browsefile t where t.upload_id = '{0}'", xtID);
                ida.RunSql(strSql);
                ida.Close(true);
            }
            catch
            {
                ida.Close(false);
            }
        }
        #endregion
    }
}
