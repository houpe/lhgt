﻿using System;
using System.Collections.Generic;
using System.Text;
using Business;
using System.Data;
using Business.Admin;
using WF_Business;

namespace Business.Common
{
    public class Message
    {
        #region 发送消息
        /// <summary>
        /// 发送消息
        /// </summary>
        /// <param name="userID">发送人</param>
        /// <param name="receiveList">接收人员列表</param>
        /// <param name="messageText">发送消息内容</param>
        /// <param name="messageTitle">标题头</param>
        /// <param name="iFlag">发送手机短信（1发送）</param>
        public void InsertInfoMessage(string userID,string receiveList,string messageText,string messageTitle,int iFlag)
        {
            string UserName = GetUserName(userID);
            if (!string.IsNullOrEmpty(receiveList))
            {
                string[] receive = receiveList.Split('|');
                StUserOperation suoTemp = new StUserOperation();
                for (int i = 0; i < receive.Length; i++)
                {
                    DataTable dtTemp = suoTemp.GetUserInfoByLoginName(receive[i]);
                    string strMobile = dtTemp.Rows[0]["Mobile"].ToString();
                    string receiveName = dtTemp.Rows[0]["User_Name"].ToString();
                    string strSql = string.Format("insert into XT_INSTANT_MESSAGE (messagetext,messagetitle,senduser,receiveuser) values ('{0}','{1}','{2}','{3}')",
                            messageText, messageTitle, UserName, dtTemp.Rows[0]["user_name"].ToString());
                    SysParams.OAConnection().RunSql(strSql);
                    if (iFlag == 1 && !string.IsNullOrEmpty(strMobile))
                    {
                        strSql = string.Format("insert into XT_INSTANT_MESSAGE(MESSAGETEXT,PHONENO,USERNAME) values('{0}','{1}','{2}')", messageText, strMobile, receiveName);
                        SysParams.OAConnection().RunSql(strSql);
                    }
                }
                string strSqlother = string.Format("insert into XT_INSTANT_MESSAGE(messagetext,messagetitle,senduser) values('{0}','{1}','{2}')", messageText, messageTitle, UserName);
                SysParams.OAConnection().RunSql(strSqlother);
            }
        }
        #endregion 

        #region 获取用户姓名
        /// <summary>
        /// 根据用户ID获取用户姓名
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        private string GetUserName(string userid)
        {
            string UserName = "";
            if (!string.IsNullOrEmpty(userid))
            {
                string strSql = string.Format("select user_Name from st_user t where t.userid='{0}'", userid);
                UserName = SysParams.OAConnection().GetValue(strSql);
            }
            return UserName;
        }
        #endregion

        

        #region 获取即时消息
        /// <summary>
        /// 获取即时消息
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="startData"></param>
        /// <param name="endData"></param>
        /// <param name="title"></param>
        /// <returns></returns>
        public DataTable GetReceiveMessage(string UserName, string startData, string endData,string title)
        {
            string strSql = string.Format(@"select t.messageid,t.messagetitle,
                       t.messagetext,
                       t.issended,
                       t.senduser,
                       t.receiveuser,
                       t.senddata
                  from XT_INSTANT_MESSAGE t
                 where t.receiveuser='{0}'", UserName);
            if (!string.IsNullOrEmpty(startData))
            {
                strSql += string.Format(" and t.senddata >=to_date('{0}','yyyy-MM-dd')", startData);
            }
            if (!string.IsNullOrEmpty(endData))
            {
                endData = Convert.ToDateTime(endData).AddDays(1).ToShortDateString();
                strSql += string.Format(" and t.senddata < to_date('{0}','yyyy-MM-dd')", endData);
            }
            if (!string.IsNullOrEmpty(title))
            {
                strSql += string.Format(" and t.messagetitle like '%{0}%'", title);
            }
            strSql += " order  by t.issended,t.senddata desc";
            DataTable dtTemp = new DataTable();
            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }
        #endregion

        #region 获取发送消息列表
        /// <summary>
        /// 获取发送消息列表
        /// </summary>
        /// <param name="UserName">发送人姓名</param>
        /// <param name="startData"></param>
        /// <param name="endData"></param>
        /// <param name="title">标题</param>
        /// <returns></returns>
        public DataTable GetSendMessage(string UserName, string startData, string endData, string title)
        {

            string strSql = string.Format("select * from XT_INSTANT_MESSAGE t where t.senduser='{0}' and t.receiveuser is null", UserName);
            if (!string.IsNullOrEmpty(startData))
            {
                strSql += string.Format(" and t.senddata >=to_date('{0}','yyyy-MM-dd')", startData);
            }
            if (!string.IsNullOrEmpty(endData))
            {
                endData = Convert.ToDateTime(endData).AddDays(1).ToShortDateString();
                strSql += string.Format(" and t.senddata < to_date('{0}','yyyy-MM-dd')", endData);
            }
            if (!string.IsNullOrEmpty(title))
            {
                strSql += string.Format(" and t.messagetitle like '%{0}%'", title);
            }
            strSql += " order  by t.issended,t.senddata desc";
            DataTable dtTemp = new DataTable();
            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }
        #endregion


        /// <summary>
        /// 来件查阅
        /// </summary>
        /// <param name="strUserId"></param>
        /// <returns></returns>
        public DataTable GetLjcy(string strUserId)
        {
            DataTable dt = new DataTable();
            string sql = string.Format(@"select a.FILENAME,
               a.UPLOADTIME,(select user_name from st_user where login_name = a.owner) as owner,
               a.owner as ownerid,b.userid, b.readflag,b.id,a.type,b.notes,b.READTIME 
                  from xt_uploadfile a,  xt_browsefile b
                 where b.upload_id = a.id
                 and b.userid = '{0}' and a.flag='0' and b.flag='0' and readflag='1'  
                order by readflag,uploadtime desc", strUserId);

            SysParams.OAConnection().RunSql(sql, out dt);
            return dt;
        }


        #region 删除即时消息
        /// <summary>
        /// 删除即时消息
        /// </summary>
        /// <param name="id"></param>
        public void DeleteMessage(string id)
        {
            string strSql = string.Format("delete from XT_INSTANT_MESSAGE t where t.messageid= '{0}'", id);
            SysParams.OAConnection().RunSql(strSql);
        }
        #endregion

        #region 根据ID获取消息内容
        /// <summary>
        /// 根据ID获取消息内容
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public DataTable GetMessageFromID(string id)
        {
            string strSql = string.Format("select * from XT_INSTANT_MESSAGE t where t.messageid = '{0}'", id);
            DataTable dt = new DataTable();
            SysParams.OAConnection().RunSql(strSql,out dt);

            strSql = string.Format("update XT_INSTANT_MESSAGE t set t.ISSENDED = 1 where t.messageid='{0}'", id);
            SysParams.OAConnection().RunSql(strSql);   
         
            return dt;
        }
        #endregion

        #region 发送消息列表
        public DataTable GetSendList(string userName)
        {
            string strSql = string.Format("select * from XT_INSTANT_MESSAGE t where t.senduser ='{0}'", userName);
            DataTable dt = new DataTable();
            SysParams.OAConnection().RunSql(strSql, out dt);
            return dt;
        }
        #endregion 
    }
}
