using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Business;
using WF_DataAccess;
using WF_Business;
using System.Data.OracleClient;

namespace Business.Common
{
    /// <summary>
    /// 通用操作类
    /// </summary>
    public class CommonOperationQuery
    {
        #region 插入数据
        /// <summary>
        /// 插入数据
        /// </summary>
        /// <param name="table"></param>
        /// <param name="key"></param>
        /// <param name="id"></param>
        /// <param name="data"></param>
        /// <param name="objNewId"></param>
        /// <returns></returns>
        public static bool InsertCqEvaluate(string table, string key, string id, Hashtable data, ref object objNewId)
        {
            string strSql = "insert into " + table;
            strSql += "(";
            string keyClause = string.Empty;
            string valueClause = string.Empty;
            bool bHaveKey = false;

            foreach (string datakey in data.Keys)
            {
                if (data[datakey] == null || data[datakey].ToString() == string.Empty)
                {
                    continue;
                }
                keyClause += datakey;
                keyClause += ",";
                if (data[datakey].ToString().StartsWith("to_date("))
                {
                    valueClause += data[datakey];
                    valueClause += ",";

                }
                else
                {
                    valueClause += ("'" + data[datakey] + "'");
                    valueClause += ",";
                }

                if (datakey.ToLower().CompareTo(key.ToLower()) == 0)
                {
                    bHaveKey = true;
                }
            }
            //如果这个id不为空，则添加id值(不一定为主键)
            if (id != null && id != string.Empty)
            {
                if (!bHaveKey)
                {
                    keyClause += key;
                    valueClause += ("'" + id + "'");
                }
                else
                {
                    keyClause = keyClause.Remove(keyClause.Length - 1);
                    valueClause = valueClause.Remove(valueClause.Length - 1);
                }
            }
            //否则把最后的逗号去掉
            else
            {
                keyClause = keyClause.Remove(keyClause.Length - 1);
                valueClause = valueClause.Remove(valueClause.Length - 1);
            }
            strSql += keyClause;
            strSql += ") VALUES (";
            strSql += valueClause;
            strSql += ")";

            //返回值
            strSql += string.Format(" returning {0} into :newId", key);


            IDataAccess ida = SysParams.OAConnection(false);
            IDataParameter[] iDataPrams = new OracleParameter[1];
            iDataPrams[0] = new OracleParameter("newId", OracleType.VarChar, 60);
            iDataPrams[0].Direction = ParameterDirection.Output;

            ida.RunSql(strSql, ref iDataPrams);

            objNewId = iDataPrams[0].Value.ToString();
            return true;
        }
        #endregion

        #region 更新数据
        /// <summary>
        /// 更新数据
        /// </summary>
        /// <param name="table"></param>
        /// <param name="key"></param>
        /// <param name="id"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static bool UpdateCqEvaluate(string table, string key, string id, Hashtable data)
        {
            string strSql = "update " + table + " set ";
            foreach (string datakey in data.Keys)
            {
                if (data[datakey] != null)
                {
                    if (data[datakey].ToString().StartsWith("to_date("))
                    {
                        strSql += (datakey + " = " + data[datakey] + ",");
                    }
                    else
                    {
                        strSql += (datakey + " = '" + data[datakey] + "',");
                    }
                }
            }
            strSql = strSql.Remove(strSql.Length - 1);
            strSql += (" where " + key + "='" + id + "'");

            return SysParams.OAConnection().RunSql(strSql) > 0 ? true : false;
        }
        #endregion

        #region 删除数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="table"></param>
        /// <param name="key"></param>
        /// <param name="id"></param>
        public static bool DeleteCqEvaluate(string table, string key, string id)
        {
            string strSql = "delete from " + table + " where " + key + " = '" + id + "'";

            return SysParams.OAConnection().RunSql(strSql) > 0 ? true : false;
        }
        #endregion

        #region 根据条件查询通用数据字典
        /// <summary>
        /// 根据条件查询通用数据字典
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static DataTable GetSysParams(string name,string strWhere)
        {
            string strSql = string.Format(@"select * from sys_params where name = '{0}'", name);
            switch (name.ToLower())
            {
                case "所有流程"://为在外网显示流程别名,保存数据为流程名称
                    strSql = "select * from (select WNAME keyvalue,wid keycode from st_workflow t where rot=0) where 1=1";
                    //strSql = @"select * from (select flowname keycode,flowtype keyvalue from xt_workflow_define where ispub='1' and isdelete='0' order by flownum) where 1=1 ";
                    break;
                case "所有用户":
                    strSql = "select * from (select user_name keyvalue,login_name keycode from st_user t) where 1=1";
                    break;
            }

            if (!string.IsNullOrEmpty(strWhere))
            {
                strSql += string.Format(" and {0}", strWhere); 
            }

            DataTable dtTemp;
            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }


        #endregion

   
        #region 根据条件查询通用数据字典
       
        #region 查询详细
        
        /// <summary>
        /// 获取详细信息
        /// </summary>
        /// <param name="table"></param>
        /// <param name="key"></param>
        /// <param name="id"></param>
        /// <param name="otherCondition">其他查询条件</param>
        /// <returns></returns>
        public static DataTable GetCqEvaluateDetail(string table, string key, string id, string otherCondition)
        {
            string strSql = string.Format("select * from {0} where {1}='{2}'", table, key, id);

            if (!string.IsNullOrEmpty(otherCondition))
            {
                strSql = strSql + " and " + otherCondition;
            }
            DataTable dtTemp;
            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }
        #endregion

        #region 设置数据交换
        /// <summary>
        /// 设置位
        /// </summary>
        /// <param name="table"></param>
        /// <param name="targetField"></param>
        /// <param name="targetValue"></param>
        /// <param name="key"></param>
        /// <param name="keyvalue"></param>
        public void UpdateCqFlag(string table, string targetField, bool targetValue, string key, string keyvalue)
        {
            string strSql = "update " + table + " set " + targetField + " = ";
            if (targetValue)
            {
                strSql += "1";
            }
            else
            {
                strSql += "0";
            }
            strSql += (" where " + key + " ='" + keyvalue + "'");

            SysParams.OAConnection().RunSql(strSql);
        }


        #endregion

        /// <summary>
        /// 根据表明和字段名获取字段的类型
        /// </summary>
        /// <param name="tablename"></param>
        /// <param name="columnname"></param>
        /// <returns></returns>
        public static string GetColumnType(string tablename, string columnname)
        {
            string strSql = string.Format("select data_type from user_tab_columns where table_Name='{0}' and column_name='{1}'",
                tablename, columnname);

            return SysParams.OAConnection().GetValue(strSql);
        }

        /// <summary>
        /// 添加统计行
        /// </summary>
        /// <param name="dtSource"></param>
        /// <param name="strRowNoField"></param>
        /// <param name="lstNeedCollectFields"></param>
        public static void AddCollectRow(DataTable dtSource, string strRowNoField,
            List<string> lstNeedCollectFields)
        {
            //整合构建datable,添加行标记、并统计企业总数和注册资本
            double[] arrTempDouble = new double[lstNeedCollectFields.Count];

            for (int i = 0; i < dtSource.Rows.Count; i++)
            {
                //dtSource.Rows[i][strRowNoField] = i + 1;

                for (int k = 0; k < lstNeedCollectFields.Count; k++)
                {
                    string strCollectField = lstNeedCollectFields[k];

                    if (!Convert.IsDBNull(dtSource.Rows[i][strCollectField]))
                    {
                        arrTempDouble[k] += Convert.ToDouble(dtSource.Rows[i][strCollectField]);
                    }
                    else
                    {
                        arrTempDouble[k] += 0;
                    }
                }
            }

            //添加统计行
            DataRow drNewRow = dtSource.NewRow();
            drNewRow[strRowNoField] = "合计";

            for (int k = 0; k < lstNeedCollectFields.Count; k++)
            {
                string strCollectField = lstNeedCollectFields[k];
                drNewRow[strCollectField] = arrTempDouble[k];
            }
            dtSource.Rows.Add(drNewRow);
        }

        #endregion


    }
}
