﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Data.OracleClient;
using WF_DataAccess;
using WF_Business;

namespace Business.Common
{
    /// <summary>
    /// File 的摘要说明
    /// </summary>
    public class AttachmentFileUpload
    {
        string filename;    //文件实际名称
        int filesize;           //文件大小
        string fileext;       //文件后缀
        string filetype;     //文件类型
        string filepath;     //文件真实路径
        byte[] filebuffer;  //文件字节流

        public AttachmentFileUpload()
        {
            //初期化
            filename = string.Empty;
            filesize = 0;
            fileext = string.Empty;
            filetype = string.Empty;
            filepath = string.Empty;
        }

        public bool readfile(HttpPostedFile objPostFile)
        {
            try
            {
                Stream objStream = objPostFile.InputStream;
                filename = Path.GetFileName(objPostFile.FileName);
                filepath = Path.GetDirectoryName(objPostFile.FileName);
                fileext = filename.Substring(filename.Length - 4, 4).ToLower();
                // 可在此限定要上传的指定文件类型            
                //
                // 获取文件流大小
                filesize = objPostFile.ContentLength;
                if (filesize < 1) return false;
                // 声明等长byte数组
                filebuffer = new byte[filesize];
                // 文件保存到缓存
                objStream.Read(filebuffer, 0, filesize);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// 上次文件至st_attachment表中
        /// </summary>
        /// <param name="IID">流程ID</param>
        /// <param name="Path">指定的上传路径</param>
        /// <param name="Fname">修改后的文件名</param>
        /// <param name="UserID">用户ID</param>
        /// <param name="Stepname">岗位名</param>
        /// <returns></returns>
        public bool Update_St_Attachment(string IID, string Path, string Fname, string UserID, string Stepname)
        {
            string strSql = @"insert into st_attachment (AID,MAGIC_NUMBER,EXT_NAME,DATA,NAME) 
                values (:AID,'1',:Ext_Name,:FileBuffer,:Name)";
            string Aid = Guid.NewGuid().ToString();
            IDataAccess ida = SysParams.OAConnection(true);
            try
            {
                IDataParameter[] idp = new System.Data.OracleClient.OracleParameter[4];
                idp[0] = new OracleParameter(":AID", OracleType.VarChar);
                idp[1] = new OracleParameter(":Ext_Name", OracleType.VarChar);
                idp[2] = new OracleParameter(":FileBuffer", OracleType.Blob);
                idp[3] = new OracleParameter(":Name", OracleType.VarChar);

                idp[0].Value = Aid;
                idp[1].Value = fileext;
                idp[2].Value = filebuffer;
                idp[3].Value = Fname;

                ida.RunSql(strSql, ref idp);
                ida.Close(true);

                if (Upate_St_Dynamic_Resource(IID, Path, Stepname, UserID, Aid)) 
                { 
                    return true; 
                } 
                else 
                { 
                    return false; 
                }
            }
            catch
            {
                ida.Close(false);
                return false;
            }
        }
        /// <summary>
        /// 更新St_Dynamic_Resource表中的关联信息
        /// </summary>
        /// <param name="IID"></param>
        /// <param name="Path"></param>
        /// <param name="Stepname"></param>
        /// <param name="UserID"></param>
        /// <param name="Res_value"></param>
        /// <returns></returns>
        public bool Upate_St_Dynamic_Resource(string IID, string Path, string Stepname, string UserID, string Res_value)
        {
            try
            {
                string StrSql = string.Format(@"insert into St_Dynamic_Resource(IID,PATH,SNAME,
                    USERID,TYPE,RES_VALUE) values ({0},'{1}','{2}','{3}',2,'{4}')",
                    IID, Path, Stepname, UserID, Res_value);
                SysParams.OAConnection().RunSql(StrSql);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public string GetFileName()
        {
            return filename;
        }

        public string GetFileSize()
        {
            decimal sizeM = Math.Round((decimal)filesize / 1048576, 2);
            int sizeKB = filesize / 1024;
            if (sizeM < 1)
            {
                return sizeKB.ToString() + "KB";
            }
            else
            {
                return sizeM.ToString() + "M";
            }
        }
        public string GetFileExt()
        {
            return fileext;
        }
        public string GetFileType()
        {
            return filetype;
        }
        public byte[] GetFilebuffer()
        {
            return filebuffer;
        }
 
        /// <summary>
        /// 获取附件操作权限
        /// </summary>
        /// <param name="strIid"></param>
        /// <param name="strStep"></param>
        /// <returns></returns>
        public int GetAttachmentRight(string strIid,string strStep)
        {
            string sql = String.Format(@"SELECT AAR FROM ST_STEP T1,ST_INSTANCE T2 WHERE T1.WID=T2.WID AND T2.IID='{0}' 
                AND T1.SNAME='{1}' and t2.isdelete <>1", strIid, strStep);
            string strReturnArr = SysParams.OAConnection().GetValue(sql);
            int nFlag = 0;
            if (!String.IsNullOrEmpty(strReturnArr))
            {
                nFlag = Convert.ToInt32(strReturnArr);
            }
            return nFlag;
        }

        
    }

}