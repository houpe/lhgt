﻿using System;
using System.Text;
using System.Web.UI;

namespace Business.Common
{
    /// <summary>
    /// 异常管理
    /// </summary>
    /// <example>
    /// 以下示例代码演示了LogAndReplace的使用方法(其它方法的使用方法相同)
    /// <code>
    /// try
    /// {
    ///    //代码块
    /// }
    /// catch (Exception ex)
    /// {
    ///    LogAndReplace(ex);
    /// }
    /// </code>
    /// </example>
    /// <!--
    /// 创建人  : LiKun
    /// 创建时间: 2006-09-26
    /// -->
    public static class ExceptionManager
    {
        /// <summary>
        /// 获取打开一个页面的脚本
        /// </summary>
        /// <param name="url">Url地址</param>
        /// <param name="urlName">打开地址的标题</param>
        /// <returns>返回一段脚本</returns>
        public static string GetOpen(string url, string urlName)
        {
            StringBuilder textBuilder = new StringBuilder();
            textBuilder.Append("Javascript:window.open('" + url + "','" + urlName +
                               "','height=680px,width=1010px,toolbar =no,menubar=no, scrollbars=yes, resizable=no, location=no, status=yes');");
            return textBuilder.ToString();
        }

        /// <summary>
        /// "获取打开一个页面的脚本"的重载函数
        /// </summary>
        /// <param name="url">Url地址</param>
        /// <param name="urlName">打开地址的标题</param>
        /// <param name="height">打开页面的高度(比如 100px)</param>
        /// <param name="width">打开页面的宽度(比如 100px)</param>
        /// <param name="resizable">打开页面的是否可以拖动(只能是 yes or no)</param>
        /// <returns>返回一段脚本</returns>
        public static string GetOpen(string url, string urlName, string height, string width, string resizable)
        {
            StringBuilder textBuilder = new StringBuilder();
            textBuilder.Append("Javascript:window.open('" + url + "','" + urlName + "','height=" + height + ",width=" +
                               width + ",top=200px,left=200px,toolbar =no,menubar=no, scrollbars=yes, resizable=" +
                               resizable + ", location=no, status=yes');");
            return textBuilder.ToString();
        }

        /// <summary>
        /// 弹出一个JavaScript对话框
        /// </summary>
        /// <param name="page">当前页面</param>
        /// <param name="tip">提示</param>
        public static void WriteAlert(Page page, string tip)
        {
            if (null != page)
            {
                page.ClientScript.RegisterStartupScript(page.GetType(), "提示：", "alert('" + tip.Replace("\n", string.Empty) + "');", true);
            }
        }



    }
}