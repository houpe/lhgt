﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using WF_Business;

namespace Business.Common
{
    /// <summary>
    /// 统计图操作类（FusionChart）
    /// </summary>
    public class FusionChartOperation
    {
        /// <summary>
        /// 生成统计报表
        /// </summary>
        /// <param name="assortField">分类字段</param>
        /// <param name="showField">展示字段</param>
        /// <param name="changeFieldShow">改变分类字段名称</param>
        /// <param name="bgColor">背景色</param>
        /// <param name="fieldColor">图形颜色</param>
        /// <param name="strSql">表名</param>
        /// <param name="strTitle">标题头</param>
        /// <param name="nType">显示类型(nType=1采用柱状图,nType=2采用饼状图)</param>
        /// <returns></returns>
        public string CollectAll(string assortField, string showField, string changeFieldShow, string bgColor, string fieldColor, string strSql, string strTitle,int nType)
        {
            DataTable dtTemp;
            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            string strXML = string.Empty;

            if (nType == 1)
            {
                strXML = CollectXML(dtTemp, assortField, showField, changeFieldShow, strTitle, fieldColor, bgColor);
            }
            else if(nType == 2)
            {
                strXML = CollectXMLPic(dtTemp, assortField, showField, changeFieldShow, strTitle, fieldColor, bgColor);
            }
            return strXML;
        }

        #region 返回隐藏字段
        public string BackHidField(string HidField)
        {
            string strHidField = "";
            if (HidField != null && HidField.IndexOf(";") >= 0)
            {
                string[] HField = HidField.Split(';');
                for (int i = 0; i < HField.Length; i++)
                {
                    strHidField += "'" + HField[i].ToUpper() + "',";
                }
                strHidField = strHidField.Substring(0, strHidField.Length - 1);
            }
            else if (HidField != null)
            {
                strHidField = "'" + HidField + "'";
            }

            return strHidField;
        }
        #endregion

        #region 获取数据(数据表)
        /// <summary>
        /// 获取数据(数据表)
        /// </summary>
        /// <param name="tableName">表名</param>
        /// <param name="orderField">排序字段</param>
        /// <returns></returns>
        public DataTable GetCollectDataFromTable(string tableName, string orderField)
        {
            string Flag = "0";
            string strSql = string.Format("select * from {0} ", tableName);
            string strSqlColumns = string.Format("select a.COLUMN_NAME from user_tab_columns a where a.TABLE_NAME ='{0}' and a.COLUMN_NAME='{1}'",tableName.ToUpper(), orderField.ToUpper());
            DataTable dtTemp;
            SysParams.OAConnection().RunSql(strSqlColumns, out dtTemp);
            if (dtTemp.Rows.Count > 0)
            {
                strSql += string.Format(" order by {0}", orderField);
                Flag = "1";
            }
            DataTable dt = null;
            SysParams.OAConnection().RunSql(strSql, out dt);
            if (Flag == "1")
            {
                dt.Columns.Remove(orderField);
            }
            return dt;
        }
        #endregion

        #region 组装XML(柱状图)
        /// <summary>
        /// 将DataTable组装成XML,只适合于单一列表
        /// </summary>
        /// <param name="dt">数据源</param>
        /// <param name="assortField">分类字段</param>
        /// <param name="showField">展示字段</param>
        /// <param name="changeFieldShow">改变分类字段名称</param>
        /// <param name="titleName">标题名</param>
        /// <param name="fieldColor">图形颜色</param>
        /// <param name="bgColor">背景色</param>
        /// <returns></returns>
        public string CollectXML(DataTable dt, string assortField, string showField, string changeFieldShow, string titleName, string fieldColor, string bgColor)
        {
            string strAssort = "<categories>";
            string strShowField = string.Format("<dataset seriesName='{0}' parentYAxis='P' color='{1}'>",
                changeFieldShow == "" ? assortField : changeFieldShow, fieldColor == "" ? "8BBA00" : fieldColor);
            string strXML = "";
            if (string.IsNullOrEmpty(bgColor))
            {
                bgColor = "f3f3f3";
            }
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i][showField].ToString() != "0" && dt.Rows[i][showField].ToString() != "")
                    {
                        strAssort += string.Format("<category name='{0}' />", dt.Rows[i][assortField].ToString());
                        strShowField += string.Format("<set value='{0}' />", dt.Rows[i][showField].ToString());
                    }
                }
            }
            string strUnit = BackUnit(showField);

            strShowField += "</dataset>";
            strAssort += "</categories>";
            strXML = string.Format(@"<graph caption='{0}' PYAxisName='Revenue' SYAxisName='Quantity' formatNumberScale='0' formatNumber ='0' baseFont='宋体' showValues='0' decimalPrecision='0' bgcolor='{3}' bgAlpha='70' showColumnShadow='1' divlinecolor='c5c5c5' numberSuffix='{4}' baseFontSize='12' divLineAlpha='60' showAlternateHGridColor='1' alternateHGridColor='f8f8f8' alternateHGridAlpha='60' SYAxisMaxValue='750'>{1}{2}</graph>", titleName, strAssort, strShowField, bgColor, strUnit);

            return strXML;
        }
        #endregion

        #region 组装XML(饼状图)
        /// <summary>
        /// 将DataTable组装成XML,只适合于单一列表
        /// </summary>
        /// <param name="dt">数据源</param>
        /// <param name="assortField">分类字段</param>
        /// <param name="showField">展示字段</param>
        /// <param name="changeFieldShow">改变分类字段名称</param>
        /// <param name="titleName">标题名</param>
        /// <param name="fieldColor">图形颜色</param>
        /// <param name="bgColor">背景色</param>
        /// <returns></returns>
        public string CollectXMLPic(DataTable dt, string assortField, string showField, string changeFieldShow, string titleName, string fieldColor, string bgColor)
        {
            string strAssort = "";
            string strXML = "";
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i][showField].ToString() != "0" && dt.Rows[i][showField].ToString() != "")
                    {
                        strAssort += string.Format("<set name='{0}' value='{1}'/> ", dt.Rows[i][assortField].ToString(), dt.Rows[i][showField].ToString());
                    }
                }
            }
            strXML = string.Format(@"<graph palette='4' decimals='0' enableSmartLabels='1' enableRotation='0' bgColor='99CCFF,FFFFFF' bgAlpha='40,100' bgRatio='0,100' bgAngle='360' showBorder='1' startingAngle='70' baseFontSize='12' formatNumberScale='0' baseFont='宋体' >{0}</graph>", strAssort);

            return strXML;
        }
        #endregion

        /// <summary>
        /// 组织单位显示
        /// </summary>
        /// <param name="showField"></param>
        /// <returns></returns>
        public string BackUnit(string showField)
        {
            string strUnit = "";
            if (showField.IndexOf("户数") >= 0)
            {
                strUnit = "户";
            }
            else if (showField.IndexOf("人数") >= 0)
            {
                strUnit = "人";
            }
            else if (showField.IndexOf("幢数") >= 0)
            {
                strUnit = "幢";
            }
            else if (showField.IndexOf("套数") >= 0)
            {
                strUnit = "套";
            }
            else if (showField.IndexOf("公司数") >= 0)
            {
                strUnit = "家";
            }
            else if (showField.IndexOf("案件数") >= 0)
            {
                strUnit = "件";
            }
            else if (showField.IndexOf("数") >= 0)
            {
                strUnit = "个";
            }
            else if (showField.IndexOf("面积") >= 0)
            {
                if (showField.IndexOf("物管面积") >= 0)
                {
                    strUnit = "万平米";
                }
                else
                {
                    strUnit = "平米";
                }
            }
            else if (showField.IndexOf("金额") >= 0)
            {
                strUnit = "元";
            }
            else
            {
                strUnit = "";
            }
            return strUnit;
        }

        #region 返回颜色
        public string BackColor(string year)
        {
            string color = "C9198D";
            switch (year)
            {
                case "2007":
                    break;
                case "2008":
                    color = "56B9F9";
                    break;
                case "2009":
                    color = "FDC12E";
                    break;
                case "2010":
                    color = "A186BE";
                    break;
            }
            return color;
        }
        #endregion

        #region 返回颜色
        public string BackColorPic(int i)
        {
            string[] sColor = { "56B9F9", "FDC12E", "C9198D", "A186BE", "AFD8F8", "F6BD0F", "8BBA00", "FF8E46", "008E8E", "D64646", "8E468E", 
                            "588526", "B3AA00" , "008ED6", "9D080D", "A186BE", "C9198D", "9CBDFE", "A2FF2D", "69D2F0A"};

            return sColor[i];
        }
        #endregion
    }
}
