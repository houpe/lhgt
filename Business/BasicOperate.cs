﻿
using System;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Oracle;
using System.Data;
using System.Data.Common;
using WF_DataAccess;
using WF_Business;

namespace Business
{
    /// <summary>
    /// 数据库一些基本操作
    /// </summary>
    /// <!--
    /// 创建人  : LiKun
    /// 创建时间: 2006-09-05
    /// -->
    public static class BasicOperate
    {

        #region 获取数据库数据

        #region String类型
        /// <summary>
        /// 获取数据库中的字符串
        /// </summary>
        /// <param name="obj">object类型的值</param>
        /// <returns>字符串</returns>
        /// <!--
        /// 创建人  : LiKun
        /// 创建时间: 2007-1-12
        /// -->
        public static string GetDataBaseString(object obj)
        {
            return GetDataBaseString(obj, false, string.Empty);
        }

        /// <summary>
        /// 获取数据库中的字符串。
        /// </summary>
        /// <param name="obj">object类型的值。</param>
        /// <param name="ifConvert">如果为 <c>true</c>，则强制转换类型。</param>
        /// <returns>字符串。</returns>
        /// <!--
        /// 创建人  : LiKun
        /// 创建时间: 2007-1-12
        /// -->
        public static string GetDataBaseString(object obj, bool ifConvert)
        {
            return GetDataBaseString(obj, ifConvert, string.Empty);
        }

        /// <summary>
        /// 获取数据库中的字符串。
        /// </summary>
        /// <param name="obj">object类型的值。</param>
        /// <param name="ifConvert">如果为 <c>true</c>，则强制转换类型。</param>
        /// <param name="defaultValue">默认值(obj为空，或转换失败时返回值)。</param>
        /// <returns>字符串。</returns>
        /// <!--
        /// 创建人  : LiKun
        /// 创建时间: 2007-1-12
        /// -->
        public static string GetDataBaseString(object obj, bool ifConvert, string defaultValue)
        {
            string result = defaultValue;
            if ((null != obj) && (DBNull.Value != obj))
            {
                if (ifConvert)
                {
                    try
                    {
                        result = Convert.ToString(obj);
                    }
                    catch
                    {
                        result = defaultValue;
                    }
                }
                else
                {
                    result = obj as string;
                    if (null == result)
                    {
                        result = defaultValue;
                    }
                }
            }
            return result;
        }
        #endregion

        #region Decimal类型
        /// <summary>
        /// 获取数据库中的数值类型数据。
        /// </summary>
        /// <param name="obj">object类型的值。</param>
        /// <returns>数值类型数据(默认为0)。</returns>
        /// <!--
        /// 创建人  : LiKun
        /// 创建时间: 2007-1-12
        /// -->
        public static decimal GetDataBaseDecimal(object obj)
        {
            return GetDataBaseDecimal(obj, false, decimal.Zero);
        }

        /// <summary>
        /// 获取数据库中的数值类型数据。
        /// </summary>
        /// <param name="obj">object类型的值。</param>
        /// <param name="ifConvert">如果为 <c>true</c>，则强制转换类型。</param>
        /// <returns>数值类型数据(默认为0)。</returns>
        /// <!--
        /// 创建人  : LiKun
        /// 创建时间: 2007-1-12
        /// -->
        public static decimal GetDataBaseDecimal(object obj, bool ifConvert)
        {
            return GetDataBaseDecimal(obj, ifConvert, decimal.Zero);
        }

        /// <summary>
        /// 获取数据库中的数值类型数据。
        /// </summary>
        /// <param name="obj">object类型的值。</param>
        /// <param name="ifConvert">如果为 <c>true</c>，则强制转换类型。</param>
        /// <param name="defaultValue">默认值(obj为空，或转换失败时返回值)。</param>
        /// <returns>数值类型数据。</returns>
        /// <!--
        /// 创建人  : LiKun
        /// 创建时间: 2007-1-12
        /// -->
        public static decimal GetDataBaseDecimal(object obj, bool ifConvert, decimal defaultValue)
        {
            decimal result = defaultValue;
            if ((null != obj) && (DBNull.Value != obj))
            {
                if (ifConvert)
                {
                    try
                    {
                        result = Convert.ToDecimal(obj);
                    }
                    catch
                    {
                        result = defaultValue;
                    }
                }
                else
                {
                    if (obj is decimal)
                    {
                        result = (decimal)obj;
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// 将obj值转换成Decimal(本方法已过时！请用GetDataBaseDecimal的重载方法！)
        /// </summary>
        /// <param name="obj">object值</param>
        /// <returns>如果不试数字类型的值返回0，不报错误</returns>
        [Obsolete("本方法已过时！请用GetDataBaseDecimal两个参数的重载方法！")]
        public static decimal GetConvertDecimal(object obj)
        {
            return GetDataBaseDecimal(obj, true, decimal.Zero);
        }
        #endregion

        #region DateTime类型

        /// <summary>
        /// 获取数据库中的时间类型数据。
        /// </summary>
        /// <param name="obj">object类型的值。</param>
        /// <returns>时间类型数据(默认为最小值)。</returns>
        /// <!--
        /// 创建人  : LiKun
        /// 创建时间: 2007-1-12
        /// -->
        public static DateTime GetDataBaseDateTime(object obj)
        {
            return GetDataBaseDateTime(obj, false, DateTime.MinValue);
        }

        /// <summary>
        /// 获取数据库中的时间类型数据。
        /// </summary>
        /// <param name="obj">object类型的值。</param>
        /// <param name="ifConvert">如果为 <c>true</c>，则强制转换类型。</param>
        /// <returns>时间类型数据(默认为最小值)。</returns>
        /// <!--
        /// 创建人  : LiKun
        /// 创建时间: 2007-1-12
        /// -->
        public static DateTime GetDataBaseDateTime(object obj, bool ifConvert)
        {
            return GetDataBaseDateTime(obj, ifConvert, DateTime.MinValue);
        }

        /// <summary>
        /// 获取数据库中的时间类型数据。
        /// </summary>
        /// <param name="obj">object类型的值。</param>
        /// <param name="ifConvert">如果为 <c>true</c>，则强制转换类型。</param>
        /// <param name="defaultValue">默认值(obj为空，或转换失败时返回值)。</param>
        /// <returns>时间类型数据。</returns>
        /// <!--
        /// 创建人  : LiKun
        /// 创建时间: 2007-1-12
        /// -->
        public static DateTime GetDataBaseDateTime(object obj, bool ifConvert, DateTime defaultValue)
        {
            DateTime result = defaultValue;
            if ((null != obj) && (DBNull.Value != obj))
            {
                if (ifConvert)
                {
                    try
                    {
                        result = Convert.ToDateTime(obj);
                    }
                    catch
                    {
                        result = defaultValue;
                    }
                }
                else
                {
                    if (obj is DateTime)
                    {
                        result = (DateTime)obj;
                    }
                }
            }
            return result;
        }
        #endregion

        #region int类型
        /// <summary>
        /// 获取数据库中的Int类型数据。
        /// </summary>
        /// <param name="obj">object类型的值。</param>
        /// <returns>Int类型数据(默认为0)。</returns>
        /// <!--
        /// 创建人  : LiKun
        /// 创建时间: 2007-1-12
        /// -->
        public static int GetDataBaseInt(object obj)
        {
            return GetDataBaseInt(obj, false, 0);
        }

        /// <summary>
        /// 获取数据库中的Int类型数据。
        /// </summary>
        /// <param name="obj">object类型的值。</param>
        /// <param name="ifConvert">如果为 <c>true</c>，则强制转换类型。</param>
        /// <returns>Int类型数据(默认为0)。</returns>
        /// <!--
        /// 创建人  : LiKun
        /// 创建时间: 2007-1-12
        /// -->
        public static int GetDataBaseInt(object obj, bool ifConvert)
        {
            return GetDataBaseInt(obj, ifConvert, 0);
        }

        /// <summary>
        /// 获取数据库中的Int类型数据。
        /// </summary>
        /// <param name="obj">object类型的值。</param>
        /// <param name="ifConvert">如果为 <c>true</c>，则强制转换类型。</param>
        /// <param name="defaultValue">默认值(obj为空，或转换失败时返回值)。</param>
        /// <returns>Int类型数据(默认为0)。</returns>
        /// <!--
        /// 创建人  : LiKun
        /// 创建时间: 2007-1-12
        /// -->
        public static int GetDataBaseInt(object obj, bool ifConvert, int defaultValue)
        {
            int result = defaultValue;
            if ((null != obj) && (DBNull.Value != obj))
            {
                if (ifConvert)
                {
                    try
                    {
                        result = Convert.ToInt32(obj);
                    }
                    catch
                    {
                        result = defaultValue;
                    }
                }
                else
                {
                    if (obj is decimal)
                    {
                        try
                        {
                            result = decimal.ToInt32((decimal)obj);
                        }
                        catch
                        {
                            result = defaultValue;
                        }
                    }
                }
            }
            return result;
        }
        #endregion

        #region long类型
        /// <summary>
        /// 获取数据库中的Long类型数据。
        /// </summary>
        /// <param name="obj">object类型的值。</param>
        /// <returns>Long类型数据(默认为0)。</returns>
        /// <!--
        /// 创建人  : LiKun
        /// 创建时间: 2007-1-12
        /// -->
        public static long GetDataBaseLong(object obj)
        {
            return GetDataBaseLong(obj, false, 0);
        }

        /// <summary>
        /// 获取数据库中的Long类型数据。
        /// </summary>
        /// <param name="obj">object类型的值。</param>
        /// <param name="ifConvert">如果为 <c>true</c>，则强制转换类型。</param>
        /// <returns>Long类型数据(默认为0)。</returns>
        /// <!--
        /// 创建人  : LiKun
        /// 创建时间: 2007-1-12
        /// -->
        public static long GetDataBaseLong(object obj, bool ifConvert)
        {
            return GetDataBaseLong(obj, ifConvert, 0);
        }

        /// <summary>
        /// 获取数据库中的Long类型数据。
        /// </summary>
        /// <param name="obj">object类型的值。</param>
        /// <param name="ifConvert">如果为 <c>true</c>，则强制转换类型。</param>
        /// <param name="defaultValue">默认值(obj为空，或转换失败时返回值)。</param>
        /// <returns>Long类型数据(默认为0)。</returns>
        /// <!--
        /// 创建人  : LiKun
        /// 创建时间: 2007-1-12
        /// -->
        public static long GetDataBaseLong(object obj, bool ifConvert, long defaultValue)
        {
            long result = defaultValue;
            if ((null != obj) && (DBNull.Value != obj))
            {
                if (ifConvert)
                {
                    try
                    {
                        result = Convert.ToInt64(obj);
                    }
                    catch
                    {
                        result = defaultValue;
                    }
                }
                else
                {
                    if (obj is decimal)
                    {
                        try
                        {
                            result = decimal.ToInt64((decimal)obj);
                        }
                        catch
                        {
                            result = defaultValue;
                        }
                    }
                }
            }
            return result;
        }
        #endregion

        #region float类型

        /// <summary>
        /// 获取数据库中的float类型数据。
        /// </summary>
        /// <param name="obj">object类型的值。</param>
        /// <returns>float类型数据(默认为0)。</returns>
        /// <!--
        /// 创建人  : LiKun
        /// 创建时间: 2007-1-12
        /// -->
        public static float GetDataBaseFloat(object obj)
        {
            return GetDataBaseFloat(obj, false, 0);
        }

        /// <summary>
        /// 获取数据库中的float类型数据。
        /// </summary>
        /// <param name="obj">object类型的值。</param>
        /// <param name="ifConvert">如果为 <c>true</c>，则强制转换类型。</param>
        /// <returns>float类型数据(默认为0)。</returns>
        /// <!--
        /// 创建人  : LiKun
        /// 创建时间: 2007-1-12
        /// -->
        public static float GetDataBaseFloat(object obj, bool ifConvert)
        {
            return GetDataBaseFloat(obj, ifConvert, 0);
        }

        /// <summary>
        /// 获取数据库中的float类型数据。
        /// </summary>
        /// <param name="obj">object类型的值。</param>
        /// <param name="ifConvert">如果为 <c>true</c>，则强制转换类型。</param>
        /// <param name="defaultValue">默认值(obj为空，或转换失败时返回值)。</param>
        /// <returns>float类型数据。</returns>
        /// <!--
        /// 创建人  : LiKun
        /// 创建时间: 2007-1-12
        /// -->
        public static float GetDataBaseFloat(object obj, bool ifConvert, float defaultValue)
        {
            float result = defaultValue;
            if ((null != obj) && (DBNull.Value != obj))
            {
                if (ifConvert)
                {
                    try
                    {
                        result = Convert.ToSingle(obj);
                    }
                    catch
                    {
                        result = defaultValue;
                    }
                }
                else
                {
                    if (obj is decimal)
                    {
                        try
                        {
                            result = decimal.ToSingle((decimal)obj);
                        }
                        catch
                        {
                            result = defaultValue;
                        }
                    }
                }
            }
            return result;
        }

        #endregion

        #region double类型

        /// <summary>
        /// 获取数据库中的double类型数据。
        /// </summary>
        /// <param name="obj">object类型的值。</param>
        /// <returns>double类型数据(默认为0)。</returns>
        /// <!--
        /// 创建人  : LiKun
        /// 创建时间: 2007-1-12
        /// -->
        public static double GetDataBaseDouble(object obj)
        {
            return GetDataBaseDouble(obj, false, 0);
        }

        /// <summary>
        /// 获取数据库中的double类型数据。
        /// </summary>
        /// <param name="obj">object类型的值。</param>
        /// <param name="ifConvert">如果为 <c>true</c>，则强制转换类型。</param>
        /// <returns>double类型数据(默认为0)。</returns>
        /// <!--
        /// 创建人  : LiKun
        /// 创建时间: 2007-1-12
        /// -->
        public static double GetDataBaseDouble(object obj, bool ifConvert)
        {
            return GetDataBaseDouble(obj, ifConvert, 0);
        }

        /// <summary>
        /// 获取数据库中的double类型数据。
        /// </summary>
        /// <param name="obj">object类型的值。</param>
        /// <param name="ifConvert">如果为 <c>true</c>，则强制转换类型。</param>
        /// <param name="defaultValue">默认值(obj为空，或转换失败时返回值)。</param>
        /// <returns>double类型数据。</returns>
        /// <!--
        /// 创建人  : LiKun
        /// 创建时间: 2007-1-12
        /// -->
        public static double GetDataBaseDouble(object obj, bool ifConvert, double defaultValue)
        {
            double result = defaultValue;
            if ((null != obj) && (DBNull.Value != obj))
            {
                if (ifConvert)
                {
                    try
                    {
                        result = Convert.ToDouble(obj);
                    }
                    catch
                    {
                        result = defaultValue;
                    }
                }
                else
                {
                    if (obj is decimal)
                    {
                        try
                        {
                            result = decimal.ToDouble((decimal)obj);
                        }
                        catch
                        {
                            result = defaultValue;
                        }
                    }
                }
            }
            return result;
        }

#endregion

        #endregion

        #region 判断字符串是否是由数字组成的
        /// <summary>
        /// 判断字符串是否是由数字组成的
        /// </summary>
        /// <param name="objParam"></param>
        /// <returns></returns>
        public static bool IsNumber(object objParam)
        {
            bool bReturn = true;
            bool bHaveSplit = true;
            string strParam = (string)objParam;

            for (int i = 0; i < strParam.Length; i++)
            {
                if (strParam.IndexOf('.') == i && i > 0 && bHaveSplit)
                {
                    bHaveSplit = false;
                    continue;
                }
                if (!char.IsNumber(strParam, i))
                {
                    bReturn = false;
                    break;
                }
            }

            return bReturn;
        } 
        #endregion

        #region 判断数据组是否存在指定的值
        /// <summary>
        /// 判断数据组是否存在指定的值
        /// </summary>
        /// <param name="arrParams">数组</param>
        /// <param name="strTarget">比对值</param>
        /// <returns></returns>
        public static bool ContainSpecialItem(string[] arrParams, string strTarget)
        {
            foreach (string strItem in arrParams)
            {
                if (strItem.Equals(strTarget))
                {
                    return true; ;
                }
            }
            return false;
        } 
        #endregion

        #region 获取数字对应在汉字中的大写
        /// <summary>
        /// 获取数字对应在汉字中的大写
        /// </summary>
        /// <param name="nIndex"></param>
        /// <returns></returns>
        public static string GetLargeCode(int nIndex)
        {
            string strReturn = string.Empty;
            switch (nIndex)
            {
                case 1:
                    strReturn = "一";
                    break;
                case 2:
                    strReturn = "二";
                    break;
                case 3:
                    strReturn = "三";
                    break;
                case 4:
                    strReturn = "四";
                    break;
                case 5:
                    strReturn = "五";
                    break;
                case 6:
                    strReturn = "六";
                    break;
                case 7:
                    strReturn = "七";
                    break;
                case 8:
                    strReturn = "八";
                    break;
                case 9:
                    strReturn = "九";
                    break;
            }
            return strReturn;
        } 
        #endregion

        #region 获取数据库中的Char型表示bool型值数据。
        /// <summary>
        /// 获取数据库中的Char型表示bool型值数据。
        /// </summary>
        /// <param name="obj">object类型值。</param>
        /// <returns>如果为'1'，返回true；其它返回false。</returns>
        /// <!--
        /// 创建人  : LiKun
        /// 创建时间: 2006-12-9
        /// -->
        public static bool GetDataBaseCharBool(object obj)
        {
            bool result = false;
            if (GetDataBaseString(obj) == "1")
            {
                result = true;
            }
            return result;
        } 
        #endregion

        #region 获取数据库中的number型表示布尔类型数据。
        /// <summary>
        /// 获取数据库中的number型表示布尔类型数据。
        /// </summary>
        /// <param name="obj">object类型值。</param>
        /// <returns>如果为1，返回true；其它返回false。</returns>
        /// <!--
        /// 创建人  : LiKun
        /// 创建时间: 2006-09-07
        /// -->
        public static bool GetDataBaseNumberBool(object obj)
        {
            if (decimal.One == GetDataBaseDecimal(obj))
            {
                return true;
            }
            else
            {
                return false;
            }
        } 
        #endregion

        #region 获取数据库中的二进制类型数据
        /// <summary>
        /// 获取数据库中的二进制类型数据
        /// </summary>
        /// <param name="obj">object类型值</param>
        /// <returns>非二进制或空，返回null</returns>
        /// <!--
        /// 创建人  : LiKun
        /// 创建时间: 2006-09-07
        /// -->
        public static byte[] GetDataBaseRaw(object obj)
        {
            if ((null == obj) || (DBNull.Value == obj))
            {
                return null;
            }
            else
            {
                byte[] bytes = obj as byte[];
                return bytes;
            }
        }        
        #endregion


        #region 设置数据库中number类型形式的布尔类型数据
        /// <summary>
        /// 设置数据库中number类型形式的布尔类型数据
        /// </summary>
        /// <param name="value">bool类型值</param>
        /// <returns>如果为false返回0，true返回1</returns>
        /// <!--
        /// 创建人  : LiKun
        /// 创建时间: 2006-09-08
        /// -->
        public static int SetDataBaseNumberBool(bool value)
        {
            if (value)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        } 
        #endregion

        #region 设置数据库中char类型形式的布尔类型数据
        /// <summary>
        /// 设置数据库中char类型形式的布尔类型数据
        /// </summary>
        /// <param name="value">bool类型值</param>
        /// <returns>如果为false返回'0'，true返回'1'</returns>
        /// <!--
        /// 创建人  : LiKun
        /// 创建时间: 2006-12-09
        /// -->
        public static char SetDataBaseCharBool(bool value)
        {
            if (value)
            {
                return '1';
            }
            else
            {
                return '0';
            }
        } 
        #endregion

        #region 设置时间类型的值，将时间最大及最小值转为DBNull。
        /// <summary>
        /// 设置时间类型的值，将时间最大及最小值转为DBNull。
        /// </summary>
        /// <param name="value">时间类型值。</param>
        /// <returns>转换后的时间值。</returns>
        /// <!--
        /// 创建人  : LiKun
        /// 创建时间: 2007-1-20
        /// -->
        public static object SetDataBaseDateTime(DateTime value)
        {
            if ((value == DateTime.MinValue) || (value == DateTime.MaxValue))
            {
                return DBNull.Value;
            }
            else
            {
                return value;
            }
        } 
        #endregion

        #region 将时间类型转换为字符串（最大最小值转为空，其它按照配置格式转换）。
        /// <summary>
        /// 将时间类型转换为字符串（最大最小值转为空，其它按照配置格式转换）。
        /// </summary>
        /// <param name="value">时间值。</param>
        /// <returns>转换后的字符串。</returns>
        /// <!--
        /// 创建人  : LiKun
        /// 创建时间: 2007-1-20
        /// -->
        public static string DateTimeToString(DateTime value)
        {
            if ((value == DateTime.MinValue) || (value == DateTime.MaxValue))
            {
                return string.Empty;
            }
            else
            {
                return value.ToString(SystemConfig.Format);
            }
        } 
        #endregion

        #region 将字符类型转换为数字类型。
        /// <summary>
        /// 将字符类型转换为数字类型。
        /// </summary>
        /// <param name="value">字符类型值。</param>
        /// <returns>转换后的数字。</returns>
        /// <!--
        /// 创建人  : LiKun
        /// 创建时间: 2007-1-24
        /// -->
        public static decimal StringToDecimal(string value)
        {
            decimal result = decimal.Zero;
            if (!string.IsNullOrEmpty(value))
            {
                value = value.Trim();
                if (!string.IsNullOrEmpty(value))
                {
                    try
                    {
                        result = decimal.Parse(value);
                    }
                    catch
                    {
                    }
                }
            }
            return result;
        } 
        #endregion

       

    }
}