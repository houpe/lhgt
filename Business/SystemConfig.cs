 
// 创建人  ：linjian
// 创建时间：2006-10-01
using System;
using System.Configuration;
using Common;
using System.Data;

namespace Business
{
    /// <summary>
    /// 系统参数类
    /// </summary>
    public static class SystemConfig
    {
        private static string _DateFormat;

        /// <summary>
        /// WebService链接地址
        /// </summary>
        public static string Format
        {
            get { return _DateFormat; }
        }

        private static int _PageSize;

        /// <summary>
        /// datagrid单个页面显示的记录行数
        /// </summary>
        public static int PageSize
        {
            get { return _PageSize; }
        }

        

        private static string[] _uploadFileExtension;

        /// <summary>
        /// 上传文件后缀
        /// </summary>
        public static string[] UploadFileExtension
        {
            get { return _uploadFileExtension; }
        }

        private static string _excelOutPath;
        /// <summary>
        /// excel的导出路径
        /// </summary>
        public static string ExcelOutPath
        {
            get { return _excelOutPath; }
        }

        private static string _netoWsUrl;
        /// <summary>
        /// 工作流引擎路径
        /// </summary>
        public static string NetoWsUrl
        {
            get { return _netoWsUrl; }
        }

        private static string _newActivexAppearUrl;
        /// <summary>
        /// 动态页面呈现地址
        /// </summary>
        public static string NewActivexAppearUrl
        {
            get { return _newActivexAppearUrl; }
        }

        private static string _workflowUrl;
        /// <summary>
        /// 看流程进度查看地址
        /// </summary>
        public static string WorkflowUrl
        {
            get { return _workflowUrl; }
        }

        /// <summary>
        /// 获取根路径
        /// </summary>
        /// <param name="request">请求信息</param>
        /// <returns></returns>
        /// <!--
        /// 创建人  : LinJian
        /// 创建时间: 2007年5月24日
        /// -->
        public static string GetBasePath(System.Web.HttpRequest request)
        {
            String path = request.ApplicationPath + "/webroot";
            String basePath = "http://" + request.ServerVariables["SERVER_NAME"]
                + ":" + Int32.Parse(request.ServerVariables["SERVER_PORT"]) + path + "/";

            return basePath;
        }

        /// <summary>
        /// 获取类似http：//server：8080的url头
        /// </summary>
        /// <param name="request">请求信息</param>
        /// <returns></returns>
        /// <!--
        /// 创建人  : LinJian
        /// 创建时间: 2007年5月28日
        /// -->
        public static string GetHeadPath(System.Web.HttpRequest request)
        {
            String basePath = "http://" + request.ServerVariables["SERVER_NAME"]
                + ":" + Int32.Parse(request.ServerVariables["SERVER_PORT"]);

            return basePath;
        }

        /// <summary>
        /// 获取每个工作日的小时数
        /// </summary>
        /// <returns></returns>
        /// <!--
        /// 创建人  : LinJian
        /// 创建时间: 2007年5月28日
        /// -->
        public static double WorkHoursInEveryDay
        {
            get
            {
                return Convert.ToDouble(ConfigurationManager.AppSettings["WorkTimeEveryDay"]);
            }
        }

        /// <summary>
        /// 获取每个工作日的总秒数
        /// </summary>
        public static double SecondInEveryDay
        {
            get
            {
                return 3600 * WorkHoursInEveryDay;
            }
        }

        /// <summary>
        /// SiteMap中Cache值
        /// </summary>
        public const string SiteMapCache = "B8B230A506944F6AB434DD205331A162";

        /// <summary>
        /// 加密密钥。
        /// </summary>
        public static string KeyPass
        {
            get
            {
                return ConfigurationManager.AppSettings["KeyPass"];
            }
        }

        private static bool _IsEncode;
        /// <summary>
        /// 配置文件是否已经加密。
        /// </summary>
        public static bool IsEncode
        {
            get
            {
                return _IsEncode;
            }
            set
            {
                _IsEncode = value;
            }
        }

        /// <summary>
        /// NetOffice数据库连接。
        /// </summary>
        public static ConnectionStringSettings NetOfficeConnection
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["CjFlowConn"];
            }
        }

        /// <summary>
        /// 加载系统配置
        /// </summary>
        static SystemConfig()
        {
            //时间格式
            _DateFormat = ConfigurationManager.AppSettings["date.format"];

            //datagrid单个页面显示的记录行数
            _PageSize = BasicOperate.GetDataBaseInt( ConfigurationManager.AppSettings["pagesize"],true);

            //上传文件后缀
            _uploadFileExtension = ConfigurationManager.AppSettings["UploadFileFormat"].Split(',');

            //获取到处excel路径
            _excelOutPath = ConfigurationManager.AppSettings["ExcelOutPath"];

            //工作流引擎路径
            _netoWsUrl = ConfigurationManager.AppSettings["NetoWsUrl"];

            //表单呈现
            _newActivexAppearUrl = ConfigurationManager.AppSettings["NewWebDisplay"];

            //进度视图查看
            _workflowUrl = ConfigurationManager.AppSettings["ActivexDisplay"];   
        }
       

   }
}