 
// 创建人  ：Wu Hansi
// 创建时间：2007年7月26日
using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;
using System.Text;

using Microsoft.Practices.EnterpriseLibrary.Data;
using WF_Business;
using System.Data.OracleClient;
using WF_DataAccess;

namespace Business.Admin
{
    /// <!--
    /// 功能描述  : 数据字典的操作
    /// 创建人  : Wu Hansi
    /// 创建时间: 2007年7月26日
    /// -->
    /// <summary>
    /// </summary>
    public class DictOperation
    {
        /// <summary>
        /// 取得所有的数据字典大类名
        /// </summary>
        /// <returns></returns>
        public DataTable GetDictNames()
        {
            string strSql = @"Select name, count(keyvalue) item_count from sys_params group by name";
            DataTable dtOut;
            SysParams.OAConnection().RunSql(strSql, out dtOut);
            return dtOut;
        }
        
        /// <summary>
        /// 取得指定的数据字典大类名
        /// </summary>
        /// <param name="item_name"></param>
        /// <returns></returns>
        public DataTable GetDictNames(string item_name)
        {
            string strSql = @"select name,count(keyvalue) item_count from sys_params group by name having name like '%" + item_name + "%'";
            DataTable dtOut;
            SysParams.OAConnection().RunSql(strSql, out dtOut);
            return dtOut;
        }

        /// <summary>
        /// 获取数据字典值
        /// </summary>
        /// <param name="strRowid">字典记录id</param>
        /// <returns></returns>
        public DataTable GetDictInfo(string strRowid)
        {
            string strSql = string.Format("Select t.*,t.rowid from sys_params t where rowid='{0}'", strRowid);
            DataTable dtOut;
            SysParams.OAConnection().RunSql(strSql, out dtOut);
            return dtOut;
        }

        /// <summary>
        /// 根据大类名取所有底下的字典项
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public DataTable GetDictFromName(string name)
        {
            string strSql = @"Select t.*,t.rowid from sys_params t where name='" + name + "'";
            DataTable dtOut;
            SysParams.OAConnection().RunSql(strSql, out dtOut);
            return dtOut;
        }

        /// <summary>
        /// 删除数据字典的行
        /// </summary>
        /// <param name="rowid"></param>
        /// <returns></returns>
        public void DeleteDictItem(string rowid)
        {
            string strSql = "Delete from sys_params t where rowid=:rowflag";

            IDataAccess ida = SysParams.OAConnection(false);
            IDataParameter[] iDataPrams = DataFactory.GetParameter(DataFactory.DefaultDbType, 1);
            iDataPrams[0].ParameterName = "rowflag";
            iDataPrams[0].DbType = DbType.String;
            iDataPrams[0].Value = rowid;

            ida.RunSql(strSql, ref iDataPrams);
        }

        /// <summary>
        /// 删除数据字典的行
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public void DeleteDictName(string name)
        { 
            //将对应数据删除
            string strSql = "Delete from sys_params t where name=:nameParam";

            IDataAccess ida = SysParams.OAConnection(false);
            IDataParameter[] iDataPrams = DataFactory.GetParameter(DataFactory.DefaultDbType, 1);
            iDataPrams[0].ParameterName = "nameParam";
            iDataPrams[0].DbType = DbType.String;
            iDataPrams[0].Value = name;

            ida.RunSql(strSql, ref iDataPrams);            
        }

        /// <summary>
        /// 增加数据字典
        /// </summary>
        /// <param name="strName">数据字典名称</param>
        /// <param name="strKeyValue">键值</param>
        /// <param name="strKeyCode">键码</param>
        /// <returns></returns>
        public void AddDictItem(string strName, string strKeyValue, string strKeyCode)
        {
            string strSql = string.Format(@"insert into sys_params t(name,KEYVALUE,KEYCODE) 
                    values('{0}','{1}','{2}')", strName, strKeyValue, strKeyCode);

            SysParams.OAConnection().RunSql(strSql);
        }

        /// <summary>
        /// 根据条件查询通用数据字典
        /// </summary>
        /// <param name="name"></param>
        /// <param name="strWhere">条件</param>
        /// <returns></returns>
        public static DataTable GetSysParams(string name, string strWhere)
        {
            string strSql = string.Format(@"select * from sys_params where name = '{0}'", name);
            switch (name.ToLower())
            {
                case "所有流程":
                    //为在外网显示流程别名,保存数据为流程名称
                    strSql = "select * from (select WNAME keyvalue,wid keycode from st_workflow t where rot=0) where 1=1 ";
                    //strSql = @"select flowname keycode,flowtype keyvalue from xt_workflow_define where ispub='1' and isdelete='0' order by flownum ";
                    break;
                case "所有单位":
                    strSql = "select * from (select unitsname keyvalue,id keycode from sys_units t) where 1=1";
                    break;
                case "发布类别":
                    strSql = string.Format(@"select * from (select t.id keycode, modulename keyvalue,t.type from XT_SERIAL_MODULE t) where 1=1 ", name);
                    break;
            }

            if (!string.IsNullOrEmpty(strWhere))
            {
                strSql += string.Format(" and {0}", strWhere);
            }

            DataTable dtOut;
            SysParams.OAConnection().RunSql(strSql, out dtOut);
            return dtOut;
        }

    }
}
