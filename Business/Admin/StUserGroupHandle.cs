﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Business.Struct;
using Business.FlowOperation;
using Business.Common;
using WF_Business;
namespace Business.Admin
{
    /// <summary>
    /// 用户组操作类
    /// 创建人：YZG
    /// 时间：2010-03-02
    /// </summary>
    public class StUserGroupHandle
    {
        /// <summary>
        /// 根据用户id获取角色id
        /// </summary>
        /// <param name="strUserId"></param>
        /// <returns></returns>
        public static string GetGroupIdByUserId(string strUserId)
        {
            string strSql = string.Format(@"select b.groupid from st_user a,st_user_group b where a.id=b.userid
                and upper(a.userid)='{0}'", strUserId.ToUpper());

            DataTable dtSource =new DataTable();
            SysParams.OAConnection().RunSql(strSql, out dtSource);
            string result = string.Empty;
            foreach (DataRow drTemp in dtSource.Rows)
            {
                if (!string.IsNullOrEmpty(result))
                {
                    result += "," + drTemp["groupid"].ToString();
                }
                else
                {
                    result += drTemp["groupid"].ToString();
                }
            }

            return result;
        }

        /// <summary>
        /// 获取角色id
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        public string GetGroupId(string userid)
        {
            string strSql = "select gid from st_user_group where userid='" + userid + "'";
            string gid = SysParams.OAConnection().GetValue(strSql);
            return gid;
        }


        /// <summary>
        /// 查询用户组
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static DataTable GetUserGroupById(string userId)
        {
            string sqlNum = "select * from st_user_group where userid='" + userId + "'";
            DataTable dt;
            SysParams.OAConnection().RunSql(sqlNum, out dt);
            return dt;
        }
        /// <summary>
        /// 查询用户所在组名
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static DataTable GetGroupName(string userId)
        {
            string strSql = " select a.group_name from st_group a where a.groupid in (select gid from st_user_group where userid='" + userId + "')";
            DataTable dt;
            SysParams.OAConnection().RunSql(strSql, out dt);
            return dt;
        }
        /// <summary>
        /// 根据组id删除组
        /// </summary>
        /// <param name="strGroupId">用户组id</param>
        /// <returns></returns>
        public static int DeleteGroupById(string strGroupId)
        {
            string strSql = string.Format("delete from st_group where groupid='{0}'",
                strGroupId);
            return SysParams.OAConnection().RunSql(strSql);
        }

        /// <summary>
        /// 根据Id更新组信息
        /// </summary>
        /// <param name="grpsParam">存放需要更新的组信息</param>
        /// <returns></returns>
        public static int UpdateGroupById(GroupStruct grpsParam)
        {
            string strSql = string.Format("update st_group set GROUP_NAME='{0}' where groupid='{1}'",
                grpsParam.GroupName, grpsParam.GroupId);
            return SysParams.OAConnection().RunSql(strSql);
        }

        /// <summary>
        /// 根据组id获取组信息
        /// </summary>
        /// <param name="strGroupId">用户组id</param>
        /// <returns></returns>
        public static GroupStruct GetGroupById(string strGroupId)
        {
            GroupStruct grpsReturn = new GroupStruct();

            string strSql = string.Format("select GROUP_NAME from st_group where groupid='{0}'",
                strGroupId);

            grpsReturn.GroupId = strGroupId;
            grpsReturn.GroupName = SysParams.OAConnection().GetValue(strSql);

            return grpsReturn;
        }

        /// <summary>
        /// 添加组信息
        /// </summary>
        /// <param name="strGroupName">组名称</param>
        /// <param name="strGroupDescrip">组描述</param>
        /// <returns></returns>
        public static int AddGroup(string strGroupName, string strGroupDescrip)
        {
            string strSql = string.Format("Insert into st_group(GROUPID,GROUP_NAME,MEMO) values('{0}','{1}','{2}')",
                Guid.NewGuid().ToString(), strGroupName, strGroupDescrip);
            return SysParams.OAConnection().RunSql(strSql);
        }

        /// <summary>
        /// 根据流程名和岗位名获取组号
        /// </summary>
        /// <param name="strWorkName">流程名</param>
        /// <param name="strStepName">岗位名</param>
        /// <returns></returns>
        public static String GetGroupIdOfStep(String strWorkName, String strStepName)
        {
            ClsUserWorkFlow clsWorkFlow = new ClsUserWorkFlow();
            string strWid = clsWorkFlow.GetFlowIdByName(strWorkName);

            String sql = string.Format("select GID from st_group_in_step where wid='{0}' " +
                "and stpname='{1}'", strWid, strStepName);
            string strGroupId = SysParams.OAConnection().GetValue(sql);

            return strGroupId;
        }

        /// <summary>
        /// 获取所有组信息
        /// </summary>
        /// <returns></returns>
        public static List<GroupStruct> GetAllGroupInfo(string groupName)
        {
            string strSql = "select * from st_group ";
            if (!String.IsNullOrEmpty(strSql))
                strSql += " WHERE Group_Name like '%" + groupName + "%'";
            //strSql += " order by INPUTTIME desc";
            DataTable dtPost;
            SysParams.OAConnection().RunSql(strSql, out dtPost);

            List<GroupStruct> lstGroup = new List<GroupStruct>();
            foreach (DataRow drTemp in dtPost.Rows)
            {
                GroupStruct gsTemp = new GroupStruct();
                gsTemp.GroupId = drTemp["GROUPID"].ToString();
                gsTemp.GroupName = drTemp["GROUP_NAME"].ToString();
                lstGroup.Add(gsTemp);
            }

            return lstGroup;
        }

        /// <summary>
        /// 添加组信息
        /// </summary>
        /// <param name="strGroupName">组名称</param>
        /// <param name="strGroupDescrip">组描述</param>
        /// <param name="orderNum">排序字段</param>
        /// <returns></returns>
        public static int AddGroup(string strGroupName, string strGroupDescrip, string orderNum, string isStop)
        {
            string strSql = string.Format("Insert into st_group(GROUPID,GROUP_NAME,MEMO,parent_gid,IS_STOP) values('{0}','{1}','{2}','{3}','{4}')",
                Guid.NewGuid().ToString(), strGroupName, strGroupDescrip, orderNum, isStop);
            return SysParams.OAConnection().RunSql(strSql);
        }

        #region 用户权限操作方法
        /// 修改人：YZG
        /// 时间：2010-03-02
        /// <summary>
        /// 查询权限用户
        /// </summary>
        /// <returns></returns>
        public static DataTable GetUserRight()
        {
            string sql = "SELECT a.*,b.user_name username FROM xt_query_right a,st_user b where a.userid=b.userid  ";
            DataTable dt;

            SysParams.OAConnection().RunSql(sql, out dt);
            return dt;
        }
        /// <summary>
        /// 更新用户权限
        /// </summary>
        /// <param name="ddlUserName"></param>
        /// <param name="ddlProcessName"></param>
        public static void UpdateUserRight(string ddlUserName, string ddlProcessName)
        {
            string sql = "INSERT INTO xt_query_right(userid,task)"
            + " VALUES('{0}','{1}')";
            sql = String.Format(sql, ddlUserName, ddlProcessName);
            SysParams.OAConnection().RunSql(sql);

        }
        /// <summary>
        /// 删除用户权限
        /// </summary>
        /// <param name="id"></param>
        public static void DeleteUserRight(string id)
        {
            string sql = "DELETE FROM xt_query_right WHERE ID='" + id + "'";
            SysParams.OAConnection().RunSql(sql);
        }
        /// <summary>
        /// 更新消息
        /// </summary>
        /// <param name="txtStepNO"></param>
        /// <param name="txtStepMsg"></param>
        /// <param name="ddlControl"></param>
        public static void UpdateMessage(string txtStepNO, string txtStepMsg, string ddlControl)
        {
            string strSql = string.Format(@"update xt_request_step set step_no='{0}',step_msg='{1}' where step_name='{2}' "
                                    , txtStepNO, txtStepMsg, ddlControl);

            SysParams.OAConnection().RunSql(strSql);
        }
        /// <summary>
        /// 获取委托
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public DataTable GetDelegate(string UserId)
        {
            string sql = string.Format(@"SELECT a.Id,
                                               b.user_name username,
                                               c.user_name delegate_UserName,
                                               delegate_task,
                                               delegate_time,
                                               begin_time,
                                               end_time
                                          FROM st_delegate a, st_user b, st_user c
                                         WHERE a.userid = b.userid
                                           and a.delegate_userid = c.userid
                                           AND a.USERID = '{0}'", UserId);
            DataTable dt;
            SysParams.OAConnection().RunSql(sql, out dt);
            return dt;
        }

        /// <summary>
        /// 查询委托用户
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="strWname"></param>
        /// <returns></returns>
        public string GetDelegateByUserId(string UserId, string strWname)
        {
            string strSql = string.Format("select userid from st_delegate where userid='{0}' and DELEGATE_TASK='{1}'", UserId, strWname);
            string strGetUserId = SysParams.OAConnection().GetValue(strSql);
            return strGetUserId;
        }

        /// <summary>
        /// 插入委托
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="ddlUserName"></param>
        /// <param name="ddlProcessName"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public int InserDelegate(string UserId, string ddlUserName, string ddlProcessName, string startDate, string endDate)
        {
            string strSql = "";
            strSql = "INSERT INTO st_delegate(userid,delegate_userid, delegate_task, delegate_time,  begin_time, end_time)"
                    + " VALUES('{0}','{1}','{2}',sysdate,to_date('{3}','YYYY-MM-DD HH24:Mi:SS'),to_date('{4}','YYYY-MM-DD HH24:Mi:SS'))";
            strSql = String.Format(strSql, UserId, ddlUserName, ddlProcessName, startDate, endDate);

            return SysParams.OAConnection().RunSql(strSql);
        }
        /// <summary>
        /// 删除委托
        /// </summary>
        /// <param name="id"></param>
        public void DeleteDelegate(string id)
        {
            string sql = "DELETE FROM ST_Delegate WHERE ID='" + id + "'";
            SysParams.OAConnection().RunSql(sql);
        }
        #endregion
    }
}
