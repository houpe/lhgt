﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Configuration;
using WF_Business;
namespace Business.Admin
{
    public class UserKaoQing
    {
        //保存用户考勤记录
        public static void SaveUserLog(string userID)
        {
            DateTime dt = DateTime.Now;
            string dtShort = DateTime.Now.ToShortDateString();
            if (UserKaoQing.GetWorkTime() != "")
            {
                string strSql = string.Format("select * from XT_TIMEBOOK t where t.userid='{0}' and t.datetimes=to_date('{1}','yyyy-MM-dd')", userID, dtShort);

                DataTable dtTemp = new DataTable();
                SysParams.OAConnection().RunSql(strSql, out dtTemp);

                if (dtTemp.Rows.Count == 0)
                {
                    strSql = string.Format("insert into XT_TIMEBOOK (userid,logintime,datetimes) values ('{0}',to_date('{1}','yyyy-MM-dd hh24:mi:ss'),to_date('{2}','yyyy-MM-dd'))", userID, dt, dtShort);
                    SysParams.OAConnection().RunSql(strSql);
                }
            }
        }
       
        /// <summary>
        /// 获取工作时间
        /// </summary>
        /// <returns></returns>
        public static String GetWorkTime()
        {
            String dtNow = DateTime.Now.ToShortDateString();
            string strSql = string.Format("select (case when t.mstime is null then t.netime else t.mstime end) worktime from st_worktime t where t.sdate=to_date('{0}','yyyy-MM-dd')", dtNow);
            return SysParams.OAConnection().GetValue(strSql);
        }

        /// <summary>
        /// 更新用户电话信息
        /// </summary>
        /// <param name="OfficeTel"></param>
        /// <param name="ExtNo"></param>
        /// <param name="Mobile"></param>
        /// <param name="HouseTel"></param>
        /// <param name="userid"></param>
        public void UpdateUserTelePhoneInfo(string OfficeTel, string ExtNo, string Mobile, string HouseTel, string userid)
        {
            string strSql = string.Format(@"Update st_user set OfficeTel='{0}',ExtNo='{1}',Mobile='{2}',HouseTel='{3}'  where userid = '{4}'",
                OfficeTel, ExtNo, Mobile, HouseTel, userid);
            SysParams.OAConnection().RunSql(strSql);
        }

        /// <summary>
        /// 获取人员基本信息
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        public DataTable GetUserBasicInfo(string userid)
        {
            DataTable dtTemp = new DataTable();
            if (!string.IsNullOrEmpty(userid))
            {
                string strSql = string.Format("select * from st_user t where t.userid='{0}'", userid);
                SysParams.OAConnection().RunSql(strSql, out dtTemp);
            }
            return dtTemp;
        }
    }
}
