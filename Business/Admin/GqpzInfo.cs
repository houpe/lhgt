﻿using System;
using System.Collections.Generic;
using System.Text;
using Business;
using Business.Struct;
using System.Data;
using WF_DataAccess;
using WF_Business;

namespace Business.Admin
{
    /// <summary>
    /// 挂起批准类操作
    /// </summary>
    public class GqpzInfo
    {
        /// <summary>
        /// 业务、流程分配人员
        /// </summary>
        /// <param name="serialID">业务ID</param>
        /// <param name="arrStrUserId">用户ID</param>
        /// <param name="flowno">流程ID</param>
        public static void AsignUserToGroup(string serialID, string[] arrStrUserId, string flowno, string isInsert)
        {
            IDataAccess dataAccess = SysParams.OAConnection(true);

            try
            {
                string strSql = string.Empty;
                if (string.IsNullOrEmpty(isInsert))//如果为空
                {
                    strSql = string.Format(@"delete  from xt_suspendflow_userid t where t.serial_id in (select wid
                                            from st_workflow b where b.wname in
                                            (select a.wname from st_workflow a where a.wid = '{0}')) and flow_id = '{1}'", serialID, flowno);
                    dataAccess.RunSql(strSql);
                }
                if (arrStrUserId != null)
                {
                    foreach (string strUserId in arrStrUserId)
                    {
                        strSql = string.Format("insert into xt_suspendflow_userid (userid, serial_id,flow_id) values ('{0}','{1}','{2}')",
                            strUserId, serialID, flowno);
                        dataAccess.RunSql(strSql);
                        if (!HaveQuertRight(serialID, strUserId))//无查询权限
                        {
                            InsertIntoQueryRight(serialID, strUserId);
                        }
                    }
                }
                dataAccess.Close(true);
            }
            catch
            {
                dataAccess.Close(false);
                throw;
            }
        }

        /// <summary>
        /// 查询人员信息 （st_user）中
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public static List<UserStruct> GetUserIdByFlowID(string username, string serialId, string flowno)
        {
            //如果查询流程ID不为空            
            string strSql = string.Format(@"select * from xt_suspendflow_userid t where t.serial_id in (select wid
                                         from st_workflow b where b.wname in (select a.wname  from st_workflow a
                                         where a.wid = '{0}'))", serialId);
            DataTable dt;
            SysParams.OAConnection().RunSql(strSql, out dt);
            DataTable dtOut = new DataTable();
            if (dt.Rows.Count > 0)
            {
                strSql = "select a.userid,c.user_name,c.login_name from xt_suspendflow_userid a,st_user c where a.userid=c.userid ";
                strSql += "and a.serial_id in (select wid from st_workflow b where b.wname in (select a.wname  from st_workflow a  where a.wid = '" + serialId + "')) and a.flow_id = (select id from xt_suspend_flow a where a.flow_no = '" + flowno + "')";
                strSql += " union all ";
                strSql += "select b.userid,b.user_name,b.login_name from st_user b where b.userid not in ";
                strSql += "(select userid from xt_suspendflow_userid where serial_id in (select wid from st_workflow b where b.wname in (select a.wname  from st_workflow a  where a.wid = '" + serialId + "')) and flow_id = (select id from xt_suspend_flow a where a.flow_no = '" + flowno + "'))";
            }
            else
            {
//                strSql = @"select b.userid, b.user_name, b.login_name  from st_user b, st_user_department a, st_department c
//                     where 1 = 1 and a.userid = b.userid and a.order_id = c.departid order by c.order_id, b.orderbyid ";
                strSql = @"select b.userid, b.user_name, b.login_name  from st_user b, st_department c
                     where b.deptid = c.departid ";
            }

            if (!string.IsNullOrEmpty(username))
            {
                strSql += " and b.user_name like '%" + username.Trim() + "%' ";
            }
            SysParams.OAConnection().RunSql(strSql, out dtOut);

            List<UserStruct> lstUserId = new List<UserStruct>();
            foreach (DataRow drTemp in dtOut.Rows)
            {
                UserStruct user = new UserStruct();
                user.UserRealName = drTemp["login_name"].ToString();
                user.UserId = drTemp["userid"].ToString();
                user.UserName = drTemp["user_name"].ToString();
                lstUserId.Add(user);
            }
            return lstUserId;
        }


        /// <summary>
        /// 已经关联的人员ID
        /// </summary>
        /// <param name="flowno"></param>
        /// <param name="iid"></param>
        /// <returns></returns>
        public static List<string> GetUserIdByFlowno(string flowID, string strFlowvalue)
        {
            string strSql = string.Format(@"select userid from xt_suspendflow_userid t where t.flow_id = '{0}' and t.serial_id  in  (select wid from st_workflow b where b.wname in (
                                    select a.wname  from st_workflow a where a.wid = '{1}'))",
                flowID, strFlowvalue);
            DataTable dtOut;
            SysParams.OAConnection().RunSql(strSql, out dtOut);

            List<string> lstUserId = new List<string>();
            foreach (DataRow drTemp in dtOut.Rows)
            {
                lstUserId.Add(drTemp["userid"].ToString());
            }
            return lstUserId;
        }


        /// <summary>
        /// 获取所有流程信息（st_workflow）
        /// </summary>
        /// <returns></returns>
        public static List<SuspendStruct> GetFlowNameInfo()
        {

            string strSql = string.Format("select t.wname,t.wid from st_workflow t where rot = 0 order by t.wname");
            DataTable dtOut;
            SysParams.OAConnection().RunSql(strSql, out dtOut);

            List<SuspendStruct> lstFlowInfo = new List<SuspendStruct>();
            foreach (DataRow drTemp in dtOut.Rows)
            {
                SuspendStruct suspend = new SuspendStruct();
                suspend.Flowname = drTemp["wname"].ToString();
                suspend.Flowno = drTemp["wid"].ToString();
                lstFlowInfo.Add(suspend);
            }
            return lstFlowInfo;
        }
        //查看用户是否具有查询权限
        private static bool HaveQuertRight(string wid, string userid)
        {
            string strSql = string.Format(@"select * from xt_query_right t where t.task=
                            (select wname from st_workflow a where a.wid='{0}' and a.rot=0) and t.userid = '{1}'", wid, userid);
            DataTable dt = new DataTable();
            SysParams.OAConnection().RunSql(strSql, out dt);
            if (dt.Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //为人员配备查询权限
        private static void InsertIntoQueryRight(string wid, string userid)
        {
            string strSql = string.Format(@"insert into xt_query_right (task,userid) values ((select t.wname
                                            from st_workflow t where t.wid = '{0}' and t.rot=0),'{1}')", wid, userid);
            SysParams.OAConnection().RunSql(strSql);
        }
        /// <summary>
        /// 获取（xt_suspend_flow）中ID
        /// </summary>
        /// <param name="flowno">流程号</param>
        /// <returns></returns>
        public static string GetFlowid(string flowno)
        {
            string strSql = string.Format("select t.id from xt_suspend_flow t where t.flow_no = '{0}'", flowno);
            string strFlowID = SysParams.OAConnection().GetValue(strSql);
            if (!string.IsNullOrEmpty(strFlowID))
            {
                return strFlowID;
            }
            else
                return "";
        }

        /// <summary>
        /// 获取挂起流程名
        /// </summary>
        /// <returns></returns>
        public DataTable GetGqFlowName()
        {
            string strSql = "select t.flow_name,t.flow_no from xt_suspend_flow t order by flow_no";
            DataTable dt = new DataTable();
            SysParams.OAConnection().RunSql(strSql, out dt);
            return dt;
        }


        /// <summary>
        /// 获取申请挂起业务信息（未挂起）
        /// </summary>
        /// <returns></returns>
        public DataTable GetAllSenialInfo()
        {
            string strSql = @"select s.serial,a.name, s.handleid,s.suspend_type, s.userid,to_char(apply_time, 'YYYY-MM-DD HH24:MI:SS'), s.memo, id,  b.wname
  from xt_suspend_apply s, st_instance a, st_workflow b where a.wid = b.wid   and a.iid = s.serial and ischeckup is null and isdelete is null";
            DataTable dt = new DataTable();
            SysParams.OAConnection().RunSql(strSql, out dt);
            return dt;
        }

    }
}
