using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using WF_DataAccess;
using System.Net;
using WF_Business;
namespace Business.Admin
{
    /// <summary>
    /// 日子操作类
    /// </summary>
    public class OperationLogs 
    {
        #region  向日志表中写入日志
        /// <summary>
        /// 向日志表中写入日志
        /// </summary>
        /// <param name="userid">用户ID</param>
        /// <param name="opttype">操作类型</param>
        /// <param name="optacction">操作动作</param>
        /// <param name="remark">备注</param>
        /// <param name="ip">操作机器的IP</param>
        /// <param name="usermemo">Request对象中的标明客户机的相关信息记录到此</param>
        public static void Write(string userid, int opttype, string optacction, string remark, string ip, string usermemo)
        {
             IDataAccess da = SysParams.OAConnection();
             IDataParameter[] idParams = DataFactory.GetParameter(DatabaseType.Oracle, 6);

             idParams[0].ParameterName = "iUserId";
             idParams[0].Value =userid;
             idParams[1].ParameterName ="iOptType"; 
             idParams[1].Value  =opttype ;
             idParams[2].ParameterName = "iOptAction";
             idParams[2].Value = optacction;
             idParams[3].ParameterName = "iRemark";
             idParams[3].Value = remark;
             idParams[4].ParameterName = "iIp";
             idParams[4].Value = ip;
             idParams[5].ParameterName = "iUserMemo";
             idParams[5].Value = usermemo;
             da.RunProc("Base.InsertOperationLogs", ref idParams);        
        }

        /// <summary>
        /// 向日志表中写入日志
        /// </summary>
        /// <param name="site">出错的方法及文件（例：Write方法在OperationLogs文件）</param>
        /// <param name="ex">出错时的异常</param>
        public static void Write(string site,Exception ex)
        {
            Write("SYS", 3, site, "错误：" + ex.Message + ",堆栈：" + ex.StackTrace,"","");
        }
        #endregion
     
        #region 查询操作日志列表
        /// <summary>
        ///  查询操作日志
        /// </summary>
        /// <param name="UserName">用户名</param>
        /// <param name="OptType">操作类型</param>
        /// <param name="action"></param>
        /// <param name="remark">备注</param>
        /// <param name="DtStart">开始时间</param>
        /// <param name="DtEnd">结束时间</param>
        /// <returns></returns>
        public static DataTable Query(string UserName,int OptType,string action,string remark,DateTime DtStart,DateTime DtEnd)
        {
            IDataAccess da = SysParams.OAConnection();
            DataTable dt = null;

            string sql = "select ID,userid,OptTime,OptType,OptAction,Remark,IP,UserMemo from XT_OperationLogs where opttime between to_date('" + DtStart + "','YYYY-MM-DD hh24:mi:ss') and to_date('" + DtEnd + "','YYYY-MM-DD hh24:mi:ss') ";

            if (UserName != "") //操作人
            {
                sql += "  and userid LIKE '%"+UserName+"%'";
            }
            if (!String.IsNullOrEmpty(action))
            {
                sql += " AND optAction LIKE '%" + action + "%'";
            }
            if (!String.IsNullOrEmpty(remark ))
            {
                sql += " AND Remark LIKE '%" + remark  + "%'";
            }
            if (OptType!=0) //操作类型
            {       
                sql += "  and opttype='" + OptType + "'";
            }
          da.RunSql(sql,out dt);
          return dt;
        }
        #endregion
      
        #region 删除日志
        /// <summary>
        ///  删除日志
        /// </summary>
        /// <param name="id">日志ID</param>
        public static void Delete(string id)
        {
            string sql = "";
            IDataAccess da = SysParams.OAConnection();
          
            sql="delete from XT_OperationLogs where id='"+id+"'";
            da.RunSql(sql);         
            da.Close();           
        }
        #endregion
    }
}