﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using WF_Business;
namespace Business.Admin
{
    /// <summary>
    /// 注册单位信息管理类
    /// </summary>
    public class SysUnitsOperation
    {
        /// <summary>
        /// 获取单位信息
        /// </summary>
        /// <param name="strID">单位ID</param>
        ///<param name="strUnitsName">单位名称</param>
        ///<param name="strOrganization">组织代码</param>
        ///<param name="strNature">单位性质</param>
        ///<param name="strLegal">法人代表</param>
        /// <returns></returns>
        public DataTable GetUnits(string strID, string strUnitsName, string strOrganization, string strNature, string strLegal)
        {
            DataTable dtTemp;
            string strSql = string.Format(@"select id,
                                               unitsname,
                                               unitstel,
                                               unitsaddress,
                                               fax,
                                               postcode,
                                               legal,
                                               organization,
                                               nature
                                          from sys_units where 1=1 ");
            if (!string.IsNullOrEmpty(strID))
                strSql += string.Format(" and id='{0}'", strID);
            if (!string.IsNullOrEmpty(strUnitsName))
                strSql += string.Format(" and unitsname like '%{0}%'", strUnitsName);
            if (!string.IsNullOrEmpty(strOrganization))
                strSql += string.Format(" and organization like '%{0}%'", strOrganization);
            if (!string.IsNullOrEmpty(strNature))
                strSql += string.Format(" and nature like '%{0}%'", strNature);
            if (!string.IsNullOrEmpty(strLegal))
                strSql += string.Format(" and legal like '%{0}%'", strLegal);
            strSql += string.Format(" order by unitsname");
            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }

        /// <summary>
        /// 删除单位信息
        /// </summary>
        /// <param name="strID">单位表ID</param>
        /// <!--addby zhongjian 20100412-->
        public static void DeleteUnitsInfo(string strID)
        {
            string strSql = string.Format("Delete from sys_units where id ='{0}'", strID);
            SysParams.OAConnection().RunSql(strSql);
        }
    }
}
