using System;
using System.Collections.Generic;
using System.Text;
using WF_DataAccess;
using System.Data;
using System.Data.OracleClient;
using Business.Struct;
using Business.Common;
using System.Security.Cryptography;
using System.Collections;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using Common;
using WF_Business;

namespace Business.Admin
{
    /// <summary>
    /// 用户信息操作
    /// </summary>
    public class StUserOperation
    {
        /// <summary>
        /// 清除锁定
        /// </summary>
        /// <param name="userName">要清除其锁定状态的用户。</param>
        /// <returns>如果成功取消锁定，则为 true；否则为 false。</returns>
        /// <!--
        /// 创建人  : LiKun
        /// 创建时间: 2006-09-06
        /// -->
        public static void UnlockUser(string userName)
        {
            if (!string.IsNullOrEmpty(userName))
            {
                string strSql = @"update st_user set locked = 0 where username = upper(:username)";

                IDataAccess ida = SysParams.OAConnection(false);
                IDataParameter[] iDataPrams = DataFactory.GetParameter(DataFactory.DefaultDbType, 1);
                iDataPrams[0].ParameterName = "username";
                iDataPrams[0].DbType = DbType.String;
                iDataPrams[0].Value = userName;

                ida.RunSql(strSql, ref iDataPrams);
            }
        }

        
        /// <summary>
        /// 根据用户id获取用户信息
        /// </summary>
        /// <param name="strUserId">用户id</param>
        /// <returns></returns>
        /// <!--
        /// 创建人  : LinJian
        /// 创建时间: 2007年5月26日
        /// -->
        public static UserStruct GetUserById(string strUserId)
        {
            string strSql = string.Format(@"select m.*,(Select t.DEPART_NAME From st_user_department n,
                st_department t  Where n.order_id=t.departid And n.userid=m.userid  And Rownum=1) DEPART_NAME,
                (Select n.order_id From st_user_department n Where n.userid=m.userid  And Rownum=1) DEPARTID 
                from st_user m Where m.userId = '{0}'", strUserId);
            DataTable dtOut;
            SysParams.OAConnection().RunSql(strSql, out dtOut);

            UserStruct user = new UserStruct();
            string strUserDepartId = string.Empty;
            if (dtOut.Rows.Count > 0)
            {
                user.UserId = dtOut.Rows[0]["USERID"].ToString();
                user.UserName = dtOut.Rows[0]["LOGIN_NAME"].ToString();
                user.UserRealName = dtOut.Rows[0]["USER_NAME"].ToString();
                user.UserOderbyid = dtOut.Rows[0]["ORDERBYID"].ToString();
                user.UserPass = dtOut.Rows[0]["PASSWORD"].ToString();
                user.UserDepartmentId = dtOut.Rows[0]["DEPARTID"].ToString();
                user.UserMobile = dtOut.Rows[0]["MOBILE"].ToString();
                user.UserInvalid = dtOut.Rows[0]["INVALID"].ToString();
                user.UserDepart_Name = dtOut.Rows[0]["DEPART_NAME"].ToString();
                foreach (DataRow drTemp in dtOut.Rows)
                {
                    strUserDepartId = strUserDepartId + drTemp["departid"].ToString() + ",";
                }
                strUserDepartId = strUserDepartId.TrimEnd(',');
                user.UserDepartId = strUserDepartId.Split(',');
            }

            return user;
        }

        /// <summary>
        /// 通过登陆名获取用户实体
        /// </summary>
        /// <param name="loginName"></param>
        /// <returns></returns>
        public MoaUser GetByLoginName(string loginName)
        {
            //try
            //{
            //    Table<MoaUser> tabUserList = _DataContext.GetTable<MoaUser>();
            //    var result = from entity in tabUserList 
            //                 where entity.Login_Name == loginName
            //                 select entity;
            //    _DataContext.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, result);
            //    if (result.Count() > 0)
            //        return result.First();
            //    else
            //        return null;
            //}
            //catch
            //{
            //    throw;
            //}

            string strSql = string.Format("select * from st_user where login_name='{0}' order by login_name", loginName);

            DataTable dt = new DataTable();
            SysParams.OAConnection().RunSql(strSql, out dt);

            if (dt.Rows.Count > 0)
            {
                List<MoaUser> lstTemp = CommandFunction.DataTableToList<MoaUser>(dt);
                return lstTemp[0];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 检查密码
        /// </summary>
        /// <param name="loginName"></param>
        /// <param name="password"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        public bool CheckPassword(string loginName, string password, out string msg)
        {
            try
            {
                MoaUser MoaUser = GetByLoginName(loginName);
                if (MoaUser == null)
                {
                    msg = "用户名不存在！";
                    return false;
                }

                if (Encode.Md5(password) == MoaUser.Password || password == MoaUser.Password)
                {
                    msg = "";
                    return true;
                }
                else
                {
                    msg = "密码错误！";
                    return false;
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message;

                return false;
            }
        }

        #region 根据用户登录名获取用户信息
        /// <summary>
        /// 根据用户登录名获取用户信息
        /// </summary>
        /// <param name="LogionName"></param>
        /// <returns></returns>
        public DataTable GetUserInfoByLoginName(string LogionName)
        {
            DataTable dt = new DataTable();
            if (!string.IsNullOrEmpty(LogionName))
            {
                string strSql = string.Format("select * from st_user t where t.login_name='{0}'", LogionName);
                SysParams.OAConnection().RunSql(strSql, out dt);
            }
            return dt;
        }
        #endregion

        /// <summary>
        /// 根据userid查询用户信息
        /// </summary>
        /// <returns></returns>
        public DataTable GetUserInfoById(string userId)
        {
            string strSql = string.Format("select * from st_user where userid = '{0}'", userId);
            DataTable dtTemp = new DataTable();
            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }

        /// <summary>
        /// 获取登录用户信息
        /// </summary>
        /// <param name="loginName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public DataTable GetLoginUserInfo(string loginName, string password)
        {
            string strSql = string.Format("select userid,user_name from st_user where login_name='{0}' and (password='{1}' or password='{2}')",
                  loginName, password, Encode.Md5(password).ToUpper());

            DataTable dtOut = null;
            SysParams.OAConnection().RunSql(strSql, out dtOut);
            return dtOut;
        }

        /// <summary>
        /// 获取登录用户信息
        /// </summary>
        /// <param name="loginName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public string GetLoginUserInfoWithJson(string loginName, string password)
        {
            string strSql = string.Format("select userid,user_name from st_user where login_name='{0}' and (password='{1}' or password='{2}')",
                  loginName, password, Encode.Md5(password).ToUpper());

            DataTable dtOut = null;
            SysParams.OAConnection().RunSql(strSql, out dtOut);
            string strReturn = string.Empty;
            if (dtOut.Rows.Count > 0)
            {
                JsonOperation _djson = new JsonOperation();
                strReturn = _djson.BindJson(dtOut);
            }
            return strReturn;
        }

        /// <summary>
        /// 获取所有用户信息
        /// </summary>
        /// <returns></returns>
        /// <!--
        /// 创建人  : LinJian
        /// 创建时间: 2007年5月23日        
        /// -->
        public static List<UserStruct> GetAllUserTemp(String groupId, String userName,int nType)
        {
            string strUserName = userName.Trim();
            string strSql = "SELECT m.* FROM ST_USER m where 1=1 ";

            if (!String.IsNullOrEmpty(groupId))
            {
                if (nType == 0)
                {
                    strSql += string.Format(" and m.UserId in(SELECT UserId FROM ST_USER_GROUP WHERE GId='{0}')", groupId);
                }
                else//角色选人
                {
                    strSql += string.Format(@" and m.UserId in 
                    (SELECT UserId FROM ST_USER_GROUP WHERE GId='{0}')
                     UNION ALL SELECT m.* FROM ST_USER m  WHERE m.UserId NOT IN 
                    (SELECT UserId FROM ST_USER_GROUP WHERE GId='{0}')", groupId);
                }
            }
            if (!String.IsNullOrEmpty(strUserName))
            {
                strSql += string.Format(" and m.User_Name LIKE '%{0}%'", strUserName);
            }

            if (nType == 0)
            {
                strSql += "order by m.orderbyid";
            }

            DataTable dtPost;

            SysParams.OAConnection().RunSql(strSql, out dtPost);

            List<UserStruct> lstUser = new List<UserStruct>();
            foreach (DataRow drTemp in dtPost.Rows)
            {
                UserStruct user = new UserStruct();
                user.UserId = drTemp["USERID"].ToString();
                user.UserName = drTemp["LOGIN_NAME"].ToString();
                user.UserRealName = drTemp["USER_NAME"].ToString();
                user.UserPass = drTemp["PASSWORD"].ToString();
                user.UserOderbyid = drTemp["ORDERBYID"].ToString();
                user.UserInvalid = drTemp["INVALID"].ToString();
                lstUser.Add(user);
            }

            return lstUser;
        }

        /// <summary>
        /// 根据部门、角色、用户获取
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="userName"></param>
        /// <param name="departName"></param>
        /// <returns></returns>
        public static List<UserStruct> GetAllUserTemp(String groupId, String userName, String departName)
        {
            string strUserName = userName.Trim();
            string strSql = "SELECT m.* FROM ST_USER m where 1 = 1 ";

            if (!String.IsNullOrEmpty(departName))
            {
                strSql = string.Format(@"select m.* from ST_USER m,st_department t,st_user_department b 
                    where t.depart_name='{0}' and m.userid=b.userid  and b.order_id=t.departid ", departName);
            }
            if (!String.IsNullOrEmpty(groupId))
            {
                strSql += string.Format(" and m.UserId in(SELECT UserId FROM ST_USER_GROUP WHERE GId='{0}')", groupId);
            }
            if (!String.IsNullOrEmpty(strUserName))
            {
                strSql += string.Format(" and m.User_Name LIKE '%{0}%'", strUserName);
            }

            strSql += @"  order by m.orderbyid";
            DataTable dtPost;
            SysParams.OAConnection().RunSql(strSql, out dtPost);

            List<UserStruct> lstUser = new List<UserStruct>();
            foreach (DataRow drTemp in dtPost.Rows)
            {
                UserStruct user = new UserStruct();
                user.UserId = drTemp["USERID"].ToString();
                user.UserName = drTemp["LOGIN_NAME"].ToString();
                user.UserRealName = drTemp["USER_NAME"].ToString();
                user.UserPass = drTemp["PASSWORD"].ToString();
                user.UserOderbyid = drTemp["ORDERBYID"].ToString();
                user.UserInvalid = drTemp["INVALID"].ToString();
                lstUser.Add(user);
            }

            return lstUser;
        }

        /// <summary>
        /// 将用户密码重置为1。
        /// </summary>
        /// <param name="userName">为其重置密码的用户。</param>
        /// <returns>指定的用户的新密码。</returns>
        /// <!--
        /// 创建人  : LiKun
        /// 创建时间: 2006-09-07
        /// -->
        public static string ResetPassword(string userName)
        {
            if (!string.IsNullOrEmpty(userName))
            {
                string strNew = "1";
                string strPass = Encode.Md5(strNew);
                string strSql = string.Format(@"update st_user set password = '{0}' where username = upper('{1}')", userName, strPass);
                SysParams.OAConnection().RunSql(strSql);
                return strNew;
            }
            return string.Empty;
        }

        /// <summary>
        /// 更新用户
        /// </summary>
        /// <param name="OfficeTel"></param>
        /// <param name="ExtNo"></param>
        /// <param name="userId"></param>
        /// <param name="HouseTel"></param>
        public void UpdateUser(string OfficeTel, string ExtNo, string userId, string HouseTel)
        {
            string strSql = string.Format("update st_user t set t.officetel='{0}', t.EXTNO='{1}',t.HOUSETEL ='{3}' where t.userid='{2}'", OfficeTel, ExtNo, userId, HouseTel);
            SysParams.OAConnection().RunSql(strSql);
        }
       
        /// <summary>
        /// 查询用户登录信息
        /// </summary>
        /// <param name="bmid"></param>
        /// <param name="sjbmlist"></param>
        /// <param name="QueCon"></param>
        /// <returns></returns>
        public static DataTable GetUserLoginInfo(string bmid, string sjbmlist, string QueCon)
        {
            string strSql = string.Format(@"SELECT 1 checked,UserId,login_name,User_Name,orderbyid FROM 
                ST_USER WHERE UserId IN(SELECT UserId FROM st_user_department WHERE Order_Id='{0}') 
                 UNION All SELECT 0 checked ,UserId,login_name,User_Name,orderbyid FROM  ST_USER WHERE 
                 UserId IN (SELECT UserId FROM st_user_department WHERE Order_Id IN 
                (SELECT DEPARTID FROM st_department WHERE DEPARTID='{1}' OR Parent_Id='{1}'))
                AND NOT EXISTS(SELECT UserId FROM  st_user_department WHERE UserId=St_User.UserId 
                and Order_Id='{0}') ",
                bmid, sjbmlist);

            if (String.IsNullOrEmpty(bmid))
            {
                strSql = "SELECT 0 checked ,UserId,login_name,User_Name,orderbyid FROM  ST_USER ";
            }

            if (!string.IsNullOrEmpty(QueCon))
            {
                strSql = string.Format(@"SELECT 1 checked,UserId,login_name,User_Name,orderbyid FROM  
                ST_USER WHERE UserId IN(SELECT UserId FROM st_user_department WHERE 
                Order_Id='{0}') UNION All SELECT 0 checked ,UserId,login_name,User_Name,orderbyid 
                FROM  ST_USER WHERE login_name like '%{1}%' or User_Name like '%{1}%'",
                    bmid, QueCon);
            }
            strSql += " order by checked desc,orderbyid asc ";

            System.Data.DataTable dt;
            SysParams.OAConnection().RunSql(strSql, out dt);
            return dt;
        }

        /// <summary>
        /// 校验用户信息
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        public DataSet ValideUser(string userid)
        {
            string strSql = "select * from st_user where userid='" + userid + "'";
            DataSet dsOut = null;
            SysParams.OAConnection().RunSql(strSql, out dsOut);
            return dsOut;
        }

        /// <summary>
        /// 检验密码是否匹配
        /// </summary>
        /// <param name="userId">用户Id</param>
        /// <param name="password">密码</param>
        /// <returns>密码匹配返回true,否则返回false</returns>
        private bool CheckPassword(string userId, string password)
        {
            string strDatabasePass = GetPassword(userId, false);
            return (strDatabasePass == password);
        }

        /// <summary>
        /// 获取密码。
        /// </summary>
        /// <param name="userId">用户Id</param>
        /// <param name="containLock">如果为 <c>true</c>，则获取时包含已锁定用户。</param>
        /// <returns>数据库中对应用户密码</returns>
        private string GetPassword(string userId, bool containLock)
        {
            string result = string.Empty;
            if (!string.IsNullOrEmpty(userId))
            {
                string strSql = string.Format("select password from st_user where userId='{0}'",
                    userId);

                result = SysParams.OAConnection().GetValue(strSql);
            }
            return result;
        }

        /// <summary>
        /// 根据用户id获取sessionid
        /// </summary>
        /// <param name="userId">用户id</param>
        /// <returns></returns>
        public static string GetSessionByUserId(string userId)
        {
            string strSql = string.Format("select SID from ST_SESSION where userId='{0}'",
                   userId);

            return SysParams.OAConnection().GetValue(strSql);
        }

        /// <summary>
        /// 处理更新成员资格用户密码的请求。
        /// </summary>
        /// <param name="userId">为其更新密码的用户Id</param>
        /// <param name="oldPassword">指定的用户的当前密码。</param>
        /// <param name="newPassword">指定的用户的新密码。</param>
        /// <returns>如果密码更新成功，则为 true；否则为 false。</returns>
        public bool ChangePassword(string userId, string oldPassword, string newPassword)
        {
            if (
                !(string.IsNullOrEmpty(userId) || string.IsNullOrEmpty(oldPassword) ||
                  string.IsNullOrEmpty(newPassword)))
            {
                //检查原密码是否正确
                if (!CheckPassword(userId, oldPassword))
                {
                    return false;
                }

                string strSql = string.Format("update st_user set password ='{0}' where userId = '{1}'", newPassword, userId);
                int nReturn = SysParams.OAConnection().RunSql(strSql);

                if (nReturn != 0)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// 根据用户id删除用户所属信息
        /// </summary>
        /// <param name="strUserId"></param>
        /// <!--
        /// 创建人  : LinJian
        /// 创建时间: 2007年5月23日
        /// -->
        public static void DeleteUserById(string strUserId)
        {
            IDataAccess dataAccess = SysParams.OAConnection(true);

            try
            {
                string strSql = string.Format("delete from st_user where userId = '{0}'", strUserId);
                dataAccess.RunSql(strSql);

                strSql = string.Format("delete from st_user_department where userid='{0}'", strUserId);
                dataAccess.RunSql(strSql);

                dataAccess.Close(true);
            }
            catch
            {
                dataAccess.Close(false);
                throw;
            }            
        }

        /// <summary>
        /// 根据id更新用户信息
        /// </summary>
        /// <param name="userInfo">用户信息</param>
        /// <!--
        /// 创建人  : LinJian
        /// 创建时间: 2007年5月26日
        /// -->
        public static void UpdateUserById(UserStruct userInfo)
        {
            IDataAccess dataAccess = SysParams.OAConnection(true);

            try
            {
                string strSql = string.Format(@"update ST_USER set LOGIN_NAME='{0}',USER_NAME='{1}',PASSWORD='{2}',MOBILE='{4}',ORDERBYID='{5}',INVALID='{6}',operation_flag =3 where userid='{3}'", userInfo.UserName, 
                      userInfo.UserRealName,userInfo.UserPass, userInfo.UserId,
                      userInfo.UserMobile,userInfo.UserOderbyid,
                      userInfo.UserInvalid);               
                dataAccess.RunSql(strSql);

                strSql = "delete from st_user_department where userid='" + userInfo.UserId + "'";
                dataAccess.RunSql(strSql);

                foreach (string depart in userInfo.UserDepartId)
                {
                    strSql = "insert into st_user_department(userid,order_id) values "
                        + "('" + userInfo.UserId + "'," + "'" + depart + "')";
                    dataAccess.RunSql(strSql);
                }
                dataAccess.Close(true);
            }
            catch
            {
                dataAccess.Close(false);
                throw;
            }
        }

        /// <summary>
        /// 根据组id获取用户信息
        /// </summary>
        /// <param name="strGroupId">组id</param>
        /// <returns></returns>
        /// <!--
        /// 创建人  : LinJian
        /// 创建时间: 2007年5月26日
        /// -->
        public static List<UserStruct> GetUserInfoByGroupId(string strGroupId)
        {
            string strSql = string.Format("select m.*,n.order_id from st_user m,st_user_department n where m.userid in (select userid from st_user_group where GID='{0}') and m.userid=n.userid",
                strGroupId);

            DataTable dtOut;
            SysParams.OAConnection().RunSql(strSql, out dtOut);

            List<UserStruct> lstUser = new List<UserStruct>();
            foreach (DataRow drTemp in dtOut.Rows)
            {
                UserStruct userStruct = new UserStruct();
                userStruct.UserId = drTemp["USERID"].ToString();
                userStruct.UserName = drTemp["LOGIN_NAME"].ToString();
                userStruct.UserRealName = drTemp["USER_NAME"].ToString();
                userStruct.UserPass = drTemp["PASSWORD"].ToString();
                userStruct.UserDepartmentId = drTemp["order_id"].ToString();

                lstUser.Add(userStruct);
            }

            return lstUser;
        }

        /// <summary>
        /// 根据组id获取用户id
        /// </summary>
        /// <param name="strGroupId">组id</param>
        /// <returns></returns>
        /// <!--
        /// 创建人  : LinJian
        /// 创建时间: 2007年5月26日
        /// -->
        public static List<string> GetUserIdByGroupId(string strGroupId)
        {
            string strSql = string.Format("select userid from st_user_group where GID='{0}'",
               strGroupId);

            DataTable dtOut;
            SysParams.OAConnection().RunSql(strSql, out dtOut);

            List<string> lstUserId = new List<string>();
            foreach (DataRow drTemp in dtOut.Rows)
            {
                lstUserId.Add(drTemp["USERID"].ToString());
            }

            return lstUserId;
        }

        /// <summary>
        /// 添加用户
        /// </summary>
        /// <param name="user">存储用户信息</param>
        /// <returns></returns>
        /// <!--
        /// 创建人  : LinJian
        /// 创建时间: 2007年5月26日
        /// -->
        public static void AddNewUser(UserStruct user)
        {
            IDataAccess dataAccess = SysParams.OAConnection(true);

            try
            {
                string strSql = string.Format("insert into ST_USER(USERID,LOGIN_NAME,USER_NAME,PASSWORD,orderbyid,mobile,operation_flag) values('{0}','{1}','{2}','{3}','{4}','{5}',1)", user.UserId, user.UserName, 
                    user.UserRealName,user.UserPass,user.UserOderbyid,user.UserMobile);
                dataAccess.RunSql(strSql);

                strSql = string.Format("insert into st_session(sid,userid) " +
                    "values('{0}','{1}')", user.UserId, user.UserId);
                dataAccess.RunSql(strSql);

                foreach (string depart in user.UserDepartId)
                {
                    strSql = string.Format("insert into st_user_department(userid,order_id) values ('{0}','{1}')",user.UserId,depart);
                    dataAccess.RunSql(strSql);
                }

                dataAccess.Close(true);
            }
            catch
            {
                dataAccess.Close(false);
                throw;
            }
        }

        /// <summary>
        /// 分配组成员
        /// </summary>
        /// <param name="strGroupId">组id</param>
        /// <param name="arrStrUserId">组成员id集合</param>
        /// <!--
        /// 创建人  : LinJian
        /// 创建时间: 2007年5月26日
        /// -->
        public static void AsignUserToGroup(string strGroupId,string[] arrStrUserId)
        {
            IDataAccess dataAccess = SysParams.OAConnection(true);

            try
            {
                string strSql = string.Format("delete from st_user_group where gid = '{0}'", strGroupId);
                dataAccess.RunSql(strSql);
                if (arrStrUserId != null)
                {
                    foreach (string strUserId in arrStrUserId)
                    {
                        strSql = string.Format("insert into st_user_group (userid, gid) values ('{0}','{1}')",
                            strUserId, strGroupId);
                        dataAccess.RunSql(strSql);
                    }
                }
                dataAccess.Close(true);
            }
            catch
            {
                dataAccess.Close(false);
                throw;
            }
        }

        /// <summary>
        /// 用户基本信息更新 所在部门不作变更 Operation_flag标志为置为2
        /// </summary>
        /// <param name="userInfo">用户信息</param>
        /// <returns></returns>
        public static void UpdateUserBasicInfo(UserStruct userInfo)
        {
            IDataAccess dataAccess = SysParams.OAConnection();
            string strSql = string.Format(@"update ST_USER set LOGIN_NAME='{0}',USER_NAME='{1}', 
                  PASSWORD='{2}',MOBILE='{4}',ORDERBYID='{5}',INVALID='{6}',operation_flag =2 where userid='{3}'", userInfo.UserName, 
                 userInfo.UserRealName,userInfo.UserPass, userInfo.UserId, userInfo.UserMobile,
                 userInfo.UserOderbyid, userInfo.UserInvalid);
            dataAccess.RunSql(strSql);
            dataAccess.Close();

        }

       
       
        /// <summary>
        /// 更新用户签名
        /// </summary>
        /// <param name="strUserId">用户id</param>
        /// <param name="bArrSignImage">二进制数组</param>
        /// <!--
        /// 创建人  : LinJian
        /// 创建时间: 2007年5月26日
        /// -->
        public static void InsertUserSign(string strUserId, byte[] bArrSignImage)
        {
            IDataAccess idsTemp = WF_Business.SysParams.OAConnection(true);

            try
            {
                //执行添加操作
                string sql = "select * from ST_USER_SIGN where USERID='0'";

                DataTable dtOut;
                idsTemp.RunSql(sql, out dtOut, true);

                //通过在datable中添加数据来更新
                DataRow drTemp = dtOut.NewRow();
                drTemp["USERID"] = strUserId;
                drTemp["SAVEDATE"] = DateTime.Now;
                drTemp["SIGNPIC"] = bArrSignImage;
                dtOut.Rows.Add(drTemp);

                idsTemp.Update(dtOut);

                //更新guid
                sql = string.Format("update ST_USER_SIGN set MAGICNUM=sys_guid() where USERID='{0}'",
                    strUserId);
                idsTemp.RunSql(sql);

                //执行提交
                idsTemp.Close(true);
            }
            catch
            {
                //执行回滚
                idsTemp.Close(false);
                throw;
            }
        }

        /// <summary>
        /// 读取登入信息
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public static IDataReader ReadUserLogin(string userID)
        {
            string strSql = "select SIGNPIC from ST_USER_SIGN where USERID='" + userID + "' order by savedate desc";
            System.Data.IDataReader idrGet = SysParams.OAConnection().GetDataReader(strSql);
            return idrGet;
        }

        /// <summary>
        /// 根据用户信息获取部门id
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        public static String UserDepartmentInfo(string userid)
        {
            IDataAccess dataAccess = SysParams.OAConnection();
            DataTable dt = new DataTable();
            string strSql = "select order_id from st_user_department where userid='" + userid + "'";
            dataAccess.RunSql(strSql, out dt);
            string dtStr = string.Empty;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dtStr += dt.Rows[i][0].ToString() + ",";
            }
            return dtStr;

        }

    }
}

