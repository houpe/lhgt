﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using WF_Business;

namespace Business.Admin
{
    public class SysSettingOperation
    {
        /// <summary>
        ///获取设置信息
        /// </summary>
        /// <returns></returns>
        public static DataTable GetAllSetting()
        {
            string strSql = @"select t.* from sys_setting t";
            DataTable dtReturn;

            SysParams.OAConnection().RunSql(strSql, out dtReturn);

            return dtReturn;
        }

        /// <summary>
        /// 根据设置参数名获取参数值.
        /// </summary>
        /// <param name="strName">The Setting name.</param>
        /// <returns></returns>
        public static DataTable GetSetting(string strName)
        {
            string strSql = string.Format(@"select t.* from sys_setting t where name='{0}'", strName);
            DataTable dtReturn;
            SysParams.OAConnection().RunSql(strSql, out dtReturn);

            return dtReturn;
        }

        /// <summary>
        /// 编辑设置信息.
        /// </summary>
        /// <param name="strName">设置参数名.</param>
        /// <param name="nValue">参数值.</param>
        public static int StoreSetting(string strName, int nValue)
        {
            string strSql = string.Format("select id from sys_setting t where t.name='{0}'", strName);
            string strSettingId = SysParams.OAConnection().GetValue(strSql);

            if (string.IsNullOrEmpty(strSettingId))
            {
                strSql = string.Format(@"insert into sys_setting t(name,value) 
                values('{0}','{1}')", strName, nValue);
            }
            else
            {
                strSql = string.Format(@"update sys_setting t set name='{0}',value='{1}' where id='{2}'",
                    strName, nValue, strSettingId);
            }

            return SysParams.OAConnection().RunSql(strSql);
        }

        /// <summary>
        /// 数据库中获取系统参数设置
        /// </summary>
        /// <param name="strName">Name of the STR.</param>
        /// <returns></returns>
        public static int GetSettingFromDb(string strName)
        {
            int nReturn = 0;
            if (!string.IsNullOrEmpty(strName))
            {
                string strSql = string.Format(@"select value from sys_setting where Name = upper('{0}')",strName);

                string strReturn = SysParams.OAConnection().GetValue(strSql);
                nReturn = BasicOperate.GetDataBaseInt(strReturn);
            }
            return nReturn;
        }
    }
}
