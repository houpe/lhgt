﻿ 
// 创建人  ：zhongjian
// 创建时间：2010年5月24日
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Data;
using System.Configuration;
using WF_Business;
namespace Business.Admin
{
    /// <summary>
    /// 系统信息操作类
    /// </summary>
    public class SystemManager
    {
        string strSql = string.Empty;//SQL语句字符
        DataTable dtTemp = new DataTable();

        /// <summary>
        /// 获取优先级分配
        /// </summary>
        /// <param name="strBusinessType">业务类型</param>
        /// <param name="strUnitName">单位名称</param>
        /// <param name="strStartDate">开始时间</param>
        /// <param name="strEndDate">结束时间</param>
        /// <returns></returns>
        /// <!--addby zhongjian 20100524-->
        public DataTable GetPriority(string strBusinessType, string strUnitName,string strStartDate,string strEndDate)
        {
            strSql = string.Format(@"select Rownum as num,f.wname,i.Name,to_char(i.accepted_time,'yyyy-mm-dd hh24:mi:ss') accptime,i.iid,i.priority  
                                    from st_instance i,st_workflow f Where i.wid=f.wid and i.status='1' ");
            if (!string.IsNullOrEmpty(strUnitName))
                strSql += string.Format(" and i.name like '%{0}%'", strUnitName);
            if (!string.IsNullOrEmpty(strStartDate))
                strSql += string.Format(" and i.ACCEPTED_TIME>=to_date('{0}','yyyy-mm-dd')", strStartDate);
            if (!string.IsNullOrEmpty(strEndDate))
                strSql += string.Format(" and i.ACCEPTED_TIME<to_date('{0}','yyyy-mm-dd')+1", strEndDate);
            if (!string.IsNullOrEmpty(strBusinessType))
                strSql += string.Format(" and f.wname = '{0}'", strBusinessType);

            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }

      
        /// <summary>
        /// 根据用户ID查询用户是否为系统管理员,如果是 返回 true
        /// </summary>
        /// <param name="userid">用户ID</param>
        /// <returns></returns>
        public static bool IsSystemUser(string userid)
        {
            string strSql = string.Format(@"select t.userid from st_user_group t,st_group p where p.group_name='管理员'
                and t.gid=p.groupid and t.userid='{0}'", userid);
            DataTable dt;
            SysParams.OAConnection().RunSql(strSql, out dt);
            bool bReturn = false;
            if (dt.Rows.Count > 0)
            {
                bReturn = true;
            }
            return bReturn;
        }


        public DataTable GetDepInfo()
        {
            strSql = string.Format(@"select distinct department_NAME,order_dep from XT_DEPARTMENT_SERIAL order by order_dep ");
            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }

    }
}
