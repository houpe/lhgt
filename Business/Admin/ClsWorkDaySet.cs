﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using WF_Business;

namespace Business.Admin
{
    /// <summary>
    /// 工作日设定操作类
    /// </summary>
    public class ClsWorkDaySet
    {
        /// <summary>
        /// 获取当前月已经设置工作时间的天
        /// </summary>
        /// <returns></returns>
        public DataTable  GetsetDays()
        {
            DataTable dtReturn = new DataTable();
            string strSql = string.Format("select * from st_worktime where timelengh>0");
            SysParams.OAConnection().RunSql(strSql, out dtReturn);
            return dtReturn;
        }
        /// <summary>
        /// 首先查询表中是否有所选择的日期的时间记录
        /// </summary>
        /// <param name="strDateTime"></param>
        /// <returns></returns>
        public DataTable GetWorkDay(string strDateTime)
        {
            DataTable dtReturn = new DataTable();
            string strSql = string.Format("select * from st_worktime where sdate like to_date('{0}','yyyy-mm-dd')", strDateTime);
            SysParams.OAConnection().RunSql(strSql, out dtReturn);  //首先查询表中是否有所选择的日期的时间记录
            return dtReturn;
        }
        /// <summary>
        /// 首先查询表中是否已经有该日的时间记录
        /// </summary>
        /// <param name="strDateTime">strDateTime</param>
        /// <returns>returns</returns>
        public DataTable GetWorkTime(string strDateTime)
        {
            DataTable dtReturn = new DataTable();
            string selectSql = string.Format("select * from st_worktime where sdate like to_date('{0}','yyyy-mm-dd hh24:mi:ss')", strDateTime);
            SysParams.OAConnection().RunSql(selectSql, out dtReturn);  
            return dtReturn; 
        }
        /// <summary>
        /// 如果有则删除这条记录
        /// </summary>
        /// <param name="strDataTime"></param>
        /// <returns></returns>
        public int DelWorkTime(string strDataTime)
        {
            int m =0;
            string strSql = string.Format("delete from st_worktime where sdate like to_date('{0}','yyyy-mm-dd hh24:mi:ss')", strDataTime);
            m = SysParams.OAConnection().RunSql(strSql);
            return m;
        }
        /// <summary>
        /// 然后插入新纪录
        /// </summary>
        /// <returns></returns>
        public int InsWorkTime(string txtFromDate, string monuptime ,string mondowntime,string afuptime,string afdowntime)
        {
            int n = 0;
            string strSql =string.Format(@"insert into st_worktime(sdate,mstime,metime,nstime,netime,timelengh) values(
                        to_date('{0}','yyyy-mm-dd hh24:mi:ss'),to_date('{1}','yyyy-mm-dd hh24:mi:ss'),
                        to_date('{2}','yyyy-mm-dd hh24:mi:ss'),to_date('{3}','yyyy-mm-dd hh24:mi:ss'),to_date('{4}','yyyy-mm-dd hh24:mi:ss'),
                        round(to_number(to_date('{2}','yyyy-mm-dd hh24:mi:ss')-to_date('{1}','yyyy-mm-dd hh24:mi:ss'))*1440*60)
                        +round(to_number(to_date('{4}','yyyy-mm-dd hh24:mi:ss')-to_date('{3}','yyyy-mm-dd hh24:mi:ss'))*1440*60))",
                        txtFromDate,monuptime,mondowntime,afuptime,afdowntime);
            n = SysParams.OAConnection().RunSql(strSql);
            return n;
        }

        /// <summary>
        /// 然后插入新纪录
        /// </summary>
        /// <returns></returns>
        public int InsWorkTime(string txtFromDate)
        {
            int n = 0;
            string strSql = string.Format( "insert into st_worktime(sdate,timelengh) values(to_date('{0}','yyyy-mm-dd hh24:mi:ss'),0)", txtFromDate);
            n = SysParams.OAConnection().RunSql(strSql);
            return n;
        }

        public static string GetSurplusWorkDay(object surplusHourObj)
        {
            double num2;
            if (string.IsNullOrEmpty(surplusHourObj.ToString()))
            {
                return "";
            }
            double num = SystemConfig.WorkHoursInEveryDay;
            if (num == 0)
            {
                return "工作日小时数不合法，无法计算";
            }
            double.TryParse(surplusHourObj.ToString(), out num2);
            double num3 = (num * 60) * 60;
            int num4 = (int)(num2 / num3);
            int num5 = (int)(num2 % num3);
            int num6 = num5 / 0xe10;
            num5 = num5 % 0xe10;
            int num7 = num5 / 60;
            string str2 = string.Empty;
            if (num4 > 0)
            {
                str2 = num4 + "天";
            }
            if (num6 > 0)
            {
                str2 = str2 + num6 + "时";
            }
            if (num7 > 0)
            {
                str2 = str2 + num7 + "分";
            }
            return str2;
        }

        /// <summary>
        /// 获取星期几
        /// </summary>
        /// <returns></returns>
        public static string GetDayOfWeek()
        {
            string[] arrStrZw = { "星期天", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六" };
            return arrStrZw[(int)DateTime.Today.DayOfWeek];
        }

        /// <summary>
        /// 将传入的秒数转换为系统参数中指定的时间单位，天为工作日
        /// </summary>
        /// <param name="dSeconds">秒数</param>
        /// <!--
        /// 修改人  : LinJian
        /// 修改时间: 2007年5月28日
        /// -->
        public static string ToDefaultTime(double dSeconds)
        {
            double dWorkTimeEveryDay = SystemConfig.WorkHoursInEveryDay;

            //折合成小时总数
            int nHoursCount = (int)(dSeconds / 3600);

            //计算剩下的总秒数
            int dLeftSeconds = (int)(dSeconds % 3600);

            //剩下的总秒数折算成分钟数
            int nMinutes = dLeftSeconds / 60;

            //剩下的总秒数
            int nSeconds = dLeftSeconds % 60;

            //将小时总数折算成天
            int nDays = 0;
            int nHours = 0;
            if (dWorkTimeEveryDay != 0.0)
            {
                nDays = (int)(nHoursCount / dWorkTimeEveryDay);
                nHours = (int)(nHoursCount % dWorkTimeEveryDay);
            }

            //获取剩下的小时数


            string strReturn = string.Format("{0}天{1}小时{2}分钟", nDays, nHours, nMinutes);
            return strReturn;
        }

        /// <summary>
        /// 查询步骤信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static DataTable SearchWorkItemInfo(string id)
        {
            //根据输入的业务ID,查找步骤信息
            string sql1 = "select * from (select step,user_name,to_char(accepted_time,'YYYY-MM-DD HH24:Mi:SS')accepted_time, ";
            sql1 += "to_char(Submit_time,'YYYY-MM-DD HH24:Mi:SS') Submit_time,WIID from st_work_item_hist ,st_user ";
            sql1 += "where st_user.userid=st_work_item_hist.userid and IID='" + id + "' union all ";
            sql1 += "select step,user_name,to_char(accepted_time,'YYYY-MM-DD HH24:Mi:SS'),'',WIID from st_work_item ,st_user ";
            sql1 += "where st_user.userid=st_work_item.userid and IID='" + id + "' and (active=0 or active=1)) order by wiid";

            DataTable rs;
            SysParams.OAConnection().RunSql(sql1, out rs);
            return rs;
        }

        /// <summary>
        /// 获取工作优先信息
        /// </summary>
        /// <param name="strSqdw"></param>
        /// <param name="strStartDate"></param>
        /// <param name="strEndDate"></param>
        /// <param name="strYwlx"></param>
        /// <returns></returns>
        public static DataTable GetWorkTime(string strSqdw, string strStartDate, string strEndDate, string strYwlx)
        {
            string strSql = "select Rownum as num,f.wname as wname,i.Name as name,to_char(i.accepted_time,'yyyy-mm-dd hh24:mi:ss') as time ,i.iid as iid ,i.priority as priority  from st_instance i,st_workflow f Where i.wid=f.wid and i.isdelete <>1";

            string where = "";

            if (!string.IsNullOrEmpty(strSqdw))
                where += " and (i.name like '%" + strSqdw + "%' OR to_char(i.iid)='" + strSqdw + "')";
            if (!string.IsNullOrEmpty(strStartDate))
                where += " and i.ACCEPTED_TIME>=to_date('" + strStartDate + "','yyyy-mm-dd')";
            if (!string.IsNullOrEmpty(strEndDate))
                where += " and i.ACCEPTED_TIME<to_date('" + strEndDate + "','yyyy-mm-dd')+1";
            if (!string.IsNullOrEmpty(strYwlx))
                where += " and f.wname = '" + strYwlx.Trim() + "' ORDER BY i.iid";

            strSql = strSql + where;

            System.Data.DataTable dtOut = new System.Data.DataTable();
            SysParams.OAConnection().RunSql(strSql, out dtOut);
            return dtOut;
        }
    }
}
