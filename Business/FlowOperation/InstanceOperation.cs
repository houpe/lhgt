﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using WF_Business;

namespace Business.FlowOperation
{
    /// <summary>
    /// st_instance表操作类
    /// </summary>
    public class InstanceOperation
    {
        public string AllCount(string strAppend)
        {
            string strSql = "select count(*) from st_instance a where 1=1 and a.isdelete <>1";
            strSql += strAppend;
            return SysParams.OAConnection().GetValue(strSql);
        }

        public string Processing(string strAppend)
        {
            string strSql = "select count(*) from st_instance a where status<>'2' and a.isdelete <>1";
            strSql += strAppend;
            return SysParams.OAConnection().GetValue(strSql);
        }

        public string HaveProcessed(string strAppend)
        {
            string strSql = "select count(*) from st_instance a where status='2' and a.isdelete <>1";
            strSql += strAppend;
            return SysParams.OAConnection().GetValue(strSql);
        }

        public string Stop(string strAppend)
        {
            string strSql = "select count(*) from st_instance a where status='-2' and a.isdelete <>1";
            strSql += strAppend;
            return SysParams.OAConnection().GetValue(strSql);
        }

        public string GetStatusById(string id)
        {
            string s_GDstate;
            string tempsql = string.Format("select status from st_instance where iid='{0}' and isdelete <>1", id);
            string strStatus = SysParams.OAConnection().GetValue(tempsql);

            if (string.IsNullOrEmpty(strStatus))
            {
                s_GDstate = "未办结";
            }
            else
            {
                if (strStatus.Equals("2") || strStatus.Equals("3"))
                {
                    s_GDstate = "办结";//归档状态
                }
                else
                {
                    s_GDstate = "未办结";
                }
            }
            return s_GDstate;
        }

        public DataTable GetInfo(string userid, string struserno, string strusername, bool ISsystemUser)
        {
            
            string strSql = "select a.iid,a.name from st_instance a,st_workflow b where 1=1 and a.wid=b.wid and a.isdelete <>1";

            if (!string.IsNullOrEmpty(struserno))
            {
                strSql += (" and iid like '%" + struserno + "%'");
            }

            if (!string.IsNullOrEmpty(strusername))
            {
                strSql += (" and name like '%" + strusername + "%'");
            }

            if (!ISsystemUser)//不是系统管理员
            {
                strSql += @" and b.wname in 
                                (select t.task  from xt_query_right t  where t.userid = '" + userid + "')";
            }

            DataTable dtOut;
            SysParams.OAConnection().RunSql(strSql, out dtOut);
            return dtOut;
        }

        /// <summary>
        /// 获取案例信息
        /// </summary>
        /// <param name="strIid">案件编号</param>
        /// <param name="strName">申请人姓名</param>
        /// <param name="strWid">流程id</param>
        /// <returns></returns>
        public DataTable GetSerialInfo(string strIid, string strName, string strWid)
        {
            string strSql = "select a.iid,a.name from st_instance a where isdelete <>1";
            if (!string.IsNullOrEmpty(strIid))
            {
                strSql += string.Format(" and iid like '%{0}%'", strIid);
            }
            if (!string.IsNullOrEmpty(strName))
            {
                strSql += string.Format(" and name like '%{0}%'", strName);
            }
            if (!string.IsNullOrEmpty(strWid))
            {
                strSql += string.Format(" and a.wid='{0}'", strWid);
            }

            DataTable dtOut;
            SysParams.OAConnection().RunSql(strSql, out dtOut);
            return dtOut;
        }

        /// <summary>
        /// 根据iid获取wid
        /// </summary>
        /// <param name="strWid">The STR wid.</param>
        /// <returns></returns>
        public string GetMaxIidByWid(string strWid)
        {
            string strSql = string.Format("select max(iid) from st_instance a where wid='{0}'", strWid);

            return SysParams.OAConnection().GetValue(strSql);
        }
    }
}
