﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using WF_DataAccess;
using WF_Business;

namespace Business.FlowOperation
{
    /// <summary>
    /// 流程打开时操作类
    /// </summary>
    public class TaskOpenOperation
    {
        public void ExecProdureTaskOpen(string strStepName,string strIid)
        {
            //处理关联数据
            IDataParameter[] idParms = DataFactory.GetParameter(DatabaseType.Oracle, 3);
            idParms[0].ParameterName = "chufaType";
            idParms[0].Value = 2;
            idParms[1].ParameterName = "chufaNode";
            idParms[1].Value = strStepName;
            idParms[2].ParameterName = "iSerial";
            idParms[2].Value = long.Parse(strIid);
            SysParams.OAConnection().RunProc("Base.GetTableContent", ref idParms);
        }
    }

}
