﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;
using WF_Business;
using WF_DataAccess;

namespace Business.FlowOperation
{
    /// <summary>
    /// 对应ST_USERDEFINETEXT表的操作
    /// </summary>
    public class UserDefneText
    {
        /// <summary>
        /// 
        /// </summary>
        private static Dictionary<string, string[]> DicDefTexts = null;
        public static Hashtable htDefinedComment = new Hashtable();
        public static Hashtable htDefinedText = new Hashtable();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="comment"></param>
        /// <returns></returns>
        public static string GetDefaultText(string id, out string comment)
        {
            SetDefTexts();
            string text = "";
            comment = "";
            if (DicDefTexts.ContainsKey(id))
            {
                string[] strArray = DicDefTexts[id];
                text = strArray[0];
                comment = strArray[1];
            }
            if (text != "")
            {
                SetDefineText(id, text, comment);
            }
            return text;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="comment"></param>
        /// <returns></returns>
        public static string GetDefineText(string id, out string comment)
        {
            DataTable table;
            string defaultText = "";
            comment = "-1";
            if (htDefinedText[id] != null)
            {
                defaultText = htDefinedText[id].ToString();
                comment = htDefinedComment[id].ToString();
                return defaultText;
            }
            string idPart = GetIdPart(id, 0);
            string str3 = GetIdPart(id, 1);
            if (string.IsNullOrEmpty("cid"))
            {
                return "-1";
            }
            string sql = "select value,susage from st_userdefinetext where spage='" + idPart + "' and skey='" + str3 + "'";
            SysParams.OAConnection().RunSql(sql, out table);
            if (table.Rows.Count < 1)
            {
                defaultText = GetDefaultText(id, out comment);
                if (defaultText == "")
                {
                    return "-1";
                }
            }
            else
            {
                defaultText = table.Rows[0]["value"].ToString();
                comment = table.Rows[0]["susage"].ToString();
            }
            htDefinedText[id] = defaultText;
            htDefinedComment[id] = comment;
            return defaultText;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="part"></param>
        /// <returns></returns>
        private static string GetIdPart(string id, int part)
        {
            string[] strArray = id.Split(new char[] { '_' });
            if (part < strArray.Length)
            {
                return strArray[part];
            }
            return "";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="text"></param>
        /// <param name="comment"></param>
        public static void SetDefineText(string id, string text, string comment)
        {
            string idPart = GetIdPart(id, 0);
            string str2 = GetIdPart(id, 1);
            int count = 1;
            if (!string.IsNullOrEmpty(comment))
            {
                count++;
            }
            IDataParameter[] parameter = DataFactory.GetParameter(DataFactory.DefaultDbType, count);
            parameter[0].ParameterName = "value";
            parameter[0].Value = text;
            if (!string.IsNullOrEmpty(comment))
            {
                parameter[1].ParameterName = "susage";
                parameter[1].Value = comment;
            }
            string sql = "select count(*) from st_userdefinetext where spage='" + idPart + "' and skey='" + str2 + "'";
            if (SysParams.OAConnection().GetValue(sql).Trim() != "0")
            {
                sql = "update st_userdefinetext set value=:value{0} where spage='" + idPart + "' and skey='" + str2 + "'";
                string str4 = "";
                if (!string.IsNullOrEmpty(comment))
                {
                    str4 = ",susage=:susage";
                }
                sql = string.Format(sql, str4);
                if (DataFactory.DefaultDbType == DatabaseType.SqlServer)
                {
                    sql = sql.Replace(":", "@");
                }
                SysParams.OAConnection().RunSql(sql, ref parameter);
            }
            else
            {
                sql = "insert into st_userdefinetext(id, spage, skey, value{0}) values('" + Guid.NewGuid().ToString() + "', '" + idPart + "', '" + str2 + "', :value{1})";
                if (string.IsNullOrEmpty(comment))
                {
                    sql = string.Format(sql, "", "");
                }
                else
                {
                    sql = string.Format(sql, ", susage", ", :susage");
                }
                if (DataFactory.DefaultDbType == DatabaseType.SqlServer)
                {
                    sql = sql.Replace(":", "@");
                }
                SysParams.OAConnection().RunSql(sql, ref parameter);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private static void SetDefTexts()
        {
            if (DicDefTexts == null)
            {
                DicDefTexts = new Dictionary<string, string[]>();
                DicDefTexts["BusinessManage_setprior"] = new string[] { "优先级设置", "业务管理-优先级设置" };
                DicDefTexts["ChangePassword_title"] = new string[] { "用户密码修改", "用户密码修改" };
                DicDefTexts["ChangePassword_id"] = new string[] { "帐号名：", "用户密码修改-帐号名：" };
                DicDefTexts["ChangePassword_oldpassword"] = new string[] { "原密码：", "用户密码修改-原密码：" };
                DicDefTexts["ChangePassword_newpassword"] = new string[] { "新密码：", "用户密码修改-新密码：" };
                DicDefTexts["ChangePassword_newpasswordconfirm"] = new string[] { "再确认：", "用户密码修改-再确认：" };
                DicDefTexts["ConfigSettings_title"] = new string[] { "个人参数配置", "用户参数修改-标题" };
                DicDefTexts["ConfigSettings_control"] = new string[] { "客户端控件", "用户参数修改-客户端控件" };
                DicDefTexts["DelegateRight_wid"] = new string[] { "待委托流程", "权利委托-待委托流程" };
                DicDefTexts["DelegateRight_to"] = new string[] { "委托给", "权利委托-委托给" };
                DicDefTexts["DelegateRight_begin"] = new string[] { "委托开始时间", "权利委托-委托开始时间" };
                DicDefTexts["DelegateRight_history"] = new string[] { "本人委托历史记录", "权利委托-本人委托历史记录" };
                DicDefTexts["InstaceEfficiency_lsUser"] = new string[] { "选择用户", "个人在办业务效能分析-选择用户" };
                DicDefTexts["InstanceSelfList_status"] = new string[] { "状态：", "经办业务查询-状态：" };
                DicDefTexts["InstanceSelfList_openinstance"] = new string[] { "处理", "经办业务查询-处理" };
                DicDefTexts["InstanceSelfList_flowedstep"] = new string[] { "流转过程", "经办业务-流转过程" };
                DicDefTexts["InstanceSelfList_openinstancelink"] = new string[] { "打开", "经办业务查询-打开业务" };
                DicDefTexts["InstanceSelfList_flowedsteplink"] = new string[] { "查看", "经办业务查询-查看业务" };
                DicDefTexts["SuperviseList_detailbutton"] = new string[] { "督办", "业务督办-督办" };
                DicDefTexts["SuperviseList_detailbutton2"] = new string[] { "督办", "业务督办-督办链接" };
                DicDefTexts["SuperviseDetail_AddHint"] = new string[] { "督办信息", "督办详情-新增督办信息" };
                DicDefTexts["SuperviseDetail_Title"] = new string[] { "督办列表", "督办详情-标题" };
                DicDefTexts["InstanceTrace_queryTerm"] = new string[] { "查询条件", "业务跟踪-查询条件" };
                DicDefTexts["InstanceTrace_applyperson"] = new string[] { "申请人：", "业务跟踪-申请人：" };
                DicDefTexts["InstanceTrace_applydate"] = new string[] { "申请日期：", "业务跟踪-申请日期：" };
                DicDefTexts["InstanceTrace_iidlist"] = new string[] { "业务列表", "业务跟踪-业务列表" };
                DicDefTexts["InstanceTrace_cancelTrace"] = new string[] { "取消跟踪", "业务跟踪-取消跟踪" };
                DicDefTexts["InteriorMsg_selsend"] = new string[] { "已发送已接收：", "站内消息-已发送已接收：" };
                DicDefTexts["InteriorMsg_msgtype"] = new string[] { "类型：", "站内消息-类型：" };
                DicDefTexts["InteriorMsg_sender"] = new string[] { "发送人：", "站内消息-发送人：" };
                DicDefTexts["InteriorMsg_senderh"] = new string[] { "发送人", "站内消息-发送人" };
                DicDefTexts["InteriorMsg_receiver"] = new string[] { "接收人：", "站内消息-接收人：" };
                DicDefTexts["InteriorMsg_receiverh"] = new string[] { "接收人", "站内消息-接收人" };
                DicDefTexts["InteriorMsg_sendtime"] = new string[] { "发送时间：", "站内消息-发送时间：" };
                DicDefTexts["InteriorMsg_sendtimeh"] = new string[] { "发送时间", "站内消息-发送时间" };
                DicDefTexts["InteriorMsg_status"] = new string[] { "状态", "站内消息-状态" };
                DicDefTexts["InteriorMsg_type"] = new string[] { "类别", "站内消息-类别" };
                DicDefTexts["InteriorMsg_content"] = new string[] { "内容", "站内消息-内容" };
                DicDefTexts["InteriorMsg_operation"] = new string[] { "操作", "站内消息-操作" };
                DicDefTexts["InteriorMsg_detailcontent"] = new string[] { "内容", "站内消息-详细内容" };
                DicDefTexts["JobHandover_Title"] = new string[] { "工作移交", "工作移交-标题" };
                DicDefTexts["JobHandover_to"] = new string[] { "移交给", "工作移交-移交给" };
                DicDefTexts["JobHandover_recordtitle"] = new string[] { "本人工作移交历史记录", "工作移交-本人工作移交历史记录" };
                DicDefTexts["Login_username"] = new string[] { "用户名：", "登录-用户名" };
                DicDefTexts["Login_password"] = new string[] { "密&nbsp;&nbsp;&nbsp; 码：", "登录-密码" };
                DicDefTexts["Login_VCode"] = new string[] { "验证码：", "登录-验证码" };
                DicDefTexts["MyApplyBusiness_applytime"] = new string[] { "申请时间", "个人申请业务-申请时间" };
                DicDefTexts["NewInstance_wid"] = new string[] { "业务类型", "新建业务-业务类型" };
                DicDefTexts["NewInstance_widname"] = new string[] { "流程名称", "新建业务-流程名称" };
                DicDefTexts["NewInstance_Department"] = new string[] { "所属部门", "新建业务-所属部门" };
                DicDefTexts["NewInstance_user"] = new string[] { "选择用户", "新建业务-选择用户" };
                DicDefTexts["NewInstance_userselected"] = new string[] { "已选用户", "新建业务-已选用户" };
                DicDefTexts["NewInstance_iidname"] = new string[] { "业务名称", "新建业务-业务名称" };
                DicDefTexts["NewInstance_expectedtime"] = new string[] { "预计办理时间", "新建业务-预计办理时间" };
                DicDefTexts["NewInstance_alarmtime"] = new string[] { "警告时间", "新建业务-警告时间" };
                DicDefTexts["NewInstance_Memo"] = new string[] { "申请事项说明", "新建业务-申请事项说明" };
                DicDefTexts["NewInstance_prior"] = new string[] { "优先级", "新建业务-优先级" };
                DicDefTexts["NewInstance_openInstance"] = new string[] { "直接处理", "新建业务-直接处理" };
                DicDefTexts["NewInstance_openInsList"] = new string[] { "转到列表", "新建业务-转到列表" };
                DicDefTexts["NewInstance_staycurrpage"] = new string[] { "继续申请", "新建业务-继续申请" };
                DicDefTexts["NoticeAddEdit_name"] = new string[] { "公告接收人", "公告发布、编辑-公告接收人" };
                DicDefTexts["NoticeAddEdit_selectuser"] = new string[] { "选择用户", "公告发布、编辑-选择用户" };
                DicDefTexts["NoticeAddEdit_selecteduser"] = new string[] { "已选用户", "公告发布、编辑-已选用户" };
                DicDefTexts["NoticeLst_Name"] = new string[] { "公告名称：", "公告信息-公告名称：" };
                DicDefTexts["NoticeLst_nameh"] = new string[] { "公告名称", "公告信息-公告名称" };
                DicDefTexts["NoticeLst_time"] = new string[] { "发布时间：", "公告信息-发布时间：" };
                DicDefTexts["NoticeLst_timeh"] = new string[] { "发布时间：", "公告信息-发布时间：" };
                DicDefTexts["NoticeLst_content"] = new string[] { "公告内容：", "公告信息-公告内容：" };
                DicDefTexts["NoticeLst_contenth"] = new string[] { "公告内容", "公告信息-公告内容" };
                DicDefTexts["NoticeLst_attach"] = new string[] { "有无附件：", "公告信息-有无附件：" };
                DicDefTexts["NoticeLst_attachh"] = new string[] { "附件", "公告信息-附件" };
                DicDefTexts["NoticeLst_operation"] = new string[] { "操作", "公告信息-操作" };
                DicDefTexts["SelectReceiveUser_content"] = new string[] { "用户姓名", "公告消息选择接收人-用户姓名" };
                DicDefTexts["ShareDoc_no"] = new string[] { "文档编号：", "他人共享文档-文档编号：" };
                DicDefTexts["ShareDoc_name"] = new string[] { "文档名称：", "他人共享文档-文档名称：" };
                DicDefTexts["ShareDoc_savetype"] = new string[] { "保管类型：", "他人共享文档-保管类型：" };
                DicDefTexts["ShareDoc_creater"] = new string[] { "创建者", "他人共享文档-创建者" };
                DicDefTexts["ShareDoc_uploaddate"] = new string[] { "上传日期", "他人共享文档-上传日期" };
                DicDefTexts["ShareDoc_owner"] = new string[] { "所有者", "他人共享文档-所有者" };
                DicDefTexts["ShowDocDetails_name"] = new string[] { "文档名称", "文档详细信息-文档名称" };
                DicDefTexts["ShowDocDetails_no"] = new string[] { "文档编号", "文档详细信息-文档编号" };
                DicDefTexts["ShowDocDetails_savetype"] = new string[] { "保管类型", "文档详细信息-保管类型" };
                DicDefTexts["ShowDocDetails_position"] = new string[] { "位置", "文档详细信息-位置" };
                DicDefTexts["ShowDocDetails_owner"] = new string[] { "文档所有人", "文档详细信息-文档所有人" };
                DicDefTexts["ShowDocDetails_keyword"] = new string[] { "关键字", "文档详细信息-关键字" };
                DicDefTexts["ShowDocDetails_uploadfile"] = new string[] { "上传文件", "文档详细信息-上传文件" };
                DicDefTexts["ShowDocDetails_section"] = new string[] { "所在区域", "文档详细信息-所在区域" };
                DicDefTexts["ShowDocDetails_keeper"] = new string[] { "保管人", "文档详细信息-保管人" };
                DicDefTexts["ShowDocDetails_keepertel"] = new string[] { "保管人联系方式", "文档详细信息-保管人联系方式" };
                DicDefTexts["ShowDocDetails_description"] = new string[] { "描述", "文档详细信息-描述" };
                DicDefTexts["ShowMsgDetails_receiver"] = new string[] { "收信人", "公告消息详情-收信人" };
                DicDefTexts["ShowMsgDetails_content"] = new string[] { "内容", "公告消息详情-内容" };
                DicDefTexts["WorkflowSuspend_iid"] = new string[] { "业务编号", "挂起业务-业务编号" };
                DicDefTexts["WorkflowSuspend_startDate"] = new string[] { "开始日期", "挂起业务-开始日期" };
                DicDefTexts["WorkflowSuspend_endDate"] = new string[] { "结束日期", "挂起业务-结束日期" };
                DicDefTexts["WorkflowUserPotency_WorkFlowType"] = new string[] { "案件类型", "效能分析-案件类型" };
                DicDefTexts["WorkflowUserPotency_timespan"] = new string[] { "案件接收时间", "效能分析-案件接收时间" };
                DicDefTexts["WorkItemDetails_no"] = new string[] { "序号", "流转过程-序号" };
                DicDefTexts["WorkItemDetails_notice"] = new string[] { "备注", "流转过程-备注" };
                DicDefTexts["WorkItemDetails_prestep"] = new string[] { "前一岗位", "流转过程-前一岗位" };
                DicDefTexts["WorkItemDetails_preuser"] = new string[] { "前一用户", "流转过程-前一用户" };
                DicDefTexts["WorkItemDetails_accepttime"] = new string[] { "接收时间", "流转过程-接收时间" };
                DicDefTexts["WorkItemDetails_currstep"] = new string[] { "当前岗位", "流转过程-当前岗位" };
                DicDefTexts["WorkItemDetails_curruser"] = new string[] { "当前用户", "流转过程-当前用户" };
                DicDefTexts["WorkItemDetails_submittime"] = new string[] { "提交时间", "流转过程-提交时间" };
                DicDefTexts["WorkItemDetails_opration"] = new string[] { "操作", "流转过程-操作" };
                DicDefTexts["WorkItemDetails_status"] = new string[] { "状态", "流转过程-状态" };
                DicDefTexts["WorkItemList_wid"] = new string[] { "业务类型：", "业务处理列表-业务类型：" };
                DicDefTexts["WorkItemList_priority"] = new string[] { "优先级：", "业务处理列表-优先级：" };
                DicDefTexts["WorkItemList_iid"] = new string[] { "业务编号：", "业务处理列表-业务编号：" };
                DicDefTexts["WorkItemList_iidname"] = new string[] { "业务名称：", "业务处理列表-业务名称：" };
                DicDefTexts["WorkItemList_CurrUser"] = new string[] { "当前用户：", "业务处理列表-当前用户：" };
                DicDefTexts["WorkItemList_deal"] = new string[] { "处理", "业务处理列表-处理" };
                DicDefTexts["WorkItemList_SuperVise"] = new string[] { "督办查看", "业务处理列表-督办查看" };
                DicDefTexts["WorkItemList_deal2"] = new string[] { "处理", "业务处理列表-处理" };
                DicDefTexts["WorkItemList_quickdeal"] = new string[] { "快速提交", "业务处理列表-快速提交" };
                DicDefTexts["WorkItemList_quickdeal2"] = new string[] { "快速提交", "业务处理列表-快速提交" };
            }
        }

        // Nested Types
        private class DefieTextValue
        {
            // Fields
            public string dispid = string.Empty;
            public string susage = string.Empty;
            public string value = string.Empty;
        }
    }


}
