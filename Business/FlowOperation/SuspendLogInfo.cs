﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using WF_DataAccess;
using Business;
using Business.Admin;
using WF_Business;
namespace Business.FlowOperation
{
    /// <summary>
    /// 挂起信息操作类
    /// </summary>
    public class SuspendLogInfo
    {
        /// <summary>
        /// 查询挂起日志
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="startData"></param>
        /// <param name="endData"></param>
        /// <returns></returns>
        public DataTable GetSuspendLog(string userid, string startData, string endData, string iid)
        {
            string strStart = "";
            string strEnd = "";
            string strIID = "";
            string strApplyStart = "";
            string strApplyEnd = "";
            string strUserid = "";
            if (!string.IsNullOrEmpty(startData))
            {
                strStart = string.Format(" and b.start_date >=to_date('{0}','yyyy-MM-dd')", startData);
                strApplyStart = string.Format(" and a.apply_time >=to_date('{0}','yyyy-MM-dd')", startData);
            }
            if (!string.IsNullOrEmpty(endData))
            {
                endData = Convert.ToDateTime(endData).AddDays(1).ToShortDateString();
                strEnd = string.Format(" and b.start_date < to_date('{0}','yyyy-MM-dd')", endData);
                strApplyEnd = string.Format(" and a.apply_time < to_date('{0}','yyyy-MM-dd')", endData);
            }
            if (!string.IsNullOrEmpty(iid))
            {
                strIID = string.Format(" and a.serial='{0}'", iid);
            }

            bool IsSystemUser = SystemManager.IsSystemUser(userid);
            if (!IsSystemUser)
            {
                strUserid = string.Format(" and a.userid = '{0}'", userid);
            }
            string strOrder = " order by start_date desc";
            string strOrder1 = " order by apply_time desc";
            string strSql = string.Format(@"select * from (select b.iid,b.start_date,b.end_date,b.description,
                                        (select user_name from st_user t where t.userid = a.userid) sqr,
                                        (select user_name from st_user t where t.userid = b.userid) gqr,
                                        '1' flag
                                        from xt_suspend_apply a, st_suspend b
                                        where b.xt_suspend_id = a.id 
                                        and b.end_date is not null {1} {2} {3} {6} {7} ) M
                                         union all 
                                         select * from(select a.serial,a.apply_time startData,a.delete_time endDate,a.memo,
                                         (select user_name from st_user t where t.userid = a.userid) sqr,
                                         (select user_name from st_user t where t.userid = a.checkuserid) gqr,
                                         '2' flag
                                         from xt_suspend_apply a
                                         where a.isdelete = 1 and a.userid = a.checkuserid {3} {4} {5} {6} {8}) N
                                        ", userid, strStart, strEnd, strIID, strApplyStart, strApplyEnd, strUserid, strOrder, strOrder1);
            DataTable dt = new DataTable();
            SysParams.OAConnection().RunSql(strSql, out dt);
            return dt;
        }
    }
}
