﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.Security;
using Microsoft.Practices.EnterpriseLibrary.Data;
using WF_DataAccess;
using WF_Business;

namespace Business.Menu
{
    /// <!--
    /// 功能描述  : 菜单操作类
    /// 创建人  : LinJian
    /// 创建时间: 2006-09-04
    /// -->
    public class XtMenuOperation : Page
    {
        #region 迁移菜单操作方法
        /// <summary>
        /// 获取父菜单
        /// </summary>
        /// <returns></returns>
        public DataTable GetParentMenu()
        {
            string strSql = "select * from xt_menu_parent";

            DataTable dtparentmenu;
            SysParams.OAConnection().RunSql(strSql, out dtparentmenu);
            return dtparentmenu;
        }


        /// <summary>
        /// 添加子菜单
        /// </summary>
        /// <returns></returns>
        public void AddChildMenu(string parentmenu,string menuname,string menuUrl,string picture)
        {
            string MaxParid = "select max(show_no)+1 from xt_menu_child where ancestor like '" + parentmenu + "'";
            DataTable dtMaxParId;
            SysParams.OAConnection().RunSql(MaxParid, out dtMaxParId);
            string maxshowNo = "";
            if (dtMaxParId.Rows[0][0].ToString() != "")
                maxshowNo = dtMaxParId.Rows[0][0].ToString();
            else
                maxshowNo = "0";

            string insertmenu = "insert into xt_menu_child(menu_child_id,menu_child_desc,ancestor,linkhref,overpic,show_no) values(sys_guid(),'" + menuname + "','" + parentmenu + "','" + menuUrl + "','"+ picture + "','" + maxshowNo + "')";
            int i = SysParams.OAConnection().RunSql(insertmenu);

            string strId = "select menu_child_id from xt_menu_child where menu_child_desc like '" + menuname + "'";
            DataTable dtchildid;
            SysParams.OAConnection().RunSql(strId, out dtchildid);

            string InsertMenuGroup = "insert into xt_group_menu(right_id,menu_child_id) values('d33c0003-62c9-7399-7c70-b1b5ef62eb29','" + dtchildid.Rows[0][0].ToString() + "')";
            int j = SysParams.OAConnection().RunSql(InsertMenuGroup);
        }

        /// <summary>
        /// 生成父菜单
        /// </summary>
        /// <returns></returns>
        public DataTable GenerateParentMenu()
        {
            string strSql = "select a.* from xt_menu_parent a order by show_no";
            DataTable dtparentmenu;
            SysParams.OAConnection().RunSql(strSql, out dtparentmenu);
            return dtparentmenu;
        }

        /// <summary>
        /// 生成子级菜单
        /// </summary>
        /// <param name="strParentId"></param>
        /// <returns></returns>
        public DataTable GenerateChildMenu(string strParentId, string userid)
        {
            string strSql = string.Format(@"select a.* from xt_menu_child a, xt_menu_parent b
                 where a.ancestor = b.menu_parent_id and a.ancestor = '{0}'
                 and  menu_child_id in(select menu_child_id from xt_group_menu where right_id in(select groupid
  from st_group a
 where a.groupid in
       (select gid
          from st_user_group
         where userid = '{1}')))                                     
                 order by a.show_no", strParentId, userid);
            System.Data.DataTable dtReturn;
            SysParams.OAConnection().RunSql(strSql, out dtReturn);
            return dtReturn;
        }

        /// <summary>
        /// 获取取子菜单
        /// </summary>
        /// <param name="menu_parent_id"></param>
        /// <returns></returns>
        public DataTable GetChildMenu(string menu_parent_id)
        {
            string strSqlmenuchild = "select * from xt_menu_child where ancestor like '" + menu_parent_id + "' order by show_no";
            System.Data.DataTable dtchild;
            SysParams.OAConnection().RunSql(strSqlmenuchild, out dtchild);
            return dtchild;
        }
        /// <summary>
        /// 更新子菜单
        /// </summary>
        /// <param name="menuname"></param>
        /// <param name="parentmenu"></param>
        /// <param name="menuUrl"></param>
        /// <param name="picture"></param>
        /// <param name="childid"></param>
        public void UpdateChildMenu(string menuname, string parentmenu, string menuUrl, string picture, string childid, string strShowNo)
        {
            string UpdateMenu = string.Format("update xt_menu_child set menu_child_desc='{0}',ancestor='{1}',linkhref='{2}',overpic='{3}',SHOW_NO='{4}'  where menu_child_id ='{5}'", menuname, parentmenu, menuUrl, picture, strShowNo, childid);
            SysParams.OAConnection().RunSql(UpdateMenu);
        }
        /// <summary>
        /// 删除子菜单
        /// </summary>
        /// <param name="delmenuchildid"></param>
        public void DeleteChildMenu(string delmenuchildid)
        {
            string delmenugroup = "delete from xt_group_menu where menu_child_id like '" + delmenuchildid + "'";
            int i = SysParams.OAConnection().RunSql(delmenugroup);
            string delmenu = "delete from xt_menu_child where menu_child_id like '" + delmenuchildid + "'";
            int j = SysParams.OAConnection().RunSql(delmenu);
        }
        /// <summary>
        /// 删除菜单组
        /// </summary>
        /// <param name="delmenuparentid"></param>
        public void DeleteMenuGroup(string delmenuparentid)
        {
            string delmenupar = "delete from xt_menu_parent where menu_parent_id like '" + delmenuparentid + "'";
            int k = SysParams.OAConnection().RunSql(delmenupar);
        }
        /// <summary>
        /// 上移子菜单
        /// </summary>
        /// <param name="ChildMno"></param>
        /// <param name="ChildMupParId"></param>
        /// <param name="ChildMupId"></param>
        public void MoveMenu(int ChildMno, string ChildMupParId, string ChildMupId)
        {
            int newId = ChildMno - 1;
            string UpshowNo = "update xt_menu_child set show_no=" + ChildMno + " where show_no=" + newId + " and ancestor like '" + ChildMupParId + "'";
            int i = SysParams.OAConnection().RunSql(UpshowNo);

            string UpshowNoNew = "update xt_menu_child set show_no=" + newId + " where menu_child_id like '" + ChildMupId + "' and ancestor like '" + ChildMupParId + "'";
            int j = SysParams.OAConnection().RunSql(UpshowNoNew);
        }
        /// <summary>
        /// 下移子菜单
        /// </summary>
        /// <param name="ChildMdownno"></param>
        /// <param name="ChildMdownParId"></param>
        /// <param name="ChildMdownId"></param>
        public void MoveDownMenu(int ChildMdownno, string ChildMdownParId, string ChildMdownId)
        {
            int newId = ChildMdownno + 1;
            string UpshowNo = "update xt_menu_child set show_no=" + ChildMdownno + " where show_no=" + newId + " and ancestor like '" + ChildMdownParId + "'";
            int i = SysParams.OAConnection().RunSql(UpshowNo);

            string UpshowNoNew = "update xt_menu_child set show_no=" + newId + " where menu_child_id like '" + ChildMdownId + "' and ancestor like '" + ChildMdownParId + "'";
            int j = SysParams.OAConnection().RunSql(UpshowNoNew);
        }
        /// <summary>
        /// 上移菜单组
        /// </summary>
        /// <param name="ParUpNo"></param>
        /// <param name="ParUpId"></param>
        public void MoveUpMenuGroup(int ParUpNo, string ParUpId)
        {
            int NewNo = ParUpNo - 1;
            string UpparNo = "update xt_menu_parent set show_no=" + ParUpNo + " where show_no=" + NewNo;
            int i = SysParams.OAConnection().RunSql(UpparNo);

            string UpparNonew = "update xt_menu_parent set show_no=" + NewNo + " where menu_parent_id like '" + ParUpId + "'";
            int j = SysParams.OAConnection().RunSql(UpparNonew);
        }
        /// <summary>
        /// 下移菜单组
        /// </summary>
        /// <param name="ParDownNo"></param>
        /// <param name="ParDownId"></param>
        public void MoveDownMenuGroup(int ParDownNo, string ParDownId)
        {
            int NewNo = ParDownNo + 1;
            string UpparNo = "update xt_menu_parent set show_no=" + ParDownNo + " where show_no=" + NewNo;
            int i = SysParams.OAConnection().RunSql(UpparNo);

            string UpparNonew = "update xt_menu_parent set show_no=" + NewNo + " where menu_parent_id like '" + ParDownId + "'";
            int j = SysParams.OAConnection().RunSql(UpparNonew);
        }
        /// <summary>
        /// 插入子菜单
        /// </summary>
        public static void InserChildMenu(string menuname,string picpath)
        {
            string SelMaxNo = "select max(show_no)+1 from xt_menu_parent";
            DataTable dt;
            SysParams.OAConnection().RunSql(SelMaxNo, out dt);

            int showNo = Convert.ToInt32(dt.Rows[0][0]);
            string InsertParentM = "insert into xt_menu_parent(menu_parent_id,menu_parent_desc,show_no,picpath) values(sys_guid(),'" + menuname + "','" + showNo + "','"+picpath+"')";
            int i = SysParams.OAConnection().RunSql(InsertParentM);
        }
        /// <summary>
        /// 更新子菜单
        /// </summary>
        /// <param name="menuname"></param>
        /// <param name="parentid"></param>
        public static void UpdateChileMenu(string menuname,string parentid,string picpath)
        {
            string UpdateM = "update xt_menu_parent set menu_parent_desc='" + menuname + "',picpath='"+picpath+"' where menu_parent_id like '" + parentid + "'";
            int i = SysParams.OAConnection().RunSql(UpdateM);
        }
        /// <summary>
        /// 获取子菜单
        /// </summary>
        /// <param name="strParentDesc">父菜单的描述</param>
        /// <returns></returns>
        public DataTable GetChildMenuInfo(string strParentDesc)
        {
            string strSql = string.Format("select c.* from xt_menu_child c,xt_menu_parent p " +
                "where c.ancestor=p.menu_parent_id and p.MENU_PARENT_DESC='{0}' order by c.show_no",
                strParentDesc);

            DataTable dtOut;
            SysParams.OAConnection().RunSql(strSql, out dtOut);
            return dtOut;
        }

        /// <summary>
        /// 保存菜单
        /// </summary>
        /// <param name="strGroupId"></param>
        public void SaveMenu(string strGroupId)
        {
            IDataAccess dataAccess = SysParams.OAConnection(true);

            try
            {
                String strSql = string.Format("delete from xt_group_menu where right_id='{0}'", strGroupId);
                dataAccess.RunSql(strSql);

                strSql = "select * from xt_menu_child";

                System.Data.DataTable dtOut;
                dataAccess.RunSql(strSql, out dtOut);

                foreach (System.Data.DataRow drRecord in dtOut.Rows)
                {
                    string strChildId = drRecord["MENU_CHILD_ID"].ToString();
                    string strChecked = Request[strChildId];

                    if (!string.IsNullOrEmpty(strChecked))
                    {
                        if (strChecked.Equals("checkbox"))
                        {
                            strSql = string.Format("insert into xt_group_menu(right_id,menu_child_id)" +
                                " values('{0}','{1}')", strGroupId, strChildId);
                            dataAccess.RunSql(strSql);
                        }
                    }
                }

                dataAccess.Close(true);
            }
            catch
            {
                dataAccess.Close(false);
                ClientScript.RegisterStartupScript(Page.GetType(), "returnHis", "window.location.href='manage_group_menu.aspx'");
            }	
        }

        /// <summary>
        /// 获取子菜单的个数
        /// </summary>
        /// <param name="strParentDesc">父菜单的描述</param>
        /// <returns></returns>
        public string GetChildMenuCount(string strParentDesc)
        {
            string strSql = string.Format("select count(c.menu_child_id) as countchild from xt_menu_child c,"
                + "xt_menu_parent p where c.ancestor=p.menu_parent_id and p.MENU_PARENT_DESC='{0}'",
                strParentDesc);

            return SysParams.OAConnection().GetValue(strSql);
        }

        /// <summary>
        /// 判断子菜单是否选中
        /// </summary>
        /// <param name="strRightId"></param>
        /// <param name="strMenuChildId"></param>
        /// <returns></returns>
        public string GetCheckedMenuId(string strRightId, string strMenuChildId)
        {
            string strSql = string.Format("select menu_child_id from xt_group_menu where right_id='{0}'" +
                " and menu_child_id='{1}'", strRightId, strMenuChildId);

            return SysParams.OAConnection().GetValue(strSql);
        }

        #endregion
    }
}
