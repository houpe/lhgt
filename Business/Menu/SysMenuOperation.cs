﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Business;
using Business.Common;
using WF_Business;

namespace Business.Menu
{
    /// <summary>
    /// 申报系统菜单操作类
    /// </summary>
    public class SysMenuOperation
    {
        /// <summary>
        /// 删除现有的菜单
        /// </summary>
        /// <param name="id"></param>
        public static void DeleteMenu(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                string strSql = string.Format("Delete from sys_menu where id ='{0}'", id);

                SysParams.OAConnection().RunSql(strSql);
            }
        }

        /// <summary>
        /// 根据流程父ID获取流程信息
        /// </summary>
        /// <param name="strParentId">父ID</param>
        /// <param name="strChUrl">子菜单Url</param>
        /// <returns></returns>
        /// <!--addby zhongjian 20091010-->
        public DataTable GetSysMenuByParentid(string strParentId, string strChUrl)
        {
            string strSql = string.Format(@"select t.* from sys_menu t where parent_id='{0}' ", strParentId);
            if (strChUrl == "")
            {
                strSql += " and menu_churl is not null";
            }
            else
            {
                strSql += string.Format(@" and menu_churl like '%{0}%'", strChUrl);
            }
            strSql += " order by order_field";
            DataTable dtReturn;
            SysParams.OAConnection().RunSql(strSql, out dtReturn);
            return dtReturn;
        }

        /// <summary>
        /// 根据菜单id获取父菜单id
        /// </summary>
        /// <param name="strMenuId">菜单id</param>
        /// <returns></returns>
        public string GetParentId(string strMenuId)
        {
            string ParentId = "";
            if (!string.IsNullOrEmpty(strMenuId))
            {
                string strSql = string.Format("select PARENT_ID from SYS_MENU where id='{0}'", strMenuId);

                ParentId = SysParams.OAConnection().GetValue(strSql);
            }
            return ParentId;
        }

        /// <summary>
        /// 根据父菜单id获取父菜单名称
        /// </summary>
        /// <param name="strParentMenuId">菜单id</param>
        /// <returns></returns>
        public string GetParentName(string strParentMenuId)
        {
            string ParentMenu = "";
            if (!string.IsNullOrEmpty(strParentMenuId))
            {
                string strSql = string.Format("SELECT MENU_TITLE FROM Sys_Menu WHERE ID = '{0}'", strParentMenuId);

                ParentMenu = SysParams.OAConnection().GetValue(strSql);
            }
            return ParentMenu;
        }

        /// <summary>
        /// 返回菜单
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public DataTable GetMenu(string id)
        {
            string strSql = "select t.*, t.rowid from sys_menu t where id= '" + id + "'";

            DataTable dt = null;
            SysParams.OAConnection().RunSql(strSql, out dt);
            return dt;
        }

        /// <summary>
        /// 增加一个菜单
        /// </summary>
        /// <param name="memu_title">菜单标题</param>
        /// <param name="menu_url">菜单对应的URL</param>
        /// <param name="parent_id">父ID</param>
        /// <param name="nOrder">菜单顺序</param>
        /// <param name="strmenu_churl">数据维护地址(子菜单地址)</param>
        /// <returns></returns>
        /// <!--editby zhongjian 20091010-->
        public static void InsertMenu(string menu_title,string menu_url,string parent_id, int nOrder,string strmenu_churl)
        {
            if (!string.IsNullOrEmpty(menu_title))
            {
                string strSql = string.Format("Insert into SYS_MENU (menu_title,menu_url,parent_id,order_field,menu_churl) VALUES ('{0}','{1}','{2}','{3}','{4}')", menu_title, menu_url, parent_id, nOrder, strmenu_churl);

                SysParams.OAConnection().RunSql(strSql);
            }
        }

        /// <summary>
        /// 修改一个菜单
        /// </summary> 
        /// <param name="id">菜单id</param>
        /// <param name="memu_title">菜单标题</param>
        /// <param name="menu_url">菜单对应的URL</param>
        /// <param name="parent_id">父ID</param>
        /// <param name="nOrder">菜单顺序</param>
        /// <param name="strmenu_churl">数据维护地址(子菜单地址)</param>
        /// <returns></returns>
        /// <!--editby zhongjian 20091010-->
        public static void UpdateMenu(string id, string menu_title, string menu_url, int nOrder, string strmenu_churl)
        {
            if (!string.IsNullOrEmpty(id) && !string.IsNullOrEmpty(menu_title))
            {
                string strSql = string.Format("Update sys_menu set menu_title='{1}',menu_url='{2}',order_field='{3}',menu_churl='{4}' where id='{0}'", id, menu_title, menu_url, nOrder, strmenu_churl);

                SysParams.OAConnection().RunSql(strSql);
            }
        }

        /// 创建人  : Wu Hansi
        /// 创建时间: 2007-07-09
        /// 修改人  : Wu Hansi
        /// 修改时间: 2007-07-09
        /// 修改描述: 描述                 
        /// <summary>
        /// 得到排序好的所有菜单
        /// </summary>
        /// <returns></returns>
        public DataTable GetMenusSorted()
        {
            DataTable dtTemp;
            string strSql = "select t2.parent_id sortid,null parentMenu, t2.menu_title childMenu,t2.id, t2.menu_url, null child ";
            strSql += "from sys_menu t1, sys_menu t2 ";
            strSql += "where t1.ID = t2.parent_id UNION ";
            strSql += "select t3.id sortid,menu_title parentMenu,null childMenu,id, menu_url, ";
            strSql += "(select count(*) from sys_menu t4 where t4.parent_id=t3.id) child ";
            strSql += "from sys_menu t3 ";
            strSql += "where t3.parent_id is null order by sortid desc,menu_url desc";

            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }

        /// <summary>
        /// 根据条件得到排序好的所有菜单
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public DataTable GetMenusSorted(string str)
        {
            DataTable dtTemp;
            string strSql = "select t2.parent_id sortid,null parentMenu, t2.menu_title childMenu,t2.id, t2.menu_url, null child ";
            strSql += "from sys_menu t1, sys_menu t2 ";
            strSql += "where t1.ID = t2.parent_id  and t2.menu_title like '%" + str + @"%' UNION ";
            strSql += "select t3.id sortid,menu_title parentMenu,null childMenu,id, menu_url, ";
            strSql += "(select count(*) from sys_menu t4 where t4.parent_id=t3.id) child ";
            strSql += "from sys_menu t3 ";
            strSql += "where t3.parent_id is null and t3.menu_title like '%" + str + @"%' order by sortid desc,menu_url desc";

            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }

    }
}
