﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using WF_Business;
namespace Business.Menu
{
    public class SysUserMenuOperation
    {
        /// <summary>
        /// 增加一个用户类型-菜单关系
        /// </summary>
        /// <param name="usertype_id"></param>
        /// <param name="menu_id"></param>
        /// <returns></returns>
        public bool InsertUsertypeMenuRelation(string usertype_id, string menu_id)
        {
            try
            {
                string strSql = @"Insert into sys_usertype_menu_relation (usertype_id, menu_id) VALUES ('" +
                                usertype_id + "','" + menu_id + "')";

                SysParams.OAConnection().RunSql(strSql);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一个用户类型-菜单关系
        /// </summary>
        /// <param name="usertype_id"></param>
        /// <param name="menu_id"></param>
        /// <returns></returns>
        public bool RemoveUsertypeMenuRelation(string usertype_id, string menu_id)
        {
            try
            {
                string strSql = string.Format(@"Delete from sys_usertype_menu_relation where 
                usertype_id ='{0}' and menu_id='{1}' ", usertype_id, menu_id);

                SysParams.OAConnection().RunSql(strSql);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// 得到排序好的，带是否拥有权限的标志
        /// </summary>
        /// <param name="usertype_id"></param>
        /// <returns></returns>
        public DataTable GetMenusSortedWithAuth(string usertype_id)
        {
            string strSql = @"select t2.parent_id sortid,null parentmenu, t2.menu_title childmenu,t2.id,t2.menu_url,
                            (select count(*) from sys_usertype_menu_relation s1 where 
                            s1.usertype_id='" + usertype_id + @"' and s1.menu_id=t2.id) auth
                            from sys_menu t1, sys_menu t2 where t1.ID = t2.parent_id
                            UNION 
                            select t3.id sortid,menu_title parentmenu,null childmenu,id,menu_url,
                            (select count(*) from sys_usertype_menu_relation s1 where 
                            s1.usertype_id='" + usertype_id + @"' and s1.menu_id=t3.id) auth
                            from sys_menu t3, sys_usertype_menu_relation s1
                            where t3.parent_id is null order by sortid desc, menu_url desc
                            ";
            DataTable dtOut;
            SysParams.OAConnection().RunSql(strSql, out dtOut);
            return dtOut;
        }

        /// <summary>
        /// 根据系统标识生成菜单
        /// </summary>
        /// <returns></returns>
        public DataTable GenerateParentMenu(string userID)
        {
            DataTable dtTemp;
            string strSql = "select a.* from sys_menu a join sys_usertype_menu_relation b on a.ID=b.Menu_ID ";
            strSql += "join sys_usertype c on c.id=b.usertype_id join sys_user d on d.Type=c.UserTypeCode ";
            strSql += "where a.parent_id is null and d.userID='" + userID + "' ";
            strSql += "order by ORDER_FIELD asc";

            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }

        /// <summary>
        /// 根据系统标识父菜单ID生成菜单
        /// </summary>
        /// <param name="userID">用户ID</param>
        /// <param name="strParentId">父菜单ID</param>
        /// <returns></returns>
        /// <!--
        /// 功能描述  : 菜单的生成与权限挂钩
        /// 修改人  : cd
        /// 修改时间: 2007-07-10
        /// -->
        public DataTable GenerateChildMenu(string userID, string strParentId)
        {
            DataTable dtTemp;
            string strSql = "select a.* from sys_menu a join sys_usertype_menu_relation b on a.ID=b.Menu_ID ";
            strSql += "join sys_usertype c on c.id=b.usertype_id join sys_user d on d.Type=c.UserTypeCode ";
            strSql += "where a.parent_id ='" + strParentId + "' and d.userID='" + userID + "' ";
            strSql += "order by order_field";

            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }

       

       
    }
}
