﻿ 
// 创建人  ：QiuXiaoChi
// 创建时间：2013-08-27
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Xml;
using System.Collections;
using System.Data.OracleClient;

namespace Business.OfficialDoc
{
    /// <!--
    /// 功能描述  : 意见权限控制
    /// 创建人  : QiuXiaoChi
    /// 创建时间: 2013-08-27
    /// -->
    public class OfficialOPModel
    {
        public enum PermissionTypes : int
        {
            None = 0,
            OPEdit = 1,
            OPRead = 2,
            OPListRead = 4,
            All = OPEdit | OPRead | OPListRead
        }

        public static void addOPEdit(ref PermissionTypes pt)
        {
            pt = pt | PermissionTypes.OPEdit;

            //添加可写权限即添加可读权限
            pt = pt | PermissionTypes.OPRead;
        }

        public static void removeOPEdit(ref  PermissionTypes pt)
        {
            pt = pt & ~PermissionTypes.OPEdit;
        }

        public static void addOPRead(ref  PermissionTypes pt)
        {
            pt = pt | PermissionTypes.OPRead;
        }

        public static void removeOPRead(ref PermissionTypes pt)
        {
            pt = pt & ~PermissionTypes.OPRead;
           
            //去掉可读权限即去掉可读权限
            pt = pt & ~PermissionTypes.OPEdit;
        }

        public static void addOPListRead(ref PermissionTypes pt)
        {
            pt = pt | PermissionTypes.OPListRead;
        }

        public static void removeOPListRead(ref PermissionTypes pt)
        {
            pt = pt & ~PermissionTypes.OPListRead;
        }

        public static bool checkOPEdit(PermissionTypes pt)
        {
            return (pt & PermissionTypes.OPEdit) == PermissionTypes.OPEdit;
        }

        public static bool checkOPRead(PermissionTypes pt)
        {
            return (pt & PermissionTypes.OPRead) == PermissionTypes.OPRead;
        }

        public static bool checkOPListRead(PermissionTypes pt)
        {
            return (pt & PermissionTypes.OPListRead) == PermissionTypes.OPListRead;
        }
    }
}