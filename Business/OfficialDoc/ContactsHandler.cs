﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using WF_DataAccess;
using Common;
using WF_Business;
namespace Business.OfficialDoc
{
    public class ContactsHandler
    {
        private static ContactsHandler _ContactsInstance;

        private ContactsHandler()
        {

        }

        public static ContactsHandler getInstance()
        {
            if (_ContactsInstance == null)
            {
                _ContactsInstance = new ContactsHandler();
            }
            return _ContactsInstance;
        }

        public string getcontactslist(string strDepartId)
        {
            IDataAccess ida = SysParams.OAConnection();
            DataTable dt = new DataTable();
            string strWhere = string.Empty;
            if(!string.IsNullOrEmpty(strDepartId))
            {
                strWhere = string.Format(" and b.departid='{0}'",strDepartId);
            }
            String sql = string.Format(@"Select a.mobile,
                       a.userid,
                       a.orderbyid,
                       b.departid dept_id,
                       a.mobile,
                       a.user_name,
                       b.depart_name dept_name,
                       b.order_id
                  From st_user a, st_department b,st_user_department c
                 Where a.userid = c.userid and c.order_id=b.departid {0}
                 Order By a.orderbyid",strWhere);
            ida.RunSql(sql, out dt);

            JsonOperation joTemp = new JsonOperation();
            return joTemp.BindJson(dt);
        }

        public string getDepartmentslist()
        {
            IDataAccess ida = SysParams.OAConnection();
            DataTable dt = new DataTable();
            String strsql = "select t.departid,t.depart_name, t.order_id from st_department t Order By t.order_id";
            ida.RunSql(strsql, out dt);
            JsonOperation joTemp = new JsonOperation();
            return joTemp.BindJson(dt);
        }

    }
}
