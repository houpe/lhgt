﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Xml;
using System.Data.OracleClient;
using WF_DataAccess;
using Business.Admin;
using Business.Struct;
using Common;
using WF_Business;

namespace Business.OfficialDoc
{
    /// <!--
    /// 功能描述  : 获取工作流相关数据
    /// 创建人  : QiuXiaoChi
    /// 创建时间: 20010-08-27
    /// -->
    public class SystemHandler
    {
        private static SystemHandler _systemInstance;

        private SystemHandler()
        {

        }

        public static SystemHandler getInstance()
        {
            if (_systemInstance == null)
            {
                _systemInstance = new SystemHandler();
            }
            return _systemInstance;
        }

        /// <summary>
        /// 获取用户有权限启动的流程
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public String UserLogin(string loginname, string password)
        {
            string msg = string.Empty;
            StUserOperation uaHelper = new StUserOperation();

            if (uaHelper.CheckPassword(loginname, password, out msg))
            {
                MoaUser user = uaHelper.GetByLoginName(loginname);
                msg = "{ \"Result\": \"True\",\"User\":{\"UserId\": \"" + user.Userid + "\",\"UserName\":\"" + user.User_Name + "\",\"UserLoginName\":\"" + user.Login_Name
                    + "\",\"UserMemo\":\"" + user.Memo + "\",\"UserTel\":\"" + user.Tel + "\",\"UserMobile\":\"" + user.Mobile
                    + "\",\"UserSign\":\"" + "" + "\"} } ";//user.Sign不应该放置在st_user表
            }
            else
            {
                msg = JsonOperation.GetJsonString("False", msg);
            }

            return msg;
        }

        public String GetSystemVersion()
        {

            IDataAccess ida = SysParams.OAConnection();

            string strSql = @"select * from moa_version_info t where valid = 1";
            string strResult = "";
            DataTable dt = new DataTable();
            try
            {
                ida.RunSql(strSql, out dt);
                if (dt.Rows.Count == 1)
                {
                    strResult = JsonOperation.DataTableToJson(dt);
                }
            }
            catch
            {
                throw;
            }
            return strResult;
        }

        public int Log(string log_type,
            string log_userid,
            string log_device_id,
            string log_apk_name,
            string log_apk_version,
            string log_time,
            string log_location,
            string log_remark,
            string log_remark2,
            string log_remark3,
            string log_remark4,
            string log_intent,
            string log_intent_parm,
            string log_category)
        {
            IDataAccess ida = WF_Business.SysParams.OAConnectionWithEnterprise();

            //单引号处理成特殊字符
            string strSQL = @"insert into moa_log
 	          (LOG_TYPE,
              LOG_USERID,
              LOG_DEVICE_ID,
              LOG_APK_NAME,
              LOG_APK_VERSION,
              LOG_TIME,
              LOG_LOCATION,
         
              LOG_REMARK,
              LOG_REMARK2,
              LOG_REMARK3,
              LOG_REMARK4,
              LOG_INTENT ,
              LOG_INTENT_PARM ,
              LOG_CATEGORY)
            values ('{0}','{1}','{2}','{3}','{4}',to_date('{5}','yyyy-MM-dd hh24:mi:ss'),'{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}')";
           return ida.RunSql(String.Format(strSQL, log_type, log_userid, log_device_id, log_apk_name, log_apk_version,
                log_time, log_location, log_remark.Replace("'", "^"), log_remark2.Replace("'", "^"), log_remark3.Replace("'", "^"), log_remark4.Replace("'", "^"),
                log_intent, log_intent_parm, log_category));
        }
    }
}