﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using WF_DataAccess;
using Common;
using WF_Business;
using System.Text;
namespace Business.OfficialDoc
{

    public class NewsandNoticesHandler
    {
        private static NewsandNoticesHandler _NewsandNoticesInstance;

        private NewsandNoticesHandler()
        {

        }

        public static NewsandNoticesHandler getInstance()
        {
            if (_NewsandNoticesInstance == null)
            {
                _NewsandNoticesInstance = new NewsandNoticesHandler();
            }
            return _NewsandNoticesInstance;
        }

        #region 新闻中心
        /// <summary>
        /// 新闻列表刷新 下拉刷新
        /// </summary>
        /// <param name="oldtime">上次刷新时间</param>
        /// <returns></returns>
        public string RefreshNewsList(string oldtime)
        {
            IDataAccess ida = SysParams.OAConnection();
            DataTable dt = new DataTable();
            string strSql = @"select rownum ,t.id, t.title,t.time,t.type from xt_other t where t.type='行业新闻' and time > to_date('" + oldtime + "','yyyy-MM-dd hh24:mi:ss') order by time desc";
            ida.RunSql(strSql, out dt);
            return JsonOperation.DataTableToJson(dt);
        }

        /// <summary>
        /// 加载更多新闻
        /// </summary>
        /// <param name="oldtime"> 初始刷新时间字符串 | 格式为 ：yyyy-mm-dd hh24:mi:ss</param>
        /// <param name="rowcount"> 当前加载的数据条数。</param>
        /// <returns></returns>
        public string getNewsMore(string oldtime, int rowcount)
        {
            IDataAccess ida = SysParams.OAConnection();
            DataTable dt = new DataTable();
            //string strSql = @"select * from (select rownum as rowindex,a.* from (select t.id, t.title,t.time,t.type,t.is_top,t.imagepath from xt_other t where  t.type='行业新闻' and time < to_date('" + oldtime +
            //    "','yyyy-MM-dd hh24:mi:ss') order by time desc) a) where rowindex > " + rowcount + " and rowindex < " + (rowcount + 21);

            string strSql = @"select * from (select rownum as rowindex,a.* from (select t.id, t.title,t.time,t.type from xt_other t where  t.type='行业新闻' and time < to_date('" + oldtime +"','yyyy-MM-dd hh24:mi:ss') order by time desc) a) where rowindex > " + rowcount + " and rowindex < " + (rowcount + 21);


            ida.RunSql(strSql, out dt);
            return JsonOperation.DataTableToJson(dt);
        }

       
        /// <summary>
        /// 获取图片新闻
        /// </summary>
        /// <returns></returns>
        public string getImageNews()
        {
            IDataAccess ida = SysParams.OAConnection();
            DataTable dt = new DataTable();
            //string strSql = "select rownum, t.title,t.imagepath,t.is_top ,t.id ,t.time from xt_other t where t.is_top='1'  and rownum <6 order by time desc";

            string strSql = @"select rownum, t.title, t.time, t.id,replace(b.filename,'localhost','10.0.2.2') imagepath,b.uploadtime  
                  from xt_other t,xt_uploadfile b
                 where rownum < 6 and b.archivesno=t.id and t.type='行业新闻' 
                 order by t.time desc";
            ida.RunSql(strSql, out dt);

            
            string strReturn = string.Empty;
            try
            {
                JsonOperation joTemp = new JsonOperation();
                strReturn = joTemp.BindJson(dt);
            }
            catch(Exception ex)
            {
                strReturn = ex.Message;
            }
            return strReturn;
        }

        /// <summary>
        ///  根据id获取通知新闻详细内容
        /// </summary>
        /// <param name="noticeid"></param>
        /// <returns></returns>
        public string GetNewsDetial(string newsid)
        {
            IDataAccess ida = SysParams.OAConnection();
            DataTable dt = new DataTable();
            string strSql = string.Format("select t.* from xt_other t where id ='{0}'",newsid);
            ida.RunSql(strSql, out dt);
            return JsonOperation.DataTableToJson(dt);
        }
        #endregion

        #region 通知公告

        /// <summary>
        /// 新闻列表刷新 下拉刷新
        /// </summary>
        /// <param name="oldtime"> 上次刷新时间</param>
        /// <returns></returns>
        public string RefreshNoticeList(string oldtime)
        {
            IDataAccess ida = SysParams.OAConnection();
            DataTable dt = new DataTable();
            string strSql = "Select to_char(sysdate, 'yyyy-mm-dd hh24:mi:ss') rtime, Rownum, a.* From (select id, t.title, time, userid, rownum from xt_other t Where type='通知通报' and time > to_date('" + oldtime + "','yyyy-mm-dd hh24:mi:ss') Order By time Desc ) a";
            ida.RunSql(strSql, out dt);
            return JsonOperation.DataTableToJson(dt);
        }
        /// <summary>
        ///  根据id获取通知公告详细内容
        /// </summary>
        /// <param name="noticeid"></param>
        /// <returns></returns>
        public string GetNoticeDetial(string noticeid)
        {
            IDataAccess ida = SysParams.OAConnection();
            DataTable dt = new DataTable();
            string strSql = string.Format("select * from xt_other t where t.id ='{0}'", noticeid);
            ida.RunSql(strSql, out dt);
            return JsonOperation.DataTableToJson(dt);
        }

        /// <summary>
        /// 加载更多通知公告
        /// </summary>
        /// <param name="oldtime"> 上次刷新时间字符串 | 格式为 ：yyyy-mm-dd hh24:mi:ss</param>
        /// <param name="rowcount"> 每次刷新添加的数据条数。</param>
        /// <returns></returns>
        public string getNoticeMore(string userid, string oldtime, int rowcount)
        {
            IDataAccess ida = SysParams.OAConnection();
            DataTable dt = new DataTable();

            string strSql = string.Format(@"select * from (select id, t.title, time, smalltype,userid, rownum as rowindex from xt_other t where type='通知通报' and time < to_date('{0}','yyyy-MM-dd hh24:mi:ss') order by time desc ) where rowindex > {1} and rowindex <{2}", oldtime, rowcount, (rowcount + 21));

            ida.RunSql(strSql, out dt);

            return JsonOperation.DataTableToJson(dt);
        }

        /// <summary>
        /// 获取新闻、通知公告 数目
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        public string getNoticeMoreCount(string userid)
        {
            IDataAccess ida = SysParams.OAConnection();
            string strSql = "select count(*) from xt_other where type='通知通报'";

            string result = "{Result:'True',Count:" + ida.GetValue(strSql) + "}";
            return result;
        }

        public string UpdateNoticeReadFlag(string userid, string issueid)
        {
            string result = string.Empty;
            IDataAccess ida = SysParams.OAConnection();
            try
            {
                string strSql = "update xt_issueattend i set i.user_read='0' where i.userid='" + userid + "' and i.issueid='" + issueid + "'";
                result = "{Result:'True',Count:" + ida.RunSql(strSql) + "}";
            }
            catch (Exception err)
            {
                result = "{Result:'False',Message:'修改失败：+" + err.Message + "'}";
            }
            return result;
        }

        #endregion
    }
}
