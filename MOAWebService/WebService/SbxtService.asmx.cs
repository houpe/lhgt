﻿using Business.Admin;
using Business.FlowOperation;
using Common;
using SbBusiness;
using SbBusiness.Menu;
using SbBusiness.PubInfo;
using SbBusiness.User;
using SbBusiness.Wsbs;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace MOAWebService.WebService
{
    /// <summary>
    /// SbxtService 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/", Description = "申报系统交互服务new")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允许使用 ASP.NET AJAX 从脚本中调用此 Web 服务，请取消注释以下行。 
    [System.Web.Script.Services.ScriptService]
    public class SbxtService : System.Web.Services.WebService
    {
        /// <summary>
        /// 获取session信息
        /// </summary>
        /// <param name="strSessionName"></param>
        [WebMethod(EnableSession = true)]
        public string GetSessionInfo(string strSessionName)
        {
            if (Session[strSessionName] != null)
            {
                return Session[strSessionName].ToString();
            }
            else
            {
                return "";
            }
        }

        /// <summary>
        /// 清除所有session
        /// </summary>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public void ClearSession()
        {
            Session.Clear();
        }

        /// <summary>
        /// 获取登录信息
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public string GetLoginInfo(string strUserId)
        {
            string strReturn = "";
            if (!string.IsNullOrEmpty(strUserId))
            {
                string strUserName = SysUserHandle.GetUserNameByUserID(strUserId);
                strReturn = string.Format("{0},欢迎您访问网上办事大厅！【当前日期:{1}&nbsp;{2}】", strUserName, DateTime.Now.ToLongDateString(), ClsWorkDaySet.GetDayOfWeek());
            }
            return strReturn;
        }

        /// <summary>
        /// 获取待办信息
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public string GetDaiBanInfo(int nType, string strUserId)
        {
            string strReturn = string.Empty;

            if (!string.IsNullOrEmpty(strUserId))
            {
                SerialInstance cInstance = new SerialInstance();                
                ShenBaoSubmit cSubmit = new ShenBaoSubmit();

                switch (nType)
                {
                    case 1:
                        strReturn = cSubmit.GetNoSubmitCount(strUserId);//新申(尚未提交)请事项个数
                        break;
                    case 2:
                        strReturn = cInstance.GetInstanceCount(strUserId);//正在办理(当前办理)事项个数
                        break;
                    case 3:
                        strReturn = cSubmit.GetBuZhengSubmitCount(strUserId);//补正补齐办理事项个数
                        break;
                    case 4:
                        strReturn = cInstance.GetHistoryCount(strUserId, "2");//已通过办理事项个数
                        break;
                    case 5:
                        strReturn = cInstance.GetBoHuiCount(strUserId);//驳回办理事项个数
                        break;
                    case 6:
                        strReturn = cInstance.GetShoujianCount(strUserId);//累计收件个数
                        break;
                    case 7:
                        strReturn = cInstance.GetBanjieCount(strUserId); //累计办结个数
                        break;
                    case 8:
                        Messagebox cMessage = new Messagebox();
                        strReturn = cMessage.GetMessageCount(strUserId);//通知个数
                        break;
                    case 9:
                        strReturn = cInstance.GetHistoryCount(strUserId, "-3"); //不予受理
                        break;
                    case 10:
                        strReturn = cInstance.GetHistoryCount(strUserId, "-4");//未通过办理
                        break;
                    case 11:
                        strReturn = cInstance.GetShoujianTotal();//累计收件个数
                        break;
                    case 12:
                        strReturn = cInstance.GetBanjieTotal(); //累计办结个数
                        break;
                    default:
                        break;
                }
            }

            return strReturn;
        }

        /// <summary>
        /// 获取信息公告
        /// </summary>
        /// <param name="Type">消息类型</param>
        /// <param name="strSmalType">流程名称</param>
        /// <returns></returns>
        [WebMethod]
        public string GetInfoType(string Type, string strSmalType)
        {
            InfoManage imTemp = new InfoManage();
            DataTable dtSource = imTemp.GetInfoType(Type, strSmalType);

            string strReturn = string.Empty;
            if (dtSource.Rows.Count > 0)
            {
                JsonOperation _djson = new JsonOperation();
                strReturn = _djson.BindJson(dtSource);
            }
            return strReturn;
        }

        /// <summary>
        /// 获取留言信息
        /// </summary>
        [WebMethod]
        public string GetLyzx()
        {
            SerialInstance stTemp = new SerialInstance();
            DataTable dtSource = stTemp.GetLyzxInfo("", true, 4, 13, "", "", "", "", "");
            string strReturn = string.Empty;
            if (dtSource.Rows.Count > 0)
            {
                JsonOperation _djson = new JsonOperation();
                strReturn = _djson.BindJson(dtSource);
            }
            return strReturn;
        }

        /// <summary>
        /// 获取常见问题
        /// </summary>
        [WebMethod]
        public string GetCjwt()
        {
            SerialInstance stTemp = new SerialInstance();
            DataTable dtSource = stTemp.GetAskQuestion("", "常见问题", "", 4, 13);
            string strReturn = string.Empty;
            if (dtSource.Rows.Count > 0)
            {
                JsonOperation _djson = new JsonOperation();
                strReturn = _djson.BindJson(dtSource);
            }
            return strReturn;
        }

        /// <summary>
        /// 获取建设用地最新的报价
        /// </summary>
        /// <param name="strWhere">拓展查询条件</param>
        /// <param name="strUserId">用户id</param>
        /// <returns></returns>
        [WebMethod]
        public string GetNewestDkxx(string strWhere, string strUserId)
        {
            JsydCrOperation jcoTemp = new JsydCrOperation();
            DataTable dtSource = jcoTemp.GetNewestDkxx(strWhere, strUserId);
            string strReturn = string.Empty;
            if (dtSource.Rows.Count > 0)
            {
                JsonOperation _djson = new JsonOperation();
                strReturn = _djson.BindJson(dtSource);
            }
            return strReturn;
        }

        /// <summary>
        /// 添加报价
        /// </summary>
        /// <param name="strUserId"></param>
        /// <param name="strBaoJia"></param>
        /// <param name="strIID"></param>
        /// <returns></returns>
        [WebMethod]
        public string AddDkxxBaoJia(string strUserId, string strBaoJia, string strIID)
        {
            JsydCrOperation jcoTemp = new JsydCrOperation();
            jcoTemp.AddDkxxBaoJia(strUserId, strBaoJia, strIID);
            return "报价成功";
        }

        /// <summary>
        /// 添加报名信息
        /// </summary>
        /// <param name="strUserId"></param>
        /// <param name="strIID"></param>
        /// <returns></returns>
        [WebMethod]
        public string AddDkxxBaoMing(string strUserId, string strIID)
        {
            JsydCrOperation jcoTemp = new JsydCrOperation();
            jcoTemp.AddDkxxBaoMing(strUserId, strIID);
            return "报名成功";
        }

        /// <summary>
        /// 获取行政许可办理事项
        /// </summary>
        /// <param name="strIID">办件编号</param>
        /// <param name="strName">申请人</param>
        /// <returns></returns>
        [WebMethod]
        public string GetXzxkjgList(string strIID, string strName)
        {
            SerialInstance stTemp = new SerialInstance();
            DataTable dtSource = stTemp.GetJggs(strIID, strName, "", "", "");
            string strReturn = string.Empty;
            if (dtSource.Rows.Count > 0)
            {
                JsonOperation _djson = new JsonOperation();
                strReturn = _djson.BindJson(dtSource);
            }
            return strReturn;
        }

        /// <summary>
        /// 获取当前用户的行政许可事项标题
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public string GetXzxkInfoTitle()
        {
            string strReturn = string.Empty;

            WorkFlowPubSetting cWorks = new WorkFlowPubSetting();
            //绑定行政许可事项
            DataTable dtSource = cWorks.GetPubWorkFlow();

            if (dtSource.Rows.Count > 0)
            {
                JsonOperation _djson = new JsonOperation();
                strReturn = _djson.BindJson(dtSource);
            }
            return strReturn;
        }

        #region 行政许可事项查看
        /// <summary>
        /// 构建数据行结构
        /// </summary>
        /// <param name="strFlowName">流程名</param>
        /// <param name="strFlowType">流程类型</param>
        /// <param name="strUserId">登录id</param>
        /// <returns></returns>
        [WebMethod]
        public string GetXzxkInfoContent(string strFlowName, string strFlowType,string strUserId)
        {
            string strReturn = string.Empty;

            //获取菜单
            ClsMenuOperation menuOperation = new ClsMenuOperation();
            DataTable dtMenu = menuOperation.GetChildMenus("网上办事");

            if (!string.IsNullOrEmpty(strUserId))
            {
                bool bSeial = SysUserRightRequest.GetPower(strUserId, strFlowName);

                if (bSeial)//有操作流程的权限则全部访问
                {
                    strReturn += GetMenuTypePower(strFlowName, strFlowType, dtMenu, 1);
                }
                else//无操作流程的权限
                {
                    strReturn += GetMenuTypePower(strFlowName, strFlowType, dtMenu, 0);
                }
            }
            else//未登录访问
            {
                strReturn += GetMenuTypePower(strFlowName, strFlowType, dtMenu, 0);
            }

            return strReturn;
        }

        /// <summary>
        /// 地理信息中心接口
        /// </summary>
        /// <param name="strUrl">接口连接地址(url)</param>
        /// <returns></returns>
        public string GetDXInterfaceUrl(string strInterfaceUrl, string strUserId)
        {
            SerialInstance cInstance = new SerialInstance();

            string strUrl = string.Empty;
            string strUserPWD = string.Empty;
            strUserPWD = cInstance.GetUserPWD(strUserId);
            strUserId = HttpUtility.UrlEncode(strUserId);//在地信中心传入USERID需加密

            strUrl = string.Format("{2}?userId={0}&userPwd={1}", strUserId, strUserPWD, strInterfaceUrl);
            return strUrl;
        }

        /// <summary>
        /// 获取行政许可事项查看（目前无用途，暂废除）
        /// </summary>
        /// <param name="strFlowName"></param>
        /// <param name="strFlowType"></param>
        /// <param name="dtMenu"></param>
        /// <param name="strInterfaceType"></param>
        /// <param name="strInterfaceUrl"></param>
        /// <param name="strUserId"></param>
        /// <returns></returns>
        protected string GetKaifang(string strFlowName, string strFlowType, DataTable dtMenu, string strInterfaceType, string strInterfaceUrl,string strUserId)
        {
            string strHtml = string.Empty;
            string strWorkFlow = strFlowName;
            strFlowName = Server.UrlEncode(strFlowName);
            strFlowType = Server.UrlEncode(strFlowType);
            foreach (DataRow drTemp in dtMenu.Rows)
            {
                string strMenuTitle = drTemp["menu_title"].ToString();
                string strMenuUrl = drTemp["menu_url"].ToString();

                ////当接口启用状态开启时，在点击“在线申报”后，应连接接口页面。
                if (strMenuTitle == "在线申报" && strInterfaceType == "1")
                {
                    strMenuUrl = GetDXInterfaceUrl(strInterfaceUrl, strUserId);
                    strHtml += string.Format("<td ><a href='{0}&flowname={1}&flowtype={3}' target=\"_blank\"><font color=\"black\">{2}</a></font></td>",
                       strMenuUrl.Replace("~/", ""), strFlowName, strMenuTitle, strFlowType);
                }
                else
                {
                    //添加模式对话框显示类型&DialogFlag=Show,只有从本地址连接过去的在线申报,才提示申报的方式.
                    strHtml += string.Format("<td ><a href='{0}&flowname={1}&flowtype={3}&DialogFlag=Show' target=\"_blank\"><font color=\"black\">{2}</a></font></td>",
                       strMenuUrl.Replace("~/", ""), strFlowName, strMenuTitle, strFlowType);
                }
            }

            return strHtml;
        }

        /// <summary>
        /// nFlag=0:获取有匿名权限查看的菜单,nFlag=1:获取登录后可查看的菜单
        /// </summary>
        /// <param name="strFlowName"></param>
        /// <param name="strFlowType"></param>
        /// <param name="dtMenu"></param>
        /// <param name="nFlag"></param>
        /// <returns></returns>
        protected string GetMenuTypePower(string strFlowName, string strFlowType, DataTable dtMenu, int nFlag)
        {
            string strHtml = string.Empty;
            WorkFlowPubSetting cWorks = new WorkFlowPubSetting();

            //获取菜单权限
            DataTable dtMenuRight = cWorks.GetChildMenuRight(strFlowName);
            strFlowName = Server.UrlEncode(strFlowName);
            strFlowType = Server.UrlEncode(strFlowType);
            int nMenuAppear = 0;
            foreach (DataRow drTemp in dtMenu.Rows)
            {
                string strMenuTitle = drTemp["menu_title"].ToString();
                string strMenuUrl = drTemp["menu_url"].ToString();

                dtMenuRight.DefaultView.RowFilter = string.Format("menu_name='{0}'", strMenuTitle);

                DataView dvTemp = dtMenuRight.DefaultView;
                if (dvTemp.Count > 0)
                {
                    //先判断是否为显示
                    if (dvTemp[0].Row["menu_appear"].ToString() == "1")
                    {
                        nMenuAppear++;

                        //有匿名访问权限或者是登录系统后菜单才能访问
                        if (dvTemp[0].Row["menu_type"].ToString() == "1" || nFlag == 1)
                        {
                            strHtml += string.Format("<td ><a href='{0}&flowname={1}&flowtype={3}' target=\"_blank\"><font color=\"black\">{2}</a></font></td>",
                                strMenuUrl.Replace("~/", ""), strFlowName, strMenuTitle, strFlowType);
                        }
                        else//如果是未登录且无法匿名访问
                        {
                            strHtml += string.Format("<td ><font color=\"gray\">{0}</font></td>", strMenuTitle);
                        }
                    }
                }
            }

            //获取标题头
            string strHeadHtml = string.Format("<tr style='cursor: move'><td colspan='{0}'><div style='text-align: left' class='page_bar_02'><a href='Wsbs/IndexPage.aspx?type=0&flowname={1}&flowtype={2}'>{3}</a></div></td></tr>", nMenuAppear, strFlowName, strFlowType, Server.UrlDecode(strFlowType));
            strHtml = string.Format("<tr style='height: 45px'>{0}</tr>", strHtml);

            return strHeadHtml + strHtml;
        }
        #endregion

        #region 绑定最新消息内容
        /// <summary>
        /// 绑定最新消息的内容 
        /// </summary>        
        [WebMethod]
        public string BindMessageText(string strUserId)
        {
            string strMsgContent = string.Empty;
            if (!string.IsNullOrEmpty(strUserId))
            {
                string strMsgUrl = GetMsgUrl(strUserId);
                //获取将作提示的消息内容
                Messagebox cMessage = new Messagebox();
                strMsgContent = cMessage.GetNewMessage(strUserId);
                if (!string.IsNullOrEmpty(strMsgContent))
                {
                    //当没有可操作的流程权限时执行
                    //if (UserHandle.GetWorkPower(strUserId))
                    //{
                    strMsgContent += string.Format("&nbsp;&nbsp;立刻办理请<a target='_blank' href='{0}'>点击这里</a>", strMsgUrl);
                    //}
                    //else
                    //{
                    //    //当没有可操作流程权限时,提示申请流程权限
                    //    strMsgContent += string.Format("&nbsp;&nbsp;立刻申请开通请<a target='_blank' href='{0}' onclick='conClose()'>点击这里</a>", strMsgUrl);
                    //}
                }
            }
            return strMsgContent;
        }
        #endregion

        #region 获取当前提示下应该操作的路途
        /// <summary>
        /// 获取当前提示下应该操作的路途
        /// </summary>
        /// <returns></returns>
        /// <!--addby zhongjian 20091203-->
        public string GetMsgUrl(string strUserId)
        {
            string strUrl = "Wsbs/submit.aspx?flag=1";
            string strStepName = string.Empty;

            if (!string.IsNullOrEmpty(strUserId))
            {
                SerialInstance cInstance = new SerialInstance();
                Messagebox cMessage = new Messagebox();

                DataTable dtTemp;
                strStepName = cInstance.GetStepName(strUserId);
                if (strStepName == "用户注册")//按照以前逻辑是要先做业务申报，但是目前只有地图审核一个业务，所以取消
                {
                    strUrl = "wsbs/WsbsMain.aspx?type=3&flowname=%e5%9c%b0%e5%9b%be%e5%ae%a1%e6%a0%b8(%e5%8d%b3%e9%80%81%e5%8d%b3%e5%ae%a1)&flowtype=%e5%9c%b0%e5%9b%be%e5%ae%a1%e6%a0%b8%e7%94%b3%e8%af%b7'";
                }
                else if (strStepName == "权限开通")//当为权限开能时,导航提示连接为在线申报.在线申报连接的地址当以是否启用接口地址的设置为准.   
                {
                    dtTemp = cInstance.GetNewWork(strUserId);
                    if (dtTemp.Rows.Count > 0)
                    {
                        string strName = dtTemp.Rows[0]["flowname"].ToString();
                        string strType = dtTemp.Rows[0]["flowtype"].ToString();
                        strName = Server.UrlEncode(strName);
                        strType = Server.UrlEncode(strType);
                        //获取userid对应的sessionid
                        SerialInstance sioTemp = new SerialInstance();
                        decimal decValue = Convert.ToDecimal(sioTemp.GetValue("ss_instance_id"));
                        strUrl = string.Format("Wsbs/ShenBao.aspx?flowname={0}&iid={1}&flowtype={2}", strName, decValue, strType);
                    }
                    else
                    {
                        strUrl = "Wsbs/SelectSerialOperation.aspx";
                    }
                }
                else if (strStepName == "尚未提交")
                {
                    strUrl = "Wsbs/submit.aspx?flag=0";
                }
                else if (strStepName == "已经提交")
                    strUrl = "Wsbs/submit.aspx?flag=1";
                else if (strStepName == "业务受理")
                {
                    strUrl = "Wsbs/submit.aspx?flag=1";
                    string strIID = cMessage.GetMessageIID(strUserId);
                    if (!string.IsNullOrEmpty(strIID))
                    {
                        DataTable dtInfo = cMessage.GetSubmitInfo(strUserId, strIID);
                        if (dtInfo.Rows.Count > 0)
                        {
                            string strSubmitFlag = dtInfo.Rows[0]["submitflag"].ToString();
                            string strFlowType = dtInfo.Rows[0]["flowtype"].ToString();
                            string strUserName = SysUserHandle.GetUserNameByUserID(strUserId);
                            if (strSubmitFlag == "2")//补发通过办理消息
                            {
                                strUrl = "Wsbs/submit.aspx?flag=3";
                                dtTemp = cMessage.GetRequestSet(0, "通过办理");
                                string strMsgContent = string.Empty;
                                string strStepNo = string.Empty;
                                if (dtTemp.Rows.Count > 0)
                                {
                                    strStepNo = dtTemp.Rows[0]["step_no"].ToString();
                                    strMsgContent = dtTemp.Rows[0]["step_msg"].ToString();
                                    if (!string.IsNullOrEmpty(strMsgContent))//当消息内容设置为空时，将不再发送消息。update by zhongjian 20091230
                                    {
                                        try
                                        {
                                            strMsgContent = string.Format(strMsgContent, strUserName, strFlowType);
                                        }
                                        catch { }
                                        //添加消息
                                        cMessage.InsertMeaasge(strMsgContent, "", strUserName, strUserId);
                                    }
                                    cMessage.UpdateUserStepNO(strUserId, strStepNo);
                                }
                            }
                        }
                    }
                }
                else if (strStepName == "补正补齐")
                    strUrl = "Wsbs/submit.aspx?flag=2";
                else if (strStepName == "通过办理")
                    strUrl = "Wsbs/submit.aspx?flag=3";
                else if (strStepName == "驳回办理")
                    strUrl = "Wsbs/submit.aspx?flag=4";
                else if (strStepName == "不予受理")
                    strUrl = "Wsbs/submit.aspx?flag=5";
                else if (strStepName == "未通过办理")
                    strUrl = "Wsbs/submit.aspx?flag=6";

            }
            return strUrl;
        }
        #endregion
    }
}