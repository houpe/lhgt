﻿using Business.OfficialDoc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace MOAWebService
{
    /// <summary>
    /// Html5Service 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允许使用 ASP.NET AJAX 从脚本中调用此 Web 服务，请取消注释以下行。 
    [System.Web.Script.Services.ScriptService]
    public class Html5Service : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        private void ResponseJsonBack(string strResult)
        {
            string callback = HttpContext.Current.Request["jsoncallback"];
            HttpContext.Current.Response.Write(callback +
                 "(" + strResult + ")");
            //关于result这词是你自己自定义的属性
            //会作为回调参数的属性供你调用结果
            HttpContext.Current.Response.End();
        }

        /// <summary>
        /// 获取用户信息
        /// </summary>
        /// <param name="loginName">用户名称</param>
        /// <param name="password">用户密码</param>
        /// <returns>用户信息</returns>
        [WebMethod]
        public void UserLogin(String loginname, String password)
        {
            string result = SystemHandler.getInstance().UserLogin(loginname.ToUpper(), password);
            ResponseJsonBack(result);
        }

        #region 获取新闻公告等公示信息
        /// <summary>
        /// 加载图片新闻 5条
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public void getImageNews()
        {
            string result = NewsandNoticesHandler.getInstance().getImageNews();

            ResponseJsonBack(result);
        }

        /// <summary>
        ///  根据id获取新闻详细内容
        /// </summary>
        /// <param name="noticeid"></param>
        /// <returns></returns>
        [WebMethod]
        public string GetNewsDetial(string newsId)
        {
            string result = NewsandNoticesHandler.getInstance().GetNewsDetial(newsId);
            return result;
        }

        /// <summary>
        ///  根据id获取通知通知公告详细内容
        /// </summary>
        /// <param name="noticeid">通知书id</param>
        /// <returns></returns>
        [WebMethod]
        public string GetNoticeDetial(string noticeId)
        {
            string result = NewsandNoticesHandler.getInstance().GetNoticeDetial(noticeId);
            return result;
        }
        /// <summary>
        /// 加载更多通知公告
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public string getNoticeMore(string userid, string time, int recordnum)
        {
            string result = NewsandNoticesHandler.getInstance().getNoticeMore(userid, time, recordnum);
            return result;
        }
        #endregion

        #region 通讯录
        [WebMethod]
        public void getDepartments()
        {
            string result = ContactsHandler.getInstance().getDepartmentslist();
            ResponseJsonBack(result);
        }

        [WebMethod]
        public void contactslist(string strDepartId)
        {
            string result = ContactsHandler.getInstance().getcontactslist(strDepartId);
            ResponseJsonBack(result);
        }
        #endregion

        #region 待办事项列表
        /// <summary>
        /// 通过用户获取待办列表
        /// </summary>
        /// <param name="loginName">用户名称</param>
        /// <returns>待办列表</returns>
        [WebMethod]
        public void GetWorkItemGTaskesListJson(String loginname)
        {
            string strReturn = OfficialDocHandler.getInstance().GetWorkItemListJson(loginname);
            ResponseJsonBack(strReturn);
        }

        /// <summary>
        /// 通过用户获取已办列表
        /// </summary>
        /// <param name="loginName">用户名称</param>
        /// <returns>已办列表</returns>
        [WebMethod]
        public void GetWorkItemATaskesListJson(String loginname,string strWhere)
        {
            string strReturn = OfficialDocHandler.getInstance().GetWorkItemSearchListCount(loginname, strWhere);
            ResponseJsonBack(strReturn);
        }

        /// <summary>
        /// 获取工作流资源结构
        /// </summary>
        /// <param name="iid">业务编号</param>
        /// <param name="wiid">业务流程ID</param>
        /// <returns></returns>
        [WebMethod]
        public void GetStepResourceJson(long iid, long wiid)
        {
            string strReturn = OfficialDocHandler.getInstance().GetStepResourceJson(iid, wiid);
            ResponseJsonBack(strReturn);
        }

        /// <summary>
        /// 获取附件资源列表
        /// </summary>
        /// <param name="iid">业务编号</param>
        /// <returns></returns>
        [WebMethod]
        public void GetAttachResourceJson(long iid)
        {
            string strReturn = OfficialDocHandler.getInstance().GetAttachResourceJson(iid);
            ResponseJsonBack(strReturn);
        }
        #endregion
    }
}
