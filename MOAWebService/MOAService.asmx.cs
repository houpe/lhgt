﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Linq;
using Business.FlowOperation;
using Business.OfficialDoc;
using Common;
using Business.Menu;
using System.Data.OleDb;
using WF_Business;

namespace MOAWebService
{
    /// <summary>
    /// MOAService 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    // 若要允许使用 ASP.NET AJAX 从脚本中调用此 Web 服务，请取消对下行的注释。
    [System.Web.Script.Services.ScriptService]
    public class MOAService : System.Web.Services.WebService
    {
        #region MOA系统服务

        /// <summary>
        /// 获取用户信息
        /// </summary>
        /// <param name="loginName">用户名称</param>
        /// <param name="password">用户密码</param>
        /// <returns>用户信息</returns>
        [WebMethod]
        public string UserLogin(String loginname, String password)
        {
            return SystemHandler.getInstance().UserLogin(loginname.ToUpper(), password);
        }

        /// <summary>
        /// 日志记录
        /// </summary>
        /// <param name="log_type"></param>
        /// <param name="log_userid"></param>
        /// <param name="log_device_id"></param>
        /// <param name="log_apk_name"></param>
        /// <param name="log_apk_version"></param>
        /// <param name="log_time"></param>
        /// <param name="log_location"></param>
        /// <param name="log_remark"></param>
        /// <param name="log_remark2"></param>
        /// <param name="log_remark3"></param>
        /// <param name="log_remark4"></param>
        /// <param name="log_intent"></param>
        /// <param name="log_intent_parm"></param>
        /// <param name="log_category"></param>
        /// <returns></returns>
        [WebMethod]
        public int Log(string log_type, string log_userid, string log_device_id, string log_apk_name, string log_apk_version,
            string log_time, string log_location, string log_remark, string log_remark2, string log_remark3, string log_remark4,
            string log_intent, string log_intent_parm, string log_category)
        {
            return SystemHandler.getInstance().Log(log_type, log_userid,
             log_device_id,
             log_apk_name,
             log_apk_version,
             log_time,
             log_location,
             log_remark,
             log_remark2,
             log_remark3,
             log_remark4,
             log_intent,
             log_intent_parm,
             log_category);
        }
        #endregion

        #region 公文系统
        /// <summary>
        /// 通过用户获取待办列表
        /// </summary>
        /// <param name="loginName">用户名称</param>
        /// <returns>待办列表</returns>
        [WebMethod]
        public DataSet GetWorkItemList(String loginname)
        {
            return OfficialDocHandler.getInstance().GetWorkItemList(loginname);
        }


        /// <summary>
        /// 通过用户，公文类型获取待办列表
        /// </summary>
        /// <param name="loginName">用户名称</param>
        /// <returns>已办列表</returns>
        [WebMethod]
        public string GetWorkItemTaskesListCount(String loginname, String official_type)
        {
            string sResult = OfficialDocHandler.getInstance().GetWorkItemSearchListCount(loginname, official_type);
            return sResult;
        }

        /// <summary>
        /// 通过用户获取待办列表
        /// </summary>
        /// <param name="loginName">用户名称</param>
        /// <returns>待办列表</returns>
        [WebMethod]
        public string GetWorkItemGTaskesListJson(String loginname)
        {
            return OfficialDocHandler.getInstance().GetWorkItemListJson(loginname);
        }

        /// <summary>
        /// 通过用户，公文类型获取待办列表--暂无用
        /// </summary>
        /// <param name="loginName">用户名称</param>
        /// <returns>待办列表</returns>
        [WebMethod]
        public string GetWorkItemTaskesListJson(String loginname, String official_type, String official_recordnum, String refresh_time)
        {
            string strResult = "";
            if (String.IsNullOrEmpty(official_recordnum))
            {
                strResult = OfficialDocHandler.getInstance().GetWorkItemSearchListJson(loginname, "SUBITEM_TYPE_NAME = '" + official_type + "' AND ACCEPTED_TIME > TO_DATE('" + refresh_time + "','yyyy-MM-dd hh24:mi:ss')", official_recordnum);
            }
            else
            {
                strResult = OfficialDocHandler.getInstance().GetWorkItemSearchListJson(loginname, "SUBITEM_TYPE_NAME = '" + official_type + "' AND ACCEPTED_TIME <= TO_DATE('" + refresh_time + "','yyyy-MM-dd hh24:mi:ss')", official_recordnum);
            }
            return strResult;
        }

        /// <summary>
        /// 通过用户，公文类型获取待办列表
        /// </summary>
        /// <param name="loginName">用户名称</param>
        /// <returns>待办列表</returns>
        [WebMethod]
        public string GetWorkItemTaskesListJsonWithQuery(String loginname, String official_type, String official_recordnum, String refresh_time, String queryStr)
        {
            string strResult = "";
            if (!String.IsNullOrEmpty(queryStr))
            {
                queryStr += " AND ";
            }
            if (String.IsNullOrEmpty(official_recordnum))
            {
                strResult = OfficialDocHandler.getInstance().GetWorkItemSearchListJson(loginname, queryStr + " SUBITEM_TYPE_NAME = '" + official_type + "' AND ACCEPTED_TIME > TO_DATE('" + refresh_time + "','yyyy-MM-dd hh24:mi:ss')", official_recordnum);
            }
            else
            {
                strResult = OfficialDocHandler.getInstance().GetWorkItemSearchListJson(loginname, queryStr + " SUBITEM_TYPE_NAME = '" + official_type + "' AND ACCEPTED_TIME <= TO_DATE('" + refresh_time + "','yyyy-MM-dd hh24:mi:ss')", official_recordnum);
            }
            return strResult;
        }

        /// <summary>
        /// 通过用户获取查询列表
        /// </summary>
        /// <param name="loginName">用户名称</param>
        /// <param name="searchSql">查询条件</param>
        /// <returns>查询结果列表</returns>
        [WebMethod]
        public string GetWorkItemSearchListJson(String loginname, String searchSql)
        {
            return OfficialDocHandler.getInstance().GetWorkItemSearchListJson(loginname, searchSql);
        }

        /// <summary>
        /// 查询全部业务
        /// </summary>
        /// <param name="loginname"></param>
        /// <param name="searchSql"></param>
        /// <returns></returns>
        [WebMethod]
        public string GetAllWorkItemSearchListJson(String loginname, String searchSql, String official_recordnum)
        {
            return OfficialDocHandler.getInstance().GetAllWorkItemSearchListJson(loginname, searchSql, official_recordnum);
        }

        /// <summary>
        /// 获取工作流资源结构
        /// </summary>
        /// <param name="iid">业务编号</param>
        /// <param name="wiid">业务流程ID</param>
        /// <returns></returns>
        [WebMethod]
        public string GetStepResourceJson(long iid, long wiid)
        {
            return OfficialDocHandler.getInstance().GetStepResourceJson(iid, wiid);
        }

        /// <summary>
        /// 获取附件资源列表
        /// </summary>
        /// <param name="iid">业务编号</param>
        /// <returns></returns>
        [WebMethod]
        public string GetAttachResourceJson(long iid)
        {
            return OfficialDocHandler.getInstance().GetAttachResourceJson(iid);
        }

        /// <summary>
        /// 获取工作流节点的表单信息
        /// </summary>
        /// <param name="iid">业务ID</param>
        /// <param name="wiid">业务流程ID</param>
        /// <param name="wid">流程ID</param>
        /// <param name="fid"></param>
        /// <returns></returns>
        [WebMethod]
        public string GetFormDetail(long iid, long wiid, string wid, string fid, string input_index)
        {
            string step = WorkFlowHandle.GetStepByWiid(wiid);
            string layoutid = OfficialDocHandler.getInstance().GetFormLayoutID(fid);
            string formPermission = OfficialDocHandler.getInstance().GetFormPermissionJson(wid, step, layoutid);
            string formDetail = OfficialDocHandler.getInstance().GetFormDetailJson(layoutid, iid, input_index);
            string result = "{Layout:" + layoutid + ",Permission:" + formPermission + ",Detail:" + formDetail + "}";
            return result;

        }

        /// <summary>
        /// 获取工作流节点的表单信息
        /// </summary>
        /// <param name="iid">业务ID</param>
        /// <param name="wiid">业务流程ID</param>
        /// <param name="wid">流程ID</param>
        /// <param name="fid"></param>
        /// <returns></returns>
        [WebMethod]
        public string GetOfficialFormDetail(long iid, long wiid, string wid, string fid)
        {
            string step = WorkFlowHandle.GetStepByWiid(wiid);

            //设置为打开状态，即actived=1
            WorkFlowHandle.SetOpened(iid.ToString(), wiid.ToString(), step);
            //获取布局和布局的权限
            string layoutid = OfficialDocHandler.getInstance().GetFormLayoutID(fid);
            string formPermission = OfficialDocHandler.getInstance().GetFormPermissionJson(wid, step, layoutid);
            //获取布局对应的表单明细
            string formDetail = OfficialDocHandler.getInstance().GetFormDetailJson(layoutid, iid);
            OfficialOPModel.PermissionTypes opPermission = (int)OfficialOPModel.PermissionTypes.None;  // 0:隐藏公文

            OfficialOPModel.addOPEdit(ref opPermission);

            string result = "{Layout:" + layoutid + ",Permission:" + formPermission + ",Detail:" + formDetail + ",OPPermission:" + (int)opPermission + "}";
            return result;

        }

        /// <summary>
        /// 保存（提交）流程节点信息
        /// </summary>
        /// <param name="iid">业务ID</param>
        /// <param name="wiid">业务流程ID</param>
        /// <param name="wid">流程ID</param>
        /// <param name="step">流程节点名</param>
        /// <param name="userid">用户名</param>
        /// <param name="op">当前节点的处理意见</param>
        /// <param name="viewUpdate">JSONObject字符串，各表单页面修改信息</param>        
        /// <param name="doSubmit">是否执行提交</param>
        /// <returns>保存结果或者流转用户列表</returns>
        [WebMethod]
        public string WorkFlowSaveOrSubmit(long iid, long wiid, string viewupdate, string dosubmit)
        {
            string result = OfficialDocHandler.getInstance().WorkFlowSaveOfSubmit(iid, wiid, viewupdate, dosubmit == "True");
            return result;
        }

        /// <summary>
        /// 保存（提交）流程节点信息
        /// </summary>
        /// <param name="iid">业务ID</param>
        /// <param name="wiid">业务流程ID</param>
        /// <param name="wid">流程ID</param>
        /// <param name="step">流程节点名</param>
        /// <param name="userid">用户名</param>
        /// <param name="op">当前节点的处理意见</param>
        /// <param name="viewUpdate">JSONObject字符串，各表单页面修改信息</param>        
        /// <param name="doSubmit">是否执行提交</param>
        /// <returns>保存结果或者流转用户列表</returns>
        [WebMethod]
        public string OfficialSaveOrSubmit(long iid, long wiid, string step, string userid, string op, string viewupdate, string dosubmit)
        {
            string result = OfficialDocHandler.getInstance().WorkFlowSaveOfSubmit(iid, wiid, step, userid, op, viewupdate, dosubmit == "True");
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="iid"></param>
        /// <param name="wiid"></param>
        /// <param name="jsonUsers"></param>
        /// <returns></returns>
        [WebMethod]
        public string WorkFlowSubmit(long iid, long wiid, string fname, string ename, string users)
        {
            string result = OfficialDocHandler.getInstance().WorkFlowSubmit(iid, wiid, fname, ename, users);
            return result;
        }

        /// <summary>
        /// 公文意见列表
        /// </summary>
        /// <param name="iid">业务id</param>
        /// <returns></returns>
        [WebMethod]
        public string GetOfficialOPByIID(long iid, long wiid, string step)
        {
            DataTable dt = OfficialDocHandler.getInstance().GetOfficialOPByIID(iid, wiid, step);
            return Common.JsonOperation.DataTableToJson(dt);
        }

        /// <summary>
        /// 获取各业务类别的统计数据
        /// </summary>
        /// <param name="strUserId">当前登录用户id，通过判定可查看的业务</param>
        /// <returns></returns>
        [WebMethod]
        public string GetWorkflowStatus(string strUserId)
        {
            ClsUserWorkFlow cuwTemp = new ClsUserWorkFlow();
            DataTable dtSource = cuwTemp.GetWorkflowStatus();
            string strReturn = string.Empty;
            if (dtSource.Rows.Count > 0)
            {
                JsonOperation _djson = new JsonOperation();
                strReturn = _djson.BindJson(dtSource);
            }
            return strReturn;
        }
        #endregion

        #region 新闻and通知公告

        /// <summary>
        /// 新闻列表刷新 下拉刷新
        /// </summary>
        /// <param name="oldtime">上次刷新时间</param>
        /// <returns></returns>
        ///    [WebMethod]
        [WebMethod]
        public string RefreshNewsList(String refreshtime)
        {
            string result = NewsandNoticesHandler.getInstance().RefreshNewsList(refreshtime);
            return result;
        }

        /// <summary>
        /// 加载更多新闻
        /// </summary>
        /// <param name="time"> 初始刷新时间字符串 | 格式为 ：yyyy-mm-dd hh24:mi:ss</param>
        /// <param name="recordnum"> 当前加载的数据条数。</param>
        /// <returns></returns>
        [WebMethod]
        public string getNewsMore(string time, int recordnum)
        {
            string result = NewsandNoticesHandler.getInstance().getNewsMore(time, recordnum);
            return result;
        }
        /// <summary>
        /// 加载图片新闻 5条
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public string getImageNews()
        {
            string result = NewsandNoticesHandler.getInstance().getImageNews();
            return result;
        }

        /// <summary>
        ///  根据id获取新闻详细内容
        /// </summary>
        /// <param name="noticeid"></param>
        /// <returns></returns>
        [WebMethod]
        public string GetNewsDetial(string newsId)
        {
            string result = NewsandNoticesHandler.getInstance().GetNewsDetial(newsId);
            return result;
        }

        /// <summary>
        /// 通知公告列表刷新 下拉刷新
        /// </summary>
        /// <param name="oldtime">上次刷新时间</param>
        /// <returns></returns>
        ///    [WebMethod]
        [WebMethod]
        public string RefreshNoticeList(String refreshtime)
        {
            string result = NewsandNoticesHandler.getInstance().RefreshNoticeList(refreshtime);
            return result;
        }
        /// <summary>
        ///  根据id获取通知通知公告详细内容
        /// </summary>
        /// <param name="noticeid">通知书id</param>
        /// <returns></returns>
        [WebMethod]
        public string GetNoticeDetial(string noticeId)
        {
            string result = NewsandNoticesHandler.getInstance().GetNoticeDetial(noticeId);
            return result;
        }
        /// <summary>
        /// 加载更多通知公告
        /// </summary>
        /// <param name="time"> 初始刷新时间字符串 | 格式为 ：yyyy-mm-dd hh24:mi:ss</param>
        /// <param name="recordnum"> 当前加载的数据条数。</param>
        /// <returns></returns>
        [WebMethod]
        public string getNoticeMore(string userid, string time, int recordnum)
        {
            string result = NewsandNoticesHandler.getInstance().getNoticeMore(userid, time, recordnum);
            return result;
        }
        /// <summary>
        /// 获取未办理数据条数
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public string GetNoticeUndoNum(string userid)
        {
            string result = NewsandNoticesHandler.getInstance().getNoticeMoreCount(userid);
            return result;
        }

        [WebMethod]
        public string UpdateNoticeReadFlag(string userid, string issueid)
        {
            string result = NewsandNoticesHandler.getInstance().UpdateNoticeReadFlag(userid, issueid);
            return result;
        }
        #endregion

        #region 通讯录
        [WebMethod]
        public string getDepartments()
        {
            string result = ContactsHandler.getInstance().getDepartmentslist();
            return result;
        }

        [WebMethod]
        public string contactslist()
        {
            string result = ContactsHandler.getInstance().getcontactslist("");
            return result;
        }
        #endregion

        #region 系统版本管理
        [WebMethod]
        public string GetSystemVersion()
        {
            string result = SystemHandler.getInstance().GetSystemVersion();
            return result;
        }
        #endregion

        #region 内网审批系统菜单管理
        /// <summary>
        /// 获取父级菜单
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public string GetParentMenu()
        {
            XtMenuOperation xmoTemp = new XtMenuOperation();
            DataTable dtTemp = xmoTemp.GenerateParentMenu();

            string strReturn = string.Empty;
            if (dtTemp.Rows.Count > 0)
            {
                JsonOperation _djson = new JsonOperation();
                strReturn = _djson.BindJson(dtTemp);
            }
            return strReturn;
        }

        
        /// <summary>
        /// 获取子菜单
        /// </summary>
        /// <param name="strId"></param>
        /// <returns></returns>
        [WebMethod]
        public string GetChildMenuList(string strId)
        {
            XtMenuOperation xmoTemp = new XtMenuOperation();
            DataTable dtTemp = xmoTemp.GetChildMenu(strId);

            string strReturn = string.Empty;
            if (dtTemp.Rows.Count > 0)
            {
                JsonOperation _djson = new JsonOperation();
                strReturn = _djson.BindJson(dtTemp);
            }
            return strReturn;
        }
        #endregion

        #region 六合门户网站操作
        /// <summary>
        /// 返回网站文章类别第一级列表
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public string GetFirstMenu()
        {
            string strReturn = string.Empty;
            string sql = "select classid,c_name from tb_class order by classid asc ";
            DataTable dtTemp;
            SysParams.OAConnection().RunSql(sql, out dtTemp);
            if (dtTemp.Rows.Count > 0) {
                JsonOperation _djson = new JsonOperation();
                strReturn = _djson.BindJson(dtTemp);
            }
            return strReturn;
        }
        
        /// <summary>
        /// 返回网站文章类别第二级列表
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public string GettwoMenu()
        {
            string strReturn = string.Empty;
            string sql = "select t.s_id,t.S_NAME ,tt.classid,tt.c_name from TB_SCLASS t ,TB_CLASS tt where t.classid=tt.classid  order by tt.c_name ASC";
            DataTable dtTemp;
            SysParams.OAConnection().RunSql(sql, out dtTemp);
            if (dtTemp.Rows.Count > 0) {
                JsonOperation _djson = new JsonOperation();
                strReturn = _djson.BindJson(dtTemp);
            }
            return strReturn;
        }
       
        /// <summary>
        /// 针对文章类别执行查增删改SQL语句并返回
        /// </summary>
        /// <param name="type">0是新增,1是更新,2是删除,3是通过id查询相应的目录名称</param>
        /// <param name="level">0表示表TB_CLASS,1表示表tb_sclass</param>
        /// <param name="par">字段</param>
        /// <param name="oldpar">旧值</param>
        /// <param name="newpar">新值</param>
        /// <returns></returns>
        [WebMethod]
        public string ExSql(string type, string level,string par,string oldpar,string newpar) 
        {
            string strReturn = string.Empty;
            string strSql = string.Empty;

            switch (type) 
            { 
                case "0":
                    if (level == "0")
                    {
                        strSql = string.Format("insert into TB_CLASS (CLASSID,C_NAME) values((select max(CLASSID)+1 FROM TB_CLASS),'{0}')",par);

                        strReturn = SysParams.OAConnection().RunSql(strSql).ToString();
                    }
                    else if (level == "1")
                    {
                        strSql = string.Format("insert into tb_sclass (s_id,s_name,classid) values((select max(S_ID)+1 from tb_sclass),'{0}','{1}')",par,newpar);

                        strReturn = SysParams.OAConnection().RunSql(strSql).ToString();
                    }
                    break;
                case "1":
                    if (level == "0")
                    {
                        strSql = string.Format("update  TB_CLASS set C_NAME='{0}' where classid='{1}'",newpar,oldpar);

                        strReturn = SysParams.OAConnection().RunSql(strSql).ToString();
                    }
                    else if (level == "1")
                    {
                        strSql = string.Format("update tb_sclass set S_NAME='{0}' where S_ID='{1}'",newpar,oldpar);

                        strReturn = SysParams.OAConnection().RunSql(strSql).ToString();
                    }
                    break;
                case "2":
                    if (level == "0")
                    {
                        strSql = string.Format("delete TB_CLASS  where CLASSID ='{0}'", par);
                        strReturn = SysParams.OAConnection().RunSql(strSql).ToString();
                    }
                    else if (level == "1")
                    {
                        strSql = string.Format("delete tb_sclass  where S_ID = '{0}'", par);
                        strReturn = SysParams.OAConnection().RunSql(strSql).ToString();
                    }
                    break;
                case "3":
                    if (level == "0")
                    {
                        strSql = string.Format("select c_name  from tb_class where classid='{0}'", par);

                        DataTable dtTemp;
                        SysParams.OAConnection().RunSql(strSql, out dtTemp);
                        if (dtTemp.Rows.Count > 0)
                        {
                            JsonOperation _djson = new JsonOperation();
                            strReturn = _djson.BindJson(dtTemp);
                        }
                    }
                    else if (level == "1")
                    {
                        strSql = string.Format("select tb_sclass.s_name, tb_class.c_name, tb_sclass.s_id from tb_sclass  inner join  tb_class  on tb_sclass.classid=tb_class.classid  where tb_sclass.s_id='{0}' ", par);

                        DataTable dtTemp;
                        SysParams.OAConnection().RunSql(strSql, out dtTemp);
                        if (dtTemp.Rows.Count > 0)
                        {
                            JsonOperation _djson = new JsonOperation();
                            strReturn = _djson.BindJson(dtTemp);
                        }
                    }
                    break; 
            }

            return strReturn;


        }
        
        /// <summary>
        /// 根据查询条件获取后台管理第一级菜单栏目
        /// </summary>
        /// <param name="con">条件</param>
        /// <returns></returns>
        [WebMethod]
        public string GetLeftFirstMenu()
        {
            string strReturn = string.Empty;
            string strSql = string.Empty;
           strSql = "select id, menuname  from tb_menuclass where pid='-1'";
         //  strSql = string.Format("select id, menuname  from TB_MENUCLASS where pid='-1' {0}",con);
            DataTable dtTemp;
            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            if (dtTemp.Rows.Count > 0)
            {
                JsonOperation _djson = new JsonOperation();
                strReturn = _djson.BindJson(dtTemp);
            }
            return strReturn; 
        }

        /// <summary>
        /// 根据查询条件获取后台管理第二级菜单栏目
        /// </summary>
        /// <param name="con">条件</param>
        /// <returns></returns>
        [WebMethod]
        public string GetSecondLeftMenu(string con, string menuName, string role)
        {
            string strReturn = string.Empty;
            string strSql = string.Empty;
            if (role == "0")
            {
                strSql = string.Format(" select menuname ,navurl from TB_MENUCLASS where to_char(id) in  ('" + con.Replace("|", "','") + "') and pid=(select id from TB_MENUCLASS where menuname='{1}')", con, menuName);
            }
            else if (role == "1")
            {
                strSql = string.Format("select menuname ,navurl from TB_MENUCLASS where pid=(select id from TB_MENUCLASS where menuname='{0}')",menuName);
            }
            DataTable dtTemp;
            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            if (dtTemp.Rows.Count > 0)
            {
                JsonOperation _djson = new JsonOperation();
                strReturn = _djson.BindJson(dtTemp);
            }
            return strReturn; 
        }

        /// <summary>
        /// 通过用户查找可以加载的菜单
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [WebMethod]
        public string GetMenuByUser(string user,string menuClassID)
        {
            string strReturn = string.Empty;
            string strSql = string.Empty;

            string strRole="select QX from tb_admin where name='"+user+"'";
            DataTable dtRoleTemp;
            SysParams.OAConnection().RunSql(strRole, out dtRoleTemp);
            DataTable dtTempPower;
            DataTable resultTable=new DataTable();
            if(dtRoleTemp.Rows.Count > 0)
            {
                if (dtRoleTemp.Rows[0]["QX"].ToString() == "1")
                {
                    string cmd = "select menuname from TB_MENUCLASS where pid='-1'";

                    resultTable.Columns.Add("菜单权限", typeof(string));
                    string resultSql = string.Empty;
                    SysParams.OAConnection().RunSql(cmd, out dtTempPower);
                    if (dtTempPower.Rows.Count > 0)
                    {
                        int index = 0;
                        for(int i = 0 ; i < dtTempPower.Rows.Count ; i++)
                        {
                            if (index == 0)
                            {
                                resultSql += dtTempPower.Rows[i]["menuname"].ToString();
                                index++;
                            }
                            else
                            {
                                resultSql += ("," + dtTempPower.Rows[i]["menuname"].ToString());
                            }

                        }
                        
                    }
                    DataRow dr = resultTable.NewRow();
                    dr["菜单权限"] = resultSql;
                    resultTable.Rows.Add(dr);
                    JsonOperation _djson = new JsonOperation();
                    strReturn = _djson.BindJson(resultTable);
                }
                else
                {
                    resultTable.Columns.Add("菜单权限", typeof(string));
                  
                    strSql = "select menuname from TB_MENUCLASS where  to_char(id ) in  ('" + menuClassID.Replace("|", "','") + "') and pid='-1'";
                    DataTable dtTemp;
                    SysParams.OAConnection().RunSql(strSql, out dtTemp);

                    if (dtTemp.Rows.Count > 0)
                    {
                        DataRow dr = resultTable.NewRow();
                        string resultmenu="";
                        for (int i = 0; i < dtTemp.Rows.Count; i++)
                        {
                            if (i != dtTemp.Rows.Count - 1)
                            {
                                resultmenu += (dtTemp.Rows[i]["menuname"].ToString() + ",");
                            }
                            else
                            {
                                resultmenu += dtTemp.Rows[i]["menuname"].ToString() ;
                            }
                        }
                            //dr["菜单权限"] = dtTemp.Rows[0]["menuname"].ToString();
                        dr["菜单权限"] = resultmenu;
                        resultTable.Rows.Add(dr);
                        JsonOperation _djson = new JsonOperation();
                        strReturn = _djson.BindJson(resultTable);
                    }

                  
                }
            }

            
            return strReturn; 

        }

        [WebMethod]
        ///判断用户是否是管理员如果是返回1如果不是返回0
        public string GetRoleByUser(string user)
        {
            string resultRole="";
         string strRole="select QX from tb_admin where name='"+user+"'";
            DataTable dtRoleTemp;
            SysParams.OAConnection().RunSql(strRole, out dtRoleTemp);
           // DataTable dtTempPower;
            //DataTable resultTable=new DataTable();
            if (dtRoleTemp.Rows.Count > 0)
            {
                if (dtRoleTemp.Rows[0]["QX"].ToString() == "1")
                {
                    resultRole = "1";
                }
                else
                {
                    resultRole = "0";
                }
            }
            return resultRole;

        }
        #endregion
    }
}
