<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PopupSelectPage.aspx.cs"
    Inherits="PopupSelectPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>��ѡ��</title>

    <script type="text/javascript">
        function setParent(displayGuid, valueGuid, displayText, valueText,otherCtrl)
        {
            var display = getElementByGuid(window.opener.document, displayGuid);
            var value = getElementByGuid(window.opener.document, valueGuid);
            display.value = displayText;
            if(valueText == null || valueText=="")
            {
                valueText = displayText;
            }
            value.value = valueText;
            
            if( otherCtrl!=null && otherCtrl!="")
            {
                var tempCtrl = getElementByInputId(window.opener.document,otherCtrl);
                
                if( tempCtrl != null)
                {
                    if( tempCtrl.value !="")
                    {
                        tempCtrl.value+=",";
                    }
                    tempCtrl.value+=displayText;
                }
            }
        }
        
        function getElementByInputId(doc, ctrlId)
        {
            var inputs = doc.getElementsByTagName("textarea");
            for(var i=0; i<inputs.length;i++)
            {
                var input = inputs[i];

                if(input.id == ctrlId||input.name == ctrlId)
                {
                    return input;
                }
            }
        }
        
        function getElementByGuid(doc, guid)
        {
            var inputs = doc.getElementsByTagName("input");
            for(var i=0; i<inputs.length;i++)
            {
                var input = inputs[i];
                if(input.guid == guid)
                {
                    return input;
                }
            }
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div style="text-align: center;">
            <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
            <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="����" SkinID="LightGreen"/>
            <defineControls:CustomGridView ID="CustomGridView1" runat="server" AllowPaging="true" SkinID="List"
                Width="550px" ShowHideColModel="None" ShowGridViewSelect="MultiSelect" OnOnCancelClick="CustomGridView1_OnCancelClick"
                OnOnComplelteClick="CustomGridView1_OnComplelteClick" PageSize="15" OnOnLoadData="CustomGridView1_OnLoadData"
                DivTrdClass-DivButtonClass="button">
                <PagerSettings Visible="False" />
                <HeaderStyle CssClass="top_table" Height="30px" />
                <RowStyle CssClass="inputtable" />
            </defineControls:CustomGridView>
        </div>
    </form>
</body>
</html>
