﻿<%@ WebHandler Language="C#" Class="AjaxInfoProcess" %>

using System;
using System.Web;
using System.Web.SessionState;

public class AjaxInfoProcess : IHttpHandler,IRequiresSessionState
{

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
    
    /// <summary>
    /// 用户校验逻辑
    /// </summary>
    /// <param name="context"></param>
    public void ProcessRequest(HttpContext context)
    {
        string strReturn = string.Empty;
        if (HttpContext.Current.Session["CheckCode"] != null)
        {
            string[] args = context.Request["params"].Split(',');
            //string userid = args[0].ToUpper();
            string userid = args[0];//区分大小写
            string password = args[1];
            string checkcode = args[2];

            if (checkcode.ToUpper() != HttpContext.Current.Session["CheckCode"].ToString())//验证码
            {
                strReturn = "验证码错误！";
            }
            else
            {
                //验证是否是管理员
                if (SbBusiness.SystemConfig.AdminUser.CompareTo(userid) != 0)
                {
                    bool userExist = true;

                    userExist =SbBusiness.User.SysUserHandle.CheckEncodePassword(userid, password);

                    if (!userExist)
                    {
                        strReturn = "用户名和密码错误！";
                    }
                    else
                    {
                        HttpContext.Current.Session["title"] = "国家测绘地理信息局业务申报系统";

                        HttpContext.Current.Session.Remove("Menu");

                        //对预审标志初始化
                        HttpContext.Current.Session["PromiseFlag"] = string.Empty;
                        HttpContext.Current.Session["UserID"] = userid;
                        HttpContext.Current.Session["LoginUserName"] = SbBusiness.User.SysUserHandle.GetUserNameByUserID(userid);
                        RecordLog(userid, context);
                    }
                }
                else
                {
                    HttpContext.Current.Session["title"] = "国家测绘地理信息局业务申报系统后台维护";
                }
            }
        }
        else
        {
            strReturn = "校验码为空";
        }

        context.Response.Write(strReturn);

    }

    #region 记录登录日志
    /// <summary>
    /// 记录登录日志
    /// </summary>
    /// <param name="UserName"></param>
    private void RecordLog(string UserName, HttpContext context )
    {
        string UserIP = context.Request.UserHostName.ToString();
        SbBusiness.SystemLogs wyLog = new SbBusiness.SystemLogs();
        wyLog.Inputlog(UserName, "登录外网申报系统", string.Empty, UserIP);
    }
    #endregion 
}