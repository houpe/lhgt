﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using SbBusiness;

using WebControls;
using Business.Admin;


public partial class UserControls_GeneralSelectAndInput : System.Web.UI.UserControl, IUserControl
{
    private DataTable _dtSouse;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {            
            Render();
        }
    }

    #region 属性
    /// <summary>
    /// 在SYS_PARAMS里的Name
    /// </summary>
    public string Name
    {
        get { return ViewState["GeneralSelect_Name"] as string; }
        set { ViewState["GeneralSelect_Name"] = value; }
    }

    public DataTable dtSouse
    {
        get { return _dtSouse; }
        set { _dtSouse = value; }
    }

    /// <summary>
    /// 客户端id
    /// </summary>
    public override string ClientID
    {
        get
        {
            return ShowSelText.ClientID;
        }
    }

    /// <summary>
    /// 显示的值
    /// </summary>
    public string Text
    {
        get
        {
            if (ShowSelText.Text!=null)
            {
                return ShowSelText.Text;
            }
            else
            {
                return string.Empty;
            }
        }
        set
        {
            ShowSelText.Text = value;
        }
    }

    /// <summary>
    /// 选择的值
    /// </summary>
    public string Value
    {
        get { return ShowSelText.Text; }
        set { ShowSelText.Text = value; }
    }

    /// <summary>
    /// 宽度
    /// </summary>
    public Unit Width
    {
        get { return ddlControl.Width; }
        set { ddlControl.Width = value; }
    }

    /// <summary>
    /// 只读
    /// </summary>
    public bool ReadOnly
    {
        get { return ddlControl.Enabled; }
        set { ddlControl.Enabled = value; }
    }

    #endregion
    protected void Render()
    {
        ReadOnly = true;
        DataTable dtTable = DictOperation.GetSysParams(Name, "");
        _dtSouse = dtTable;
        int iWidth = Convert.ToInt32(ShowSelText.Width.Value);
        if (iWidth == 0)
        {
            iWidth = 120;
            ShowSelText.Width = Unit.Parse("120px");
        }

        int sWidth = iWidth + 23;
        int spanWidth = sWidth - 23;



        ddlControl.Width = Unit.Parse(sWidth.ToString() + "px");
        ddlControl.Height = Unit.Parse("20px");
        //ddlControl.Style.Add("MARGIN-LEFT", "-" + spanWidth.ToString() + "px");
        ddlControl.ID = ShowSelText.ID + "_Select";
        ddlControl.Attributes.Add("onchange", "this.parentNode.nextSibling.value=this.value");
        ddlControl.Attributes.Add("onfocus", "" + this.getFocusScript() + "");

        if (_dtSouse != null)
        {
            if (_dtSouse.Rows.Count > 0)
            {
                ddlControl.DataSource = _dtSouse;
                ddlControl.DataValueField = _dtSouse.Columns[1].ColumnName;
                ddlControl.DataTextField = _dtSouse.Columns[1].ColumnName;
                ddlControl.DataBind();
            }
        }

        ShowSelText.Style.Clear();
        ShowSelText.Width = Unit.Parse(iWidth.ToString() + "px");
        ShowSelText.Style.Add("left", "0px");
        ShowSelText.Style.Add("position", "absolute");
    }

    private string getFocusScript()
    {
        string strScript = "\n";
        strScript += "var isExist = -2;\n";
        strScript += "var obj = event.srcElement;\n";
        strScript += "var str = this.parentNode.nextSibling.value;\n";
        strScript += "var ary = obj.options;\n";
        strScript += "for(var i=0;i<ary.length;i++){\n";
        strScript += " if(str == ary[i].text){\n";
        strScript += "  isExist = i;\n";
        strScript += "  break;\n";
        strScript += " }\n";
        strScript += "}\n";
        strScript += "if(isExist != -2){\n";
        strScript += " obj.selectedIndex = isExist;\n";
        strScript += "}\n";
        strScript += "else{\n";
        strScript += " obj.selectedIndex = -1;\n";
        strScript += "}\n";

        return strScript;
    }
}
