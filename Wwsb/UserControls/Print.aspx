﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Print.aspx.cs" Inherits="Controls_User_Print" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>打印</title>
    <object classid="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2" height="0" width="0"
        id="WebBrowser" name="WebBrowser">
    </object>

    <script type="text/javascript">
    function initSetCtrlVisible(needAppearCtrls,displayStatus)
    {
        if( needAppearCtrls!=null || needAppearCtrls!="")
        {
            var arrAppearCtrls = needAppearCtrls.split(',');
            for(var i=0;i<arrAppearCtrls.length;i++)
            {
               var ctrl = document.getElementById( arrAppearCtrls[i]);
               if( ctrl != null)
               {
                   ctrl.style.display = displayStatus;
               }
            }
        }
    } 
    function setTableSize()
    {
        var str=document.getElementById("Gridviewid").value;
        if(str!=null&&str!="")
        {
            var obj=document.getElementById(str);
            obj.style.height="900px";
            obj.style.width="100%";
        }
    }
    </script>

</head>
<body onload="setTableSize();">
    <form id="form1" runat="server">
        <div style="text-align: center">
            <%= PrintContent%>

            <script type="text/javascript">
                initSetCtrlVisible('<%=AppearCtrlParamInfo%>','block');
                initSetCtrlVisible('<%=HiddenCtrlParamInfo%>','none');
            </script>

            <table>
                <tr>
                    <td style="width: 509px" align="center">
                        <asp:Button ID="button_print" runat="server" Text="打印" SkinID="LightGreen" CausesValidation="False"
                            UseSubmitBehavior="False" />
                        <asp:Button ID="button_setup" runat="server" Text="打印设置" SkinID="LightGreen" CausesValidation="False"
                            UseSubmitBehavior="False" />
                        <asp:Button ID="button_preview" runat="server" Text="打印预览" SkinID="LightGreen" CausesValidation="False"
                            UseSubmitBehavior="False" />
                        <asp:HiddenField ID="Gridviewid" runat="server" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
