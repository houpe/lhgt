﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


using SbBusiness.User;
using Business.Admin;

public partial class UserControls_SecionSelectMultiple : System.Web.UI.UserControl
{
    /// <summary>
    /// 选择的值，用逗号隔开
    /// </summary>
    public string SelectedValue
    {
        get { return DropDownCheckList1.SelectedValuesToString(","); }
        set { DropDownCheckList1.SelectedValue = value; }
    }

    /// <summary>
    /// 选择的文本，用逗号隔开
    /// </summary>
    public string SelectedText
    {
        get { return DropDownCheckList1.SelectedLabelsToString(","); }
    }

    /// <summary>
    /// 获取列表项
    /// </summary>
    public ListItemCollection Items 
    {
        get { return DropDownCheckList1.Items;}
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void DropDownCheckList1_Init(object sender, EventArgs e)
    {
        DropDownCheckList1.DropImageSrc = Page.ResolveUrl("~/App_Themes/SkinFile/Images/toolbardrop.gif");
        BindData();
    }

    private void BindData()
    {
        DataTable dtParams = DictOperation.GetSysParams("区属", "");

        SysUserHandle userOperation = new SysUserHandle();
        DataTable dtRegion = userOperation.GetUserTypeRegionByUserID(Session["UserId"].ToString());

        string strSectionId = string.Empty;
        DropDownCheckList1.Items.Clear();

        if (dtRegion.Rows.Count > 0)
        {
            strSectionId = dtRegion.Rows[0]["region_id"].ToString();
        }

        if (!string.IsNullOrEmpty(strSectionId))//外网
        {
            ListItem lstItemFirst = new ListItem();
            lstItemFirst.Text = dtRegion.Rows[0]["area"].ToString();
            lstItemFirst.Value = strSectionId;
            DropDownCheckList1.Items.Add(lstItemFirst);
            DropDownCheckList1.SelectedIndex = 0;
        }
        else//内网
        {
            ListItem lstItemFirst = new ListItem();
            lstItemFirst.Text = "全部";
            lstItemFirst.Value = "-1";
            DropDownCheckList1.Items.Add(lstItemFirst);

            foreach (DataRow drArea in dtParams.Rows)
            {
                ListItem lstItem = new ListItem();
                lstItem.Text = drArea["KeyValue"].ToString();
                lstItem.Value = drArea["KEYCODE"].ToString();
                DropDownCheckList1.Items.Add(lstItem);
            }
        }
    }
}
