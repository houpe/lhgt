﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


using WebControls;
using Business.Common;
using Business.Struct;
using Common;

public partial class UserControls_FileUpdate : System.Web.UI.UserControl
{
    private UploadFileClass uploadFileClass = new UploadFileClass();

    #region 属性

    /// <summary>
    /// 主键ID
    /// </summary>
    public string KeyId
    {
        set
        {
            ViewState["KeyId"] = value;
        }
        get
        {
            if (ViewState["KeyId"] != null)
            {
                return ViewState["KeyId"].ToString();
            }
            else
            {
                return string.Empty;
            }
        }
    }

    /// <summary>
    /// 主键ID值
    /// </summary>
    public string KeyIdValue
    {
        set
        {
            ViewState["KeyIdValue"] = value;
        }
        get
        {
            if (ViewState["KeyIdValue"] != null)
            {
                return ViewState["KeyIdValue"].ToString();
            }
            else
            {
                return string.Empty;
            }
        }
    }

    /// <summary>
    /// 存放的表名
    /// </summary>
    public string TableName
    {
        set
        {
            ViewState["TableName"] = value;
        }
        get
        {
            if (ViewState["TableName"] != null)
            {
                return ViewState["TableName"].ToString();
            }
            else
            {
                return string.Empty;
            }
        }
    }

    /// <summary>
    /// 文件存储的字段名
    /// </summary>
    public string FileStoreFieldName
    {
        set
        {
            ViewState["FileStoreFieldName"] = value;
        }
        get
        {
            if (ViewState["FileStoreFieldName"] != null)
            {
                return ViewState["FileStoreFieldName"].ToString();
            }
            else
            {
                return string.Empty;
            }
        }
    }

    /// <summary>
    /// 文件类型存储的字段名
    /// </summary>
    public string FileTypeFieldName
    {
        set
        {
            ViewState["FileTypeFieldName"] = value;
        }
        get
        {
            if (ViewState["FileTypeFieldName"] != null)
            {
                return ViewState["FileTypeFieldName"].ToString();
            }
            else
            {
                return string.Empty;
            }
        }
    }

    /// <summary>
    /// 存储的关键字段名
    /// </summary>
    public string StoreKeyFieldName
    {
        set
        {
            ViewState["StoreKeyFieldName"] = value;
        }
        get
        {
            if (ViewState["StoreKeyFieldName"] != null)
            {
                return ViewState["StoreKeyFieldName"].ToString();
            }
            else
            {
                return string.Empty;
            }
        }
    }

    /// <summary>
    /// 存储的关键字段值
    /// </summary>
    public string StoreKeyValue
    {
        set
        {
            ViewState["StoreKeyValue"] = value;
        }
        get
        {
            if (ViewState["StoreKeyValue"] != null)
            {
                return ViewState["StoreKeyValue"].ToString();
            }
            else
            {
                return string.Empty;
            }
        }
    }

    /// <summary>
    /// 存储资源名称的字段名
    /// </summary>
    public string StoreResourceFieldName
    {
        set
        {
            ViewState["StoreResourceFieldName"] = value;
        }
        get
        {
            if (ViewState["StoreResourceFieldName"] != null)
            {
                return ViewState["StoreResourceFieldName"].ToString();
            }
            else
            {
                return string.Empty;
            }
        }
    }

    /// <summary>
    /// 产生的新ID值
    /// </summary>
    public string NewIdValue
    {
        get
        {
            if (!string.IsNullOrEmpty(ViewState["NewId"].ToString()))
            {
                return ViewState["NewId"].ToString();
            }
            else
            {
                return string.Empty;
            }
        }
    }

    /// <summary>
    /// 设置需要获取值的控件的名称
    /// </summary>
    public string StoreResourceNameControlName
    {
        set
        {
            ViewState["StoreResourceNameControlName"] = value;
        }
        get
        {
            if (ViewState["StoreResourceNameControlName"] != null)
            {
                return ViewState["StoreResourceNameControlName"].ToString();
            }
            else
            {
                return string.Empty;
            }
        }
    }

    /// <summary>
    /// 存放资源的类型
    /// </summary>
    public string StoreResourceType
    {
        set
        {
            ViewState["StoreResourceType"] = value;
        }
        get
        {
            if (ViewState["StoreResourceType"] != null)
            {
                return ViewState["StoreResourceType"].ToString();
            }
            else
            {
                return string.Empty;
            }
        }
    }

    /// <summary>
    /// 存放数据字典对应值的字段名
    /// </summary>
    public string StoreParamFieldName
    {
        set
        {
            ViewState["StoreParamName"] = value;
        }
        get
        {
            if (ViewState["StoreParamName"] != null)
            {
                return ViewState["StoreParamName"].ToString();
            }
            else
            {
                return string.Empty;
            }
        }
    }

    /// <summary>
    /// 存放数据字典对应值的字段名
    /// </summary>
    public string StoreParamValue
    {
        set
        {
            ViewState["StoreParamValue"] = value;
        }
        get
        {
            if (ViewState["StoreParamValue"] != null)
            {
                return ViewState["StoreParamValue"].ToString();
            }
            else
            {
                return string.Empty;
            }
        }
    }

    #endregion

    public event EventHandler<EventArgs> _afterUploadedEvent;
    /// <summary>
    /// 捕获ONCHANGE事件
    /// </summary>
    public event EventHandler<EventArgs> AfterUploadedEvent
    {
        add
        {
            _afterUploadedEvent += value;
        }
        remove
        {
            _afterUploadedEvent -= value;
        }
    }

    /// <summary>
    /// 页面加载
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(KeyIdValue))
            {
                FileUploadStruct _fileUploadStruct = new FileUploadStruct();
                _fileUploadStruct.KeyId = KeyId;
                _fileUploadStruct.KeyIdValue = KeyIdValue;
                _fileUploadStruct.TableName = TableName;
                _fileUploadStruct.FileStoreFieldName = FileStoreFieldName;
                _fileUploadStruct.FileTypeFieldName = FileTypeFieldName;

                object objFileType = uploadFileClass.GetStoreFileType(_fileUploadStruct);
                if (objFileType!=null)
                {
                    string strFileType = objFileType.ToString();

                    FileUpload1.Enabled = true;
                    btnFileUpload.Enabled = true;
                    
                    if (!string.IsNullOrEmpty(strFileType))
                    {
                        Session["UpFileCondition"] = _fileUploadStruct;
                        btnAppearResource.Visible = true;

                        string strScript = WindowAppear.GetOpen(Request.ApplicationPath+"/Common/AppearOtherResouce.aspx", "查看附件");
                        btnAppearResource.Attributes.Add("onclick", strScript);
                    }
                }
            }

            if (StoreResourceType.ToLower().CompareTo("insert") == 0)
            {
                FileUpload1.Enabled = true;
                btnFileUpload.Enabled = true;
            }
        }
    }

    /// <summary>
    /// 开始上传
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnFileUpload_Click(object sender, EventArgs e)
    {
        if (FileUpload1.HasFile)
        {
            bool bPromise = false;
            if (!string.IsNullOrEmpty(StoreResourceType))
            {
                if (StoreResourceType.ToLower().CompareTo("insert") == 0)
                {
                    bPromise = true;
                }
            }

            if (!string.IsNullOrEmpty(KeyIdValue) || bPromise)
            {
                //验证文件类型
                String strFileExtension = System.IO.Path.GetExtension(FileUpload1.FileName).ToLower();
                string strFileName = FileUpload1.FileName.Replace(strFileExtension, "");
                strFileExtension = strFileExtension.Replace(".", "");
                bool bOk =uploadFileClass.HaveValideFileExtension(strFileExtension);

                if (bOk)
                {
                    FileUploadStruct _fileUploadStruct = new FileUploadStruct();
                    _fileUploadStruct.KeyId = KeyId;
                    _fileUploadStruct.KeyIdValue = KeyIdValue;
                    _fileUploadStruct.TableName = TableName;
                    _fileUploadStruct.FileStoreFieldName = FileStoreFieldName;
                    _fileUploadStruct.FileTypeFieldName = FileTypeFieldName;

                    _fileUploadStruct.StoreParamFieldName = StoreParamFieldName;
                    _fileUploadStruct.StoreParamValue = StoreParamValue;

                    //开始上传                
                    _fileUploadStruct.FileTypeFieldValue = strFileExtension;
                    _fileUploadStruct.FileBytes = FileUpload1.FileBytes;

                    DataStoreType storeType = DataStoreType.Update;

                    _fileUploadStruct.StoreResourceFieldName = StoreResourceFieldName;
                    //设置资源名称
                    Control ctrlTemp = Page.FindControl(StoreResourceNameControlName);
                    if (ctrlTemp != null)
                    {
                        string strText = "";
                        TextBox txtCtrl = ctrlTemp as TextBox;
                        if (txtCtrl == null)
                        {
                            IUserControl tmpCtrl = ctrlTemp as IUserControl;

                            if (tmpCtrl != null)
                            {
                                strText = tmpCtrl.Text;
                            }
                        }
                        else
                        {
                            strText = txtCtrl.Text;
                        }

                        if (!string.IsNullOrEmpty(strText))
                        {
                            _fileUploadStruct.StoreResourceNameValue = strText;
                        }
                        else
                        {
                            _fileUploadStruct.StoreResourceNameValue = strFileName;
                        }
                    }

                    if (bPromise)
                    {
                        storeType = DataStoreType.Insert;

                        _fileUploadStruct.StoreKeyFieldName = StoreKeyFieldName;
                        _fileUploadStruct.StoreKeyValue = StoreKeyValue;
                        

                        Control ctrlParamTemp = Page.FindControl(StoreParamValue);
                        if (ctrlParamTemp != null)
                        {
                            string strText = "";
                            TextBox txtCtrl = ctrlParamTemp as TextBox;
                            if (txtCtrl == null)
                            {
                                IUserControl tmpCtrl = ctrlParamTemp as IUserControl;

                                if (tmpCtrl != null)
                                {
                                    strText = tmpCtrl.Value;
                                }
                            }
                            else
                            {
                                strText = txtCtrl.Text;
                            }
                            
                            _fileUploadStruct.StoreParamValue = strText;
                        }
                    }
                    
                    int nResult= uploadFileClass.StoreUploadFile(_fileUploadStruct, storeType);

                    if (nResult == 1)
                    {
                        WindowAppear.WriteAlert(this.Page, "文件上传成功！");

                        ViewState["NewId"] = uploadFileClass.NewId;

                        //上传成功后触发事件
                        if (_afterUploadedEvent != null)
                        {
                            _afterUploadedEvent(sender, e);
                        }
                    }
                }
                else
                {
                    WindowAppear.WriteAlert(this.Page, "对不起，系统不支持您选择上传的文件类型！");
                }
            }
            else
            {
                WindowAppear.WriteAlert(this.Page, "请设置上传文件的主键！");
            }
        }
        else
        {
            Common.WindowAppear.WriteAlert(this.Page, "请选择上传文件！");
        }
    }
}
