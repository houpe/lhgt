﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PersistenceControl.ascx.cs"
    Inherits="UserControls_PersistenceControl" %>
<%@ Register TagPrefix="uc2" TagName="Calendar" Src="~/UserControls/Calendar.ascx" %>
<%@ Register TagPrefix="uf3" TagName="GeneralSelect" Src="~/UserControls/GeneralSelect.ascx" %>
<asp:Button ID="btnSave" runat="server" Text="保存" OnClick="btnSave_Click" SkinID="LightGreen" />
<asp:Button ID="btnReturn" runat="server" Text="返回" OnClick="btnReturn_Click" CausesValidation="false"
    SkinID="LightGreen" />
<asp:HiddenField ID="hfKeyvalue" runat="server" />
<asp:HiddenField ID="hfStatus" runat="server" />
