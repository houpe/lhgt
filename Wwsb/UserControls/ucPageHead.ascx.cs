﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SbBusiness;
using Business.Admin;

public partial class UserControls_ucPageHead : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["LoginUserName"] != null)
            {
                btnReturn.Visible = true;

                labMsg.Text = string.Format("{0},欢迎您访问网上办事大厅！【当前日期:{1}&nbsp;{2}】",
                    Session["LoginUserName"], DateTime.Now.ToLongDateString(), ClsWorkDaySet.GetDayOfWeek());
            }
            else
            {
                labMsg.Text = string.Format("欢迎您访问网上办事大厅！【当前日期:{0}&nbsp;{1}】",
                    DateTime.Now.ToLongDateString(), ClsWorkDaySet.GetDayOfWeek());
            }

            if (!string.IsNullOrEmpty(Request["flowtype"]))
            {
                labMsg.Text += string.Format("【办理事项：{0}】", Server.UrlDecode(Request["flowtype"]));
            }
            else if ((Session["flowtype"] != null && Session["userid"] != null) && (string.IsNullOrEmpty(Request["flowtype"])))//如果流程别名不为空,时直接添加流程别名 addby zhongjian 20100112
            {
                labMsg.Text += string.Format("【办理事项：{0}】", Session["flowtype"]);
            }
        }
    }

    #region 返回登陆主界面
    /// <summary>
    /// Handles the Clicked event of the btnReturn control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void btnReturn_Clicked(object sender, EventArgs e)
    {
        string strRedirectPage = string.Empty;
        if (Session["MainPage"] != null)
        {
            strRedirectPage = Session["MainPage"].ToString();
        }
        else
        {
            strRedirectPage = Request.ApplicationPath + "/" + SystemConfig.LoginPage;
        }
        Session.RemoveAll();
        Response.Redirect(strRedirectPage);
    }
    #endregion
}
