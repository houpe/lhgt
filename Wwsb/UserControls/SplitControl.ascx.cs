﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.ComponentModel;

public partial class Controls_Com_Split_SplitControl : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SplitImg.Attributes.Add("onclick", string.Format("SplitIsHide('{0}','Content');",DivId));
    }

    private string _DivId = string.Empty;
    /// <summary>
    /// DivID
    /// </summary>
    [CategoryAttribute("Split设置"), DescriptionAttribute("需要控制显示隐藏Div"), ReadOnlyAttribute(false)]
    public string DivId
    {
        get
        {
            if (ViewState["DivId"] != null)
            {
                return ViewState["DivId"].ToString();
            }
            else
            {
                return "tdLeft";
            }
        }
        set 
        {
            ViewState["DivId"] = value;
        }
    }
}
