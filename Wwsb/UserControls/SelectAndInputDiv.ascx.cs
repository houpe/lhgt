﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Business.Admin;

public partial class UserControls_SelectAndInputDiv : System.Web.UI.UserControl
{
    #region 属性
    /// <summary>
    /// 在SYS_PARAMS里的Name
    /// </summary>
    public string Name
    {
        get { return ViewState["SelectAndInputDiv_Name"] as string; }
        set { ViewState["SelectAndInputDiv_Name"] = value; }
    }

    public DataTable dtSouse
    {
        get { return _dtSouse; }
        set { _dtSouse = value; }
    }

    /// <summary>
    /// 客户端id
    /// </summary>
    public override string ClientID
    {
        get
        {
            return ShowSelText.ClientID;
        }
    }

    /// <summary>
    /// 显示的值
    /// </summary>
    public string Text
    {
        get
        {
            if (ShowSelText.Text != null)
            {
                return ShowSelText.Text;
            }
            else
            {
                return string.Empty;
            }
        }
        set
        {
            ShowSelText.Text = value;
        }
    }

    /// <summary>
    /// 选择的值
    /// </summary>
    public string Value
    {
        get { return ShowSelText.Text; }
        set { ShowSelText.Text = value; }
    }

    /// <summary>
    /// 宽度
    /// </summary>
    public Unit Width
    {
        get { return ShowSelText.Width; }
        set { ShowSelText.Width = value; }
    }

    /// <summary>
    /// 只读
    /// </summary>
    public bool ReadOnly
    {
        get { return ShowSelText.Enabled; }
        set { ShowSelText.Enabled = value; }
    }
    public string strShowDiv = "";
    public string strFun = "";
    #endregion

    private DataTable _dtSouse;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Render();
        }
    }

    protected void Render()
    {
        ReadOnly = true;
        DataTable dtTable = DictOperation.GetSysParams(Name, "");
        _dtSouse = dtTable;
        strFun = ClientID.Replace("ShowSelText", "value");
        strShowDiv = string.Format("<div  id='{0}' style='width: 120px; height: 200px;overflow: auto;overflow-x: hidden;position: absolute;visibility:hidden '>", ClientID.Replace("ShowSelText", "value"));
        string strDivHead = string.Format("<div class='link_record0' onmouseover='hiLight(this)' onclick='CheckMe(this,{0})'>", ClientID);
        string strDivEnd = "</div>";
        if (_dtSouse.Rows.Count > 0)
        {
            for (int i = 0; i < _dtSouse.Rows.Count; i++)
            {
                strShowDiv += strDivHead;
                strShowDiv += _dtSouse.Rows[i]["keyvalue"].ToString();
                strShowDiv += strDivEnd;
            }
        }
        strShowDiv += "</div>";
    }
}
