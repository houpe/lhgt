﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UserDefineQuery.ascx.cs"
    Inherits="UserControls_UserDefineQuery" %>
    
    <script type="text/javascript">
    function AddCondition()
    {
        var queryField = document.getElementById("<%=ddlFieldQuery.ClientID%>").value;
        var operation = document.getElementById("<%=ddlOperation.ClientID%>").value;
        var queryFieldValue =document.getElementById("<%=txtFieldValue.ClientID%>").value;
        
        var ctrl = document.getElementById("<%=txtSelectedCondition.ClientID%>");
        
        if( queryFieldValue!="")
        {
            if( ctrl.value != "")
            {
                ctrl.value += " and ";
            }
            
            if(operation=="like")
            {
                ctrl.value += queryField+" "+operation+" '%"+queryFieldValue+"%'";
            }
            else
            {
                ctrl.value += queryField+operation+"'"+queryFieldValue+"'";
            }                
        }
    }     
   
    </script>
    
<div align="center">
    <table border="0" cellpadding="0" cellspacing="0" width="300">
        <tr>
            <td class="txtrighttd" style="height: 24px;" width="100">
                <asp:Label ID="lblProposer" runat="server" Text="查询字段：" Width="84px"></asp:Label></td>
            <td class="txtlefttd">
                <asp:DropDownList ID="ddlFieldQuery" SkinID="DropDownList" runat="server" Width="80px">
                </asp:DropDownList></td>
            <td class="txtlefttd">
                <asp:DropDownList ID="ddlOperation" SkinID="DropDownList" runat="server">
                        <asp:ListItem>=</asp:ListItem>
                        <asp:ListItem>&gt;</asp:ListItem>
                        <asp:ListItem>&gt;=</asp:ListItem>
                        <asp:ListItem>&lt;</asp:ListItem>
                        <asp:ListItem>&lt;=</asp:ListItem>
                        <asp:ListItem>like</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="txtlefttd">
                <asp:TextBox ID="txtFieldValue" runat="server" SkinID="InputGreen"></asp:TextBox>
            </td>
            <td class="txtlefttd">
                <input type="button" id="btnSetCondition" value="确定" onclick="AddCondition();" />
            </td>
        </tr>
        <tr>
            <td class="txtrighttd">
            </td>
            <td class="txtlefttd" colspan="4">
                <asp:TextBox ID="txtSelectedCondition" runat="server" Height="98px" Width="300px"
                    TextMode="MultiLine"></asp:TextBox></td>
        </tr>       
        <tr>
            <td class="txtcentertd" colspan="5" style="text-align: center">
                
                <asp:Button ID="btnClick" runat="server" OnClick="btnClick_Click" SkinID="LightGreen"
                    Text="清空" />
            </td>
        </tr>
    </table>
</div>
