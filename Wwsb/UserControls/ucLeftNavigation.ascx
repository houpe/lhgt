﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucLeftNavigation.ascx.cs"
    Inherits="UserControls_ucLeftNavigation" %>

<script type="text/javascript">
function ControlDivVisible(num)
{
    var ctrlname="ul"+num;
    var ctrl = document.getElementById(ctrlname);
    
    if( ctrl.style.display == 'none')
    { 
        ctrl.style.display = 'block';    
        document.getElementById("div"+num).className= "left_bar";
    }
    else    
    {
        ctrl.style.display = 'none';               
        document.getElementById("div"+num).className= "left_bar";
    }
 
    //存放cookie
    //setCookie(ctrlname,ctrl.style.display);
}

function mouseMove(i,j)
{
    var myli=document.getElementById('menu'+i+j);
    myli.className="left_menu_ul_li1";
}
function mouseOut(i,j)
{	
    var myli=document.getElementById('menu'+i+j);
    myli.className="left_menu_ul_li"
}

function GetCookieValue()
{	
   var inputList = document.body.getElementsByTagName("ul");
   var nFlag = 0;//判断是否有cookie
   for(var i=0;i<inputList.length;i++)
   {
        var cookieValue = getCookie(inputList[i].id);     

        if( cookieValue != null)
        {   
            inputList[i].style.display=cookieValue;
            nFlag = 1;
        }
   }
   
   if(nFlag==0)
   {
       inputList[i].style.display=cookieValue;
   }
}

</script>
<asp:HiddenField ID="hdKey" runat="server" />
<%=Session["Menu"]%>