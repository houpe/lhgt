﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GeneralSelect.ascx.cs"
    Inherits="UserControls_GeneralSelect" %>
<asp:DropDownList ID="ddlControl" runat="server">
</asp:DropDownList>
<asp:TextBox ID="ShowSelText" runat="server" Visible="false"></asp:TextBox>
<asp:RequiredFieldValidator ID="rfvControl" runat="server" ErrorMessage="" Text="*"
    ControlToValidate="ddlControl" Display="Dynamic"></asp:RequiredFieldValidator>