﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Business.FlowOperation;

public partial class UserControls_ucDrawFlow : System.Web.UI.UserControl
{
    public string strHtml = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            AppearFlowData();
        }
    }

    public string LoginUserId
    {
        get
        {
            if (ViewState["loginUserId"] != null)
            {
                return ViewState["loginUserId"].ToString();
            }
            return string.Empty;
        }
        set
        {
            ViewState["loginUserId"] = value;
        }        
    }

    public string GetStringWithCode(List<string> lstParams)
    {
        string strReturn = string.Empty;
        for (int i = 0; i < lstParams.Count; i++)
        {
            string strOneTemp = lstParams[i];
            if (i >0)
            {
                strReturn += ",";
            }
            strReturn += string.Format("'{0}'",strOneTemp);
        }
        return strReturn;
    }

    /// <summary>
    /// 显示流程数据
    /// </summary>
    public void AppearFlowData()
    {
        RequestFlowOperation requestFlow = new RequestFlowOperation();
        DataTable dtSteps = requestFlow.GetRequestFlow("");
        string strCurrentStepNo = requestFlow.GetStepNoByUser(LoginUserId);

        List<string> lstAllStep = new List<string>();
        List<string> lstStepMsg = new List<string>();
        foreach (DataRow drTemp in dtSteps.Rows)
        {
            lstAllStep.Add(drTemp["step_name"].ToString());
            lstStepMsg.Add(drTemp["step_msg"].ToString());
        }

        string strAllStep = GetStringWithCode(lstAllStep);
        string strStepMsg = GetStringWithCode(lstStepMsg);

        strStepMsg = string.Format(strStepMsg,Session["LoginUserName"],"地图审核");

        //此段代码要放于画板所在div的后边
        //var textArr = ["注 册", "申请业务权限", "在线申报", "提交申请", "业务受理", "业务审批", "审批通过"];  //存储岗位名
        //var stepPassArr = ["注 册", "申请业务权限", "在线申报", "业务审批"];  //存储岗位名
        //DrawFlow("#DrawPanel", 100, 10, textArr, stepPassArr, "red");
        strHtml += string.Format(@"var textArr = [{0}];var stepMsgArr = [{2}]; 
            DrawFlow('#DrawPanel', 20, 10, textArr,stepMsgArr, {1}, 'red',100,1);",
           strAllStep, strCurrentStepNo, strStepMsg);
    }
}
