﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Controls_Com_WebPart_DisplayMode : System.Web.UI.UserControl
{
    WebPartManager _manager;

    protected void Page_Init(object sender, EventArgs e)
    {
        this.Page.InitComplete += new EventHandler(Page_InitComplete);
    }

    void Page_InitComplete(object sender, EventArgs e)
    {
        //添加列表项
        _manager = WebPartManager.GetCurrentWebPartManager(this.Page);
        
        foreach (WebPartDisplayMode mode in _manager.SupportedDisplayModes)
        {
            string value = mode.Name;
            string text = string.Empty;

            if (mode.IsEnabled(_manager))
            {
                if (mode == WebPartManager.BrowseDisplayMode)
                {
                    text = "浏览";
                }
                else if (mode == WebPartManager.DesignDisplayMode)
                {
                    text = "设计";
                }
                else if (mode == WebPartManager.EditDisplayMode)
                {
                    text = "编辑";
                }
                else if (mode == WebPartManager.CatalogDisplayMode)
                {
                    text = "目录";
                }
                else
                {
                    continue;
                }
                ListItem item = new ListItem(text, value);
                ModeList.Items.Add(item);
            }
        }
    }

    protected void ModeList_SelectedIndexChanged(object sender, EventArgs e)
    {
        //切换选择模式
        string selectedMode = ModeList.SelectedValue;
        WebPartDisplayMode mode = _manager.SupportedDisplayModes[selectedMode];
        if (mode != null)
        {
            _manager.DisplayMode = mode;
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        //保证显示模式与选择状态一致
        ListItemCollection items = ModeList.Items;
        int selectedIndex = items.IndexOf(items.FindByValue(_manager.DisplayMode.Name));
        ModeList.SelectedIndex = selectedIndex;
    }

    protected void ResetButton_Click(object sender, EventArgs e)
    {
        //重置页面
        _manager.Personalization.ResetPersonalizationState();
    }
}
