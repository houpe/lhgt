﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DisplayMode.ascx.cs" Inherits="Controls_Com_WebPart_DisplayMode" %>
<ul class="search_right_style">
    <li class="search_right_ico"></li>
    <li>
        <asp:Literal ID="Title" runat="server" meta:ResourceKey="Title" /><asp:DropDownList
            ID="ModeList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ModeList_SelectedIndexChanged" />
        <asp:LinkButton ID="ResetButton" runat="server" meta:ResourceKey="ResetButton" OnClick="ResetButton_Click" />
    </li>
</ul>
