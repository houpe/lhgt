﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ASP;

using WebControls;

/// <!--
/// 功能描述  : 根据GridView更新子表数据
/// 创建人  : cd
/// 创建时间: 2007-07-12
/// -->
public partial class UserControls_UpdateGridView : System.Web.UI.UserControl
{
    #region 属性监控
    /// <summary>
    /// 要操作的GridView控件的ID
    /// </summary>
    public string GridViewName
    {
        get
        {
            return ViewState["GridViewName"] as string;
        }
        set
        {
            ViewState["GridViewName"] = value;
        }
    }

    /// <summary>
    /// 放GridView的容器
    /// </summary>
    public Control container;
    private Control _container
    {
        get
        {
            if (container == null)
            {
                return Page.Master.FindControl("ContentPlaceHolderContent");
            }
            return container;
        }
    }

    /// <summary>
    /// 在页面中找到GridView
    /// </summary>
    private GridView gridview
    {
        get
        {
            GridView gv = _container.FindControl(GridViewName) as GridView;
            return gv;
        }
    }

    /// <summary>
    ///  对应数据库的表名
    /// </summary>
    public string TableName
    {
        get
        {
            return ViewState["TableName"] as string;
        }
        set
        {
            ViewState["TableName"] = value;
        }
    }

    /// <summary>
    /// 与主表关联的字段名
    /// </summary>
    public string RelationName
    {
        get
        {
            return ViewState["RelationName"] as string;
        }
        set
        {
            ViewState["RelationName"] = value;
        }
    }

    /// <summary>
    /// 与主表关联的字段值
    /// </summary>
    public string RelationValue
    {
        get
        {
            return ViewState["RelationValue"] as string;
        }
        set
        {
            ViewState["RelationValue"] = value;
        }
    }

    /// <summary>
    /// 子表的主键名
    /// </summary>
    public string KeyName
    {
        get
        {
            return ViewState["KeyName"] as string;
        }
        set
        {
            ViewState["KeyName"] = value;
        }
    }

    /// <summary>
    /// 子表的必填列，默认为第0列
    /// </summary>
    public int MustFillCol
    {
        get
        {
            if (ViewState["MustFillCol"] == null)
            {
                return 0;
            }
            return int.Parse(ViewState["MustFillCol"].ToString());
        }
        set
        {
            ViewState["MustFillCol"] = value;
        }
    }

    /// <summary>
    /// 其他条件
    /// </summary>
    public string OtherCondition
    {
        get
        {
            return ViewState["OtherCondition"] == null ? string.Empty : ViewState["OtherCondition"].ToString();
        }
        set
        {
            ViewState["OtherCondition"] = value;
        }
    }

    /// <summary>
    /// 业务编号（内网新增子表记录时使用）
    /// </summary>
    public string IID
    {
        get
        {
            return ViewState["iid"] == null ? string.Empty : ViewState["iid"].ToString();
        }
        set
        {
            ViewState["iid"] = value;
        }
    } 
    #endregion

    #region 更新子表
    /// <summary>
    /// 更新子表
    /// </summary>
    /// /// <!--
    /// 创建人  : cd
    /// 创建时间: 2007-07-12
    /// -->
    public void Update()
    {
        Dictionary<string, string> dictionary = new Dictionary<string, string>();
        foreach (GridViewRow gvr in gridview.Rows)
        {
            bool IsContinue = false;
            dictionary.Add(RelationName, RelationValue);
            dictionary.Add(KeyName, gridview.DataKeys[gvr.RowIndex].Value.ToString());
            for (int i = 0; i < gvr.Cells.Count; i++)
            {
                if (!gvr.Cells[i].HasControls())  //不是模版列，跳过
                {
                    continue;
                }
                else
                {
                    try
                    {
                        string strTemp = gvr.Cells[i].Controls[1].ID;
                        string strKey = strTemp.Replace("txt", "");
                        strKey = strKey.Replace("ddl", "");

                        string strValue = string.Empty;
                        if (gvr.Cells[i].Controls[1] is UserControl)
                        {
                            Control ctrlTemp = gvr.Cells[i].Controls[1];
                            if (ctrlTemp is Controls_Com_Calendar)
                            {
                                Controls_Com_Calendar calendar = (Controls_Com_Calendar)ctrlTemp;
                                if (calendar.Text != string.Empty)
                                {
                                    DateTime date = Convert.ToDateTime(calendar.Text);
                                    calendar.Text = date.ToShortDateString();
                                    strValue = "to_date('" + calendar.Text + "','yyyy-mm-dd')";
                                }
                            }
                            else if (ctrlTemp is UserControls_TextBoxWithValidator)
                            {
                                strValue = ((UserControls_TextBoxWithValidator)ctrlTemp).Text;
                            }
                            else if (ctrlTemp is IUserControl)
                            {
                                strValue = ((IUserControl)ctrlTemp).Value;
                            }
                        }
                        else if (gvr.Cells[i].Controls[1].GetType().Name == "TextBox")
                        {
                            strValue = ((TextBox)gvr.Cells[i].Controls[1]).Text;
                        }
                        else
                        {
                            continue;
                        }
                        if (i == MustFillCol && string.IsNullOrEmpty(strValue))//必填列为空
                        {
                            dictionary.Clear();
                            IsContinue = true;
                            break;
                        }
                        dictionary.Add(strKey, strValue);
                    }
                    catch
                    {
                        throw;
                    }
                }
            }
            if (!IsContinue)
            {
                //EvaluateGrade.UpdateChildTable(TableName, KeyName, dictionary, OtherCondition, IID);
                dictionary.Clear();
            }
        }
    } 
    #endregion
}
