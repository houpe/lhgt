﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MutiFilesUpload.aspx.cs"
    Inherits="UserControls_MutiFilesUpload" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>多文件上传</title>
    <script src="../ScriptFile/jquery.js" type="text/javascript"></script>
    <script src="../ScriptFile/uploadify/jquery.uploadify.min.js" type="text/javascript"></script>
</head>
<script type="text/javascript">
    $(document).ready(function () {
        $("#uploadify").uploadify({
            'swf': '../ScriptFile/uploadify/uploadify.swf',
            'uploader': 'MutiUploadHandler.ashx',
            'formData': { 'iid': '<%=iid%>', 'moudleid': '<%=moudleid%>' },
            'cancelImg': '../images/uploadify-cancel.png',
            'folder': 'UploadFile',
            'sizeLimit': 10737418240,       //10G
            'queueID': 'fileQueue',
            'buttonText': '浏览',
            'height': '21',
            'width': '65',
            'auto': true, //是否自动上传
            'multi': true,
            onUploadStart: function (file) {
                $("#uploadify").uploadify("settings", "formData", { "ml": "test" });
            },
            //返回一个错误，选择文件的时候触发
            onSelectError: function (file, errorCode, errorMsg) {
                switch (errorCode) {
                    case -100:
                        alert("上传的文件数量已经超出系统限制的" + $('#file_upload').uploadify('settings', 'queueSizeLimit') + "个文件！");
                        break;
                    case -110:
                        alert("文件 [" + file.name + "] 大小超出系统限制的" + $('#file_upload').uploadify('settings', 'fileSizeLimit') + "大小！");
                        break;
                    case -120:
                        alert("文件 [" + file.name + "] 大小异常！");
                        break;
                    case -130:
                        alert("文件 [" + file.name + "] 类型不正确！");
                        break;
                }
            },
            //检测FLASH失败调用
            onFallback: function () {
                alert("您未安装FLASH控件，无法上传图片！请安装FLASH控件后再试。");
            },
            onUploadError: function (file, errorCode, errorMsg, errorString) {
                alert("上传失败：" + errorString);
            },
            //onUploadSuccess: function (file, data, response) {//上传完成时触发（每个文件触发一次）
            //    alert("上传成功");
            //    refreshParent();
            //},
            onQueueComplete: function (queueData) {//当队列中的所有文件全部完成上传时触发
                alert('上传成功文件数：' + queueData.uploadsSuccessful + '\n  上传失败数：' + queueData.uploadsErrored);
                refreshParent();
            }
        });
    });

    function fileUpload() {
        $('#uploadify').uploadify('upload', '*');
    }

    function cancelFileUpload() {
        $('#uploadify').uploadify('cancel', '*');
    }

    function refreshParent() {
        var strTemp = window.parent.location.href;
        if (strTemp.indexOf('rdm=') > 0) {
            strTemp = strTemp.substr(0, strTemp.indexOf('rdm='));
            strTemp += 'rdm=' + Math.random() * 100;
        }
        else {
            strTemp += '&rdm=' + Math.random() * 100;
        }
        window.parent.location.href = strTemp;
    }

    function refreshParentSrc() {
        if (window.parent.parent.document.getElementById("tdSccl") != null) {
            window.parent.parent.document.getElementById("tdSccl").click();
        }
        else {
            refreshParent();
        }
    }
</script>
<body>
    
        <div style="width: 100%; text-align:center;">
            <input type="file" name="uploadify" id="uploadify" />
            <input type="button" id="btnUpload" class="button" onclick="fileUpload()" runat="server" value="上传" visible="false" />
            <input type="button" id="btnCancel" class="button" onclick="cancelFileUpload()" value="取消上传" style=" display:none" />
        </div>
        <div id="fileQueue" class="fileUploadList" style="border:1px solid; height:100px;">
        上传文件区域
        </div>
    
</body>
</html>
