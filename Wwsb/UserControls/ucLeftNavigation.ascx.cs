﻿
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SbBusiness;
using SbBusiness.Wsbs;
using SbBusiness.Menu;

/// <!--
/// 功能描述  : 菜单导航控件
/// 创建人  : LinJian
/// 创建时间: 2006-09-04
/// -->
public partial class UserControls_ucLeftNavigation : System.Web.UI.UserControl
{
    private ClsMenuOperation clsMenu = new ClsMenuOperation();
    private SerialInstance cInstance = new SerialInstance();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            GenerateBodyScript();
        }
    }

    #region 地理信息平台入口
    /// <summary>
    /// 地理信息中心接口
    /// </summary>
    /// <param name="strUrl">接口连接地址(url)</param>
    /// <returns></returns>
    public string GetDXInterfaceUrl(string strInterfaceUrl)
    {
        string strUrl = string.Empty;
        string UserID = string.Empty;
        string strUserPWD = string.Empty;
        if (Session["UserID"] != null)
            UserID = Session["UserID"].ToString();
        strUserPWD = cInstance.GetUserPWD(UserID);
        UserID = HttpUtility.UrlEncode(UserID);//在地信中心传入USERID需加密

        strUrl = string.Format("{2}?userId={0}&userPwd={1}", UserID, strUserPWD, strInterfaceUrl);
        return strUrl;
    }
    #endregion

    public void GenerateBodyScript()
    {
        //Session["Menu"] = null;
        if (Session["Menu"] == null)
        {
            if (Session["UserID"] != null)
            {
                DataTable dtParentMenu = clsMenu.GenerateParentMenu(Session["UserID"].ToString());
                string strBodyScript = @"<table>";

                //设置初始显示样式(菜单比较多时，例如管理员的菜单就较多，只显示一个栏目的菜单，
                //因为目前采用cookie的寄存方式存在问题，所以暂时屏蔽此功能)
                string strInitVisible = string.Empty;

                for (int i = 1; i <= dtParentMenu.Rows.Count; i++)
                {
                    DataRow drParentItem = dtParentMenu.Rows[i - 1];

                    strBodyScript += string.Format(@"<tr><td><div class=""left_box"" style=""text-align: left;width:150px;margin-top: 8px;"">
                        <div class=""left_box_bar"" style=""cursor:pointer;"" id=""div{0}"" onclick=""ControlDivVisible('{0}')"">{1}</div>
                        <ul class=""left_ul"" id=""ul{0}"" {2}> ", i, drParentItem["menu_title"], strInitVisible);
                    //获取子菜单
                    DataTable dtChildMenu = new DataTable();
                    if (string.IsNullOrEmpty(Request["flowname"]))
                    {
                        dtChildMenu = clsMenu.GenerateChildMenu(Session["UserID"].ToString(), drParentItem["ID"].ToString());
                    }
                    else
                    {
                        dtChildMenu = clsMenu.GetChildMenus(drParentItem["menu_title"].ToString(), Request["flowname"]);
                    }

                    for (int j = 1; j <= dtChildMenu.Rows.Count; j++)
                    {
                        DataRow drChildItem = dtChildMenu.Rows[j - 1];
                        string Url = drChildItem["menu_url"].ToString().Replace("~",
                            HttpContext.Current.Request.ApplicationPath);

                        //begin 将子菜单添加流程名称参数,使其与主页面连接保持一致.
                        if (Session["flowname"] != null)
                        {
                            Url += string.Format("&flowname={0}", Server.UrlEncode(Session["flowname"].ToString()));
                        }
                        else if (Request["flowname"] != null)
                        {
                            Url += string.Format("&flowname={0}", Server.UrlEncode(Request["flowname"]));
                        }
                        //end

                        if (Session["flowtype"] != null)//添加流程别名参数
                        {
                            string strFlowType = Session["flowtype"].ToString();
                            Url += string.Format("&flowtype={0}", Server.UrlEncode(strFlowType));
                            //==begin==当启用接口地址时,在线申报连接地信中心系统
                            if (drChildItem["menu_title"].ToString() == "在线申报")
                            {
                                DataTable dtTemp = clsMenu.GetWorkflowDefine(Session["flowtype"].ToString());
                                if (dtTemp.Rows.Count > 0)
                                {
                                    string strInterfaceType = dtTemp.Rows[0]["INTERFACETYPE"].ToString();
                                    string strInterfaceUrl = dtTemp.Rows[0]["INTERFACEURL"].ToString();
                                    if (!string.IsNullOrEmpty(strInterfaceUrl) && strInterfaceType == "1")
                                        Url = GetDXInterfaceUrl(strInterfaceUrl);
                                }
                            }
                            //==end
                        }
                        
                        strBodyScript += string.Format(@"<li id=""menu{1}{2}"">
                            <img src=" + HttpContext.Current.Request.ApplicationPath + "/App_Themes/SkinFile/Images/ico-01.gif" + @" />
                            <a href=""{0}"" id='{1}{2}'>{3}</a></li>", Url,
                            i, j, drChildItem["menu_title"]);
                    }
                    strBodyScript += @"</ul></div></td></tr>";
                }
                strBodyScript += @"</table>";

                if (dtParentMenu.Rows.Count > 0)
                {
                    Session["Menu"] = strBodyScript;
                }
            }
        }
    }
}
