﻿ 
// 创建人  ：Wu Hansi
// 创建时间：2007年7月11日
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using SbBusiness;

using WebControls;
using Business.Admin;

/// <!--
/// 功能描述  : 通用的下拉框，从SYS_PARAMS表中检索名-值对
/// 创建人  : Wu Hansi
/// 创建时间: 2007年7月11日
/// -->
public partial class UserControls_GeneralSelect : System.Web.UI.UserControl, IUserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DataBind();
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        
    }

    #region 属性
    /// <summary>
    /// 在SYS_PARAMS里的Name
    /// </summary>
    public string Name
    {
        get { return ViewState["GeneralSelect_Name"] as string; }
        set { ViewState["GeneralSelect_Name"] = value; }
    }

    /// <summary>
    /// 客户端id
    /// </summary>
    public override string ClientID
    {
        get
        {
            return ddlControl.ClientID;
        }
    }

    /// <summary>
    /// 是否验证为空
    /// </summary>
    public bool ValidateEmpty
    {
        get { return rfvControl.Enabled; }
        set
        {
            rfvControl.Enabled = value;
            rfvControl.Visible = value;
        }
    }

    /// <summary>
    /// 客户端onchange事件
    /// </summary>
    public string ClientOnChangeScript
    {
        get { return ViewState["ClientOnChangeScript"] as string; }
        set 
        { 
            ViewState["ClientOnChangeScript"] = value;
        }
    }

    /// <summary>
    /// 验证为空失败显示的文本
    /// </summary>
    public string ErrorMessage
    {
        get { return rfvControl.ErrorMessage; }
        set { rfvControl.ErrorMessage = value; }
    }

    /// <summary>
    /// 选择的值
    /// </summary>
    public string SelectedValue
    {
        get
        {
            return ddlControl.SelectedValue;
        }
        set
        {
            ddlControl.SelectedValue = value;
        }
    }

    /// <summary>
    /// 选择的值
    /// </summary>
    public string Value
    {
        get { return ddlControl.SelectedValue; }
        set { ddlControl.SelectedValue = value; }
    }

    /// <summary>
    /// 显示的值
    /// </summary>
    public string Text
    {
        get
        {
            if (ddlControl.SelectedItem != null)
            {
                return ddlControl.SelectedItem.Text;
            }
            else
            {
                return string.Empty;
            }
        }
        set
        {
            ddlControl.Text = value;
        }
    }

    /// <summary>
    /// 是否只读
    /// </summary>
    public bool ReadOnly
    {
        get
        {
            return !ddlControl.Enabled;
        }
        set
        {
            ddlControl.Enabled = !value;
            if (value)
            {
                if (ddlControl.SelectedItem != null)
                {
                    ShowSelText.Visible = true;
                    ShowSelText.ReadOnly = true;
                    ShowSelText.Text = ddlControl.SelectedItem.Text;
                    ShowSelText.Width = Unit.Parse((ShowSelText.Text.Length * 20).ToString() + "px");
                }
                ddlControl.Style["display"] = "none";
            }
            else
            {
                ShowSelText.Visible = false;
                ddlControl.Style["display"] = "";
            }
        }
    }

    public Unit Width
    {
        get { return ddlControl.Width; }
        set { ddlControl.Width = value; }
    }

    public bool FirstEmpty
    {
        get
        {
            if (ViewState["General_Select_FirstEmpty"] == null)
            {
                return false;
            }
            else
            {
                return (bool)ViewState["General_Select_FirstEmpty"];
            }
        }
        set { ViewState["General_Select_FirstEmpty"] = value; }
    }

    public bool TextOnly
    {
        get
        {
            if (ViewState["General_Select_TextOnly"] == null)
            {
                return false;
            }
            else
            {
                return (bool)ViewState["General_Select_TextOnly"];
            }
        }
        set { ViewState["General_Select_TextOnly"] = value; }
    }

    /// <summary>
    /// 设置默认值(add by lj)
    /// </summary>
    public string DefaultValue
    {
        get
        {
            string strReturn = string.Empty;

            if (ViewState["DefaultValue"] != null)
            {
                strReturn = ViewState["DefaultValue"].ToString();
            }

            return strReturn;
        }
        set
        {
            ViewState["DefaultValue"] = value;

            if (!string.IsNullOrEmpty(value))
            {
                int i = 0;
                foreach (ListItem lstItem in ddlControl.Items)
                {
                    if (lstItem.Value == value)
                    {
                        ddlControl.SelectedIndex = i;
                        break;
                    }
                    i++;
                }
            }
        }
    }
    
    public string DefaultText
    {
        get
        {
            string strReturn = string.Empty;

            if (ViewState["DefaultText"] != null)
            {
                strReturn = ViewState["DefaultText"].ToString();
            }

            return strReturn;
        }
        set
        {
            ViewState["DefaultText"] = value;

            if (!string.IsNullOrEmpty(value))
            {
                int i = 0;
                foreach (ListItem lstItem in ddlControl.Items)
                {
                    if (lstItem.Text == value)
                    {
                        ddlControl.SelectedIndex = i;
                        break;
                    }
                    i++;
                }
            }
        }
    }

    public string ValidationGroup
    {
        get { return rfvControl.ValidationGroup; }
        set
        {
            rfvControl.ValidationGroup = value;
        }
    }
    #endregion

    /// <!--
    /// 创建人  : Wu Hansi
    /// 创建时间: 2007年7月11日
    /// 修改人  : Wu Hansi
    /// 修改时间: 2007-07-11
    /// 修改描述: 重写了DataBind()，这个不需要手动的调用，在OnInit()函数里就调用好了，抢在父页面的Page_Load()前执行绑定
    /// -->
    public override void  DataBind()
    {
        ddlControl.Items.Clear();
        if (FirstEmpty)
        {
            ddlControl.Items.Add("");
        }
        dataBinding();

        if (!string.IsNullOrEmpty(ClientOnChangeScript))
        {
            ddlControl.Attributes.Add("onchange", ClientOnChangeScript);
        }
    }

    private string javascript 
    {
        get
        {
            return string.Format(@"
                    var ctrl=document.getElementById('{0}');
                    var selectValue = ctrl.options[ctrl.selectedIndex].text;
                    document.getElementById('{1}').title=selectValue;", ddlControl.ClientID, ValidatorName);
        }
    }

    #region 验证控件名
    /// <summary>
    /// 验证控件名
    /// </summary>
    public string ValidatorName
    {
        get
        {
            return ViewState["ValidatorName"].ToString();
        }
        set
        {
            ViewState["ValidatorName"] = value;
            if (!string.IsNullOrEmpty(value))
            {
                ddlControl.Attributes.Add("onchange", javascript);
                ddlClientID = ddlControl.ClientID;
            }
        }
    }
    #endregion

    #region 下拉框的客户端名
    /// <summary>
    /// 下拉框的客户端名
    /// </summary>
    public string ddlClientID
    {
        get
        {
            if (ViewState["ddlClientID"] != null)
            {
                return ViewState["ddlClientID"].ToString();
            }
            else
            {
                return this.ClientID;
            }
        }
        set
        {
            ViewState["ddlClientID"] = value;
        }
    }
    #endregion

    #region 给ddlControl绑定查询条件
    /// <summary>
    /// 给ddlControl绑定查询条件
    /// </summary>
    /// <param name="strWyFlag"></param>
    public void dataBinding()
    {
        DataTable dtTable = DictOperation.GetSysParams(Name, "");
      
        foreach (DataRow row in dtTable.Rows)
        {
            ListItem lstItem = new ListItem();
            lstItem.Text = row["KEYVALUE"].ToString();
            if (TextOnly)
            {
                lstItem.Value = lstItem.Text;
            }
            else
            {
                lstItem.Value = row["KEYCODE"].ToString();
            }
            ddlControl.Items.Add(lstItem);
        }

        //再次设置默认值
        if (!string.IsNullOrEmpty(DefaultValue))
        {
            int i = 0;
            foreach (ListItem lstItem in ddlControl.Items)
            {
                if (lstItem.Value == DefaultValue)
                {
                    ddlControl.SelectedIndex = i;
                    break;
                }
                i++;
            }
        }
        //再次设置默认值DefaultText
        if (!string.IsNullOrEmpty(DefaultText))
        {
            int i = 0;
            foreach (ListItem lstItem in ddlControl.Items)
            {
                if (lstItem.Text == DefaultText)
                {
                    ddlControl.SelectedIndex = i;
                    break;
                }
                i++;
            }
        }
    }
    #endregion

    /// <summary>
    /// 删除特定文本的选择项
    /// </summary>
    /// <param name="strText"></param>
    public void DeleteSpecialItem(string strText)
    {
        if (ddlControl.Items.Count > 0)
        {
            foreach (ListItem lstItem in ddlControl.Items)
            {
                if (lstItem.Text == strText)
                {
                    ddlControl.Items.Remove(lstItem);
                    break;
                }
            }
        }
    }
}
