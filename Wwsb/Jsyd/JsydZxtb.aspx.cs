﻿using SbBusiness.Wsbs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Jsyd_JsydZxtb : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if( !IsPostBack)
        {
            InitData();
        }
    }

    public void InitData()
    {
        string strUserId = string.Empty;
        if (Session["UserId"] != null)
        {
            strUserId = Session["UserId"].ToString();
        }

        //设置待办件的地址
        string strMsg = "<script>";

        strMsg += string.Format(" var strUserId ='{0}';", strUserId);
        //定义定时获取待办事件
        strMsg += "function GetNewestDkxx(){WebService(Common.GetServicePath() + \"WebService/SbxtService.asmx/GetNewestDkxx\", AppearNewestDkxx, \"{'strWhere':'','strUserId':'" + strUserId + "'}\");}";

        //脚本结束信息
        strMsg += "</script>";
        Response.Write(strMsg);
    }

}