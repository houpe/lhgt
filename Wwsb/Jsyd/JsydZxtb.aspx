﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="JsydZxtb.aspx.cs" Inherits="Jsyd_JsydZxtb" %>

<%@ Register Src="~/UserControls/ucPageHead.ascx" TagName="ucPageHead" TagPrefix="uc4" %>
<%@ Register Src="~/UserControls/ucPageFoot.ascx" TagName="ucPageFoot" TagPrefix="uc3" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>建设用地在线投标</title>
    <link href="../App_Themes/SkinFile/CSS/easyui.css" rel="stylesheet" />
    <script type="text/javascript" src="../ScriptFile/jquery.js"></script>
    <script src="../ScriptFile/jquery.easyui.min.js"></script>
    <script src="../ScriptFile/Common/Jquery.Common.js"></script>
    <script type="text/javascript" src="../ScriptFile/Common/Jquery.Global.js"></script>

    <script type="text/javascript">
        //显示最新的投标信息（横向列表）
        function AppearNewestDkxx_Tab(result) {
            if (result != "") {
                json = result.toJson(); //格式化

                var table = $("<table style='width:100%;color: dodgerblue; border-color: #4fbff9; font-size: larger; font-weight: bold' border='1' cellspacing='0' bordercolorlight='#5eb4e' bordercolordark='#eaf0ff' cellpadding='1'></table>");
                for (var i = 0; i < json.length; i++) {
                    o1 = json[i];
                    var row = $("<tr></tr>");
                    if (i == 0) {//添加标题头
                        row = $("<tr style='background-color: #dcedf6'\"></tr>");
                        for (key in o1) {
                            if (key != "iid") {
                                var td = $("<td></td>");
                                if (key == "dk_bh") {
                                    td.text("地块编号");
                                }
                                else if (key == "dk_zl") {
                                    td.text("土地坐落");
                                }
                                else if (key == "dk_tdmj") {
                                    td.text("土地面积");
                                }
                                else if (key == "dk_xmnr") {
                                    td.text("项目内容");
                                }
                                else if (key == "dk_tdyt") {
                                    td.text("土地用途");
                                }
                                else if (key == "zxbj") {
                                    td.text("最新报价");
                                }
                                td.appendTo(row);
                            }
                        }
                        row.appendTo(table);

                        //第一行设定标题头后需重置行标记
                        row = $("<tr></tr>");
                    }


                    for (key in o1) {//添加内容
                        if (key != "iid") {
                            var td = $("<td></td>");
                            td.text(o1[key].toString());
                            td.appendTo(row);
                        }
                    }
                    row.appendTo(table);
                }

                $("#divCrMsg").html("");
                table.appendTo($("#divCrMsg"));
            }
        }

        //显示最新的投标信息(竖向列表)
        function AppearNewestDkxx(result) {
            if (result != "") {
                json = result.toJson(); //格式化
                $("#divCrMsg").html("");

                for (var i = 0; i < json.length; i++) {
                    o1 = json[i];

                    var divTemp = $("<div style='width:320px;height:190px;float:left; margin-left:5px; '></div>");
                    var table = $("<table cellspacing='0' cellpadding='1' class='jsyd_jb'></table>");

                    for (key in o1) {
                        if (key != "iid") {
                            var row = $("<tr></tr>");

                            var tdHead = $("<td style='width:80px;'></td>");
                            var tdContent = $("<td></td>");

                            if (key == "dk_bh") {
                                tdHead.text("地块编号：");

                                var aTemp = $("<a href='#' onclick=\"CrxxPub('" + o1["iid"].toString() + "')\"></a>");
                                aTemp.text(o1[key].toString());
                                aTemp.appendTo(tdContent);

                                //显示报名信息或报价信息
                                if (o1["bmsj"].toString() != "已结束") {//判断报名时间是否结束
                                    if (o1["bmgs"].toString() == "0") {
                                        var aBaoj = $("<a href='#' onclick=\"OpenInfo('" + o1["iid"].toString() + "',0)\"></a>");
                                        aBaoj.text("   报名   ");
                                        aBaoj.appendTo(tdContent);
                                    }
                                    else {
                                        var aBaoj = $("<span></span>");
                                        aBaoj.text("  已报名  ");
                                        aBaoj.appendTo(tdContent);
                                    }
                                }
                                else {//报名结束则看报价是否结束,如已报名则可报价
                                    if (o1["jjsj"].toString() != "已结束" && o1["bmgs"].toString() != "0") {
                                        var aBaoj = $("<a href='#' onclick=\"OpenInfo('" + o1["iid"].toString() + "',1)\"></a>");
                                        aBaoj.text("  >>报价 ");
                                        aBaoj.appendTo(tdContent);
                                    }
                                }
                            }
                            else if (key == "dk_zl") {
                                tdHead.text("土地坐落：");
                                tdContent.text(o1[key].toString());
                            }
                            else if (key == "dk_tdmj") {
                                tdHead.text("土地面积：");

                                tdContent.text(o1[key].toString() + "(平方米)");
                            }
                            else if (key == "dk_xmnr") {
                                tdHead.text("项目内容：");
                                tdContent.text(o1[key].toString());
                            }
                            else if (key == "dk_tdyt") {
                                tdHead.text("土地用途：");
                                tdContent.text(o1[key].toString());
                            }
                            else if (key == "zxbj") {
                                row = $("<tr style='color:red;'></tr>");
                                tdHead.text("最新报价：");
                                tdContent.text(o1[key].toString() + "(万元)");
                            }
                            else if (key == "bmsj") {
                                row = $("<tr style='color:red;'></tr>");
                                tdHead.text("报名时间：");
                                tdContent.text(o1[key].toString());
                            }
                            else if (key == "jjsj") {
                                row = $("<tr style='color:red;'></tr>");
                                tdHead.text("竞价时间：");
                                tdContent.text(o1[key].toString());
                            }
                            tdHead.appendTo(row);

                            //添加内容           
                            tdContent.appendTo(row);

                            //行加入到表格
                            row.appendTo(table);
                        }
                    }

                    table.appendTo(divTemp);
                    divTemp.appendTo($("#divCrMsg"));
                }
            }
        }

        function CrxxPub(strIid) {
            window.open("../Common/CommonTabUpdate.aspx?iid=" + strIid + "&tabname=工业用地出让信息");
        }

        $(document).ready(function () {
            var MyMar = setInterval(GetNewestDkxx, 1000); //定时(1分钟)扫描待办件
            HideLoading(); //隐藏进度条
        });

        //关闭窗口
        function CloseDivDlg() {
            $('#dlgConfig').window('close');
        }

        //打开内嵌网页的通用方法1（window）
        function openDivDlg(strTitle) {
            //增加参数queryid
            var divWidth = 400;
            var divHeight = 100;

            $('#dlgConfig').window({
                title: strTitle, width: divWidth, height: divHeight
            });
            // $('#dlgConfig').window({ maximized: true });
            $('#dlgConfig').window('open');
        }

        var iid = "";
        function OpenInfo(strIid, type) {
            iid = strIid;
            switch (type) {
                case 0:
                    btnSaveBaoMing();
                    break;
                case 1:
                    openDivDlg('输入报价');
                    break;
            }
        }

        function btnSaveBaoMing() {
            WebService(Common.GetServicePath() + "WebService/SbxtService.asmx/AddDkxxBaoMing", saveInfoCallBack, "{'strUserId':'" + strUserId + "','strIID':'" + iid + "'}");
        }

        function btnSaveBaoJia() {
            var strBaoJia = $("#txtBaoJia").val();
            WebService(Common.GetServicePath() + "WebService/SbxtService.asmx/AddDkxxBaoJia", saveInfoCallBack, "{'strUserId':'" + strUserId + "','strBaoJia':'" + strBaoJia + "','strIID':'" + iid + "'}");
        }

        function saveInfoCallBack(result) {
            if (result != "") {
                alert(result);

                if (result == "报价成功") {
                    CloseDivDlg();
                }
            }

        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <script type="text/javascript">
            ShowLoading(); //加载进度条
        </script>

        <div style="text-align: center;">
            <div id="tdTop" style="margin: 0px auto; text-align: center; " class="page_box">
                <uc4:ucPageHead ID="ucPageHead1" runat="server" />
            </div>
            <div style="text-align: center; margin: 0px auto; border:solid 1px #999999;" class="page_box">

                <div><span>最新出让信息</span></div>
                <div id="divCrMsg" style="text-align: center; overflow: hidden;">
                </div>

            </div>
            <div id="divBottom" style="margin: 0px auto; text-align: center;" class="page_box">

                <uc3:ucPageFoot ID="ucPageFoot1" runat="server" />
            </div>
        </div>
        <!--  信息查看  -->
        <div id="dlgConfig" class="easyui-window" title="信息查看" closed="true" modal="false"
            minimizable="false" style="width: 400px; height: 200px; background: #fafafa;">
            <table>
                <tr>
                    <td>报价<input type="text" id="txtBaoJia" /></td>
                    <td>
                        <input type="button" id="btnOk" value="确定" class="button_long" onclick="btnSaveBaoJia()" /></td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
