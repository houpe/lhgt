﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="SelectSerialOperation.aspx.cs" Inherits="Wsbs_SelectSerialOperation"
    Title="事项办理权限申请" %>

<%@ Register Src="../UserControls/GeneralSelect.ascx" TagName="GeneralSelect" TagPrefix="uc2" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">

    <script type="text/javascript"> 
        function AddMore(){ 
            var more = document.getElementById("file"); 
            var br = document.createElement("br"); 
            var input = document.createElement("input"); 
            var button = document.createElement("input"); 
            
            input.type = "file"; 
            input.name = "file"; 
            
            button.type = "button"; 
            button.value = "删除"; 
            
            more.appendChild(br); 
            more.appendChild(input); 
            more.appendChild(button); 
            
            button.onclick = function(){ 
                more.removeChild(br); 
                more.removeChild(input); 
                more.removeChild(button); 
            }; 
        } 
    </script>

    <div style="text-align: center;">
        <table class="inputtable" style="width: 90%">
            <tr>
                <td style="background-color: Snow; text-align: center; font-weight: bold;">
                    业务类型
                </td>
                <td style="text-align: left;">
                    <%--<uc2:GeneralSelect ID="ddlFlow" Name="所有流程" runat="server" />--%>
                    <asp:DropDownList ID="ddlFlow" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="background-color: Snow; text-align: center; font-weight: bold;">
                    申请理由
                </td>
                <td style="text-align: left;">
                    <asp:TextBox ID="txtNotes" Width="600" Height="200" TextMode="MultiLine" SkinID="LongInputGreen"
                        runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="background-color: Snow; text-align: center; font-weight: bold;">
                    上传材料
                </td>
                <td style="text-align: left;" id="file">
                    <input type="button" value="增加附件" onclick="AddMore()" /><br />
                    <input id="File2" type="file" runat="server" style="display: none" />
                    <asp:FileUpload ID="file1" runat="server" Width="500" Visible="false" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Button ID="btnSave" runat="server" Text="提交申请" OnClick="btnSave_Click" />
                </td>
            </tr>
        </table>
        <br />
        <defineControls:CustomGridView ID="CustomGridView1" runat="server" AllowPaging="True" SkinID="List"
            CustomSortExpression="" AutoGenerateColumns="false" ShowCmpAndCancel="False"
            ShowHideColModel="None" OnOnLoadData="CustomGridView1_OnLoadData" Width="750"
            EmptyDataText="." AllowOnMouseOverEvent="True" AutoGenerateDeleteConfirm="True"
            OnRowCreated="CustomGridView1_RowCreated">
            <PagerSettings Visible="False" />
            <Columns>
                <asp:HyperLinkField DataNavigateUrlFields="id" HeaderText="查看" DataNavigateUrlFormatString="AppearSerialInfo.aspx?id={0}"
                    Target="_blank" DataTextField="id" DataTextFormatString="详情" />
                <asp:BoundField DataField="flowtype" HeaderText="业务类型"></asp:BoundField>
                <asp:BoundField DataField="Notes" HeaderText="申请理由"></asp:BoundField>
                <asp:BoundField DataField="pass" HeaderText="是否通过"></asp:BoundField>
                <asp:TemplateField HeaderText="编辑">
                    <ItemTemplate>
                        <asp:HyperLink ID="hlDelete" runat="server" Visible='<%# SetDelete(Container)%>'>删除</asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </defineControls:CustomGridView>
    </div>
</asp:Content>
