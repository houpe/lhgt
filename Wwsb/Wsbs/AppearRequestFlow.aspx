﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AppearRequestFlow.aspx.cs"
    Inherits="Wsbs_AppearRequestFlow" %>

<%@ Register Src="../UserControls/ucDrawFlow.ascx" TagName="ucDrawFlow" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>画流程箭头</title>

    <script type="text/javascript" src="../ScriptFile/jquery.js"></script>

    <script type="text/javascript" src="../ScriptFile/DrawApi/DrawArrow.js"></script>

    <script type="text/javascript" src="../ScriptFile/DrawApi/DrawFlowInterface.js"></script>

</head>
<body>
    <form id="form1" runat="server">
    <uc1:ucDrawFlow ID="ucDrawFlow1" runat="server" />
    </form>
</body>
</html>
