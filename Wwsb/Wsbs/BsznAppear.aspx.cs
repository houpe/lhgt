﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SbBusiness.PubInfo;

public partial class Wsbs_BsznAppear : System.Web.UI.Page
{
    public string strHtmlInfo = string.Empty;
    InfoManage piTemp = new InfoManage();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
           
        }
    }

    /// <summary>
    /// 绑定办事指南
    /// </summary>
    //public string BindContent()
    //{
    //    System.Data.DataTable dtTemp = piTemp.GetPubContentInfo("办事指南", Request["wid"]);
    //    if (dtTemp.Rows.Count > 0)
    //    {
    //        strHtmlInfo = dtTemp.Rows[0]["content"].ToString();
    //    }
    //    return strHtmlInfo;
    //}

    /// <summary>
    /// 绑定办事指南(新表)
    /// </summary>
    /// <returns></returns>
    /// <!--addby zhongjian 20100118-->
    public string BindLawGuide()
    {
        DataTable dtTemp = new DataTable(); 
        dtTemp = piTemp.GetPubContentInfo(Request["wid"]);

        if (dtTemp.Rows.Count > 0)
        {
            strHtmlInfo="<table cellspacing='1' cellpadding='8' width='98%' bgcolor='#a5cdf2' border='0' style='margin-left: 10px;'>";
            for (int i = 0; i < dtTemp.Rows.Count; ++i)
            {
                string strTitle = dtTemp.Rows[i]["ftitle"].ToString();
                string strContent = dtTemp.Rows[i]["fcontent"].ToString();
                strHtmlInfo += string.Format(@"<tr>
                            <td bgcolor='#ffffff' style='width: 20%; font-weight: bold; text-align: center;background-color:Snow'>
                                <font color='#1467c7'>{0}</font>
                            </td>
                            <td style='width: 80%; text-align: left;' bgcolor='#ffffff'>
                                <textarea id='TextArea1' cols='20' readonly='readonly' rows='4' class='tatarea'>{1}</textarea>
                            </td>
                        </tr>", strTitle, strContent);

            }
            strHtmlInfo += "</table>";
        }
        return strHtmlInfo;
    }
}
