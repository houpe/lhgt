﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LyzxList.aspx.cs" Inherits="Wsbs_LyzxList" %>

<%@ Register Src="~/UserControls/MsgPlate.ascx" TagName="MsgPlate" TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/ucPageFoot.ascx" TagName="ucPageFoot" TagPrefix="uc2" %>
<%@ Register Src="~/UserControls/ucPageHead.ascx" TagName="ucPageHead" TagPrefix="uc3" %>
<%@ Register Src="../UserControls/Calendar.ascx" TagName="Calendar" TagPrefix="uc4" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>留言咨询</title>
    <link href="../App_Themes/SkinFile/CSS/gcpage.css" rel="stylesheet" type="text/css" />
    <link href="../App_Themes/SkinFile/CSS/StyleSheet.css" rel="stylesheet" type="text/css" />

    <script src="../ScriptFile/Calendar.js" type="text/javascript"></script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table border="0" align="center" cellpadding="0" cellspacing="0" class="page_box">
            <tr>
                <td colspan="3">
                    <uc3:ucPageHead ID="ucPageHead1" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="3" align="center" valign="top">
                    <table border="0" align="center" cellpadding="0" cellspacing="0" width="80%">
                        <tr>
                            <td align="center">
                                <div class="end_bar">
                                    留言咨询</div>
                                <div style="text-align: center">
                                    <table class="tab_Content">
                                        <tr>
                                            <td>
                                                类型：
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlWtlx" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                主题：
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtTitle" runat="server"></asp:TextBox>
                                            </td>
                                            <td>
                                                留言查询码：
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtMsgCode" runat="server" Width="80px"></asp:TextBox>
                                            </td>
                                            <td>
                                                开始日期：
                                            </td>
                                            <td>
                                                <input onfocus="setday(this)" id="txtFromDate" runat="server" readonly="readonly"
                                                    size="10" />
                                                <%-- <uc4:Calendar ID="txtFromDate" runat="server" Width="80" />--%>
                                            </td>
                                            <td>
                                                结束日期：
                                            </td>
                                            <td>
                                                <input onfocus="setday(this)" id="txtToDate" runat="server" readonly="readonly" size="10" />
                                                <%-- <uc4:Calendar ID="txtToDate" runat="server" Width="80" />--%>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnQuery" Text="查询" OnClick="btnQuery_OnClick" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <br />
                                <div style="color: Black">
                                    <defineControls:CustomGridView ID="cgvLyzx" runat="server" AllowPaging="True" AutoGenerateColumns="false"
                                        ShowItemNumber="true" SkinID="List" CheckboxAutoPostBack="False" CustomImportToExcel="false"
                                        CustomSortExpression="" DeleteToolTipOfCol="-1" HideTextLength="35" ShowCmpAndCancel="False"
                                        ShowGridViewSelect="List" ShowHideColModel="None" ShowImportButton="False" ShowTreeView="False"
                                        SelectOrDelete="True" AllowSortingAscImgCss="" AllowSortingDescImgCss="" Width="950"
                                        OnOnLoadData="cgvLYZX_OnLoadData">
                                        <Columns>
                                            <asp:BoundField DataField="TypeText" HeaderText="类型" ItemStyle-Height="25px" />
                                            <asp:TemplateField HeaderText="主题">
                                                <ItemTemplate>
                                                    <a href="LyzxMessage.aspx?id=<%# Eval("id")%>" target="_blank">
                                                        <%# Eval("title")%></a>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="notes" HeaderText="内容" />
                                            <asp:BoundField DataField="createdate" HeaderText="留言日期" />
                                        </Columns>
                                    </defineControls:CustomGridView>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <uc2:ucPageFoot ID="ucPageFoot1" runat="server" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
