﻿<%@ WebHandler Language="C#" Class="AjaxServerResponse" %>

using System;
using System.Web;
using System.Web.SessionState;
using System.Data;

public class AjaxServerResponse : IHttpHandler, IRequiresSessionState
{
    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/plain";
        //context.Response.Charset = "utf-8";
        context.Response.Write(PostData(context, context.Request.Url.Authority));
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    public string PostData(HttpContext contextParam, string strServerName)
    {
        string strReturn = string.Empty;
        string[] arrStrParams = contextParam.Server.UrlDecode(contextParam.Request["dataParam"]).Split(',');
        
        //获取userid对应的sessionid
        Business.FlowOperation.ClsUserWorkFlow cuwfTemp = new Business.FlowOperation.ClsUserWorkFlow();
        string strWid = cuwfTemp.GetFlowIdByName(arrStrParams[1]);

        switch (arrStrParams[0])
        {
            case "0"://办事指南---稍后要改成动态的
                strReturn = string.Format("BsznAppear.aspx?wid={0}", strWid);
                break;
            case "1"://办理流程
                strReturn = string.Format("http://{0}{1}?wid={2}", strServerName,
                    System.Configuration.ConfigurationManager.AppSettings["ActivexDisplay"], strWid);

                break;
            case "2"://表格下载
                strReturn = string.Format("../Common/FileUploadView.aspx?datatype={0}", contextParam.Server.UrlEncode(arrStrParams[1]));
                break;
            case "3"://在线申报---第三个参数对应iid
                if (arrStrParams[2].Trim() == "-1")
                {
                    SbBusiness.Wsbs.SerialInstance sioTemp = new SbBusiness.Wsbs.SerialInstance();
                    long decValue = sioTemp.GetIIDWithEnterprise("", "", "");
                    //long decValue = WF_Business.SeedNumber.GetIIDWithEnterprise("", "", "");
                    strReturn = string.Format("ShenBao.aspx?flowname={0}&iid={1}", arrStrParams[1], decValue);
                }
                else//已有编号的前提下
                {
                    strReturn = string.Format("ShenBao.aspx?flowname={0}&iid={1}", arrStrParams[1], arrStrParams[2]);
                }
                break;
            case "4"://结果公示
                strReturn = string.Format("JggsPage.aspx?wid={0}", strWid);
                break;
            case "5"://办件查询
                strReturn = string.Format("SerialQuery.aspx?wid={0}", strWid);
                break;
            case "6"://相关规定
                strReturn = string.Format("AppearXggd.aspx?wid={0}", strWid);
                break;
            case "7"://在线投诉
                //strReturn = string.Format("TsManager.aspx?wid={0}", strWid);
                strReturn = string.Format("TsOnline.aspx?wid={0}", strWid);
                break;
            default:
                break;
        }

        return strReturn;
    }

}