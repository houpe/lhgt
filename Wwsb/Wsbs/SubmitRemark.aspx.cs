﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SbBusiness.Wsbs;
using SbBusiness;

public partial class Wsbs_SubmitRemark : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindRemark();
        }
    }

    /// <summary>
    /// 绑定补证补齐原因
    /// </summary>
    /// <!--addby zhongjian 20091225-->
    public void BindRemark()
    {
        ShenBaoSubmit cSubmit = new ShenBaoSubmit();
        if(Request["flag"]=="4")//驳回原因查看
            txtRemark.Text = cSubmit.GetSubmitStopRemark(Request["iid"]);
        else//不予受理或补正补齐
            txtRemark.Text = cSubmit.GetSubmitRemark(Request["iid"]);
    }
}
