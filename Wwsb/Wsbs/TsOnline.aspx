﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master"
    CodeFile="TsOnline.aspx.cs" Inherits="Wsbs_TsOnline" %>

<%@ Register Src="../UserControls/PersistenceControl.ascx" TagName="PersistenceControl"
    TagPrefix="uc2" %>
<%@ Register Src="../UserControls/TextBoxWithValidator.ascx" TagName="TextBoxWithValidator"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControls/GeneralSelect.ascx" TagName="GeneralSelect" TagPrefix="uc3" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">

    <script  type="text/javascript" src="../ScriptFile/Regex.js"></script>

    <script type="text/javascript">
        function openwin() {
            window.open('SerialInfoList.aspx?wid=<%=Request["wid"]%>', 'openInstance', 'width=620,height=420,left=50,top=100');
        }

        function SetControlsValue(strIid) {
            document.getElementById("<%=txtIID.ChildControlClientId%>").value = strIid;
        }
   	   
    </script>

    <div style="text-align: center;">
        <table cellspacing="0" cellpadding="0" width="98%" border="0">
            <tr>
                <td valign="middle">
                    <table style="border-collapse: collapse" bordercolordark="#869ed9" width="96%" bordercolorlight="#869ed9"
                        border="1">
                        <tr>
                            <td bgcolor="#73a8dd" colspan="4" style="text-align: left;">
                                <font color="#ffffff">投诉信息：</font>
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 19px" width="15%" bgcolor="#cee7ff">
                                投诉部门
                            </td>
                            <td style="height: 19px" bgcolor="#dfefff">
                                <font color="#ff0000">
                                    <%--<uc3:GeneralSelect ID="ddlTsbm" Name="投诉部门" runat="server" />--%>
                                    <asp:DropDownList ID="ddlTsbm" runat="server"></asp:DropDownList>
                                </font>
                            </td>
                            <td style="height: 19px" width="15%" bgcolor="#cee7ff">
                                受理编号
                            </td>
                            <td style="height: 19px" bgcolor="#dfefff">
                                <uc1:TextBoxWithValidator ID="txtIID" Type="Double" runat="server" Width="80%" />
                                <a href="javascript:openwin();">选择</a>
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 33px" width="15%" bgcolor="#cee7ff">
                                投诉类型
                            </td>
                            <td style="height: 33px" bgcolor="#dfefff">
                                <uc3:GeneralSelect ID="ddlTslx" Name="投诉类型" runat="server" />
                            </td>
                            <td style="height: 33px" width="15%" bgcolor="#cee7ff">
                            </td>
                            <td style="height: 33px" bgcolor="#dfefff">
                            </td>
                        </tr>
                        <tr>
                            <td width="100%" bgcolor="#73a8dd" colspan="4" style="text-align: left;">
                                <font color="#ffffff">必填项目：</font>
                            </td>
                        </tr>
                        <tr>
                            <td style="background-color: #cee7ff">
                                您的姓名
                            </td>
                            <td style="height: 25px" width="35%" bgcolor="#dfefff">
                                <uc1:TextBoxWithValidator ID="txtTSR" runat="server" Width="98%" />
                            </td>
                            <td style="height: 25px" width="15%" bgcolor="#cee7ff">
                                联系电话
                            </td>
                            <td style="height: 25px" width="35%" bgcolor="#dfefff">
                                <uc1:TextBoxWithValidator ID="txtLxdh" Type="Phone" ErrorMessage="联系电话输入错误" runat="server"
                                    Width="98%" />
                            </td>
                        </tr>
                        <tr>
                            <td width="15%" bgcolor="#cee7ff">
                                电子邮箱
                            </td>
                            <td width="35%" bgcolor="#dfefff">
                                <uc1:TextBoxWithValidator ID="txtEmail" runat="server" Width="98%" />
                            </td>
                            <td bgcolor="#cee7ff">
                                您的身份
                            </td>
                            <td bgcolor="#dfefff">
                                <uc3:GeneralSelect ID="ddlSflb" Name="身份类别" runat="server" />
                            </td>
                        </tr>
                    </table>
                    <table style="border-collapse: collapse" bordercolordark="#869ed9" width="96%" bordercolorlight="#869ed9"
                        border="1">
                        <tr>
                            <td width="100%" bgcolor="#73a8dd" colspan="4" style="text-align: left;">
                                <font color="#ffffff">选填项目：</font>
                            </td>
                        </tr>
                        <tr>
                            <td width="15%" bgcolor="#cee7ff" height="21">
                                您的年龄
                            </td>
                            <td width="35%" bgcolor="#dfefff" height="21">
                                <uc1:TextBoxWithValidator ID="txtAge" Type="Integer" ErrorMessage="输入年龄为非法数据" runat="server"
                                    Width="98%" />
                            </td>
                            <td width="15%" bgcolor="#cee7ff" height="21">
                                您的性别
                            </td>
                            <td width="35%" bgcolor="#dfefff" height="21">
                                <asp:RadioButtonList runat="server" ID="rdbSex" RepeatDirection="Horizontal">
                                    <asp:ListItem>男</asp:ListItem>
                                    <asp:ListItem>女</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td width="15%" bgcolor="#cee7ff">
                                居民身份证号码
                            </td>
                            <td width="35%" bgcolor="#dfefff">
                                <uc1:TextBoxWithValidator ID="txtSfzh" Type="IDCard" ErrorMessage="15或18位身份证号输入错误"
                                    runat="server" Width="98%" />
                            </td>
                            <td width="15%" bgcolor="#cee7ff">
                                教育程度
                            </td>
                            <td width="35%" bgcolor="#dfefff">
                                <uc3:GeneralSelect ID="ddlXl" Name="学历" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td width="15%" bgcolor="#cee7ff" height="22">
                                住址/邮政地址
                            </td>
                            <td width="35%" bgcolor="#dfefff" height="22">
                                <uc1:TextBoxWithValidator ID="txtYzdz" runat="server" Width="98%" />
                            </td>
                            <td width="15%" bgcolor="#cee7ff" height="22">
                                邮政编码
                            </td>
                            <td width="35%" bgcolor="#dfefff" height="22">
                                <uc1:TextBoxWithValidator ID="txtYzbm" Type="Postcode" ErrorMessage="6位邮政编码格式输入错误"
                                    runat="server" Width="98%" />
                            </td>
                        </tr>
                    </table>
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                    <table id="table2" style="border-collapse: collapse" bordercolordark="#869ed9" width="96%"
                        bordercolorlight="#869ed9" border="1">
                        <tr>
                            <td width="15%" bgcolor="#cee7ff">
                                您的建议或意见：
                            </td>
                            <td bgcolor="#dfefff" colspan="5">
                                <uc1:TextBoxWithValidator ID="txtTSNR" TextMode="MultiLine" runat="server" Rows="10"
                                    Width="98%" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <asp:Button ID="btnSave" runat="server" Text="保存" OnClick="btnSave_Click" SkinID="LightGreen" />
        <asp:Button ID="btnReturn" runat="server" Text="返回" CausesValidation="False" OnClick="btnReturn_Click"
            SkinID="LightGreen" Visible="false" />
    </div>
    <uc2:PersistenceControl ID="PersistenceControl1" runat="server" Key="id" Table="XT_TSINFO" />
</asp:Content>
