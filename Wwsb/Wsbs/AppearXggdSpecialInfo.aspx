﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AppearXggdSpecialInfo.aspx.cs"
    Inherits="Wsbs_AppearXggdSpecialInfo" %>

<%@ Register Src="~/UserControls/MsgPlate.ascx" TagName="MsgPlate" TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/ucPageFoot.ascx" TagName="ucPageFoot" TagPrefix="uc2" %>
<%@ Register Src="~/UserControls/ucPageHead.ascx" TagName="ucPageHead" TagPrefix="uc3" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>相关规定详情展示</title>
</head>
<body>
    <form id="form1" runat="server">
    <table border="0" align="center" cellpadding="0" cellspacing="0" class="page_box">
        <tr>
            <td colspan="3">
                <uc3:ucPageHead ID="ucPageHead1" runat="server" />
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center" valign="top">
                <div style="text-align: center">
                    <table style="width: 95%">
                        <tr>
                            <td style="width: 80px">
                            </td>
                            <td>
                                <table cellspacing="0" style="text-align: center" cellpadding="0" width="760px" border="0">
                                    <tr>
                                        <td class="bgtop" colspan="3">
                                            <%=strTitle%><%--
                                            <p align="center">
                                                发布时间：<%=strPubTime%></p>--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="bgleft">
                                        </td>
                                        <td class="tablesynopsis">
                                            <br />
                                        </td>
                                        <td class="bgright">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="bgleft">
                                        </td>
                                        <td class="tablesynopsis">
                                            <%=strHtmlInfo %>
                                        </td>
                                        <td class="bgright">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="bgleft">
                                        </td>
                                        <td align="right" style="background-color: #FBF6F6;">
                                            &nbsp;
                                        </td>
                                        <td class="bgright">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="bgBottom" colspan="3">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="3">
                                            <a class="font1" href="#" onclick="self.close();">[关闭窗口]</a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style="width: 80px">
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <uc2:ucPageFoot ID="ucPageFoot1" runat="server" />
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
