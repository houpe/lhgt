﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SbBusiness.Wsbs;
using SbBusiness;

public partial class Wsbs_SerialList : System.Web.UI.Page
{
    private SerialInstance cInstance = new SerialInstance();
    private ShenBaoSubmit cSubmit = new ShenBaoSubmit();
    public string strRenWu = "当前任务";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindData();
        }
    }

    /// <summary>
    /// 绑定当前办理事项进度信息
    /// </summary>
    /// <!--addby zhongjian 20091021-->
    private void BindData()
    {
        string strSerial = Request["serial"];
        DataTable dtSource = new DataTable(); 
        strRenWu = "当前办理进度查看";
        dtSource = cInstance.GetSerialList(strSerial);

        this.CustomGridView1.DataSource = dtSource;
        this.CustomGridView1.PageSize = SystemConfig.PageSize;
        this.CustomGridView1.RecordCount = dtSource.Rows.Count;
        this.CustomGridView1.DataBind();
    }
    protected void CustomGridView1_OnLoadData(object sender, EventArgs e)
    {
        BindData();
    }
    public string DangQianRenWu()
    {
        return strRenWu;
    }
    protected void btnReturn_Click(object sender, EventArgs e)
    {
        string strFlag = Request["flag"];
        Response.Redirect("submit.aspx?flag=" + strFlag + "");
    }
}
