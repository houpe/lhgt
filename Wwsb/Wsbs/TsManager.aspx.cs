﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SbBusiness.PubInfo;
using SbBusiness;

public partial class Wsbs_TsManager : System.Web.UI.Page
{
    TsInfoManager tsManager = new TsInfoManager();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Params["action"] == "delete")
        {
            tsManager.DeleteTsInfo(Request.Params["id"]);
        }

        if (!IsPostBack)
        {
            BindData();
        }
    }

    /// <summary>
    /// Binds the data.
    /// </summary>
    private void BindData()
    {
        DataTable dtSource = tsManager.GetTsInfo(Request["wid"], txtTsr.Text, txtBstr.Text, txtFromTssj.Text, txtToTssj.Text);

        this.CustomGridView1.DataSource = dtSource;
        this.CustomGridView1.PageSize = SystemConfig.PageSize;
        this.CustomGridView1.RecordCount = dtSource.Rows.Count;
        this.CustomGridView1.DataBind();
    }

    /// <summary>
    /// Handles the OnLoadData event of the CustomGridView1 control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void CustomGridView1_OnLoadData(object sender, EventArgs e)
    {
        BindData();
    }

    protected void btnQuery_OnClick(object sender, EventArgs e)
    {
        BindData();
    }

    #region 行创建
    //
    protected void gridviewCar_RowCreated(object sender, GridViewRowEventArgs e)
    {
        switch (e.Row.RowType)
        {
            case DataControlRowType.DataRow:
                DataRowView dtrv = e.Row.DataItem as DataRowView;
                if (dtrv == null)
                {
                    break;
                }
                break;
        }
    }
    #endregion

    #region 添加行
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        //Response.Redirect("TsEdit.aspx");
        Response.Redirect(string.Format("TsOnline.aspx?wid={0}",Request["wid"]));
    }
    #endregion
}
