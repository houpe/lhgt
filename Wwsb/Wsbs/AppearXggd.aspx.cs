﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using SbBusiness;

using System.Configuration;
using SbBusiness.PubInfo;

public partial class Wsbs_AppearXggd : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindData();
        }
    }

    /// <summary>
    /// Binds the data.
    /// </summary>
    private void BindData()
    {
        InfoManage pubInfo = new InfoManage();
        DataTable dtSource=new DataTable();
        if (Request["wid"] != null)
        {
            dtSource = pubInfo.GetPubContentInfo("相关规定",
            Request["wid"], txtTitle.Text, txtQSSJ.Text, txtZZSJ.Text);
        }
        else if (Session["flowname"] != null)
        {
            dtSource = pubInfo.GetInfoType("相关规定", Session["flowname"].ToString());
        }
        this.dgXggd.DataSource = dtSource;
        this.dgXggd.PageSize = SystemConfig.PageSize;
        this.dgXggd.RecordCount = dtSource.Rows.Count;
        this.dgXggd.DataBind();
        
    }

    /// <summary>
    /// Handles the OnLoadData event of the dgXggd control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void dgXggd_OnLoadData(object sender, EventArgs e)
    {
        BindData();
    }

    /// <summary>
    /// Handles the OnClick event of the btnQuery control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void btnQuery_OnClick(object sender, EventArgs e)
    {
        BindData();
    }

    /// <summary>
    /// 创建GridView行
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dgXggd_RowCreated(object sender, GridViewRowEventArgs e)
    {
        switch (e.Row.RowType)
        {
            case DataControlRowType.DataRow:
                DataRowView dtrv = e.Row.DataItem as DataRowView;
                if (dtrv != null)
                {
                    HyperLink hlView = e.Row.FindControl("hlView") as HyperLink;
                    if (hlView != null)
                    {
                        hlView.NavigateUrl = "#";
                        string strOpenClick = string.Format("ProgressView('AppearXggdSpecialInfo.aspx?id={0}');",dtrv["id"]);
                        hlView.Attributes.Add("onclick", strOpenClick);
                    }
                }
                break;
        }
    }
}
