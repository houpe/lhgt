﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using SbBusiness;
using SbBusiness.Wsbs;

public partial class Wsbs_SerialQuery : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindData();
        }
    }

    /// <summary>
    /// Binds the data.
    /// </summary>
    private void BindData()
    {
        SerialInstance instanceOperation = new SerialInstance();

        string strUserId=string.Empty;
        if (Session["UserId"] != null)
        {
            strUserId = Session["UserId"].ToString();
        }

        DataTable dtSource = instanceOperation.GetMainSerialInfo(txtIid.Text, txtSQQSSJ.Value,
            txtSQZZSJ.Value, Request["wid"], strUserId);

        this.CustomGridView1.DataSource = dtSource;
        this.CustomGridView1.PageSize = SystemConfig.PageSize;
        this.CustomGridView1.RecordCount = dtSource.Rows.Count;
        this.CustomGridView1.DataBind();   
    }

    /// <summary>
    /// Handles the OnLoadData event of the CustomGridView1 control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void CustomGridView1_OnLoadData(object sender, EventArgs e)
    {
        BindData();
    }

    /// <summary>
    /// Handles the OnClick event of the btnQuery control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void btnQuery_OnClick(object sender, EventArgs e)
    {
        BindData();
    }

    /// <summary>
    /// 创建GridView行
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void CustomGridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        switch (e.Row.RowType)
        {
            case DataControlRowType.DataRow:
                DataRowView dtrv = e.Row.DataItem as DataRowView;
                if (dtrv != null)
                {
                    HyperLink hlView = e.Row.FindControl("hlView") as HyperLink;
                    if (hlView != null)
                    {
                        hlView.NavigateUrl = "#";
                        string strOpenClick = string.Format("ProgressView('http://{0}{1}?iid={2}');", Request.Url.Authority,
                            ConfigurationManager.AppSettings["ActivexDisplay"], dtrv["案件编号"]);
                        //string strOpenClick = string.Format(@"ProgressView('WsbsMain.aspx?type=1&flowtype={1}&flowname={2}&iid={0}');",
                        //    dtrv["案件编号"], Request["flowtype"], Request["flowname"]);
                        hlView.Attributes.Add("onclick", strOpenClick);
                    }
                }
                break;
        }
    }
}
