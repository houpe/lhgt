﻿<%@ WebHandler Language="C#" Class="AjaxSubmitFlag" %>

using System;
using System.Web;
using System.Data;

public class AjaxSubmitFlag : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/plain";

        string strFlowName = context.Server.UrlDecode(context.Request["dataFlowname"]);
        string strFlowType = context.Server.UrlDecode(context.Request["flowType"]);
        string strUserID = context.Request["userId"];
        string strUserName = context.Request["userName"];
        string strBgFlag = context.Request["bgflag"];
        string strYqFlag = context.Request["yqflag"];
        string strIID = context.Request["dataIId"];

        //先处理iid的问题
        SbBusiness.Wsbs.ShenBaoSubmit submit = new SbBusiness.Wsbs.ShenBaoSubmit();
        int nReturnSubmit = submit.InsertSubmitInfo(strIID, strFlowName, strUserID);

        context.Response.Write(PostIId(strIID, strFlowName, strUserID, strUserName, strBgFlag, strFlowType));
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    /// <summary>
    /// 提交并设置状态位
    /// </summary>
    /// <param name="strIId"></param>
    /// <param name="strFlowname">办理事项名称</param>
    /// <param name="strUserID"></param>
    /// <param name="strUserName"></param>
    /// <param name="strBgFlag">变更标志</param>
    /// <param name="strFlowType">办理事项分类</param>
    /// <returns></returns>
    public string PostIId(string strIId, string strFlowname, string strUserID, string strUserName, string strBgFlag, string strFlowType)
    {
        SbBusiness.Wsbs.ShenBaoSubmit submit = new SbBusiness.Wsbs.ShenBaoSubmit();
        string strFlag = submit.GetFlag(strFlowname);
        if (strFlag == "1")
        {
            Business.Common.UploadFileClass upload = new Business.Common.UploadFileClass();

            string strAlredyCount = upload.GetAlreadyUpdateCount(strIId, strFlowname);
            string strCount = upload.GetUpdateCount(strFlowname);
            if (strAlredyCount != strCount)
            {
                return "";
            }
        }

        int nReturn = 0;
        nReturn = submit.UpdateSubmit(strIId, 1);

        //添加操作日志 addby zhongjian 20100401
        string strSql = string.Format(@"update XT_SUBMIT_INFO set SUBMITFLAG={1}, edittime=sysdate where IID='{0}'", strIId, 1);
        string strRemark = string.Format("提交{0}的业务,申请编号为: {1}", strFlowType, strIId);
        SbBusiness.SystemLogs.AddSystemLogs(strUserID, "update", strRemark, strSql);

        if (nReturn == 1)
        {
            //提交成功后发送消息 editby zhongjian 20091208
            SbBusiness.Wsbs.Messagebox message = new SbBusiness.Wsbs.Messagebox();
            DataTable dtTemp = message.GetRequestSet(0, "已经提交");
            string strMsgContent = string.Empty;
            string strStepNo = string.Empty;

            if (dtTemp.Rows.Count > 0)
            {
                strStepNo = dtTemp.Rows[0]["step_no"].ToString();
                strMsgContent = dtTemp.Rows[0]["step_msg"].ToString();
            }
            if (!string.IsNullOrEmpty(strMsgContent))//当消息内容设置为空时，将不再发送消息。
            {
                try
                {
                    strMsgContent = string.Format(strMsgContent, strUserName, strFlowType);
                }
                catch { }

                message.InsertMeaasge(strMsgContent, "", strUserName, strUserID);
            }
            message.UpdateUserStepNO(strUserID, strStepNo);

            return "提交成功";
        }

        return "";
    }
}