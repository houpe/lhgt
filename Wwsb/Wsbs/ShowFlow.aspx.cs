﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


using System.IO;

using Business.FlowOperation;

public partial class Wsbs_ShowFlow : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindData();
        }
    }

    /// <summary>
    /// 绑定上传流程图
    /// </summary>
    /// <!--addby zhongjian 20100426-->
    private void BindData()
    {
        WorkFlowPubSetting wfpsTemp = new WorkFlowPubSetting();
        DataTable dtTemp = new DataTable();
        if (!string.IsNullOrEmpty(Request["id"]))
        {
            dtTemp = wfpsTemp.GetWorkFlowView(Request["id"], "");
        }

        if (dtTemp.Rows.Count > 0)
        {            
            byte[] bArr = dtTemp.Rows[0]["flowview"] as Byte[];
            Response.ClearContent();
            Response.Clear();
            Response.Buffer = true;
            ImageView1.Images = bArr;
            ImageView1.Visible = true;
            if (bArr != null)
            {
                Response.BinaryWrite(bArr);
            }
            else
            {
                this.Response.Write("未公开该办理事项的流程图。");
                ImageView1.Visible = false;
            }
        }
    }
}
