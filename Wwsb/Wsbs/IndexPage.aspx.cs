﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using SbBusiness.Wsbs;

using SbBusiness.User;
using SbBusiness;

using SbBusiness.Menu;
using SbBusiness.PubInfo;
using Business.FlowOperation;

public partial class Wsbs_IndexPage : System.Web.UI.Page
{
    private WorkFlowPubSetting cWorks = new WorkFlowPubSetting();
    private SerialInstance sioTemp = new SerialInstance();
    private ShenBaoSubmit cSubmit = new ShenBaoSubmit();
    private Messagebox cMessage = new Messagebox();
    private SerialInstance cInstance = new SerialInstance();
    private InfoManage pubInfo = new InfoManage();

    public string strWid = string.Empty;

    private string strNoSubmit = string.Empty; //新申(尚未提交)请事项个数
    private string strInstance = string.Empty; //正在办理(当前办理)事项个数
    private string strTongZhi = string.Empty; //通知个数

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindValue();
            BindFileUpLoad();
            BindAbout();
            BindResults();
        }
    }

    /// <summary>
    /// 绑定结果公示
    /// </summary>
    /// <!--addby zhongjian 20100317-->
    private void BindResults()
    {
        DataTable dtSource = sioTemp.GetInstanceResultInfo("", "", "", strWid);
        this.rptResults.DataSource = dtSource;
        this.rptResults.DataBind();
    }

    /// <summary>
    /// Handles the OnLoadData event of the CustomGridView1 control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void CustomGridView1_OnLoadData(object sender, EventArgs e)
    {
        BindResults();
    }

    /// <summary>
    /// 绑定表格下载
    /// </summary>
    /// <!--addby zhongjian 20100113-->
    private void BindFileUpLoad()
    {
        if (Session["flowname"] != null)
        {
            Business.Common.UploadFileClass ufcGloab = new Business.Common.UploadFileClass();
            DataTable dtSource = ufcGloab.GetResourceInfo(string.Empty, hidWname.Value);
            if (dtSource.Rows.Count > 0)
            {
                rptFileUpLoad.DataSource = dtSource;
                rptFileUpLoad.DataBind();
            }
        }
    }

    /// <summary>
    ///绑定相关规定
    /// </summary>
    /// <!--addby zhongjian 20100113-->
    private void BindAbout()
    {
        if (Session["flowname"] != null)
        {
            DataTable dtSource = pubInfo.GetPubContentInfo("相关规定", strWid,"","","");
            if (dtSource.Rows.Count > 0)
            {
                rptAbout.DataSource = dtSource;
                rptAbout.DataBind();
            }
        }
    }

    /// <summary>
    /// 绑定其它连接
    /// </summary>
    /// <param name="row"></param>
    /// <returns></returns>
    /// <!--addby zhongjian 20100311-->
    protected string GetImage(IDataItemContainer row)
    {
        string strHtml = string.Empty;
        string strLinkHref = DataBinder.Eval(row.DataItem, "LINKEHREF").ToString();
        string strTitle = DataBinder.Eval(row.DataItem, "title").ToString();
        string strImgSrc = DataBinder.Eval(row.DataItem, "imgsrc").ToString();
        if (!string.IsNullOrEmpty(strImgSrc))
        {
            //string strPath = System.IO.Directory.GetCurrentDirectory();
            //strImgSrc = Server.MapPath("~/image/uploadimg/" + strImgSrc);
            strHtml += string.Format(@"<a href='{0}' target='_blank'><img alt='{1}' style='border:0' src='{2}' /></a><br>",
                                        strLinkHref, strTitle, strImgSrc);
        }
        else
        {
            strHtml += string.Format(@"<li><a href='{0}' target='_blank'>{1}</a></li><br>", strLinkHref, strTitle);
        }

        return strHtml;
    }

    /// <summary>
    /// 绑定一些参数
    /// </summary>
    /// <!--addby zhongjian 20100316-->
    public void BindValue()
    {
        //流程名称
        if (!string.IsNullOrEmpty(Request["flowname"]))
        {
            hidWname.Value = Request["flowname"];
            Session["flowname"] = hidWname.Value;
        }
        else if (Session["flowname"] != null)
        {
            hidWname.Value = Session["flowname"].ToString();
        }

        //流程别名
        if (!string.IsNullOrEmpty(Request["flowtype"]))
        {
            HidWType.Value = Server.UrlEncode(Request["flowtype"]);
            Session["flowtype"] = Request["flowtype"];
            //ddlFlow.SelectedItem.Text = Session["flowtype"].ToString();
        }
        else if (Session["flowtype"] != null)
        {
            HidWType.Value = Session["flowtype"].ToString();
            //ddlFlow.SelectedItem.Text = Session["flowtype"].ToString();
        }

        if (!string.IsNullOrEmpty(Request["iid"]))
        {
            hidIId.Value = string.Format("{0}", Request["iid"]);
        }
        else
        {
            hidIId.Value = "-1";
        }
        //在线申报
        decimal decValue = Convert.ToDecimal(sioTemp.GetValue("ss_instance_id"));
        string strUrl = string.Format("ShenBao.aspx?flowname={0}&iid={1}", Server.UrlEncode(hidWname.Value), decValue);
        sioTemp.UpdateValue("ss_instance_id", decValue + 1);
        hidShenBaoUrl.Value = strUrl;

        //办理流程 update zhongjian 20100427
        string strFlagPath = string.Empty;//流程图路径
        string strWorkWid = string.Empty; //流程图ID

        ClsUserWorkFlow cuwfTemp = new ClsUserWorkFlow();
        strWid = cuwfTemp.GetFlowIdByName(hidWname.Value);


        strFlagPath = string.Format("http://{0}{1}?wid={2}", Request.Url.Authority,
            System.Configuration.ConfigurationManager.AppSettings["ActivexDisplay"], strWid);

        hidWorkFlowUrl.Value = strFlagPath;


        string strUserId = string.Empty;
        if (Session["UserId"] != null)
        {
            strUserId = Session["UserId"].ToString();
            strNoSubmit = cSubmit.GetNoSubmitCount(strUserId);//新申(尚未提交)请事项个数
            strInstance = cInstance.GetInstanceCount(strUserId);//正在办理(当前办理)事项个数        
            strTongZhi = cMessage.GetMessageCount(strUserId);//通知个数
            div_Login.Visible = false;
            div_Sheet.Visible = true;

            //判断事项操作权限
            if (SysUserRightRequest.GetPower(strUserId, hidWname.Value))
                hidPowerFlag.Value = "true";
            else
                hidPowerFlag.Value = "false";
        }
        else
        {
            div_Login.Visible = true;
            div_Sheet.Visible = false;
            hidPowerFlag.Value = "no";
        }
    }

    #region 系统登录
    /// <summary>
    /// Handles the LoginError event of the Login1 control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Login1_LoginError(object sender, EventArgs e)
    {
        Label labMsg = Login1.FindControl("labMsg") as Label;
        if (labMsg.Text == "")
        {
            labMsg.Text = "密码错误！";
        }
    }

    /// <summary>
    /// Handles the LoggingIn event of the Login1 control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.LoginCancelEventArgs"/> instance containing the event data.</param>
    protected void Login1_LoggingIn(object sender, LoginCancelEventArgs e)
    {
        if (Session["CheckCode"] == null)
        {
            return;
        }

        TextBox txtCheckCode = Login1.FindControl("txtCheckCode") as TextBox;
        Label labMsg = Login1.FindControl("labMsg") as Label;

        if (txtCheckCode.Text.ToUpper() != Session["CheckCode"].ToString())　　//验证码
        {
            labMsg.Text = "验证码错误！";
            e.Cancel = true;//退出LogginIn
            return;
        }

        string userName = Login1.UserName.ToString();//Login1.UserName.ToUpper();不再将用户名转为大小之后进行验证 editby zhongjian 20090924

        bool userExist = true;

        //验证是否是管理员
        if (SystemConfig.AdminUser.CompareTo(userName) != 0)
        {
            string strSelectSys = string.Empty;
            if (Session["SelectSys"] != null)
                strSelectSys = Session["SelectSys"].ToString();
            userExist = SysUserHandle.CheckUserExist(userName, strSelectSys);

            if (!userExist)
            {
                labMsg.Text = "用户名不存在！";
                return;
            }
            else
            {
                if (!SysUserHandle.UserIsvalid(userName))
                {
                    labMsg.Text = "该用户还未通过审核！";
                    return;
                }
                else//用户验证成功
                {
                    Session["title"] = "国家测绘地理信息局业务申报系统";
                    RecordLog(userName);
                }
            }
        }
        else
        {
            Session["title"] = "国家测绘地理信息局业务申报系统后台维护";
        }
    }

    /// <summary>
    /// Handles the LoggedIn event of the Login1 control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Login1_LoggedIn(object sender, EventArgs e)
    {
        Session.Remove("Menu");

        //对预审标志初始化
        Session["PromiseFlag"] = string.Empty;
        Session["UserID"] = Login1.UserName.ToString();
        Session["LoginUserName"] = SysUserHandle.GetUserNameByUserID(Login1.UserName.ToString());
    }
    #endregion

    #region 记录登录日志
    /// <summary>
    /// 记录登录日志
    /// </summary>
    /// <param name="UserName"></param>
    private void RecordLog(string UserName)
    {
        string UserIP = Request.UserHostName.ToString();
        SystemLogs wyLog = new SystemLogs();
        wyLog.Inputlog(UserName, "登录外网申报系统", string.Empty, UserIP);
    }
    #endregion 

    #region 办件统计

    /// <summary>
    /// 正在办理
    /// </summary>
    /// <returns></returns>
    protected string GetInstanceCount()
    {
        return strInstance;
    }

    /// <summary>
    /// 尚未提交
    /// </summary>
    /// <returns></returns>
    protected string GetSubmitCount()
    {
        return strNoSubmit;
    }

    /// <summary>
    /// 通知个数
    /// </summary>
    /// <returns></returns>
    protected string GetMessageCount()
    {
        return strTongZhi;
    }
    #endregion
}
