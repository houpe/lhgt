﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using SbBusiness.User;
using SbBusiness;

public partial class Wsbs_AppearOtherResouce : System.Web.UI.Page
{
    SysUserRightRequest rightRequest = new SysUserRightRequest();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindData();
            BindSerialAtt();
        }
    }

    /// <summary>
    /// 绑定数据源,绑定数据
    /// </summary>
    /// <!--update zhongjian 20090923-->
    private void BindData()
    {
        if (!string.IsNullOrEmpty(Request["id"]))
        {
            DataTable dtSource = rightRequest.GetAllRightById(Request["id"].ToString());
            LabelFlow.Text = dtSource.Rows[0]["SERIAL_NAME"].ToString();
            txtNotes.Text = dtSource.Rows[0]["NOTES"].ToString();
        }
    }

    /// <summary>
    /// 绑定流程权限申请时的附件信息
    /// </summary>
    /// <!--addby zhongjian 20090923-->
    private void BindSerialAtt()
    {
        if (!string.IsNullOrEmpty(Request["id"]))
        {
            string strFileName = string.Empty;
            string strID = string.Empty;
            string strFileType = string.Empty;
            string strPath = string.Empty;
            System.Text.StringBuilder strFileAtt = new System.Text.StringBuilder();
            DataTable dtSource = rightRequest.GetSerialAtt(Request["id"].ToString());
            if (dtSource.Rows.Count > 0)
            {
                for (int i = 0; i < dtSource.Rows.Count; ++i)
                {
                    strFileName = dtSource.Rows[i]["filename"].ToString();
                    strFileType = dtSource.Rows[i]["filetype"].ToString();
                    strID = dtSource.Rows[i]["id"].ToString();
                    strFileName = strFileName + "." + strFileType+";";
                    strPath = string.Format("<a href='../Common/AppearOtherResouce.aspx?type=serial&id={0}'>{1}</a>&nbsp;&nbsp;", strID, strFileName);
                    strFileAtt.Append(strPath);
                }
                lblFileAtt.Text = strFileAtt.ToString();
            }
        } 
    }
}
