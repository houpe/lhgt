﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using SbBusiness;
using System.Data;
using System.Configuration;
using SbBusiness.Wsbs;

public partial class Wsbs_JggsPage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindData();
        }
    }

    /// <summary>
    /// Binds the data.
    /// </summary>
    private void BindData()
    {
        SerialInstance instanceOperation = new SerialInstance();

        string strUserId = string.Empty;
        if (Session["UserId"] != null)
        {
            strUserId = Session["UserId"].ToString();
        }

        DataTable dtSource = instanceOperation.GetInstanceResultInfo(txtIid.Text,
            txtSQQSSJ.Text,txtSQZZSJ.Text, Request["wid"]);

        this.CustomGridView1.DataSource = dtSource;
        this.CustomGridView1.PageSize = SystemConfig.PageSize;
        this.CustomGridView1.RecordCount = dtSource.Rows.Count;
        this.CustomGridView1.DataBind();   
    }

    /// <summary>
    /// Handles the OnLoadData event of the CustomGridView1 control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void CustomGridView1_OnLoadData(object sender, EventArgs e)
    {
        BindData();
    }

    /// <summary>
    /// Handles the OnClick event of the btnQuery control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void btnQuery_OnClick(object sender, EventArgs e)
    {
        BindData();
    }
}
