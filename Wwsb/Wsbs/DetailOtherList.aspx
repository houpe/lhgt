﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DetailOtherList.aspx.cs"
    Inherits="Wsbs_DetailOtherList" %>

<%@ Register Src="~/UserControls/MsgPlate.ascx" TagName="MsgPlate" TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/ucPageFoot.ascx" TagName="ucPageFoot" TagPrefix="uc2" %>
<%@ Register Src="~/UserControls/ucPageHead.ascx" TagName="ucPageHead" TagPrefix="uc3" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
        <%=PageTitle %></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table border="0" align="center" cellpadding="0" cellspacing="0" class="page_box">
            <tr>
                <td colspan="3">
                    <uc3:ucPageHead ID="ucPageHead1" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="3" valign="top" style="text-align: center;">
                    <div class="end_bar">
                        <asp:Label ID="lblTypeName" runat="server" Text="信息公告"></asp:Label></div>
                    <div style="color: Black">
                        <defineControls:CustomGridView ID="cgvOther" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                            ShowItemNumber="true" SkinID="List" CheckboxAutoPostBack="False" CustomImportToExcel="false"
                            CustomSortExpression="" DeleteToolTipOfCol="-1" HideTextLength="20" ShowCmpAndCancel="False"
                            ShowGridViewSelect="List" ShowHideColModel="None" ShowImportButton="False" ShowTreeView="False"
                            SelectOrDelete="True" AllowSortingAscImgCss="" AllowSortingDescImgCss="" Width="1000"
                            OnOnLoadData="cgvOther_OnLoadData">
                            <Columns>
                                <asp:TemplateField HeaderText="标题" ItemStyle-Height="20px">
                                    <ItemTemplate>
                                        <a href="detail.aspx?id=<%# Eval("id")%>&type=<%=Request["Type"] %>" target="_blank">
                                            <%# Eval("title")%></a>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="CreateTime" HeaderText="发布日期" />
                            </Columns>
                        </defineControls:CustomGridView>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <uc2:ucPageFoot ID="ucPageFoot1" runat="server" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
