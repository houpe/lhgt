﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AppearXggd.aspx.cs" Inherits="Wsbs_AppearXggd" %>

<%@ Register Src="../UserControls/Calendar.ascx" TagName="Calendar" TagPrefix="uc3" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head2" runat="server">
    <title>相关规定</title>

    <script type="text/javascript">
        function ProgressView(url) {
            window.open(url, "proWin", "width=700,height=450,top=50");
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div style="text-align: center; display: none">
        <table class="tab_Content">
            <tr>
                <td>
                    发布标题:
                </td>
                <td>
                    <asp:TextBox ID="txtTitle" runat="server" SkinID="InputGreen"></asp:TextBox>
                </td>
                <td>
                    发布起始日期:
                </td>
                <td>
                    <uc3:Calendar ID="txtQSSJ" runat="server" />
                </td>
                <td>
                    发布中止日期:
                </td>
                <td>
                    <uc3:Calendar ID="txtZZSJ" runat="server" />
                </td>
                <td>
                    <asp:Button ID="btnQuery" Text="查询" OnClick="btnQuery_OnClick" runat="server" />
                </td>
            </tr>
        </table>
    </div>
    <br />
    <div style="text-align: center;">
        <defineControls:CustomGridView ID="dgXggd" runat="server" AllowPaging="True" AutoGenerateColumns="false"
            ShowItemNumber="true" SkinID="List" CheckboxAutoPostBack="False" CustomImportToExcel="false"
            CustomSortExpression="" DeleteToolTipOfCol="-1" HideTextLength="20" ShowCmpAndCancel="False"
            ShowGridViewSelect="List" ShowHideColModel="None" ShowImportButton="False" ShowTreeView="False"
            SelectOrDelete="True" AllowSortingAscImgCss="" AllowSortingDescImgCss="" Width="700px"
            OnOnLoadData="dgXggd_OnLoadData" OnRowCreated="dgXggd_RowCreated">
            <Columns>
                <asp:TemplateField HeaderText="标题">
                    <ItemTemplate>
                        <a href="detail.aspx?Type=2&id=<%# Eval("id")%>" target="_blank">
                            <%# Eval("title")%></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="time" HeaderText="发布时间" />
            </Columns>
        </defineControls:CustomGridView>
    </div>
    </form>
</body>
</html>
