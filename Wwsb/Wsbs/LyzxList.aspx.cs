﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SbBusiness.Wsbs;

using SbBusiness.User;
using SbBusiness;

using SbBusiness.Menu;
using Business.Admin;

public partial class Wsbs_LyzxList : System.Web.UI.Page
{
    SerialInstance SerialIO = new SerialInstance();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindLyzxList();
            BindQuertionList();
        }
    }

    protected void cgvLYZX_OnLoadData(object sender, EventArgs e)
    {
        BindLyzxList();
    }

    /// <summary>
    /// 绑定留言咨询列表
    /// </summary>
    /// <!--addby zhongjian 20091028-->
    public void BindLyzxList()
    {
        DataTable dtSource = SerialIO.GetLyzxInfo("", true, 0, 50,"","","","","");
        this.cgvLyzx.DataSource = dtSource;
        this.cgvLyzx.PageSize = SystemConfig.PageSize;
        this.cgvLyzx.RecordCount = dtSource.Rows.Count;
        this.cgvLyzx.DataBind();
    }
    protected void btnQuery_OnClick(object sender, EventArgs e)
    {
        bool bFlag=true;
        if (txtMsgCode.Text.Trim() != "")
            bFlag = false;
        DataTable dtSource = SerialIO.GetLyzxInfo("", bFlag, 0, 50, ddlWtlx.SelectedValue, txtTitle.Text.Trim(), txtMsgCode.Text.Trim(), txtFromDate.Value, txtToDate.Value);
        this.cgvLyzx.DataSource = dtSource;
        this.cgvLyzx.PageSize = SystemConfig.PageSize;
        this.cgvLyzx.RecordCount = dtSource.Rows.Count;
        this.cgvLyzx.DataBind();
    }
    /// <summary>
    /// 绑定问题类型
    /// </summary>
    /// <!--addby zhongjian 20091118-->
    public void BindQuertionList()
    {
        DictOperation doTemp = new DictOperation();
        DataTable dtTable = doTemp.GetDictFromName("问题类型");
        ddlWtlx.Items.Clear();
        ddlWtlx.Items.Add(new ListItem("请选择问题类型...", "0"));
        foreach (DataRow row in dtTable.Rows)
        {
            ddlWtlx.Items.Add(new ListItem(row["KEYVALUE"].ToString(), row["KEYCODE"].ToString()));
        }
    }
}
