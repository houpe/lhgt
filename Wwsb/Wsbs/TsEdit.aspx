﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TsEdit.aspx.cs" Inherits="Wsbs_TsEdit" %>

<%@ Register Src="../UserControls/Calendar.ascx" TagName="Calendar" TagPrefix="uc1" %>
<%@ Register Src="../UserControls/TextBoxWithValidator.ascx" TagName="TextBoxWithValidator"
    TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>投诉管理</title>
     <script language="javascript" type="text/javascript" src="../ScriptFile/Regex.js"></script>
</head>
<body>
    <form id="form1" runat="server">

        <script type="text/javascript">
		function openwin() 
		{
		    window.open('SerialInfoList.aspx?wid=<%=Request["wid"]%>', 'openInstance', 'width=620,height=420,left=50,top=100');
		}
		
		function SetControlsValue(strIid)
		{
			document.getElementById("<%=txtTsiid.ClientID%>").value = strIid;
		}
   	   
        </script>

        <br />
        <div style="text-align: center;">
            <table style='width: 700px; height: 200px' border="0" cellspacing="0" cellpadding="0"
                class="inputtable">
                <tr>
                    <td colspan="2">
                        投诉管理
                    </td>
                </tr>
                <tr>
                    <td>
                        投诉人</td>
                    <td align="left">
                        <uc2:TextBoxWithValidator ID="txtTsr" runat="server" Type="Required" ErrorMessage="投诉人不能为空" />
                    </td>
                </tr>
                <tr>
                    <td>
                        时间</td>
                    <td align="left">
                        <uc1:Calendar ID="txtTime" Required="true" ErrorMessage="时间不能为空" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        被投诉对象</td>
                    <td align="left">
                        <asp:TextBox runat="server" ID="txtBtsdx"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        投诉案件</td>
                    <td align="left">
                        <asp:TextBox runat="server" ID="txtTsiid"></asp:TextBox>
                        <input type="button" id="Save" value="..." onclick="openwin()">
                    </td>
                </tr>
                <tr>
                    <td>
                        投诉内容</td>
                    <td align="left">
                        <uc2:TextBoxWithValidator ID="txtTsContent" TextMode="MultiLine" Height="90%" Width="98%"
                            runat="server" Type="Required" ErrorMessage="投诉内容不能为空" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Button runat="server" ID="btnSave" Text="保存" SkinID="LightGreen" OnClick="btnSave_OnClick" />
                        <asp:Button ID="btnReturn" SkinID="LightGreen" runat="server" Text="返回" OnClick="btnReturn_Click"
                            CausesValidation="False" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
<script language="javascript" type="text/javascript" src="../ScriptFile/AutoHeightAndWidthDouble.js"></script>

