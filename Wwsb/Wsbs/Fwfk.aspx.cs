﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SbBusiness.Wsbs;

using SbBusiness.User;
using SbBusiness;

using SbBusiness.Menu;
using Business.Admin;

public partial class Wsbs_Fwfk : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string action = Request.QueryString["action"];

        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                PersistenceControl1.PageControl = this.form1;
                PersistenceControl1.KeyValue = Request.QueryString["id"];
                PersistenceControl1.Status = "edit";
                PersistenceControl1.Render();
            }

            if (Session["LoginUserName"] != null)
            {
                txtName.Text = Session["LoginUserName"].ToString();
            }
            BindDepList();
            BindFlowName();
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (txtNotes.Text.Trim().Length > 3000)
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "ok", "<script>alert('输入的内容不能太多。')</script>");
            return;
        }
        if (string.IsNullOrEmpty(txtNotes.Text.Trim()))
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "ok", "<script>alert('请认真输入建议或意见内容。')</script>");
            return;
        }
        if (string.IsNullOrEmpty(txtName.Text.Trim()))
        {
            txtName.Text = "匿名";
        }
        PersistenceControl1.PageControl = this.form1;
        PersistenceControl1.Update();
        Page.ClientScript.RegisterStartupScript(this.GetType(), "ok", "<script>window.alert('保存成功。')</script>");
    }

    protected void btnReturn_Click(object sender, EventArgs e)
    {
        this.Response.Write("<script>window.close();</script>");
    }

    /// <summary>
    /// 绑定咨询部门
    /// </summary>
    /// <!--addby zhongjian 20091201-->
    public void BindDepList()
    {
        DepartHandle dhTemp = new DepartHandle();
        DataTable dtTable = dhTemp.GetSecondDepart();
        ddlSlbm.Items.Clear();
        ddlSlbm.Items.Add(new ListItem("请选择审批部门...", "0"));
        foreach (DataRow row in dtTable.Rows)
        {
            ddlSlbm.Items.Add(new ListItem(row["depart_name"].ToString(), row["departid"].ToString()));
        }
    }

    /// <summary>
    /// 绑定所有流程名称
    /// </summary>
    /// <!--addby zhongjian 20091214-->
    public void BindFlowName()
    {
        SysUserRightRequest urrTemp = new SysUserRightRequest();
        DataTable dtTable = urrTemp.GetFlowList();
        ddlFlow.Items.Clear();
        ddlFlow.Items.Add(new ListItem("请选择事项流程...", "0"));
        foreach (DataRow row in dtTable.Rows)
        {
            ddlFlow.Items.Add(new ListItem(row["KEYVALUE"].ToString(), row["KEYVALUE"].ToString()));
        }

        if (Request["flow"] != null && Request["flow"] != "-1")
            ddlFlow.SelectedIndex = int.Parse(Request["flow"].ToString());
        else if (Session["flowtype"] != null)//设置默认流程别名 addby zhongjian 20100319
        {
            string strFlwtype = string.Format("{0}", Session["flowtype"]);
            for (int i = 0; i < ddlFlow.Items.Count; ++i)
            {
                if (strFlwtype == ddlFlow.Items[i].ToString())
                {
                    ddlFlow.SelectedIndex = i;
                    ddlFlow.Enabled = false;
                    break;
                }
            }
        }
    }
}
