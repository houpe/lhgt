﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="submit.aspx.cs" Inherits="Wsbs_submit" Title="各业务状态查询" %>

<%@ Register Src="../UserControls/Calendar.ascx" TagName="Calendar" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">
    <script type="text/javascript" src="../ScriptFile/jquery.js"></script>
    <script type="text/javascript" src="../ScriptFile/jquery.easyui.min.js"></script>
    <script type="text/javascript">
        //关闭窗口
        function CloseDivDlg() {
            $('#dlgConfig').window('close');
        }

        //打开内嵌网页的通用方法1（window）
        function openDivDlg(strTitle, strUrl, divWidth, divHeight) {
            //增加参数queryid
            $('#dlgConfig').window({
                title: strTitle, width: divWidth, height: divHeight
            });

            $('#iframeConfig').attr("src", strUrl);
            // $('#dlgConfig').window({ maximized: true });
            $('#dlgConfig').window('open');
        }

        var flag = '<%= Request["flag"]%>';
        function OpenReasonDiag(striid) {
            //转向网页的地址;
            var url = "SubmitRemark.aspx?iid=" + striid + "&flag=" + flag;

            var name = "详细内容";                 //网页名称，可为空;
            var iWidth = 400;                          //弹出窗口的宽度;
            var iHeight = 290;                         //弹出窗口的高度;

            openDivDlg(name, url, iWidth, iHeight);
        }
    </script>
    <!--  信息查看  -->
    <div id="dlgConfig" class="easyui-window" title="信息查看" closed="true" modal="false"
        minimizable="false" style="width: 400px; height: 300px; background: #fafafa;">
        <iframe id="iframeConfig" scrolling="no" frameborder="0" src="" style="width: 98%;
            height: 100%;"></iframe>
    </div>
    <div style="text-align: center">
        申请编号：
        <asp:TextBox runat="server" ID="txtIID"></asp:TextBox>
        <asp:Button runat="server" Text="查询" ID="btnQuery" OnClick="btnQuery_Click" />
        <br />
        <defineControls:CustomGridView ID="CustomGridView1" runat="server" AllowPaging="True"
            AutoGenerateColumns="false" ShowItemNumber="true" SkinID="List" CheckboxAutoPostBack="False"
            DeleteToolTipOfCol="-1" HideTextLength="20" ShowHideColModel="None" SelectOrDelete="True"
            Width="100%" AllowOnMouseOverEvent="true" OnOnLoadData="CustomGridView1_OnLoadData"
            OnRowCreated="CustomGridView1_RowCreated" EmptyDataText=".">
            <Columns>
                <asp:BoundField DataField="flowtype" HeaderText="事项名称" />
                <asp:TemplateField HeaderText="申请编号">
                    <ItemTemplate>
                        <%# GetUrl(Container)%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="tm" HeaderText="图(书)名" Visible="false" />
                <asp:BoundField DataField="slh" HeaderText="受理号" Visible="false" />
                <asp:BoundField DataField="inputdate" HeaderText="提交时间" />
                <asp:BoundField DataField="edittime" HeaderText="接收时间" />
                <asp:BoundField DataField="FINISH_TIME" HeaderText="完成时间" />
                <asp:TemplateField HeaderText="审批状态" Visible="false">
                    <ItemTemplate>
                        <%# GetQuery(Container)%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="补正原由" Visible="false">
                    <ItemTemplate>
                        <a href="#" onclick='OpenReasonDiag(<%# Eval("iid")%>)'>
                            <%# GetRemark(Container)%></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="操作" Visible="false">
                    <ItemTemplate>
                        <asp:HyperLink ID="hlDelete" runat="server">删除</asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </defineControls:CustomGridView>
    </div>
</asp:Content>
