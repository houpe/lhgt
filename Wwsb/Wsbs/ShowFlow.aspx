﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ShowFlow.aspx.cs" Inherits="Wsbs_ShowFlow" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>上传流程图查看</title>
</head>
<body>
    <form id="form1" runat="server">
    <div id="divImg">
            <defineControls:ImageView ID="ImageView1" Visible="false" runat="server" />
        </div>
        <div id="divObj" style="display:none;">
            <object classid="clsid:00460182-9E5E-11d5-B7C8-B8269041DD57" id="oframe" width="100%"
                height="100%">
                <param name="BorderStyle" value="1"/>
                <param name="TitlebarColor" value="52479"/>
                <param name="TitlebarTextColor" value="0"/>
                <param name="Menubar" value="1"/>
            </object>
        </div>
    </form>
</body>
</html>
