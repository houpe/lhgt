﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using SbBusiness;
using SbBusiness.PubInfo;

public partial class Wsbs_detail : System.Web.UI.Page
{
    public string txt, title, time;
    protected string PageTitle;
    private void Page_Load(object sender, System.EventArgs e)
    {
        if (!this.Page.IsPostBack)
        {
            if (Request["id"] != null)
            {
                txt = GetTxt(Request["id"]);
            }
            string strType = Request["Type"];
            if (strType == "1")
                strType = "常见问题";
            else if (strType == "2")
                strType = "相关规定";
            else
                strType = "信息公告";
            PageTitle = strType;
        }
    }

    private string GetTxt(string id)
    {
        string temp = "";

        InfoManage imTemp = new InfoManage();
        DataTable dt = imTemp.GetPubInfoById(id);

        if(dt.Rows.Count>0)
        {
            DataRow dr = dt.Rows[0];
            temp = dr["content"].ToString();
            title = dr["title"].ToString();
            time = dr["time"].ToString();
        }

        return temp;
    }
}


