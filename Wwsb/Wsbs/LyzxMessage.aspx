﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LyzxMessage.aspx.cs" Inherits="Wsbs_LyzxMessage" %>

<%@ Register Src="~/UserControls/MsgPlate.ascx" TagName="MsgPlate" TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/ucPageFoot.ascx" TagName="ucPageFoot" TagPrefix="uc2" %>
<%@ Register Src="~/UserControls/ucPageHead.ascx" TagName="ucPageHead" TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>留言咨询信息</title>
</head>
<body>
    <form id="form1" runat="server">
    <table border="0" align="center" cellpadding="0" cellspacing="0" class="page_box">
        <tr>
            <td>
                <uc3:ucPageHead ID="ucPageHead1" runat="server" />
                <div class="end_bar">
                    留言咨询信息</div>
            </td>
        </tr>
        <tr>
            <td valign="top" style="text-align: center;">
                <div style="width: 98%;">
                    <div class="page_bar" style="width: 100%; vertical-align: top">
                        <div class="tdbox" style="color: Dodgerblue; font-weight: bold;">
                            <table>
                                <tr>
                                    <td>
                                        主题：<asp:Label ID="lblTitle" runat="server" Text=""></asp:Label>
                                        【日期：<asp:Label ID="lblData" runat="server" Text=""></asp:Label>
                                        &nbsp;留言人：<asp:Label ID="lblUser" runat="server" Text=""></asp:Label>】
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <br />
                    <div style="color: Dodgerblue; font-weight: bold; text-align:left; margin-left: 0px; width:80%">
                        <asp:Label ID="lblNotes" runat="server" Text=""></asp:Label>
                    </div>
                    <br />
                    <!--绑定回复信息-->
                    <%=BindMessageFK()%>
                </div>
            </td>
        </tr>
        <tr>
            <td style="text-align: center;">
                <a class="font1" href="#" onclick="self.close();">[关闭窗口]</a>
            </td>
        </tr>
        <tr>
            <td>
                <uc2:ucPageFoot ID="ucPageFoot1" runat="server" />
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hfmsshowtype" runat="server" />
    </form>
</body>
</html>
