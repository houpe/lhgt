﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AppearSerialInfo.aspx.cs"
    Inherits="Wsbs_AppearOtherResouce" %>

<%@ Register Src="~/UserControls/ucPageFoot.ascx" TagName="ucPageFoot" TagPrefix="uc2" %>
<%@ Register Src="~/UserControls/ucPageHead.ascx" TagName="ucPageHead" TagPrefix="uc3" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>业务申报权限申请详情</title>
    <script language="javascript" type="text/jscript">
        function Close() {
            window.parent.close();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server" style="text-align: center;">
    <div style="text-align: center;margin: 0px auto;" class="page_box">
        <uc3:ucPageHead ID="ucPageHead1" runat="server" />
        <div class="end_bar">
            <asp:Label ID="lblTypeName" runat="server" Text="业务申报理由信息"></asp:Label></div>
        <div style="color: Black">
            <table class="inputtable" style="width: 80%">
                <tr>
                    <td style="background-color: Snow; text-align: center; font-weight: bold;">
                        业务类型
                    </td>
                    <td style="text-align: left; vertical-align: middle;">
                        <asp:Label ID="LabelFlow" runat="server" Height="30" CssClass="label01"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="background-color: Snow; text-align: center; font-weight: bold;">
                        申请理由
                    </td>
                    <td style="text-align: left;">
                        <asp:TextBox ID="txtNotes" runat="server" ReadOnly="true" Width="100%" Height="300"
                            TextMode="MultiLine"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="background-color: Snow; text-align: center; font-weight: bold;">
                        附件信息
                    </td>
                    <td style="text-align: left;">
                        &nbsp;
                        <asp:Label ID="lblFileAtt" runat="server" Width="80%"></asp:Label>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Button ID="Button1" runat="server" Text="关闭" OnClientClick="return Close ();" />
                    </td>
                </tr>
            </table>
        </div>
        <uc2:ucPageFoot ID="ucPageFoot1" runat="server" />
    </div>
    </form>
</body>
</html>
