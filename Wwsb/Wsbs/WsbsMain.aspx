﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="WsbsMain.aspx.cs" Inherits="Wsbs_WsbsMain" EnableEventValidation="false" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">

    <script src="../ScriptFile/jquery.js" type="text/javascript"></script>

    <script src="../ScriptFile/AutoIframeHeight.js" type="text/javascript"></script>

    <script type="text/javascript">
        function setTab(i) {
            //构建参数
            $.ajax({
                url: 'AjaxServerResponse.ashx',
                data: "dataParam=" + i + ",<%=strParam%>",
                type: 'GET',
                dataType: 'text',
                contentType: 'application/x-www-form-urlencoded;charset=UTF-8',
                cache: false,
                async: false,
                timeout: 500,
                error: function () {
                    alert('Error loading Text document');
                },
                success: function (retUrl) {
                    var dialogflag = '<%=Request["DialogFlag"]%>'; //添加对话框显示类型参数
                    var flag = '<%=Request["flag"]%>'; //添加导航申报类型参数
                    var strFlowType = '<%=strFlowType%>';
                    retUrl += "&flowtype=" + strFlowType + "&type=" + i + "&DialogFlag=" + dialogflag + "&flag=" + flag;

                    if (retUrl.indexOf('flowname') == -1) {
                        var strFlowName = '<%=Session["flowname"]%>';
                        retUrl += '&flowname=' + encodeURI(strFlowName);
                    }
                    if (i == 3 || i == 7) {
                        //为加载底部说明页面,修改为加载模板页,因此直接跳转 editby zhongjian 20091102
                        window.location.href = retUrl;
                    }
                    else {
                        //如果是办理流程 由本页面打开
                        retUrl = retUrl + "&IsShowDialog=" + $("#hidShowType").attr("value");
                        $("#frmChild").attr("src", retUrl); 
                    }
                }
            });
        }

        //文档加载完成
        $(document).ready(function() {
            setTab('<%=Request["type"]%>');
        });
    </script>

    <input type="hidden" value="" id="hidShowType" runat="server" />
    <div style="text-align: left">
        <iframe src='' width='100%' onload="SetWinHeight(this);" id="frmChild" frameborder="0"
            scrolling="no"></iframe>
    </div>
</asp:Content>
