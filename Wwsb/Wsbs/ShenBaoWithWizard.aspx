﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ShenBaoWithWizard.aspx.cs" Inherits="Wsbs_ShenBaoWithWizard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">
    <script type="text/javascript" src="../ScriptFile/jquery.js"></script>
    <script type="text/javascript" src="../ScriptFile/AutoIframeHeight.js"></script>
    <script type="text/javascript">
        var currentUrl;
        var divPre, divNext;
        var isChange = false;
        var isSaveSuccess = true;
        var strFuJianUrl = "SbFileUploadNew.aspx";
        var strFlowType = encodeURI('<%=Request["flowtype"]%>');
        var strFlowName = encodeURI('<%=Request["flowname"]%>');
        var userid = encodeURI('<%=Session["UserID"]%>');
        var username = encodeURI('<%=ViewState["UserName"]%>');

        function GetIID()
        {
            return $("#<%=hidIId.ClientID %>").val(); //获取client的值
        }
        //消息提示框
        function ConfirmMsg(strInfo) {
            return confirm(strInfo);
        }

        //保存后的回调函数
        function SaveIId() {
            $.ajax({
                url: 'AjaxSaveForm.ashx',
                data: "dataIId=" + GetIID() + "&dataFlowname=" + strFlowName + "&flowType=" + strFlowType + "&userId=" + userid + "&userName=" + username,
                type: 'POST',
                dataType: 'text',
                cache: false,
                async: false,
                error: function () {
                    alert('服务器端处理程序报错');
                },
                success: function (data) {
                    if (data == "保存失败") {
                        alert('保存失败');
                    }
                    else {
                        if (currentUrl != "") {
                            $("#step" + divPre).hide();
                            $("#step" + divNext).fadeIn("fast");
                            $("#frmChild").attr("src", currentUrl);
                            isSaveSuccess = true;
                            isChange = false;
                        }
                    }
                }
            });
        }

        function SaveInfo() {
            if ($("#frmChild").attr("src").indexOf(strFuJianUrl) == -1) {
                if (isChange) {
                    //老保存方法
                    //frmChild.SaveAll(SaveIId);

                    //新保存方法//采用HtmlMainPage.aspx的方法
                    frmChild.SaveAll(false, SaveIId);
                }
                else {
                    alert("未经过任何改动，无需保存");
                }
            }
        }


        function SubmitInfo() {

            if ($("#frmChild").attr("src").indexOf(strFuJianUrl) == -1) {
                if (isChange) {
                    frmChild.SaveAll(false); //采用HtmlMainPage.aspx的方法
                    //frmChild.SaveAll(); //提交时,自动保存 addby zhongjian 20091218
                }
            }

            $.ajax({
                url: 'AjaxSubmitFlag.ashx',
                data: "dataIId=" + GetIID() + "&dataFlowname=" + strFlowName + "&flowType=" + strFlowType + "&bgflag=&yqflag=&userId=" + userid + "&userName=" + username,
                type: 'GET',
                dataType: 'text',
                cache: false,
                async: false,
                timeout: 1000,
                error: function() {
                    alert('请检查是否未上传材料或是否未保存表单');
                },
                success: function(data) {
                    if (data == "提交成功") {
                        alert('提交成功');
                        $("#saveform").attr("disabled", true);
                        $("#submitform").attr("disabled", true);
                    }
                    else
                        alert('请检查是否未上传材料或是否未保存表单');
                }
            });
        }

        //设置tabcontrol按钮的背景颜色,并判断是否进行过表单修改
        function SetIframeUrl() {
            if (isChange) //数据已修改
            {   
                var varEnableSave = $("#saveform").attr("disabled");
                //判断按钮是否可用
                if (varEnableSave == false || typeof(varEnableSave)=="undefined") {
                     isSaveSuccess = false;
                     SaveInfo();//有修改时自动保存
                }
            }
        }

        //控制翻页
        function loadnext(divout, divin, strUrl, strTitle) {
            //初始化表单
            var strTabName = strTitle;

            var strInitScript = "";
            if (strUrl.indexOf(strFuJianUrl) == -1) {//表单页面
                strInitScript = getInitScript(strTabName);
                $("#saveform").show();
            }
            else {//附件页面
                $("#saveform").hide();
            }

            if (strInitScript != "") {
                strUrl = strUrl + "&initScript=" + encodeURI(strInitScript);
            }
            strUrl += "&rdm=" + Math.random();

            //换页
            currentUrl = strUrl;
            divPre = divout;
            divNext = divin;
            if (divout < divin) {
                SetIframeUrl();
            }
            //实现切换效果
            if (isSaveSuccess) {
                $("#step" + divout).hide();
                $("#step" + divin).fadeIn("fast");
                $("#frmChild").attr("src", strUrl);   
            }
        }

        function openWizardUrl() {
            window.location.href = "ShenBao.aspx?IId=" + GetIID() + "&flowname=" + strFlowName+ "&flowType=" + strFlowType;
        }

        //获取初始值
        function getInitScript(strTabName) {
            var strInitScript = "";
            var varEnableSave = $("#submitform").attr("disabled");
            if (typeof(varEnableSave)=="undefined") {//只有初始添加时才加载
                $.ajax({
                    url: 'ajaxInitFormValue.ashx',
                    data: "flowname=" + strFlowName + "&tabName=" + strTabName+"&userId="+userid,
                    type: 'POST',
                    dataType: 'text',
                    async: false,
                    success: function (dataScript) {
                        if (dataScript != "") {
                            strInitScript = dataScript; //执行加载后的代码
                        }
                    }
                });
            }
            return strInitScript;
        }

        //页面加载时执行
        $(document).ready(function () {
            //$("#tabMenu td:first").click();

            var defaultUrl = $("#hidFirst").val();
            if (defaultUrl != "") {
                //初始化表单
                var strTabName = $("#hidFirst").attr("title");
                var strInitScript = "";

                if (defaultUrl.indexOf(strFuJianUrl) == -1) {//表单默认加载
                    strInitScript = getInitScript( strTabName);
                }
                if (strInitScript != "") {
                    defaultUrl = defaultUrl + "&initScript=" + encodeURI(strInitScript);
                }

                $("#frmChild").attr("src", defaultUrl);

            }
        });
        
        //打印
        function framePrint() {
            if ($("#frmChild").attr("src") != "") {
                window.open($("#frmChild").attr("src") + "&act=Print", "printfrm", "width=1,height=1");
            }
        }

        //打印预览
        function framePrintPreview() {
            if ($("#frmChild").attr("src") != "") {
                window.open($("#frmChild").attr("src") + "&act=PrintView", "printfrm");
            }
        }
    </script>
    <input type="hidden" value="" id="hidIId" runat="server" />
    <div>
        <%=strHtmlInfo %>
        <iframe name="frmChild" id="frmChild" width="800" onload="SetWinHeight(this);" frameborder="0"
            scrolling="no"></iframe>
    </div>
</asp:Content>
