﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="IndexPage.aspx.cs" Inherits="Wsbs_IndexPage" %>

<%@ Register Src="../UserControls/ucPageHead.ascx" TagName="ucPageHead" TagPrefix="uc1" %>
<%@ Register Src="../UserControls/ucPageFoot.ascx" TagName="ucPageFoot" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>国家测绘地理信息局行政许可网上办理大厅</title>
    <script type="text/javascript" src="../ScriptFile/jquery.js"></script>
    <script type="text/javascript" src="../ScriptFile/AutoIframeHeight.js"></script>
    <script src="../ScriptFile/Common/Jquery.Global.js" type="text/javascript"></script>

    <script type="text/javascript">
        //用户注册 addby zhongjian 20100112
        function clickRegister() {
            window.open("../UserRegister/UserReg.aspx");
        }
        //打开表格下载页面方法 addby zhongjian 20100113
        function DownLoad(strID) {
            window.location.href = "../Common/AppearOtherResouce.aspx?id=" + strID;
        }
        //打开在线申报 addby zhongjian 20100318
        function OpenShenBao() {
            var Url = $("#hidShenBaoUrl").attr("value");
            var flag = $("#hidPowerFlag").attr("value");
            if (flag == "true") {
                window.open(Url);
            }
            else if (flag == "false") {
                alert("您还没有申报该事项的权限.");
            }
            else if (flag == "no") {
                alert("登录之后才可能进行在线申报.");
            }
        }
        //打开流程查看
        function OpenWorkFlow() {
            var Url = $("#hidWorkFlowUrl").attr("value");
            window.open(Url);
        }
        function clickUrl(strUrl) {
            window.open(strUrl);
        }
        function Cleaner() {
            document.form1.txtIID.value = "";
            document.form1.txtName.value = "";
        }

        function OpenReasonDiag() {
            //转向网页的地址;
            var url = $("#hidWorkFlowUrl").attr("value");

            var name = "办理流程查看";                    //网页名称，可为空;
            var iWidth = 700;                          //弹出窗口的宽度;
            var iHeight = 400;                         //弹出窗口的高度;
            //获得窗口的垂直位置
            var iTop = (window.screen.availHeight - 30 - iHeight) / 2;
            //获得窗口的水平位置
            var iLeft = (window.screen.availWidth - 10 - iWidth) / 2;
            window.open(url, name, 'height=' + iHeight + ',,innerHeight=' + iHeight + ',width=' + iWidth + ',innerWidth=' + iWidth + ',top=' + iTop + ',left=' + iLeft + ',status=no,toolbar=no,menubar=no,location=no,resizable=no,scrollbars=0,titlebar=no');
        }


        //查询文本框验证 addby zhongjian 20100319
        var keyvalue = new Array("输入受理编号", "输入申请单位");
        function erase(q, n) {
            if (q.value == keyvalue[n]) {
                q.value = "";
            }
        }
        function recovery(q, n) {
            if (q.value == "") {
                q.value = keyvalue[n];
            }
        }

        //添加行政许可查询功能 add by zhongjian 20100317
        function OpenQueryXuKe() {
            var flow = document.form1.ddlFlow.selectedIndex;
            var iid = document.form1.txtIID.value;
            var name = document.form1.txtName.value;
            var fromdate = "";
            var todate = "";
            var strUrl = "XingZhengXuKe.aspx";
            if (iid == keyvalue[0])
                iid = "";
            if (name == keyvalue[1])
                name = "";
            if ((iid == "" && name == "") || (iid == keyvalue[0] && name == "") || (iid == "" && name == keyvalue[1]) || (iid == keyvalue[0] && name == keyvalue[1])) {
                alert("请输入查询条件");
            }
            else {
                strUrl += "?flow=" + flow + "&iid=" + iid + "&name=" + name + "&fromdate=" + fromdate + "&todate=" + todate + "";
                window.open(strUrl);
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <input type="hidden" value="" id="hidWname" runat="server" />
        <input type="hidden" value="" id="HidWType" runat="server" />
        <input type="hidden" value="" id="hidIId" runat="server" />
        <input type="hidden" value="" id="hidPowerFlag" runat="server" />
        <input type="hidden" value="" id="hidShenBaoUrl" runat="server" />
        <input type="hidden" value="" id="hidWorkFlowUrl" runat="server" />

        <div style="text-align: center; width: 100%">
            <div style="text-align: center; margin: 0px auto;" class="page_box">

                <div id="tdTop" style="width: 100%;">
                    <uc1:ucPageHead ID="ucPageHead1" runat="server" />
                </div>
                <div style="text-align: center">
                    <table border="0" align="center" cellpadding="0" cellspacing="0" class="page_box"
                        style="height: 100%">
                        <tr>
                            <td width="170" valign="top">
                                <div class="page_left_box" id="div_Login" runat="server" style="margin-bottom: 3px; width:100%">
                                    <div class="page_left_bar">
                                        用户登录
                                    </div>
                                    <asp:Login ID="Login1" runat="server" DestinationPageUrl="~/Wsbs/IndexPage.aspx"
                                        DisplayRememberMe="False" TitleText="" PasswordLabelText="密     码：" UserNameLabelText="用户名："
                                        SkinID="Loging" OnLoggedIn="Login1_LoggedIn" OnLoginError="Login1_LoginError"
                                        OnLoggingIn="Login1_LoggingIn">
                                        <LayoutTemplate>
                                            <table style="border-color: Black; text-align: left; margin-bottom: 6px;">
                                                <tr>
                                                    <td align="left">
                                                        <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">用户名</asp:Label>
                                                    </td>
                                                    <td style="text-align: left;">
                                                        <asp:TextBox ID="UserName" runat="server" Width="100" Height="16px"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                                                            ErrorMessage="必须填写“用户名”。" ToolTip="必须填写“用户名”。" ValidationGroup="Login1">*</asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password">密&nbsp;&nbsp;码</asp:Label>
                                                    </td>
                                                    <td style="text-align: left">
                                                        <asp:TextBox ID="Password" runat="server" TextMode="Password" Width="100" Height="16px"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                                                            ErrorMessage="必须填写“密码”。" ToolTip="必须填写“密码”。" ValidationGroup="Login1">*</asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">验证码
                                                    </td>
                                                    <td style="text-align: left;" colspan="2">
                                                        <asp:TextBox ID="txtCheckCode" runat="server" Width="40px" Height="16px"></asp:TextBox>
                                                        <img alt="验证码" src="../CheckCode.aspx" width="55px" height="22px" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: center; margin-bottom: 3px;" colspan="2">
                                                        <asp:Button ID="LoginButton" runat="server" TabIndex="0" CommandName="Login" Text="登录"
                                                            ValidationGroup="Login1" />
                                                        <input id="btnRegister" onclick="clickRegister()" type="button" class="button" value="注册" />
                                                        <asp:Label ID="labMsg" runat="server" ForeColor="Red"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </LayoutTemplate>
                                    </asp:Login>
                                </div>
                                <div class="page_left_box" id="div_Sheet" runat="server">
                                    <div class="page_left_bar">
                                        办件情况一览
                                    </div>
                                    <table width="90%" border="0" cellspacing="0" cellpadding="0" style="margin-top: 8px; margin: 8px;">
                                        <tr style="height: 20px">
                                            <td>类型
                                            </td>
                                            <td>数量
                                            </td>
                                        </tr>
                                        <tr style="height: 20px">
                                            <td>
                                                <a href='Tongzhi.aspx?query=1' target="_blank">未读通知</a>
                                            </td>
                                            <td>
                                                <%= GetMessageCount() %>
                                            </td>
                                        </tr>
                                        <tr style="height: 20px">
                                            <td>
                                                <a href='submit.aspx?flag=1' target="_blank">正在办理</a>
                                            </td>
                                            <td>
                                                <%= GetInstanceCount() %>
                                            </td>
                                        </tr>
                                        <tr style="height: 20px">
                                            <td>
                                                <a href='submit.aspx?flag=0' target="_blank">尚未提交</a>
                                            </td>
                                            <td>
                                                <%= GetSubmitCount() %>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="page_left_box">
                                    <div class="page_left_bar">
                                        办理情况查询
                                    </div>
                                    <table width="98%" border="0" cellspacing="0" cellpadding="0" style="margin-top: 8px; margin-bottom: 6px;">
                                        <tr style="height: 25px; display: none">
                                            <td>办理事项
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlFlow" runat="server" Width="220px">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="height: 25px">
                                            <td style="text-align: center;">
                                                <input id="txtIID" name="txtIID" type="text" value="输入受理编号" onfocus="erase(this,0);"
                                                    onblur="recovery(this,0);" style="width: 140px;" />
                                            </td>
                                        </tr>
                                        <tr style="height: 25px">
                                            <td style="text-align: center;">
                                                <input id="txtName" name="txtName" type="text" value="输入申请单位" onfocus="erase(this,1);"
                                                    onblur="recovery(this,1);" style="width: 140px;" />
                                            </td>
                                        </tr>
                                        <tr style="height: 25px">
                                            <td style="text-align: center;" colspan="2">
                                                <input id="btnQuery" onclick="OpenQueryXuKe()" type="button" class="button" value="查询" />
                                                <input id="btnRegister" onclick="Cleaner()" type="button" class="button" value="重置" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <img src="../App_Themes/SkinFile/Images/page_input_01.gif" border="0" onclick="OpenShenBao();"
                                    style="margin-top: 2px;" alt="" class="left_img" />
                                <img src="../App_Themes/SkinFile/Images/page_input_02_.gif" border="0" onclick="clickUrl('Lyzx.aspx');"
                                    style="margin-top: 0px;" alt="" class="left_img" />
                                <img src="../App_Themes/SkinFile/Images/page_input_03_.gif" border="0" onclick="clickUrl('Fwfk.aspx');"
                                    style="margin-top: 0px;" alt="" class="left_img" />
                            </td>
                            <td width="620" valign="top">
                                <iframe id="frmChild" src="BsznAppear.aspx?wid=<%=strWid %>" width="100%" onload="SetWinHeight(this);"
                                    frameborder="0" scrolling="no"></iframe>
                            </td>
                            <td width="250" valign="top">

                                <div class="page_right_box2">
                                    <div class="page_right_bar_ico01">
                                        结果公示
                                    </div>
                                </div>
                                <div id="demo" style="width: 100%; height: 150px;">
                                    <div id="demo1">
                                        <asp:Repeater ID="rptResults" runat="server">
                                            <ItemTemplate>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="background-color: #F1FAFF;">
                                                    <tr>
                                                        <td style="text-align: left; width: 5%">
                                                            <img alt="" src="../App_Themes/SkinFile/Images/arrow2.gif" />
                                                        </td>
                                                        <td style="text-align: left; height: 20px; width: 75%;">
                                                            <a href='XingZhengXuKe.aspx?iid=<%#Eval("受理编号") %>' target="_blank" style="text-align: left;">
                                                                <%# DataBinder.Eval(Container.DataItem, "申请单位")%></a>
                                                        </td>
                                                        <td style="text-align: right; width: 20%">
                                                            <%# DataBinder.Eval(Container.DataItem, "办件状态")%>&nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                    <div id="demo2">
                                    </div>
                                </div>

                                <div class="page_right_box2" style="text-align: left; border: 1px;">
                                    <div class="page_right_bar_ico02" style="height:35px;">
                                        表格下载
                                    </div>
                                    <%--<div class="page_bar_ul" style=" background-color:Blue;">--%>
                                    <ul class="page_bar_ul">
                                        <asp:Repeater ID="rptFileUpLoad" runat="server">
                                            <ItemTemplate>
                                                <li><a href="#" onclick="DownLoad('<%# DataBinder.Eval(Container.DataItem, "id")%>')">
                                                    <%# DataBinder.Eval(Container.DataItem, "RESOURCE_NAME")%></a></li>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </ul>
                                </div>

                                <div class="page_right_box2" style="text-align: left; border: 1px;">
                                    <div class="page_right_bar_ico03">
                                        相关规定
                                    </div>
                                    <ul class="page_bar_ul">
                                        <asp:Repeater ID="rptAbout" runat="server">
                                            <ItemTemplate>
                                                <li><a href="detail.aspx?Type=2&id=<%# Eval("id")%>" target="_blank">
                                                    <%# Eval("title")%></a></li>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </ul>
                                </div>

                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center" valign="top">
                                <img src="../App_Themes/SkinFile/Images/page_end.gif" alt="" width="437" height="69"
                                    border="0" onclick="OpenReasonDiag();" class="left_img" />
                            </td>
                        </tr>
                    </table>
                </div>

                <div id="divBottom">
                    <uc2:ucPageFoot ID="ucPageFoot1" runat="server" />
                </div>
            </div>
        </div>
        <script type="text/javascript">
            scrollText();
        </script>
    </form>
</body>
</html>
