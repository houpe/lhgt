﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="detail.aspx.cs" Inherits="Wsbs_detail" %>

<%@ Register Src="~/UserControls/ucPageFoot.ascx" TagName="ucPageFoot" TagPrefix="uc2" %>
<%@ Register Src="~/UserControls/ucPageHead.ascx" TagName="ucPageHead" TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
        <%=PageTitle %></title>
</head>
<body>
    <form id="Form1" method="post" runat="server">
    <div id="tdTop" style="margin: 0px auto; text-align: center;" class="page_box">
        <uc3:ucPageHead ID="ucPageHead1" runat="server" />
    </div>
    <div style="margin: 0px auto;text-align: center;" class="page_box">
        <div class="detbar_fill">
            <div class="detbar_left">
                <div class="detbar_right">
                    <%=title%></div>
            </div>
        </div>
        <div class="detbar_box">
            <table align="center" class="tablesynopsis">
                <tr>
                    <td>
                        <%=txt%>
                    </td>
                </tr>
            </table>
        </div>
        <div>
            <a class="font1" href="#" onclick="self.close();">[关闭窗口]</a></div>
    </div>
    <div style="margin: 0px auto; text-align: center;" class="page_box">
        <uc2:ucPageFoot ID="ucPageFoot1" runat="server" />
    </div>
    </form>
</body>
</html>
