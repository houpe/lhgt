﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SerialQuery.aspx.cs" Inherits="Wsbs_SerialQuery" %>

<%@ Register Src="../UserControls/Calendar.ascx" TagName="Calendar" TagPrefix="uc3" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head2" runat="server">
    <title>申报案件查询</title>

    <script src="../ScriptFile/Calendar.js" type="text/javascript"></script>

    <script type="text/javascript">
        function ProgressView(url) {
            //window.location.href = url;
            window.open(url, "proWin", "width=700,height=450,top=50");
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div style="text-align: center;">
        <table class="tab_Content">
            <tr>
                <td>
                    案件编号:
                    <asp:TextBox ID="txtIid" runat="server" SkinID="InputGreen"></asp:TextBox>
                </td>
                <td>
                    申请起始日期:
                    <%--<uc3:Calendar ID="txtSQQSSJ" runat="server" />--%>
                    <input onfocus="setday(this)" id="txtSQQSSJ" runat="server" readonly="readonly" size="10" />
                </td>
                <td>
                    申请中止日期:
                    <%--<uc3:Calendar ID="txtSQZZSJ" runat="server" />--%>
                    <input onfocus="setday(this)" id="txtSQZZSJ" runat="server" readonly="readonly" size="10" />
                </td>
                <td>
                    <asp:Button ID="btnQuery" Text="查询" OnClick="btnQuery_OnClick" runat="server" />
                </td>
            </tr>
        </table>
        <br />
        <defineControls:CustomGridView ID="CustomGridView1" runat="server" AllowPaging="True" AutoGenerateColumns="true"
            ShowItemNumber="false" SkinID="List" CheckboxAutoPostBack="False" CustomImportToExcel="false"
            CustomSortExpression="" DeleteToolTipOfCol="-1" HideTextLength="20" ShowCmpAndCancel="False"
            ShowGridViewSelect="List" ShowHideColModel="None" ShowImportButton="False" ShowTreeView="False"
            SelectOrDelete="True" AllowSortingAscImgCss="" AllowSortingDescImgCss="" Width="700px"
            OnOnLoadData="CustomGridView1_OnLoadData" OnRowCreated="CustomGridView1_RowCreated">
            <Columns>
                <asp:TemplateField HeaderText="查看进度">
                    <ItemTemplate>
                        <asp:HyperLink ID="hlView" runat="server" Target="_self">查看</asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </defineControls:CustomGridView>
    </div>
    </form>
</body>
</html>
