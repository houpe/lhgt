﻿<%@ WebHandler Language="C#" Class="AjaxCopyAndAdd" %>

using System;
using System.Web;

public class AjaxCopyAndAdd : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/plain";
        string strFlowName = context.Server.UrlDecode(context.Request["flowname"]);
        string strFlowType = context.Server.UrlDecode(context.Request["flowtype"]);
        context.Response.Write(ProcessInfo(context.Request["iid"], strFlowName,   strFlowType));
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

    public string ProcessInfo(string strIId, string strFlowname,string strFlowType)
    {
        SbBusiness.Wsbs.ShenBaoSubmit submit = new SbBusiness.Wsbs.ShenBaoSubmit();

        string strNewIID = submit.CopyAndAddShenBao(strIId);
        if (!string.IsNullOrEmpty(strNewIID))
        {
            string strReturn = string.Format("ShenBao.aspx?iid={0}&flowname={1}&flowtype={2}", strNewIID, strFlowname, strFlowType);
            return strReturn;
        }

        return "保存失败";
    }
}