﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SbBusiness.Wsbs;

using SbBusiness.User;
using SbBusiness;

using SbBusiness.Menu;

public partial class Wsbs_DetailOtherList : System.Web.UI.Page
{
    protected string PageTitle;
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {            
            BindOtherList();
        }
    }

    protected void cgvOther_OnLoadData(object sender, EventArgs e)
    {
        BindOtherList();
    }

    public void BindOtherList()
    {
        string strType = Request["Type"];
        if (strType=="1")
            this.lblTypeName.Text = "常见问题";
        else
            this.lblTypeName.Text = "信息公告";
        PageTitle = lblTypeName.Text+ "列表";
        SerialInstance instanceOperation = new SerialInstance();
        DataTable dtSource = instanceOperation.GetAskQuestion("", lblTypeName.Text, "", 0, 50);
        this.cgvOther.DataSource = dtSource;
        this.cgvOther.PageSize = SystemConfig.PageSize;
        this.cgvOther.RecordCount = dtSource.Rows.Count;
        this.cgvOther.DataBind();
    }
}
