﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SbBusiness.Menu;
using SbBusiness.Wsbs;
using Business.FlowOperation;

public partial class Wsbs_WsbsMain : System.Web.UI.Page
{
    public string strParam;
    public string strFlowType;//获取流程别名

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //流程名
            if (!string.IsNullOrEmpty(Request["flowname"]))
            {
                Session["flowname"] = Request["flowname"];
                //添加流程办理人员的显示控制 addby zhongjian 20091212
                WorkFlowPubSetting cWorks = new WorkFlowPubSetting();
                hidShowType.Value = cWorks.GetIsShowDialog(Session["flowname"].ToString())?"1":"0";

                strParam = Server.UrlEncode(Request["flowname"]);
            }
            else
            {
                if (Session["flowname"] != null)
                {
                    strParam = Session["flowname"].ToString();
                }
            }

            //流程别名
            if (!string.IsNullOrEmpty(Request["flowtype"]))
            {
                Session["flowtype"] = Request["flowtype"];
                strFlowType = Request["flowtype"];
                //strFlowType = Server.UrlEncode(strFlowType);
            }

            if (!string.IsNullOrEmpty(Request["iid"]))
            {
                strParam += string.Format(",{0}", Request["iid"]);
            }
            else
            {
                strParam += ",-1";//-1代表需要系统自动生成案件编号
            }
        }
    }
}
