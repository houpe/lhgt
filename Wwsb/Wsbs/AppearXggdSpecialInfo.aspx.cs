﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Wsbs_AppearXggdSpecialInfo : System.Web.UI.Page
{
    public string strHtmlInfo = string.Empty;
    public string strTitle = string.Empty;
    public string strPubTime = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SbBusiness.PubInfo.InfoManage piTemp = new SbBusiness.PubInfo.InfoManage();
            System.Data.DataTable dtTemp = piTemp.GetPubInfoById(Request["id"]);
            if (dtTemp.Rows.Count > 0)
            {
                strTitle = dtTemp.Rows[0]["title"].ToString();
                strHtmlInfo = dtTemp.Rows[0]["content"].ToString();
                strPubTime = dtTemp.Rows[0]["time"].ToString();
            }
        }
    }
}
