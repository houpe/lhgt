﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="XingZhengXuKe.aspx.cs" Inherits="Wsbs_XingZhengXuKe" %>

<%@ Register Src="~/UserControls/MsgPlate.ascx" TagName="MsgPlate" TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/ucPageFoot.ascx" TagName="ucPageFoot" TagPrefix="uc2" %>
<%@ Register Src="~/UserControls/ucPageHead.ascx" TagName="ucPageHead" TagPrefix="uc3" %>
<%@ Register Src="../UserControls/Calendar.ascx" TagName="Calendar" TagPrefix="uc4" %>
<head id="Head1" runat="server">
    <title>行政许可公示</title>
    <link href="../App_Themes/SkinFile/CSS/gcpage.css" rel="stylesheet" type="text/css" />
    <link href="../App_Themes/SkinFile/CSS/StyleSheet.css" rel="stylesheet" type="text/css" />

    <script src="../ScriptFile/Calendar.js" type="text/javascript"></script>

    <script type="text/javascript" language="javascript">
    function Cleaner()
    {
        document.form1.ddlFlow.selectedIndex=0;   
        document.form1.txtIID.value = "";
        document.form1.txtName.value = "";
        document.form1.txtFromDate.value = "";
        document.form1.txtToDate.value = "";
    }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div style="text-align: center;">
        <table border="0" cellpadding="0" cellspacing="0" class="page_box">
            <tr>
                <td colspan="3">
                    <uc3:ucPageHead ID="ucPageHead1" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="3" valign="top">
                    <table border="0" cellpadding="0" cellspacing="0" width="80%">
                        <tr>
                            <td align="center">
                                <div class="end_bar">
                                    行政许可公示</div>
                                <div style="text-align: left">
                                    <table class="tab_Content">
                                        <tr>
                                            <td>
                                                受理编号
                                                <asp:TextBox ID="txtIID" runat="server" Width="90px"></asp:TextBox>
                                            </td>
                                            <td>
                                                办理事项<asp:DropDownList ID="ddlFlow" runat="server" Width="220px">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                申请单位<asp:TextBox ID="txtName" runat="server" Width="120px"></asp:TextBox>
                                            </td>
                                            <td>
                                                申请日期(从)
                                                <input onfocus="setday(this)" id="txtFromDate" runat="server" readonly="readonly"
                                                    size="10" />
                                                到
                                                <input onfocus="setday(this)" id="txtToDate" runat="server" readonly="readonly" size="10" />
                                            </td>
                                            <td>
                                                <asp:Button ID="btnQuery" Text="查询" OnClick="btnQuery_Click" runat="server" />
                                                <input id="btnCleaner" type="button" value="重置" onclick="Cleaner()" class="button" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <br />
                                <div style="color: Black" align="center">
                                    <defineControls:CustomGridView ID="dgXkjggs" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                        ShowItemNumber="true" SkinID="List" CheckboxAutoPostBack="False" CustomImportToExcel="false"
                                        CustomSortExpression="" DeleteToolTipOfCol="-1" HideTextLength="20" ShowCmpAndCancel="False"
                                        ShowGridViewSelect="List" ShowHideColModel="None" ShowImportButton="False" ShowTreeView="False"
                                        SelectOrDelete="True" AllowSortingAscImgCss="" AllowSortingDescImgCss="" Width="1000"
                                        OnOnLoadData="dgXkjggs_OnLoadData">
                                        <Columns>
                                            <asp:BoundField DataField="受理编号" HeaderText="受理编号" ItemStyle-Width="10%" ItemStyle-Height="20px" />
                                            <asp:BoundField DataField="受理事项" HeaderText="办理事项" ItemStyle-Width="25%" />
                                            <asp:BoundField DataField="申请单位" HeaderText="申请单位" ItemStyle-Width="33%" />
                                            <asp:BoundField DataField="办件状态" HeaderText="办件状态" ItemStyle-Width="8%" />
                                            <asp:BoundField DataField="申请日期" HeaderText="申请日期" ItemStyle-Width="10%" />
                                            <asp:BoundField DataField="应办结日期" HeaderText="应办结日期" ItemStyle-Width="10%" />
                                        </Columns>
                                    </defineControls:CustomGridView>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <uc2:ucPageFoot ID="ucPageFoot1" runat="server" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
