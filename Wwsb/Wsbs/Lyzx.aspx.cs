﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SbBusiness.Wsbs;

using SbBusiness.User;
using SbBusiness;

using SbBusiness.Menu;
using Business.Admin;

public partial class Wsbs_Lyzx : System.Web.UI.Page
{
    SerialInstance SerialIO = new SerialInstance();
    protected void Page_Load(object sender, EventArgs e)
    {
        string action = Request.QueryString["action"];

        if (!IsPostBack)
        {
            BindFlowName();
            BindDepList();
            BindQuertionList();
            if (!string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                PersistenceControl1.PageControl = this.form1;
                PersistenceControl1.KeyValue = Request.QueryString["id"];
                PersistenceControl1.Status = "edit";
                PersistenceControl1.Render();
            }

            if (Session["LoginUserName"] != null)
            {
                txtName.Text = Session["LoginUserName"].ToString();
            }
        }
    }

    /// <summary>
    /// 随机生成留言码
    /// </summary>
    /// <!--addby zhongjian 20091117-->
    protected void GetMsgCode()
    {
        Random ran = new Random();
        int RandKey = ran.Next(1, 999999);
        hfdMsgCode.Value = RandKey.ToString();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (ddlFlow.Visible)
        {
            if (ddlFlow.SelectedValue == "0")
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "ok", "<script>window.alert('请选择留言办理事项！')</script>");
                return;
            }
        }
        else
        {
            if (ddlSlbm.SelectedValue == "0")
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "ok", "<script>window.alert('请选择留言审批部门！')</script>");
                return;
            }
        }
        if (ddlWtlx.SelectedValue == "0")
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "ok", "<script>window.alert('请选择问题类型！')</script>");
            return;
        }
        if (txtZt.Text.Trim() == "")
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "ok", "<script>window.alert('留言主题不能为空！')</script>");
            return;
        }
        if (txtNotes.Text.Trim() == "")
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "ok", "<script>window.alert('请认真填写留言内容！')</script>");
            return;
        }
        GetMsgCode();
        PersistenceControl1.PageControl = this.form1;
        PersistenceControl1.Update();
        string strMsg = "<script>window.alert('提交成功！')</script>";
        string strPub=rdbSfgk.SelectedIndex.ToString();
        if (strPub == "1")
            strMsg = string.Format("<script>window.alert('您选择的是不公开此留言，若要查询此留言及相关的回复信息，仅通过留言码查询。请记住该留言码：{0} ')</script>", hfdMsgCode.Value);
        Page.ClientScript.RegisterStartupScript(this.GetType(), "ok", strMsg);
    }

    protected void btnReturn_Click(object sender, EventArgs e)
    {
        //this.Response.Write("<script>window.close();</script>");
        //Response.Redirect(string.Format("TsManager.aspx?wid={0}", Request["wid"]));
        //屏关闭功能,修改为重置功能 update by zhongjian 20091203
        txtAddress.Text = "";
        txtDh.Text = "";
        txtEmail.Text = "";
        txtName.Text = "";
        txtNotes.Text = "";
        txtSj.Text = "";
        txtZt.Text = "";
    }

    /// <summary>
    /// 咨询分为:事项和部门 两个部分
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// <!--addby zhongjian 20091116-->
    protected void rblMsgType_SelectedIndexChanged(object sender, EventArgs e)
    {
        string strValue =rdbMsgType.SelectedIndex.ToString();
        if (strValue=="0")
        {
            lblMsgtype.Text = "选择咨询事项：";
            ddlFlow.Visible = true;
            ddlSlbm.Visible = false;
        }
        else
        {
            lblMsgtype.Text = "选择咨询部门：";
            ddlFlow.Visible = false;
            ddlSlbm.Visible = true;
        }
    }

    /// <summary>
    /// 绑定所有流程名称
    /// </summary>
    /// <!--addby zhongjian 20091118-->
    public void BindFlowName()
    {
        SysUserRightRequest urrTemp = new SysUserRightRequest();
        DataTable dtTable = urrTemp.GetFlowList();

        ddlFlow.Items.Clear();
        ddlFlow.Items.Add(new ListItem("请选择事项流程...", "0"));
        foreach (DataRow row in dtTable.Rows)
        {
            ddlFlow.Items.Add(new ListItem(row["KEYVALUE"].ToString(), row["KEYVALUE"].ToString()));
        }
        //
        if (Request["flow"] != null && Request["flow"] != "-1")
            ddlFlow.SelectedIndex = int.Parse(Request["flow"].ToString());
        else if (Session["flowtype"] != null)//设置默认流程别名 addby zhongjian 20100319
        {
            string strFlwtype = string.Format("{0}", Session["flowtype"]);
            for (int i = 0; i < ddlFlow.Items.Count; ++i)
            {
                if (strFlwtype == ddlFlow.Items[i].ToString())
                {
                    ddlFlow.SelectedIndex = i;
                    ddlFlow.Enabled = false;
                    break;
                }
            }
        }
    }

    /// <summary>
    /// 绑定咨询部门
    /// </summary>
    /// <!--addby zhongjian 20091118-->
    public void BindDepList()
    {
        DepartHandle dhTemp = new DepartHandle();
        DataTable dtTable = dhTemp.GetSecondDepart();
        ddlSlbm.Items.Clear();
        ddlSlbm.Items.Add(new ListItem("请选择审批部门...", "0"));
        foreach (DataRow row in dtTable.Rows)
        {
            ddlSlbm.Items.Add(new ListItem(row["depart_name"].ToString(), row["departid"].ToString()));
        }        
    }

    /// <summary>
    /// 绑定问题类型
    /// </summary>
    /// <!--addby zhongjian 20091118-->
    public void BindQuertionList()
    {
        DictOperation doTemp = new DictOperation();
        DataTable dtTable = doTemp.GetDictFromName("问题类型");
        ddlWtlx.Items.Clear();
        ddlWtlx.Items.Add(new ListItem("请选择问题类型...", "0"));
        foreach (DataRow row in dtTable.Rows)
        {
            ddlWtlx.Items.Add(new ListItem(row["KEYVALUE"].ToString(), row["KEYCODE"].ToString()));
        }
    }
}
