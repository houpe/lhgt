﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Lyzx.aspx.cs" Inherits="Wsbs_Lyzx" %>

<%@ Register Src="../UserControls/PersistenceControl.ascx" TagName="PersistenceControl"
    TagPrefix="uc2" %>
<%@ Register Src="../UserControls/TextBoxWithValidator.ascx" TagName="TextBoxWithValidator"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControls/GeneralSelect.ascx" TagName="GeneralSelect" TagPrefix="uc3" %>
<%@ Register Src="../UserControls/ucPageFoot.ascx" TagName="ucPageFoot" TagPrefix="uc4" %>
<%@ Register Src="../UserControls/ucPageHead.ascx" TagName="ucPageHead" TagPrefix="uc5" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>留言咨询</title>

    <script language="javascript" type="text/javascript" src="../ScriptFile/Regex.js"></script>

</head>
<body style="text-align: center;">
    <form id="form1" runat="server">
    <div style="text-align: center; width: 1000px;">
        <div>
            <uc5:ucPageHead ID="ucPageHead1" runat="server" />
        </div>
        <table id="table33" cellspacing="4" cellpadding="0" width="100%" border="0">
            <tr>
                <td style="text-align: left;">
                    <table id="table33" cellspacing="0" bordercolordark="#ffffff" cellpadding="4" width="100%"
                        bgcolor="#f0f5fd" bordercolorlight="#c0c0c0" border="1">
                        <tr align="center">
                            <td bgcolor="#c1dcff" colspan="2">
                                <font style="font-size: 9pt"></font><font color="#ff0000" style="font-size: 9pt">请选择咨询类型：</font>
                            </td>
                        </tr>
                        <tr align="center">
                            <td align="center">
                                <asp:RadioButtonList runat="server" ID="rdbMsgType" AutoPostBack="true" RepeatDirection="Horizontal"
                                    OnSelectedIndexChanged="rblMsgType_SelectedIndexChanged">
                                    <asp:ListItem Selected="True" Value="0">对审批事项有疑问</asp:ListItem>
                                    <asp:ListItem Value="1">对审批部门有疑问</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr align="center">
                            <td>
                                <asp:Label ID="lblMsgtype" runat="server" Text="选择办理事项："></asp:Label>
                                <asp:DropDownList ID="ddlFlow" runat="server"></asp:DropDownList>
                                <asp:DropDownList ID="ddlSlbm" runat="server" Visible="false"></asp:DropDownList>
                                <%--<uc3:GeneralSelect ID="ddlFlow" Name="所有流程" runat="server" />
                                <uc3:GeneralSelect ID="ddlSlbm" Name="投诉部门" runat="server" Visible="false" />--%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table id="table34" cellspacing="0" bordercolordark="#ffffff" cellpadding="4" width="100%"
                        bgcolor="#f0f5fd" bordercolorlight="#c0c0c0" border="1">
                        <tr>
                            <td bgcolor="#c1dcff" colspan="4">
                                <font style="font-size: 9pt">请填写您的</font><font color="#ff0000" style="font-size: 9pt">个人信息：</font>
                            </td>
                        </tr>
                        <tr>
                            <td width="70" align="center">
                                <font style="font-size: 9pt">姓 名</font>
                            </td>
                            <td width="35%">
                                <uc1:TextBoxWithValidator ID="txtName" runat="server" Width="98%" />
                            </td>
                            <td width="70" align="center">
                                <font style="font-size: 9pt">性 别</font>
                            </td>
                            <td width="35%" align="left">
                                <asp:RadioButtonList runat="server" ID="rdbSex" RepeatDirection="Horizontal">
                                    <asp:ListItem Selected="True">男</asp:ListItem>
                                    <asp:ListItem>女</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td width="70" align="center">
                                <font style="font-size: 9pt">手 机</font>
                            </td>
                            <td width="35%">
                                <uc1:TextBoxWithValidator ID="txtSj" Type="Phone" runat="server" Width="98%" />
                            </td>
                            <td width="70" align="center">
                                <font style="font-size: 9pt">地 址</font>
                            </td>
                            <td width="35%">
                                <uc1:TextBoxWithValidator ID="txtAddress" runat="server" Width="98%" />
                            </td>
                        </tr>
                        <tr>
                            <td width="70" align="center">
                                <font style="font-size: 9pt">电 话</font>
                            </td>
                            <td width="35%">
                                <uc1:TextBoxWithValidator ID="txtDh" Type="Phone" runat="server" Width="98%" />
                            </td>
                            <td width="70" align="center">
                                <font style="font-size: 9pt">邮 箱</font>
                            </td>
                            <td width="35%">
                                <uc1:TextBoxWithValidator ID="txtEmail" runat="server" Width="98%" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table id="table35" cellspacing="0" bordercolordark="#ffffff" width="100%" bgcolor="#f0f5fd"
                        bordercolorlight="#c0c0c0" border="1">
                        <tr>
                            <td bgcolor="#c1dcff" colspan="4">
                                <font style="font-size: 9pt">请填写您的</font><font style="font-size: 9pt" color="#ff0000">信件信息：</font>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" width="70">
                                <font style="font-size: 9pt">类 型</font>
                            </td>
                            <td width="35%">
                                <%--<uc3:GeneralSelect ID="ddlWtlx" Name="问题类型" runat="server" Width="98%" />--%>
                                <asp:DropDownList ID="ddlWtlx" runat="server"></asp:DropDownList>
                            </td>
                            <td width="70" align="center">
                                <font style="font-size: 9pt">是否公开</font>
                            </td>
                            <td width="35%" align="left">
                                <asp:RadioButtonList runat="server" ID="rdbSfgk" RepeatDirection="Horizontal">
                                    <asp:ListItem Selected="True">公开</asp:ListItem>
                                    <asp:ListItem>不公开</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td width="70" align="center">
                                <font style="font-size: 9pt">主 题</font>
                            </td>
                            <td width="90%" colspan="6">
                                <uc1:TextBoxWithValidator ID="txtZt" runat="server" Width="98%" />
                            </td>
                        </tr>
                        <tr>
                            <td width="70" align="center" height="100%">
                                <font style="font-size: 9pt">内 容</font>
                            </td>
                            <td width="90%" colspan="6">
                                <uc1:TextBoxWithValidator ID="txtNotes" TextMode="MultiLine" runat="server" Rows="10"
                                    Width="98%" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <font style="font-size: 9pt" color="#ff0000">说明：</font><font style="font-size: 9pt"
                        color="#003399">对涉及到具体工作人员的违规和服务态度的问题，请您详细提供有关情况（具体时间、地点）和您的具体联系方法，以便有关部门进行核对。如果您不希望在网上公开您的问题，可以选择“不公开”选项，提交后只能凭系统反馈给您的</font><font
                            style="font-size: 9pt" color="#ff0000">查询码</font><font style="font-size: 9pt" color="#003399">进行查询。</font>
                </td>
            </tr>
        </table>
        <asp:HiddenField ID="hfdMsgCode" runat="server" />
        <asp:Button ID="btnSave" runat="server" Text="提交" OnClick="btnSave_Click" SkinID="LightGreen" />
        <asp:Button ID="btnReturn" runat="server" Text="重置" CausesValidation="False" OnClick="btnReturn_Click"
            SkinID="LightGreen" />
        <uc2:PersistenceControl ID="PersistenceControl1" runat="server" Key="id" Table="XT_LYZX" />
        <div>
            <uc4:ucPageFoot ID="ucPageFoot1" runat="server" />
        </div>
    </div>
    </form>
</body>
</html>
