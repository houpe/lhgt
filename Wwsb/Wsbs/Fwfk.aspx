﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Fwfk.aspx.cs" Inherits="Wsbs_Fwfk" %>

<%@ Register Src="../UserControls/PersistenceControl.ascx" TagName="PersistenceControl"
    TagPrefix="uc2" %>
<%@ Register Src="../UserControls/TextBoxWithValidator.ascx" TagName="TextBoxWithValidator"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControls/GeneralSelect.ascx" TagName="GeneralSelect" TagPrefix="uc3" %>
<%@ Register Src="../UserControls/ucPageFoot.ascx" TagName="ucPageFoot" TagPrefix="uc4" %>
<%@ Register Src="../UserControls/ucPageHead.ascx" TagName="ucPageHead" TagPrefix="uc5" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>服务反馈</title>

    <script language="javascript" type="text/javascript" src="../ScriptFile/Regex.js"></script>

</head>
<body style="text-align: center;">
    <form id="form1" runat="server">
    <div style="text-align: center; width: 1000px;">
        <div>
            <uc5:ucPageHead ID="ucPageHead1" runat="server" />
        </div>
        <table width="100%" border="0" cellpadding="0" cellspacing="0" background="../images/bg_zwbg.gif">
            <tr>
                <td height="450" align="center" valign="top">
                    <table width="100%">
                        <tr class="GridDblHeader">
                            <td id="mytips" align="center" class="GridDblHeader">
                                服务质量反馈
                            </td>
                        </tr>
                    </table>
                    <table width="96%" border="1" bordercolorlight="#869ed9" bordercolordark="#869ed9"
                        id="table1" style="border-collapse: collapse">
                        <tr>
                            <td bgcolor="#cee7ff" width="15%">
                                姓 名
                            </td>
                            <td bgcolor="#dfefff" width="35%">
                                <uc1:TextBoxWithValidator ID="txtName" runat="server" Width="98%" />
                            </td>
                            <td bgcolor="#cee7ff" width="15%">
                                电子邮件
                            </td>
                            <td bgcolor="#dfefff" width="35%">
                                <uc1:TextBoxWithValidator ID="txtEmail" runat="server" Width="98%" />
                            </td>
                        </tr>
                        <tr>
                            <td height="21" bgcolor="#cee7ff" width="15%">
                                年 龄
                            </td>
                            <td height="21" bgcolor="#dfefff" width="35%">
                                <uc1:TextBoxWithValidator ID="txtAge" Type="Integer" runat="server" Width="98%" />
                            </td>
                            <td height="21" bgcolor="#cee7ff" width="15%">
                                性 别
                            </td>
                            <td height="21" bgcolor="#dfefff">
                                <asp:RadioButtonList runat="server" ID="rdbSex" RepeatDirection="Horizontal">
                                    <asp:ListItem Selected="True">男</asp:ListItem>
                                    <asp:ListItem>女</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#cee7ff" width="15%">
                                居民身份证号码
                            </td>
                            <td bgcolor="#dfefff" width="35%">
                                <uc1:TextBoxWithValidator ID="txtSfzh" Type="IDCard" ErrorMessage="15或18位身份证号输入错误"
                                    runat="server" Width="98%" />
                            </td>
                            <td bgcolor="#cee7ff" width="15%">
                                教育程度
                            </td>
                            <td bgcolor="#dfefff">
                                <uc3:GeneralSelect ID="ddlJycd" Name="学历" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td height="22" bgcolor="#cee7ff" width="15%">
                                住址/邮政地址
                            </td>
                            <td height="22" bgcolor="#dfefff" width="35%">
                                <uc1:TextBoxWithValidator ID="txtAddress" runat="server" Width="98%" />
                            </td>
                            <td height="22" bgcolor="#cee7ff" width="15%">
                                邮政编码
                            </td>
                            <td height="22" bgcolor="#dfefff" width="35%">
                                <uc1:TextBoxWithValidator ID="txtYzbm" Type="Postcode" runat="server" Width="98%" />
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#cee7ff" width="15%">
                                电 话
                            </td>
                            <td bgcolor="#dfefff" width="35%">
                                <uc1:TextBoxWithValidator ID="txtDh" Type="Phone" runat="server" Width="98%" />
                            </td>
                            <td bgcolor="#cee7ff">
                            </td>
                            <td bgcolor="#dfefff">
                            </td>
                        </tr>
                    </table>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                    <table border="1" width="96%" id="table2" bordercolorlight="#869ed9" bordercolordark="#869ed9"
                        style="border-collapse: collapse">
                        <tr>
                            <td width="120" bgcolor="#cee7ff" style="height: 33px">
                                受理部门：
                            </td>
                            <td colspan="5" bgcolor="#dfefff" width="85%" style="height: 33px; text-align:left;">
                                <asp:DropDownList ID="ddlSlbm" runat="server"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td width="120" rowspan="1" bgcolor="#cee7ff">
                                办理事项：
                            </td>
                            <td colspan="5" bgcolor="#dfefff" width="85%" style="height: 33px; text-align:left;">
                                <asp:DropDownList ID="ddlFlow" runat="server"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#cee7ff" width="15%">
                                服务质量：
                            </td>
                            <td bgcolor="#dfefff">
                                <asp:RadioButtonList runat="server" ID="rdbFwzl" RepeatDirection="Horizontal">
                                    <asp:ListItem>其他不满意</asp:ListItem>
                                    <asp:ListItem>业务不熟</asp:ListItem>
                                    <asp:ListItem>态度不好</asp:ListItem>
                                    <asp:ListItem Selected="True">满意</asp:ListItem>
                                    <asp:ListItem>非常满意</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#cee7ff" width="15%">
                                建议或意见：
                            </td>
                            <td colspan="5" bgcolor="#dfefff" align="left">
                                <uc1:TextBoxWithValidator ID="txtNotes" TextMode="MultiLine" runat="server" Rows="10"
                                    Width="98%" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <asp:Button ID="btnSave" runat="server" Text="保存" OnClick="btnSave_Click" SkinID="LightGreen" />
        <asp:Button ID="btnReturn" runat="server" Text="关闭" CausesValidation="False" OnClick="btnReturn_Click"
            SkinID="LightGreen" />
        <div>
            <uc4:ucPageFoot ID="ucPageFoot1" runat="server" />
        </div>
    </div>
    <uc2:PersistenceControl ID="PersistenceControl1" runat="server" Key="id" Table="XT_FWFK" />
    </form>
</body>
</html>
