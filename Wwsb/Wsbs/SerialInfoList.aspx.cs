﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Business.FlowOperation;
using SbBusiness.Wsbs;

public partial class Wsbs_SerialInfoList : System.Web.UI.Page
{
    private SerialInstance serialOperation = new SerialInstance();

    /// <summary>
    /// 窗体加载
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        CustomGridView1.DataKeyNames = new string[] { "iid" };

        if (!IsPostBack)
        {
            BindData();
        }
    }

    /// <summary>
    /// 完成事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void CustomGridView1_OnComplelteClick(object sender, WebControls.CustomGridViewEventArgs e)
    {
        if (e == null)
        {
            BindData();
            return;
        }

        ProcesOnlyValue(e.SingleOrMultiSelectKey);
    }

    /// <summary>
    /// 处理用户名单选
    /// </summary>
    /// <param name="keys"></param>
    private void ProcesOnlyValue(DataKey keys)
    {
        string values = keys.Value.ToString();
        Page.ClientScript.RegisterStartupScript(this.GetType(), "selectInstance", string.Format("<script>setParentValue('{0}');</script>", values));
    }

    /// <summary>
    /// 取消按钮
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void CustomGridView1_OnCancelClick(object sender, EventArgs e)
    {
        Response.Write("<script>window.close();</script>");
    }

    protected void CustomGridView1_OnLoadData(object sender, EventArgs e)
    {
        BindData();
    }

    /// <summary>
    /// 绑定数据
    /// </summary>
    private void BindData()
    {
        InstanceOperation ioTemp = new InstanceOperation();
        DataTable dtTable = ioTemp.GetSerialInfo(txtIid.Text, txtSqr.Text, Request["wid"]);

        CustomGridView1.DataSource = dtTable;
        CustomGridView1.RecordCount = dtTable.Rows.Count;
        CustomGridView1.DataBind();
    }

    /// <summary>
    /// 查询
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }
}
