﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SbBusiness.Wsbs;
using SbBusiness;

public partial class Wsbs_Tongzhi : System.Web.UI.Page
{
    Messagebox message = new Messagebox();
    private string strQuery = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        strQuery = Request["query"];
        if (!IsPostBack)
        {
            BindData();
            Session["DangQianRenWu"] = "通知查询";
        }
    }

    /// <summary>
    /// Binds the data.
    /// </summary>
    private void BindData()
    {
        string strReadType = string.Empty;
        if (strQuery == "1")
        {
            strReadType = "0";
            this.CustomGridView1.Columns[6].Visible = false;
            this.CustomGridView1.EmptyDataText = "您暂时没有未读通知消息。";
        }
        DataTable dtSource = message.GetMessageInfo(Session["LoginUserName"].ToString(), txtFromPlan.Text, txtToPlan.Text, Session["UserID"].ToString(), strReadType);
        this.CustomGridView1.DataSource = dtSource;
        this.CustomGridView1.PageSize = SystemConfig.PageSize;
        this.CustomGridView1.RecordCount = dtSource.Rows.Count;
        
        this.CustomGridView1.DataBind();
    }

    /// <summary>
    /// Handles the OnLoadData event of the CustomGridView1 control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void CustomGridView1_OnLoadData(object sender, EventArgs e)
    {
        BindData();
    }

    protected void btnQuery_OnClick(object sender, EventArgs e)
    {
        BindData();
    }

    #region 行创建
    //
    protected void gridviewCar_RowCreated(object sender, GridViewRowEventArgs e)
    {
        switch (e.Row.RowType)
        {
            case DataControlRowType.DataRow:
                DataRowView dtrv = e.Row.DataItem as DataRowView;
                if (dtrv == null)
                {
                    break;
                }
                break;
        }
    }
    #endregion

    protected string GetDisplay()
    {
        return (strQuery == "0") ? "block" : "none";
    }
    /// <summary>
    /// 连接消息详细信息字符串
    /// </summary>
    /// <param name="row">当前行</param>
    /// <returns></returns>
    /// <!--addby zhongjian 20090901-->
    protected string GetUrl(IDataItemContainer row)
    {
        string strHtml = string.Empty;
        string strReadType = DataBinder.Eval(row.DataItem, "READTYPE").ToString();
        string strMessage = DataBinder.Eval(row.DataItem, "messagetext").ToString();
        if (strMessage.Length >= 40)
        {
            strMessage = strMessage.Substring(0, 40) + "...";
        }
        if (strQuery == "0")
        {
            if (strReadType == "0")
                strMessage = strMessage + "(未读)";
        }
        string strIId = DataBinder.Eval(row.DataItem, "messageid").ToString(); 

        strHtml = string.Format("<a href='TongzhiManager.aspx?messageid={0}' target='_blank'>{1}</a>", strIId, strMessage);
        return strHtml;
    }

    protected void lbtnDelete_Click(object sender, EventArgs e)
    {
        LinkButton lbtn = (LinkButton)sender;
        string strID = lbtn.CommandArgument.ToString();
        message.DeleteMessage(strID);
        //添加操作日志 addby zhongjian 20100421
        string strRemark = string.Format("删除通知");
        string strSql = string.Format(@"delete xt_messagebox where messageid='{0}'", strID);
        SystemLogs.AddSystemLogs(Session["UserId"].ToString(), "delete", strRemark, strSql);
        BindData();
    }
}
