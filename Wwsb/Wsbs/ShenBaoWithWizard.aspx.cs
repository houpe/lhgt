﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using SbBusiness.Wsbs;
using System.Configuration;

public partial class Wsbs_ShenBaoWithWizard : System.Web.UI.Page
{
    public string strHtmlInfo = string.Empty;

    /// <summary>
    /// 页面加载
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["LoginUserName"] == null)
            {
                Response.Redirect("~/" + SbBusiness.SystemConfig.LoginPage);
            }

            if (!string.IsNullOrEmpty(Request["iid"]))
            {
                hidIId.Value = Request["iid"];
            }
            else
            {
                hidIId.Value = "0";
            }

            //在取消的情况下不执行
            BinddingWizard();
        }

        ViewState["UserName"] = Server.UrlEncode(Session["LoginUserName"].ToString());
    }

    /// <summary>
    /// 实现向导
    /// </summary>
    protected void BinddingWizard()
    {
        string strFlowName = Request["flowname"];
        string strServername = Request.Url.Authority;
        string strNewwebdisplay = ConfigurationManager.AppSettings["NewWebDisplay"];
        string strIId = Request["iid"];        
        string strFid = string.Empty;
        string strTitle = string.Empty;//表单别名
        //控制保存按钮是否可操作
        string strSaveBtnStyle = string.Empty;
        string strSubmitBtnStyle = string.Empty;
        string strPrintBtnStyle = string.Empty;
        string strFuJianUrl = "../Common/SbFileUploadNew.aspx";//"../Common/CailiaoUpload.aspx"

        //默认添加权限打开参数--针对webservice2
        if (strNewwebdisplay.IndexOf("?") == -1)
        {
            strNewwebdisplay += "?readonly=false&input_index=0";
        }
        else
        {
            strNewwebdisplay += "&readonly=false&input_index=0";
        }

        //构建向导模式
        XtProcess cProcess = new XtProcess();        
        DataTable dtTable = cProcess.GetProcess(strFlowName);

        for (int i = 0; i <= dtTable.Rows.Count;i++ )//涵盖申请表和附件页
        {
            string strDivStyle = string.Empty;
            string strTdStyle = "divStep";

            if (i > 0)//默认第一页显示，其他页隐藏
            {
                strDivStyle = "style='display:none'";
            }

            strHtmlInfo += string.Format(@"<div {1} id='step{0}'><table><tr>", i, strDivStyle);
            //组织显示所有的步骤列表
            for (int j = 0; j < dtTable.Rows.Count; j++)
            {
                strTitle = dtTable.Rows[j]["TABLEANOTHERNAME"].ToString();//表单别名
                strFid = dtTable.Rows[j]["FID"].ToString();//表单FID
                if (i == j)
                {
                    strTdStyle = "current";
                }
                else if (j < i)
                {
                    strTdStyle = "lastDone";   
                }
                else
                {
                    strTdStyle = "divStep";
                }
                strHtmlInfo += string.Format("<td class='{2}' title='{3}'>第{0}步 {1}</td>", j + 1, strTitle, strTdStyle, strTitle);
            }

            //实现最后一步的附件材料上传
            if (i == dtTable.Rows.Count)//没有申请表只有材料上传的前提下
            {
                strTdStyle = "current";
            }
            else
            {
                strTdStyle = "divStep";
            }
            strHtmlInfo += string.Format("<td class='{1}' title='上传材料'>第{0}步 上传材料</td>", dtTable.Rows.Count + 1, strTdStyle);

            //组织显示按钮
            if (i < dtTable.Rows.Count)//代表申请表记录数大于0
            {
                strFid = dtTable.Rows[i]["FID"].ToString();
                string strNext = string.Empty;
                strTitle= Server.UrlEncode(dtTable.Rows[i]["TABLEANOTHERNAME"].ToString());
                strHtmlInfo += "<td>";
                if (i == 0)//第一页（显示“下一步”）
                {
                    //处理表单数量只有一张的情况 addby zhongjian 20091218
                    if (dtTable.Rows.Count == 1)//只有一页的情况
                    {
                        strNext = dtTable.Rows[i]["FID"].ToString();

                        strHtmlInfo += string.Format(@"<input type='hidden' value='http://{2}{3}&fid={4}&userid=-1&iid={5}&step=-1' id='hidFirst' title='{7}'/><input type='button' class='zxsbButton' value='下一步' onclick=""loadnext({0},{1},'{9}?flowname={8}&NewsId={5}&Flag=','{7}');"" />", i, i + 1, strServername, strNewwebdisplay, strFid, strIId, strNext, strTitle, strFlowName, strFuJianUrl);
                    }
                    else//多于一页的情况
                    {
                        strNext = dtTable.Rows[i + 1]["FID"].ToString();
                        strHtmlInfo += string.Format(@"<input type='hidden' value='http://{2}{3}&fid={4}&userid=-1&iid={5}&step=-1' id='hidFirst' title='{7}'/><input type='button' class='zxsbButton' value='下一步' onclick=""loadnext({0},{1},'http://{2}{3}&fid={6}&userid=-1&iid={5}&step=-1','{7}');"" />",
                        i, i + 1, strServername, strNewwebdisplay, strFid, strIId, strNext, strTitle);
                    }
                }
                else//第二条及以上记录（显示“上一步 下一步”）
                {
                    string strPre = dtTable.Rows[i - 1]["FID"].ToString();
                    strTitle = Server.UrlEncode(dtTable.Rows[i - 1]["TABLEANOTHERNAME"].ToString());
                    if (i < dtTable.Rows.Count - 1)//第2至倒数第2页
                    {
                        strNext = dtTable.Rows[i + 1]["FID"].ToString();
                        strTitle = Server.UrlEncode(dtTable.Rows[i + 1]["TABLEANOTHERNAME"].ToString());
                        strHtmlInfo += string.Format(@"<input type='button' value='上一步' class='zxsbButton' onclick=""loadnext({0},{6},'http://{2}{3}&fid={4}&userid=-1&iid={5}&step=-1','{8}');"" /><input type='button' class='zxsbButton' value='下一步' onclick=""loadnext({0},{1},'http://{2}{3}&fid={7}&userid=-1&iid={5}&step=-1','{8}');"" />",
                        i, i + 1, strServername, strNewwebdisplay, strPre, strIId, i - 1, strNext, strTitle);
                    }
                    else//最后一页
                    {
                        strHtmlInfo += string.Format(@"<input type='button' value='上一步' class='zxsbButton' onclick=""loadnext({0},{6},'http://{1}{2}&fid={3}&userid=-1&iid={4}&step=-1','{8}');"" /><input type='button' class='zxsbButton' value='下一步' onclick=""loadnext({0},{7},'{9}?flowname={5}&NewsId={4}&Flag=','{8}');"" />", i, strServername, strNewwebdisplay, strPre, strIId, strFlowName, i - 1, i + 1, strTitle, strFuJianUrl);
                    }
                }
                strHtmlInfo += "</td>";
            }
            else//申请表循环结束，针对上传材料页（显示“上一步”）
            {             
                string strPre = dtTable.Rows[i - 1]["FID"].ToString();
                strTitle = Server.UrlEncode(dtTable.Rows[i - 1]["TABLEANOTHERNAME"].ToString());
                strHtmlInfo += string.Format(@"<td><input type='button' value='上一步' class='zxsbButton' onclick=""loadnext({0},{6},'http://{1}{2}&fid={3}&userid=-1&iid={4}&step=-1','{7}');"" />
                </td>", i, strServername, strNewwebdisplay, strPre, strIId, strFlowName, i - 1, strTitle);
            }

            strHtmlInfo += "</tr></table></div>";
        }//for循环结束

        //控制保存、提交、打印的可操作性
        if (!string.IsNullOrEmpty(strIId))
        {
            string strSubmitFlag = ShenBaoSubmit.GetIsSubmit(strIId);

            //0:未提交;1:已提交;2:已通过;3:预审;-1:返回补证;-2:驳回中止;-3:不予受理;-4:审批不通过
            //已经提交或者在内网受理的案件均不能再次保存提交
            if (strSubmitFlag == "1" || strSubmitFlag == "2" || strSubmitFlag == "3" || strSubmitFlag == "-2" || strSubmitFlag == "-3" || strSubmitFlag == "-4")
            {
                strSaveBtnStyle = "disabled='disabled'";
                strSubmitBtnStyle = "disabled='disabled'";
            }

            if (strSubmitFlag == "0" || strSubmitFlag == "1" || strSubmitFlag == "-1" || strSubmitFlag == "-3" || strSubmitFlag == "-4" || strSubmitFlag == "")//预审通过时,才能打印
            {
                strPrintBtnStyle = "disabled='disabled'";
            }
        }
        else
        {
            strSaveBtnStyle = "disabled='disabled'";
            strSubmitBtnStyle = "disabled='disabled'";
            strPrintBtnStyle = "disabled='disabled'";
        }
        strHtmlInfo += string.Format(@"<div style='width:100%;text-align:right;'><input id='saveform' type='button' value=' 保 存 ' {0} 
                onclick='SaveInfo();' class='zxsbButton'/>&nbsp;<input id='submitform' type='button' value=' 提 交 ' {1} 
                onclick='SubmitInfo();' class='zxsbButton'/>&nbsp;<input id='printform' type='button' value=' 打 印 ' {2}
                onclick='framePrint()' class='zxsbButton' />&nbsp;<input id='printformView' type='button' value='打印预览' {2} onclick='framePrintPreview()' class='zxsbButton' />&nbsp;<input id='btnWizard' type='button' 
                value='普通模式' onclick='openWizardUrl()' 
                class='zxsbButton' /></div>", strSaveBtnStyle, strSubmitBtnStyle, strPrintBtnStyle);
    }

}
