﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SbBusiness.Wsbs;

using SbBusiness.User;
using SbBusiness;

using SbBusiness.Menu;

public partial class Wsbs_XingZhengXuKe : System.Web.UI.Page
{
    SerialInstance instanceOperation = new SerialInstance();
    protected void Page_Load(object sender, EventArgs e)
    {        
        if (!IsPostBack)
        {
            BindFlowName();
            BindWhere();
            BindData();
        }
    }

    #region 行政许可结果公示
    /// <summary>
    /// Binds the data.
    /// </summary>
    private void BindData()
    {
        DataTable dtSource = instanceOperation.GetInstanceInfo(txtIID.Text.Trim(), txtName.Text.Trim(), ddlFlow.SelectedItem.Value, txtFromDate.Value.Trim(), txtToDate.Value.Trim());
        this.dgXkjggs.DataSource = dtSource;
        this.dgXkjggs.PageSize = SystemConfig.PageSize;
        this.dgXkjggs.RecordCount = dtSource.Rows.Count;
        this.dgXkjggs.DataBind();
    }

    /// <summary>
    /// 许可结果公示的翻页事件
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void dgXkjggs_OnLoadData(object sender, EventArgs e)
    {
        BindData();
    }
    #endregion

    protected void btnQuery_Click(object sender, EventArgs e)
    {
        BindData();
    }

    /// <summary>
    /// 绑定所有流程名称
    /// </summary>
    /// <!--addby zhongjian 20091225-->
    public void BindFlowName()
    {
        SysUserRightRequest urrTemp = new SysUserRightRequest();
        DataTable dtTable = urrTemp.GetFlowList();
        ddlFlow.Items.Clear();
        ddlFlow.Items.Add(new ListItem("请选择事项流程...", ""));
        foreach (DataRow row in dtTable.Rows)
        {
            ddlFlow.Items.Add(new ListItem(row["KEYVALUE"].ToString(), row["KEYVALUE"].ToString()));
        }
    }

    /// <summary>
    /// 绑定首页所输入的条件
    /// </summary>
    /// <!--addby zhongjian 20091228-->
     public void BindWhere()
     {
        if (Request["flow"] != null && Request["flow"]!="-1")
            ddlFlow.SelectedIndex = int.Parse(Request["flow"].ToString());
        else if (Session["flowtype"] != null)//设置默认流程别名 addby zhongjian 20100319
        {
            string strFlwtype = string.Format("{0}", Session["flowtype"]);
            for (int i = 0; i < ddlFlow.Items.Count; ++i)
            {
                if (strFlwtype == ddlFlow.Items[i].ToString())
                {
                    ddlFlow.SelectedIndex = i;
                    break;
                }
            }
            
        }
        if (Request["iid"] != null)
            txtIID.Text= Request["iid"];
        if (Request["name"] != null)
            txtName.Text= Request["name"];
        if (Request["fromdate"] != null)
            txtFromDate.Value= Request["fromdate"];
        if (Request["todate"] != null)
            txtToDate.Value= Request["todate"];
    }
}
