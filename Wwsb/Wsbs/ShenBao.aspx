﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master"
    CodeFile="ShenBao.aspx.cs" Inherits="Wsbs_ShenBao" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">
    <script type="text/javascript" src="../ScriptFile/jquery.js"></script>
    <script type="text/javascript" src="../ScriptFile/JqueryUi/jquery.ui.core.js"></script>
    <script type="text/javascript" src="../ScriptFile/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../ScriptFile/AutoIframeHeight.js"></script>
    <script type="text/javascript">
        var isSaveSuccess = false;
        //判断表单是否更改
        var isChange = false;
        var currentUrl, currentCtrlId;
        var strFlowType = encodeURI('<%=Request["flowtype"]%>');
        var strFlowName = encodeURI('<%=Request["flowname"]%>');
        var userid = encodeURI('<%=Session["UserID"]%>');
        var username = encodeURI('<%=ViewState["UserName"]%>');
      
        function GetIID()
        {
            return $("#<%=hidIId.ClientID %>").val(); //获取client的值
        }

        //消息提示框
        function ConfirmMsg(strInfo)
        {
            return confirm(strInfo);
        }

        //表单保存后的回调方法
        function SaveIId() {
            var strBgFlag = $("#<%=hidBgFlag.ClientID %>").val(); //查看是否具备变更权限
            var strYqFlag = $("#<%=hidYqFlag.ClientID %>").val(); //查看是否具备延期权限
            if (strBgFlag == "3" || strYqFlag=="3") {//如果是变更或延期状态则不需要设置状态位，直接跳出
                return ;
            }

            //添加提醒消息
            $.ajax({
                url: 'AjaxSaveForm.ashx',
                data: "dataIId=" + GetIID() + "&dataFlowname=" + strFlowName+ "&flowType=" + strFlowType+"&userId="+userid+"&userName="+username,
                type: 'GET',
                dataType: 'text',
                cache: false,
                async: false,
                error: function() {
                    alert('保存出现异常');
                },
                success: function(data) {    //alert(data);
                    if (data == "保存失败") {
                        alert(data);
                    }
                    else {
                        isSaveSuccess = true;
                        if (currentCtrlId != "" && typeof(currentCtrlId)!="undefined" && currentUrl != "") {
                            $("#frmChild").attr("src", currentUrl);

                            $("#tabMenu td[id ^= 'td']").attr("class", "tabcontrol_button03");
                            $("#" + currentCtrlId).attr("class", "tabcontrol_button05");
                            currentCtrlId = "";
                            currentUrl = "";
                        }
                        isChange=false;
                        alert("保存成功");
                    }
                }
            });
        }

        function SaveInfo() {
            //老保存方法
            //frmChild.SaveAll(SaveIId);
            //新保存方法,采用HtmlMainPage.aspx的方法
            if(isChange)
            {
                frmChild.SaveAll(false, SaveIId);
            }
            else
            {
                alert("未经过任何改动，无需保存");
            }
        }

        //提交
        function SubmitInfo() {    
            if( !ConfirmMsg("确定您填写的申请信息和上报材料符合要求并提交吗？"))
            {
                return;
            }

            if ($("#saveform").css("display") != "none") {
                if(isChange)
                {
                    frmChild.SaveAll(false);//采用HtmlMainPage.aspx的方法
                    //frmChild.SaveAll(); //提交时,自动保存
                }
            }

            var strBgFlag = $("#<%=hidBgFlag.ClientID %>").val(); //查看是否具备变更权限
            var strYqFlag = $("#<%=hidYqFlag.ClientID %>").val(); //查看是否具备延期权限
           
            //设置提交状态位
            $.ajax({
                url: 'AjaxSubmitFlag.ashx',
                data: "dataIId=" + GetIID() + "&dataFlowname=" + strFlowName + "&flowtype=" + strFlowType + "&bgflag=" + strBgFlag + "&yqflag=" + strYqFlag + "&userId=" + userid + "&userName=" + username,
                type: 'GET',
                dataType: 'text',
                cache: false,
                async: false,
                error: function() {
                    alert('提交未成功，请检查是否未上传材料或是否未保存表单');
                },
                success: function(data) {
                    if (data == "提交成功") {
                        alert('提交成功');
                        $("#saveform").attr("disabled", true);
                        $("#submitform").attr("disabled", true);
                        //$("#btnAddCopy").attr("disabled", true);
                    }
                    else
                        alert('请检查是否未上传材料或是否未保存表单');
                }
            });
        }

        //选项卡点击事件，设置tabcontrol按钮的背景颜色,并判断是否进行过表单修改
        function SetBackColor(varCtrl, strUrl, strTabName) {
            //初始化表单
            var strInitScript = "";

            //判断提交按钮是否可用，如果可用才加载数据
            var varEnableSave = $("#submitform").attr("disabled");
            if (typeof(varEnableSave)=="undefined") {
                if( strTabName!="-1") {//表单默认加载
                    $.ajax({
                        url: 'ajaxInitFormValue.ashx',
                        data: "flowname=" + strFlowName + "&tabName=" + strTabName + "&userId=" + userid,
                        type: 'POST',
                        dataType: 'text',
                        async: false,
                        success: function (dataScript) {
                            if (dataScript != "") {
                                strInitScript=dataScript;
                            }
                            //执行加载后的代码
                        }
                    });
                }//默认加载项结束
            }

            if (isChange) //数据已修改
            {
                currentUrl = strUrl;
                currentCtrlId = varCtrl.id;
                
                var varEnableSave = $("#saveform").attr("disabled");
                //判断按钮是否可用
                if (varEnableSave == false || typeof(varEnableSave)=="undefined") {
                    isSaveSuccess = false;
                    //当数据有修改时,调用保存方法,以起到自动保存的功能,且不再提示用户是否保存
                    //按三种选项模式框提示 edit by zhongjian 20091109
//                    if( ConfirmMsg("表单数据已修改，是否保存？"))
//                    {
                        isSaveSuccess = false;
                        SaveInfo();
//                    }
//                    else{
//                        isSaveSuccess = true;
//                        isChange=false;
//                    }
                }
                else{
                        isSaveSuccess = true;
                        isChange=false;
                    }
            }
            else {
                isSaveSuccess = true;
            }

            //判定是否保存成功
            if (isSaveSuccess) {
                if( strTabName=="-1")
                {
                    var strYqFlag = $("#<%=hidYqFlag.ClientID %>").val(); //查看是否具备延期权限
                    if(strYqFlag=="3")//如果是延期
                    {
                        strUrl=strUrl+"&Flag=2";
                    } 
                    else
                    {
                        strUrl=strUrl+"&Flag=<%=Request["flag"]%>";
                    }
                }

                if(strInitScript!="")
                {
                    strUrl=strUrl+"&initScript="+ encodeURI(strInitScript);
                }
                strUrl+= "&rdm="+Math.random();

                $("#frmChild").attr("src", strUrl);
                $("#tabMenu td[id ^= 'td']").attr("class", "tabcontrol_button03");
                $("#" + varCtrl.id).attr("class", "tabcontrol_button05");
                
                //排除上传附件
                if (strTabName == "-1") {
                    $("#saveform").hide();
                }
                else {
                    $("#saveform").show();
                    
                }//判断是否是材料上传--结束
                //end
           }
        }

        //页面加载时执行
        $(document).ready(function() {
            $("#tabMenu td:first").click();

            //加载对话框
//            var cookie_string = getCookie("XDcookie");
//            var dialogflag = '<%=Request["DialogFlag"]%>'; // 模式对话框显示类型
//            if (dialogflag == "Show") {
//                if (cookie_string == "0" || cookie_string == "") {
//                     OpenConfirm();
//                }
//            }

            //设置打印按钮的可操作性
            var printFlag = $("#<%=hidPrintFlag.ClientID%>").val();          
            if (printFlag == "2" || printFlag == "3" || printFlag == "-4") {
                $("#printform").show();
                 $("#printformView").show();
            }
        });

        //打开向导模式
        function openWizardUrl() {
            window.location.href = "ShenBaoWithWizard.aspx?IId=" + GetIID() + "&flowname=" + strFlowName+ "&flowtype=" + strFlowType;
        }

        //拷贝并添加
        function CopyAndAdd()
        {
            //SaveInfo();
            var strIID = GetIID();
            //拷贝当前数据并新建
            $.ajax({
                    url: 'AjaxCopyAndAdd.ashx',
                    data: "flowname=" + strFlowName + "&flowtype=" + strFlowType+"&iid="+strIID,
                    type: 'POST',
                    dataType: 'text',
                    async: false,
                    success: function (newUrl) {
                        if (newUrl != "") {
                            window.location.href = newUrl;
                        }
                    }
                });
        }

        //打开提示框
        function OpenConfirm() {
            //$('#divConfirm').window('resize', { height: 150, width: 400 });
            //$('#divConfirm').window('expand', '测试');
            $('#divConfirm').window('open');
        }

        //保存消息提示框的cookie
        function SaveCookie() {
            var ckvalue = "0";
            if ($("#cbxNotes").is(":checked")) {
                ckvalue = "1";
            }
            else {
                ckvalue = "0";
            }
            setCookie('XDcookie', ckvalue);
            $('#divConfirm').window('close');
        }
        //打印
        function framePrint() {
            if ($("#frmChild").attr("src") != "") {
                window.open($("#frmChild").attr("src") + "&act=Print", "printfrm", "width=1,height=1");
            }
        }
        //打印预览
        function framePrintPreview() {
            if ($("#frmChild").attr("src") != "") {
                window.open($("#frmChild").attr("src") + "&act=PrintView", "printfrm");
            }
        }
    </script>
    <div id="divConfirm" class="easyui-window" title="国家测绘地理信息局行政许可网上办理大厅" data-options="modal:true,closed:true,iconCls:'icon-save',minimizable:false,maximizable
:false,collapsible:false" style="width: 400px; height: 150px; padding: 10px;">
        在线申报有普通模式和向导模式两种方式，向导模式可以引导您进行在线申报，进入向导模式<a href="#" onclick="openWizardUrl();">点击这里</a>。
        <table style="width: 100%;">
            <tr>
                <td>
                    <input id="cbxNotes" type="checkbox" />下次不再提示
                </td>
                <td style="text-align: right">
                    <input id="btnOK" class="button" onclick="SaveCookie()" value="确定" type="button" /><input
                        id="btnClose" class="button" value="关闭" type="button" onclick="$('#divConfirm').window('close')" />
                </td>
            </tr>
        </table>
    </div>
    <input type="hidden" value="" id="hidIId" runat="server" />
    <input type="hidden" value="" id="hidBgFlag" runat="server" />
    <input type="hidden" value="" id="hidYqFlag" runat="server" />
    <input type="hidden" value="" id="hidPrintFlag" runat="server" />
    <div>
        <table border="0" width="100%" id="tabMenu" style="width: 799px; height: 30px; background-image: url(../App_Themes/SkinFile/Images/shenbao.gif);">
            <tr>
                <asp:Repeater ID="RepeaterWeb" runat="server">
                    <ItemTemplate>
                        <%#GetContent(Container)%>
                    </ItemTemplate>
                </asp:Repeater>
                <%= GenerateOtherTabs()%>
                <td align="right">
                    <%=GetSubmit()%>
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td>
                    <iframe name="frmChild" id="frmChild" width="800" onload="SetWinHeight(this);" frameborder="0"
                        scrolling="no"></iframe>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
