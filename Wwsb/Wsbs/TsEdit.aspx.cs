﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SbBusiness.PubInfo;
using SbBusiness;

public partial class Wsbs_TsEdit : System.Web.UI.Page
{
    TsInfoManager tsManager = new TsInfoManager();
    private string strId = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request["id"] != "")
            strId = Request["id"];
        if (!IsPostBack)
        {
            if (strId == null || strId == "")
            {
                if (Session["LoginUserName"] != null)
                {
                    txtTsr.Text = Session["LoginUserName"].ToString();
                }
            }
            else
            {
                DataTable dt = tsManager.GetTs(strId);

                if (dt.Rows.Count > 0)
                {
                    txtTsr.Text = dt.Rows[0]["TSR"].ToString();
                    txtTime.Text = ((DateTime)dt.Rows[0]["TSSJ"]).ToString("yyyy-MM-dd");
                    txtBtsdx.Text = dt.Rows[0]["BTSR"].ToString();
                    txtTsiid.Text = dt.Rows[0]["IID"].ToString();
                    txtTsContent.Text = dt.Rows[0]["TSNR"].ToString();
                }
            }
        }
    }

    /// <summary>
    /// Handles the OnClick event of the btnQuery control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void btnSave_OnClick(object sender, EventArgs e)
    {
        if (strId == null || strId == "")
        {
            tsManager.InsertTsInfo(txtTsr.Text, txtBtsdx.Text, txtTime.Text,
                    txtTsiid.Text, txtTsContent.Text);
        }
        else
        {
            tsManager.UpdateTsInfo(strId, txtTsr.Text, txtBtsdx.Text, txtTime.Text,
                   txtTsiid.Text, txtTsContent.Text);
        }

        Response.Redirect("TsManager.aspx");
    }

    #region 返回菜单管理主界面
    protected void btnReturn_Click(object sender, EventArgs e)
    {
        Response.Redirect("TsManager.aspx");
    }
    #endregion
}
