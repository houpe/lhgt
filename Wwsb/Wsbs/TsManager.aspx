﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TsManager.aspx.cs" Inherits="Wsbs_TsManager" %>

<%@ Register Src="../UserControls/Calendar.ascx" TagName="Calendar" TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>投诉管理</title>

    <script language="javascript" type="text/javascript" src="../ScriptFile/Regex.js"></script>

</head>
<body>
    <form id="form1" runat="server">
        <div style="text-align: center;">
        <table class="tab_Content">
            <tr>
                <td>
                    投诉人:
                </td>
                <td>
                    <asp:TextBox ID="txtTsr" runat="server" SkinID="InputGreen"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    被投诉对象:
                </td>
                <td>
                    <asp:TextBox ID="txtBstr" runat="server" SkinID="InputGreen"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    开始投诉时间:
                </td>
                <td>
                    <uc3:Calendar ID="txtFromTssj" runat="server" Width="150" />
                </td>
            </tr>
            <tr>
                <td>
                    结束投诉时间:
                </td>
                <td>
                    <uc3:Calendar ID="txtToTssj" runat="server" Width="150" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Button ID="btnQuery" Text="查询" OnClick="btnQuery_OnClick" runat="server" />
                    <asp:Button ID="Button1" runat="server" Text="添加新投诉" OnClick="btnAdd_Click"/>
                </td>
            </tr>
        </table>        
        <br />
        </div>
        <div style="text-align: center;">
            <defineControls:CustomGridView ID="CustomGridView1" runat="server" AllowPaging="True" AutoGenerateColumns="false"
                ShowItemNumber="false" SkinID="List" CheckboxAutoPostBack="False" CustomImportToExcel="false"
                CustomSortExpression="" DeleteToolTipOfCol="-1" HideTextLength="20" ShowCmpAndCancel="False"
                ShowGridViewSelect="List" ShowHideColModel="None" ShowImportButton="False" ShowTreeView="False"
                SelectOrDelete="True" AllowSortingAscImgCss="" AllowSortingDescImgCss="" Width="700px"
                OnOnLoadData="CustomGridView1_OnLoadData">
                <Columns>
                    <asp:BoundField DataField="序号" HeaderText="序号" />
                    <asp:BoundField DataField="iid" HeaderText="申报案件审批编号" />
                    <asp:BoundField DataField="tsr" HeaderText="投诉人" />
                    <asp:BoundField DataField="btsr" HeaderText="被投诉对象" />
                    <asp:BoundField DataField="tssj" HeaderText="投诉时间" />
                    <asp:BoundField DataField="tsnr" HeaderText="投诉内容" />
                    <asp:TemplateField HeaderText="编辑">
                        <ItemTemplate>
                            <asp:HyperLink ID="hlEdit" runat="server" NavigateUrl='<%# DataBinder.Eval(Container.DataItem,"ID", "TsOnline.aspx?id={0}&action=edit") %>'>编辑</asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>                        
                    <asp:TemplateField HeaderText="删除">
                        <itemtemplate>
                            <a href='<%# DataBinder.Eval(Container.DataItem,"ID", "TsManager.aspx?action=delete&id={0}") %>'>删除</a>
                        </itemtemplate>  
                    </asp:TemplateField>                  
                </Columns>
            </defineControls:CustomGridView>
        </div>
    </form>
</body>
</html>
<script language="javascript" type="text/javascript" src="../ScriptFile/AutoHeightAndWidthDouble.js"></script>
