﻿<%@ WebHandler Language="C#" Class="ajaxInitFormValue" %>

using System;
using System.Web;

public class ajaxInitFormValue : IHttpHandler
{
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/plain";
        string strFlowName = context.Server.UrlDecode(context.Request["flowname"]);
        string strTabName = context.Server.UrlDecode(context.Request["tabName"]);
        string strUserId = context.Server.UrlDecode(context.Request["userId"]);
        context.Response.Write(PostData(strFlowName, strTabName, strUserId));
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

    public string PostData(string strFlowName,string strTabName,string strUserId)
    {
        string strSqlData = string.Empty;//获取初始化设定
        string strTableName = string.Empty;//表名
        string strColName = string.Empty;//字段名
        string strDefaultValue = string.Empty;//初始值
        string strJSValue = string.Empty;        
        
        //获取所有的默认表和字段
        string strWhere = string.Format(" and userid='{0}'", strUserId);//初始化设定条件

        SbBusiness.Wsbs.XtFormDefaultOperation xfdoTemp = new SbBusiness.Wsbs.XtFormDefaultOperation();
        System.Data.DataTable dtTable = xfdoTemp.GetFlowDefault(strFlowName, strTabName);
        for (int i = 0; i < dtTable.Rows.Count; ++i)
        {
            strTableName += string.Format("'{0}',", dtTable.Rows[i]["target_tab_name"]);
            strColName += string.Format("'{0}',", dtTable.Rows[i]["target_tabcol_name"]);

            strSqlData = dtTable.Rows[i]["logic_sql"].ToString();

            string strTempValue = WF_Business.SysParams.OAConnection().GetValue(strSqlData + strWhere);
            if (!string.IsNullOrEmpty(strTempValue))
            {
                strDefaultValue += string.Format("{0},", strTempValue);
            }
            else//为空也要加，否则对应不上
            {
                strDefaultValue += ",";
            }
        }

        if (!string.IsNullOrEmpty(strTableName) && strDefaultValue.Length>1)
        {
            strTableName = strTableName.Substring(0, strTableName.Length - 1);
            strColName = strColName.Substring(0, strColName.Length - 1);
            strDefaultValue = strDefaultValue.Substring(0, strDefaultValue.Length - 1);

            strJSValue += string.Format("initdefaultvalue(\"{0}\",\"{1}\",\"{2}\");", strTableName, strColName, strDefaultValue);
        }
        
        return strJSValue;
    }

}