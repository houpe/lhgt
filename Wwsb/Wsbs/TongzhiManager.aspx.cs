﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using SbBusiness;
using SbBusiness.Wsbs;

public partial class Wsbs_TongzhiManager : System.Web.UI.Page
{
    public string strTitle = "消息详细信息";//消息标题
    public string strMessage = "";//消息内容
    Messagebox msgOperation = new Messagebox();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.Page.IsPostBack)
        {
            if (Request.QueryString["messageid"] != null)
            {
                string strMegID = Request.QueryString["messageid"].ToString();
                BindMassage(strMegID);
                //设置消息读取状态
                msgOperation.UpdateMessageType(strMegID, "1");
            }
        }
    }
    /// <summary>
    /// 绑定消息详细信息
    /// </summary>
    /// <param name="messageid">消息ID</param>
    /// <!--addby zhongjian 20090901-->
    private void BindMassage(string messageid)
    {
       string strTime = "";//消息发送时间
       string strPhoneNO = "";//联系电话
        DataTable dt = msgOperation.GetMessageBoxById(messageid);
        if (dt != null)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];
                strMessage = dr["messagetext"].ToString();
                strTime = dr["plansendtime"].ToString();
                strPhoneNO = dr["phoneno"].ToString();
            }
        }
    }
}
