﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SbBusiness.Wsbs;

using SbBusiness.User;
using SbBusiness;

using SbBusiness.Menu;
using System.Collections.Generic;
using Business.Admin;

public partial class Wsbs_TsOnline : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string action = Request.QueryString["action"];

        if (!IsPostBack)
        {
            BindDepList();
            if (!string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                //PersistenceControl1.PageControl = this.form1;
                PersistenceControl1.KeyValue = Request.QueryString["id"];
                PersistenceControl1.Status = "edit";
                PersistenceControl1.Render();
            }

            if (Session["LoginUserName"] != null)
            {
                txtTSR.Text = Session["LoginUserName"].ToString();
            }
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(txtIID.Text.Trim()))
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "ok", "<script>alert('请选择投诉编号！')</script>");
            return;
        }
        if (txtTSNR.Text.Trim().Length > 3000)
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "ok", "<script>alert('输入的内容不能太多！')</script>");
            return;
        }

        //PersistenceControl1.PageControl =this.form1;
        PersistenceControl1.Update();
        Page.ClientScript.RegisterStartupScript(this.GetType(), "ok", "<script>alert('投诉已保存成功！')</script>");
        //Response.Redirect(string.Format("TsManager.aspx?wid={0}",Request["wid"]));
    }

    protected void btnReturn_Click(object sender, EventArgs e)
    {
        Response.Redirect(string.Format("TsManager.aspx?wid={0}", Request["wid"]));
    }

    /// <summary>
    /// 绑定咨询部门
    /// </summary>
    /// <!--addby zhongjian 20091201-->
    public void BindDepList()
    {
        DepartHandle dhTemp = new DepartHandle();
        DataTable dtTable = dhTemp.GetSecondDepart();
        ddlTsbm.Items.Clear();
        ddlTsbm.Items.Add(new ListItem("请选择审批部门...", "0"));
        foreach (DataRow row in dtTable.Rows)
        {
            ddlTsbm.Items.Add(new ListItem(row["depart_name"].ToString(), row["departid"].ToString()));
        }
    }
}
