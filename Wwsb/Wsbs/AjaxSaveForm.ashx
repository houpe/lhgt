﻿<%@ WebHandler Language="C#" Class="AjaxSaveForm" %>

using System;
using System.Web;
using System.Data;

public class AjaxSaveForm : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/plain";
        string strFlowName = context.Server.UrlDecode(context.Request["dataFlowname"]);
        string strFlowType = context.Server.UrlDecode(context.Request["flowtype"]);
        string strUserID = context.Server.UrlDecode(context.Request["userId"]);
        string strUserName = context.Server.UrlDecode(context.Request["userName"]);
        context.Response.Write(PostIId(context.Request["dataIId"], strFlowName, strUserID, strUserName,strFlowType));
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    public string PostIId(string strIId, string strFlowname, string strUserID, string strUserName, string strFlowType)
    {
        int nReturnSubmit = 0;
        SbBusiness.Wsbs.ShenBaoSubmit submit = new SbBusiness.Wsbs.ShenBaoSubmit();

        nReturnSubmit = submit.InsertSubmitInfo(strIId, strFlowname, strUserID);
        if (nReturnSubmit == 1)
        {
            //保存成功后发送消息 editby zhongjian 20091208
            SbBusiness.Wsbs.Messagebox message = new SbBusiness.Wsbs.Messagebox();
            DataTable dtTemp = message.GetRequestSet(0, "尚未提交");
            string strMsgContent = string.Empty;
            string strStepNo = string.Empty;
            string strMessageID = string.Empty;
            if (dtTemp.Rows.Count > 0)
            {
                strStepNo = dtTemp.Rows[0]["step_no"].ToString();
                strMsgContent = dtTemp.Rows[0]["step_msg"].ToString();
                if (!string.IsNullOrEmpty(strMsgContent))//当消息内容设置为空时，将不再发送消息。update by zhongjian 20091230
                {
                    try
                    {
                        strMsgContent = string.Format(strMsgContent, strUserName, strFlowType);
                    }
                    catch { }
                    strMessageID = message.GetMessageID(strStepNo, strUserID);
                    if (!string.IsNullOrEmpty(strMessageID))
                    {
                        //修改消息
                        message.UpdateMeaasge(strMsgContent, "", strUserName, strUserID, strMessageID);                       
                    }
                    else
                    {
                        //添加消息 addby zhongjian 20091230
                        message.InsertMeaasge(strMsgContent, "", strUserName, strUserID);                        
                    }
                }
                message.UpdateUserStepNO(strUserID, strStepNo);

            }
            return string.Empty;
        }

        return "保存失败";
    }
}