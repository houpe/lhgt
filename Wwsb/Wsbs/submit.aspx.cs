﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SbBusiness.Wsbs;
using SbBusiness;
using System.Drawing;

public partial class Wsbs_submit : System.Web.UI.Page
{
    private SerialInstance cInstance = new SerialInstance();
    private ShenBaoSubmit cSubmit = new ShenBaoSubmit();
    protected void Page_Load(object sender, EventArgs e)
    {    
        if (!IsPostBack)
        {
            SetDelete();
            BindData();
        }
    }

    /// <summary>
    /// 处理列表列的展示
    /// </summary>
    private void ProcessGridCols()
    {
        string strCurrentSys = ConfigurationManager.AppSettings["currentSysNam"];
        string[] arrSysParams = strCurrentSys.Split(':');
        foreach (string strTemp in arrSysParams[1].Split(','))
        {
            for (int i = 0; i < CustomGridView1.Columns.Count; i++)
            {
                if (strTemp == CustomGridView1.Columns[i].HeaderText)
                {
                    CustomGridView1.Columns[i].Visible = true;
                }
            }
        }
    }

    private void BindData()
    {
        if (Session["UserId"] != null)
        {
            string strRenWu = "当前任务";
            string strUserId = Session["UserId"].ToString();
            string strFlag = Request["flag"];
            
            DataTable dtSource = new DataTable();
            string strIID = txtIID.Text.Trim();

            if (strFlag == "0")
            {
                strRenWu = "尚未提交事项";
                dtSource = cSubmit.GetNoSubmit(strUserId,strIID,"");
                this.CustomGridView1.Columns[6].Visible = false;//受理号
                this.CustomGridView1.Columns[7].HeaderText = "创建时间";
                this.CustomGridView1.Columns[8].HeaderText = "修改时间";
                this.CustomGridView1.Columns[9].HeaderText = "剩余时间";
                this.CustomGridView1.Columns[12].Visible = true;//操作列
                this.CustomGridView1.EmptyDataText = "没有尚未提交的事项。";
            }
            else if (strFlag == "1")//正在办理
            {
                strRenWu = "当前办理事项";
                dtSource = cInstance.GetZzblInstance(strUserId, strIID, "");
                this.CustomGridView1.Columns[8].Visible = false;//接件时间
                this.CustomGridView1.Columns[9].Visible = false;//完成时间
                this.CustomGridView1.Columns[10].Visible = true;//审核状态

                //this.CustomGridView1.Columns[13].Visible = true;//异常变更申请列
                this.CustomGridView1.EmptyDataText = "没有当前办理的事项。";
            }
            else if (strFlag == "2")
            {
                strRenWu = "补正补齐办理事项";
                dtSource = cSubmit.GetBuZhengSubmit(strUserId, strIID, "");

                this.CustomGridView1.Columns[6].Visible = false;//受理号
                this.CustomGridView1.Columns[8].HeaderText = "处理时间";//接收时间
                this.CustomGridView1.Columns[9].HeaderText = "补正补齐期限";//补正补齐期限

                this.CustomGridView1.Columns[11].Visible = true;        //补正原由
                this.CustomGridView1.Columns[11].HeaderText = "补正原由";
                this.CustomGridView1.Columns[12].Visible = true;//操作列
                this.CustomGridView1.EmptyDataText = "没有补正补齐办理的事项。";
            }
            else if (strFlag == "3")
            {
                strRenWu = "通过办理事项";
                dtSource = cInstance.GetCompleteInstance(strUserId, "2,4", strIID);

                this.CustomGridView1.Columns[6].HeaderText = "审图号";//审图号
                this.CustomGridView1.Columns[8].Visible = false;//接收时间
                this.CustomGridView1.EmptyDataText = "没有通过办理的事项。";
            }
            else if (strFlag == "4")
            {
                strRenWu = "驳回办理事项";
                dtSource = cInstance.GetBoHuiHistory(strUserId, strIID, "");
                this.CustomGridView1.Columns[8].HeaderText = "驳回时间";//接收时间
                this.CustomGridView1.Columns[9].Visible = false;//完成时间

                this.CustomGridView1.Columns[11].Visible = true;
                this.CustomGridView1.Columns[11].HeaderText = "审批意见";
                this.CustomGridView1.EmptyDataText = "没有驳回办理的事项。";
            }
            else if (strFlag == "5")
            {
                strRenWu = "不予受理事项";
                dtSource = cInstance.GetNoPass(strUserId, strIID, "-3");
                this.CustomGridView1.Columns[6].Visible = false;//受理号
                this.CustomGridView1.Columns[8].Visible = false;//接收时间
                this.CustomGridView1.Columns[11].Visible = true;
                this.CustomGridView1.Columns[11].HeaderText = "审批意见";

                this.CustomGridView1.Columns[12].Visible = false;//操作列
                this.CustomGridView1.EmptyDataText = "没有不予受理事项的事项。";
            }
            else if (strFlag == "6")
            {
                strRenWu = "未通过办理事项";
                dtSource = cInstance.GetCompleteInstance(strUserId, "-4", strIID);
                this.CustomGridView1.Columns[8].Visible = false;//接收时间
                this.CustomGridView1.EmptyDataText = "没有未通过办理的事项。";
            }

            Session["DangQianRenWu"] = strRenWu;
            this.CustomGridView1.DataSource = dtSource;
            this.CustomGridView1.PageSize = SystemConfig.PageSize;
            this.CustomGridView1.RecordCount = dtSource.Rows.Count;
            this.CustomGridView1.DataBind();

            //处理列表的显示
            ProcessGridCols();
        }
        else
        {
            if (Session["MainPage"] != null)
            {
                Response.Redirect(Session["MainPage"].ToString());
            }
            else
            {
                Response.Redirect("~/" + SystemConfig.LoginPage);
            }
        }
    }

    protected void CustomGridView1_OnLoadData(object sender, EventArgs e)
    {
        BindData();
    }

    /// <summary>
    /// 获取办理事项查询地址
    /// </summary>
    /// <param name="row"></param>
    /// <returns></returns>
    /// <!--addby zhongjian 20091021-->
    protected string GetUrl(IDataItemContainer row)
    {
        string strHtml = string.Empty;
        string strFlowName = DataBinder.Eval(row.DataItem, "flowname").ToString();
        string strFlowType = DataBinder.Eval(row.DataItem, "flowtype").ToString();
        string strIId = ((decimal)DataBinder.Eval(row.DataItem, "IID")).ToString();
       
        strFlowName = Server.UrlEncode(strFlowName);
        strFlowType = Server.UrlEncode(strFlowType);

        //string strFlag = Request["flag"];
        //strHtml = string.Format("<a href='WsbsMain.aspx?type=3&Flag={3}&flowname={0}&iid={1}&flowtype={2}' target='_blank'>{1}</a>",
        //    strFlowName, strIId, strFlowType, strFlag);

        strHtml = string.Format("<a href='shenbao.aspx?flowname={0}&iid={1}&flowtype={2}' target='_blank'>{1}</a>",
            strFlowName, strIId, strFlowType);
        return strHtml;
    }

    /// <summary>
    /// 绑定审批状态
    /// </summary>
    /// <param name="row"></param>
    /// <returns></returns>
    protected string GetQuery(IDataItemContainer row)
    {
        string strHtml = string.Empty;

        string strIId = ((decimal)DataBinder.Eval(row.DataItem, "IID")).ToString();
        string strSubmitFlag = ((decimal)DataBinder.Eval(row.DataItem, "SUBMITFLAG")).ToString();

        DataTable dtSource = cInstance.GetSerialList(strIId);
        if (dtSource.Rows.Count > 0)
        {
            string strName = "正在办理";
            for (int i = 0; i < dtSource.Rows.Count; ++i)
            {
                string strStatus = dtSource.Rows[i]["status"].ToString();
                if (strStatus == "2")//当是已办结或已退回时
                {
                    strName = "已办结";
                }
            }
            strHtml = strName;

            //string strFlag = Request["flag"];
            //strHtml = string.Format("<a href='SerialList.aspx?serial={0}&Flag={1}'>{2}</a>", strIId, strFlag,strName);
        }
        else
        {
            strHtml = "预审";

            if (strSubmitFlag == "3")
            {
                strHtml = "预审完成(可打印)";
            }
        }
        return strHtml;
    }

    /// <summary>
    /// 删除尚未提交事项
    /// </summary>
    /// <param name="row"></param>
    /// <!--addby zhongjian 20091218-->
    protected void SetDelete()
    {
        if (Request["action"] =="delete")
        {
            string strIId = Request["iid"];
            bool flag = cSubmit.DeleteSubmit(Session["UserId"].ToString(),strIId);
            string strReturn = string.Empty;
            if (flag)
            {
                strReturn = "OK";
                BindData();
            }
        }
    }

    /// <summary>
    /// 行创建
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void CustomGridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HyperLink hlTemp = e.Row.FindControl("hlDelete") as HyperLink;

            DataRowView dtr = (DataRowView)e.Row.DataItem;

            if (dtr != null)
            {
                hlTemp.Attributes.Add("onclick", "return confirm('您确定要删除吗？')");//为删除添加提示
                hlTemp.NavigateUrl = string.Format("submit.aspx?flag={1}&iid={0}&action=delete",
                    dtr["iid"], Request["flag"]);
            }
        }
    }

    public string GetRemark(IDataItemContainer row)
    {
        string strRemark = DataBinder.Eval(row.DataItem, "Remark").ToString();
        if (strRemark.Length<15)
            return strRemark;
        else
        {
            strRemark = strRemark.Substring(0, 15)+"...";
            return strRemark;
        }
    }

    protected void btnQuery_Click(object sender, EventArgs e)
    {
        BindData();
    }
}
