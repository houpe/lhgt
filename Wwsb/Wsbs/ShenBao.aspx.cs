﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SbBusiness.Wsbs;


public partial class Wsbs_ShenBao : System.Web.UI.Page
{
    /// <summary>
    /// 页面加载
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LoginUserName"] == null)
        {
            Response.Redirect("~/" + SbBusiness.SystemConfig.LoginPage);
        }

        if (string.IsNullOrEmpty(hidIId.Value))//如果为空则获取iid
        {
            if (!string.IsNullOrEmpty(Request["iid"]) && Request["iid"] != "-1")
            {
                hidIId.Value = Request["iid"];
            }
            else//未获取到iid或者iid为-1时，重新生成一个iid
            {
                SerialInstance sioTemp = new SerialInstance();
                long decValue = sioTemp.GetIIDWithEnterprise("", "", "");
                hidIId.Value = decValue.ToString();
            }

            //Session["IidForNavigation"] = hidIId.Value;//用途左侧导航菜单留存iid
        }

        if (!IsPostBack)
        {
            //获取当前办件的业务状态
            hidPrintFlag.Value = ShenBaoSubmit.GetIsSubmit(hidIId.Value);

            BindTitleMenu();
        }

        ViewState["UserName"] = Server.UrlEncode(Session["LoginUserName"].ToString());
    }

    /// <summary>
    /// 绑定TAB菜单按钮 
    /// </summary>
    protected void BindTitleMenu()
    {
        XtProcess cProcess = new XtProcess();
        string strFlowName = Request["flowname"];
        DataTable dtTable = cProcess.GetProcess(strFlowName);
        RepeaterWeb.DataSource = dtTable;
        RepeaterWeb.DataBind();
    }

    /// <summary>
    /// 设置按钮样式
    /// </summary>
    /// <returns></returns>
    protected string GetSubmit()
    {   
        string strIid = hidIId.Value;
        string strSaveBtnStyle = string.Empty;
        string strSubmitBtnStyle = string.Empty;
        string strPrintBtnStyle = string.Empty;
        string strWizardBtnStyle = string.Empty;

        if (!string.IsNullOrEmpty(strIid))
        {
            string strSubmit = hidPrintFlag.Value;

            //0:未提交;1:已提交;2:已通过;3:预审;-1:返回补证;-2:驳回中止;-3:不予受理;-4:审批不通过
            //已经提交或者在内网受理的案件均不能再次保存提交
            if (strSubmit == "1" || strSubmit == "2" || strSubmit == "3" || strSubmit == "-2" || strSubmit == "-3" || strSubmit == "-4")
            {
                if (hidBgFlag.Value != "3" && hidYqFlag.Value != "3")
                {
                    strSaveBtnStyle = "disabled='disabled'";
                    strSubmitBtnStyle = "disabled='disabled'";
                }
                else
                {
                    strWizardBtnStyle = "disabled='disabled'";
                    if (hidYqFlag.Value == "3")
                    {
                        strSaveBtnStyle = "disabled='disabled'";
                    }
                }
            }

            if (strSubmit == "0" || strSubmit == "1" || strSubmit == "-1" || strSubmit == "-3" || strSubmit == "-4" || strSubmit == "")//预审通过时,才能打印
            {
                strPrintBtnStyle = "disabled='disabled'";
            }
        }
        else
        {
            strSaveBtnStyle = "disabled='disabled'";
            strSubmitBtnStyle = "disabled='disabled'";
            strPrintBtnStyle = "disabled='disabled'";
            strWizardBtnStyle = "disabled='disabled'";
        }

        return string.Format(@"<input id='btnAddCopy' type='button' value='复制添加'  onclick='CopyAndAdd()'  class='zxsbButton'/><input id='saveform' type='button' value=' 保 存 ' {0} onclick='SaveInfo()'  class='zxsbButton'/>&nbsp;<input id='submitform' type='button' value=' 提 交 ' {1} onclick='SubmitInfo()' class='zxsbButton'/>&nbsp;<input id='printform' type='button' value=' 打 印 ' {2} onclick='framePrint()' class='zxsbButton' />&nbsp;<input id='printformView' type='button' value='打印预览' {2} onclick='framePrintPreview()' class='zxsbButton' />&nbsp;<input id='btnWizard' type='button' value='向导模式' {3} onclick='openWizardUrl()' class='zxsbButton' />", strSaveBtnStyle, strSubmitBtnStyle, strPrintBtnStyle, strWizardBtnStyle);
        
    }

    /// <summary>
    /// 遍历各表生成tab选项卡
    /// </summary>
    /// <param name="row"></param>
    /// <returns></returns>
    protected string GetContent(IDataItemContainer row)
    {
        string strHtml = string.Empty;

        string strServername = Request.Url.Authority;
        string strNewwebdisplay = ConfigurationManager.AppSettings["NewWebDisplay"];
        string strIId = hidIId.Value;
        string strTitle = (string)DataBinder.Eval(row.DataItem, "TABLEANOTHERNAME");
        string strFid = (string)DataBinder.Eval(row.DataItem, "FID");

        //默认添加权限打开参数--针对webservice2
        if (strNewwebdisplay.IndexOf("?") == -1)
        {
            strNewwebdisplay += "?readonly=false&input_index=0";
        }
        else
        {
            strNewwebdisplay += "&readonly=false&input_index=0";
        }

        //各表格对应的选项卡
        strHtml = string.Format("<td id='td{2}' onclick='SetBackColor(this,\"http://{0}{1}&fid={2}&userid=-1&iid={3}&step=-1\",\"{5}\")' class='tabcontrol_button03'>{4}</td>", strServername, strNewwebdisplay, strFid, strIId, strTitle, Server.UrlEncode(strTitle));

        return strHtml;
    }

    /// <summary>
    /// 生成表单之外的其他选项卡
    /// </summary>
    /// <returns></returns>
    protected string GenerateOtherTabs()
    {
        //说明函
        SerialInstance sioGloab = new SerialInstance();
        string strFid = sioGloab.GetFirstFidByWname("说明函");

        string strServername = Request.Url.Authority;
        string strNewwebdisplay = ConfigurationManager.AppSettings["NewWebDisplay"];
        string strIId = hidIId.Value;

        //默认添加权限打开参数--针对webservice2
        if (strNewwebdisplay.IndexOf("?") == -1)
        {
            strNewwebdisplay += "?readonly=false&input_index=0";
        }
        else
        {
            strNewwebdisplay += "&readonly=false&input_index=0";
        }
        string strHtml = string.Empty;

        //一致性说明函打印
        if (hidPrintFlag.Value == "3")//预算通过后状态才能打印
        {
            strHtml = string.Format("<td id='td{2}' onclick='SetBackColor(this,\"http://{0}{1}&fid={2}&userid=-1&iid={3}&step=-1\",\"{4}\");' class='tabcontrol_button03'>说明函</td>", strServername, strNewwebdisplay, strFid, strIId, Server.UrlEncode("说明函"));
        }

        string strCaiLiaoTitle = "查看材料";
        string strSubmitFlag = hidPrintFlag.Value;
        //0:未提交;-1:返回补证;空 均可采用上传材料
        if (strSubmitFlag == "0" || strSubmitFlag == "-1" || string.IsNullOrEmpty(strSubmitFlag))
        {
            strCaiLiaoTitle = "上传材料";
        }

        strHtml += string.Format("<td id=\"tdSccl\" onclick='SetBackColor(this,\"../Common/SbFileUploadNew.aspx?flowname={0}&NewsId={1}\",\"-1\");' class=\"tabcontrol_button03\">{2}</td>", Request["flowname"], strIId, strCaiLiaoTitle);

        return strHtml;
    }
}
