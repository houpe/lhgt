﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SbBusiness.Wsbs;

using SbBusiness.User;
using SbBusiness;

using SbBusiness.Menu;

public partial class Wsbs_LyzxMessage : System.Web.UI.Page
{
    SerialInstance instanceOperation = new SerialInstance();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindLyzxInfo();
        }
    }

    /// <summary>
    /// 绑定留言咨询详细信息
    /// </summary>
    /// <!--addby zhongjian 20091028-->
    public void BindLyzxInfo()
    {
        string strID = Request["id"];
        DataTable dtSource = instanceOperation.GetLyzxInfo(strID, false, 0, 50,"","","","","");
        lblData.Text = dtSource.Rows[0]["createdate"].ToString();
        lblUser.Text = dtSource.Rows[0]["name"].ToString();
        lblTitle.Text = dtSource.Rows[0]["title"].ToString();
        lblNotes.Text = dtSource.Rows[0]["notes"].ToString();
    }

    /// <summary>
    /// 绑定留言咨询回馈信息
    /// </summary>
    /// <!--addby zhongjian 20091103-->
    protected string BindMessageFK()
    {
        string strHTML = string.Empty;
        string strData = string.Empty;//回复时间
        string strNotes = string.Empty;//回复内容
        string strUserName = string.Empty;//回复人员名称
        string strUserID = string.Empty;//回复人员ID
        string strFKID = string.Empty;//回复ID
        string strID = Request["id"];

        DataTable dtTemp = instanceOperation.GetLyzxFKInfo(strID);
        if (dtTemp.Rows.Count > 0)
        {
            for (int i = 0; i < dtTemp.Rows.Count; ++i)
            {
                strData = dtTemp.Rows[i]["fk_date"].ToString();
                strUserName = dtTemp.Rows[i]["user_name"].ToString();
                strNotes = dtTemp.Rows[i]["fk_notes"].ToString();
                strUserID = dtTemp.Rows[i]["fk_userid"].ToString();
                strFKID = dtTemp.Rows[i]["id"].ToString();

                strHTML += string.Format(@"<div class='page_bar' style='width: 100%; text-align: center;'>
                <div class='tdbox'>
                    <table>
                        <tr>
                            <td style='width: 45%;'>
                               {0}
                            </td>
                            <td>
                                回复内容如下：
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <br />
            <div style='margin-left: 0px; text-align:left;width: 80%;'>                    
                {1}
            </div>", strData, strNotes);
            }
        }
        return strHTML;
    }
}
