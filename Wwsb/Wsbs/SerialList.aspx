﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master"
    CodeFile="SerialList.aspx.cs" Inherits="Wsbs_SerialList" Title="当前办理事项进度查看" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">

    <script type="text/javascript">
        function ProgressView(url) {
            window.open(url, "proWin", "width=700,height=450,top=50");
        }
    </script>

    <div style="text-align: center;">
        <defineControls:CustomGridView ID="CustomGridView1" runat="server" AllowPaging="True" AutoGenerateColumns="false"
            ShowItemNumber="false" SkinID="List" CheckboxAutoPostBack="False" CustomImportToExcel="false"
            CustomSortExpression="" DeleteToolTipOfCol="-1" HideTextLength="20" ShowCmpAndCancel="False"
            ShowGridViewSelect="List" ShowHideColModel="None" ShowImportButton="False" ShowTreeView="False"
            SelectOrDelete="True" AllowSortingAscImgCss="" AllowSortingDescImgCss="" Width="700px"
            OnOnLoadData="CustomGridView1_OnLoadData">
            <Columns>
                <asp:BoundField DataField="serial" HeaderText="案件编号" />
                <asp:BoundField DataField="办理环节编号" HeaderText="办理环节编号" />
                <asp:BoundField DataField="办理人员" HeaderText="办理人员" />
                <asp:BoundField DataField="办理处室" HeaderText="办理处室" />
                <asp:BoundField DataField="办理时间" HeaderText="办理时间" />
            </Columns>
        </defineControls:CustomGridView>
        <asp:Button ID="btnReturn" runat="server" Text="返回" OnClick="btnReturn_Click" />
    </div>
</asp:Content>
