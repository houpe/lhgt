﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="JggsPage.aspx.cs" Inherits="Wsbs_JggsPage" %>

<%@ Register Src="../UserControls/Calendar.ascx" TagName="Calendar" TagPrefix="uc3" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head2" runat="server">
    <title>结果公示</title>

    <script type="text/javascript">
        function ProgressView(url) {
            window.open(url, "proWin", "width=700,height=450,top=50");
        }
    </script>

</head>
<body>
    <div style="text-align: center">
        <form id="form1" runat="server">
        <div style="text-align: center; display: none;">
            <table class="tab_Content">
                <tr>
                    <td>
                        案件编号:
                    </td>
                    <td>
                        <asp:TextBox ID="txtIid" runat="server" SkinID="InputGreen"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        申请起始日期:
                    </td>
                    <td>
                        <uc3:Calendar ID="txtSQQSSJ" runat="server" Width="150" />
                    </td>
                </tr>
                <tr>
                    <td>
                        申请中止日期:
                    </td>
                    <td>
                        <uc3:Calendar ID="txtSQZZSJ" runat="server" Width="150" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Button ID="btnQuery" Text="查询" OnClick="btnQuery_OnClick" runat="server" />
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <div style="text-align: center;">
            <defineControls:CustomGridView ID="CustomGridView1" runat="server" AllowPaging="True" AutoGenerateColumns="true"
                ShowItemNumber="true" SkinID="List" CheckboxAutoPostBack="False" CustomImportToExcel="false"
                CustomSortExpression="" DeleteToolTipOfCol="-1" HideTextLength="20" ShowCmpAndCancel="False"
                ShowGridViewSelect="List" ShowHideColModel="None" ShowImportButton="False" ShowTreeView="False"
                SelectOrDelete="True" AllowSortingAscImgCss="" AllowSortingDescImgCss="" Width="700px"
                OnOnLoadData="CustomGridView1_OnLoadData">
            </defineControls:CustomGridView>
        </div>
        </form>
    </div>
</body>
</html>
