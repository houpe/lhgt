﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Tongzhi.aspx.cs" Inherits="Wsbs_Tongzhi" Title="通知查询" %>

<%@ Register Src="../UserControls/Calendar.ascx" TagName="Calendar" TagPrefix="uc3" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">
    <div style="text-align: center; display: <%=GetDisplay() %>;">
        <table class="tab_Content" style="margin-left: auto; margin-right: auto;">
            <tr>
                <td>
                    起始发送时间：
                </td>
                <td>
                    <uc3:Calendar ID="txtFromPlan" runat="server" Width="150" />
                </td>
                <td>
                    终止发送时间：
                </td>
                <td>
                    <uc3:Calendar ID="txtToPlan" runat="server" Width="150" />
                </td>
                <td>
                    <asp:Button ID="btnQuery" Text="查询" OnClick="btnQuery_OnClick" runat="server" />
                </td>
            </tr>
        </table>
    </div>
    <div style="text-align: left;">
        <defineControls:CustomGridView ID="CustomGridView1" runat="server" AllowPaging="True"
            AutoGenerateColumns="false" ShowItemNumber="true" SkinID="List" CheckboxAutoPostBack="False"
            CustomImportToExcel="false" CustomSortExpression="" DeleteToolTipOfCol="-1" HideTextLength="20"
            ShowCmpAndCancel="False" ShowGridViewSelect="List" ShowHideColModel="None" ShowImportButton="False"
            ShowTreeView="False" SelectOrDelete="True" Width="98%" OnOnLoadData="CustomGridView1_OnLoadData">
            <PagerSettings Visible="False"></PagerSettings>
            <Columns>
                <asp:TemplateField HeaderText="消息内容" HeaderStyle-Height="20px" ItemStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <%# GetUrl(Container)%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="PHONENO" HeaderText="电话号码" />
                <asp:BoundField DataField="plansendtimetext" HeaderText="发送时间" />
                <asp:TemplateField HeaderText="删除">
                    <ItemTemplate>
                        <asp:LinkButton ID="lbtnDelete" CommandArgument='<%# Eval("messageid") %>' runat="server"
                            OnClick="lbtnDelete_Click">删除</asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </defineControls:CustomGridView>
    </div>
</asp:Content>
