﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.IO;
using Business.Common;
using SbBusiness.User;
using Business.Admin;

public partial class Common_AppearOtherResouce : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindData();
        }
    }

    /// <summary>
    /// 绑定数据
    /// </summary>
    private void BindData()
    {
        //ProcessBarClass.Process(this, "1");
        DataTable dtStoreInfo = new DataTable();
        string strFileType = "";
        string strFileName = "";
        byte[] bArr=null;

        //获取流程权限上传附件        

        if (!string.IsNullOrEmpty(Request["id"]))
        {
            string strId = Request["id"];
            UploadFileClass uploadFileClass = new UploadFileClass();

            if (!string.IsNullOrEmpty(Request["type"]))
            {
                string strType = Request["type"];

                if (strType == "fujian")//临时上传的附件
                {
                    dtStoreInfo = uploadFileClass.GetModuleStoreFile("xt_serial_attachment", Request["id"]);
                    if (dtStoreInfo.Rows.Count > 0)
                    {
                        strFileType = dtStoreInfo.Rows[0][2].ToString();//附件类型
                        strFileName = dtStoreInfo.Rows[0][3].ToString();//附件名称
                        bArr = dtStoreInfo.Rows[0][1] as Byte[];
                    }
                }
                else if (strType == "moban")//模板
                {
                    dtStoreInfo = uploadFileClass.GetModuleStoreFile("XT_SERIAL_MODULE", Request["id"]);
                    if (dtStoreInfo.Rows.Count > 0)
                    {
                        strFileType = dtStoreInfo.Rows[0][2].ToString();//附件类型
                        strFileName = dtStoreInfo.Rows[0][3].ToString();//附件名称
                        bArr = dtStoreInfo.Rows[0][1] as Byte[];
                    }
                }
                else//类别"serial"
                {
                    SysUserRightRequest urrTemp = new SysUserRightRequest();
                    dtStoreInfo = urrTemp.GetSerialAtt(Request["id"]);

                    if (dtStoreInfo.Rows.Count > 0)
                    {
                        strFileType = dtStoreInfo.Rows[0][1].ToString();
                        strFileName = dtStoreInfo.Rows[0][0].ToString();
                        bArr = dtStoreInfo.Rows[0][2] as Byte[];
                    }
                }
            }
            else
            {
                dtStoreInfo = uploadFileClass.GetStoreFile("SYS_RESOURCE", strId);

                if (dtStoreInfo.Rows.Count > 0)
                {
                    strFileType = dtStoreInfo.Rows[0][1].ToString();
                    strFileName = dtStoreInfo.Rows[0][0].ToString();
                    bArr = dtStoreInfo.Rows[0][2] as Byte[];
                }
            }
        }

        if (!string.IsNullOrEmpty(strFileType))
        {
            Response.ClearContent();

            strFileType = strFileType.ToLower();
            if (strFileType.CompareTo("jpg") == 0 || strFileType.CompareTo("gif") == 0 ||
                strFileType.CompareTo("png") == 0 || strFileType.CompareTo("bmp") == 0)
            {
                //Response.Clear();
                //Response.Buffer = true;
                //Response.Charset = "gb2312";
                //Response.ContentEncoding = System.Text.Encoding.GetEncoding("GB2312");
                //Response.AppendHeader("content-disposition", "attachment;filename=\"" + System.Web.HttpUtility.UrlEncode(strFileName, System.Text.Encoding.UTF8) + "." + strFileType + "\"");
                //Response.ContentType = "image/gif";
                //Response.ContentType = "image/" + strFileType;
                //Response.BinaryWrite(bArr);
                //Response.End();

                ImageView1.Images = bArr;
                ImageView1.Visible = true;


            }
            else
            {
                //控制生成的文件名字 edit by zhongjian 20090910
                Response.Clear();
                Response.ClearHeaders();
                Response.ClearContent();
                Response.Buffer = true;
                Response.Charset = "gb2312";
                Response.ContentEncoding = System.Text.Encoding.GetEncoding("GB2312");
                Response.AppendHeader("content-disposition", "attachment;filename=\"" + System.Web.HttpUtility.UrlEncode(strFileName, System.Text.Encoding.UTF8) + "." + strFileType + "\"");

                if (strFileType.CompareTo("xls") == 0)
                {
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.BinaryWrite(bArr);
                }
                else if (strFileType.CompareTo("doc") == 0)
                {
                    Response.ContentType = "application/msword";
                    Response.BinaryWrite(bArr);
                }
                //针对office 2007 update by zhongjian 20100409
                else if (strFileType.CompareTo("docx") == 0 || strFileType.CompareTo("xlsx") == 0 || strFileType.CompareTo("pptx") == 0)
                {
                    Response.BinaryWrite(bArr);
                    Response.End();
                }
                else if (strFileType.CompareTo("ppt") == 0)
                {
                    Response.ContentType = "application/vnd.ms-powerpoint";
                    Response.BinaryWrite(bArr);
                }
                else if (strFileType.CompareTo("rtf") == 0)
                {
                    Response.ContentType = "application/rtf";
                    Response.BinaryWrite(bArr);
                }
                else if (strFileType.CompareTo("pdf") == 0)
                {
                    Response.ContentType = "application/pdf";
                    Response.BinaryWrite(bArr);
                }
                else if (strFileType.CompareTo("zip") == 0)
                {
                    Response.ContentType = "application/zip";
                    Response.BinaryWrite(bArr);
                }
                else
                {
                    Response.ContentType = "application/x-msdownload;";
                    Response.BinaryWrite(bArr);
                }

                Response.Flush();
                Response.End();
            }
        }
    }
}
