﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CommonTabUpdate.aspx.cs"
    Inherits="Common_CommonTabUpdate" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>业务申报表单详细信息</title>
    <link rel="stylesheet" id="skinCss" type="text/css" href="../css/default/easyui.css" />
    <link rel="stylesheet" type="text/css" href="../css/icon.css" />
    <script type="text/javascript" src="../ScriptFile/jquery.js"></script>
    <script type="text/javascript" src="../ScriptFile/jquery.easyui.min.js"></script>
</head>
<script type="text/javascript">
    var isSaveSuccess = false;
    var currentUrl, currentCtrlId;

    //保存后的逻辑处理
    function SaveIId() {
        isSaveSuccess = true;
        alert("保存成功");
    }
    //获取当前选项卡
    function GetCurrentTabName() {
        var objTemp = $('#divMain').tabs('getSelected').find("iframe");
        if (typeof (objTemp) == "undefined") {
            objTemp = $('#divMain').tabs('getSelected').find("IFRAME");
        }

        return $(objTemp).attr("name");
    }
    //保存
    function SaveInfo() {
        var strId = GetCurrentTabName();
        window.frames[strId].SaveAll(SaveIId);

        //老保存方法
        //frmSjb.SaveAll(SaveIId);
        //新保存方法
        //frmSjb.SaveAll(true, SaveIId);
    }

    //自动高度
    function IfrmLoad(frmId) {
        $("#" + frmId).load(function () {
            var thisheight = $(this).contents().find("body").height() + 30;
            $(this).height(thisheight < 600 ? 600 : thisheight);
        });
    }

</script>
<body>
    <div id="divNorth" style="text-align: right;">
        <input id='saveform' type='button' value=' 保 存 ' onclick='SaveInfo();' class='NewButton' />
    </div>
    <div id="divMain">
        <%=GetContent()%>
    </div>
    <script type="text/javascript">
        $("#divMain").tabs({
            width: $("#divMain").parent().width() - 20,
            height: "auto"
            //height: $("#divMain").parent().height()
        });

        //遍历所有tab选项并设置load事件
        $("iframe").each(function (index) {
            var strId = $(this).attr("id");
            IfrmLoad(strId);
        });
    </script>
</body>
</html>
