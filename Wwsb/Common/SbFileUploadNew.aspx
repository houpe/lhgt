﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SbFileUploadNew.aspx.cs"
    Inherits="Common_SbFileUploadNew" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>申报页面文件上传</title>
    <link href="../App_Themes/SkinFile/CSS/easyui.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../App_Themes/SkinFile/CSS/StyleSheet.css" />

    <script type="text/javascript" src="../ScriptFile/jquery.js"></script>
    <script type="text/javascript" src="../ScriptFile/jquery.easyui.min.js"></script>
    <script type="text/javascript">
        //关闭窗口
        function CloseDivDlg() {
            $('#dlgConfig').window('close');
        }

        //打开内嵌网页的通用方法1（window）
        function openDivDlg(strTitle, strUrl) {
            var divWidth = 400;
            var divHeight = 200;

            //增加参数queryid
            $('#dlgConfig').window({
                title: strTitle, width: divWidth, height: divHeight
            });

            $('#iframeConfig').attr("src", strUrl);

            // $('#dlgConfig').window({ maximized: true });
            $('#dlgConfig').window('open');
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="dlgConfig" class="easyui-window" title="文件上传" closed="true" modal="false"
        minimizable="false" style="width: 400px;height: 300px;left:100px;top:50px; background: #fafafa;">
        <iframe id="iframeConfig" scrolling="yes" frameborder="0" src="" style="width: 100%;
            height: 100%;"></iframe>
    </div>
    <div id='context' style="width:100%; margin:0px auto; text-align:center;" runat="server">
    </div>
    </form>
</body>
</html>
