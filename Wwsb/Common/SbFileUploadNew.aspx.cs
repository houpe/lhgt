﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using SbBusiness.Wsbs;
using Business.Common;
using Business.Admin;

public partial class Common_SbFileUploadNew : System.Web.UI.Page
{
    UploadFileClass ufcGloab = new UploadFileClass();

    //流程ID 
    public string iid
    {
        get
        {
            return Request["NewsId"];
        }
    }
    //流程类别
    public string flowname
    {
        get
        {
            return Request["flowname"];
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //判断附件权限
            GenerateAllFuJian();
        }
        ProcessResource();
    }

    /// <summary>
    /// 处理附件（删除或设置为模板）
    /// </summary>
    public void ProcessResource()
    {
        if (!string.IsNullOrEmpty(Request["id"]))
        {
            if (Request["action"] == "delete")
            {
                ufcGloab.DeleteStoreFile("XT_SERIAL_ATTACHMENT", "id", Request["id"]);
            }
            else if (Request["action"] == "setMoudle")
            {
                ufcGloab.SetAtachmentMoudle(Request["id"],Session["userid"].ToString());
            }

            GenerateAllFuJian();
        }
    }

    /// <summary>
    /// 生成附件表格样式
    /// </summary>
    public void GenerateAllFuJian()
    {
        StringBuilder html = new StringBuilder();
        html.Append("<table class='window_tab_list' style='color: dodgerblue; border-color: #4fbff9; width:100%;border-collapse:collapse;' align='center' border=\"1\"><tr class='title' align='center'>");
        html.Append("<th style='height:25px'>序号</th><th width=\"300px\">类别</th><th>附件名称</th><th>格式</th><th>操作</th></tr>");
        DataTable dt = DictOperation.GetSysParams("发布类别", string.Format("type='{0}'", flowname));

        string strSubmitFlag = "";
        if (!string.IsNullOrEmpty(iid))
        {
            strSubmitFlag = ShenBaoSubmit.GetIsSubmit(iid);
        }

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            //绘制种类行
            if (dt.Rows[i]["keyvalue"].ToString() == System.Configuration.ConfigurationManager.AppSettings["dtshAttType"])
            {
                html.Append("<tr title='文件大小不可超过100M' style='background-color:Silver;font-weight:bold; height:25px'>");
            }
            else
            {
                html.Append("<tr style='background-color:#ECF2FB;font-weight:bold; height:25px'>");
            }
            html.Append(string.Format("<td>{1}</td><td>{0}</td><td></td><td></td>", dt.Rows[i]["keyvalue"], i + 1));

            //0:未提交;-1:返回补证;空 均可使用上传材料
            if (strSubmitFlag == "0" || strSubmitFlag == "-1" || string.IsNullOrEmpty(strSubmitFlag))
            {
                string strUrl = string.Format("../UserControls/MutiFilesUpload.aspx?iid={0}&moudleid={1}", iid, dt.Rows[i]["keycode"]);
                html.Append(string.Format("<td><a href=\"#\" onclick=\"openDivDlg('上传附件','{0}')\">上传</a></td>", strUrl));
            }
            else{
                html.Append("<td></td>");
            }

            html.Append("</tr>");


            //获取上传的附件
            string strWhere = string.Format(" (地信申报信息ID='{0}' or (IS_TEMPLET=1 and USERID='{2}')) and MODULE_ID='{1}'", iid, dt.Rows[i]["keycode"], Session["userid"]);
            DataTable dtchild = ufcGloab.GetAttachmentResourceInfo(strWhere);

            //绘制附件所对应的行            
            for (int j = 0; j < dtchild.Rows.Count; j++)
            {
                string strRowSpan = "";

                if (j == 0)//第一行设置占用行数，其他行少<td></td>
                {
                    strRowSpan = string.Format("<td rowspan='{0}'></td>", dtchild.Rows.Count);
                }

                string strBackColor = "";
                string strSwmb = string.Empty;
                if (dtchild.Rows[j]["IS_TEMPLET"].ToString() == "1")
                {
                    strBackColor = "color:red;";
                }
                else
                {
                    strSwmb = string.Format("&nbsp;&nbsp;<a href=\"SbFileUploadNew.aspx?action=setMoudle&id={0}&flowname={1}&NewsId={2}\" onclick=\"return confirm('确定要设置为长期有效吗？')\">设为长期有效</a>", dtchild.Rows[j]["id"], flowname, iid);
                }


                html.Append(string.Format("<tr style='height:25px;'>{1}<td style='{2}'>({0})</td>", j + 1, strRowSpan, strBackColor));
                html.Append(string.Format("<td style='{2}'><a href='AppearOtherResouce.aspx?type=fujian&id={1}' style='{2}'>{0}</a></td>", dtchild.Rows[j]["filename"], dtchild.Rows[j]["id"], strBackColor));
                html.Append(string.Format("<td style='{1}'>{0}</td>", dtchild.Rows[j]["fileTYPE"],strBackColor));

                //0:未提交;-1:返回补证;空 均可使用删除和设置长期有效
                if (strSubmitFlag == "0" || strSubmitFlag == "-1" || string.IsNullOrEmpty(strSubmitFlag))
                {
                    html.Append(string.Format("<td style='{4}'><a href=\"SbFileUploadNew.aspx?action=delete&id={0}&flowname={1}&NewsId={2}\" onclick=\"return confirm('确定删除？')\">删除</a>{3}</td>", dtchild.Rows[j]["id"], flowname, iid, strSwmb,strBackColor));
                }
                else
                {
                    html.Append("<td style='{2}'></td>");
                }

                html.Append("</tr>");
            }

        }
        html.Append("</table>");

        this.context.InnerHtml = html.ToString();
    }
}