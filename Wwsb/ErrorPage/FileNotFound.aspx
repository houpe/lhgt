﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="FileNotFound.aspx.cs" Inherits="ErrorPage_FileNotFound" Title="无法找到指定页面" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">
    <div>
        <table border="0" cellpadding="0" cellspacing="0" class="window_tab">
            <tr>
                <td class="txtrighttd">
                    对不起，您查找的页面不存在.
                </td>
            </tr>
            <tr>
                <td>
                    <input id="Button1" type="button" value="返回"  class="LightGreen" onclick="javascript:history.back(-1);" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
