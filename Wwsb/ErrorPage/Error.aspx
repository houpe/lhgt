﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Error.aspx.cs" Inherits="ErrorPage_Error" Title="发生错误" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">
    <div>
        <table border="0" cellpadding="0" cellspacing="0" class="window_tab">
            <tr>
                <td class="txtrighttd" style="text-align:center;">
                    出现异常：<%=Session["Error"]%>
                </td>
            </tr>
            <tr>
                <td>
                    <input id="Button1" class="button" type="button" value="返回" onclick="javascript:history.back(-1);" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
