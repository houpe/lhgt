﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddSysName.aspx.cs" Inherits="UserManage_AddSysName"
    Title="添加系统名称" %>

<%@ Register Src="../UserControls/TextBoxWithValidator.ascx" TagName="TextBoxWithValidator"
    TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
</head>
<body>
    <form id="form1" runat="server">
        <div style="text-align: center;">
            <br />
            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="right">
                        系统名称
                    </td>
                    <td align="left">
                        <uc1:TextBoxWithValidator ID="txtUserTypeName" runat="server" ErrorMessage="不能为空"
                            Type="Required" />
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnCommit" runat="server" Text="提交" OnClick="btnCommit_Click" SkinID="LightGreen" />
                    </td>
                    <td>
                        <input type="button" id="btnCancel" class="button" value="取消" skinid="LightGreen"
                            onclick="Javascript:window.close();" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
