﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using SbBusiness;
using SbBusiness.User;
using Common;

public partial class UserManage_ModifyPWD : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindData();
            Session["DangQianRenWu"] = "修改密码";
        }
    }

    private void BindData()
    {       
        this.txtUserID.Text = Session["UserId"].ToString();
        this.txtUserID.Enabled = false;
    }

    protected void btnModify_Clicked(object sender, EventArgs e)
    {
        SysUserHandle uhUser = new SysUserHandle();
        if (Encode.Md5(this.txtOldPWD.Text) != (uhUser.GetPWDByUserID(txtUserID.Text)))
        {
            WindowAppear.WriteAlert(Page, "输入的原始密码不正确");
            return;
        }
        if (this.txtNewPWD.Text != this.txtConfirmPWD.Text)
        {
            WindowAppear.WriteAlert(Page, "输入的新密码不一致");
            return;
        }
        if (uhUser.ModifyPWD(this.txtUserID.Text, txtNewPWD.Text))
        {
            //获取用户信息 addby zhongjian 20100303
            DataTable dtTemp = SysUserHandle.GetUser(Session["UserId"].ToString());
            if (dtTemp.Rows.Count > 0)
            {
                ViewState["Sync_Type"] = dtTemp.Rows[0]["sync_type"].ToString();//数据交换类型(2:地信中心数据)
                ViewState["strID"] = dtTemp.Rows[0]["id"].ToString();//主键ID
               
            }
            WindowAppear.WriteAlert(Page, "修改密码成功");
        }
    }

  
}
