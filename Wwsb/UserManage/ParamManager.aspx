﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ParamManager.aspx.cs" Inherits="UserManage_ParamManager" Title="数据字典管理" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">

    <script type="text/javascript">
    function newParam()
    {
        var name = prompt('请输入数据字典类别名','');
        if(name != "")
        {
            window.location.href = "ParamItemManager.aspx?action=addroot&name=" + name;
        }
            return false;
    }
    </script>

    <div style="text-align: center">
        <asp:Label ID="lblLabel1" runat="server" Text="请输入数据字典名"></asp:Label>
        &nbsp;
        <asp:TextBox ID="txtParamItem" runat="server"></asp:TextBox>
        &nbsp;
        <asp:Button ID="btnQuery" runat="server" OnClick="btnQuery_Clicked" Text="查询" />
        &nbsp;
        <asp:HyperLink ID="InsertName" runat="server" NavigateUrl="ParamItemManager.aspx?action=addroot"
            Text="新增类别" />
    </div>
    <br />
    <defineControls:CustomGridView ID="CustomGridView1" runat="server" AllowPaging="false" AutoGenerateColumns="false"
        Width="550pt" ShowHideColModel="None" SkinID="List" OnOnLoadData="CustomGridView1_OnLoadData">
        <Columns>
            <asp:BoundField DataField="name" HeaderText="数据字典类名"></asp:BoundField>
            <asp:BoundField DataField="item_count" HeaderText="个数"></asp:BoundField>
            <asp:TemplateField HeaderText="字典项">
                <itemtemplate>
                    <asp:HyperLink ID="hlChildren" runat="server"
                        NavigateUrl='<%#string.Format("ParamNameManager.aspx?name={0}", Eval("name")) %>'>编辑子项</asp:HyperLink>                                 
                
</itemtemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="删除">
                <itemstyle horizontalalign="Center" verticalalign="Middle" />
                <itemtemplate>
                     <asp:HyperLink ID="hlDelete" runat="server" NavigateUrl='<%# string.Format("ParamManager.aspx?action=delete&name={0}", Eval("name"))%>'>删除</asp:HyperLink>
                
</itemtemplate>
            </asp:TemplateField>
        </Columns>
        <PagerSettings Visible="False" />
        <HeaderStyle CssClass="top_table" Height="30px" Width="50px" />
        <RowStyle CssClass="inputtable" />
    </defineControls:CustomGridView>
</asp:Content>
