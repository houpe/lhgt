﻿ 
// 创建人  ：Wu Hansi
// 创建时间：2007年7月20日
using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using SbBusiness;
using SbBusiness.Menu;


/// <!--
/// 功能描述  : 功能描述
/// 创建人  : Wu Hansi
/// 创建时间: 2007年7月20日
/// -->
public partial class UserManage_UserMenuManager : System.Web.UI.Page
{
    private ClsMenuOperation clsMenu = new ClsMenuOperation();

    private List<string[]> AuthList
    {
        get
        {
            if (ViewState["UserMenuManager_AuthList"] != null)
            {
                return ViewState["UserMenuManager_AuthList"] as List<string[]>;
            }
            else
            {
                List<string[]> authList = new List<string[]>();
                ViewState["UserMenuManager_AuthList"] = authList;
                return authList;
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindData();
        }
    }

    private void BindData()
    {
        string usertype_id = Request.Params["usertype_id"];
        DataTable dtTable = clsMenu.GetMenusSortedWithAuth(usertype_id);

        CustomGridView1.DataSource = dtTable;
        CustomGridView1.AllowPaging = false;
        CustomGridView1.RecordCount = dtTable.Rows.Count;
        CustomGridView1.DataBind();

    }

    protected void CustomGridView1_OnLoadData(object sender, EventArgs e)
    {
        BindData();
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Session["UserID"].ToString() == SystemConfig.AdminUser)
        {
            for (int i = 0; i < CustomGridView1.Rows.Count; i++)
            {
                GridViewRow gvr = CustomGridView1.Rows[i];
                CheckBox ckb = gvr.FindControl("ckbAuth") as CheckBox;
                if (AuthList[i][0] == "1" && ckb.Checked == false)
                {

                    clsMenu.RemoveUsertypeMenuRelation(Request.Params["usertype_id"], AuthList[i][1]);


                }
                if (AuthList[i][0] == "0" && ckb.Checked == true)
                {

                    clsMenu.InsertUsertypeMenuRelation(Request.Params["usertype_id"], AuthList[i][1]);


                }
            }
        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "scriptBak", "<script>alert('不是超级管理员不能删除！');</script>");
        }
    }
    protected void CustomGridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        switch (e.Row.RowType)
        {
            case DataControlRowType.DataRow:
                DataRowView drv = e.Row.DataItem as DataRowView;
                if (drv == null)
                {
                    break;
                }
                CheckBox ckb = e.Row.FindControl("ckbAuth") as CheckBox;
                if (drv["auth"] != null && drv["auth"].ToString() != "0")
                {
                    ckb.Checked = true;
                    AuthList.Add(new string[] { "1", drv["id"].ToString() });
                }
                else
                {
                    AuthList.Add(new string[] { "0", drv["id"].ToString() });
                }
                break;
        }
    }
    protected void btnReturn_Click(object sender, EventArgs e)
    {
        Response.Redirect("UserPrivilage.aspx");
    }
}
