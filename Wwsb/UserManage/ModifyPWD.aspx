﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ModifyPWD.aspx.cs" Inherits="UserManage_ModifyPWD" Title="修改密码" %>

<%@ Register Src="../UserControls/TextBoxWithValidator.ascx" TagName="TextBoxWithValidator"
    TagPrefix="uc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">
    <div style="text-align: center;">
        <p>
            修改密码</p>
        <table class="window_tab_list" style="width:55%;" align="center" cellpadding="8" cellspacing="0" border="1">
            <tr>
                <td style="background-color: Snow; width: 100px">
                    用户登录ID
                </td>
                <td align="left">
                    <asp:TextBox ID="txtUserID" runat="server" Width="150"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="background-color: Snow; width: 100px">
                    输入原密码
                </td>
                <td align="left">
                    <asp:TextBox ID="txtOldPWD" runat="server" TextMode="Password" Width="150"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="background-color: Snow; width: 100px">
                    输入新密码
                </td>
                <td align="left">
                    <uc1:TextBoxWithValidator ID="txtNewPWD" Width="150px" runat="server" Type="Passwords"
                        TextMode="Password"></uc1:TextBoxWithValidator>
                </td>
            </tr>
            <tr>
                <td style="background-color: Snow; width: 100px">
                    确认新密码
                </td>
                <td align="left" colspan="3">
                    <asp:TextBox ID="txtConfirmPWD" runat="server" TextMode="Password" Width="150"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>
    <br />
    <div style="text-align: center">
        <asp:Button ID="btnModify" runat="server" Text="确定" OnClick="btnModify_Clicked" />
        <input type="button" value="关闭" class="button" onclick="javascript:window.close()" />
    </div>
</asp:Content>
