﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="MenuItemManager.aspx.cs" Inherits="UserManage_MenuItemManager" Title="菜单项目管理" %>

<%@ Register Src="../UserControls/PersistenceControl.ascx" TagName="PersistenceControl"
    TagPrefix="uc2" %>

<%@ Register Src="../UserControls/TextBoxWithValidator.ascx" TagName="TextBoxWithValidator"
    TagPrefix="uc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">
    <div style="text-align:center">
    <table border="0" cellpadding="0" cellspacing="0" class="window_tab" style="width:80%">
        <tr>
            <td class="txtrighttd">
                <asp:Label ID="Label2" runat="server" Text="菜单名称"></asp:Label></td>
            <td class="txtlefttd">
                <uc1:TextBoxWithValidator ID="txtMenu_title" runat="server" ErrorMessage="不能为空" Type="Required"
                    ValidateEmptyText="true" Width="300pt"/>
            </td>
        </tr>
        <tr>
            <td class="txtrighttd">
                <asp:Label ID="Label3" runat="server" Text="菜单路径"></asp:Label></td>
            <td class="txtlefttd">
                <uc1:TextBoxWithValidator ID="txtMenu_Url" runat="server" ErrorMessage="路径不能为空"
                    Width="300pt"/></td>
        </tr>
        <tr>
            <td class="txtrighttd">
                <asp:Label ID="Label1" runat="server" Text="父菜单id"></asp:Label></td>
            <td class="txtlefttd">
                <asp:TextBox ID="txtParent_id" runat="server" Enabled="false" Width="300pt"/></td>
        </tr>
    </table>
        <asp:Button ID="btnSave" runat="server" Text="保存" OnClick="btnSave_Click" SkinID="LightGreen"/>
        <asp:Button ID="btnReturn" runat="server" Text="返回" CausesValidation="False" SkinID="LightGreen" OnClick="btnReturn_Click" />
    </div>
    <uc2:PersistenceControl ID="PersistenceControl1" runat="server" Key="ID" Table="SYS_MENU" />
</asp:Content>
