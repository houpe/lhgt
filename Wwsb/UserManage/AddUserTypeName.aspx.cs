﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SbBusiness;
using SbBusiness.User;

public partial class UserManage_AddUserTypeName : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindData();
        }
    }

    private void BindData()
    {
        SysUserTypeHandle userhandle = new SysUserTypeHandle();
        this.ddlSysName.DataSource = userhandle.GetSysName();
        this.ddlSysName.DataValueField = "系统名称";
        this.ddlSysName.DataBind();
    }

    protected void btnCommit_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(ddlSysName.Text))
        {
            SysUserTypeHandle sutHandle = new SysUserTypeHandle();
            Hashtable table = new Hashtable();

            table.Add("SYSCODE", sutHandle.GetSysCodeBySysName(this.ddlSysName.Text));
            table.Add("USERTYPENAME", this.txtUserTypeName.Text);
            table.Add("SYSNAME", this.ddlSysName.Text);

            if (sutHandle.InsertUserType(table))
            {
                Response.Write("<script language='javascript'>alert('插入成功');window.opener.location.href=window.opener.location.href;window.opener=null;window.close();</script>");
            }
            else
            {
                Response.Write("<script language='javascript'>alert('插入失败')</script>");
            }
        }
    }
    
}
