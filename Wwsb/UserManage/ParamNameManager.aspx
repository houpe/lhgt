﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ParamNameManager.aspx.cs" Inherits="UserManage_ParamNameManager" Title="数据字典项" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">
    <div style="text-align: center">
        <defineControls:CustomGridView ID="CustomGridView1" runat="server" AllowPaging="false" AutoGenerateColumns="false"
            Width="550pt" SkinID="List" ShowHideColModel="None" OnOnLoadData="CustomGridView1_OnLoadData">
            <Columns>
                <asp:BoundField DataField="name" HeaderText="数据字典类名"></asp:BoundField>
                <asp:BoundField DataField="keyvalue" HeaderText="数据字典项名"></asp:BoundField>
                <asp:BoundField DataField="keycode" HeaderText="数据字典项值"></asp:BoundField>
                <asp:TemplateField HeaderText="编辑">
                    <itemtemplate>
                    <asp:HyperLink ID="hlEdit" runat="server"
                        NavigateUrl='<%#string.Format("ParamItemManager.aspx?action=edit&rowid={0}", Server.UrlEncode(Eval("rowid").ToString())) %>'>编辑</asp:HyperLink>                                 
                
</itemtemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="删除">
                    <itemstyle horizontalalign="Center" verticalalign="Middle" />
                    <itemtemplate>
                     <asp:HyperLink ID="hlDelete" runat="server" 
                     NavigateUrl='<%# string.Format("ParamNameManager.aspx?action=delete&rowid={0}&name={1}&keyvalue={2}", Server.UrlEncode(Eval("rowid").ToString()), Eval("name").ToString(),Eval("keyvalue").ToString())%>'>删除</asp:HyperLink>
                
</itemtemplate>
                </asp:TemplateField>
            </Columns>
            <PagerSettings Visible="False" />
            <HeaderStyle CssClass="top_table" Height="30px" Width="50px" />
            <RowStyle CssClass="inputtable" />
        </defineControls:CustomGridView>
        <asp:Button ID="btnSave" runat="server" Text="新增" OnClick="btnAdd_Click" SkinID="LightGreen" />
        <asp:Button ID="btnReturn" runat="server" Text="返回" CausesValidation="False" OnClick="btnReturn_Click"
            SkinID="LightGreen" />
    </div>
</asp:Content>
