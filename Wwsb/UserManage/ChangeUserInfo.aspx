﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ChangeUserInfo.aspx.cs" Inherits="UserManage_ChangeUserInfo" Title="个人信息维护" %>

<%@ Register Src="../UserControls/GeneralSelect.ascx" TagName="GeneralSelect" TagPrefix="uc2" %>
<%@ Register Src="../UserControls/TextBoxWithValidator.ascx" TagName="TextBoxWithValidator"
    TagPrefix="uc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">
    <script type="text/javascript" src="../ScriptFile/Regex.js"></script>
    <script type="text/javascript">
        function onRefer() {
            var eUserId = document.getElementById("<%=txtUserId.ChildControlClientId %>");
            var eUserPwd = document.getElementById("<%=txtUserPwd.ChildControlClientId %>");
            var eUserRePwd = document.getElementById("<%=txtUserRePwd.ClientID %>");
            var eHidden = document.getElementById("<%=HiddenFieldFlag.ClientID %>");
            var eZjhm = document.getElementById("<%=txtZjhm.ClientID %>");
            var eUnitsName = document.getElementById("<%=txtUnitsName.ClientID %>");

            eHidden.value = "";
            if (eUserId.value == "") {
                alert("用户名不能为空");
                eHidden.value = "0";
            }           
            if (eZjhm.value == "") {
                alert("证件号码不能为空");
                eHidden.value = "3";
            }
           
			if (eUnitsName.value == "") {
				alert("单位名称不能为空");
                eHidden.value = "4";
            }
        }

        function SetCusIsValid(objcon) {
            var sourceid = objcon.id.replace("ddlZjlx_ddlControl", "cvZjhm");
            var objsource = document.getElementById(sourceid);
            if (objcon.value == "2" || objcon.value == "3") {
                objsource.enabled = false;
            }
            else {
                objsource.enabled = true;
            }
        }
        
         function UserType_onchange() {
		    var si=0;
		    si=<%=GetUserType()%>;		 
		    if(si==1){     
			    document.all("trUnits").style.display="block";
			    document.all("trAddress").style.display="none";
			    document.getElementById("<%=lblFiel.ClientID %>").value="经办人信息";
		    }
		    else{			    
			    document.all("trUnits").style.display="none";
			    document.all("trAddress").style.display="block";
			    document.getElementById("<%=lblFiel.ClientID %>").value="个人信息";
		    }
	    }
    </script>
    <div style="text-align: center; width: 100%;">
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr id="trUnits" style="display: block">
                <td>
                    <p>
                        单位信息</p>
                    <table class="window_tab_list" style="width: 95%;" align="center" cellpadding="8" cellspacing="0">
                        <tr>
                            <td style="background-color: Snow; width: 100px">
                                单位名称
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtUnitsName" Width="180px" runat="server"></asp:TextBox>
                            </td>
                            <td style="background-color: Snow; width: 100px">
                                组织代码
                            </td>
                            <td align="left" colspan="3">
                                <asp:TextBox ID="txtOrganization" Width="180px" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="background-color: Snow; width: 100px">
                                法人代表或负责人
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtLegal" Width="180px" runat="server"></asp:TextBox>
                            </td>
                            <td style="background-color: Snow; width: 100px">
                                单位电话
                            </td>
                            <td align="left" colspan="3">
                                <asp:TextBox ID="txtUnitsTel" Width="180px" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="background-color: Snow; width: 100px">
                                单位类型
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtNature" Width="180px" runat="server"></asp:TextBox>
                            </td>
                            <td style="background-color: Snow; width: 100px">
                                传&nbsp;&nbsp;&nbsp;&nbsp;真
                            </td>
                            <td align="left" colspan="3">
                                <asp:TextBox ID="txtUnitsFax" Width="180px" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="background-color: Snow; width: 100px">
                                单位地址
                            </td>
                            <td align="left" colspan="3">
                                <asp:TextBox ID="txtUnitsAddress" Width="100%" runat="server"></asp:TextBox>
                            </td>
                            <td style="background-color: Snow; width: 80px">
                                邮政编码
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtUnitsPostCode" Width="82px" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <p>
                        <asp:Label ID="lblFiel" runat="server" Text="个人信息"></asp:Label></p>
                    <table class="window_tab_list" style="width: 95%;" align="center"  cellpadding="8" cellspacing="0">
                        <tr>
                            <td style="background-color: Snow; width: 100px">
                                用 户 名
                            </td>
                            <td align="left">
                                <uc1:TextBoxWithValidator ID="txtUserId" Width="180px" runat="server" ReadOnly="true"
                                    Type="UserId"></uc1:TextBoxWithValidator>
                            </td>
                            <td style="background-color: Snow; width: 100px">
                                姓&nbsp;&nbsp;&nbsp;&nbsp;名
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtUserName" Width="180px" runat="server"></asp:TextBox>
                            </td>
                            <td style="background-color: Snow; width: 50px">
                                性&nbsp;&nbsp;&nbsp;&nbsp;别
                            </td>
                            <td align="left">
                                <uc2:GeneralSelect ID="ddlSex" Width="50px" runat="server" ValidateEmpty="false"
                                    Name="性别" />
                            </td>
                        </tr>
                        <tr style="display: none">
                            <td style="background-color: Snow; width: 100px">
                                登录密码
                            </td>
                            <td align="left">
                                <uc1:TextBoxWithValidator ID="txtUserPwd" Width="180px" runat="server" Type="Passwords"
                                    TextMode="Password"></uc1:TextBoxWithValidator>
                            </td>
                            <td style="background-color: Snow; width: 100px">
                                重复密码
                            </td>
                            <td align="left" colspan="3">
                                <asp:TextBox ID="txtUserRePwd" Width="180px" runat="server" TextMode="Password"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="background-color: Snow; width: 100px">
                                证件类别
                            </td>
                            <td align="left">
                                <uc2:GeneralSelect ID="ddlZjlx" Width="180px" runat="server" ClientOnChangeScript="SetCusIsValid(this)"
                                    ValidateEmpty="true" Name="证件类型" />
                            </td>
                            <td style="background-color: Snow; width: 100px">
                                证件号码
                            </td>
                            <td align="left" colspan="3">
                                <asp:TextBox ID="txtZjhm" Width="180px" runat="server"></asp:TextBox>
                                <asp:CustomValidator ID="cvZjhm" runat="server" ControlToValidate="txtZjhm" ErrorMessage=""
                                    Display="Dynamic" ClientValidationFunction="checkNumberNew" ValidateEmptyText="true"></asp:CustomValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="background-color: Snow; width: 100px">
                                联系电话
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtTel" Width="180px" runat="server"></asp:TextBox>
                            </td>
                            <td style="background-color: Snow; width: 100px">
                                传&nbsp;&nbsp;&nbsp;&nbsp;真
                            </td>
                            <td align="left" colspan="3">
                                <asp:TextBox ID="txtCz" Width="180px" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="background-color: Snow; width: 100px">
                                手机号码
                            </td>
                            <td align="left">
                                <uc1:TextBoxWithValidator ID="txtMobile" Width="180px" runat="server" Type="Phone">
                                </uc1:TextBoxWithValidator>
                            </td>
                            <td style="background-color: Snow; width: 100px">
                                电子邮件
                            </td>
                            <td align="left" colspan="3">
                                <asp:TextBox ID="txtEmail" Width="180px" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr id="trAddress" style="display: none">
                            <td style="background-color: Snow; width: 100px">
                                联系地址
                            </td>
                            <td align="left" colspan="3">
                                <asp:TextBox ID="txtAddress" Width="100%" runat="server"></asp:TextBox>
                            </td>
                            <td style="background-color: Snow; width: 80px">
                                邮政编码
                            </td>
                            <td align="left">
                                <uc1:TextBoxWithValidator ID="txtYZBM" Width="82px" runat="server" Type="Postcode">
                                </uc1:TextBoxWithValidator>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="labMsg" ForeColor="red" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnRefer" SkinID="LightGreen" runat="server" Text="保存" OnClick="btnRefer_Click"
                        OnClientClick="onRefer ();" />
                    <asp:Button ID="btnManageUser" Visible="false" SkinID="LongLightGreen" runat="server"
                        Text="内部用户管理" OnClick="btnManageUser_Click" />
                </td>
            </tr>
        </table>
        <asp:HiddenField ID="HiddenFieldFlag" runat="server" />
    </div>
</asp:Content>
