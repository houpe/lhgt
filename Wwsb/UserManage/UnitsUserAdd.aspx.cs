﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using SbBusiness.Wsbs;
using SbBusiness.User;
using SbBusiness;
using SbBusiness.Menu;

using Business.Common;

public partial class UserManage_UnitsUserAdd : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    /// <summary>
    /// 单位管理员用户添加单位用户功能
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// <!--addby zhongjian 20100125-->
    protected void btnRefer_Click(object sender, EventArgs e)
    {
        try
        {
            if (HiddenFieldFlag.Value == "0" || HiddenFieldFlag.Value == "1"
                || HiddenFieldFlag.Value == "2" || HiddenFieldFlag.Value == "3")
            {
                return;
            }
            else
            {
                //检测用户是否存在过
                bool UserOldExist = SysUserHandle.CheckUserOldExist(txtUserId.Text.Trim());
                if (!UserOldExist)
                {
                    bool userExist = SysUserHandle.CheckUserExist(txtUserId.Text, SystemConfig.WebSystemFlag);
                    if (!userExist)
                    {
                        string strUnitsID = string.Empty;//单位ID
                        if (Session["UserId"] != null)
                        {
                            string strUserId = Session["UserId"].ToString();
                            ClsMenuOperation menuOperation = new ClsMenuOperation();
                            //为单位用户管理员时,展示管理员添加用户的功能 addby zhongjian 20100125
                            DataTable dtTemp = menuOperation.SelectUnitsAdmin(strUserId);
                            if (dtTemp.Rows.Count > 0)
                            {
                                strUnitsID =dtTemp.Rows[0]["unitid"].ToString();
                                //添加用户信息
                                SysUserHandle.CreateUser(txtUserId.Text, txtUserName.Text, txtUserPwd.Text,int.Parse(SystemConfig.BlqxsphUserType), txtTel.Text, txtMobile.Text,txtAddress.Text, ddlZjlx.Value, txtZjhm.Text, txtCz.Text, txtEmail.Text, txtYZBM.Text,1,"1", strUnitsID,"", "0","");
                                //添加操作日志 addby zhongjian 20100421
                                string strRemark = string.Format("添加用户信息,用户ID:" + txtUserId.Text + " 用户名称:" + txtUserName.Text + " ");
                                SystemLogs.AddSystemLogs(strUserId, "add", strRemark, "");

                                SaveMessage();//发送通知消息
                                string strWrite = string.Format(@"<script>alert('用户添加成功。');</script>");
                                Page.ClientScript.RegisterStartupScript(Page.GetType(), "", strWrite);
                            }
                        }
                    }
                    else
                    {
                        labMsg.Text = "用户名已存在，请重新输入！";
                    }
                }
                else
                {
                    labMsg.Text = "用户名已存在，请重新输入！";
                }
            }
        }
        catch (Exception ex)
        {
            ExceptionManager.WriteAlert(this, ex.Message);
        }
    }

    /// <summary>
    /// 发送用户通知
    /// </summary>
    /// <!--addby zhongjian 20091120-->
    public void SaveMessage()
    {
        Messagebox message = new Messagebox();
        DataTable dtTemp = message.GetRequestSet(0, "用户注册");
        string strMsgContent = string.Empty;
        string strStepNo = string.Empty;
        if (dtTemp.Rows.Count > 0)
        {
            strStepNo = dtTemp.Rows[0]["step_no"].ToString();
            strMsgContent = dtTemp.Rows[0]["step_msg"].ToString();
        }
        if (!string.IsNullOrEmpty(strMsgContent))//当消息内容设置为空时，将不再发送消息。update by zhongjian 20091230
        {
            try
            {
                //整理消息内容:strMsgContent中的{0}:代表用户名称; addby zhongjian 20091207
                strMsgContent = string.Format(strMsgContent, txtUserName.Text.Trim());
            }
            catch
            { }
            message.InsertMeaasge(strMsgContent, txtMobile.Text, txtUserName.Text, txtUserId.Text);
        }
        message.UpdateUserStepNO(txtUserId.Text, strStepNo);
    }
}
