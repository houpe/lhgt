﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="MenuManager.aspx.cs" Inherits="UserManage_MenuManager" Title="菜单管理" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">
    <div style="text-align: center">
        <table>
            <tr>
                <td>
                    菜单名</td>
                <td>
                    <asp:TextBox ID="txtMenu" runat="server">
                    </asp:TextBox>
                </td>
                <td>
                    <asp:Button ID="Button1" runat="server" OnClick="btnQuery_Clicked" Text="查询" />
                </td>
                <td>&nbsp;&nbsp;</td>
                <td>
                    <asp:Button ID="btnInsertRoot" runat="server" OnClick="btnInsertRoot_Click" Text="新增根菜单"
                        SkinID="LightGreen" />
                </td>
            </tr>
        </table>
    </div>
    <br />
    <defineControls:CustomGridView ID="CustomGridView1" runat="server" AllowPaging="True" AutoGenerateColumns="false"
        Width="550pt" ShowHideColModel="None" SkinID="List" OnOnLoadData="CustomGridView1_OnLoadData"
        HideColName="sortid" OnRowCreated="CustomGridView1_RowCreated">
        <Columns>
            <asp:BoundField DataField="parentMenu" HeaderText="父菜单名"></asp:BoundField>
            <asp:BoundField DataField="childMenu" HeaderText="子菜单名"></asp:BoundField>
            <asp:BoundField DataField="menu_url" HeaderText="路径"></asp:BoundField>
            <asp:TemplateField HeaderText="编辑记录">
                <itemtemplate>
                    <asp:HyperLink ID="hlEdit" runat="server" >编辑</asp:HyperLink>                                 
                
</itemtemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="删除记录">
                <itemstyle horizontalalign="Center" verticalalign="Middle" />
                <itemtemplate>
                     <asp:HyperLink ID="hlDelete" runat="server">删除</asp:HyperLink>
                
</itemtemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="新增">
                <itemstyle horizontalalign="Center" verticalalign="Middle" />
                <itemtemplate>
                     <asp:HyperLink ID="hlAdd" runat="server">新增</asp:HyperLink>
                
</itemtemplate>
            </asp:TemplateField>
        </Columns>
        <PagerSettings Visible="False" />
        <HeaderStyle CssClass="top_table" Height="30px" Width="50px" />
        <RowStyle CssClass="inputtable" />
    </defineControls:CustomGridView>
</asp:Content>
