﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="UnitsUserManager.aspx.cs" Inherits="UserManage_UnitsUserManager" Title="单位内部用户信息管理" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">
    <div style="text-align: center">
        <div style="text-align: center;">
            单位内部用户信息管理
        </div>
        <div style="text-align: right">
            <asp:Button ID="btnAddUnitUser" SkinID="LongLightGreen" runat="server" Text="添加内部用户"
                OnClick="btnAddUnitUser_Click" />
        </div>
        <defineControls:CustomGridView ID="CustomGridView1" runat="server" AllowPaging="True"
            AutoGenerateColumns="false" Width="100%" ShowHideColModel="None" SkinID="List"
            OnOnLoadData="CustomGridView1_OnLoadData" HideColName="sortid" OnRowCreated="CustomGridView1_RowCreated">
            <Columns>
                <asp:BoundField DataField="USERID" HeaderText="用户ID"></asp:BoundField>
                <asp:BoundField DataField="USERNAME" HeaderText="用户名"></asp:BoundField>
                <asp:BoundField DataField="Tel" HeaderText="电话"></asp:BoundField>
                <asp:BoundField DataField="ADDRESS" HeaderText="地址"></asp:BoundField>
                <asp:TemplateField HeaderText="删除操作">
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    <ItemTemplate>
                        <asp:HyperLink ID="hlDelete" runat="server">删除</asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </defineControls:CustomGridView>
    </div>
</asp:Content>
