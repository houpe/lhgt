﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SbBusiness;
using SbBusiness.User;
using Common;
public partial class UserManage_UsersManagerDetail : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
       
        if (!IsPostBack)
        {
            BindControl();
            if (!string.IsNullOrEmpty(Request.Params["id"]))
            {
                PersistenceControl1.KeyValue = Request.Params["id"];
                PersistenceControl1.Render();
                
            }
            
            string action = Request.Params["action"];
            if (action == "view")
            {
                this.btnSave.Visible = false;
                
                PersistenceControl1.Status = "view";

            }
            else if (action == "edit")
            {
                this.PersistenceControl1.Status = "edit";
                this.PersistenceControl1.UnlockControl();
            }
            else
                this.PersistenceControl1.Status = "insert";
        }
    }

    protected void BindControl()
    {
        SysUserTypeHandle userhandle = new SysUserTypeHandle();
        DataTable dtTemp = userhandle.GetAllUserType();
        if(dtTemp!=null)
        {
            this.ddlTYPE.DataSource = dtTemp;
            this.ddlTYPE.DataTextField = "usertypename";
            this.ddlTYPE.DataValueField = "usertypecode";
            this.ddlTYPE.DataBind();
        }
       
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (PersistenceControl1.Status == "view")
        {
            PersistenceControl1.UnlockControl();
            PersistenceControl1.Status = "edit";
            btnSave.Text = "保存";
        }
        else
        {
            if (PersistenceControl1.Status == "insert")
            {
                if (txtQUERY_PASS.Text != string.Empty)
                {
                    this.hfdUSERPWD.Value = Encode.Md5(txtQUERY_PASS.Text);
                }
                else
                {
                    this.txtQUERY_PASS.Text = "1";
                    this.hfdUSERPWD.Value = Encode.Md5(txtQUERY_PASS.Text);
                }
                
                PersistenceControl1.Update();
                Response.Redirect("~/UserManage/UsersManager.aspx");
            }
            else
            {
                if (txtQUERY_PASS.Text != string.Empty)
                {
                    this.hfdUSERPWD.Value = Encode.Md5(txtQUERY_PASS.Text);
                }
                else
                {
                    this.txtQUERY_PASS.Text = "1";
                    this.hfdUSERPWD.Value = Encode.Md5(txtQUERY_PASS.Text);
                }
                PersistenceControl1.Update();
                PersistenceControl1.LockControl();
                PersistenceControl1.Status = "view";
                btnSave.Text = "修改";
                Response.Redirect("~/UserManage/UsersManager.aspx");
            }
        }
    }

    protected void btnReturn_Click(object sender, EventArgs e)
    {
        if (PersistenceControl1.Status == "view")
        {
            Response.Write("<script>javascript:history.go(-2);</script>");
        }
        else
        {
            Response.Redirect("~/UserManage/UsersManager.aspx");
        }
    }
}
