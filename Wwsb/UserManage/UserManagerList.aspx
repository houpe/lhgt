﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="UserManagerList.aspx.cs" Inherits="UserManage_UserManagerList" Title="用户注册信息审核" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">
    <div style="text-align: center">
        <div style="text-align: center;">
            用户注册信息审核
        </div>
        <br />
        <div style="text-align: center">
            <defineControls:TabControl ID="tabFirst" SkinID="TabContentLong" runat="server">
                <defineControls:TabPage ID="tabPage1" runat="server" Caption="待审批用户">
                    <defineControls:CustomGridView ID="CustomGridView1" runat="server" AllowPaging="True" AutoGenerateColumns="false"
                        Width="550pt" ShowHideColModel="None" SkinID="List" OnOnLoadData="CustomGridView1_OnLoadData"
                        HideColName="sortid" OnRowCreated="CustomGridView1_RowCreated">
                        <Columns>
                            <asp:BoundField DataField="USERID" HeaderText="用户ID"></asp:BoundField>
                            <asp:BoundField DataField="USERNAME" HeaderText="用户名"></asp:BoundField>
                            <asp:BoundField DataField="Tel" HeaderText="电话"></asp:BoundField>
                            <asp:BoundField DataField="ADDRESS" HeaderText="地址"></asp:BoundField>
                            <asp:TemplateField HeaderText="审核操作">
                                <itemtemplate>
                    <asp:HyperLink ID="hlpass" runat="server" >通过</asp:HyperLink> 
</itemtemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="审核操作">
                                <itemstyle horizontalalign="Center" verticalalign="Middle" />
                                <itemtemplate>
                     <asp:HyperLink ID="hlunpass" runat="server">不通过</asp:HyperLink>                
</itemtemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="删除操作">
                                <itemstyle horizontalalign="Center" verticalalign="Middle" />
                                <itemtemplate>
                     <asp:HyperLink ID="hlDelete" runat="server">删除</asp:HyperLink>
</itemtemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerSettings Visible="False" />
                        <HeaderStyle CssClass="top_table" Height="30px" Width="50px" />
                        <RowStyle CssClass="inputtable" />
                    </defineControls:CustomGridView>
                </defineControls:TabPage>
                <defineControls:TabPage ID="tabPage2" runat="server" Caption="已审批用户">
                    <defineControls:CustomGridView ID="CustomGridView2" runat="server" AllowPaging="True" AutoGenerateColumns="false"
                        Width="550pt" ShowHideColModel="None" SkinID="List" OnOnLoadData="CustomGridView1_OnLoadData"
                        HideColName="sortid" OnRowCreated="CustomGridView1_RowCreated">
                        <Columns>
                            <asp:BoundField DataField="USERID" HeaderText="用户ID"></asp:BoundField>
                            <asp:BoundField DataField="USERNAME" HeaderText="用户名"></asp:BoundField>
                            <asp:BoundField DataField="Tel" HeaderText="电话"></asp:BoundField>
                            <asp:BoundField DataField="spjg" HeaderText="审核状态"></asp:BoundField>
                        </Columns>
                        <PagerSettings Visible="False" />
                        <HeaderStyle CssClass="top_table" Height="30px" Width="50px" />
                        <RowStyle CssClass="inputtable" />
                    </defineControls:CustomGridView>
                </defineControls:TabPage>
            </defineControls:TabControl>
        </div>
    </div>
</asp:Content>
