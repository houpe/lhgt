﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using SbBusiness;
using Business.Admin;

public partial class UserManage_ParamManager : System.Web.UI.Page
{   
    private int flag;
    private DictOperation dictOperation = new DictOperation();

    protected void Page_Load(object sender, EventArgs e)
    {        
        string action = Request.QueryString["action"];
        if (action == "delete")
        {
            string name = Request.QueryString["name"];
            if (!string.IsNullOrEmpty(name))
            {
                SystemLogs wy = new SystemLogs();
                wy.Inputlog(Session["LoginUserName"].ToString(), "删除sys_params的行", name);                    
                dictOperation.DeleteDictName(name);
            }
        }

        if (!IsPostBack)
        {
            BindData("");
            flag = 0;
        }
    }

    protected void CustomGridView1_OnLoadData(object sender, EventArgs e)
    {
        if (flag == 0)
            BindData("");
        else
            BindData(this.txtParamItem.Text);
    }

    private void BindData(string str)
    {
        DataTable dtSource = null;

        if (string.IsNullOrEmpty(str))
        {
            dtSource = dictOperation.GetDictNames();
        }
        else
        {
            dtSource = dictOperation.GetDictNames(str);
        }

        CustomGridView1.DataSource = dtSource;
        CustomGridView1.RecordCount = dtSource.Rows.Count;
        CustomGridView1.DataBind();
    }

    protected void btnInsertRoot_Click(object sender, EventArgs e)
    {
        Response.Write("<script type='text/javascript>prompt('请输入数据字典类别名','')</script>");
    }

    protected void btnQuery_Clicked(object sender, EventArgs e)
    {
        if (this.txtParamItem.Text == string.Empty)
        {
            BindData("");
            flag = 0;
        }
        else 
        {
            BindData(this.txtParamItem.Text);
            flag = 1;
        }
    }
}
