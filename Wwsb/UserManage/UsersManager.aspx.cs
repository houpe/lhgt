﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SbBusiness;
using SbBusiness.User;

public partial class UserManage_UsersManager : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request["action"]))
            {
                if (Request["action"] == "delete")
                {
                    DelUserInfo();
                }
            }
            BindData();
        }
    }
    protected void DelUserInfo()
    {
        if (!string.IsNullOrEmpty(Request["id"]))
        {
            if (Session["UserID"].ToString() == SystemConfig.AdminUser)
            {
                SystemLogs wyLog = new SystemLogs();
                wyLog.Inputlog(Session["LoginUserName"].ToString(), "删除sys_user的行", Request["id"]);
                SysUserHandle.DelUserInfoById(Request["id"]);
            }
            else
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "scriptB", "<script>alert('不是超级管理员不能删除！');</script>");
            }
        } 
    }
    protected void BindData()
    { 
        SysUserHandle userhandle=new SysUserHandle();
        DataTable dtTemp = userhandle.GetAllUsers(this.userid.Text.Trim(),this.username.Text.Trim());        

        if (dtTemp != null)
        { 
            this.CustomGridView1.DataSource = dtTemp;
            this.CustomGridView1.RecordCount = dtTemp.Rows.Count;
            this.CustomGridView1.PageSize = SystemConfig.PageSize;
            this.CustomGridView1.HideColName = "ID";
            this.CustomGridView1.DataBind();
        }

    }
    protected void CustomGridView1_OnLoadData(object sender, EventArgs e)
    {
        BindData();
    }

    protected void CustomGridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        switch (e.Row.RowType)
        { 
            case DataControlRowType.Header:
                break;
            case DataControlRowType.DataRow:
                {
                    HyperLink linkview = e.Row.FindControl("hlView") as HyperLink;
                    HyperLink linkedit = e.Row.FindControl("hlEdit") as HyperLink;
                    HyperLink linkdel = e.Row.FindControl("hlDel") as HyperLink;

                    string id = string.Empty;

                    if (e.Row.DataItem != null)
                    {
                        id = ((DataRowView)e.Row.DataItem)["ID"].ToString();

                    }
                    linkview.NavigateUrl = "~/UserManage/UsersManagerDetail.aspx?id=" + id + "&action=view";
                    linkedit.NavigateUrl = "~/UserManage/UsersManagerDetail.aspx?id=" + id + "&action=edit";
                    linkdel.NavigateUrl = "~/UserManage/UsersManager.aspx?id=" + id + "&action=delete";
                } break;
        }
       
       
    }
    protected void btnQuery_Click(object sender, EventArgs e)
    {
        BindData();
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/UserManage/UsersManagerDetail.aspx?action=insert");
    }
}
