﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ParamItemManager.aspx.cs" Inherits="UserManage_ParamItemManager" Title="数据字典管理" %>

<%@ Register Src="../UserControls/PersistenceControl.ascx" TagName="PersistenceControl"
    TagPrefix="uc2" %>

<%@ Register Src="../UserControls/TextBoxWithValidator.ascx" TagName="TextBoxWithValidator"
    TagPrefix="uc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">
    <div style="text-align: center">
        <script type="text/javascript" src="../ScriptFile/Regex.js"></script>
        <table border="0" cellpadding="0" cellspacing="0" class="window_tab" style="width: 80%">
            <tr>
                <td class="txtrighttd">
                    <asp:Label ID="Label2" runat="server" Text="数据字典类名"></asp:Label></td>
                <td class="txtlefttd">
                    <uc1:textboxwithvalidator id="txtName" runat="server" errormessage="数据字典类名不能为空" type="Required"
                        validateemptytext="true" width="300pt" />
                </td>
            </tr>
            <tr>
                <td class="txtrighttd">
                    <asp:Label ID="Label3" runat="server" Text="数据字典项名"></asp:Label></td>
                <td class="txtlefttd">
                    <uc1:textboxwithvalidator id="txtKeyvalue" runat="server" errormessage="数据字典项名不能为空" width="300pt" />
                </td>
            </tr>
            <tr>
                <td class="txtrighttd">
                    <asp:Label ID="Label1" runat="server" Text="数据字典项值"></asp:Label></td>
                <td class="txtlefttd">
                    <uc1:textboxwithvalidator id="txtKeycode" runat="server" errormessage="数据字典项值不能为空" Type="integer" width="300pt" />
                </td>
            </tr>
        </table>
        <asp:Button ID="btnSave" runat="server" Text="保存" OnClick="btnSave_Click" SkinID="LightGreen" />
        <asp:Button ID="btnReturn" runat="server" Text="返回" CausesValidation="False" OnClick="btnReturn_Click" SkinID="LightGreen"/>
    </div>
    <uc2:persistencecontrol id="PersistenceControl1" runat="server" key="rowid" table="SYS_PARAMS" />
</asp:Content>
