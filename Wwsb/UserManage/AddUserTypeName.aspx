﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddUserTypeName.aspx.cs"
    Inherits="UserManage_AddUserTypeName" Title="添加类型名称" %>

<%@ Register Src="../UserControls/TextBoxWithValidator.ascx" TagName="TextBoxWithValidator"
    TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>添加类型名称</title>
</head>
<body>
    <form id="form1" runat="server">
        <div style="text-align: center">
            <br />
            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="right">
                        用户类型名
                    </td>
                    <td align="left">
                        <uc1:TextBoxWithValidator ID="txtUserTypeName" runat="server" ErrorMessage="不能为空"
                            Type="Required" />
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        系统名称
                    </td>
                    <td align="left">
                        <defineControls:DropDownList ID="ddlSysName" runat="server">
                        </defineControls:DropDownList></td>
                </tr>
                <tr>
                    <td colspan="2">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Button ID="btnCommit" runat="server" Text="提交" OnClick="btnCommit_Click" SkinID="LightGreen" />
                    </td>
                    <td>
                        <input type="button" id="btnCancel" class="button" value="取消" onclick="Javascript:window.close();" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
