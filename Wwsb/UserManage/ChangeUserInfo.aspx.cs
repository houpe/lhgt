﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using SbBusiness.User;
using SbBusiness;
using SbBusiness.Wsbs;
using Business.Common;


public partial class UserManage_ChangeUserInfo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindUserInfo();
            Session["DangQianRenWu"] = "个人信息维护";
        }
    }

    /// <summary>
    /// 绑定用户信息
    /// </summary>
    /// <!--addby zhongjian 20100130-->
    protected void BindUserInfo()
    {
        Page.ClientScript.RegisterStartupScript(this.GetType(), "bind", "<script>UserType_onchange();</script>");
        if (Session["userid"] != null)
        {
            Session["DangQianRenWu"] = "个人信息维护";
            //用户信息
            DataTable dtTemp = SysUserHandle.GetUser(Session["userid"].ToString());
            if (dtTemp.Rows.Count > 0)
            {
                txtUserId.Text = dtTemp.Rows[0]["userID"].ToString();
                txtUserName.Text = dtTemp.Rows[0]["username"].ToString();
                ddlZjlx.Text = dtTemp.Rows[0]["IDTYPE"].ToString();
                txtZjhm.Text = dtTemp.Rows[0]["IDNUMBER"].ToString();
                txtTel.Text = dtTemp.Rows[0]["TEL"].ToString();
                txtCz.Text = dtTemp.Rows[0]["FAX"].ToString();
                txtMobile.Text = dtTemp.Rows[0]["MOBILE"].ToString();
                txtEmail.Text = dtTemp.Rows[0]["EMAIL"].ToString();
                txtAddress.Text = dtTemp.Rows[0]["ADDRESS"].ToString();
                txtYZBM.Text = dtTemp.Rows[0]["POSTCODE"].ToString();

                ddlSex.DefaultValue = dtTemp.Rows[0]["sex"].ToString();
                ViewState["UserType"] = dtTemp.Rows[0]["USERTYPE"].ToString();//用户类型
                ViewState["UnitsID"] = dtTemp.Rows[0]["UNITID"].ToString();//单位ID
                ViewState["UnitsAdmin"] = dtTemp.Rows[0]["UNITSADMIN"].ToString();//单位管理员标识
                ViewState["Sync_Type"] = dtTemp.Rows[0]["sync_type"].ToString();//数据交换类型(2:地信中心数据)
                ViewState["strID"] = dtTemp.Rows[0]["id"].ToString();//主键ID
                ViewState["strtype"] = dtTemp.Rows[0]["type"].ToString();//用户注册类型

            }
            if (ViewState["UserType"].ToString() == "1" && ViewState["UnitsID"].ToString() != "")
            {
                dtTemp = SysUserHandle.GetUnitsInfo(ViewState["UnitsID"].ToString());
                if (dtTemp.Rows.Count > 0)
                {
                    txtUnitsName.Text = dtTemp.Rows[0]["UNITSNAME"].ToString();
                    txtOrganization.Text = dtTemp.Rows[0]["Organization"].ToString();
                    txtLegal.Text = dtTemp.Rows[0]["Legal"].ToString();
                    txtUnitsTel.Text = dtTemp.Rows[0]["UNITSTEL"].ToString();
                    txtNature.Text = dtTemp.Rows[0]["Nature"].ToString();
                    txtUnitsFax.Text = dtTemp.Rows[0]["FAX"].ToString();
                    txtUnitsAddress.Text = dtTemp.Rows[0]["UNITSADDRESS"].ToString();
                    txtUnitsPostCode.Text = dtTemp.Rows[0]["POSTCODE"].ToString();
                }

                if (ViewState["UnitsAdmin"].ToString() != "1")
                {
                    txtUnitsName.ReadOnly = true;
                    txtOrganization.ReadOnly = true;
                    txtLegal.ReadOnly = true;
                    txtUnitsTel.ReadOnly = true;
                    txtNature.ReadOnly = true;
                    txtUnitsFax.ReadOnly = true;
                    txtUnitsAddress.ReadOnly = true;
                    txtUnitsPostCode.ReadOnly = true;
                }
                else
                {
                    btnManageUser.Visible = true;
                    btnManageUser.ToolTip = ViewState["UnitsID"].ToString();
                }
            }
        }
    }

    protected void btnRefer_Click(object sender, EventArgs e)
    {
        try
        {
            if (HiddenFieldFlag.Value == "0" || HiddenFieldFlag.Value == "1"
                || HiddenFieldFlag.Value == "2" || HiddenFieldFlag.Value == "3" || HiddenFieldFlag.Value == "4")
            {
                return;
            }
            else
            {
                string strRemark = string.Empty;
                if (ViewState["UserType"].ToString() == "1" && ViewState["UnitsAdmin"].ToString() == "1" && ViewState["UnitsID"].ToString() != "")
                {
                    bool flag = SysUserHandle.UpdateUnits(txtUnitsName.Text.Trim(), txtUnitsTel.Text.Trim(), txtUnitsAddress.Text.Trim(),
                        txtUnitsFax.Text.Trim(), txtUnitsPostCode.Text.Trim(), txtOrganization.Text.Trim(), txtLegal.Text.Trim(),
                        txtNature.Text.Trim(), ViewState["UnitsID"].ToString());
                    //添加操作日志 addby zhongjian 20100421
                    strRemark = string.Format("修改单位信息,单位ID: {0},单位名称: {1}", ViewState["UnitsID"].ToString(), txtUnitsName.Text.Trim());
                    SystemLogs.AddSystemLogs(Session["userid"].ToString(), "update", strRemark, "");
                }
                SysUserHandle.UpdateUserInfo(txtUserId.Text, txtUserName.Text, txtUserPwd.Text,
                     int.Parse(ViewState["strtype"].ToString()), txtTel.Text, txtMobile.Text,
                     txtAddress.Text, ddlZjlx.Value, txtZjhm.Text, txtCz.Text, txtEmail.Text,
                     txtYZBM.Text, 1, ViewState["UserType"].ToString(), ViewState["UnitsID"].ToString(), ViewState["UnitsAdmin"].ToString(),ddlSex.Value);

                //添加操作日志 addby zhongjian 20100421
                strRemark = string.Format("修改用户信息,用户ID: {0},用户名称: {1}", txtUserId.Text, txtUserName.Text);
                SystemLogs.AddSystemLogs(Session["userid"].ToString(), "update", strRemark, "");

                BindUserInfo();

                string strWrite = string.Format(@"<script>alert('修改用户信息成功。');</script>");
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "", strWrite);
            }
        }
        catch (Exception ex)
        {
            ExceptionManager.WriteAlert(this, ex.Message);
        }
    }

    /// <summary>
    /// 获取用户类型
    /// </summary>
    /// <returns></returns>
    public string GetUserType()
    {
        if (ViewState["UserType"] != null)
            return ViewState["UserType"].ToString();
        else
            return "0";
    }

    /// <summary>
    /// 内部单位用户管理
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnManageUser_Click(object sender, EventArgs e)
    {
        Response.Redirect("UnitsUserManager.aspx?ParentName=个人信息管理&unitid=" + btnManageUser.ToolTip);
    }
}
