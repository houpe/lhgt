﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="UnitsUserAdd.aspx.cs" Inherits="UserManage_UnitsUserAdd" Title="单位用户添加" %>

<%@ Register Src="../UserControls/GeneralSelect.ascx" TagName="GeneralSelect" TagPrefix="uc2" %>
<%@ Register Src="../UserControls/TextBoxWithValidator.ascx" TagName="TextBoxWithValidator"
    TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/MsgPlate.ascx" TagName="MsgPlate" TagPrefix="uc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">
    <script src="../ScriptFile/Regex.js" type="text/javascript"></script>
    <script type="text/javascript">
        function onRefer() {
            var eUserId = document.getElementById("<%=txtUserId.ChildControlClientId %>");
            var eUserPwd = document.getElementById("<%=txtUserPwd.ChildControlClientId %>");
            var eUserRePwd = document.getElementById("<%=txtUserRePwd.ClientID %>");
            var eHidden = document.getElementById("<%=HiddenFieldFlag.ClientID %>");
            var eZjhm = document.getElementById("<%=txtZjhm.ClientID %>");

            eHidden.value = "";
            if (eUserId.value == "") {
                alert("用户名不能为空");
                eHidden.value = "0";
            }
            if (eUserPwd.value == "") {
                alert("密码不能为空");
                eHidden.value = "1";
            }
            if (eUserPwd.value != eUserRePwd.value) {
                alert("两次密码输入不一致密码");
                eHidden.value = "2";
            }
            if (eZjhm.value == "") {
                alert("证件号码不能为空");
                eHidden.value = "3";
            }
        }

        function SetCusIsValid(objcon) {
            var sourceid = objcon.id.replace("ddlZjlx_ddlControl", "cvZjhm");
            var objsource = document.getElementById(sourceid);
            if (objcon.value == "军官证" || objcon.value == "其它") {
                objsource.enabled = false;
            }
            else {
                objsource.enabled = true;
            }
        }
    </script>
    <div style="text-align: center;">
        <table width="95%" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <fieldset style="width: 100%; text-align: center">
                        <legend style="font-weight: bold; font-size: 12;">单位用户添加</legend>
                        <table class="inputtable" style="width: 100%;" cellpadding="4" cellspacing="0">
                            <tr>
                                <td style="background-color: Snow; width: 100px">
                                    用 户 名
                                </td>
                                <td align="left">
                                    <uc1:TextBoxWithValidator ID="txtUserId" Width="120px" runat="server" Type="UserId">
                                    </uc1:TextBoxWithValidator>
                                    <font color="red">必须输入以字母开头的6-16个字母、数字或下划线</font>
                                </td>
                                <td style="background-color: Snow; width: 100px">
                                    姓&nbsp;&nbsp;&nbsp;&nbsp;名
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtUserName" Width="120px" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="background-color: Snow; width: 100px">
                                    登录密码
                                </td>
                                <td align="left">
                                    <uc1:TextBoxWithValidator ID="txtUserPwd" Width="120px" runat="server" Type="Passwords"
                                        TextMode="Password"></uc1:TextBoxWithValidator>
                                    <font color="red">请输入6-20个字母、数字、下划线</font>
                                </td>
                                <td style="background-color: Snow; width: 100px">
                                    重复密码
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtUserRePwd" Width="120px" runat="server" TextMode="Password"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="background-color: Snow; width: 100px">
                                    证件类别
                                </td>
                                <td align="left">
                                    <uc2:GeneralSelect ID="ddlZjlx" Width="120px" runat="server" ClientOnChangeScript="SetCusIsValid(this)"
                                        ValidateEmpty="true" Name="证件类型" />
                                </td>
                                <td style="background-color: Snow; width: 100px">
                                    证件号码
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtZjhm" Width="120px" runat="server"></asp:TextBox>
                                    <%--<asp:CustomValidator ID="cvZjhm" runat="server" ControlToValidate="txtZjhm" ErrorMessage="*"
                                        Display="Dynamic" ClientValidationFunction="checkNumberNew" ValidateEmptyText="true"></asp:CustomValidator>&nbsp;&nbsp;
                                    <font color="red">请输入正确的证件号码</font>--%>
                                </td>
                            </tr>
                            <tr>
                                <td style="background-color: Snow; width: 100px">
                                    联系电话
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtTel" Width="120px" runat="server"></asp:TextBox>
                                </td>
                                <td style="background-color: Snow; width: 100px">
                                    传&nbsp;&nbsp;&nbsp;&nbsp;真
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtCz" Width="120px" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="background-color: Snow; width: 100px">
                                    手机号码
                                </td>
                                <td align="left">
                                    <uc1:TextBoxWithValidator ID="txtMobile" Width="120px" runat="server" Type="Phone">
                                    </uc1:TextBoxWithValidator>
                                    <font color="red">请输入正确的手机号码</font>
                                </td>
                                <td style="background-color: Snow; width: 100px">
                                    电子邮件
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtEmail" Width="120px" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="background-color: Snow; width: 100px">
                                    邮政编码
                                </td>
                                <td align="left">
                                    <uc1:TextBoxWithValidator ID="txtYZBM" Width="120px" runat="server" Type="Postcode">
                                    </uc1:TextBoxWithValidator>
                                    <font color="red">请输入正确的邮政编码</font>
                                </td>
                                <td>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td style="background-color: Snow; width: 100px">
                                    联系地址
                                </td>
                                <td align="left" colspan="3">
                                    <asp:TextBox ID="txtAddress" Width="95%" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="labMsg" ForeColor="red" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Button ID="btnRefer" SkinID="LightGreen" runat="server" Text="保存" OnClick="btnRefer_Click"
                        OnClientClick="onRefer ();" />
                    <input type="button" value="关闭" class="button" onclick="javascript:window.close()" />
                </td>
            </tr>
        </table>
        <asp:HiddenField ID="HiddenFieldFlag" runat="server" />
    </div>
</asp:Content>
