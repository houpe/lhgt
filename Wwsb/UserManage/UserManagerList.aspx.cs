﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SbBusiness.User;

public partial class UserManage_UserManagerList : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["action"]))
        {
            if (Request["action"] == "pass")
            {
                SysUserHandle.UpdataPassData(Request["username"].ToString());
            }
            else if (Request["action"] == "unpass")
            {
                SysUserHandle.UpdataUnPassData(Request["username"].ToString());
            }
            else if (Request["action"] == "delete")
            {
                SysUserHandle.DelUserInfoByUserID(Request["username"].ToString());
            }
        }

        if (!IsPostBack)
        {
            BindData();
        }
    }
    /// <summary>
    /// 更新GridView事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void CustomGridView1_OnLoadData(object sender, EventArgs e)
    {
        BindData();
    }
    /// <summary>
    /// 创建GridView行
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void CustomGridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        switch (e.Row.RowType)
        {
            case DataControlRowType.DataRow:
                DataRowView dtrv = e.Row.DataItem as DataRowView;
                if (dtrv == null)
                {
                    break;
                }

                HyperLink hlDelete = e.Row.FindControl("hlDelete") as HyperLink;
                if (hlDelete != null)
                {
                    hlDelete.Attributes.Add("OnClick", "return confirm('确定要删除该用户?')");
                    hlDelete.NavigateUrl = "~/UserManage/UserManagerList.aspx?action=delete&username=" + dtrv["USERID"];
                }

                HyperLink hlpass = e.Row.FindControl("hlpass") as HyperLink;
                if (hlpass != null)
                {
                    hlpass.NavigateUrl = "~/UserManage/UserManagerList.aspx?action=pass&username=" + dtrv["USERID"];
                }

                HyperLink hlunpass = e.Row.FindControl("hlunpass") as HyperLink;
                if (hlunpass != null)
                {
                    hlunpass.NavigateUrl = "~/UserManage/UserManagerList.aspx?action=unpass&username=" + dtrv["USERID"];
                }
                break;
        }
    }
    /// <summary>
    /// 绑定数据
    /// </summary>
    public void BindData()
    {
        SysUserHandle userhandle = new SysUserHandle();

        DataTable dtTemp = userhandle.GetAllUserExclusionByUser(false);
        if (dtTemp != null)
        {
            this.CustomGridView1.DataSource = dtTemp;
            this.CustomGridView1.RecordCount = dtTemp.Rows.Count;
            this.CustomGridView1.PageSize = SbBusiness.SystemConfig.PageSize;
            this.CustomGridView1.HideColName = "ID";
            this.CustomGridView1.DataBind();
        }

        dtTemp = userhandle.GetAllUserExclusionByUser(true);
        if (dtTemp != null)
        {
            this.CustomGridView2.DataSource = dtTemp;
            this.CustomGridView2.RecordCount = dtTemp.Rows.Count;
            this.CustomGridView2.PageSize = SbBusiness.SystemConfig.PageSize;
            this.CustomGridView2.HideColName = "ID";
            this.CustomGridView2.DataBind();
        }
    }
}
