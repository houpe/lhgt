﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using SbBusiness;
using SbBusiness.User;

public partial class UserManage_UserPassQuery : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindData();
        }
    }

    /// <summary>
    /// 绑定数据
    /// </summary>
    private void BindData()
    {
        DataTable dtSource = SysUserHandle.QueryPassByUserInfo(txtUserId.Text, txtUserName.Text);

        if (dtSource!=null)
        {
            this.CustomGridView1.DataSource = dtSource;
            this.CustomGridView1.RecordCount = dtSource.Rows.Count;
            this.CustomGridView1.PageSize = SystemConfig.PageSize;
            this.CustomGridView1.DataBind();
        }
    }

    protected void btnOk_Click(object sender, EventArgs e)
    {
        BindData();
    }

    protected void CustomGridView1_OnLoadData(object sender, EventArgs e)
    {
        BindData();
    }
}
