﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SbBusiness.User;
using SbBusiness;
using SbBusiness.Menu;


public partial class UserManage_UserPrivilage : System.Web.UI.Page
{
    SysUserTypeHandle sutoTemp = new SysUserTypeHandle();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Params["action"] == "delete")
        {
            if (Session["UserID"].ToString() == SystemConfig.AdminUser)
            {
                sutoTemp.DeleteMenuUserType(Request.Params["id"]);
            }
            else
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "scriptB", "<script>alert('不是超级管理员不能删除！');</script>");
            }
        }
        if (!IsPostBack)
        {
            BindData();           
        }
    }

    private void BindData()
    {
        
        DataTable dtTable = sutoTemp.GetUserTypes();

        CustomGridView1.DataSource = dtTable;
        CustomGridView1.HideColName = "ID";
        CustomGridView1.PageSize = SystemConfig.PageSize;
        CustomGridView1.RecordCount = dtTable.Rows.Count;
        CustomGridView1.DataBind();

    }

    protected void CustomGridView1_OnLoadData(object sender, EventArgs e)
    {
        BindData();
    }

    protected void CustomGridView1_RowCreated(object sender, GridViewRowEventArgs e)
    { 
        switch (e.Row.RowType)
        {
            case DataControlRowType.DataRow:
                DataRowView dtrv = e.Row.DataItem as DataRowView;
                if (dtrv == null)
                {
                    break;
                }
                HyperLink hlDelete = e.Row.FindControl("hlDelete") as HyperLink;
                if (hlDelete != null)
                {
                    hlDelete.Attributes.Add("OnClick", "return confirm('确定要删除该菜单?')");
                    hlDelete.NavigateUrl = "~/UserManage/UserPrivilage.aspx?action=delete&id=" + dtrv["id"];
                }
                break;
        }
    }
   
}
