﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SbBusiness.Menu;
using SbBusiness;


public partial class UserManage_MenuItemManager : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request.Params["parentId"]))
            {
                txtParent_id.Text = Request.Params["parentId"];
            }
            else
            {
                Label1.Visible = false;
                txtParent_id.Visible = false;
                //txtParent_id.ID = "hiddenCtrl";                    
            }

            string action = Request.Params["action"];
            if (action != "add")
            {
                PersistenceControl1.KeyValue = Request.Params["id"];

                PersistenceControl1.PageControl = Page.Master.FindControl("ContentPlaceHolderContent");
                PersistenceControl1.Render();
                PersistenceControl1.UnlockControl();
            }
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Session["UserID"].ToString() == SystemConfig.AdminUser)
        {
            PersistenceControl1.Update();
            Response.Redirect("MenuManager.aspx");
        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "scriptBak", "<script>alert('不是超级管理员不能删除！');</script>");
        }
    }
    protected void btnReturn_Click(object sender, EventArgs e)
    {
        Response.Redirect("MenuManager.aspx");
    }
}
