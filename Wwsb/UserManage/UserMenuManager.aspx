﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="UserMenuManager.aspx.cs" Inherits="UserManage_UserMenuManager" Title="菜单权限" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">
    <div style="text-align: center">
        <defineControls:CustomGridView ID="CustomGridView1" runat="server" AllowPaging="True" AutoGenerateColumns="false"
            ShowItemNumber="true" SkinID="List" CheckboxAutoPostBack="False" CustomImportToExcel="false"
            CustomSortExpression="" DeleteToolTipOfCol="-1" HideTextLength="20" ShowCmpAndCancel="False"
            ShowGridViewSelect="List" ShowImportButton="False" ShowTreeView="False" SelectOrDelete="False"
            AllowSortingAscImgCss="" AllowSortingDescImgCss="" Width="600" OnOnLoadData="CustomGridView1_OnLoadData"
            OnRowCreated="CustomGridView1_RowCreated">
            <Columns>
                <asp:BoundField DataField="parentmenu" HeaderText="父菜单名"></asp:BoundField>
                <asp:BoundField DataField="childmenu" HeaderText="子菜单名"></asp:BoundField>
                <asp:BoundField DataField="menu_url" HeaderText="路径"></asp:BoundField>
                <asp:TemplateField HeaderText="查看权限">
                    <itemtemplate>
<asp:CheckBox id="ckbAuth" runat="server" Text="可查看"></asp:CheckBox> 
</itemtemplate>
                </asp:TemplateField>
            </Columns>
            <PagerSettings Visible="False" />
        </defineControls:CustomGridView>
        <asp:Button ID="btnSave" runat="server" Text="保存" OnClick="btnSave_Click" SkinID="LightGreen"/>
        <asp:Button ID="btnReturn" runat="server" Text="返回" OnClick="btnReturn_Click"  SkinID="LightGreen"/>
    </div>
</asp:Content>
