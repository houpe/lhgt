﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SbBusiness.User;

public partial class UserManage_UnitsUserManager : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["action"]))
        {
            if (Request["action"] == "delete")
            {
                SysUserHandle.DelUserInfoByUserID(Request["userid"]);
            }
        }

        if (!IsPostBack)
        {
            BindData();
        }
    }
    /// <summary>
    /// 更新GridView事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void CustomGridView1_OnLoadData(object sender, EventArgs e)
    {
        BindData();
    }
    /// <summary>
    /// 创建GridView行
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void CustomGridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        switch (e.Row.RowType)
        {
            case DataControlRowType.DataRow:
                DataRowView dtrv = e.Row.DataItem as DataRowView;
                if (dtrv == null)
                {
                    break;
                }

                HyperLink hlDelete = e.Row.FindControl("hlDelete") as HyperLink;
                if (hlDelete != null)
                {
                    hlDelete.Attributes.Add("OnClick", "return confirm('确定要删除该用户?')");
                    hlDelete.NavigateUrl = "UnitsUserManager.aspx?action=delete&userid=" + dtrv["USERID"];
                }
                break;
        }
    }
    /// <summary>
    /// 绑定数据
    /// </summary>
    public void BindData()
    {
        if (!string.IsNullOrEmpty(Request["unitid"]))
        {
            SysUserHandle userhandle = new SysUserHandle();
            DataTable dtTemp = userhandle.GetUserInUnit(Request["unitid"]);
            if (dtTemp != null)
            {
                this.CustomGridView1.DataSource = dtTemp;
                this.CustomGridView1.RecordCount = dtTemp.Rows.Count;
                this.CustomGridView1.PageSize = SbBusiness.SystemConfig.PageSize;
                this.CustomGridView1.HideColName = "ID";
                this.CustomGridView1.DataBind();
            }
        }
    }

    /// <summary>
    /// 内部单位用户管理
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnAddUnitUser_Click(object sender, EventArgs e)
    {
        Response.Redirect("UnitsUserAdd.aspx?ParentName=个人信息管理");
    }
}
