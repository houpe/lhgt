﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="UserPrivilage.aspx.cs" Inherits="UserManage_UserPrivilage" Title="人员权限管理" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">
    <div style="text-align: center">
        <defineControls:CustomGridView ID="CustomGridView1" runat="server" AllowPaging="True" AutoGenerateColumns="false"
            ShowItemNumber="true" SkinID="List" CheckboxAutoPostBack="False" CustomImportToExcel="false"
            CustomSortExpression="" DeleteToolTipOfCol="-1" HideTextLength="20" ShowCmpAndCancel="False"
            ShowGridViewSelect="List" ShowHideColModel="None" ShowImportButton="False" ShowTreeView="False"
            SelectOrDelete="False" AllowSortingAscImgCss="" AllowSortingDescImgCss="" Width="600"
            OnOnLoadData="CustomGridView1_OnLoadData" OnRowCreated="CustomGridView1_RowCreated">
            <Columns>
                <asp:BoundField DataField="系统代号" HeaderText="系统代号"></asp:BoundField>
                <asp:BoundField DataField="用户类型名" HeaderText="用户类型名"></asp:BoundField>
                <asp:BoundField DataField="用户类型代码" HeaderText="用户类型代码"></asp:BoundField>
                <asp:BoundField DataField="系统名称" HeaderText="系统名称"></asp:BoundField>
                <asp:TemplateField HeaderText="菜单权限">
                    <itemtemplate>
                        <asp:HyperLink id="hlEdit" runat="server" NavigateUrl='<%# DataBinder.Eval(Container.DataItem,"ID", "UserMenuManager.aspx?usertype_id={0}") %>'>编辑</asp:HyperLink> 
                    </itemtemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="删除菜单">
                    <itemtemplate>
                        <asp:HyperLink id="hlDelete" runat="server">删除</asp:HyperLink> 
                    </itemtemplate>
                </asp:TemplateField>
            </Columns>
            <PagerSettings Visible="False" />
        </defineControls:CustomGridView>
        <input type="button" id="btnAddSysName" class="button" onclick="Javascript:window.open('AddSysName.aspx','添加系统名称','height=100,width=300,top=200px,left=200px,toolbar =no,menubar=no, scrollbars=yes, resizable=no, location=no, status=yes');"
            value="添加系统" />
        &nbsp;&nbsp;&nbsp;
        <input type="button" id="btnAddUserType" class="button" onclick="Javascript:window.open('AddUserTypeName.aspx','添加类型名称','height=100,width=300,top=200px,left=200px,toolbar =no,menubar=no, scrollbars=yes, resizable=no, location=no, status=yes');"
            value="添加类型" />
    </div>
</asp:Content>
