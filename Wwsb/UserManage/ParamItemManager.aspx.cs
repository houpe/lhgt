﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SbBusiness;


public partial class UserManage_ParamItemManager : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string action = Request.QueryString["action"];

        if (!IsPostBack)
        {
            PersistenceControl1.KeyValue = Request.QueryString["rowid"];
            PersistenceControl1.Status = "edit";
            PersistenceControl1.Render();
        }
        if (action != "addroot")
        {
            txtName.Enabled = false;
        }
        if (action == "add")
        {
            txtName.Value = Request.QueryString["name"];
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Session["UserID"].ToString() == SystemConfig.AdminUser)
        {
            PersistenceControl1.Update();
            Response.Redirect("ParamNameManager.aspx?name=" + txtName.Text);
        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "scriptBak", "<script>alert('不是超级管理员不能删除！');</script>");
        }
        
    }
    protected void btnReturn_Click(object sender, EventArgs e)
    {
        Response.Redirect("ParamNameManager.aspx?name=" + txtName.Text);
    }
}
