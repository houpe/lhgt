﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SbBusiness;
using SbBusiness.User;
using Common;

public partial class UserManage_AddSysName : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void btnCommit_Click(object sender, EventArgs e)
    {
        SysUserTypeHandle sutHandle = new SysUserTypeHandle();
        if (sutHandle.CheckSysName(txtUserTypeName.Text))
        {
            WindowAppear.WriteAlert(Page, "系统名已存在");
            return;
        }

        if (!string.IsNullOrEmpty(txtUserTypeName.Text))
        {
            if (sutHandle.InsertSysName(txtUserTypeName.Text))
            {
                Response.Write("<script language='javascript'>alert('插入成功');window.opener.location.href=window.opener.location.href;window.opener=null;window.close();</script>");
            }
            else
            {
                Response.Write("<script language='javascript'>alert('插入失败')</script>");
            }
        }
    }
   
}
