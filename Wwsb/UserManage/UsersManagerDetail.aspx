﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master"
    CodeFile="UsersManagerDetail.aspx.cs" Inherits="UserManage_UsersManagerDetail"
    Title="人员具体信息" %>

<%@ Register Src="../UserControls/PersistenceControl.ascx" TagName="PersistenceControl"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControls/TextBoxWithValidator.ascx" TagName="TextBoxWithValidator"
    TagPrefix="uc2" %>
<%@ Register Src="../UserControls/GeneralSelect.ascx" TagName="GeneralSelect" TagPrefix="uc3" %>
<asp:Content ContentPlaceHolderID="ContentPlaceHolderContent" ID="content" runat="server">
    <div style="text-align: center">
        <table class="inputtable" style="width: 100%; ">
            <tr style="height:30px">
                <td style="width:15%">
                    用户ID：</td>
                <td style="width:35%">
                    <uc2:TextBoxWithValidator ID="txtUSERID" runat="server" Width="98%" />
                </td>
                <td style="width:15%">
                    用户名：</td>
                <td style="width:35%">
                    <uc2:TextBoxWithValidator ID="txtUSERNAME" runat="server" Width="98%" />
                </td>
            </tr>
            <tr style="height:30px">
                <td>
                    用户密码：</td>
                <td>
                    <uc2:TextBoxWithValidator ID="txtQUERY_PASS" runat="server" Width="98%" />
                </td>
                <td>
                    用户类型：</td>
                <td>
                    <asp:DropDownList ID="ddlTYPE" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr style="height:30px">
                <td>
                    手机：
                </td>
                <td>
                    <uc2:TextBoxWithValidator ID="txtMOBILE" runat="server" Width="98%" />
                </td>
                <td>
                    联系电话：</td>
                <td>
                    <uc2:TextBoxWithValidator ID="txtTEL" runat="server" Width="98%" />
                </td>
            </tr>
            <tr style="height:30px">
                <td>
                    是否有效：</td>
                <td>
                    <uc3:GeneralSelect ID="ddlISVALID" Name="有效" runat="server" />
                </td>
                <td>地址：
                    </td>
                <td>
                    <uc2:TextBoxWithValidator ID="txtADDRESS" runat="server" Width="98%" />
                </td>
            </tr>
        </table>
        <br />
        <asp:HiddenField ID="hfdUSERPWD" runat="server" />
        <uc1:PersistenceControl ID="PersistenceControl1" runat="server" Key="ID" Table="SYS_USER" />
        <asp:Button ID="btnSave" runat="server" Text="保存" OnClick="btnSave_Click" />
        <asp:Button ID="btnReturn" runat="server" Text="返回" OnClick="btnReturn_Click" /></div>
</asp:Content>
