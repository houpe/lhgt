﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


using SbBusiness;
using SbBusiness.Menu;

public partial class UserManage_MenuManager : System.Web.UI.Page
{
    private SystemLogs wyLog = new SystemLogs(); 
    private ClsMenuOperation clsMenu = new ClsMenuOperation();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Params["action"] == "delete")
        {
            if (Session["UserID"].ToString() == SystemConfig.AdminUser)
            {
                wyLog.Inputlog(Session["LoginUserName"].ToString(), "删除sys_menu的行", Request.Params["id"]);
                clsMenu.DeleteMenu(Request.Params["id"]);
            }
            else
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "scriptB", "<script>alert('不是超级管理员不能删除！');</script>");
            }
        }
        if (!IsPostBack)
        {
            BindData();
            ViewState["flag"] = "0";
        }
    }
    protected void CustomGridView1_OnLoadData(object sender, EventArgs e)
    {
        if (ViewState["flag"].ToString() == "0")
        {
            BindData();
        }
        else
        {
            BindData(this.txtMenu.Text);
        }
    }

    private void BindData(string str)
    {
        DataTable dtTable = clsMenu.GetMenusSorted(str);
        CustomGridView1.DataSource = dtTable;
        CustomGridView1.RecordCount = dtTable.Rows.Count;
        CustomGridView1.AllowPaging = false;
        CustomGridView1.DataBind();
    }

    private void BindData()
    {
        DataTable dtTable = clsMenu.GetMenusSorted();
        CustomGridView1.DataSource = dtTable;
        CustomGridView1.RecordCount = dtTable.Rows.Count;
        CustomGridView1.AllowPaging = false;
        CustomGridView1.DataBind();
    }

    protected void btnQuery_Clicked(object sender, EventArgs e)
    {
        if (this.txtMenu.Text != string.Empty)
        {
            BindData(this.txtMenu.Text);
            ViewState["flag"] = "1";
        }
        else
        {
            BindData();
            ViewState["flag"] = "0";
        }
    }

    protected void CustomGridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        switch (e.Row.RowType)
        {
            case DataControlRowType.DataRow:

                DataRowView drv = e.Row.DataItem as DataRowView;
                if (drv == null)
                {
                    break;
                }
                HyperLink hlEdit = e.Row.FindControl("hlEdit") as HyperLink;
                if (hlEdit != null)
                {
                    hlEdit.NavigateUrl = "MenuItemManager.aspx?id=" + drv["id"];
                }

                HyperLink hlDelete = e.Row.FindControl("hlDelete") as HyperLink;
                if (hlDelete != null)
                {
                    if (string.IsNullOrEmpty(drv["child"].ToString()) || drv["child"].ToString() == "0")
                    {
                        hlDelete.NavigateUrl = "MenuManager.aspx?action=delete&id=" + drv["id"];
                    }
                    else
                    {
                        hlDelete.Visible = false;
                    }
                }

                HyperLink hlAdd = e.Row.FindControl("hlAdd") as HyperLink;
                if (hlAdd != null)
                {
                    if (string.IsNullOrEmpty(drv["menu_url"].ToString()))
                    {
                        hlAdd.NavigateUrl = "MenuItemManager.aspx?action=add&parentId=" + drv["id"];
                    }
                    else
                    {
                        hlAdd.Visible = false;
                    }
                }
                break;
        }
    }
    protected void btnInsertRoot_Click(object sender, EventArgs e)
    {
        Response.Redirect("MenuItemManager.aspx?action=add");
    }
}
