﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UserReg.aspx.cs" Inherits="UserRegister_UserReg" %>

<%@ Register Src="../UserControls/GeneralSelect.ascx" TagName="GeneralSelect" TagPrefix="uc2" %>
<%@ Register Src="../UserControls/TextBoxWithValidator.ascx" TagName="TextBoxWithValidator"
    TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/ucPageFoot.ascx" TagName="ucPageFoot" TagPrefix="uc2" %>
<%@ Register Src="~/UserControls/ucPageHead.ascx" TagName="ucPageHead" TagPrefix="uc3" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>用户注册</title>
    <script type="text/javascript" src="../ScriptFile/Regex.js"></script>
</head>
<body>
    <script type="text/javascript">
        function onRefer() {
            var eUserId = document.getElementById("<%=txtUserId.ChildControlClientId %>");
            var eUserPwd = document.getElementById("<%=txtUserPwd.ChildControlClientId %>");
            var eUserRePwd = document.getElementById("<%=txtUserRePwd.ClientID %>");
            var eHidden = document.getElementById("<%=hidValideFlag.ClientID %>");
            var eZjhm = document.getElementById("<%=txtZjhm.ClientID %>");
            var eUnitsName = document.getElementById("<%=txtUnitsName.ClientID %>");
            var eUserName = document.getElementById("<%=txtUserName.ClientID %>");
            var eUnitsAddress = document.getElementById("<%=txtUnitsAddress.ClientID %>");

            eHidden.value = "";
            if (eUserId.value == "") {
                alert("用户名不能为空");
                eHidden.value = "0";
                return;
            }
            if (eUserName.value == "") {
                alert("姓名不能为空");
                eHidden.value = "0";
                return;
            }
            if (eUserPwd.value == "") {
                alert("密码不能为空");
                eHidden.value = "1";
                return;
            }
            if (eUserPwd.value != eUserRePwd.value) {
                alert("两次密码输入不一致密码");
                eHidden.value = "2";
                return;
            }
            if (eZjhm.value == "") {
                alert("证件号码不能为空");
                eHidden.value = "3";
                return;
            }
            var si = document.form1.UserType.selectedIndex;
            if (si == 1) {
                if (eUnitsName.value == "") {
                    alert("单位名称不能为空");
                    eHidden.value = "4";
                    return;
                }
                if (eUnitsAddress.value == "") {
                    alert("单位地址不能为空");
                    eHidden.value = "4";
                    return;
                }
            }
        }

        function SetCusIsValid(objcon) {
            var sourceid = objcon.id.replace("ddlZjlx_ddlControl", "cvZjhm");
            var objsource = document.getElementById(sourceid);
            if (objcon.value == "2" || objcon.value == "3") {
                objsource.enabled = false;
            }
            else {
                objsource.enabled = true;
            }
        }

        function UserType_onchange() {
            var si = document.form1.UserType.selectedIndex;
            if (si == 0) {
                document.all("trUnits").style.display = "none";
                document.all("tabOtherInfo").style.display = "block";
                document.getElementById("lgGrxx").innerText = "个人信息";
            }
            else {
                document.all("trUnits").style.display = "block";
                document.all("tabOtherInfo").style.display = "none";
                document.getElementById("lgGrxx").innerText = "经办人信息";
            }
        }
    </script>
    <form id="form1" name="form1" runat="server">

        <div style="text-align: center; ">
            <div id="tdTop" style="margin: 0px auto; text-align: center;" class="page_box">
                <uc3:ucPageHead ID="ucPageHead1" runat="server" />
            </div>
            <div style="margin: 0px auto;" class="page_box">


                <%-- <div class="end_bar">
                            <asp:Label ID="Label1" runat="server" Text="用户注册"></asp:Label>
                        </div>
                        <span style="width: 90%; text-align: left; margin-top: 5px; margin-bottom: 5px; display: none;">
                            <font color="red">* 办理</font><font color="red" style="font-weight: bold">国家涉密基础测绘成果资料提供使用审批事项</font>
                            <font color="red">的用户注册，<a href="http://218.244.250.76/BusinessModules/Apply/OrgRegister.aspx"
                            target="_self">点击这里>></a>通过测绘成果分发服务辅助办公系统注册，办理其他审批事项的用户请继续填写注册。 </font></span>
                        <br />--%>
                <div style="width: 90%; text-align: center;">
                    <span style="font-weight: bold">用户类型</span><select id="UserType" onchange="UserType_onchange()"
                        runat="server">
                    </select>
                    <asp:Label ID="labMsg" ForeColor="red" runat="server"></asp:Label>
                </div>
                <div style="width: 90%; text-align: center;">
                    <div id="trUnits" style="display: block">
                        <fieldset style="width: 100%; text-align: center">
                            <legend style="font-weight: bold; font-size: 12px;">单位信息</legend>
                            <table class="inputtable" style="width: 100%;" cellpadding="4" >
                                <tr>
                                    <td style="background-color: Snow; width: 100px">单位名称
                                            </td>
                                    <td style="text-align: left">
                                        <asp:TextBox ID="txtUnitsName" Width="280px" runat="server"></asp:TextBox>
                                    </td>
                                    <td style="background-color: Snow;">组织代码
                                            </td>
                                    <td style="text-align: left" colspan="3">
                                        <asp:TextBox ID="txtOrganization" Width="280px" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="background-color: Snow;">法人代表或负责人
                                            </td>
                                    <td style="text-align: left">
                                        <asp:TextBox ID="txtLegal" Width="280px" runat="server"></asp:TextBox>
                                    </td>
                                    <td style="background-color: Snow;">单位电话
                                            </td>
                                    <td style="text-align: left" colspan="3">
                                        <asp:TextBox ID="txtUnitsTel" Width="280px" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="background-color: Snow;" title="事业单位、国有企业、私营企业、其它">单位类型
                                            </td>
                                    <td style="text-align: left">
                                        <asp:DropDownList runat="server" ID="ddlNature" ToolTip="事业单位、国有企业、私营企业、其它">
                                            <asp:ListItem Value="事业单位">事业单位</asp:ListItem>
                                            <asp:ListItem Value="国有企业">国有企业</asp:ListItem>
                                            <asp:ListItem Value="私营企业">私营企业</asp:ListItem>
                                            <asp:ListItem Value="其它">其它</asp:ListItem>
                                        </asp:DropDownList>
                                        <%--<asp:TextBox ID="txtNature" Width="280px" runat="server" ToolTip="事业单位、国有企业、私营企业、其它"></asp:TextBox>--%>
                                            </td>
                                    <td style="background-color: Snow;">传&nbsp;&nbsp;&nbsp;&nbsp;真
                                            </td>
                                    <td style="text-align: left" colspan="3">
                                        <asp:TextBox ID="txtUnitsFax" Width="280px" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="background-color: Snow;">单位地址
                                            </td>
                                    <td style="text-align: left" colspan="3">
                                        <asp:TextBox ID="txtUnitsAddress" Width="100%" runat="server"></asp:TextBox>
                                    </td>
                                    <td style="background-color: Snow; width: 80px">邮编
                                            </td>
                                    <td style="text-align: left">
                                        <asp:TextBox ID="txtUnitsPostCode" Width="182" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </div>
                    <div>
                        <fieldset style="width: 100%; text-align: center">
                            <legend style="font-weight: bold; font-size: 12px;" id="lgGrxx">经办人信息</legend>
                            <table class="inputtable" style="width: 100%;" cellpadding="4">
                                <tr>
                                    <td style="background-color: Snow; width: 100px">用 户 名
                                            </td>
                                    <td style="text-align: left">
                                        <uc1:TextBoxWithValidator ID="txtUserId" Width="280" runat="server" Type="UserId"></uc1:TextBoxWithValidator>
                                    </td>
                                    <td style="background-color: Snow;">姓&nbsp;&nbsp;&nbsp;&nbsp;名
                                            </td>
                                    <td style="text-align: left">
                                        <asp:TextBox ID="txtUserName" Width="180" runat="server"></asp:TextBox>
                                    </td>
                                    <td style="background-color: Snow; width: 50px">性&nbsp;&nbsp;&nbsp;&nbsp;别
                                            </td>
                                    <td style="text-align: left">
                                        <uc2:GeneralSelect ID="ddlSex" Width="50" runat="server" ValidateEmpty="false"
                                            Name="性别" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="background-color: Snow;">登录密码
                                            </td>
                                    <td style="text-align: left">
                                        <uc1:TextBoxWithValidator ID="txtUserPwd" Width="280px" runat="server" Type="Passwords"
                                            TextMode="Password"></uc1:TextBoxWithValidator>
                                    </td>
                                    <td style="background-color: Snow;">重复密码
                                            </td>
                                    <td style="text-align: left" colspan="3">
                                        <asp:TextBox ID="txtUserRePwd" Width="280" runat="server" TextMode="Password"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="background-color: Snow;">证件类别
                                            </td>
                                    <td style="text-align: left">
                                        <uc2:GeneralSelect ID="ddlZjlx" Width="280" runat="server" ClientOnChangeScript="SetCusIsValid(this)"
                                            ValidateEmpty="true" Name="证件类型" />
                                    </td>
                                    <td style="background-color: Snow;">证件号码
                                            </td>
                                    <td style="text-align: left" colspan="3">
                                        <asp:TextBox ID="txtZjhm" Width="280" runat="server"></asp:TextBox>
                                        <asp:CustomValidator ID="cvZjhm" runat="server" ControlToValidate="txtZjhm" ErrorMessage="请输入正确的证件号码"
                                            Display="Dynamic" ClientValidationFunction="checkNumberNew" ValidateEmptyText="true"></asp:CustomValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="background-color: Snow;">联系电话
                                            </td>
                                    <td style="text-align: left">
                                        <asp:TextBox ID="txtTel" Width="280" runat="server"></asp:TextBox>
                                    </td>
                                    <td style="background-color: Snow;">传&nbsp;&nbsp;&nbsp;&nbsp;真
                                            </td>
                                    <td style="text-align: left" colspan="3">
                                        <asp:TextBox ID="txtCz" Width="280" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="background-color: Snow;">手机号码
                                            </td>
                                    <td style="text-align: left">
                                        <uc1:TextBoxWithValidator ID="txtMobile" Width="280" runat="server" Type="Phone"></uc1:TextBoxWithValidator>
                                    </td>
                                    <td style="background-color: Snow;">电子邮件
                                            </td>
                                    <td style="text-align: left" colspan="3">
                                        <asp:TextBox ID="txtEmail" Width="280" runat="server"></asp:TextBox>
                                    </td>
                                </tr>

                            </table>
                            <table id="tabOtherInfo" style="display: none; text-align:center; width: 100%;"  cellpadding="4">
                                <tr>
                                    <td style="background-color: Snow; width:100px;">所在单位
                                            </td>
                                    <td style="text-align: left">
                                        <asp:TextBox ID="txtGrssdw" Width="280"  runat="server"></asp:TextBox>
                                    </td>
                                    <td style="background-color: Snow;">联系地址
                                            </td>
                                    <td style="text-align: left">
                                        <asp:TextBox ID="txtAddress" Width="180"  runat="server"></asp:TextBox>
                                    </td>
                                    <td style="background-color: Snow; width: 50px">邮编
                                            </td>
                                    <td style="text-align: left">
                                        <uc1:TextBoxWithValidator ID="txtYZBM" runat="server" Width="50" Type="Postcode"></uc1:TextBoxWithValidator>
                                    </td>
                                </tr>
                                <tr>

                                </tr>
                            </table>
                        </fieldset>
                    </div>
                    <div>
                        <asp:Button ID="btnRefer" SkinID="LightGreen" runat="server" Text="提交" OnClick="btnRefer_Click"
                            OnClientClick="onRefer ();" />
                        <input type="button" value="关闭" class="button" onclick="javascript: window.close()" />

                    </div>
                </div>

                <asp:HiddenField ID="hidValideFlag" runat="server" />
            </div>
            <div id="divBottom" style="padding-top: 30px; margin: 0px auto; text-align: center" class="page_box">
                <uc2:ucPageFoot ID="ucPageFoot1" runat="server" />
            </div>

        </div>
    </form>
</body>
</html>
