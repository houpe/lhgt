﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SbBusiness;
using SbBusiness.User;

public partial class UserRegister_UserIDQuery : System.Web.UI.Page
{
    private static int flag; 

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindData("");
            flag = 0;
        }
    }

    private void BindData(string strUnitName)
    {
        SysUserTypeHandle userHandle = new SysUserTypeHandle();
        DataTable dtTemp = null;
        string strSysTypeCode = Session["SelectSys"].ToString();

        if (!string.IsNullOrEmpty(strUnitName))
        {
            dtTemp = userHandle.GetUserIDAndUserName(strSysTypeCode, strUnitName);
        }
        else
        {
            dtTemp = userHandle.GetUserIDAndUserName(strSysTypeCode);
        }

        this.CustomGridView1.DataSource = dtTemp;
        this.CustomGridView1.AllowPaging = true;
        this.CustomGridView1.RecordCount = dtTemp.Rows.Count;
        this.CustomGridView1.PageSize = SystemConfig.PageSize;        
        this.CustomGridView1.DataBind();
    }

    protected void btnQuery_Click(object sender, EventArgs e)
    {
        if (this.txtDWMC.Text.ToString() == string.Empty)
        {
            BindData("");
            flag = 0;
        }
        else
        {
            BindData(this.txtDWMC.Text.ToString());
            flag = 1;
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        string strJs = string.Format("<script>window.location.href='{0}';</script>",
             Session["MainPage"]);
        Response.Write(strJs); 
    }
    protected void CustomGridView1_OnLoadData(object sender, EventArgs e)
    {                    
        if (flag == 1)
            BindData(this.txtDWMC.Text.ToString());
        else
            BindData("");
    }
    protected void CustomGridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        switch (e.Row.RowType)
        {
            case DataControlRowType.Header:
                TableCellCollection headerTC = e.Row.Cells;
                TableHeaderCell gviewInputHeader = new TableHeaderCell();
                gviewInputHeader.Text = "选择";
                headerTC.Add(gviewInputHeader);
                break;
            case DataControlRowType.DataRow:
               
                TableCellCollection dataTC = e.Row.Cells;
                string UserID=string.Empty ;
                                
                DataRowView dtr = (DataRowView)e.Row.DataItem;
                if (dtr != null)
                {
                    UserID = dtr["用户帐号"].ToString();
                }
               
                TableCell gviewInputCell = new TableCell();
                HyperLink inputLink = new HyperLink();
                inputLink.Text = "选择";
                inputLink.NavigateUrl = Session["MainPage"].ToString()+"?UserID="+UserID;
                gviewInputCell.Controls.Add(inputLink);
                dataTC.Add(gviewInputCell); 
                break;
        }
    }
}
