﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SbBusiness.User;
using SbBusiness;
using SbBusiness.Wsbs;

using Business.Admin;
using Business.Common;

public partial class UserRegister_UserReg : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindUserType();
            //设置身份证类型的验证
            ddlZjlx.ValidatorName = cvZjhm.ClientID;
            Page.ClientScript.RegisterStartupScript(this.GetType(), "knh1", "<script>ddlChange('" + ddlZjlx.ddlClientID + "');ddlChange('UserType');</script>");
        }
    }

    /// <summary>
    /// 用户注册
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnRefer_Click(object sender, EventArgs e)
    {
        try
        {
                        if (hidValideFlag.Value == "0" || hidValideFlag.Value == "1"
                || hidValideFlag.Value == "2" || hidValideFlag.Value == "3" || hidValideFlag.Value == "4")

            {
                return;
            }
            else
            {
                //检测用户是否存在历史表中
                //bool UserOldExist = UserHandle.CheckUserOldExist(txtUserId.Text.Trim());

                bool userExist = SysUserHandle.CheckUserExist(txtUserId.Text, SystemConfig.WebSystemFlag);
                if (!userExist)
                {
                    string strUnitsID = string.Empty;//单位ID
                    if (UserType.SelectedIndex == 1)
                    {
                        string strUnitName = SysUserHandle.IsExistUnitsInfo(txtUnitsName.Text.Trim());

                        if (string.IsNullOrEmpty(strUnitName))
                        {
                            strUnitsID = SysUserHandle.InsertUnits(txtUnitsName.Text.Trim(), txtUnitsTel.Text.Trim(), txtUnitsAddress.Text.Trim(),
                                txtUnitsFax.Text.Trim(), txtUnitsPostCode.Text.Trim(), txtOrganization.Text.Trim(), txtLegal.Text.Trim(), ddlNature.Text);
                        }
                        else
                        {
                            labMsg.Text = "该单位名称已被注册，请联系本单位管理员！";
                            return;
                        }
                    }
                    //添加用户信息
                    int nYs = SysSettingOperation.GetSettingFromDb(SystemConfig.ZcyhysName);//获取是否预审的标志
                    SysUserHandle.CreateUser(txtUserId.Text, txtUserName.Text, txtUserPwd.Text,
                         int.Parse(SystemConfig.CszcUserType), txtTel.Text, txtMobile.Text,
                         txtAddress.Text, ddlZjlx.Value, txtZjhm.Text, txtCz.Text, txtEmail.Text,
                         txtYZBM.Text, nYs, UserType.SelectedIndex.ToString(), strUnitsID, ddlSex.Value, "0", txtGrssdw.Text);
                    //添加操作日志
                    //string strRemark = string.Format("用户注册,用户ID:" + txtUserId.Text + " 用户名称:" + txtUserName.Text);
                    //SystemLogs.AddSystemLogs(txtUserId.Text, "add", strRemark, "");

                    //SaveMessage();//注册后默认不发送通知消息，预审时发送
                    if (nYs == 1)
                    {
                        string strWrite = string.Format(@"<script>alert('申请成功，您可以登录本系统了。');window.close();</script>");
                        Page.ClientScript.RegisterStartupScript(Page.GetType(), "", strWrite);
                    }
                    else
                    {
                        string strWrite = string.Format(@"<script>alert('申请成功，请等待预审之后方可登录本系统。');window.close();</script>");
                        Page.ClientScript.RegisterStartupScript(Page.GetType(), "", strWrite);
                    }
                }
                else
                {
                    labMsg.Text = "用户名已存在，请重新输入！";
                }
            }
         }
        catch (Exception ex)
        {
            ExceptionManager.WriteAlert(this, ex.Message);
        }
    }

    /// <summary>
    /// 发送用户通知
    /// </summary>
    /// <!--addby zhongjian 20091120-->
    public void SaveMessage()
    {
        Messagebox message = new Messagebox();
        DataTable dtTemp = message.GetRequestSet(0, "用户注册");
        string strMsgContent = string.Empty;
        string strStepNo = string.Empty;
        if (dtTemp.Rows.Count > 0)
        {
            strStepNo = dtTemp.Rows[0]["step_no"].ToString();
            strMsgContent = dtTemp.Rows[0]["step_msg"].ToString();
        }
        if (!string.IsNullOrEmpty(strMsgContent))//当消息内容设置为空时，将不再发送消息。
        {
            try
            {
                //整理消息内容:strMsgContent中的{0}:代表用户名称; addby zhongjian 20091207
                strMsgContent = string.Format(strMsgContent, txtUserName.Text.Trim());
            }
            catch
            { }
            message.InsertMeaasge(strMsgContent, txtMobile.Text, txtUserName.Text, txtUserId.Text);
        }
        message.UpdateUserStepNO(txtUserId.Text, strStepNo);
    }

    /// <summary>
    /// 绑定用户类型
    /// </summary>
    /// <!--addby zhongjian 20100121-->
    protected void BindUserType()
    {
        DataTable dtTemp = DictOperation.GetSysParams("用户类型","");
        UserType.Items.Clear();
        for (int i = 0; i < dtTemp.Rows.Count; ++i)
        {
            UserType.Items.Add(new ListItem(dtTemp.Rows[i]["keyvalue"].ToString(), dtTemp.Rows[i]["keycode"].ToString()));
        }
        UserType.SelectedIndex = 1;//默认选择单位
    }
}
