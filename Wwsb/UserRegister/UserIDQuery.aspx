﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UserIDQuery.aspx.cs" Inherits="UserRegister_UserIDQuery"
    Title="用户ID查询" %>

<%@ Register Src="../UserControls/TextBoxWithValidator.ascx" TagName="TextBoxWithValidator"
    TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>用户ID查询</title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="text-align: center; azimuth: center">
        <br />
        <table border="0" cellpadding="0" cellspacing="0" class="inputtable" width="500px">
            <caption>
                单位帐号查询</caption>
            <tr>
                <td style="width: 150px">
                    <asp:Label ID="lblDWMC" runat="server" Height="25px" Text="请输入单位名:" Width="110px"></asp:Label>
                </td>
                <td>
                    <uc1:TextBoxWithValidator ID="txtDWMC" runat="server" Width="350px" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btnQuery" runat="server" SkinID="LightGreen" Text="查询" CausesValidation="false"
                        OnClick="btnQuery_Click" />
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btnCancel" runat="server" SkinID="LightGreen" Text="返回" OnClick="btnCancel_Click"
                        CausesValidation="false" />
                </td>
            </tr>
        </table>
        <br />
        <table class="inputtable" cellpadding="0" cellspacing="0" border="0" width="490px">
            <tr>
                <td>
                    <defineControls:CustomGridView ID="CustomGridView1" runat="server" SkinID="List"
                        OnOnLoadData="CustomGridView1_OnLoadData" Width="500px" ShowHideColModel="None"
                        OnRowCreated="CustomGridView1_RowCreated">
                    </defineControls:CustomGridView>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
