﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UserRegist_old.aspx.cs" Inherits="UserRegister_UserRegist" %>

<%@ Register Src="../UserControls/GeneralSelect.ascx" TagName="GeneralSelect" TagPrefix="uc2" %>
<%@ Register Src="../UserControls/TextBoxWithValidator.ascx" TagName="TextBoxWithValidator"
    TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/MsgPlate.ascx" TagName="MsgPlate" TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/ucPageFoot.ascx" TagName="ucPageFoot" TagPrefix="uc2" %>
<%@ Register Src="~/UserControls/ucPageHead.ascx" TagName="ucPageHead" TagPrefix="uc3" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>用户注册</title>
    <script language="javascript" type="text/javascript" src="../ScriptFile/Regex.js"></script>
</head>
<body>
    <script type="text/javascript">
        function onRefer() {
            var eUserId = document.getElementById("<%=txtUserId.ChildControlClientId %>");
            var eUserPwd = document.getElementById("<%=txtUserPwd.ChildControlClientId %>");
            var eUserRePwd = document.getElementById("<%=txtUserRePwd.ClientID %>");
            var eHidden = document.getElementById("<%=HiddenFieldFlag.ClientID %>");
            var eZjhm = document.getElementById("<%=txtZjhm.ClientID %>");

            eHidden.value = "";
            if (eUserId.value == "") {
                alert("用户名不能为空");
                eHidden.value = "0";
            }
            if (eUserPwd.value == "") {
                alert("密码不能为空");
                eHidden.value = "1";
            }
            if (eUserPwd.value != eUserRePwd.value) {
                alert("两次密码输入不一致密码");
                eHidden.value = "2";
            }
            if (eZjhm.value == "") {
                alert("证件号码不能为空");
                eHidden.value = "3";
            }
        }

        function SetCusIsValid(objcon) {
            var sourceid = objcon.id.replace("ddlZjlx_ddlControl", "cvZjhm");
            var objsource = document.getElementById(sourceid);
            if (objcon.value == "军官证" || objcon.value == "其它") {
                objsource.enabled = false;
            }
            else {
                objsource.enabled = true;
            }
        }
    </script>
    <form id="form1" runat="server">
    <table border="0" align="center" cellpadding="0" cellspacing="0" class="page_box">
        <tr>
            <td colspan="3">
                <uc3:ucPageHead ID="ucPageHead1" runat="server" />
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center" valign="top">
                <table border="0" align="center" cellpadding="0" cellspacing="0" width="80%">
                    <tr>
                        <td align="center">
                            <div class="end_bar">
                                <asp:Label ID="Label1" runat="server" Text="用户注册"></asp:Label></div>
                            <div style="color: Black">
                                <table class="inputtable" style="width: 65%; height: 470px;" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="width: 30%">
                                            用户名：
                                        </td>
                                        <td align="left">
                                            &nbsp;
                                            <uc1:TextBoxWithValidator ID="txtUserId" Width="30%" runat="server" Type="UserId">
                                            </uc1:TextBoxWithValidator>
                                            <font color="red">必须输入以字母开头的6-16个字母、数字或下划线</font>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            姓 名：
                                        </td>
                                        <td align="left">
                                            &nbsp;
                                            <asp:TextBox ID="txtUserName" Width="30%" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            登录密码：
                                        </td>
                                        <td align="left">
                                            &nbsp;
                                            <uc1:TextBoxWithValidator ID="txtUserPwd" Width="30%" runat="server" Type="Passwords"
                                                TextMode="Password"></uc1:TextBoxWithValidator>
                                            <font color="red">请输入6-20个字母、数字、下划线</font>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            重复密码：
                                        </td>
                                        <td align="left">
                                            &nbsp;
                                            <asp:TextBox ID="txtUserRePwd" Width="30%" runat="server" TextMode="Password"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            证件类别：
                                        </td>
                                        <td align="left">
                                            &nbsp;
                                            <uc2:GeneralSelect ID="ddlZjlx" Width="30%" runat="server" ClientOnChangeScript="SetCusIsValid(this)"
                                                ValidateEmpty="true" Name="证件类型" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            证件号码：
                                        </td>
                                        <td align="left">
                                            &nbsp;
                                            <asp:TextBox ID="txtZjhm" Width="30%" runat="server"></asp:TextBox>
                                            <asp:CustomValidator ID="cvZjhm" runat="server" ControlToValidate="txtZjhm" ErrorMessage="*"
                                                Display="Dynamic" ClientValidationFunction="checkNumberNew" ValidateEmptyText="true"></asp:CustomValidator>&nbsp;&nbsp;
                                            <font color="red">请输入正确的证件号码</font>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            联系电话：
                                        </td>
                                        <td align="left">
                                            &nbsp;
                                            <asp:TextBox ID="txtTel" Width="30%" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            传 真：
                                        </td>
                                        <td align="left">
                                            &nbsp;
                                            <asp:TextBox ID="txtCz" Width="30%" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            手机号码：
                                        </td>
                                        <td align="left">
                                            &nbsp;
                                            <uc1:TextBoxWithValidator ID="txtMobile" Width="30%" runat="server" Type="Phone">
                                            </uc1:TextBoxWithValidator>
                                            <font color="red">请输入正确的手机号码</font>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            邮 件：
                                        </td>
                                        <td align="left">
                                            &nbsp;
                                            <asp:TextBox ID="txtEmail" Width="30%" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            邮政编码：
                                        </td>
                                        <td align="left">
                                            &nbsp;
                                            <uc1:TextBoxWithValidator ID="txtYZBM" Width="30%" runat="server" Type="Postcode">
                                            </uc1:TextBoxWithValidator>
                                            <font color="red">请输入正确的邮政编码</font>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            联系地址：
                                        </td>
                                        <td align="left">
                                            &nbsp;
                                            <asp:TextBox ID="txtAddress" Width="95%" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="labMsg" ForeColor="red" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Button ID="btnRefer" SkinID="LightGreen" runat="server" Text="提交" OnClick="btnRefer_Click"
                                                OnClientClick="onRefer ();" />
                                            <input type="button" value="关闭" class="button" onclick="javascript:window.close()" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="HiddenFieldFlag" runat="server" />
               
            </td>
        </tr>
        <tr>
            <td>
                <uc2:ucPageFoot ID="ucPageFoot1" runat="server" />
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
