﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SbBusiness.User;
using SbBusiness;
using SbBusiness.Wsbs;
using Business.Common;
using Business.Admin;

public partial class UserRegister_UserRegist : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //设置身份证类型的验证
            ddlZjlx.ValidatorName = cvZjhm.ClientID;
            Page.ClientScript.RegisterStartupScript(this.GetType(), "knh1", "<script>ddlChange('" + ddlZjlx.ddlClientID + "');</script>");
        }
    }

    /// <summary>
    /// Handles the Click event of the btnRefer control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void btnRefer_Click(object sender, EventArgs e)
    {
        try
        {
            if (HiddenFieldFlag.Value == "0" || HiddenFieldFlag.Value == "1" 
                || HiddenFieldFlag.Value == "2" || HiddenFieldFlag.Value == "3")
            {
                return;
            }
            else
            {
                bool UserOldExist = SysUserHandle.CheckUserOldExist(txtUserId.Text.Trim());
                if (!UserOldExist)
                {
                    bool userExist = SysUserHandle.CheckUserExist(txtUserId.Text, SystemConfig.WebSystemFlag);
                    if (!userExist)
                    {
                        int nYs = SysSettingOperation.GetSettingFromDb(SystemConfig.ZcyhysName);//获取是否预审的标志
                        SysUserHandle.CreateUser(txtUserId.Text, txtUserName.Text, txtUserPwd.Text,
                             int.Parse(SystemConfig.CszcUserType), txtTel.Text, txtMobile.Text,
                             txtAddress.Text, ddlZjlx.Value, txtZjhm.Text, txtCz.Text, txtEmail.Text,
                             txtYZBM.Text, nYs,"0","","","","");
                        //添加操作日志 addby zhongjian 20100421
                        string strRemark = string.Format("用户注册,用户ID:" + txtUserId.Text + " 用户名称:" + txtUserName.Text + " ");
                        SystemLogs.AddSystemLogs(txtUserId.Text, "add", strRemark, "");
                        SaveMessage();//发送通知消息
                        if (nYs == 1)
                        {                            
                            string strWrite = string.Format(@"<script>alert('申请成功，您可以登录本系统了。');window.close();</script>");
                            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", strWrite);
                        }
                        else
                        {
                            SaveMessage();
                            string strWrite = string.Format(@"<script>alert('申请成功，请等待预审之后方可登录本系统。');window.close();</script>");
                            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", strWrite);
                        }
                    }
                    else
                    {
                        labMsg.Text = "用户名已存在，请重新输入！";
                    }
                }
                else
                {
                    labMsg.Text = "用户名已存在，请重新输入！";
                }
            }
         }
        catch (Exception ex)
        {
            ExceptionManager.WriteAlert(this, ex.Message);
        }
    }

    /// <summary>
    /// 发送用户通知
    /// </summary>
    /// <!--addby zhongjian 20091120-->
    public void SaveMessage()
    {
        Messagebox message = new Messagebox();
        DataTable dtTemp = message.GetRequestSet(0, "用户注册");
        string strMsgContent = string.Empty;
        string strStepNo = string.Empty;
        if (dtTemp.Rows.Count > 0)
        {
            strStepNo = dtTemp.Rows[0]["step_no"].ToString();
            strMsgContent = dtTemp.Rows[0]["step_msg"].ToString();
        }
        if (!string.IsNullOrEmpty(strMsgContent))//当消息内容设置为空时，将不再发送消息。
        {
            try
            {
                //整理消息内容:strMsgContent中的{0}:代表用户名称; addby zhongjian 20091207
                strMsgContent = string.Format(strMsgContent, txtUserName.Text.Trim());
            }
            catch
            { }
            message.InsertMeaasge(strMsgContent, txtMobile.Text, txtUserName.Text, txtUserId.Text);
        }
        message.UpdateUserStepNO(txtUserId.Text, strStepNo);
    }
}
