﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SbBusiness;
using SbBusiness.User;
using SbBusiness.Menu;
using SbBusiness.Wsbs;
using Business.FlowOperation;

public partial class MasterPage : System.Web.UI.MasterPage
{
    private string strDangQianRenWu = "当前任务";
    private string strFlag = "0";

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            if (Session["MainPage"] != null)
            {
                Response.Redirect(Session["MainPage"].ToString());
            }
            else
            {
                Response.Redirect("~/" + SystemConfig.LoginPage);
            }
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindRenWu();
            //设置显示隐藏div
            SplitControl1.DivId = "tdLeft";

            //嵌入js文件
            Page.ClientScript.RegisterClientScriptInclude(this.ID + "1", this.Page.Request.ApplicationPath + "/ScriptFile/Com.js");
            Page.ClientScript.RegisterClientScriptInclude(this.ID + "2", this.Page.Request.ApplicationPath + "/ScriptFile/cookie.js");

            //流程名称            
            if (Session["flowname"] != null)
            {
                ViewState["strFlowName"] = Session["flowname"].ToString();
            }
            else if (Request["flowname"] != null)
            {
                ViewState["strFlowName"] = Server.UrlDecode(Request["flowname"]);
            }

            string strUserID = Session["UserID"].ToString();

            //加载不同的左边树菜单。addby zhongjian 20091102 
            //if (Request["flowtype"] != null || Request["flowname"] != null || Request["datatype"] != null || Request["wid"] != null)
            //{
            //    bool bSeial = UserHandle.GetPower(strUserID, ViewState["strFlowName"].ToString());
            //    if (bSeial)//具备权限
            //    {
            //        divLeftDefine.Visible = false;
            //        //UcLeftNavigation1.Visible = true;
            //        //设置显示隐藏div
            //        //SplitControl1.DivId = UcLeftNavigation1.ClientID;
            //    }
            //    else//不具备办理权限
            //    {
            //        strFlag = "1";
            //        divLeftDefine.Visible = true;
            //        //UcLeftNavigation1.Visible = false;

            //        //设置显示隐藏div
            //        SplitControl1.DivId = divLeftDefine.ClientID;
            //    }
            //}
            //else
            //{
                divLeftDefine.Visible = true;
                SplitControl1.DivId = divLeftDefine.ClientID;
            //}
        }
    }

    #region 当前任务
    /// <summary>
    /// 绑定当前任务
    /// </summary>
    /// <!--addby zhongjian 20100111-->
    protected void BindRenWu()
    {
        if (!string.IsNullOrEmpty(Request["type"]))
        {
            string strType = Request["type"];
            switch (strType)
            {
                case "0":
                    strDangQianRenWu = "办事指南";
                    break;
                case "1":
                    strDangQianRenWu = "办理流程";
                    break;
                case "2":
                    strDangQianRenWu = "表格下载";
                    break;
                case "3":
                    strDangQianRenWu = "在线申报";
                    break;
                case "4":
                    strDangQianRenWu = "结果公示";
                    break;
                case "5":
                    strDangQianRenWu = "办理查询";
                    break;
                case "6":
                    strDangQianRenWu = "相关规定";
                    break;
                case "7":
                    strDangQianRenWu = "在线投诉";
                    break;
            }
            Session["DangQianRenWu"] = strDangQianRenWu;
        }
    }

    /// <summary>
    /// 当前任务
    /// </summary>
    /// <returns></returns>
    protected string DangQianRenWu()
    {
        //添加当前任务名称
        if (Session["DangQianRenWu"] != null)
        {
            strDangQianRenWu = Session["DangQianRenWu"].ToString();
        }

        return strDangQianRenWu;
    }
    #endregion

    #region 加载网上办事左边树菜单
    /// <summary>
    /// 加载左边树菜单
    /// </summary>
    /// <returns></returns>
    /// <!--addby zhongjian 20091102-->
    protected string GetContent()
    {
        string strHtml = string.Empty;
        if (Session["UserId"] == null)
            strHtml = GetContentByFlow();
        else
        {
            if (strFlag == "0")
            {
                string strParentName = string.IsNullOrEmpty(Request["ParentName"]) ? "导航申报" : Request["ParentName"];
                strHtml = GetLeftNavgation(strParentName);
            }
            else
            {
                strHtml = GetContentByFlow();
            }
        }
        return strHtml;
    }

    /// <summary>
    /// 加载网上办事左边树菜单
    /// </summary>
    /// <returns></returns>
    /// <!--addby zhongjian 20091102-->
    protected string GetContentByFlow()
    {
        if (ViewState["strFlowName"] == null)
        {
            return "";
        }
        string strRequestFlowName = ViewState["strFlowName"].ToString();
        string strMenuType = string.Empty;
        string strHtml = string.Format("<div class='left_box_bar' style='cursor: pointer; margin-left: 0px;' id='div1'>{0}</div><ul class='left_ul' id='ulMenu1'>", "网上办事");

        //获取菜单权限
        ClsMenuOperation menuOperation = new ClsMenuOperation();
        DataTable dtMenu = menuOperation.GetChildMenus("网上办事");

        //获取菜单权限
        WorkFlowPubSetting cWorks = new WorkFlowPubSetting();
        DataTable dtMenuRight = cWorks.GetChildMenuRight(strRequestFlowName);
        string strFlowName = Server.UrlEncode(strRequestFlowName);
        foreach (DataRow drTemp in dtMenu.Rows)
        {
            string strMenuTitle = drTemp["menu_title"].ToString();
            string strMenuUrl = drTemp["menu_url"].ToString();

            int nOrder = Convert.ToInt32(drTemp["ORDER_FIELD"].ToString());
            nOrder = nOrder - 1;

            dtMenuRight.DefaultView.RowFilter = string.Format("menu_name='{0}'", strMenuTitle);
            if (dtMenuRight.DefaultView.Count > 0)
            {
                //添加流程别名参数 addby zhongjian 20100111
                string strFlowType = Server.UrlEncode(dtMenuRight.DefaultView[0].Row["flowtype"].ToString());
                //判断是否有权限查看
                //if (dtMenuRight.DefaultView[0].Row["menu_type"].ToString() == "1")
                //{
                strMenuUrl = strMenuUrl.Replace("~", HttpContext.Current.Request.ApplicationPath);
                    //从网上办事页面跳转到相关页面 addby zhongjian 20091102
                    strHtml += string.Format("<li class=\"left_ul\" id=\"menu{0}\"><img src=\"../App_Themes/SkinFile/Images/ico-01.gif\" />&nbsp;<a href=\"{4}&flowname={2}&flowtype={3}\" onclick=\"setTab({0})\" id='a{0}'>{1}</a></li>",
                        nOrder, strMenuTitle, strFlowName, strFlowType,strMenuUrl);
                //}
            }
        }

        return strHtml;
    }

    /// <summary>
    /// 加载导航申报左边树菜单
    /// </summary>
    /// <returns></returns>
    /// <!--addby zhongjian 20091102-->
    protected string GetLeftNavgation(string strParentMenu)
    {
        ClsMenuOperation menuOperation = new ClsMenuOperation();
        DataTable dtMenu = menuOperation.GetChildMenus(strParentMenu);
        string strHtml = string.Format("<div class='left_box_bar' style='cursor: pointer; margin-left: 0px;' id='div1'>{0}</div><ul class='left_ul' id='ulMenu1'>", strParentMenu);

        int nIndex = 0;
        foreach (DataRow drTemp in dtMenu.Rows)
        {
            string strMenuTitle = drTemp["menu_title"].ToString();
            string strMenuUrl = drTemp["menu_url"].ToString();
            nIndex++;

            strMenuUrl = strMenuUrl.Replace("~",HttpContext.Current.Request.ApplicationPath);
            strHtml += string.Format("<li class=\"left_ul\" id=\"menu{0}\"><img src=\"../App_Themes/SkinFile/Images/ico-01.gif\" />&nbsp;<a href=\"{2}\"  id='a{0}'>{1}</a></li>",
                    nIndex, strMenuTitle, strMenuUrl);
        }
        strHtml += "</ul>";

        return strHtml;
    }
    #endregion
}
