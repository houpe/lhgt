﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class FileDown_FileDown : System.Web.UI.Page
{
    public string StrTable = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string downName = Session["downName"].ToString();
            StrTable = "<table class='tab' style='width:100%'><tr><td>文件下载</td><td><a href='" + downName + ".xls'>下载</a></td></tr></table>";            
        }
    }
}
