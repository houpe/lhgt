﻿//防止SQL注入
function AntiSqlValid(strValue) {
    re = /select|update|delete|exec|count|’|"|=|;|>|<|%/i;
    if (re.test(strValue)) {
        //alert("请您不要在参数中输入特殊字符和SQL关键字！"); //注意中文乱码
        return false;
    }
    return true;
}

//密码验证回调
function validate() {
    var context = "";
    var user = $('#username').val();
    var password = $('#password').val();
    var checkcode = $('#txtCheckCode').val();
    

    if (user != "" && password != "" && checkcode != "") {
        $('#btnLogin').attr("disabled", "true");
        var args = user + ',' + password + ',' + checkcode;

        $.ajax({
            type: "POST",
            url: "UserControls/AjaxInfoProcess.ashx",
            data: "params=" + args,
            success: function (msg) {
                validateResult(msg);
            },
            error: function (errMsg) {
                alert("数据库访问异常，请再次尝试登录！");
                $('#btnLogin').removeAttr("disabled");
            }
        });
    }
    else {
        alert("请填写用户名、密码、验证码");
    }
}

function EnterKeyClick() {
    if (event.keyCode == 13) {
        event.keyCode = 9;
        event.returnValue = false;
        var btnLogin = document.getElementById('btnLogin');
        btnLogin.click();
    }
}

//验证结果
function validateResult(result) {
    if (result != "") {
        if (result == "校验码为空") {
            alert("校验码错误");
            clickYzm();
        }
        else {
            alert(result);
        }
        $("#btnLogin").removeAttr("disabled");
    }
    else {
        var user = $('#username').val();
        //var password = $('#password').val();

        setCookie('login_user', user);

        //flag=2控制菜单是否可用，以及工具条的展示个数
        var strMainUrl = GetConfig("mainPage");
        if (strMainUrl == "") {
            strMainUrl = "MainPageNew.htm";
        }

        window.location = strMainUrl;
    }
}