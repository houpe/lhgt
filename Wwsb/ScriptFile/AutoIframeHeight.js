﻿//自适应框架的高度
function SetCwinHeight(cwin) {
    if (document.getElementById) {
        if (cwin && !window.opera) {
            if (cwin.document && cwin.document.body.scrollHeight) {
                cwin.height = cwin.document.body.scrollHeight;
                //cwin.width = cwin.document.body.scrollWidth;
            }
            else if (cwin.contentDocument && cwin.contentDocument.body.offsetHeight) {
                cwin.height = cwin.contentDocument.body.offsetHeight;
            }
        }
    }
}

//为iframe自适应高度 addby zhongjian 20100112
//onload="this.height=frmChild.document.body.scrollHeight";//本语名适合在同一站点的情况
function SetWinHeight(obj) {
    var win = obj;
    if (win && !window.opera) {
        if (win.contentDocument && win.contentDocument.body.offsetHeight) {
            win.height = win.contentDocument.body.offsetHeight;
        }
        if (win.Document && win.Document.body.scrollHeight) {
            if (win.height < win.Document.body.scrollHeight)
                win.height = win.Document.body.scrollHeight;
        }
        if (win.height < 1150)//设置最小值
        {
            win.height = "1150";
        }
    }
}
   