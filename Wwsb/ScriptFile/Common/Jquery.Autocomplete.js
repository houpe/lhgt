﻿function jsAuto(instanceName, objID, top, left) {
    this._msg = [];
    this._x = null;
    this._o = document.getElementById(objID);
    if (!this._o) return;
    this._f = null;
    this._i = instanceName;
    this._r = null;
    this._c = 0;
    this._s = false;
    this._v = null;
    this._o.style.visibility = "hidden";
    this._o.style.position = "absolute";
    this._o.style.zIndex = "9999";
    this._o.style.overflow = "auto";
    if (top != undefined) {
        this._o.style.top = top+2;
    }
    if (left != undefined) {
        this._o.style.left = left;
    }
    this._o.style.height = "100";
    return this;
};

jsAuto.prototype.directionKey = function() {
    with (this) {
        var e = _e.keyCode ? _e.keyCode : _e.which;
        var l = _o.childNodes.length;
        (_c > l - 1 || _c < 0) ? _s = false : "";

        if (e == 40 && _s) {
            _o.childNodes[_c].className = "mouseout";
            (_c >= l - 1) ? _c = 0 : _c++;
            _o.childNodes[_c].className = "mouseover";
        }
        if (e == 38 && _s) {
            _o.childNodes[_c].className = "mouseout";
            _c-- <= 0 ? _c = _o.childNodes.length - 1 : "";
            _o.childNodes[_c].className = "mouseover";
        }
        if (e == 13) {
            if (_o.childNodes[_c] && _o.style.visibility == "visible") {
                _r.value = _x[_c];
                _o.style.visibility = "hidden";
            }
        }
        if (!_s) {
            _c = 0;
            _o.childNodes[_c].className = "mouseover";
            _s = true;
        }
    }
};

// mouseEvent.
jsAuto.prototype.domouseover = function(obj) {
    with (this) {
        _o.childNodes[_c].className = "mouseout";
        _c = 0;
        obj.tagName == "DIV" ? obj.className = "mouseover" : obj.parentElement.className = "mouseover";
    }
};
jsAuto.prototype.domouseout = function(obj) {
    obj.tagName == "DIV" ? obj.className = "mouseout" : obj.parentElement.className = "mouseout";
};
jsAuto.prototype.doclick = function(msg) {
    with (this) {
        if (_r) {
            _r.value = msg;
            _o.style.visibility = "hidden";
        }
        else {
            alert("jquery.autocomplete.js ERROR :\n\n can not get return object.");
            return;
        }
    }
};


// object method;
jsAuto.prototype.item = function(msg) {
    if (msg.indexOf(",") > 0) {
        var arrMsg = msg.split(",");
        for (var i = 0; i < arrMsg.length; i++) {
            arrMsg[i] ? this._msg.push(arrMsg[i]) : "";
        }
    }
    else {
        this._msg.push(msg);
    }
    this._msg.sort();
};
jsAuto.prototype.append = function(msg, fValue) {
    with (this) {
        _i ? "" : _i = eval(_i);
        _x.push(msg);
        var div = document.createElement("DIV");
        //bind event to object.
        div.onmouseover = function() { _i.domouseover(this) };
        div.onmouseout = function() { _i.domouseout(this) };
        div.onclick = function() { _i.doclick(msg) };
        div.style.lineHeight = "140%";
        div.className = "mouseout";
        var newmsg = msg;
        var fValues = fValue.split(" ");
        for (var j = 0; j < fValues.length; j++) {
            var res = new RegExp("(" + fValues[j] + ")", "i");
            if (res) {
                newmsg = newmsg.replace(res, "<strong style='color:red'>$1</strong>");
            }
        }

        div.innerHTML = newmsg;
        div.style.fontFamily = "verdana";
        _o.appendChild(div);
    }
};
jsAuto.prototype.display = function() {
    with (this) {
        if (_f) {
            var _r_offset = $(_r).offset();
            _o.style.left = _r_offset.left;
            //_o.style.width = $(_r).width();//宽度自适应
            //min-width: 4em;
            $(_o).css("min-width", $(_r).width())
            _o.style.top = _r_offset.top + $(_r).height()+5;
            _o.style.visibility = "visible";
        }
        else {
            _o.style.visibility = "hidden";
        }
    }
};
jsAuto.prototype.handleEvent = function(fValue, fID, event) {
    with (this) {
        var re;
        _e = event;
        var e = _e.keyCode ? _e.keyCode : _e.which;
        _x = [];
        _f = false;
        _r = document.getElementById(fID);
        _v = fValue;
        _i = eval(_i);
        _o.innerHTML = "";
        for (var i = 0; i < _msg.length; i++) {
            _i.append(_msg[i], fValue);
            _f = true;
        }

        _i ? _i.display() : alert("can not get instance");

        if (_f) {
            if ((e == 38 || e == 40 || e == 13)) {
                _i.directionKey();
            }
            else {
                _c = 0;
                _o.childNodes[_c].className = "mouseover";
                _s = true;
            }
        }
    }
};
window.onerror = new Function("return true;");

var ja = new jsAuto("ja", "divc");
var oldid = "";
var myvalue = "";
function AutoItem(el, event) {
    $("#divc").show();
    var id = $(el).attr("id");
    if (!id || id == "") {
        var now = new Date();
        id = "i_" + now.getSeconds();        
        $(el).attr("id", id);
    }
    var val = $(el).val();
    var autoFlag = $(el).attr("AutoFlag");
    var flag = val == "" ? "1" : "0";
    flag = autoFlag == undefined ? flag : autoFlag;
   
    var dataval = "";

    var datafield = $(el).attr("datafield");
    var datatable = $(el).attr("datatable");
    var datashowfield = $(el).attr("datashowfield");
    var valid = $(el).attr("valueid");
    var datawhere = $(el).attr("datawhere");
    if (valid != undefined) {
        dataval = $("#" + valid).val();
    }
    else {
        dataval = val;
    }
    var where = "";
    if (datawhere) {
        where = datawhere;
    }
    if (dataval != "") {
        where += " and " + datafield + " like '%" + dataval.replaceAll(" ", "%") + "%'";
    }
    if (!datashowfield || datashowfield == undefined) {
        datashowfield = datafield;
    }


    if (oldid != id || dataval != myvalue) {
        
        var json = GetAutoItemData(datatable, datashowfield, where, flag);
        ja = new jsAuto("ja", "divc", 150);
        for (var i = 0; i < json.length; i++) {
            ja.item(json[i].name);
            if (json[i].length > 1) {
                ja.vals(json[i][1]);
            }
        }
        $("#divc").css("width", $(el).css("width"));
        if ($("#divc").css("pixelHeight") < 150) {
            $("#divc").css("height", 150);
        }
        
        oldid = id;
        myvalue = dataval;
    }
    ja.handleEvent(val, id, event);
}



function GetAutoItemData(table, field, where, flag) {    
    var param = table + "," + field + "," + where + "," + flag ;
    var json = GetServerData("Office", "Office.WorkHandle", "Autocomplete", param, "", _url).toJson();
    return json;
}

function dclose() {
    $("#divc").hide();
}

function AutoItemBind() {   
    $("<div id='divc' class='divc'></div>").appendTo($("body"));
    $("[auto='1'][datatable][datafield]").bind("dblclick", function() {
        AutoItem(this, event);
    }).bind("keyup", function() {
        AutoItem(this, event);
    });
    $("body").bind("click", dclose);
}