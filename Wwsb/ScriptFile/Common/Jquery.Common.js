﻿var Common = Common || {};
$.extend(Common, {
    //浏览器时下窗口可视区域高度 
    HeightWindow: $(window).height(),
    //浏览器时下窗口可视区域宽度 
    WidthWindow: $(window).width(),

    //浏览器判断
//    IsIE: $.support   != undefined,
//    IsIE6: $.support   && parseInt($.browser.version) === 6,
//    IsIE7: $.support   && parseInt($.browser.version) === 7,
//    IsIE8: $.support   && parseInt($.browser.version) === 8,
//    IsIE9: $.support   && parseInt($.browser.version) === 9,
//    IsIE10: $.support   && parseInt($.browser.version) === 10,
    //生成Guid
    Guid: function() { var guid = ""; for (var i = 1; i <= 32; i++) { var n = Math.floor(Math.random() * 16.0).toString(16); if (i == 8 || i == 12 || i == 16 || i == 20) { guid += "-"; } guid += n; } return guid; },
    //Cookie操作
    GetCookie: function(name) { var r = new RegExp('(^|;|\\s+)' + name + '=([^;]*)(;|$)'); var m = document.cookie.match(r); return (!m ? '' : Common.Decode(m[2])); },
    SetCookie: function(name, value, expire, domain, path) { var s = name + '=' + Common.Encode(value); if (!Common.IsUndefined(path)) s = s + '; path=' + path; if (expire > 0) { var d = new Date(); d.setTime(d.getTime() + expire * 1000); if (!Common.IsUndefined(domain)) s = s + '; domain=' + domain; s = s + '; expires=' + d.toGMTString(); } document.cookie = s; },
    RemoveCookie: function(name, domain, path) { var s = name + '='; if (!Common.IsUndefined(domain)) s = s + '; domain=' + domain; if (!Common.IsUndefined(path)) s = s + '; path=' + path; s = s + '; expires=Fri, 02-Jan-1970 00:00:00 GMT'; document.cookie = s; },

    //Ztree操作
    //
    ZTreeCheckClear: function(id, field) { var zTree = $.fn.zTree.getZTreeObj(id); if (zTree) { var nodes = zTree.getCheckedNodes(true); for (var i = 0; i < nodes.length; i++) { var node = zTree.getNodeByParam('id', nodes[i][field]); zTree.checkNode(node, false, true, true); } } },
    ZTreechecked: function(id, field, folder) { var zTree = $.fn.zTree.getZTreeObj(id); var values = []; if (zTree) { var checkedNodes = zTree.getCheckedNodes(true); if (!field || field == "") { field = "id"; } for (var i = 0; i < checkedNodes.length; i++) { if (folder) { values[values.length] = checkedNodes[i][field]; } else if (!checkedNodes[i].children) { values[values.length] = checkedNodes[i][field]; } } } return values },
    //Obj操作
    Clear: function(obj) { $("#" + obj).val("") },
    IsUndefined: function(obj) { return typeof obj == 'undefined'; },
    IsObject: function(obj) { return typeof obj == 'object'; },
    IsNumber: function(obj) { return typeof obj == 'number'; },
    IsString: function(obj) { return typeof obj == 'string'; },
    IsElement: function(obj) { return obj && obj.nodeType == 1; },
    IsFunction: function(obj) { return typeof obj == 'function'; },
    Upper: function(obj) { return obj.toLocaleUpperCase(); },
    Lower: function(obj) { return obj.toLocaleLowerCase(); },
    
    //判断是否对应数据类型
    IsInt: function(str) { return /^-?\d+$/.test(str); },
    IsFloat: function(str) { return /^(-?\d+)(\.\d+)?$/.test(str); },
    IsIntPositive: function(str) { return /^[0-9]*[1-9][0-9]*$/.test(str); },
    IsFloatPositive: function(str) { return /^(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*))$/.test(str); },
    //是否字母
    IsLetter: function(str) { return /^[A-Za-z]+$/.test(str); },
    //是否中文
    IsChinese: function(str) { return /^[\u0391-\uFFE5]+$/.test(str); },
    //是否邮编
    IsZipCode: function(str) { return /^[1-9]\d{5}$/.test(str); },
    //是否Email
    IsEmail: function(str) { return /^[A-Z_a-z0-9-\.]+@([A-Z_a-z0-9-]+\.)+[a-z0-9A-Z]{2,4}$/.test(str); },
    //是否电话
    IsMobile: function(str) { return /^((\(\d{2,3}\))|(\d{3}\-))?((1[35]\d{9})|(18[89]\d{8}))$/.test(str); },
    //判断是否有效的URL
    IsUrl: function(str) { return /^(http:|ftp:)\/\/[A-Za-z0-9]+\.[A-Za-z0-9]+[\/=\?%\-&_~`@[\]\':+!]*([^<>\"])*$/.test(str); },
    //判断是否IP地址
    IsIpAddress: function(str) { return /^(0|[1-9]\d?|[0-1]\d{2}|2[0-4]\d|25[0-5]).(0|[1-9]\d?|[0-1]\d{2}|2[0-4]\d|25[0-5]).(0|[1-9]\d?|[0-1]\d{2}|2[0-4]\d|25[0-5]).(0|[1-9]\d?|[0-1]\d{2}|2[0-4]\d|25[0-5])$/.test(str); },

    //URL转码
    Encode: function(str) { return encodeURIComponent(str); },
    //URL解码
    Decode: function(str) { return decodeURIComponent(str); },
    //加密算法  MD5加密算法不可逆加密算法
    Encry: function(str, tp) { var vstr = BussinessTools("Encrypt", "EncrypStr", str + "," + tp); return vstr; },
    //解密算法  MD5加密算法 不能解密
    Decryp: function(str, tp) { var vstr = BussinessTools("Encrypt", "DecrypStr", str + "," + tp); return vstr; },
    
    //获取网站路径不要虚拟目录路径
    GetLocalPath: function() { var strFullPath = window.document.location.href; var strPath = window.document.location.pathname; var pos = strFullPath.indexOf(strPath); var prePath = strFullPath.substring(0, pos); return prePath; },
    //获取网站路径以及虚拟目录
    GetRootPath: function() { var strFullPath = window.document.location.href; var strPath = window.document.location.pathname; var pos = strFullPath.indexOf(strPath); var prePath = strFullPath.substring(0, pos); var postPath = strPath.substring(0, strPath.substr(1).indexOf('/') + 2); return (prePath + postPath); },
    //获取页面完整路径
    GetPagePath: function() { var root = getRootPath(); var strFullPath = window.document.location.href; var pagePath = strFullPath.replace(root, ""); return pagePath; },
    //获取页面完整路径 去掉参数
    GetPagePathNoPram: function () { var root = getRootPath(); var strFullPath = window.document.location.href; var pagePath = strFullPath.replace(root, ""); var pages = pagePath.split('?'); return pages[0]; },
    //获取服务路径
    GetServicePath: function () {  return "http://localhost/MOAWebService/"; },
    //json转换为字符串
    Json2String: function(obj) { var isArray = obj instanceof Array; var r = []; for (var i in obj) { var value = obj[i]; if (typeof value == 'string') { value = '"' + value + '"'; } else if (value != null && typeof value == 'object') { value = '"' +value + '"'; } r.push((isArray ? '' : i + ':') + value); } if (isArray) { return '[' + r.join(',') + ']'; } else { return '{' + r.join(',') + '}'; } },
    //json转换为OBject
    Json2Object: function(obj) { var isArray = obj instanceof Array; var r = []; for (var i in obj) { var value = obj[i]; if (typeof value == 'object') { value = '"' + value + '"'; } else if (value != null && typeof value == 'object') { value = '"' + value + '"'; } r.push((isArray ? '' : i + ':') + value); } if (isArray) { return '[' + r.join(',') + ']'; } else { return '{' + r.join(',') + '}'; } },
    JsonAdd: function(jsonname, element, attribute) { var jsonString = Common.Json2String(jsonname); jsonString = jsonString.substring(0, jsonString.length - 1); var jsonArr = "," + element + ":" + attribute + "}"; jsonString = jsonString.concat(jsonArr); return jsonString.toJson(); },

    //组装控件属性
    ComAttr: function(attr, val) { if (val && val != "") { return " " + attr + "=" + "'" + val + "'"; } else { return ""; } },

    //JS获取后缀名
    Extend: function(val) { var d = /\.[^\.]+$/.exec(val); return d; },

    //JsonSelect Json 查询方法
    query: function(val, exp) { if (Common.left(exp, 1) != "@") { exp = "Common." + exp }; if (!exp) { return []; } var fn = Common.cache[exp]; try { if (!fn) { var code = Common.interpret(exp); code = Common.templ.replace("$C", code); fn = Common.cache[exp] = Common.complite(code); } return fn(val); } catch (e) { return []; } },

    //缓存,解决快速查找
    cache: {},
    rQuote: /""/g,
    rQuoteTemp: /!~/g,
    //扩展运算符
    alias: [/@/g, "_e.", /AND/gi, "&&", /OR/gi, "||", /<>/g, "!=", /NOT/gi, "!", /([^=<>])=([^=]|$)/g, '$1==$2'],
    // 定义常用的函数
    len: function(s) { return s.length; },
    left: function(s, n) { return s.substr(0, n); },
    right: function(s, n) { return s.substr(-n); },
    index: function(s, find) { return s.indexOf(find) + 1; },
    // 定义模函数
    templ: function(_list) { var _ret = []; var _i = -1; for (var _k in _list) { var _e = _list[_k]; if (_e != Object.prototype[_k]) { if ($C) { _ret[++_i] = _e; } } } return _ret; } .toString(),
    // 编译
    complite: function(code) { return eval("0," + code); },
    // 将扩展符号转换成标准的JS符号
    interpret: function(exp) { exp = exp.replace(Common.rQuote, "!~"); var arr = exp.split('"'); var i, n = arr.length; var k = Common.alias.length; for (var i = 0; i < n; i += 2) { var s = arr[i]; for (var j = 0; j < k; j += 2) { if (Common.index(s, Common.alias[j]) > -1) { s = s.replace(Common.alias[j], Common.alias[j + 1]); } } arr[i] = s; } for (var i = 1; i < n; i += 2) { arr[i] = arr[i].replace(Common.rQuoteTemp, '\\"'); } return arr.join('"'); },

    //引用js、css
    include: function(file) { var files = typeof file == "string" ? [file] : file; for (var i = 0; i < files.length; i++) { var name = files[i].replace(/^\s|\s$/g, ""); var att = name.split('.'); var ext = att[att.length - 1].toLowerCase(); var isCSS = ext == "css"; var tag = isCSS ? "link" : "script"; var attr = isCSS ? " type='text/css' rel='stylesheet' " : " language='javascript' type='text/javascript' "; var link = (isCSS ? "href" : "src") + "='" + Common.GetRootPath() + name + "'"; if ($(tag + "[" + link + "]").length == 0) { $("head").append("<" + tag + attr + link + "></" + tag + ">"); } } }

});

 
String.prototype.replaceAll = function(str1, str2) {
    var regS = new RegExp(str1, "gi");
    return this.replace(regS, str2);
}

//将string转换成json
String.prototype.toJson = function() {
    var _val = this;
    if (_val && _val != "") {
        eval("var theJsonValue = " + _val.replace(/\r\n/g, ""));
        return theJsonValue;
    }
}

//Json查询例子
/*

var doTest1 = function() {

var heros = [
{ name: '冰室女巫', DP: 38, AP: 1.3, Str: 16, Dex: 16, Int: 21 },
{ name: '沉默术士', DP: 39, AP: 1.1, Str: 17, Dex: 16, Int: 21 },
{ name: '娜迦海妖', DP: 51, AP: 6.0, Str: 21, Dex: 21, Int: 18 },
{ name: '赏金猎人', DP: 39, AP: 4.0, Str: 17, Dex: 21, Int: 16 },
{ name: '剧毒术士', DP: 45, AP: 3.1, Str: 18, Dex: 22, Int: 15 },
{ name: '光之守卫', DP: 38, AP: 1.1, Str: 16, Dex: 15, Int: 22 },
{ name: '炼金术士', DP: 49, AP: 0.6, Str: 25, Dex: 11, Int: 25 }

];

var match = Common.query(heros, '@Str=21 AND @Dex=21');
ShowResult(match);


// 查询：“士”结尾的
// 结果：沉默术士,剧毒术士,炼金术士
var match = Common.query(heros, 'JsonSel.right(@name,1)="士"');
ShowResult(match);
}

function ShowResult(result) {
for (var i = 0; i < result.length; i++) {
alert(result[i].name + " " + result[i].DP + " " + result[i].AP + " " + result[i].Str + " " + result[i].Dex + " " + result[i].Int);
}
}


*/