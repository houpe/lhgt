﻿//加载并变换div
function ChangeDiv() {
    //存储cookie里边的代码
    var arrCookieLeft = new Array();
    var arrCookieRight = new Array();
    var arrCookieMid = new Array();

    if (getCookie('ck_dragdiv1') != "") {
        arrCookieLeft = getCookie('ck_dragdiv1').split(",");
    }

    if (getCookie('ck_dragdiv2') != "") {
        arrCookieRight = getCookie('ck_dragdiv2').split(",");
    }

    if (getCookie('ck_dragdiv3') != "") {
        arrCookieMid = getCookie('ck_dragdiv3').split(",");
    }

    //存储初始加载的html
    var arrHtmlLeft = new Array();
    var arrHtmlRight = new Array();
    var arrHtmlMid = new Array();
    //alert(arrCookieLeft.length);
    for (var i = 0; i < arrCookieLeft.length; i++) {
        var divId = arrCookieLeft[i];
        //alert(divId);
        if (divId == "") return;
        arrHtmlLeft[i] = "<div id='" + divId + "'>" + $("#" + divId).html() + "</div>";

    }
    for (var i = 0; i < arrCookieRight.length; i++) {
        var divId = arrCookieRight[i];
        if (divId == "") return;
        arrHtmlRight[i] = "<div id='" + divId + "'>" + $("#" + divId).html() + "</div>";

    }
    for (var i = 0; i < arrCookieMid.length; i++) {
        var divId = arrCookieMid[i];
        if (divId == "") return;
        arrHtmlMid[i] = "<div id='" + divId + "'>" + $("#" + divId).html() + "</div>";

    }

    if (arrCookieLeft.length > 0) {
        $("#dragdiv1").html("");
    }

    if (arrHtmlRight.length > 0) {
        $("#dragdiv2").html("");
    }
    if (arrHtmlMid.length > 0) {
        $("#dragdiv3").html("");
    }

    for (var i = 0; i < arrHtmlLeft.length; i++) {
        $("#dragdiv1").append(arrHtmlLeft[i]);
    }
    for (var i = 0; i < arrHtmlRight.length; i++) {
        $("#dragdiv2").append(arrHtmlRight[i]);
    }
    for (var i = 0; i < arrHtmlMid.length; i++) {
        $("#dragdiv3").append(arrHtmlMid[i]);
    }
}

//保存模块排序并写入Cookie (^_^我这里只有COOKIE保存.当然你可以保存在数据库里面)
function saveLayout() {
    setCookie('ck_dragdiv1', getVales('dragdiv1'));
    setCookie('ck_dragdiv2', getVales('dragdiv2'));
    setCookie('ck_dragdiv3', getVales('dragdiv3'));
}

//每一列模块的id值,其实sortable这个函数里有一个serialize可以直接取到对应的序列值:格式如下:
// $('#left').sortable('serialize',{key:'leftmod[]'}) + '&' + $('#right').sortable('serialize',{key:'rightmod[]'})
function getVales(divParentId) {
    var arrString = new Array();
    $("#" + divParentId + ">div[id^='div_']").each(function(i) {
        arrString[i] = this.id;
    });
    return arrString;
}