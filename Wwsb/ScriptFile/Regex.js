﻿/************ the follow code for Regex added by linjian **************/
/************ 主要用于结合验证控件CustomValidator **************/

      //触发下拉框的onchang事件
      function ddlChange(id)
      {
        var ctrl=document.getElementById(id);
        if(ctrl!=null)
        {
            ctrl.fireEvent('onchange');
        }
      }

     //校验身份证号
     function checkNumberNew(source, arguments)
     {
        if(source.title=='身份证'|| source.title=='')
        {
            var len = arguments.Value.length;
            if( (len==15 && arguments.Value!='111111111111111' ) || len==18)
            {                   
                   //格式/^。。。$/
                   //15位的全数字,18位的最后一位可能是x
                   var pattern = /^\d{18}|\d{15}|\d{17}[Xx]$/;
                   if (pattern.test(arguments.Value))
                   {
                        var re;
                        if   (len   ==   15)   
                        {
                            re   =   new   RegExp(/^(\d{6})()?(\d{2})(\d{2})(\d{2})(\d{3})$/);  
                        } 
                        else if   (len   ==   18)   
                        {
                            re   =   new   RegExp(/^(\d{6})()?(\d{4})(\d{2})(\d{2})(\d{3})(\d|[Xx])$/); 
                        }  
                        
                        //核查日期
                        var a   =  arguments.Value.match(re);   
      
                        if(a  !=   null)   
                        {   
                            if(len==15)   
                            {   
                                if(a[3]<100 && a[4]<13 && a[5]<32)
                                {
                                    arguments.IsValid= true;
                                }
                                else
                                {
                                    arguments.IsValid= false;
                                }
                            }   
                            else   
                            {   
                                if(a[3]>1900 && a[4]<13 && a[5]<32)
                                {
                                    arguments.IsValid= true;
                                }
                                else
                                {
                                    arguments.IsValid= false;
                                }
                            }   
                        }
                        else
                        {
                            arguments.IsValid= false;
                        }
                   }
                   else
                   {
                        arguments.IsValid= false;
                   }
             }
             else
             {
                   arguments.IsValid= false;
             }     
        }
        else
        {
              arguments.IsValid= true;
        }    
     }
        //校验身份证号
     function checkNumberNew2(source, arguments)
     {
        if(source.title=='身份证')
        {
            var len = arguments.Value.length;
            if( (len==15 && arguments.Value!='111111111111111' ) || len==18)
            {                   
                   //格式/^。。。$/
                   //15位的全数字,18位的最后一位可能是x
                   var pattern = /^\d{18}|\d{15}|\d{17}[Xx]$/;
                   if (pattern.test(arguments.Value))
                   {
                        var re;
                        if   (len   ==   15)   
                        {
                            re   =   new   RegExp(/^(\d{6})()?(\d{2})(\d{2})(\d{2})(\d{3})$/);  
                        } 
                        else if   (len   ==   18)   
                        {
                            re   =   new   RegExp(/^(\d{6})()?(\d{4})(\d{2})(\d{2})(\d{3})(\d|[Xx])$/); 
                        }  
                        
                        //核查日期
                        var a   =  arguments.Value.match(re);   
      
                        if(a  !=   null)   
                        {   
                            if(len==15)   
                            {   
                                if(a[3]<100 && a[4]<13 && a[5]<32)
                                {
                                    arguments.IsValid= true;
                                }
                                else
                                {
                                    arguments.IsValid= false;
                                }
                            }   
                            else   
                            {   
                                if(a[3]>1900 && a[4]<13 && a[5]<32)
                                {
                                    arguments.IsValid= true;
                                }
                                else
                                {
                                    arguments.IsValid= false;
                                }
                            }   
                        }
                        else
                        {
                            arguments.IsValid= false;
                        }
                   }
                   else
                   {
                        arguments.IsValid= false;
                   }
             }
             else
             {
                   arguments.IsValid= false;
             }     
        }
        else
        {
              arguments.IsValid= true;
        }    
     }
    //校验中文字符
    function checkChinese(source, arguments)
    {
        if( arguments.Value.length>0)
        {
              var pattern = /^[u4e00-u9fa5]/;
              if (!pattern.test(arguments.Value))
              {
                   arguments.IsValid= true;
              }
              else
              {
                   arguments.IsValid= false;
              }
         }
         else
         {
              arguments.IsValid= false;
         }
     }
     
     //检验是否为空
     function checkIsEmpty(source,arguments)
     {
        if(source.title=='donotValid')
        {
            arguments.IsValid= true;
            return;
        }
        if(arguments.Value.length>0)
        {
              var pattern = /(^[\s]*)|([\s]*$)/g;
              if (!pattern.test(arguments.Value))
              {
                   arguments.IsValid= true;
              }
              else
              {
                   arguments.IsValid= false;
              }
         }
         else
         {
              arguments.IsValid= false;
         }
     }
     
     
     //在数值不为空的前提下，校验双精度
     function checkDoubleWithPromiseEmpty(source, arguments)
     {
         if( arguments.Value.length>0)
         {
             var pattern = /^[-\+]?\d+(\.\d+)?$/;
             if (pattern.test(arguments.Value))
             {
                 arguments.IsValid= true;
             }
             else
             {
                 arguments.IsValid= false;
             }
         }
     }
     
     //验证日期
     function isDate(source, arguments)
     { 
        var reg =/^(\d{0,4})-(\d{0,2})-(\d{0,2}) (\d{0,2}):(\d{0,2}):(\d{0,2})$/;
        
        if (reg.test(arguments.Value)) 
        {
            arguments.IsValid= true;
        }
        else
        {
            arguments.IsValid= false;
        }
      }
     
     //校验双精度
     function checkDouble(source, arguments)
     {
           var pattern = /^[-\+]?\d+(\.\d+)?$/;
           if (pattern.test(arguments.Value))
           {
                arguments.IsValid= true;
           }
           else
           { 
                arguments.IsValid= false;
           }
     }
     
     //校验整数
     function checkInteger(source, arguments)
     {
         if( arguments.Value.length>0)
         {
             var pattern = /^[-+]?\d*$/;
             if (pattern.test(arguments.Value))
             {
                 arguments.IsValid= true;
             }
             else
             {
                 arguments.IsValid= false;
             }
         }
         else
         {
              arguments.IsValid= false;
         }
     }
     
     //校验电话号码
     function checkPhoneNumber(source, arguments)
     {
           var pattern = /^(\(\d{3,4}\)|\d{3,4}-)?\d{7,8}$/;
           if (pattern.test(arguments.Value))
           {
                arguments.IsValid= true;
           }
           else
           {
                //检验是否是手机号码                
                pattern = /^\d{11}$/;
                if (pattern.test(arguments.Value))
                {
                    arguments.IsValid= true;
                }
                else
                {
                    arguments.IsValid= false;
                }
           }
     }
     
     //校验身份证号
     function checkNumber(source, arguments)
     {
          var len = arguments.Value.length;
       
          if( (len==15 && arguments.Value!='111111111111111' ) || len==18)
          {
               //格式/^。。。$/
               //15位的全数字,18位的最后一位可能是x
               var pattern = /^\d{18}|\d{15}|\d{17}[Xx]$/;
               if (pattern.test(arguments.Value))
               {
                    var re;
                    if   (len   ==   15)   
                    {
                        re   =   new   RegExp(/^(\d{6})()?(\d{2})(\d{2})(\d{2})(\d{3})$/);  
                    } 
                    else   if   (len   ==   18)   
                    {
                        re   =   new   RegExp(/^(\d{6})()?(\d{4})(\d{2})(\d{2})(\d{3})(\d|[Xx])$/); 
                    }  
                    
                    //核查日期
                    var a   =  arguments.Value.match(re);   
  
                    if(a  !=   null)   
                    {   
                        if(len==15)   
                        {   
                            if(a[3]>0&&a[3]<100 && a[4]>0&&a[4]<13 && a[5]>0&&a[5]<32)
                            {
                                arguments.IsValid= true;
                            }
                            else
                            {
                                arguments.IsValid= false;
                            }
                        }   
                        else   
                        {   
                            if(a[3]>1900 && a[4]>0&&a[4]<13 && a[5]>0&&a[5]<32)
                            {
                                arguments.IsValid= true;
                            }
                            else
                            {
                                arguments.IsValid= false;
                            }
                        }   
                    }
                    else
                    {
                        arguments.IsValid= false;
                    }
               }
               else
               {
                    arguments.IsValid= false;
               }
          }
          else
          {
               arguments.IsValid= false;
          }    
     }
     
     //校验邮编
     function checkPostcode(source, arguments)
     {
        var pattern = /^\d{6}$/;
        if(pattern.test(arguments.Value))
        {
            arguments.IsValid = true;
        }
        else
        {
            arguments.IsValid = false;
        }
     }
     
     //校验密码，只能输入6-20个字母、数字、下划线
     function checkPasswords(source, arguments)
     {
        var pattern = /^(\w){6,20}$/;
        if(pattern.test(arguments.Value))
        {
            arguments.IsValid = true;
        }
        else
        {
            arguments.IsValid = false;
        }
     }

     //校验用户名，字母开头，允许6-16字节，允许字母数字下划线
     function checkUserId(source, arguments)
     {
        var pattern = /^[a-zA-Z][a-zA-Z0-9_]{5,15}$/;
        if(pattern.test(arguments.Value))
        {
            arguments.IsValid = true;
        }
        else
        {
            arguments.IsValid = false;
        }
     }
     
     //校验层次/总层
     function checkCczc(source, arguments)
     {
        var pattern = /^(\d+)\/(\d+)$/;
        var rs = pattern.exec(arguments.Value);
        if(rs != null && rs.length > 2)
        {
            if(parseInt(rs[1]) <= parseInt(rs[2]))
            {
                arguments.IsValid = true;
            }
            else
            {
                arguments.IsValid = false;
            }
        }
        else
        {
            arguments.IsValid = false;
        }
     }
     
     /********** end *************/

function GetId(obj)
{
    var rdbList=document.getElementsByName(obj.name);
    if(rdbList!=null)
    {
        for(i=0;i<rdbList.length;i++)
        {
            if(rdbList[i].checked)
            {
                obj.title=rdbList[i].id;
                break;
            }
        }
    }
}

function Lock(obj)
{
    obj.checked=false;
    if(obj.title!='')
    {
        var rdb=document.getElementById(obj.title);
        if(rdb!=null)
        {rdb.checked=true;}
    }
}
//0 - 去除前后空格; 1 - 去前导空格; 2 - 去尾部空格
function cTrim(sInputString,iType)
{
    var sTmpStr = ' '
    var i = -1

    if(iType == 0 || iType == 1)
    {
          while(sTmpStr == ' ')
          {
               ++i
               sTmpStr = sInputString.substr(i,1)
          }
          sInputString = sInputString.substring(i)
    }

    if(iType == 0 || iType == 2)
    {
          sTmpStr = ' '
          i = sInputString.length
          while(sTmpStr == ' ')
          {
               --i
               sTmpStr = sInputString.substr(i,1)
          }
          sInputString = sInputString.substring(0,i+1)
    }
    return sInputString
}

//校验身份证是否合法，不能验证最后一位为X的
function checkUsertest(idcard) {
    var Errors = new Array(
                    "验证通过!",
                    "身份证号码位数不对!",
                    "身份证号码出生日期超出范围或含有非法字符!",
                    "身份证号码校验错误!",
                    "身份证地区非法!");
    var area = { 11: "北京", 12: "天津", 13: "河北", 14: "山西", 15: "内蒙古", 21: "辽宁", 22: "吉林", 23: "黑龙江", 31: "上海", 32: "江苏", 33: "浙江", 34: "安徽", 35: "福建", 36: "江西", 37: "山东", 41: "河南", 42: "湖北", 43: "湖南", 44: "广东", 45: "广西", 46: "海南", 50: "重庆", 51: "四川", 52: "贵州", 53: "云南", 54: "西藏", 61: "陕西", 62: "甘肃", 63: "青海", 64: "宁夏", 65: "新疆", 71: "台湾", 81: "香港", 82: "澳门", 91: "国外" }
    var idcard, Y, JYM;
    var S, M;
    var idcard_array = new Array();
    idcard_array = idcard.split("");
    //地区检验
    if (area[parseInt(idcard.substr(0, 2))] == null) {
        return Errors[4];
    }
    //身份号码位数及格式检验
    switch (idcard.length) {
        case 15:
            if ((parseInt(idcard.substr(6, 2)) + 1900) % 4 == 0 || ((parseInt(idcard.substr(6, 2)) + 1900) % 100 == 0 && (parseInt(idcard.substr(6, 2)) + 1900) % 4 == 0)) {
                ereg = /^[1-9][0-9]{5}[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|[1-2][0-9]))[0-9]{3}$/;//测试出生日期的合法性
            }
            else {
                ereg = /^[1-9][0-9]{5}[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|1[0-9]|2[0-8]))[0-9]{3}$/;//测试出生日期的合法性
            }
            if (ereg.test(idcard)) {

            }
            else {
                alert("身份证无效");
                return false;
            }
            break;
        case 18:
            //18位身份号码检测
            //出生日期的合法性检查
            //闰年月日:((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|[1-2][0-9]))
            //平年月日:((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|1[0-9]|2[0-8]))
            if (parseInt(idcard.substr(6, 4)) % 4 == 0 || (parseInt(idcard.substr(6, 4)) % 100 == 0 && parseInt(idcard.substr(6, 4)) % 4 == 0)) {
                ereg = /^[1-9][0-9]{5}19[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|[1-2][0-9]))[0-9]{3}[0-9Xx]$/;//闰年出生日期的合法性正则表达式
            }
            else {
                ereg = /^[1-9][0-9]{5}19[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|1[0-9]|2[0-8]))[0-9]{3}[0-9Xx]$/;//平年出生日期的合法性正则表达式
            }
            if (ereg.test(idcard)) {//测试出生日期的合法性
                //计算校验位
                S = (parseInt(idcard_array[0]) + parseInt(idcard_array[10])) * 7
                + (parseInt(idcard_array[1]) + parseInt(idcard_array[11])) * 9
                + (parseInt(idcard_array[2]) + parseInt(idcard_array[12])) * 10
                + (parseInt(idcard_array[3]) + parseInt(idcard_array[13])) * 5
                + (parseInt(idcard_array[4]) + parseInt(idcard_array[14])) * 8
                + (parseInt(idcard_array[5]) + parseInt(idcard_array[15])) * 4
                + (parseInt(idcard_array[6]) + parseInt(idcard_array[16])) * 2
                + parseInt(idcard_array[7]) * 1
                + parseInt(idcard_array[8]) * 6
                + parseInt(idcard_array[9]) * 3;
                Y = S % 11;
                M = "F";
                JYM = "10X98765432";
                M = JYM.substr(Y, 1);//判断校验位
                if (M == idcard_array[17]) {
                    return Errors[0]; //检测ID的校验位
                }
                else {
                    return Errors[3];
                }
            }
            else
                return Errors[2];
            break;
        default:
            return Errors[1];
            break;
    }
}
