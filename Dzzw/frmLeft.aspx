﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frmLeft.aspx.cs" Inherits="frmLeft" %>

<!-- 注意以下标题头-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>菜单导航</title>
    <link href="css/page.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        a
        {
            behavior: url(include/Link.htc);
        }
    </style>

    <script type="text/javascript" src="ScriptFile/jquery.js"></script>

    <script type="text/javascript">

        function RemoveAll() {
            top.fraMain.win.removeall();
        }

        function AddWin(url, title) {
            top.fraMain.AddWin(url, title);
        }

        //菜单点击事件
        function mouseMove(i, j) {
            $("#liMenu" + i + j).css("backgroundImage", "url(images/checked.gif)");
        }

        function mouseClick(i,j)
        {
            var myli=document.getElementById("liMenu" +i+j);
            var imgurl=document.getElementById("imgurl");
            if( myli!=null)
            {
                if(imgurl!=null)
                {
                    imgurl.value=myli.style.backgroundImage;
                    
                    for(l=0;l<50;l++)
                    {
                        var tempLi=document.getElementById("liMenu" +i+l);
                        if(tempLi!=null)
                        {
                            if(j!=l)
                            {
                                tempLi.style.backgroundImage ='';
                            }
                        }
            			
                    }
                }
                
                myli.style.backgroundImage='url(image/checked.gif)';
            }
        }
        
        function mouseOut(i, j) {
            $("#liMenu" + i + j).css("backgroundImage", "");
        }

        function openOrCloseMenu(i) {
            $("ul").hide();
            $(".left_menu_bar").css("backgroundImage", "url(images/left_bar_02.gif)");

            if ((typeof i) == "undefined" || i == "")
                return;
            try {
                $("#ulMenu" + i).show();
                $("#imgMenu" + i).css("backgroundImage", "url(images/left_bar_01.gif)");
            } catch (e) {
                var info = "initMenu";
            }
        }

        $(document).ready(function() {
            // 在这里写你的代码...
            openOrCloseMenu($('#hfdMenuValue').val());
        });
    </script>

</head>
<body bgcolor="#6E8ADE" style="border-left: 1pt solid white; border-bottom: 1pt solid white;"
    leftmargin="0" topmargin="0" oncontextmenu="return false" onselectstart="return false"
    ondragstart="return false">
    <form runat="server">
    <asp:HiddenField runat="server" ID="hfdMenuValue" />
    <%=strHtml%>
    </form>
</body>
</html>
