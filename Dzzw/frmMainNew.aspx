﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frmMainNew.aspx.cs" Inherits="frmMainNew" %>

<%@ Register Src="UserControls/ucLeftNavigation.ascx" TagName="ucLeftNavigation"
    TagPrefix="uc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>OA办公区</title>
    <link rel="stylesheet" id="skinCss" type="text/css" href="css/default/easyui.css" />
    <link rel="stylesheet" type="text/css" href="css/icon.css" />
    <script type="text/javascript" src="ScriptFile/jquery.js"></script>
    <script type="text/javascript" src="ScriptFile/jquery.easyui.min.js"></script>

    <script type="text/javascript">

        //显示进度条
        function ShowLoading() {
            //定时器2秒
            $.messager.progress({
                title: '请稍候',
                msg: '数据加载中...'
            });

        }

        //隐藏进度条
        function HideLoading() {
            $.messager.progress('close');

            //$("#loadbox").hide();
        }



        //控制右侧区域的展开和折叠
        function ctrlRegion(nType, strRegion) {
            strAction = "";
            if (nType == 1) {
                //隐藏east侧区域
                if (strAction == "expand") {
                    $('#divBody').layout('collapse', strRegion); //折叠
                    strAction = "collapse";
                }
            }
            else {
                if (strAction == "collapse") {
                    $('#divBody').layout('expand', strRegion); //展开
                    strAction = "expand";
                }
            }
        }

        //消息框
        function loadMsg(strMsg) {
            $.messager.show({
                title: '待办事项(5秒后自动关闭)',
                msg: strMsg,
                timeout: 5000,
                showType: 'slide'
            });
        }

        //设置标题头
        function loadTitle(strId, strTile) {
            $("#" + strId).panel({ title: strTile });
        }

        //关闭窗口
        function CloseDivDlg() {
            $('#dlgConfig').window('close');
        }

        //打开内嵌网页的通用方法1（window）
        function openDivDlg(strTitle, strUrl) {
            //增加参数queryid
            var divWidth = 800;
            var divHeight = 600;

            $('#dlgConfig').window({
                title: strTitle, width: divWidth, height: divHeight
            });

            $('#iframeConfig').attr("src", strUrl);

            // $('#dlgConfig').window({ maximized: true });
            $('#dlgConfig').window('open');
        }

        //改变样式
        function changeSkin() {
            $('#skin a').click(function () {
                $("#" + this.id).addClass("selected").siblings().removeClass("selected");
                $('#skinCss').attr("href", "css/" + (this.id) + "/easyui.css");
            });
        }

        $(document).ready(function () {
            changeSkin();

            initWinInfo(); //加载各类待办件的提醒
            $('#divBody').layout('collapse', 'east');
        });
    </script>
</head>
<body class="easyui-layout" id="divBody" onload="HideLoading()" style=" height:100%;">
    <script type="text/javascript">
        ShowLoading();
    </script>
    <div id="divNorth" region="north" split="false" style="height: 47px; margin: 0px;
        padding: 0px">
        <table style="background-image: url(images/top_bg.gif); height: 44px" border="0"
            cellspacing="0" cellpadding="0" width="100%">
            <tbody>
                <tr>
                    <%--<td style="color: #e04f22; font-size: 18pt" height="44">
                        泰州市高教园区管委会办公自动化系统
                    </td>--%>
                    <td style="color: #e04f22; font-size: 18pt" height="44">
                        创绩工作流平台
                    </td>
                    <td height="44">
                        <a style="float: right" href="Default.aspx" target="_parent">退出登录</a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div id="divwest" region="west" title='功能导航' split="true" style="width: 140px;">
        <uc1:ucLeftNavigation ID="ucLeftNavigation1" runat="server" />
    </div>
    <div id="divsouth" region="south" split="false" style="overflow: hidden; height: 28px;">
        <div class="easyui-layout" fit="true" style="background: #ccc;">
            <div id="starusscale" region="west" split="true" style="width: 139px; z-index: -10;">
                <div id="skin">
                    皮肤：<a id="default" style="cursor: pointer; color: Blue;" class="selected" title="">蓝</a>
                    <a id="gray" style="cursor: pointer; color: Gray;" title="">灰</a>
                </div>
            </div>
            <div id="statusbar" region="center">
            </div>
        </div>
    </div>
    <div region="east" id="divEast" split="true" title="公示信息" style="padding-left: 0px;
        margin-left: 0px; width: 150px;">
        <iframe id="ifmGsxx" scrolling="no" frameborder="0" src="ManageInside/TzggInfo.aspx"
            style="width: 100%; height: 100%;"></iframe>
    </div>
    <div region="center" title="业务办公区" style="overflow: hidden;" id="divCenter">
        <div class="easyui-tabs" id="divMain" fit="true" border="true">
            <div id="divIndex" title="业务情况一览" icon="icon-search" closable="false" style="padding: 2px;
                overflow: hidden;">
                <iframe src="Xnjc/WorkflowStatus.aspx" width="100%" height="100%" frameboder="0" id="业务情况一览"></iframe>
                <!--  信息查看  -->
                <div id="dlgConfig" class="easyui-window" title="信息查看" closed="true" modal="false"
                    minimizable="false" style="width: 700px; height: 500px; background: #fafafa;">
                    <iframe id="iframeConfig" scrolling="yes" frameborder="0" src="" style="width: 100%;
                        height: 100%;"></iframe>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
