﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using WF_Business;
using Business.FlowOperation;
using System.Text;

public partial class Office_TaskProcessingresource : PageBase
{
    //第一个可显示页面的节点号
    public string FirstNode = "";
    private int nFirst = 0;
    private RequestFlowOperation rfo = new RequestFlowOperation();


    private long iid
    {
        get
        {
            return Request["iid"] == null ? -1 : Convert.ToInt64(Request["iid"]);
        }
    }

    private long wiid
    {
        get
        {
            return Request["itemid"] == "" ? -1 : Convert.ToInt64(Request["itemid"]);
        }
    }

    private string step
    {
        get
        {
            return Request["step"];
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request["IsBack"] == "false")
        {
            ImageForm imgForm = new ImageForm();
            imgForm.TransFormToImage(iid.ToString(), step, wiid.ToString(), "辅助功能");
        }
    }

    public String GetTreeHTML()
    {
        WorkFlowResource wfr = new WorkFlowResource();
        DataTable dt = wfr.ListInstanceStepResource(wiid.ToString(), step, iid.ToString());
        dt.Columns.Add(new DataColumn("ParentId"));
        dt.Columns.Add(new DataColumn("Site"));
        dt.Columns.Add(new DataColumn("Name"));
        dt = editStepResource(dt);
        StringBuilder sb = new StringBuilder();
        sb.Append("<script type='text/javascript'>\n");
        sb.Append("var dtree1 = new dTree('dtree1');\n");
        sb.Append("dtree1.config.useCookies = false;\n");
        sb.Append("dtree1.add(0,-1,'所有表单');\n");
        string wid = WorkFlowResource.GetWidByIID(iid.ToString());
        printSubNodes(dt, sb, wid);
        sb.Append("document.write(dtree1);\n");
        sb.Append("</script>\n");

        return sb.ToString();
    }

    private DataTable editStepResource(DataTable dt)
    {
        DataView dv = dt.DefaultView;
        dv.Sort = "Path";
        for (int i = 0; i < dv.Count; i++)
        {
            string path = dv[i]["Path"].ToString();
            string[] tmps = path.Split('/');
            dv[i]["Site"] = i + 1;
            if (tmps.Length > 1)//不是顶节点
            {
                dv[i]["Name"] = tmps[tmps.Length - 1];
                string pre = path.Substring(0, path.LastIndexOf('/'));
                DataRow[] drs = dt.Select("Path='" + pre + "'");
                if (drs.Length > 0)
                {
                    dv[i]["ParentId"] = drs[0]["Site"].ToString();
                }
            }
            else
            {
                dv[i]["Name"] = dv[i]["Path"].ToString();
                dv[i]["ParentId"] = 0;
            }
        }

        return dt;
    }

    private void printSubNodes(DataTable dt, StringBuilder sb, string wid)
    {
        foreach (DataRow dr in dt.Rows)
        {
            string type = dr["type"].ToString();
            String strOutput = string.Empty;
            DataRow[] drs = dt.Select("ParentId='" + dr["Site"].ToString() + "'");
            string strsite = dr["Site"].ToString();
            string strparentid = dr["ParentId"].ToString();
            string strName = dr["Name"].ToString();
            string strPath = dr["Path"].ToString();
            string strfolder = "../images/folder.gif";
            string strfolderopen = "../images/folderopen.gif";
            string idex = dr["input_index"].ToString();
            string rtid = dr["rtid"].ToString();
            string right = "false";

            if (string.IsNullOrEmpty(strparentid))
            {
                strparentid = "0";
            }

            if (drs.Length > 0)//有子节点，说明不是叶结点
            {
                strOutput = setOutPut(strsite, strparentid, strName, "", strPath, "", strfolder, strfolderopen, type, right, idex,rtid);
            }
            else
            {
                string resValue = dr["Res_Value"].ToString();
                switch (type)
                {
                    case "0"://目录
                        strOutput = setOutPut(strsite, strparentid, strName, "", strName, "", "", "", type, right, idex,rtid);
                        break;
                    case "1":
                        resValue = ResolveUrl(resValue);
                        strOutput = setOutPut(strsite, strparentid, strName, resValue, resValue, "", "", "", type, right, idex, rtid);
                        break;
                    case "2":
                        string extName = rfo.GetAttachementExtName(resValue);
                        if (extName == ".doc" || extName == ".docx")
                        {
                            type = "21";
                        }
                        strOutput = setOutPut(strsite, strparentid, strName, strPath, resValue, "", "", "", type, right, idex, rtid);
                        break;
                    case "3":
                        //string count = rfo.GetFormTableNum(wid, Request["step"], dr["path"].ToString(), dr["Res_Value"].ToString());
                        //if (!String.IsNullOrEmpty(count) && count != "0")
                        if(WorkFlow.CheckTableRight(iid.ToString(), wid, step, dr["rtid"].ToString(), resValue,UserId))
                        {
                            right = "true";

                            //如果有权限操作表单但不是第一个则默认打开最后一个有权限的表单
                            if (nFirst <= 1)
                            {
                                if (string.IsNullOrEmpty(FirstNode))
                                {
                                    nFirst = 2;
                                }
                                else
                                {
                                    nFirst++;
                                }

                                FirstNode = strsite;

                            }
                        }
                        strOutput = setOutPut(strsite, strparentid, strName, strPath, resValue, "", "", "", type, right, idex, rtid);
                        break;
                }
                //如果是第一次不管有没有权限，都将表单地址寄存
                if (nFirst == 0 && (type == "3" || type == "1"))//表单或外部页面
                {
                    FirstNode = strsite;
                    nFirst++;
                }
            }
            sb.Append(strOutput);
        }
        Response.Write("<input type='hidden' id='txtFirstNode' value ='" + FirstNode + "' />");
    }

    //构建左边树
    private string setOutPut(string site, string parentid, string name, string url, string path, string target,
        string folder, string folderopen, string type, string right, string idex,string rtid)
    {
        string strOutput = string.Format("dtree1.add({0},{1},'{2}','{3}','{4}','{5}','{6}','{7}',{8},{9},{10},'{11}');\n",
                             site, parentid, name, url, path, target, folder, folderopen, type, right, idex,rtid);

        return strOutput;
    }

}
