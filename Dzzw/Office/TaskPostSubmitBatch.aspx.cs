using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using WF_Business;
using WF_DataAccess;
using Business.FlowOperation;
using Business;
using Common;

/// <summary>
/// 业务提交逻辑页面（包括归档）
/// </summary>
public partial class Office_TaskPostSubmit : System.Web.UI.Page
{
    RequestFlowOperation rfo = new RequestFlowOperation();
    protected void Page_Load(object sender, System.EventArgs e)
    {
        //Put user code to initialize the page here
        if (!IsPostBack)
        {
            InstanceSubmit();
        }
    }

    //所选的流向列表
    private string[] flows
    {
        get { return Request.Params.GetValues("flows"); }
    }
    //所选结点列表
    private string[] stages
    {
        get { return Request.Params.GetValues("stages"); }
    }

    //获取提交流向           
    private string fName
    {
        get { return (flows == null) ? Request["steps"] : flows[0]; }
    }


    public void InstanceSubmit()
    {
        //案件提交部分
        bool Success = true;
        string msg = string.Empty;
        WorkItem workItem = new WorkItem();
        ArrayList arrList = new ArrayList();

        //如果提交采用流向显示模式时
        if (flows != null)
        {
            for (int i = 0; i < flows.Length; i++)
            {
                FlowUserStruct fus = new FlowUserStruct();
                fus.IsRound = Request["ckbRound"] != null;
                fus.FlowName = flows[i];
                fus.Users = Request.Params.GetValues("User_" + flows[i]); //为null时为岗位所有用户
                arrList.Add(fus);
            }
        }

        //如果提交采用岗位显示模式时
        if (stages != null)
        {
            for (int i = 0; i < stages.Length; i++)
            {
                string[] users = Request.Params.GetValues("User_" + stages[i]);
                if (users != null)
                {
                    if (users.Length > 0)//为null时无效
                    {
                        FlowUserStruct fus = new FlowUserStruct();
                        fus.IsRound = Request["ckbRound"] != null;
                        fus.FlowName = fName;
                        fus.StepName = stages[i];
                        fus.Users = users;
                        arrList.Add(fus);
                    }
                }
            }
        }

        //往往是最后一个岗位时
        if (flows == null && stages == null & !string.IsNullOrEmpty(fName))
        {
            FlowUserStruct fus = new FlowUserStruct();
            fus.IsRound = false;
            fus.FlowName = fName;
            arrList.Add(fus);
        }

        if (arrList.Count > 0)
        {
            FlowUserStruct[] arrFus = new FlowUserStruct[arrList.Count];
            for (int i = 0; i < arrList.Count; i++)
            {
                arrFus[i] = (FlowUserStruct)arrList[i];
            }

            //开始遍历iid并提交
            string[] arrIIDs = Request["iids"].Split(',');
            string[] arrWIIDs = Request["wiids"].Split(',');
            for (int i = 0; i < arrIIDs.Length; i++)
            {
                IDataAccess dataAccess = SysParams.OAConnection(true);
                try
                {
                    long iid = Int64.Parse(arrIIDs[i]);
                    long wiid = Int64.Parse(arrWIIDs[i]);
                    Success = workItem.Sumbit(iid, wiid, arrFus, ref dataAccess, ref msg);
                    string wname = rfo.GetWnameFromStinsance(iid.ToString());

                    if (Success == false)
                    {
                        dataAccess.Close(false);
                        string strScript = "alert('提交没有成功，原因是：" + msg + "');window.close();";
                        WindowAppear.ExecuteScript(this.Page, strScript);
                    }
                    dataAccess.Close(true);
                }
                catch
                {
                    dataAccess.Close(false);
                }
            }//遍历iid结束
        }
        else
        {
            Success = false;
            msg = "没有选择提交流向";
        }
        
    }

    /// <summary>
    /// 判断是否是有效的iid
    /// </summary>
    /// <param name="strIid">有效的iid</param>
    /// <returns></returns>
    public bool IsValideIid(string strIid)
    {
        bool bReturn = false;
        string strFirstNum = strIid.Substring(0, 1);
        if (strFirstNum == "2")
        {
            bReturn = true;
        }

        return bReturn;
    }

}