using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using WF_Business;
using Common;

/// <summary>
/// Summary description for doworkitem.
/// </summary>
public partial class Office_doworkitem : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        String strIID = Request["IID"];
        //暂时先写在这里实现了来
        string sql = "select b.aid,b.ext_name from st_dynamic_resource a,st_attachment b Where a.res_value=b.aid  AND a.iid=" + strIID + " AND a.Path='" + Request["formname"] + "'";
        if (!String.IsNullOrEmpty(Request["formname"]))
        {
            System.Data.DataTable dt;
            SysParams.OAConnection().RunSql(sql, out dt);
            if (dt.Rows.Count > 0)
            {
                string ext = dt.Rows[0]["ext_name"].ToString();
                if (ext.ToLower().IndexOf("doc")>-1)
                {
                    ConfigHelper config = new ConfigHelper(true);
                    String ResServerPath = string.Format("http://{0}{1}", Request.Headers[5],
                        Business.SystemConfig.NetoWsUrl.Replace("WebFormService.asmx", ""));

                    Response.Redirect(ResServerPath + "Attachment/OfficeHandle.aspx?UserId=" + UserId + "&iid=" + strIID + "&aid=" + dt.Rows[0]["aid"].ToString());
                }
            }
        }
    }
}