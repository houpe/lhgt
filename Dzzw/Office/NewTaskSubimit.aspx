﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="NewTaskSubimit.aspx.cs" Inherits="NewTaskSubimit" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script src="../ScriptFile/jquery.js" type="text/javascript"></script>
    <script src="../ScriptFile/Common/Jquery.Global.js" type="text/javascript"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>提交页面</title>

    <style type="text/css">
        #DivFlowList table {
            border-right: 1.2px solid blue;
            border-bottom: 1.2px solid blue;
            empty-cells: show;
            border-collapse: collapse;
        }

            #DivFlowList table td {
                border-left: 1.2px solid blue;
                border-top: 1.2px solid blue;
            }
    </style>

</head>
<script type="text/javascript">
    var strServicePath = window.location.protocol + "//" + window.location.host + "<%=Business.SystemConfig.NetoWsUrl%>";
    var StepsFlow = "";

    //设置默认的下个流程
    function SetDefaultSteps() {
        $("#DivHead").find("input[type='radio'][name='steps']").each(function (index) {
            if (index == 0) {

                $(this).attr("checked", "checked");
                StepsFlow = $(this).val();
            }
        })
    }

    //选择相应的流向
    function OnStepSelect(aa, object) {
        StepsFlow = $(object).attr("value");
        getrolelist();
    }

    //获取下一个岗位
    function getnextlist() {
        WebService(strServicePath + '/GetNextSteps', GetStepsCallBack, '{"wid":"<%=wid%>","step":"<%=ctlid%>","iid":"<%=iid%>","wiid":"<%=wiid%>"}');
    }

    function GetStepsCallBack(result) {
        if (result != null) {
            var divhead = document.getElementById("DivHead");
            divhead.innerHTML = result;
        }
        SetDefaultSteps();
    }

    //获取指定流向下角色列表
    function getrolelist() {
        WebService(strServicePath + '/GetRoleList', GetRoleListResult, '{"nItemId":"<%=wiid%>","iid":"<%=iid%>","flow":"' + StepsFlow + '"}');
    }

    //返回流向名称
    function GetRoleListResult(result) {
        if (result != null) {
            if (result == "#END#") {
                if (confirm("确认提交归档吗？")) {
                    Submit(); //单一用户直接提交
                }
                else {
                    window.close();//关闭
                }
            }
            else {
                $("#DivFlowList").html("");
                var tableHtml = "<table width='100%' cellpadding='0' border='0' cellspacing='0' s><tr><td style='width:25px'></td><td style='width:235px'></td><td  style='width:228px'> <input type='checkbox'   id='chkAll' onclick='ChkAll(this)' />全选</td></tr>";
                var result2 = "<xml>" + result + "</xml>";
                var xmlDoc = $.parseXML(result2);

                $(xmlDoc).find("flow").each(function (indext) {
                    tableHtml += "<tr><td ><input   type='checkbox'  id='row_" + indext + "'  onclick='GetUserbyRole(this)' /></td><td id='row1_" + indext + "'>" + $(this).text() + "</td><td id='row2_" + indext + "'></td></tr>";
                });
                tableHtml += "<tr><td colspan='2' style='text-align:center'>  <input type='button' value='提交' id='submit' onclick='Submit()'  /></td><td><input type='button' value='关闭' onclick='Cclose()' id='btnClose' /></td> </tr>";
                tableHtml += "</table>"

                $("#DivFlowList").append(tableHtml);
                $("#chkAll").trigger("click");
            }
        }
    }

    //角色勾选响应逻辑
    var chkID = "";
    function ChkAll(object) {
        chkID = "";
        var trlength = $("#DivFlowList tr").length;

        if ($(object).is(":checked")) {

            for (var i = 0; i < trlength - 2; i++) {
                $("#row_" + i).prop("checked", true);
                chkID = "row_" + i;
                GetUserbyRole();
            }

        } else {
            for (var i = 0; i < trlength - 2; i++) {
                $("#row_" + i).attr("checked", false);
                $("#row2_" + i).html("");
            }
        }
    }

    //通过角色查找用户
    var tmpRowId;
    function GetUserbyRole(object) {        
        try {
            tmpRowId = object.id;
        } catch (ex) {
            tmpRowId = chkID;
        }

        var idindex = tmpRowId.indexOf("_");
        var isFirsttd = tmpRowId.substring(idindex - 1, idindex);
        var newid = tmpRowId.substring(0, idindex) + "_1";
        var last = tmpRowId.substr(tmpRowId.length - 1, 1);
        if (isFirsttd == "w") {
            if ($("#row2_" + last).text() == "" || $("#row2_" + last).text() == null) {
                var Name = $("#row1_" + last).text();
                WebService(strServicePath + '/GetUserInStep', ShowUsers, '{"nItemId":"<%=wiid%>","iid":"<%=iid%>","strName":"' + Name + '","findMode":"1"}');
            }
            else {
                $("#row2_" + last).text('');
            }

        }
    }

    //勾选角色显示对应用户
    function ShowUsers(result) {
        if (result == "#END#") {
            return;
        }
        if (result == "#ERROR#") {
            alert("下一节点上没有配置用户。");
            return;
        }

        //存储返回用户
        var arresult = result.split(";");

        var idindex = tmpRowId.indexOf("_");
        var isFirsttd = tmpRowId.substring(idindex - 1, idindex);
        var newid = tmpRowId.substring(0, idindex) + "_1";
        var last = tmpRowId.substr(tmpRowId.length - 1, 1);

        for (var i = 0; i < arresult.length; i++) {
            var nameindex = arresult[i].indexOf("|");
            var username = arresult[i].substr(nameindex + 1, arresult[i].length);
            $("#row2_" + last).append("<input  id=chkName" + i + "  type='checkbox'  checked='0'  title='" + arresult[i] + "'  />" + username);
        }
    }

    //提交
    function Submit() {
        var users="";  //存储角色对应的用户
        var index = 0;
        var trLength = $("#DivFlowList tr").length;//获取tr的数量
        if (trLength == 0) {
            WebService(strServicePath + '/Sumbit', submitCallBack, '{"iid":"<%=iid%>","wiid":"<%=wiid%>","flow":"' + StepsFlow + '","strUsers":"' + users + '"}');
        }
        else {
            for (var i = 0; i < trLength - 2; i++) {
                users = "";
                $("#row2_" + i).find("input").attr("checked", true).each(function () {
                    var titleall = $(this).attr("title");
                    var nameindex = titleall.indexOf("|");
                    var titlename = titleall.substr(0, nameindex);
                    if (index == 0) {
                        users = titlename;
                        index++;
                    }
                    else {
                        users += ("," + titlename);
                    }
                });
                if ($("#row2_" + i).find("input").attr("checked") == 'checked') {
                    WebService(strServicePath + '/Sumbit', submitCallBack, '{"iid":"<%=iid%>","wiid":"<%=wiid%>","flow":"' + StepsFlow + '","strUsers":"' + users + '"}');
                }
            }
        }
    }

    function submitCallBack(result) {
        if (result != "") {
            alert(result);
        }
        else {
            alert("提交成功!");
            Cclose();
        }
    }

    

    //关闭窗口和父窗口并更新父窗口的父窗口内容
    function Cclose() {
        var strUrl = opener.opener.location.href;
        if (strUrl.indexOf("?") > 0) {
            strUrl += "&rdm=" + Math.random();
        }
        else {
            strUrl += "?rdm=" + Math.random();
        }
        opener.opener.location.href = strUrl;
        window.opener.close();
        window.close();
    }

   
</script>

<body onload="getnextlist(),getrolelist()">
    <form id="form1" runat="server">

        <div id="DivHead" style="text-align: center"></div>
        <div id="DivFlowList" style="text-align: center">
        </div>

    </form>
</body>
</html>
