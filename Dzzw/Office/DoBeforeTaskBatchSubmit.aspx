﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DoBeforeTaskBatchSubmit.aspx.cs"
    Inherits="Office_DoBeforeTaskBatchSubmit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>批量提交页面</title>
    <link type="text/css" rel="Stylesheet" href="../css/page.css" />
    <script src="../ScriptFile/jquery.js" type="text/javascript"></script>
    <script src="../ScriptFile/Common/jquery.xslt.js" type="text/javascript"></script>
    <script type="text/javascript" src="../ScriptFile/Common/Jquery.Global.js"></script>
</head>
<script type="text/jscript">
    //工作流引擎地址
    var strServicePath = window.location.protocol + "//" + window.location.host + "<%=Business.SystemConfig.NetoWsUrl%>";

    //单选按钮响应16--add by lj
    function GetWorkItemID() {
        return formSubmit.workitem.value;
    }

    //获取岗位信息
    function GetStepList() {
        WebService(strServicePath+'/GetNextSteps', ShowSteps, '{"wid":"<%=wid%>","step":"<%=ctlid%>","iid":"<%=iid%>","wiid":"<%=wiid%>"}');
    }

    //事件响应3
    function ShowSteps(result) {
        if (result != null) {
            DivSteps.innerHTML = result;

            var objflowsCtrl = eval("formSubmit.steps");

            //触发事件OnStepSelect();
            if (objflowsCtrl != null) {
                if (objflowsCtrl.length == null) //修改
                {
                    objflowsCtrl.click();
                }
                else {
                    objflowsCtrl[0].click();
                }
            }
            else {
                alert('下一节点暂未配置任何岗位');
            }
        }
    }

    //各岗位对应的checkbox点击事件33--add by lj
    function ResetFlowSelect(Sender) {
        var objAllRadio = document.getElementsByName("flows")

        for (i = 0; i < objAllRadio.length; i++) {
            var UserDivID = "Div_" + objAllRadio[i].value;
            var objUserDiv = document.getElementById(UserDivID);
            if (Sender.value != objAllRadio[i].value)
                objAllRadio[i].checked = "";
            ClearUserDiv(objUserDiv);
        }
        objAllRadio = document.getElementsByName("stages")
        for (i = 0; i < objAllRadio.length; i++) {
            var UserDivID = "Div_" + objAllRadio[i].value;
            var objUserDiv = document.getElementById(UserDivID);
            if (Sender.value != objAllRadio[i].value)
                objAllRadio[i].checked = "";
            ClearUserDiv(objUserDiv);
        }
    }

    var UserDivID = "";
    var strFlowName;
    var objUserDiv;
    //流向的复选框点击事件，strMode=0流向查找，strMode=1代表角色查找
    function OnFlowChangeState(FlowName, strMode, Sender) {
        strFlowName = FlowName;
        UserDivID = "Div_" + FlowName;
        objUserDiv = document.getElementById(UserDivID);

        if (Sender.type == "checkbox") {
            if (Sender.checked == false) {
                ClearUserDiv(objUserDiv);
                return;
            }
        }
        else {
            ResetFlowSelect(Sender);
        }

        objUserDiv.style.display = "block";
        var strUsers = GetUserListInStep(GetWorkItemID(), FlowName, strMode);
    }

    //得到指定节点上的用户列表,vMode=0流向查找，vMode=1代表角色查找
    var WorkItemId = "";
    var FlowName = "";
    function GetUserListInStep(vWorkItemId, vFlowName, vMode) {
        WorkItemId = vWorkItemId;
        FlowName = vFlowName;

        WebService(strServicePath+'/GetUserInStep', GetUserInStepResult, '{"nItemId":"' + vWorkItemId + '","iid":"<%=iid%>","strName":"' + vFlowName + '","findMode":"' + vMode + '"}');
    }

    var firstflow = false;
    //各岗位对应的checkbox点击事件35--add by lj
    function GetUserInStepResult(result) {
        if (result != null) {
            var strUsers = result;
            if (strUsers == "#END#") {
                return;
            }
            if (strUsers == "#ERROR#") {
                alert("选定节点上没有配置用户。");
                return;
            }
            if (firstflow == true) {
                var Users = strUsers.split(";");
                if (Users.length == 1) {
                    strFlowName = FlowName
                    UserDivID = "Div_" + FlowName;
                    objUserDiv = document.getElementById(UserDivID);
                    AddUserToDiv(strUsers, objUserDiv, strFlowName);
                }
                else {
                    if (strUsers != "")
                        AddUserToDiv(strUsers, objUserDiv, strFlowName);
                }
            }
            else {
                if (strUsers != "")
                    AddUserToDiv(strUsers, objUserDiv, strFlowName);
            }
            firstflow = false;
        }
    }

    //清除用户区域
    function ClearUserDiv(objUserDiv) {
        objUserDiv.innerText = "";
        objUserDiv.style.display = "none";
    }

    //提交事件前验证是否选择用户
    function ValidateSelectUser(objUserCtrl) {
        var bResult = false;

        if (objUserCtrl != null) {
            if (objUserCtrl.length == 0 || objUserCtrl.length == null) {
                if (objUserCtrl.checked == true) {
                    bResult = true;
                }
            }
            else {
                for (j = 0; j < objUserCtrl.length; j++) {
                    if (objUserCtrl[j].checked == true) {
                        bResult = true;
                        break;
                    }
                }
            }
        }
        else
            return true;

        return bResult;
    }

    //提交按钮事件（进行提交验证）
    function OnBeforePost() {
        //先判断是否选择了流向和用户
        var flowsCtrlName = "formSubmit.stages"; //还有一种情况是获取"formSubmit.flows"
        var objflowsCtrl = eval(flowsCtrlName);
        var bSelected = false;

        if (objflowsCtrl.length == null) {
            if (objflowsCtrl.checked == true) {
                var UserCtrlID = "formSubmit.User_" + objflowsCtrl.value;
                var objUserCtrl = eval(UserCtrlID);
                bSelected = ValidateSelectUser(objUserCtrl);//判断是否选择用户
            }
        }
        else {
            for (i = 0; i < objflowsCtrl.length; i++) {
                if (objflowsCtrl[i].checked == true) {
                    var UserCtrlID = "formSubmit.User_" + objflowsCtrl[i].value;
                    var objUserCtrl = eval(UserCtrlID);

                    bSelected = ValidateSelectUser(objUserCtrl);//判断是否选择用户
                }

                if (bSelected)
                    break;
            }
        }
        if (!bSelected) {
            alert("请选择提交流向和用户。");
            return false;
        }
        else {//选择了用户则提交
            if (confirm("确认提交吗？")) {
                formSubmit.submit();
                return true;
            }
        }
    }

    //各岗位对应的checkbox点击事件36--add by lj
    function AddUserToDiv(strUsers, objUserDiv, FlowName) {
        var Users = strUsers.split(";");
        var strHTML = "";
        var bFirst = true;
        var count = 0;
        var tabHead = "<table border='0'>";
        var tabEnd = "</table>";

        for (i = 0; i < Users.length; i++) {
            var UserIdAndName = Users[i].split("|");
            var type = "type='checkbox'";
            //var check = "checked";
            var check = "";
            if (FlowName == "岗位内提交") {
                type = "type='radio'";
                if (bFirst == false)
                    check = "";
                else
                    bFirst = false;
            }
            count++;
            if (count % 2 == 0) {
                strHTML += "<td align='left' width='50%'><span><input " + type + " id = 'User_" + FlowName + "' name = 'User_" + FlowName + "' value = '" + UserIdAndName[0] + "' " + check + ">" + UserIdAndName[1] + "</span></td></tr>";
            }
            else {
                strHTML += "<tr><td align='left' width='50%'><span><input " + type + " id = 'User_" + FlowName + "' name = 'User_" + FlowName + "' value = '" + UserIdAndName[0] + "' " + check + ">" + UserIdAndName[1] + "</span></td>";
            }
        }
        if (count % 2 != 0) {
            tabEnd = "<td></td></tr></table>"
        }
        strHTML = tabHead + strHTML + tabEnd;
        objUserDiv.innerHTML = strHTML;
    }

    //全选事件
    function SelectAll(ctl) {
        //var objs = formSubmit.getElementsByTagName("input");
        //        if (ctl.checked == true) {//全选框
        //            for (var i = 0; i < objs.length; i++) {
        //                if (objs[i].type.toLowerCase() == "checkbox")
        //                    objs[i].checked = true;
        //            }
        //        }
        //        else {
        //            for (var i = 0; i < objs.length; i++) {
        //                if (objs[i].type.toLowerCase() == "checkbox")
        //                    objs[i].checked = false;
        //            }
        //        }

        var strId = ctl.id;
        var objs = $(":checkbox[name!='flows'][name!='stages'][id!='" + strId + "']");
        for (var i = 0; i < objs.length; i++) {
            objs[i].checked = ctl.checked;
        }
    }

    //获取角色列表11
    function OnStepSelect(exType, sender) {
        WebService(strServicePath+'/GetRoleList', GetFlowListResult, '{"nItemId":"<%=wiid %>","iid":"<%=iid%>","flow":"' + sender.value + '"}');
    }

    //加载xml代码并分析
    function JqueryLoadXml(xml) {
        var xmlDoc = $.parseXML(xml);

        //$(xmlDoc).find("result > out").each(function () {
        //    alert($(this).find("flow").text());//each是循环执行，即多次弹出。 
        //    alert($(this).attr("name"));//取得属性的方法 
        //});

        var nodeOut = $(xmlDoc).find("result > out");
        if (nodeOut.length > 0) {
            firstflow = true;
            return true;
        }
        else {
            return false;
        }
    }

    //返回流向名称
    function GetFlowListResult(result) {
        if (result != null) {
            if (result == "#END#") {
                if (confirm("确认提交归档吗？")) {
                    formSubmit.submit(); //单一用户直接提交
                }
                else {
                    window.close();//关闭
                }
            }
            else {
                //解析查询得到流向列表
                //var xmldoc = GetXmlDocObject();
                //xmldoc.async = false;
                //xmldoc.loadXML(result);
                //if (!BeforeShowFlows(xmldoc, GetWorkItemID())) return;
                //ShowFlows(xmldoc);

                if (!JqueryLoadXml(result)) return;
                //加载xml并匹配xslt，最终结果放入DivFlowList中
                $("#DivFlowList").xslt(result, "FlowList.xslt", clickStep);
            }
        }
    }

    //自动触发岗位点击事件
    var clickStep = function () {
        var flowsCtrlName = "formSubmit.flows";
        var objflowsCtrl = eval(flowsCtrlName);
        if (objflowsCtrl == null) {
            objflowsCtrl = eval("formSubmit.stages");
        }
        if (objflowsCtrl != null && objflowsCtrl.length == null) {
            objflowsCtrl.click();
        }
    };
		
</script>
<body topmargin="30" onload="GetStepList()">
    <form name="formSubmit" action="TaskPostSubmitBatch.aspx?wiids=<%=Request["ItemId"]%>&iids=<%=Request["iid"]%>" method="post">
    <input type="hidden" name="workitem" value="<%=wiid%>" />
    <input type="hidden" name="ctlid" value="<%=ctlid%>" />
    <input type="hidden" name="iid" value="<%=iid%>" />
    <div align="center" id="DivSteps">
    </div>
    <div id="DivFlowList">
    </div>
    </form>
</body>
</html>
