﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WF_Business;
using Business.FlowOperation;


public partial class Office_DoBeforeTaskBatchSubmit: System.Web.UI.Page
{
    public string[] arrWiid;
    public string[] arrIID;

    /// <summary>
    /// 节点ID。
    /// </summary>
    public decimal wiid
    {
        get
        {
            if (Request["ItemId"] == null)
                return decimal.Zero;
            else
            {
                arrWiid = Request["ItemId"].Split(',');
                return Convert.ToDecimal(arrWiid[0]);
            }
        }
    }

    /// <summary>
    /// 岗位ctlid
    /// </summary>
    public string ctlid
    {
        get
        {
            RequestFlowOperation rfoTemp = new RequestFlowOperation();
            return Request["ctlid"] == null ? string.Empty : Request["ctlid"];
        }
    }

    /// <summary>
    /// 实例ID。
    /// </summary>
    public decimal iid
    {
        get
        {
            if (Request["iid"] == null)
                return decimal.Zero;
            else
            {
                arrIID = Request["iid"].Split(',');
                return Convert.ToDecimal(arrIID[0]);
            }
        }
    }

    /// <summary>
    /// 流程ID
    /// </summary>
    public string wid
    {
        get
        {
            return WorkFlowResource.GetWidByIID(iid.ToString());
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
    }
}
