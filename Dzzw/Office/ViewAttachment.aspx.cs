﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Business;
using WF_Business;

public partial class office_ViewAttachment : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (string.IsNullOrEmpty(Request["iid"]) || string.IsNullOrEmpty(Request["path"]))
            {
                return;
            }

            string strIid = Request["iid"];
            string strSql = string.Format(@"select a.ext_name,a.data,a.aid
              from st_attachment a
             where trim(aid) in (select t.res_value
                             from st_dynamic_resource t
                            where iid = '{0}' and trim(path)='{1}')",
                    strIid, Request["path"].Trim());

            DataTable dtStoreInfo = null;
            SysParams.OAConnection().RunSql(strSql, out dtStoreInfo);

            if (dtStoreInfo.Rows.Count > 0)
            {
                string strFileType = dtStoreInfo.Rows[0][0].ToString();
                strFileType = strFileType.ToLower();
                Response.Buffer = true;

                if (!string.IsNullOrEmpty(strFileType))
                {
                    Response.ClearContent();

                    byte[] bArr = dtStoreInfo.Rows[0][1] as Byte[];

                    strFileType = strFileType.ToLower().Replace(".", "");
                    if (strFileType.CompareTo("jpg") == 0 || strFileType.CompareTo("jpeg") == 0 || strFileType.CompareTo("gif") == 0 ||
                        strFileType.CompareTo("png") == 0 || strFileType.CompareTo("bmp") == 0)
                    {
                        ImageView1.Src = string.Format("ProcessAttachment.ashx?aid={0}", dtStoreInfo.Rows[0][2]);
                        ImageView1.Visible = true;
                        ImageView1.Attributes.Add("style", "zoom:100%");
                    }
                    else
                    {
                        if (strFileType.CompareTo("pdf") == 0)
                        {
                            Response.ContentType = "application/pdf";
                            Response.BinaryWrite(bArr);
                        }
                        else if (strFileType.CompareTo("zip") == 0)
                        {
                            Response.ContentType = "application/zip";
                            Response.BinaryWrite(bArr);
                        }
                        else if (strFileType.CompareTo("txt") == 0)
                        {
                            Response.BinaryWrite(bArr);
                        }
                        else if (strFileType.CompareTo("xls") == 0 ||
                            strFileType.CompareTo("doc") == 0 || strFileType.CompareTo("docx") == 0 ||
                            strFileType.CompareTo("ppt") == 0 ||
                            strFileType.CompareTo("rtf") == 0)
                        {
                            string strScript = string.Format("<script>window.parent.SwichControl('ViewAttachment','{0}');</script>", Request["path"]);
                            Response.Write(strScript);
                        }
                        else
                        {
                            Response.ContentType = "application/x-msdownload;";

                            string strFileName = string.Format("{0}.{1}", dtStoreInfo.Rows[0][2], strFileType);
                            Response.AddHeader("content-disposition", "filename=" + strFileName);
                            Response.BinaryWrite(bArr);
                        }
                    }

                    Response.Flush();
                }
            }
            //if判断结束
        }
    }
}
