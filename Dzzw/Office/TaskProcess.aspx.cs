﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business.FlowOperation;
using WF_Business;
using Business;
using System.Data;
using Common.Log;

public partial class Office_TaskProcess : PageBase
{
    
    public bool bBack = false;//可以回退否，要根据流程定义来取
    public bool bStop = true;//是否可以中止，根据角色中is_Stop来取决
    public string strWiid = "";
    public string strInstanceId = "";
    public string strStepName = "";

    public string strServicePath = "";//工作流引擎地址
    public string strPageViewPath = "";//页面查看
    public string strFlowViewPath = "";//流程查看

    public string strWname = "";
    public string strStepCtlid = "";//获取当前岗位对应的id

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            RequestFlowOperation flowOperation = new RequestFlowOperation();

            strWiid = Request["ItemId"];
            strInstanceId = Request["iid"];
            strStepName = Server.UrlDecode(Request["step"]);
            strStepCtlid = strStepName;

            //获取岗位组
            DataTable dtStep = flowOperation.GetStepDetail(strWiid);
            if (dtStep.Rows.Count > 0)
            {
                if (!string.IsNullOrEmpty(dtStep.Rows[0]["stepctlid"].ToString()))
                {
                    strStepCtlid = dtStep.Rows[0]["stepctlid"].ToString();
                }

                if (!string.IsNullOrEmpty(dtStep.Rows[0]["stepgroupctlid"].ToString()))
                {
                    strStepCtlid = dtStep.Rows[0]["stepgroupctlid"].ToString();
                }

                //如果是岗位组则将岗位名称设置为岗位组的名称，因为Request["step"]获取的是角色名称
                if (!string.IsNullOrEmpty(dtStep.Rows[0]["stepgroup"].ToString()))
                {
                    strStepName = strStepCtlid;
                }
            }

            //判断是否终止过
            string strIsStop = flowOperation.IsHaveStopRight(strStepName);
            if (strIsStop == "0")
            {
                bStop = false;
            }

            //判断是否回退过
            string strHaveFlowRecord = flowOperation.HaveFlowRecord(strInstanceId, UserId);
            if (!String.IsNullOrEmpty(strHaveFlowRecord))
            {
                bBack = true;
            }
            else
            {
                bBack = false;
            }

            ///触发案件打开事件
            string msg = "";
            if (false == WorkFlow.TaskOpenCheck(strInstanceId, strWiid, strStepCtlid, UserId, out msg))
            {
                Response.Write("<script language=javascript>alert('" + msg + "');window.close();</script>");
                Response.End();
            }


            try
            {
                TaskOpenOperation taskOpen = new TaskOpenOperation();
                taskOpen.ExecProdureTaskOpen(strStepName, strInstanceId);
            }
            catch (Exception ex)
            {
                ILogger ilog = LoggerFactory.GetLogger();
                ilog.WriteInfo("OfficeWeb", "错误", "打开业务时发生错误,User=" + UserId
                    + ",IID=" + strInstanceId + ",Step=" + strStepName + ",错误信息为" + ex.Message + "\n,出错堆栈为" + ex.StackTrace);
            }

            strServicePath = SystemConfig.NetoWsUrl;
            strFlowViewPath = SystemConfig.WorkflowUrl;
            strPageViewPath = string.Format("{0}?iid={1}&Step={2}&UserId={3}",
                SystemConfig.NewActivexAppearUrl, strInstanceId, Server.UrlEncode(strStepCtlid), UserId);
        }
    }
}
