<?xml version="1.0" encoding="gb2312" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="result">
    <DIV>
      <table border="0" align="center" cellspacing="1" cellpadding="2" bgcolor="#97b4d4" width="100%">
        <tr>
          <td height="23" width="4%" style="font-size:16px;font-family:Arial;background-image: url(../images/tab_bar.gif);">
            
          </td>
          <td height="23" width="48%" style="font-size:16px;font-family:Arial;background-image: url(../images/tab_bar.gif);" align="center">

          </td>
          <td height="23" width="48%" style="font-size:16px;font-family:Arial;background-image: url(../images/tab_bar.gif);" align="center">
<input id="chkSelect" type="checkbox" value="全选" onclick="SelectAll(this);" />全选
          </td>
        </tr>
        <xsl:apply-templates select="out" />
        <tr>

          <!--<table border="0" align="center" cellspacing="0.1" cellpadding="2" bgcolor="#97b4d4" width="50%">-->
          <td align="center" width="48%" bgcolor="white" colspan="2">
            <input type="button" class="NewButton" value="提交" style="font-size:12px;font-family:Arial;">
              <xsl:attribute name="onclick">return OnBeforePost();</xsl:attribute>
            </input>
          </td>
          <td align="center" width="48%"  bgcolor="white">
            <input type="button" class="NewButton" value="关闭" style="font-size:12px;font-family:Arial;">
              <xsl:attribute name="onclick">window.close();</xsl:attribute>
            </input>
          </td>
          <!--</table>-->

        </tr>
      </table>
    </DIV>
  </xsl:template>

  <xsl:template match="out">
    <xsl:choose>
      <xsl:when test="@method='1'">
        <xsl:apply-templates select="flow" mode="MULTI" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates select="flow" mode="SINGLE" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="flow" mode="MULTI">
    <tr>
      <td bgcolor="white" width="25">
        <xsl:choose>
          <xsl:when test="@type='0'">
            <input type="checkbox" style="font-size:16px;font-family: Tohoma" name="flows">
              <xsl:attribute name="value">
                <xsl:value-of select="current()"/>
              </xsl:attribute>
              <xsl:attribute name="onclick">
                OnFlowChangeState('<xsl:value-of select="current()"/>','0',this)
              </xsl:attribute>
            </input>
          </xsl:when>
          <xsl:when test="@type='1'">
            <input type="checkbox" style="font-size:16px;font-family: Tohoma;" name="stages">
              <xsl:attribute name="value">
                <xsl:value-of select="current()"/>
              </xsl:attribute>
              <xsl:attribute name="onclick">
                OnFlowChangeState('<xsl:value-of select="current()"/>','1',this)
              </xsl:attribute>
            </input>
          </xsl:when>
        </xsl:choose>
      </td>
      <td bgcolor="white" align="center" style="font-size:16px;font-family: Tohoma;">
        <xsl:value-of select="current()"/>
      </td>
      <td bgcolor="white" align="center" onwrap="true" width="48%">
        <div style="display:none;font-size:16px;font-family: Tohoma;" align="left">
          <xsl:attribute name="id">Div_<xsl:value-of select="current()"/>
          </xsl:attribute>
        </div>
      </td>
    </tr>
  </xsl:template>

  <xsl:template match="flow" mode="SINGLE">
    <tr>
      <td bgcolor="white" width="25">
        <xsl:choose>
          <xsl:when test="@type='0'">
            <input type="radio" style="font-size:16px;font-family: Tohoma" name="flows">
              <xsl:attribute name="value">
                <xsl:value-of select="current()"/>
              </xsl:attribute>
              <xsl:attribute name="onclick">
                OnFlowChangeState('<xsl:value-of select="current()"/>','0',this)
              </xsl:attribute>
            </input>
          </xsl:when>
          <xsl:when test="@type='1'">
            <input type="radio" style="font-size:16px;font-family: Tohoma;" name="stages">
              <xsl:attribute name="value">
                <xsl:value-of select="current()"/>
              </xsl:attribute>
              <xsl:attribute name="onclick">
                OnFlowChangeState('<xsl:value-of select="current()"/>','1',this)
              </xsl:attribute>
            </input>
          </xsl:when>
        </xsl:choose>
      </td>
      <td bgcolor="white" align="center" style="font-size:16px;font-family: Tohoma;">
        <xsl:value-of select="current()"/>
      </td>
      <td bgcolor="white" align="center"  width="48%">
        <div style="display:none;font-size:16px;font-family: Tohoma;" align="left">
          <xsl:attribute name="id">Div_<xsl:value-of select="current()"/>
          </xsl:attribute>
        </div>
      </td>
    </tr>
  </xsl:template>
</xsl:stylesheet>

