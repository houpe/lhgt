using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using WF_Business;
using Business.FlowOperation;

/// <summary>
/// Summary description for postsubmit.
/// </summary>
public partial class Office_TaskPostSubmit : System.Web.UI.Page
{
    RequestFlowOperation rfo = new RequestFlowOperation();
    protected void Page_Load(object sender, System.EventArgs e)
    {
        //Put user code to initialize the page here
        if (!IsPostBack)
        {
            InstanceSubmit();
        }
    }

    //所选的流向列表
    private string[] flows
    {
        get { return Request.Params.GetValues("flows"); }
    }
    //所选结点列表
    private string[] stages
    {
        get { return Request.Params.GetValues("stages"); }
    }

    //获取提交流向           
    private string fName
    {
        get { return (flows == null) ? Request["steps"] : flows[0]; }
    }

    //获取案件ID
    private long iid
    {
        get { return Int64.Parse(Request["iid"]); }
    }
    //获取流程ID           
    private string wid
    {
        get
        {
            string strwid = rfo.GetWidFromStinstance(" and iid= " + iid.ToString());
            return strwid;
        }
    }
    //获取岗位ID
    private long wiid
    {
        get { return Int64.Parse(Request["workitem"]); }
    }

    public void InstanceSubmit()
    {
        //案件提交部分
        bool Success = true;
        string msg = string.Empty;
        WorkItem workItem = new WorkItem();
        ArrayList arrList = new ArrayList();
        string strFlow = string.Empty;

        //添加提交流向
        if (flows != null)
        {
            for (int i = 0; i < flows.Length; i++)
            {
                FlowUserStruct fus = new FlowUserStruct();
                fus.IsRound = Request["ckbRound"] != null;
                fus.FlowName = flows[i];
                fus.Users = Request.Params.GetValues("User_" + flows[i]); //为null时为岗位所有用户
                arrList.Add(fus);
                strFlow = flows[i];
            }
        }

        //添加提交结点
        if (stages != null)
        {
            for (int i = 0; i < stages.Length; i++)
            {
                string[] users = Request.Params.GetValues("User_" + stages[i]);
                if (users != null)
                {
                    if (users.Length > 0)//为null时无效
                    {
                        FlowUserStruct fus = new FlowUserStruct();
                        fus.IsRound = Request["ckbRound"] != null;
                        fus.FlowName = fName;
                        fus.StepName = stages[i];
                        fus.Users = users;
                        arrList.Add(fus);
                    }
                }
            }
        }

        if (flows == null && stages == null & !string.IsNullOrEmpty(fName))
        {
            FlowUserStruct fus = new FlowUserStruct();
            fus.IsRound = false;
            fus.FlowName = fName;
            arrList.Add(fus);
        }

        if (arrList.Count > 0)
        {
            FlowUserStruct[] arrFus = new FlowUserStruct[arrList.Count];
            for (int i = 0; i < arrList.Count; i++)
            {
                arrFus[i] = (FlowUserStruct)arrList[i];
            }

            Success = workItem.Sumbit(iid, wiid, arrFus, ref msg);
        }
        else
        {
            Success = false;
            msg = "没有选择提交流向";
        }

        if (Success == false)
        {
            Response.Write("<script language=javascript>alert('提交没有成功，原因是：" + msg + "');window.close();</script>");
            Response.End();
        }
        else
        {
            //设置交换位                
            //rfo.UpdateChangeFlag("1", iid.ToString());

            if (strFlow == "提交归档")
            {
                rfo.UpdateArchiveFlag("3", iid.ToString());
            }
        }
    }

    /// <summary>
    /// 判断是否是有效的iid
    /// </summary>
    /// <param name="strIid">有效的iid</param>
    /// <returns></returns>
    public bool IsValideIid(string strIid)
    {
        bool bReturn = false;
        string strFirstNum = strIid.Substring(0, 1);
        if (strFirstNum == "2")
        {
            bReturn = true;
        }

        return bReturn;
    }

}