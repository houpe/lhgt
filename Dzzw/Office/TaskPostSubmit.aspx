<%@ Page Language="c#" AutoEventWireup="true" Inherits="Office_TaskPostSubmit" CodeFile="TaskPostSubmit.aspx.cs" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head runat="server">
    <title>业务提交</title>

    <script language="javascript" type="text/javascript">
        function openerback() {
            window.close();
            //window.parent.closeWindow();

            //更新弹出框
            var strUrl = opener.opener.location.href;
            if (strUrl.indexOf("?") > 0) {
                strUrl += "&rdm=" + Math.random();
            }
            else {
                strUrl += "?rdm=" + Math.random();
            }
            opener.opener.location.href = strUrl;
            window.opener.close();
            
        }
    </script>

</head>
<body onload="openerback()">
</body>
</html>
