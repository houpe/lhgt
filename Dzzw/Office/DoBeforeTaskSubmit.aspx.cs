﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WF_Business;


public partial class Office_DoBeforeTaskSubmit : System.Web.UI.Page
{
    /// <summary>
    /// 节点名称。
    /// </summary>
    public string step
    {
        get
        {
            return Request["step"] == null ? string.Empty : Request["step"].ToString();
        }
    }

    /// <summary>
    /// 节点ID。
    /// </summary>
    public decimal wiid
    {
        get
        {
            if (Request["ItemId"] == null)
                return decimal.Zero;
            else
                return Convert.ToDecimal(Request["ItemId"]);
        }
    }

    /// <summary>
    /// 节点ctlid
    /// </summary>
    public string ctlid
    {
        get
        {
            return Request["ctlid"] == null ? string.Empty : Request["ctlid"].ToString();
        }
    }

    /// <summary>
    /// 实例ID。
    /// </summary>
    public decimal iid
    {
        get
        {
            if (Request["iid"] == null)
                return decimal.Zero;
            else
                return Convert.ToDecimal(Request["iid"]);
        }
    }

    /// <summary>
    /// 流程ID
    /// </summary>
    public string wid
    {
        get
        {
            return WorkFlowResource.GetWidByIID(iid.ToString());
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }
}
