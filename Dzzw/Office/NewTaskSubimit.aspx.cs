﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WF_Business;

public partial class NewTaskSubimit : System.Web.UI.Page
{
    /// <summary>
    /// 节点名称。
    /// </summary>
    public string step
    {
        get
        {
            return Request["step"] == null ? string.Empty : Request["step"].ToString();
        }
    }

    /// <summary>
    /// 节点ID。
    /// </summary>
    public long wiid
    {
        get
        {
            if (Request["ItemId"] == null)
                return 0;
            else
                return Convert.ToInt64(Request["ItemId"]);
        }
    }

    /// <summary>
    /// 节点ctlid
    /// </summary>
    public string ctlid
    {
        get
        {
            return Request["ctlid"] == null ? string.Empty : Request["ctlid"].ToString();
        }
    }

    /// <summary>
    /// 实例ID。
    /// </summary>
    public long iid
    {
        get
        {
            if (Request["iid"] == null)
                return 0;
            else
                return Convert.ToInt64(Request["iid"]);
        }
    }

    /// <summary>
    /// 流程ID
    /// </summary>
    public string wid
    {
        get
        {
            return WorkFlowResource.GetWidByIID(iid.ToString());
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
    }


    
}