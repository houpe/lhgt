﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TaskProcess.aspx.cs" Inherits="Office_TaskProcess" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>业务办公区</title>
    <link rel="stylesheet" type="text/css" href="../css/default/easyui.css" />
    <link rel="stylesheet" type="text/css" href="../css/icon.css" />
    <link href="../css/page.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../ScriptFile/jquery.js"></script>

    <script type="text/javascript" src="../ScriptFile/jquery.easyui.min.js"></script>

    <script type="text/javascript" src="../ScriptFile/Common/Jquery.Global.js"></script>
</head>

<script type="text/javascript">
    var isChange = false; //是否修改了数据，修改了则需要提示保存（注意，不能修改此变量的名称）
    var showControl = false; //是否打开了控件
    var type = null;   //正在显示的窗体类型，用于切换时获知原来的类型
    var formname = null; //正在显示的窗体的名称，用于打印切换时	
    var inputIndex = "0";

    function ReturnTask() {
        var nLeft = (screen.width - 490) / 2;
        var nTop = (screen.height - 320) / 2;
        window.open("DoBeforeTaskUntread.aspx?action=stop&ItemId=<%=strWiid%>&iid=<%=strInstanceId %>", "submit", "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,width=490,height=320,left=" + nLeft + ",top=" + nTop, true);
    }

    function Attach() {
        var nLeft = (screen.width - 625) / 2;
        var nTop = (screen.height - 400) / 2;
        window.open("../UserControls/file.aspx?iid=<%=strInstanceId %>&wiid=<%=strWiid%>&ctlid=<%=strStepCtlid %>", "submit", "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=auto,resizable=yes,width=625,height=500,left=" + nLeft + ",top=" + nTop, true);
    }
    function AttachManage() {
        var nLeft = (screen.width - 600) / 2;
        var nTop = (screen.height - 400) / 2;
        window.open("../SystemManager/DetailAttachment.aspx?ItemId=<%=strWiid%>&iid=<%=strInstanceId %>&step=<%=strStepName %>", "submit", "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,left=" + nLeft + ",top=" + nTop + ",height=500 , width=600", true);
    }

    function Save() {
        if (isChange == true || type == "11")//11为外部页面，但提供了SaveAll方法
        {
            FormView.SaveAll(true);

            if (type != '21')   //word文档永远为修改过
            {
                isChange = false;
            }
        }
        else
            alert("没有进行过修改，无须保存。");
    }

    //回退
    function ReturnSender() {
        if (showControl == false && type == 3 && isChange == true) {
            FormView.SaveAll();
            isChange = false;
        }

        var nLeft = (screen.width - 490) / 2;
        var nTop = (screen.height - 320) / 2;
        window.open("DoBeforeTaskUntread.aspx?ItemId=<%=strWiid%>&iid=<%=strInstanceId %>", "submit", "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,width=490,height=320,left=" + nLeft + ",top=" + nTop, true);
    }

    //提交
    function Submit() {
        if (showControl == false && type == 3 && isChange == true) {
            if (confirm("你已修改了表单数据，是否先保存？")) {
                FormView.SaveAll(false, "OpenSubmit"); //保存后调用OpenSumbit打开提交窗口提交。	
                isChange = false;
            }
            else {
                OpenSubmit();
            }
        }
        else {
            OpenSubmit();
        }
    }

    //打开提交页面
    function SelectNextFlow(result) {
        if (result != null ) {
            if (result == '') {
                var nLeft = (screen.width - 490) / 2;
                var nTop = (screen.height - 320) / 2;
                window.open("NewTaskSubimit.aspx?ItemId=<%=strWiid%>&iid=<%=strInstanceId %>&ctlid=<%=strStepCtlid %>&step=<%=strStepName %>", "submit", "toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=no,width=490,height=320,left=" + nLeft + ",top=" + nTop, true);
//                window.location.href = "DoBeforeTaskSubmit.aspx?ItemId=<%=strWiid%>&iid=<%=strInstanceId %>&step=<%=strStepName %>&ctlid=<%=strStepCtlid %>";
            }
            else
                alert(result);
        }
    }

    //校验是否存在必填字段未填
    function OpenSubmit() {
        //查看必填项是否已经填充,保存并刷新，重新加载数据
        WebService(window.location.protocol + "//" + window.location.host + '<%=strServicePath %>/CheckData', SelectNextFlow, '{"iid":"<%=strInstanceId%>","step":"<%=strStepCtlid%>","userId":"<%=UserId%>"}');
    }

    function Print(act) {
        if (type == 3)//表单
        {
            if (showControl == false)//打开了控件，先关掉	  
            {
                var url = window.location.protocol + "//" + window.location.host + "<%=strPageViewPath%>&FormName=" + formname + "&input_index=" + inputIndex + "&act=" + act;
                window.open(url, '_blank', "height=" + screen.availHeight + " , width=" + screen.availWidth + ", top=0, left=0,toolbar=no, menubar=no, scrollbars=no, location=n o, status=no");
            }
        }
        else if (type == 11 || type == 12)//外部页面，但支持打印的
        {
            var url = formname.replace("STEP=", "NORIGHT="); //把节点名称的参数替掉，使打印时没权限不显示新增之类的按钮
            window.open(url + "&PrintAct=" + act, '_blank', "height=" + screen.availHeight + " , width=" + screen.availWidth + ", top=0, left=0,toolbar=no, menubar=no, scrollbars=no, location=n o, status=no");
        }
        else if (vtype == 2) {
            isChange == false;
            if (showControl == false) {
                FormView.document.location = "ViewAttachment.aspx?iid=<%=strInstanceId%>&path=" + vname;

            }
            else {
                FormView.OpenResource(vname);
            }
        }
        if (showControl == true)
            FormView.Print();
    }

    function ViewWorkflow() {
        //采用新方法展示流程图
        var url = window.location.protocol + "//" + window.location.host + "<%=strFlowViewPath%>?iid=<%=strInstanceId%>";
        FormView.document.location = url;
    }

    //未打开过控件时切换到控件
    function SwichControl(act, tname) {
        var url = "doworkitem.aspx?WIID=<%=strWiid%>&IID=<%=strInstanceId%>&UserId=<%=UserId %>&act=" + act;
        if (tname != "")
            url = url + "&formname=" + encodeURIComponent(tname);
        //如果前面打开的是表单，则先保存
        if (type == 3 && isChange == true)//表单（type==1 URL后一步考虑）
        {
            FormView.SaveAll(false);
        }
        isChange = true;
        FormView.document.location = url;
        showControl = true;
    }

    function CloseWindow() {
        window.close();
    }

    function Zoomin() {
        FormView.Zoomin();
    }

    function Zoomout() {
        FormView.Zoomout();
    }

    function BeforeUnLoad() {
        if (showControl == true)//打开了控件，关掉
        {
            FormView.Close();
            showControl = false;
        }
    }
    //打开窗体
    function OpenResource(vname, vtype, input_index, rtid, print) {
        if (print == false)//非打印状态
        {
            if (vtype == 3)//表单
            {
                BeforeUnLoad()

                if (type == 3 && isChange == true)//原来是表单，先保存
                {
                    FormView.SaveAll(false);
                    isChange == false;
                }

                var url = window.location.protocol + "//" + window.location.host + "<%=strPageViewPath%>&FormName=" + vname + "&rtid=" + rtid + "&input_index=" + input_index;
                FormView.document.location = url;
            }
            else if (vtype == 1 || vtype == 11 || vtype == 12)//Url
            {
                if (showControl == true)//打开了控件，先关掉
                    FormView.Close();
                else if (type == 3 && isChange == true)//原来是表单，先保存
                {
                    FormView.SaveAll();
                    isChange == false;
                }
                showControl = false;
                vname = vname.replace("{IID}", "IID=<%=strInstanceId%>");
                vname = vname.replace("{SID}", "SID=<%=UserId%>");
                vname = vname.replace("{WIID}", "WIID=<%=strWiid%>");
                vname = vname.replace("{STEP}", "STEP=<%=strStepName%>");
                FormView.document.location = vname;
            }
            else if (vtype == 2) {
                if (showControl == false) {
                    FormView.document.location = "ViewAttachment.aspx?iid=<%=strInstanceId%>&path=" + vname;
                }
                else
                    FormView.OpenResource(vname);
            }
            else if (vtype == 21) {
                SwichControl("ViewAttachment", vname)
                showControl = false;
            }
        }
        formname = vname;
        inputIndex = input_index;
        type = vtype;
    }

    //刷新
    function PageRefresh() {
        WebService(window.location.protocol + "//" + window.location.host + '<%=strServicePath%>/ExecuteCheckWhenOpen', RefreshCurrentPage, '{"iid":"<%=strInstanceId%>","wiid":"<%=strWiid%>","step":"<%=strStepCtlid%>","userId":"<%=UserId%>"}');
    }
    //刷新返回结果
    function RefreshCurrentPage(result) {
        if (result != "") {
            alert(result);
        }
        FormView.document.location.href = FormView.document.location.href;
    }
</script>

<body class="easyui-layout" id="divBody" style="width:100%; height:100%">
    <div id="divNorth" region="north" split="false" style="height: 47px; margin: 0px;
        padding: 0px;background-color:#C1DBFE">
        <table style="text-align:center; " >
            <tr id="menu_tr">
                <td width="50" class="showCursor" onclick="ViewWorkflow()">
                    <img alt="" src="../images/top_ico_01.gif" width="22" height="22" /><br />
                    进度视图
                </td>
                <td width="20">
                    <img alt="" src="../images/top_img.gif" width="2" height="36" />
                </td>
                <td width="35" class="showCursor" onclick="Save()">
                    <img alt="" src="../images/top_ico_04.gif" width="22" height="22" /><br />
                    保存
                </td>
                <td width="20">
                    <img alt="" src="../images/top_img.gif" width="2" height="36" />
                </td>
                <td width="35" class="showCursor" onclick="Submit()">
                    <img alt="" src="../images/top_ico_03.gif" width="22" height="22" /><br />
                    提交
                </td>
                <%
                    if (bBack)
                    {
                %>
                <td width="35" class="showCursor" onclick="ReturnSender()">
                    <img alt="" src="../images/top_ico_15.gif" width="22" height="22" /><br />
                    回退
                </td>
                <%
                    }
                %>
                <td width="20">
                    <img alt="" src="../images/top_img.gif" width="2" height="36" />
                </td>
                <%
                                
                    if (bStop)
                    {   
                %>
                <td width="35" class="showCursor" onclick="ReturnTask()">
                    <img alt="" src="../images/top_ico_15.gif" width="22" height="22" /><br />
                    中止
                </td>
                <td width="20">
                    <img alt="" src="../images/top_img.gif" width="2" height="36" />
                </td>
                <%
                    }                            
                %>
                <td width="35" class="showCursor" onclick="Attach()">
                    <img alt="" src="../images/top_ico_11.gif" width="22" height="22" /><br />
                    附件
                </td>
                <td width="55" class="showCursor" onclick="AttachManage()">
                    <img alt="" src="../images/top_ico_11.gif" width="22" height="22" /><br />
                    附件管理
                </td>
                <td width="20">
                    <img alt="" src="../images/top_img.gif" width="2" height="36" />
                </td>
                <td width="35" class="showCursor" onclick="Print('Print')">
                    <img alt="打印" src="../images/top_ico_05.gif" width="22" height="22" /><br />
                    打印
                </td>
                <td width="55" class="showCursor" onclick="Print('PrintView')">
                    <img alt="打印预览" src="../images/top_ico_05.gif" width="22" height="22" /><br />
                    打印预览
                </td>
                <td width="35" class="showCursor" onclick="Print('Print2')">
                    <img alt="套打" src="../images/top_ico_06.gif" width="22" height="22" /><br />
                    套打
                </td>
                <td width="55" class="showCursor" onclick="Print('Print2View')">
                    <img alt="套打预览" src="../images/top_ico_06.gif" width="22" height="22" /><br />
                    套打预览
                </td>
                <td width="20">
                    <img alt="" src="../images/top_img.gif" width="2" height="36" />
                </td>
                <td width="35" class="showCursor" onclick="Zoomin()">
                    <img alt="放大" src="../images/top_ico_09.gif" width="22" height="22" title="仅应用于图片" /><br />
                    放大
                </td>
                <td width="35" class="showCursor" onclick="Zoomout()">
                    <img alt="缩小" src="../images/top_ico_08.gif" width="22" height="22" title="仅应用于图片" /><br />
                    缩小
                </td>
                <td width="20">
                    <img alt="" src="../images/top_img.gif" width="2" height="36" />
                </td>
                <td width="35" onclick="PageRefresh()">
                    <img alt="刷新" src="../images/Map/shuanxin.ico" width="22" height="22" /><br />
                    刷新
                </td>
                <td width="35" class="showCursor" onclick="CloseWindow()">
                    <img alt="关闭" src="../images/top_ico_07.gif" width="22" height="22" /><br />
                    关闭
                </td>
              
            </tr>
        </table>
    </div>
    <div id="divwest" region="west" title='<%= strStepName%>' split="true" style="width: 200px;">
        <iframe src="TaskProcessingresource.aspx?ItemId=<%=strWiid%>&iid=<%=strInstanceId %>&step=<%=strStepCtlid %> " id="ResourceView" name="ResourceView" scrolling="auto" width="100%" height="100%"  frameboder="0" resize="true"></iframe>
    </div>
    <div region="center" title="业务办公区" style="overflow: hidden;" id="divCenter">
        <iframe src="" id="FormView" name="FormView" scrolling="auto" width="100%" height="100%"
            style="border: 0px" resize="true"></iframe>
    </div>
</body>
</html>
