﻿<%@ WebHandler Language="C#" Class="ProcessAttachment" %>

using System;
using System.Web;
using Business;

public class ProcessAttachment : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {

        if (!string.IsNullOrEmpty(context.Request["aid"]))
        {
            string strSql = string.Format(@"select a.ext_name,a.data from st_attachment a where aid='{0}'",
                       context.Request["aid"]);

            System.Data.DataTable dtStoreInfo = null;
            WF_Business.SysParams.OAConnection().RunSql(strSql, out dtStoreInfo);
            
            if (dtStoreInfo.Rows.Count > 0)
            {
                context.Response.ClearContent();
                context.Response.ContentType = string.Format("image/{0}", dtStoreInfo.Rows[0][0]);
                byte[] bArr = dtStoreInfo.Rows[0][1] as Byte[];
                context.Response.BinaryWrite(bArr);
            }
        }
        else
        {
            context.Response.ContentType = "image/Gif";
            context.Response.Write("Hello World");
        }
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}