<%@ Page Language="c#" Inherits="Office_DoBeforeTaskUntread" CodeFile="DoBeforeTaskUntread.aspx.cs" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head runat="server">
    <title>案件回退</title>
    <link href="../css/page.css" rel="stylesheet" type="text/css" />
    <script language="javascript">
        function openerback() {
            window.close();
            //window.parent.closeWindow();

            //更新弹出框
            var strUrl = opener.opener.location.href;
            if (strUrl.indexOf("?") > 0) {
                strUrl += "&rdm=" + Math.random();
            }
            else {
                strUrl += "?rdm=" + Math.random();
            }
            opener.opener.location.href = strUrl;
            window.opener.close();

        }
    </script>
</head>
<body topmargin="0" leftmargin="0">
    <form runat="server">
    <table border="0" cellspacing="0" cellpadding="0" width="100%" height="100%">
        <tr>
            <td height="30" align="center">
                <asp:Label runat="server" ID="labTitle"></asp:Label>
            </td>
        </tr>
        <tr>
            <td width="100%">
                <asp:TextBox TextMode="MultiLine" ID="txtRemark" runat="server" Width="100%" Height="200"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td height="30" align="center">
                <asp:Button ID="btnOk" runat="server" Text="确定" OnClick="btnOk_Click" />
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
