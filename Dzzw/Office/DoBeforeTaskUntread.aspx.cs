using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using WF_Business;
using Business.FlowOperation;

/// <summary>
/// Summary description for dobeforeuntread.
/// </summary>
public partial class Office_DoBeforeTaskUntread : Page
{
    
    protected void Page_Load(object sender, System.EventArgs e)
    {
        if (!IsPostBack)
        {
            string action = Request["action"];
            labTitle.Text = "回退意见";
            this.Page.Title = "案件回退";
            if (!String.IsNullOrEmpty(action))
            {
                labTitle.Text = "终止意见";
                this.Page.Title = "案件中止";
            }
            
        }
    }

    /// <summary>
    /// 判断是否是有效的iid
    /// </summary>
    /// <param name="strIid">有效的iid</param>
    /// <returns></returns>
    public bool IsValideIid(string strIid)
    {
        bool bReturn = false;
        string strFirstNum = strIid.Substring(0, 1);
        if (strFirstNum == "2")
        {
            bReturn = true;
        }

        return bReturn;
    }

    protected void btnOk_Click(object sender, EventArgs e)
    {
        System.String strRemark = txtRemark.Text;

        if (!string.IsNullOrEmpty(strRemark))
        {
            System.Int64 workItemID = System.Int64.Parse(Request["ItemId"]);
            string iid = Request["iid"];
            WorkItem workItem = new WorkItem();
            RequestFlowOperation rfo = new RequestFlowOperation();

            if (Request["action"] == "stop")//中止
            {
                bool bSuccess = workItem.Stop(long.Parse(iid), workItemID, strRemark);

                if (bSuccess)
                {
                    rfo.UpdateStatusFlag(iid, "-2");
                    rfo.UpdateArchiveFlag("-1", iid);
                    rfo.UpdateChangeFlag("1", strRemark, iid);
                    rfo.SetMessage(iid, "您的业务申报在行政审批过程中被终止");

                    Page.ClientScript.RegisterStartupScript(this.GetType(), "refreshPage", "<script>openerback();</script>");
                }
            }
            else//回退
            {
                bool bSuccess = workItem.Untread(workItemID, strRemark);
                if (bSuccess)
                {
                    rfo.UpdateChangeFlag("1", iid);
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "refreshPage", "<script>openerback();</script>");
                }
            }
        }
    }
}