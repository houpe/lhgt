<%@ Page Language="c#" Inherits="Office_doworkitem" CodeFile="doworkitem.aspx.cs" %>

<%
    Common.ConfigHelper config = new Common.ConfigHelper(true);
    String ResServerPath = Business.SystemConfig.NetoWsUrl + "?WSDL"; //加载NetoWs的路径信息

    String strWIID = Request["WIID"];
    String strIID = Request["IID"];
    if (String.IsNullOrEmpty(UserId))
    {
        Session["UserId"] = Request["UserId"];
    }

    string strSessionId = Business.Admin.StUserOperation.GetSessionByUserId(UserId);
    if (String.IsNullOrEmpty(strSessionId))
    {
        Response.Write("<script language=javascript>alert('用户Session为空无效，无法打开业务，请与管理员联系解决！');</script>");
        Response.End();
    }
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head runat="server">
    <title></title>
</head>
<body leftmargin="0" topmargin="0" onload="Init()">

    <script type="text/javascript">
        var formname = "<%=Request["formname"] %>";
        function Init() {
            var url = window.location.protocol + "//" + window.location.host + "<%=ResServerPath%>";
			    WorkFlowOle.init(url, "<%=strSessionId%>", "<%=strWIID%>", "<%=strIID%>");
			}

			function CheckNecessaryFieldIsFill() {
			    return WorkFlowOle.CheckNecessaryFieldIsFill();
			}

			function AttachResource() {
			    WorkFlowOle.add_attachment();
			}

			function DeleteResource() {
			    WorkFlowOle.del_attachment("VOID");
			}

			function SaveAll() {
			    WorkFlowOle.save_all();
			}

			function OpenResource(name) {
			    WorkFlowOle.open_resource(name);
			}

			function RemoveAttach(name) {
			    WorkFlowOle.del_attachment(name);
			}

			function Print() {
			    WorkFlowOle.Print(0);
			}

			function Print2() {
			    WorkFlowOle.Print(1);
			}

			function Zoomin() {
			    WorkFlowOle.zoomin();
			}

			function Zoomout() {
			    WorkFlowOle.zoomout();
			}

			function ViewWorkflow() {
			    WorkFlowOle.switchview(1);
			}

			function Close() {
			    WorkFlowOle.close();
			}

    </script>

    <script for="WorkFlowOle" event="OnAfterInit()" type="text/javascript">
        //控件初始化完毕后触发,根据要求做的不同事情来完成不同操作
        var act = "<%=Request["act"]%>";
        if (act == "ViewWorkflow") {
            ViewWorkflow();
        }
        else if (act == "ViewAttachment") {
            OpenResource(formname);
        }
        else if (act == "AttachResource") {
            AttachResource();
        }
        else if (act == "DeleteResource") {
            DeleteResource();
        }
        else if (act == "Print") {
            OpenResource(formname);
            WorkFlowOle.Print(0);
        }
        else if (act == "Print2") {
            OpenResource(formname);
            WorkFlowOle.Print(1);
        }
        //parent.ResourceView.OpenFirst();
    </script>

    <script for="WorkFlowOle" event="OnResourceChange()" type="text/javascript">
        //资源发生改变时触发
        parent.ResourceView.location.reload(true);
    </script>

    <script for="WorkFlowOle" event="ViewResourceChanged(ResourceName)" type="text/javascript">
        //资源发生改变时触发
    </script>

    <script for="WorkFlowOle" event="StepClick(StepName)" type="text/javascript">
        alert(StepName);
    </script>

    <div align="center">
        <script language="javascript" src="../ScriptFile/ClientOle.js"></script>

    </div>
</body>
</html>
