<%@ Page Language="c#" AutoEventWireup="true" Inherits="Office_TaskPostSubmit" CodeFile="TaskPostSubmitBatch.aspx.cs" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head runat="server">
    <title>提交或归档页面</title>
    <script type="text/javascript">
        function openerback() {
            //更新弹出框
            if (typeof (opener.location) != "undefined") {
                if (opener.location != null) {
                    var strUrl = opener.location.href;
                    if (strUrl.indexOf("?") > 0) {
                        strUrl += "&rdm=" + Math.random();
                    }
                    else {
                        strUrl += "?rdm=" + Math.random();
                    }
                    opener.location.href = strUrl;
                }
            }
            window.close();
        }
    </script>
</head>
<body onload="openerback()">
</body>
</html>
