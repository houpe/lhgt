﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ViewAttachment.aspx.cs" Inherits="office_ViewAttachment" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>查看附件</title>

    <script type="text/javascript">
    function changeSize(args)
    {
        var oImg = document.getElementById("ImageView1");
        oImg.style.zoom = parseInt(oImg.style.zoom)+(args?+5:-5)+'%';
    }
    
    function Zoomin()
	{	
	    changeSize(true);
	}
	
	function Zoomout()
	{
	    changeSize(false);
	}		
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div>
            <img src="" id="ImageView1" visible="false" runat="server" />
        </div>

        <script type="text/javascript">
        var imageTemp = document.getElementById("ImageView1");
        //添加自适应的功能
        if(imageTemp.offsetWidth>document.body.scrollWidth)
        {
            imageTemp.style.width = document.body.scrollWidth;
        }
        if(imageTemp.offsetHeight>document.body.scrollHeight)
        {
            imageTemp.style.height=document.body.scrollHeight;
        }
        </script>

    </form>
</body>
</html>
