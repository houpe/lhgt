﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TaskProcessingresource.aspx.cs"
    Inherits="Office_TaskProcessingresource" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>树形导航</title>
    <link rel="stylesheet" type="text/css" href="../css/dtree.css" />

    <script type="text/javascript" src="../ScriptFile/dtree.js"></script>

    <script type="text/javascript">
        var bInit = false;

        function OpenFirst() {
            //if (typeof (dtree1) != "undefined") {
            dtree1.openTo(txtFirstNode.value, true);
            //}
        }
        function OpenResource(name, type, input_index, rtid) {
            parent.OpenResource(name, type, input_index, rtid, false);
        }
        function Init() {
            OpenFirst();
        }
        function ShowMenu(path, e) {
            var hiddenValue = document.getElementById("HiddenValue");
            var divMenu = document.getElementById("divMenu");
            divMenu.style.top = e.clientY + 10;
            divMenu.style.left = e.clientX - 5;
            divMenu.style.display = '';

            hiddenValue.value = path;
        }
        function HidMenu() {
            var divMenu = document.getElementById("divMenu");
            divMenu.style.display = 'none';
        }		    
    </script>

</head>
<body scroll="auto" topmargin="5" leftmargin="3" onload="Init()">
    <input type="hidden" id="txtValue" value="" />
    <div>
        <%=GetTreeHTML()%>
    </div>

</body>
</html>
