﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="viewworkflow.aspx.cs" Inherits="QueryAndCollect_viewworkflow" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head runat="server">
    <title>流程图</title>
    <link href="../css/page.css" rel="stylesheet" type="text/css" />
</head>
<script type="text/javascript">

    function Init() {
        var url = window.location.protocol + "//" + window.location.host + '<%=ServerPath %>' + "/HtmlPage/ViewWorkflowProcess.aspx?iid=<%=strIid%>";
        document.getElementById("imgFlow").src = url;
    }

    function ViewInstance(itemId, Instance) {
        var strURL = "TaskProcessView.aspx?ItemId=" + itemId + "&iid=" + Instance;
        window.open(strURL, 'viewer', "height=" + (screen.availHeight - 30) + " , width=" + screen.availWidth + ", top=0, left=0,toolbar=no, menubar=no, scrollbars=no, location=no, status=no");
    }
</script>
<body leftmargin="0" topmargin="0" onload="Init()">
    <table class="tab_top" width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td width="3%" height="30">
                <img src="../images/tab_top_left.gif" width="17" height="35" />
            </td>
            <td>
                <div class="tab_top_text2" style="cursor: hand;" onclick="javascript:history.go(-1)">
                    详细信息</div>
                <div class="tab_top_text3">
                    流程明细</div>
            </td>
            <td align="right">
                <input name="Submit3" type="button" class="NewButton" value="案件信息" onclick="ViewInstance(<%=itemId%>,<%=strIid%>)" />
                <input name="Submit1" type="button" class="NewButton" value="返回" onclick="javascript:history.go(-1)" />
            </td>
            <td width="1%" align="right">
                <img src="../images/tab_top_right.gif" width="5" height="35" />
            </td>
        </tr>
    </table>
    <div style="margin-top: 0px">
        <img id="imgFlow" src="" /></div>
</body>
</html>
