using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;

public partial class QueryAndCollect_TaskQuery : PageBase
{
    protected string strhtml = string.Empty;
    protected void Page_Load(object sender, System.EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["UserId"] != null)
            {
                Business.FlowOperation.ClsUserWorkFlow clsWorkFlow = new Business.FlowOperation.ClsUserWorkFlow();
                System.Data.DataTable dtFlowName = clsWorkFlow.GetFlowNameByUser(Session["UserId"].ToString());

                foreach (System.Data.DataRow drTemp in dtFlowName.Rows)
                {
                    string strWorkFlowName = drTemp["wname"].ToString();

                    strhtml += "<option value=" + strWorkFlowName + ">" + strWorkFlowName + "</option>";
                }
            }
        }
    }
}