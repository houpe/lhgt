﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Car_Fee_Collect.aspx.cs"
    Inherits="QueryAndCollect_Car_Fee_Collect" %>

<%@ Register Src="../UserControls/Calendar.ascx" TagName="Calendar" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>车辆费用统计表</title>
    <link href="../css/page.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div style="text-align: center">
        <br />
        <table style="width: 100%;" cellpadding="0px" cellspacing="0" class="tabTop">
            <tr style="height: 35px">
                <td style="text-align: left;">
                    &nbsp;&nbsp;发送日期：
                    <uc2:Calendar ID="calStart" runat="server" Width="120px" />
                    至
                    <uc2:Calendar ID="calEnd" runat="server" Width="120px" />
                    <asp:Button ID="btnQuery" runat="server" Text="查询" CssClass="NewButton" OnClick="btnQuery_Click" />
                </td>
            </tr>
        </table>
        <br />
        <controlDefine:CustomGridView ID="CustomGridView1" runat="server" AllowPaging="True"
            AutoGenerateColumns="false" ShowItemNumber="true" CheckboxAutoPostBack="False"
            CustomImportToExcel="false" CustomSortExpression="" DeleteToolTipOfCol="-1" HideTextLength="20"
            ShowCmpAndCancel="False" ShowGridViewSelect="List" ShowHideColModel="None" ShowImportButton="False"
            ShowTreeView="False" SelectOrDelete="False" Width="100%" OnOnLoadData="CustomGridView1_OnLoadData"
            OnRowCreated="CustomGridView1_RowCreated" CssClass="window_tab_list" CustomGridViewClass="window_tab_list_all"
            DivFstClass-DivAllClass="window_tab_list_fst" DataTableClass-OnMouseover="#b5e0f3"
            DataTableClass-OnMouseout="#eff8fc" DivFstClass-DivButtonClass="button" DivFstClass-DivLabelClass="window_tab_list_fst_lbl"
            DivFstClass-DivInputClass="Input" DivScdClass-DivAllClass="window_tab_list_sed"
            DivScdClass-DivButtonClass="button" DivScdClass-DivInputClass="ShortInput" DivScdClass-DivLabelClass="window_tab_list_fst_lbl"
            DivScdClass-DragLayerClass="window_tab_list_scd_dray" DivTrdClass-DivButtonClass="button"
            AllowSortingAscImgCss="window_tab_list_Asc" AllowSortingDescImgCss="window_tab_list_desc"
            AlternatingRowStyle-BackColor="#E6F4FF" RowStyle-HorizontalAlign="Center" AllowOnMouseOverEvent="false"
            Font-Size="12px">
            <RowStyle BackColor="#ffffff" Height="28px" />
            <Columns>
                <asp:TemplateField HeaderText="车牌号">
                    <ItemTemplate>
                        <a href='#'>
                            <%#Eval("cph") %></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="username" HeaderText="驾驶员"></asp:BoundField>
                <asp:BoundField DataField="zgls" HeaderText="总公里数"></asp:BoundField>
                <asp:BoundField DataField="ctcs" HeaderText="长途次数"></asp:BoundField>
                <asp:BoundField DataField="ctgls" HeaderText="长途公里数"></asp:BoundField>
                <asp:BoundField DataField="wxje" HeaderText="维修费"></asp:BoundField>
                <asp:BoundField DataField="hyje" HeaderText="汽油费"></asp:BoundField>
                <asp:BoundField DataField="sjyh" HeaderText="油耗"></asp:BoundField>
                <asp:BoundField DataField="yhbz" HeaderText="油耗标准"></asp:BoundField>
                <asp:BoundField DataField="jybfb" HeaderText="节油百分比"></asp:BoundField>
                <asp:BoundField DataField="yxfyxj" HeaderText="总费用"></asp:BoundField>
            </Columns>
            <PagerSettings Visible="False" />
        </controlDefine:CustomGridView>
    </div>
    </form>
</body>
</html>
