<%@ Page Language="c#" Inherits="QueryAndCollect_TaskQueryInstance" CodeFile="TaskQueryInstance.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head runat="server">
    <title>流程业务列表</title>
    <link href="../css/page.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function ReturnFromComplete(strIID) {
            window.location.href = window.location.href + "&opera=-1&iid=" + strIID;
        }
    </script>
</head>
<body>
    <form id="Form1" runat="server">
        <div class="tab_top" width="100%" border="0" cellpadding="0" cellspacing="0">
            <div style="padding-left: 10px;float: left; padding-top: 10px;">
                <input type="button" class="NewButton" value="返回" onclick="javascript: history.go(-1)" />
            </div>
            <div style="padding-left: 10px; padding-top: 8px; width: 90%; text-align: center; float: left">
                办件查询结果
            </div>
        </div>
        <controlDefine:CustomGridView runat="server" ID="gvSerial" SkinID="List" AutoGenerateColumns="false"
            ShowItemNumber="true" OnOnLoadData="gvSerial_OnLoadData" ShowHideColModel="None"
            AllowPaging="true" OnRowCreated="gvSerial_RowCreated">
            <Columns>
                <asp:BoundField DataField="IID" HeaderText="编号" />
                <asp:BoundField DataField="业务类型" HeaderText="案件类型" />
                <asp:BoundField DataField="申请单位" HeaderText="申请人" />
                <asp:BoundField DataField="接件时间" HeaderText="接件时间" DataFormatString="{0:yyyy-MM-dd}"
                    HtmlEncode="false" />
                <asp:TemplateField HeaderText="优先级">
                    <ItemTemplate>
                        <img src="../images/<%#Getyou(Eval("优先级").ToString())%>" alt="" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="详细">
                    <ItemTemplate>
                        <a href="TaskQueryDetail.aspx?iid=<%#Eval("iid") %>&search=detail">详细</a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="办理状态">
                    <ItemTemplate>
                        <a href='#' onclick='ReturnFromComplete("<%# Eval("iid") %>")'>退回</a>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </controlDefine:CustomGridView>
        <input type="hidden" id="hidden1" name="hidden1" value="<%=searchWord%>" />
    </form>
</body>
</html>
