﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Business.Admin;
using Business;
using Business.Common;

public partial class QueryAndCollect_JieDaiCollectInOneDepart : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindData();
        }
    }

    private void BindData()
    {
        string strDateStart = Request["dtStart"];
        string strDateEnd = Request["dtEnd"];
        string strBm = Request["bm"];
        DataTable dtTable = JieDaiInfo.GetJieDaiDetailInfo(strDateStart, strDateEnd, strBm);

        List<string> lstFields = new List<string>();
        lstFields.Add("zxf");
        CommonOperationQuery.AddCollectRow(dtTable, "bm", lstFields);

        CustomGridView1.DataSource = dtTable;
        CustomGridView1.PageSize = SystemConfig.PageSize;
        CustomGridView1.RecordCount = dtTable.Rows.Count;
        CustomGridView1.DataBind();
    }

    protected void CustomGridView1_OnLoadData(object sender, EventArgs e)
    {
        BindData();
    }

    protected void CustomGridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        switch (e.Row.RowType)
        {
            case DataControlRowType.DataRow:
                DataRowView dtrv = e.Row.DataItem as DataRowView;
                if (dtrv == null)
                {
                    break;
                }
                break;
        }
    }

    /// <summary>
    /// 查询界面
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnQuery_Click(object sender, EventArgs e)
    {
        BindData();
    }
}