<%@ Page Language="c#" Inherits="QueryAndCollect_TaskQuery" CodeFile="TaskQuery.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head runat="server">
    <title>案件查询</title>
    <link rel="stylesheet" id="skinCss" type="text/css" href="../css/default/easyui.css" />
    <link rel="stylesheet" type="text/css" href="../css/icon.css" />
    <link href="../css/page.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../ScriptFile/jquery.js"></script>
    <script type="text/javascript" src="../ScriptFile/jquery.easyui.min.js"></script>
    <script src="../ScriptFile/easyui-lang-zh_CN.js" type="text/javascript"></script>
    <script type="text/javascript">
        function CheckRight() {
            var dropOption = document.getElementById("ywlx"); 
            if (dropOption.options.length == 1 && dropOption.options[0].text == "所有流程") {
                alert("您没有该权限！");
                return false;
            }
            else {
                var strUrl = "TaskQueryInstance.aspx?";
                strUrl += "startDate=" + $("#startDate").datebox('getValue');
                strUrl += "&endDate=" + $("#endDate").datebox('getValue');
                strUrl += "&ywlx=" + document.all.ywlx.value;
                strUrl += "&yxj=" + document.all.yxj.value;
                strUrl += "&sqdw=" + document.all.sqdw.value;

                strUrl += "&isComplete=";
                if ($("#isComplete").is(':checked')) {
                    strUrl += "on";
                }
                strUrl += "&notComplete=";
                if ($("#notComplete").is(':checked')) {
                    strUrl += "on";
                }
                strUrl += "&isCommand=" ;
                if ($("#isCommand").is(':checked')) {
                    strUrl += "on";
                }
                strUrl += "&notCommand=";
                if ($("#notCommand").is(':checked')) {
                    strUrl += "on";
                }
                strUrl += "&isStop=";
                if ($("#isStop").is(':checked')) {
                    strUrl += "on";
                }
                strUrl += "&notStop=";
                if ($("#notStop").is(':checked')) {
                    strUrl += "on";
                }
                strUrl += "&ywbh=" + document.all.ywbh.value;
                window.location.href = strUrl;
            }
        }
    </script>
    <style type="text/css">
        .style1 { width: 99px; }
    </style>
</head>
<body>
    <form name="queryForm" action="">
    <div style="width: 80%; margin: 0px auto; text-align: center; height: auto; padding-top: 20px;">
        <div class="box_top_bar" style="width: 100%">
            <div class="box_top_left">
                <div class="box_top_right">
                    <div class="box_top_text">
                        输入查询条件</div>
                </div>
            </div>
        </div>
        <div class="box_middle" style="width: 100%">
            <table width="89%" border="0" cellpadding="0" cellspacing="0" class="box_middle_tab">
                <tr>
                    <td class="style1">
                        业务类型：
                    </td>
                    <td>
                        <select name="ywlx" id="ywlx" style="width: 190px">
                            <option value="" selected="selected">所有流程</option>
                            <%=strhtml %>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="style1">
                        优先级：
                    </td>
                    <td>
                        <select name="yxj" id="yxj" style="width: 190px">
                            <option value="" selected="selected">全部</option>
                            <option value="0">普通</option>
                            <option value="1">加急</option>
                            <option value="2">特急</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="style1">
                        申请单位(个人)：
                    </td>
                    <td>
                        <input maxlength="100" size="23" name="sqdw" id="sqdw" value="" />
                    </td>
                </tr>
                <tr>
                    <td class="style1">
                        办件编号：
                    </td>
                    <td>
                        <input maxlength="100" size="23" name="ywbh" id="ywbh" value="" />
                    </td>
                </tr>
                <tr>
                    <td class="style1">
                        时间段：
                    </td>
                    <td>
                        自<input class="easyui-datebox" id="startDate" />
到
                        <input class="easyui-datebox" id="endDate"/>
                    </td>
                </tr>
                <tr>
                    <td class="style1">
                        是否办结：
                    </td>
                    <td>
                        <input type="checkbox" id="isComplete" checked="checked" />已办结
                        <input type="checkbox" id="notComplete" checked="checked" />未办结
                    </td>
                </tr>
                <tr>
                    <td class="style1">
                        是否经手：
                    </td>
                    <td>
                        <input type="checkbox" id="isCommand" checked="checked" />已经手
                        <input type="checkbox" id="notCommand" checked="checked" />未经手
                    </td>
                </tr>
                <tr>
                    <td class="style1">
                        是否终止：
                    </td>
                    <td>
                        <input type="checkbox" id="notStop" checked="checked" />未终止
                        <input type="checkbox" id="isStop" checked="checked" />已终止
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input name="Submit3" type="button" class="NewButton" value="提交" onclick="CheckRight()" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </form>
</body>
</html>
