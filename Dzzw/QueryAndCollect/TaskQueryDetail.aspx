<%@ Page Language="c#" Inherits="QueryAndCollect_TaskQueryDetail" CodeFile="TaskQueryDetail.aspx.cs" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head runat="server">
    <title>详细信息</title>
    <link href="../css/page.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <script type="text/javascript">
	function ViewInstanceFrame(itemId,Instance)
    {
	    var strURL = "TaskProcessView.aspx?ItemId="+ itemId+"&iid=" + Instance;
	    var name="<%=strBuyerName %>";
	    if(name!="")
	    {
	        strURL+="&name="+name;
	    }
	    window.open(strURL,'viewer',"height="+(screen.availHeight-30)+" , width="+screen.availWidth+", top=0, left=0,toolbar=no, menubar=no, scrollbars=no, location=n o, status=no");
    }
    function ViewInstanceFrm(itemId,Instance)
    {
	    var strURL = "viewworkflow.aspx?ItemId="+itemId+"&IID=" + Instance;
	    window.location.href=strURL;
    }
	function tosubmit(page)
	{	
		var strUrl="TaskQueryDetail.aspx";
		var testm = document.all.hidden1.value.replace('','%10');
		strUrl+="?search="+testm;
		if(page==""|| page==null)
			strUrl+="&pagenow="+document.all.jumpPage.value
		else				
			strUrl+="&pagenow="+page;
		strUrl+="&iid="+<%=iid%>;
		window.location.href=strUrl;
	}
	
	function toXMTP(iid)
	{
	    var strUrl="querydetialXMTP.aspx";
	    var typeValue=document.getElementById("btnTPXM").value;
	    if(iid!="")
	    {
	        strUrl+="?iid="+iid+"&type="+typeValue;
	        window.location.href=strUrl;
	    }
	    else
	    {
	        alert('请选择项目');
	    }
	}
	function isTP(iid)
	{
	    var typeValue=document.getElementById("btnTPXM").value;
	    if(typeValue=="特批项目")
	    {
	        if(confirm("确定要设定为特批项目？"))
	        {
	            toXMTP(iid);
	        }
	        else
	        {
	            return false;
	        }
	    }
	    else
	    {
	        if(confirm("确定要取消该特批项目？"))
	        {
	            toXMTP(iid);
	        }
	        else
	        {
	            return false;
	        }
	    }	  
	}
    </script>
    <table class="tab_top" width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <img alt="" src="../images/tab_top_left.gif" width="17" height="35" />
            </td>
            <td>
                <div class="tab_top_text3">
                    详细信息</div>
                <div class="tab_top_text2" onclick="ViewInstanceFrm(<%=itemId %>,'<%=iid%>')" style="cursor: hand;">
                    流程明细</div>
            </td>
            <td align="right">
                <input name="Submit3" type="button" class="newbutton" value="案件信息" onclick="ViewInstanceFrame(<%=itemId %>,<%=iid%>)" />
                <input name="Submit1" type="button" class="newbutton" value="返回" onclick="javascript:history.go(-1)" />
            </td>
            <td align="right">
                <img alt="" src="../images/tab_top_right.gif" width="5" height="35" />
            </td>
        </tr>
    </table>
    <table class="tab" width="100%" border="1">
        <tr align="center" valign="middle">
            <th onclick="tosubmitorder('顺序号')" style="cursor: hand">
                编号
            </th>
            <th onclick="tosubmitorder('所在岗位')" style="cursor: hand">
                所在岗位
            </th>
            <th onclick="tosubmitorder('申请单位')" style="cursor: hand">
                经办人
            </th>
            <th onclick="tosubmitorder('接件时间')" style="cursor: hand">
                接件时间
            </th>
            <th onclick="tosubmitorder('联系人')" style="cursor: hand">
                提交时间
            </th>
            <th onclick="tosubmitorder('优先级')" style="cursor: hand">
                办理时间
            </th>
            <th onclick="tosubmitorder('优先级')" style="cursor: hand">
                最大时限
            </th>
        </tr>
        <%=strhtml%>
    </table>
    <table width="100%" border="0" cellspacing="0" cellpadding="4">
        <tr>
            <td align="right" colspan="8" height="20">
                每页<%=pagesize%>行 共<%=mypage.RecordCount%>行 当前第<%=pagenow%>页 共<%=pagecount%>页
                <%
                    if (pagenow == 1)
                    {
                        Response.Write(" 首页 上一页");
                    }
                    else
                    {
		
                %>
                <a href=" javascript:tosubmit('1')">首页</a> <a href="javascript:tosubmit('<%=pagenow - 1%>')">
                    上一页</a>
                <%
                    }
	
                %>
                <%
                    if (pagecount == pagenow)
                    {
                        Response.Write("下一页 尾页");
                    }
                    else
                    {
		
                %>
                <a href=" javascript:tosubmit('<%=pagenow + 1%>')">下一页</a> <a href="javascript:tosubmit('<%=pagecount%>')">
                    尾页</a>
                <%
                    }
	
                %>
                转到第<select name="jumpPage" id="jumpPage" onchange="tosubmit()">
                    <%
                        for (int i = 1; i <= pagecount; i++)
                        {
                            if (i == pagenow)
                            {
			
                    %>
                    <option selected value="<%=i%>">
                        <%=i%>
                    </option>
                    <%
}
                            else
                            {
			
                    %>
                    <option value="<%=i%>">
                        <%=i%>
                    </option>
                    <%
                        }
                        }
                    %>
                </select>
                页
            </td>
        </tr>
    </table>
    <input type="hidden" id="hidden1" name="hidden1" value="<%=strSearch%>" />
</body>
</html>
