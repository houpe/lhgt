﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Business;
using Business.Admin;
using Business.Common;

public partial class QueryAndCollect_JieDaiCollectByDepart : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindData();
        }
    }

    private void BindData()
    {
        DataTable dtTable = JieDaiInfo.GetJieDaiCollectInfo(calStart.Value, calEnd.Value);

        List<string> lstFields = new List<string>();
        lstFields.Add("zxf");
        CommonOperationQuery.AddCollectRow(dtTable, "bm", lstFields);

        CustomGridView1.DataSource = dtTable;
        CustomGridView1.PageSize = SystemConfig.PageSize;
        CustomGridView1.RecordCount = dtTable.Rows.Count;
        CustomGridView1.DataBind();
    }

    protected void CustomGridView1_OnLoadData(object sender, EventArgs e)
    {
        BindData();
    }

    protected void CustomGridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        switch (e.Row.RowType)
        {
            case DataControlRowType.DataRow:
                DataRowView dtrv = e.Row.DataItem as DataRowView;
                
                if (dtrv != null)
                {
                    HyperLink hlBm = e.Row.FindControl("hlBm") as HyperLink;
                    hlBm.Text = dtrv["bm"].ToString();
                    string strUrl = string.Format("JieDaiCollectInOneDepart.aspx?bm={0}&dtStart={1}&dtEnd={2}", dtrv["bm"], calStart.Value, calEnd.Value);
                    hlBm.NavigateUrl = strUrl;
                    hlBm.Target = "_self";
                }
                break;
        }
    }

    /// <summary>
    /// 查询界面
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnQuery_Click(object sender, EventArgs e)
    {
        BindData();
    }
}