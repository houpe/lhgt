﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TaskCollectResult.aspx.cs"
    Inherits="QueryAndCollect_TaskCollectResult" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head runat="server">
    <title>案件执行情况</title>
    <link rel="stylesheet" type="text/css" href="../css/page.css" />
    <link href="../css/default/easyui.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../ScriptFile/jquery.js"></script>
    <script src="../ScriptFile/jquery.easyui.min.js" type="text/javascript"></script>

    <script type="text/javascript" src="../ScriptFile/DataCollect/echarts/esl.js"></script>
    <script type="text/javascript" src="../ScriptFile/DataCollect/MyEcharts.js"></script>
    <script type="text/javascript" src="../ScriptFile/DataCollect/WapCharts.js"></script>
    <script type="text/javascript" src="../ScriptFile/RequestParams.js"></script>

    <script type="text/javascript">

        function getCollectData(strDivId, strType, strCollectType, strWname) {
            $.ajax({
                url: "../Xnjc/DataCollectHandler.ashx",
                data: { collecttype: strCollectType, dataType: strType, wname: strWname },
                cache: false,
                async: false,
                dataType: 'json',

                success: function (data) {
                    if (data) {
                        DrawPie(data, strDivId);
                    }
                },

                error: function (msg) {
                    alert("系统发生错误");
                }

            });
        }

        //采用js统计数据
        $(document).ready(function () {
            var strWname = "";
            if (request("wname") != "" && request("wname") != "undefined" && request("wname") != "null") {
                strWname = decodeURI(request("wname"));
            }
            getCollectData("divJjl", "3", "pie", strWname);
            getCollectData("divBjzt", "4", "pie", strWname);
           
            //$(".echarts-dataview").remove();
        });

        //展示chart
        //function ShowChart(grapType, strText) {
        //    if (grapType == "") {
        //        grapType = "FCF_Column2D.swf";
        //    }
        //    var myChart = new FusionCharts("../ScriptFile/FusionCharts/" + grapType, "myChartId", "500", "350");
        //    myChart.setDataXML(strText)
        //    myChart.render("divYxj");
        //}

    </script>
</head>
<body>
    <div class="easyui-tabs" fit="true" border="true">

        <div title="统计列表">
            <!--某个流程或全部流程，指定时间内或全部时间内，案件处理情况在各部门之间的分布-->
            <table width="100%" cellspacing="0" cellpadding="0" align="center" border="0" class="tab">
                <tr>
                    <th>流程名称
                    </th>
                    <th>案件数
                    </th>
                    <th>办结数
                    </th>
                    <th>挂起数
                    </th>
                    <th>警告数
                    </th>
                    <th>超期数
                    </th>
                    <th>加急数
                    </th>
                    <th>特急数
                    </th>
                    <th>平均用时
                    </th>
                    <th>详细
                    </th>
                </tr>
                <%=strContentHtml   %>
                <tr>
                    <td>合计
                    </td>
                    <td>
                        <%=instanceCountX%>
                    </td>
                    <td>
                        <%=status2X%>
                    </td>
                    <td>
                        <%=status0X%>
                    </td>
                    <td>
                        <%=alarmX%>
                    </td>
                    <td>
                        <%=overtimeX%>
                    </td>
                    <td>
                        <%=priority1X%>
                    </td>
                    <td>
                        <%=priority2X%>
                    </td>
                    <td>
                        <%=strPjys  %>
                    </td>
                    <td></td>
                </tr>
            </table>
        </div>
        <div title="图表分析" id="divTbtj">
            <%--<div class="easyui-tabs" fit="true" border="true">--%>
                <div style=" float: left; border:solid 1px;">
                    <div>按月统计接件量</div>
                    <div title="按月统计接件量" id="divJjl" style="width: 450px; height: 400px;">
                    </div>
                </div>
                <div style=" padding-left:10px;float: left;border:solid 1px;">
                    <div>办理状态统计</div>
                    <div title="办理状态统计" id="divBjzt" style="width: 450px; height: 400px;">
                    </div>
                </div>
           <%-- </div>--%>
        </div>
    </div>
</body>
</html>
