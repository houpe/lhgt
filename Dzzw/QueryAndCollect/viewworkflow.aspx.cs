﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class QueryAndCollect_viewworkflow : PageBase
{
    protected string ServerPath = string.Empty;
    protected string strIid = string.Empty;
    protected string itemId = string.Empty;

    protected void Page_Load(object sender, System.EventArgs e)
    {
        ConfigHelper config = new ConfigHelper(true);
        ServerPath = config.GetAppSettingsValue("NetoWsUrl").Replace("WebFormService.asmx", "");
        strIid = Request["IID"];
        itemId = Request["ItemId"];
    }

}
