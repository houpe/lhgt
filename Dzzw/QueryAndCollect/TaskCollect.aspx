<%@ Page Language="c#" Inherits="QueryAndCollect_TaskCollect" CodeFile="TaskCollect.aspx.cs" %>

<html>
<script type="text/javascript">
    //打开内嵌网页的通用方法1（window）
    function openDivDlg(strUrl) {
        $('#ifmContent').attr("src", strUrl);
    }

    function PostInfo(appearType) {
        var strName = $("#wname").val();
        var strStartDate = $("#startDate").datebox('getValue');
        var strEndDate = $("#endDate").datebox('getValue');

        var strUrl = "TaskCollectResult.aspx?wname=" + encodeURI(strName) + "&timeFromOfAccept=" + strStartDate + "&timeToOfAccept=" + strEndDate + "&displayType=" + appearType;

        openDivDlg(strUrl);
    }
</script>
<head id="Head1" runat="server">
    <title>案件执行情况</title>
    <link rel="stylesheet" id="skinCss" type="text/css" href="../css/default/easyui.css" />
    <link rel="stylesheet" type="text/css" href="../css/icon.css" />
    <script type="text/javascript" src="../ScriptFile/jquery.js"></script>
    <script type="text/javascript" src="../ScriptFile/jquery.easyui.min.js"></script>
    <script src="../ScriptFile/easyui-lang-zh_CN.js" type="text/javascript"></script>
</head>
<body class="easyui-layout" id="divBody">
    <div id="divwest" region="west" title='办件类型' split="true" style="width: 180px;">
        <table cellspacing="0" cellpadding="0" border="0" class="tab" style="width: 100%;
            height: auto; font-size: 12pt;">
            <tr>
                <td>
                    <select name="wname" id="wname" size="10" style='width: 100%; height: 250px; border: 0'>
                        <option value="">全部</option>
                        <%=strhtml%>
                    </select>
                </td>
            </tr>
            <tr>
                <td style="font-size: 10pt;">
                    案件接收时间：
                </td>
            </tr>
            <tr>
                <td>
                    自<input class="easyui-datebox" id="startDate" />
                </td>
            </tr>
            <tr>
                <td>
                    到<input class="easyui-datebox" id="endDate" />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <input id="btnSubmit" onclick="PostInfo('search')" type="button" class="NewButton"
                        value="提交" />
                    <input id="btnImage" type="button" class="NewButton" value="图表" onclick="showAsChart('charts')"
                        style="display: none;" />
                </td>
            </tr>
        </table>
    </div>
    <div region="center" title="内容区" id="divCenter">
        <iframe src="" width="100%" height="100%" frameboder="0" id="ifmContent"></iframe>
    </div>
</body>
</html>
