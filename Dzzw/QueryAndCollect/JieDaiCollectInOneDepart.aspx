﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="JieDaiCollectInOneDepart.aspx.cs"
    Inherits="QueryAndCollect_JieDaiCollectInOneDepart" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>单部门的接待情况</title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="text-align: center">
        <controlDefine:CustomGridView ID="CustomGridView1" runat="server" AllowPaging="True"
            AutoGenerateColumns="false" ShowItemNumber="true" CheckboxAutoPostBack="False"
            CustomImportToExcel="false" CustomSortExpression="" DeleteToolTipOfCol="-1" HideTextLength="20"
            ShowCmpAndCancel="False" ShowGridViewSelect="List" ShowHideColModel="None" ShowImportButton="False"
            ShowTreeView="False" SelectOrDelete="False" Width="100%" OnOnLoadData="CustomGridView1_OnLoadData"
            OnRowCreated="CustomGridView1_RowCreated" CssClass="window_tab_list" CustomGridViewClass="window_tab_list_all"
            DivFstClass-DivAllClass="window_tab_list_fst" DataTableClass-OnMouseover="#b5e0f3"
            DataTableClass-OnMouseout="#eff8fc" DivFstClass-DivButtonClass="button" DivFstClass-DivLabelClass="window_tab_list_fst_lbl"
            DivFstClass-DivInputClass="Input" DivScdClass-DivAllClass="window_tab_list_sed"
            DivScdClass-DivButtonClass="button" DivScdClass-DivInputClass="ShortInput" DivScdClass-DivLabelClass="window_tab_list_fst_lbl"
            DivScdClass-DragLayerClass="window_tab_list_scd_dray" DivTrdClass-DivButtonClass="button"
            AllowSortingAscImgCss="window_tab_list_Asc" AllowSortingDescImgCss="window_tab_list_desc"
            AlternatingRowStyle-BackColor="#E6F4FF" RowStyle-HorizontalAlign="Center" AllowOnMouseOverEvent="false"
            Font-Size="12px">
            <RowStyle BackColor="#ffffff" Height="28px" />
            <Columns>
                <asp:BoundField DataField="bm" HeaderText="部门"></asp:BoundField>
                <asp:TemplateField HeaderText="表单明细">
                    <ItemTemplate>
                        <a href='TaskQueryDetail.aspx?iid=<%#Eval("iid") %>&search=detail?'>
                            <%#Eval("iid") %></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="zxf" HeaderText="总消费金额"></asp:BoundField>
            </Columns>
            <PagerSettings Visible="False" />
        </controlDefine:CustomGridView>
    </div>
    </form>
</body>
</html>
