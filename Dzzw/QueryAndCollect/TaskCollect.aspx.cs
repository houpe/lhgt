using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Business.FlowOperation;


public partial class QueryAndCollect_TaskCollect : System.Web.UI.Page
{
    public DataTable dtWorkFlowNames = null;
    protected string strhtml = string.Empty;
    protected void Page_Load(object sender, System.EventArgs e)
    {
        ClsUserWorkFlow clsWorkFlow = new ClsUserWorkFlow();
        dtWorkFlowNames = clsWorkFlow.GetFlowName();
        foreach (System.Data.DataRow drName in dtWorkFlowNames.Rows)
        {
            string strOption = string.Format("<option value='{0}'>{0}</option>", drName[0]);
            strhtml += strOption;
        }
    }
}
