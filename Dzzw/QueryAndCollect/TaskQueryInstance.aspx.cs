using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Business;
using Business.FlowOperation;
using WF_Business;


public partial class QueryAndCollect_TaskQueryInstance:PageBase
{
    public string searchWord = string.Empty;

    private void Page_Load(object sender, System.EventArgs e)
    {
        ReturnFromComplete();
        if (!Page.IsPostBack)
        {
            BindList();
        }
    }

    public void ReturnFromComplete()
    {
        if (Request["opera"] == "-1")
        {
            string strIID = Request["iid"];
            ClsUserWorkFlow rfoTemp = new ClsUserWorkFlow();
            rfoTemp.ReturnTaskFromComplete(strIID);
        }
    }

    public void BindList()
    {
        DataTable dtSource;
        if (ViewState["dtTemp"] == null)
        {
            if (Session["querySql"] != null)
            {
                SysParams.OAConnection().RunSql(Session["querySql"].ToString(), out dtSource);
                Session["querySql"] = null;
            }
            else
            {
                string strYwlx = Request["ywlx"];//业务类型    
                string strYxj = Request["yxj"];//优先级    
                string strSqdw = Request["sqdw"];//申请单位    

                string strStartDate = Request["startDate"];
                string strEndDate = Request["endDate"];
                string isComplete = Request["isComplete"];
                string notComplete = Request["notComplete"];
                string isCommand = Request["isCommand"];
                string notCommand = Request["notCommand"];
                string isStop = Request["isStop"];
                string notStop = Request["notStop"];
                string strSearch = Request["search"];
                string strYwbh = Request["ywbh"];

                ClsUserWorkFlow CLWF = new ClsUserWorkFlow();
                dtSource = CLWF.SearchFlowDataGrid(strStartDate, strEndDate, strYwlx, strYxj, strSqdw, strYwbh, isComplete, notComplete, isCommand, notCommand, isStop, notStop, UserId, ref searchWord);
            }

            ViewState["dtTemp"] = dtSource;
        }
        else
        {
            dtSource = ViewState["dtTemp"] as DataTable;
        }
        gvSerial.DataSource = ViewState["dtTemp"];
        gvSerial.RecordCount = dtSource.Rows.Count;
        gvSerial.DataBind();
    }


    /// <summary>
    /// 优先级图标
    /// </summary>
    /// <param name="str"></param>
    /// <returns></returns>
    protected string Getyou(string str)
    {
        if (str.EndsWith("0"))
            str = "top_ico_yxj1.gif";
        else if (str.Equals("1"))
            str = "top_ico_yxj2.gif";
        else
            str = "top_ico_yxj3.gif";

        return str;
    }

    /// <summary>
    /// 绑定数据
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvSerial_OnLoadData(object sender, EventArgs e)
    {
        BindList();
    }


     /// <summary>
    /// 行创建
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvSerial_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView dtr = (DataRowView)e.Row.DataItem;

            if (dtr != null)
            {
                try
                {
                    if (dtr["status"].ToString() != "2")
                    {
                        e.Row.Cells[e.Row.Cells.Count - 1].Text = "正在办理";
                    }
                }
                catch( Exception ex)
                {
                }
            }
        }
    }
}
