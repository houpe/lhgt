﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Business.Admin;
using Business;
using Business.Common;

public partial class QueryAndCollect_Car_Run_Collect : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindData();
        }
    }

    private void BindData()
    {
        DataTable dtTable = CarManage.GetRunCollectInfo(calStart.Value, calEnd.Value);

        //添加运行费用小计
        DataColumn dcTemp = new DataColumn("yxfyxj");
        dtTable.Columns.Add(dcTemp);
        for (int i = 0; i < dtTable.Rows.Count; i++)
        {
            string strWxfy = dtTable.Rows[i]["wxje"].ToString();
            string strHyje = dtTable.Rows[i]["hyje"].ToString();

            double dWxfy = 0;
            double dHyje = 0;
            if (!string.IsNullOrEmpty(strWxfy))
            {
                dWxfy = Convert.ToDouble(strWxfy);
            }
            if (!string.IsNullOrEmpty(strHyje))
            {
                dHyje = Convert.ToDouble(strHyje);
            }

            dtTable.Rows[i]["yxfyxj"] = dHyje+dWxfy;
        }

        List<string> lstFields = new List<string>();
        lstFields.Add("zgls");
        lstFields.Add("ctgls");
        lstFields.Add("ctcs");
        lstFields.Add("wxje");
        lstFields.Add("hyje");
        lstFields.Add("yxfyxj");
        CommonOperationQuery.AddCollectRow(dtTable, "cph", lstFields);

        CustomGridView1.DataSource = dtTable;
        CustomGridView1.PageSize = SystemConfig.PageSize;
        CustomGridView1.RecordCount = dtTable.Rows.Count;
        CustomGridView1.DataBind();
    }

    protected void CustomGridView1_OnLoadData(object sender, EventArgs e)
    {
        BindData();
    }

    protected void CustomGridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        switch (e.Row.RowType)
        {
            case DataControlRowType.DataRow:
                DataRowView dtrv = e.Row.DataItem as DataRowView;
                if (dtrv == null)
                {
                    break;
                }
                break;
        }
    }

    /// <summary>
    /// 查询界面
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnQuery_Click(object sender, EventArgs e)
    {
        BindData();
    }
}