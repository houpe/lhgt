﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using Business.Admin;
using Common;

public partial class QueryAndCollect_TaskProcessView : PageBase
{
    public string Inputindex = string.Empty;
    public string strIid = string.Empty;
    public string ResServerPath = string.Empty;
    public bool ISsystemUser = true;
    public string name = "";
    public string strWiid = "";
    public string ServerPath = "";
    protected void Page_Load(object sender, System.EventArgs e)
    {
        Inputindex = "";
        strIid = Request["iid"];
        name = Request["name"];
        strWiid = Request["ItemId"];

        //设置附件上传页面参数
        ConfigHelper config = new ConfigHelper(true);
        ServerPath = SystemConfig.NetoWsUrl;
        String strDataRepairUserId = config.GetAppSettingsValue("DataRepairUserId");
        if (strDataRepairUserId.ToUpper().Trim() == UserId.ToUpper().Trim())
        {
            Inputindex = "0";
        }
        //判断当前登录用户是否为超级管理员
        ISsystemUser = SystemManager.IsSystemUser(UserId);

        //表单路径
        ResServerPath = string.Format("{0}?iid={1}&UserId={2}",
                 SystemConfig.NewActivexAppearUrl, strIid, UserId);
    }
}
