using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Business;
using Business.Inspect;

public partial class QueryAndCollect_TaskQueryDetail : PageBase
{
    protected string strBuyerName = string.Empty;
    protected string iid = "";
    protected System.Collections.Generic.List<System.Data.DataRow> listwork = null;
    protected int pagesize = 10;//页面记录数
    protected int pagenow = 1;//当前页
    protected int pagecount = 0;//总页数
    protected Business.Common.PageOperation mypage = null;
    protected string strSearch = "";
    protected string itemId = "";
    protected string strhtml = string.Empty;

    protected void Page_Load(object sender, System.EventArgs e)
    {
        string searchWord = string.Empty;

        iid = Request["iid"];
        string strPageNow = Request["pagenow"];
        strSearch = Request["search"];
        string type = string.Empty;

        if (!string.IsNullOrEmpty(Request["name"]))
        {
            strBuyerName = Request["name"];
        }
        Session["strBuyerName"] = strBuyerName;

        if (!string.IsNullOrEmpty(Request["type"]))
        {
            Response.Write("<script>alert('保存成功');</script>");
        }

        if (!string.IsNullOrEmpty(strPageNow))
        {
            pagenow = Int32.Parse(strPageNow); ;
            if (pagenow < 1)
                pagenow = 1;
        }
        double dHourInEveryDay = Business.SystemConfig.SecondInEveryDay;
        Xnjc xnjc = new Xnjc();
        DataTable dtSource = xnjc.TaskQueryDetail_list(dHourInEveryDay, iid);

        mypage = new Business.Common.PageOperation(dtSource, pagenow, pagesize);
        if (dtSource.Rows.Count > 0)
        {
            itemId = dtSource.Rows[0].ItemArray[6].ToString();

            listwork = mypage.CurrentPageList;
            pagecount = mypage.PageCount;

            for (int i = 0; i < listwork.Count; i++)
            {
                System.Data.DataRow myArray = listwork[i] as System.Data.DataRow;

                string str5 = myArray[5].ToString();
                if (str5.Equals("0天0小时") || str5.Equals("天小时"))
                    str5 = "小于一小时";

                //在回退的岗位人员前加标识
                string strPerson = myArray[2].ToString();
                if (!string.IsNullOrEmpty(myArray["isback"].ToString()))
                {
                    if (myArray["isback"].ToString() == "1")
                    {
                        strPerson = string.Format("[{0}]", strPerson);
                    }
                }

                strhtml += "<tr align='center' valign='middle'>       <td>" +
                         myArray[0] +
                     "</td>            <td>" +
                         myArray[1] +
                     "</td>        <td>" +
                         strPerson +
                     "</td>        <td>" +
                         myArray[3] +
                     "</td>         <td>" +
                         myArray[4] +
                     "</td>          <td>" +
                         str5 +
                     "</td>          <td>" +
                         myArray[9] +
                     "</td>        </tr>";

            }
        }
    }
}
