﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using WF_Business;
using Business;

public partial class QueryAndCollect_viewinstanceresource : PageBase
{
    //第一个可显示页面的节点号
    public string FirstNode = "0";
    private bool bFirst = true;

    private string iid
    {
        get
        {
            return Request["iid"] == null ? string.Empty : Request["iid"].ToString();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
       
    }

    public String GetTreeHTML()
    {
        WorkFlowResource wfr = new WorkFlowResource();
        DataTable dt = wfr.ListInstanceResource(iid);
        dt.Columns.Add(new DataColumn("ParentId"));
        dt.Columns.Add(new DataColumn("Site"));
        dt.Columns.Add(new DataColumn("Name"));
        DataView dv = dt.DefaultView;
        dv.Sort = "Path";
        for (int i = 0; i < dv.Count; i++)
        {
            string path = dv[i]["Path"].ToString();
            string[] tmps = path.Split('/');
            dv[i]["Site"] = i + 1;
            if (tmps.Length > 1)//不是顶节点
            {
                dv[i]["Name"] = tmps[tmps.Length - 1];
                string pre = path.Substring(0, path.LastIndexOf('/'));
                DataRow[] drs = dt.Select("Path='" + pre + "'");
                if (drs.Length > 0)
                    dv[i]["ParentId"] = drs[0]["Site"].ToString();
            }
            else
            {
                dv[i]["Name"] = dv[i]["Path"].ToString();
                dv[i]["ParentId"] = 0;
            }
        }
        StringBuilder sb = new StringBuilder();
        sb.Append("<script type='text/javascript'>\n");
        sb.Append("var dtree1 = new dTree('dtree1');\n");
        sb.Append("dtree1.config.useCookies = false;\n");
        sb.Append("dtree1.add(0,-1,'所有表单');\n");
        printSubNodes(dt, sb);
        sb.Append("document.write(dtree1);\n");
        sb.Append("</script>\n");
        return sb.ToString();
    }

    private void printSubNodes(DataTable dt, StringBuilder sb)
    {
        foreach (DataRow dr in dt.Rows)
        {
            string type = dr["type"].ToString();
            String strOutput = string.Empty;
            string strparentid = dr["ParentId"].ToString();
            if (string.IsNullOrEmpty(strparentid))
            {
                strparentid = "0";
            }

            DataRow[] drs = dt.Select("ParentId='" + dr["Site"].ToString() + "'");


            if (drs.Length > 0)//有子节点，说明不是叶结点
            {
                strOutput = "dtree1.add(" +
                            dr["Site"].ToString() + "," +
                            strparentid + ",'" +
                            dr["Name"].ToString() + "','','" +
                            dr["Path"].ToString() + "','','../images/dtree/folder.gif','../images/dtree/folderopen.gif'," +
                            type + ",false," +
                            dr["input_index"].ToString() + ",'" + dr["rtid"].ToString() + "' );\n";
            }
            else
            {
                string resValue = dr["Res_Value"].ToString();



                if (type != "0")//不是目录
                {
                    string right = "false";
                    if (type == "3")//如果是表单的话
                    {
                        strOutput = "dtree1.add(" +
                                    dr["Site"].ToString() + "," +
                                    strparentid + ",'" +
                                    dr["Name"].ToString() + "','" +
                                    dr["Path"].ToString() + "','" +
                                    resValue + "','','../images/dtree/page.gif','../images/dtree/page.gif'," +
                                    type + "," + right + "," +
                                    dr["input_index"].ToString() + ",'" + dr["rtid"].ToString() + "' );\n";
                    }
                    else if (type == "1" || type == "11" || type == "12")
                    {
                        resValue = ResolveUrl(resValue);
                        strOutput = "dtree1.add(" +
                                    dr["Site"].ToString() + "," +
                                    strparentid + ",'" +
                                    dr["Name"].ToString() + "','" +
                                    resValue + "','" +
                                    resValue + "','','../images/dtree/ie.gif','../images/dtree/ie.gif'," +
                                    type + "," + right + "," +
                                    dr["input_index"].ToString() + ",'" + dr["rtid"].ToString() + "');\n";
                    }
                    else
                    {
                        string ico = "../images/dtree/file.gif";
                        if (type == "2")
                        {
                            string strSql = "select ext_name from st_attachment where aid='" + resValue + "'";
                            string extName = SysParams.OAConnection().GetValue(strSql).ToLower();
                            if (extName == ".doc" || extName == ".docx")
                            {
                                ico = "../images/dtree/word.gif";
                                type = "21";
                            }
                            else if (extName == ".xls" || extName == ".xlsx")
                            {
                                ico = "../images/dtree/excel.gif";
                            }
                            else if (extName == ".tif" || extName == ".jpg" || extName == ".gif" || extName == ".bmp")
                            {
                                ico = "../images/dtree/image.gif";
                            }
                        }
                        strOutput = "dtree1.add(" +
                                    dr["Site"].ToString() + "," +
                                    strparentid + ",'" +
                                    dr["Name"].ToString() + "','" +
                                    dr["Path"].ToString() + "','" +
                                    resValue + "','','" + ico + "','" + ico + "'," +
                                    type + "," + right + "," +
                                    dr["input_index"].ToString() + ",'" + dr["rtid"].ToString() + "');\n";
                    }
                    if (bFirst && (type == "3" || type == "1"))
                    {
                        FirstNode = dr["Site"].ToString();
                        bFirst = false;
                    }
                }
                else
                {
                    strOutput = "dtree1.add(" +
                                dr["Site"].ToString() + "," +
                                strparentid + ",'" +
                                dr["Name"].ToString() + "','','" +
                                dr["Name"].ToString() + "','','',''," +
                                type + ",false," +
                                dr["input_index"].ToString() + ",'" + dr["rtid"].ToString() + "');\n";
                }
            }
            sb.Append(strOutput);
        }
        Response.Write("<input type='hidden' id='txtFirstNode' value ='" + FirstNode + "' />");
    }
}
