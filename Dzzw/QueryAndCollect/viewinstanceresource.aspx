﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="viewinstanceresource.aspx.cs" Inherits="QueryAndCollect_viewinstanceresource" %>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head id="Head1" runat="server">
    <title>资源树</title>
    <link rel="stylesheet" type="text/css" href="../css/dtree.css" />

    <script type="text/javascript" src="../ScriptFile/dtree.js"></script>

    <script type="text/javascript">			
			var bInit = false;
			
			function OpenFirst()
			{
				dtree1.openTo(txtFirstNode.value,true);
			}
			function OpenResource(name,type,input_index,rtid)
			{	
			   parent.OpenResource(name,type,input_index,rtid,false);
			}			
			function Init()
			{
				OpenFirst();
			}			
    </script>

</head>
<body scroll="auto" topmargin="5" leftmargin="3" onload="Init()">
    <div style="font-size: 12px">
        <%=GetTreeHTML()%>
    </div>
</body>
</html>
