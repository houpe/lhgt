﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business.Admin;
using System.Data;
using Business;
using Business.FlowOperation;
using Business.Common;

public partial class QueryAndCollect_TaskCollectResult : PageBase
{
    protected string strContentHtml = string.Empty;//datagrid主要内容
    protected string strPjys = string.Empty;//平均数
    protected int instanceCountX = 0;
    protected int status2X = 0;
    protected int status0X = 0;
    protected int alarmX = 0;
    protected int overtimeX = 0;
    protected int priority1X = 0;
    protected int priority2X = 0;
    protected int totalTimeX = 0;

    protected void Page_Load(object sender, System.EventArgs e)
    {
        if (!IsPostBack)
        {
            string displayType = string.Empty;
            string wname = string.Empty;
            string orderID = string.Empty;
            Dictionary<string, string> dicList = null;
            string timeFromOfAccept = string.Empty;
            string timeToOfAccept = string.Empty;

            displayType = Request["displayType"];
            orderID = Request["orderID"];

            if (!string.IsNullOrEmpty(Request["wname"]) && Request["wname"]!="null")
            {
                wname = Request["wname"];
            }

            timeFromOfAccept = Request["timeFromOfAccept"];
            timeToOfAccept = Request["timeToOfAccept"];

            ClsUserWorkFlow clsWorkFlow = new ClsUserWorkFlow();

            string stStaticSql = string.Empty;
            if (displayType.Equals("search"))
            {
                dicList = clsWorkFlow.StaticOfInstance(wname, timeFromOfAccept, timeToOfAccept);
            }

            string strWhere = string.Empty;
            if (dicList != null)
            {
                string sql = @"select iid,name 申请单位,
               (select wname from st_workflow a where a.wid = ins.wid) 业务类型,
               ins.accepted_time 接件时间,
               priority 优先级 from st_instance ins where  1=1  ";
                if (!string.IsNullOrEmpty(wname) && ("全部" != wname))
                {
                    strWhere += " and ins.wid in (select wid from st_workflow wf where wf.wname='" + wname + "') ";
                    sql += strWhere;
                }
                if (!string.IsNullOrEmpty(timeFromOfAccept))
                {
                    strWhere += "  and accepted_time>=to_date('" + timeFromOfAccept + "','YYYY-MM-DD HH24:MI:SS') ";
                    sql += strWhere;
                }
                if (!string.IsNullOrEmpty(timeToOfAccept))
                {
                    strWhere += " and accepted_time<=to_date('" + timeToOfAccept + "','YYYY-MM-DD HH24:MI:SS')";
                    sql += strWhere;
                }

                int instanceCount = Int32.Parse(dicList["instanceCount"]);
                int status2 = Int32.Parse(dicList["status2"]);
                int status0 = Int32.Parse(dicList["status0"]);
                int alarm = Int32.Parse(dicList["alarm"]);
                int overtime = Int32.Parse(dicList["overtime"]);
                int priority1 = Int32.Parse(dicList["priority1"]);
                int priority2 = Int32.Parse(dicList["priority2"]);

                instanceCountX += instanceCount;
                status2X += status2;
                status0X += status0;
                alarmX += alarm;
                overtimeX += overtime;
                priority1X += priority1;
                priority2X += priority2;


                strContentHtml += " <tr><td>" + wname + "</td><td>" + dicList["instanceCount"] +
                    "</td> <td>" + dicList["status2"] +
                    "</td><td>" +
                        dicList["status0"] +
                    "</td> <td>" +
                        dicList["alarm"] +
                    "</td>     <td>" +
                        dicList["overtime"] +
                    "</td>            <td>" +
                        dicList["priority1"] +
                    "</td>            <td>" +
                        dicList["priority2"] +
                    "</td>            <td>";

                string totalTimeStr = dicList["exusedtimeTotal"];
                long totalTime = 0;
                if (!string.IsNullOrEmpty(totalTimeStr))
                {
                    totalTime = Int64.Parse(totalTimeStr);
                    totalTimeX = (int)(totalTimeX + totalTime);
                }
                string cntStr = dicList["instanceCount"];
                int cnt = Int32.Parse(cntStr);
                if (cnt != 0)
                {
                    string strTime = ClsWorkDaySet.ToDefaultTime(totalTime / cnt);
                    strContentHtml += strTime;
                }

                Session["querySql"] = sql;
                strContentHtml += "</td> <td> <a href='../QueryAndCollect/TaskQueryInstance.aspx'>详细</a></td></tr>";
            }
            if (instanceCountX != 0)
            {
                string strTime = ClsWorkDaySet.ToDefaultTime(totalTimeX / instanceCountX);
                strPjys += strTime;
            }
        }
    }
}
