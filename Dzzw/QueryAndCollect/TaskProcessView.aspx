﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TaskProcessView.aspx.cs"
    Inherits="QueryAndCollect_TaskProcessView" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../css/default/easyui.css" />
    <link rel="stylesheet" type="text/css" href="../css/icon.css" />
    <link href="../css/page.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../ScriptFile/jquery.js"></script>

    <script type="text/javascript" src="../ScriptFile/jquery.easyui.min.js"></script>

</head>

<script type="text/javascript">
    var showControl = false; //是否打开了控件
    var type = null;   //正在显示的窗体类型，用于切换时获知原来的类型
    var formname = null; //正在显示的窗体的名称，用于打印切换时	
    var isComplete = false; //用于检测子页面是否已加载完毕
    var inputIndex = "0";

    function Refresh() {
        window.close();
        opener.location.reload(true); //重新加载打开本窗口的窗口（父窗口）
    }

    function Print(act) {
        if (type == 3)//表单
        {
            if (showControl == false)//打开了控件，先关掉	  
            {
                var url = window.location.protocol + "//" + window.location.host + "<%=ResServerPath%>&FormName=" + formname + "&input_index=" + inputIndex + "&act=" + act;
                window.open(url, '_blank', "height=" + screen.availHeight + " , width=" + screen.availWidth + ", top=0, left=0,toolbar=no, menubar=no, scrollbars=no, location=n o, status=no");
            }
        }
        else if (type == 11 || type == 12)//外部页面，但支持打印的
        {
            var url = formname.replace("STEP=", "NORIGHT="); //把节点名称的参数替掉，使打印时没权限不显示新增之类的按钮
            window.open(url + "&PrintAct=" + act, '_blank', "height=" + screen.availHeight + " , width=" + screen.availWidth + ", top=0, left=0,toolbar=no, menubar=no, scrollbars=no, location=n o, status=no");
        }
        if (showControl == true)
            FormView.Print();
    }

    function ViewWorkflow() {
        //采用新方法展示流程图
        var url = window.location.protocol + "//" + window.location.host + '<%=Business.SystemConfig.WorkflowUrl%>?iid=<%=strIid%>';
        FormView.document.location = url;
    }

    //未打开过控件时切换到控件
    function SwichControl(act, tname) {
        var url = "../office/doworkitem.aspx?IID=<%=strIid%>&UserId=<%=UserId %>&act=" + act;
        if (tname != "")
            url = url + "&formname=" + tname;
        FormView.document.location = url;
        showControl = true;
    }

    function CloseWindow() {
        window.close();
    }

    function Zoomin() {
        FormView.Zoomin();
    }

    function Zoomout() {
        FormView.Zoomout();
    }

    function BeforeUnLoad() {
        if (showControl == true)//打开了控件，关掉
            FormView.Close();
    }

    //打开窗体
    function OpenResource(vname, vtype, input_index, rtid, print) {
        if (print == false)//非打印状态
        {
            BeforeUnLoad();
            if (vtype == 3)//表单
            {
                FormView.document.location = "<%=ResServerPath%>&FormName=" + vname + "&input_index=" + input_index;
                showControl = false;
            }
            else if (vtype == 1)//Url
            {
                vname = vname.replace("{IID}", "IID=<%=strIid%>");
                vname = vname.replace("{SID}", "SID=<%=UserId%>");
                FormView.document.location = vname;
                showControl = false;
            }
            else if (vtype == 2 || vtype == 21) {
                if (showControl == false)
                    SwichControl("ViewAttachment", vname)
                else
                    FormView.OpenResource(vname);
            }
        }
        inputIndex = input_index;
        formname = vname;
        type = vtype;
    }

    function Attach() {
        var nLeft = (screen.width - 625) / 2;
        var nTop = (screen.height - 400) / 2;
        window.open("../UserControls/file.aspx?iid=<%=strIid %>&wiid=<%=strWiid%>", "submit", "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=auto,resizable=yes,width=625,height=500,left=" + nLeft + ",top=" + nTop, true);
    }

    function AttachManage() {
        var nLeft = (screen.width - 600) / 2;
        var nTop = (screen.height - 400) / 2;
        window.open("../SystemManager/DetailAttachment.aspx?ItemId=<%=strWiid%>&iid=<%=strIid %>&step=<%=name %>", "submit", "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,left=" + nLeft + ",top=" + nTop + ",height=500 , width=600", true);
    }
</script>

<body class="easyui-layout" id="divBody" style="width:100%; height:100%">
    <div id="divNorth" region="north" split="false" style="height: 47px; margin: 0px;
        padding: 0px; background-color: #C1DBFE">
        <table style="text-align: center;">
            <tr>
                <td width="50" class="showCursor" onclick="ViewWorkflow()">
                    <img alt="进度视图" src="../images/top_ico_01.gif" width="22" height="22" /><br />
                    进度视图
                </td>
                <td style="width: 20px">
                    <img alt="" src="../images/top_img.gif" width="2" height="36" />
                </td>
                <%
                    if (ISsystemUser)
                    {
                %>
                <td width="35" class="showCursor" onclick="Attach()">
                    <img src="../images/top_ico_11.gif" width="22" height="22" /><br />
                    附件
                </td>
                <td width="55" class="showCursor" onclick="AttachManage()">
                    <img src="../images/top_ico_11.gif" width="22" height="22" /><br />
                    附件管理
                </td>
                <%
                    }
                %>
                <td width="35" class="showCursor" onclick="Print('Print')">
                    <img alt="打印" src="../images/top_ico_05.gif" width="22" height="22" /><br />
                    打印
                </td>
                <td width="50" class="showCursor" onclick="Print('PrintView')">
                    <img alt="打印预览" src="../images/top_ico_05.gif" width="22" height="22" /><br />
                    打印预览
                </td>
                <td width="35" class="showCursor" onclick="Print('Print2')">
                    <img alt="套打" src="../images/top_ico_06.gif" width="22" height="22" /><br />
                    套打
                </td>
                <td width="50" class="showCursor" onclick="Print('Print2View')">
                    <img alt="套打预览" src="../images/top_ico_06.gif" width="22" height="22" /><br />
                    套打预览
                </td>
                <td width="35" class="showCursor" onclick="Zoomin()">
                    <img alt="放大" src="../images/top_ico_09.gif" width="22" height="22" /><br />
                    放大
                </td>
                <td width="35" class="showCursor" onclick="Zoomout()">
                    <img alt="缩小" src="../images/top_ico_08.gif" width="22" height="22" /><br />
                    缩小
                </td>
                <td width="35" class="showCursor" onclick="location.reload()">
                    <img alt="刷新" src="../images/top_ico_07.gif" width="22" height="22" /><br />
                    刷新
                </td>
                <td width="35" class="showCursor" onclick="CloseWindow()">
                    <img alt="关闭" src="../images/top_ico_07.gif" width="22" height="22" /><br />
                    关闭
                </td>
            </tr>
        </table>
    </div>
    <div id="divwest" region="west" title='资源列表' split="true" style="width: 200px;">
        <iframe src="viewinstanceresource.aspx?iid=<%=strIid%> "
            id="ResourceView" name="ResourceView" scrolling="auto" width="100%" height="100%"
             frameboder="0" resize="true"></iframe>
    </div>
    <div region="center" title="业务办公区" style="overflow: hidden;" id="divCenter">
        <iframe src="" id="FormView" name="FormView" scrolling="auto" width="100%" height="100%"
            style="border: 0px" resize="true"></iframe>
    </div>
</body>
</html>
