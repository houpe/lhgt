﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business.Inspect;
using Business;
using WF_Business;

public partial class Xnjc_insurgedetail : PageBase
{
    public string id = string.Empty;//业务流水号
    public string d_s = string.Empty;
    public string d_e = string.Empty;
    public string s_worktype1 = string.Empty;
    public string s_worktype = string.Empty;
    public string s_type = string.Empty;

    public string step = string.Empty;
    public string username = string.Empty;
    public string AccTime = string.Empty;
    public string DoTime = string.Empty;
    public string strtag = string.Empty;
    public string Extotaltime = string.Empty;

    public System.Data.DataTable rs;
    protected string strhtml = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        s_type = Request["s_lctype"];//流程类型

        d_s = Request["Date_begin"];
        d_e = Request["Date_end"];
        s_worktype1 = Request["s_worktype1"];
        s_worktype = Request["s_worktype"];

        if (Request["id"] != null)
        {
            id = Request["id"];
        }
        if (string.IsNullOrEmpty(d_s))
        {
            d_s = string.Empty;
        }
        if (string.IsNullOrEmpty(d_e))
        {
            d_e = string.Empty;
        }
        if (string.IsNullOrEmpty(s_type))
        {
            s_type = string.Empty;
        }
        if (string.IsNullOrEmpty(s_worktype))
        {
            s_worktype = string.Empty;
        }

        //根据输入的业务ID,查找两个表
        string sql1 = "select distinct step, exbeginmonitor from sv_work_step where iid='" + id +
                     "' order by exbeginmonitor";
        SysParams.OAConnection().RunSql(sql1, out rs);

        int dt = 0;
        int ttime = 0;
        //根据节点列出其他的信息,经办人，提交时间，办理时间，是否超时等
        for (int i = 0; i < rs.Rows.Count; i++)
        {
            strtag = string.Empty;
            step = rs.Rows[i][0].ToString();
            Xnjc xnjc = new Xnjc();
            System.Data.DataTable rs1 = xnjc.insurgedetail_list(id, step);
            if (rs1.Rows.Count > 0)
            {
                username = rs1.Rows[0][0].ToString();
                AccTime = rs1.Rows[0][1].ToString();
                DoTime = rs1.Rows[0][2].ToString();
                dt = Convert.ToInt32(rs1.Rows[0][3]);
                ttime = Convert.ToInt32(rs1.Rows[0][4]);
            }

            if (dt > ttime && ttime != 0)
            {
                strtag = "超时";
            }
            if (dt == 0)
            {
                DoTime = "不到一分钟";
            }

            //查找在办人

            strhtml += "<tr bgcolor=\"#ffffff\" onmouseover=\"this.style.backgroundColor='#caf7fd'\" onMouseOut=\"this.style.backgroundColor='#ffffff'\">" + "\r\n";
            strhtml += "<td align=\"center\">" + step + "</td>" + "\r\n";
            strhtml += "<td align=\"center\">" + username + "</td>" + "\r\n";
            strhtml += "<td align=\"center\" >" + AccTime + "</td>" + "\r\n";
            strhtml += "<td  align=\"center\">" + DoTime + "</td>" + "\r\n";
            strhtml += "<td  align=\"center\">" + strtag + "</td>" + "\r\n";
            strhtml += "</tr>" + "\r\n";
        }
    }
}
