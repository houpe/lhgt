using System.Web.UI;
using System.Data;
using System.Text;
using System;
using Business.FlowOperation;
using InfoSoftGlobal;

public partial class Xnjc_WorkflowPotency : Page
{
    public int ico;

    // Methods
    private void BindSelect()
    {
        DataTable newWorkflowNoFree = ClsUserWorkFlow.GetNewWorkflowNoFree();
        this.selYwlx.DataSource = newWorkflowNoFree;
        this.selYwlx.DataTextField = "wname";
        this.selYwlx.DataValueField = "wid";
        this.selYwlx.DataBind();
    }

    private void GetData2()
    {
        DataTable stepInfo = BusinessStatistics.GetStepInfo(base.Request);
        DataTable timeInfo = BusinessStatistics.GetTimeInfo(base.Request);
        string wname = WorkFlowHandle.GetWnameByWid(base.Request["selYwlx"]);
        StringBuilder builder = new StringBuilder();
        StringBuilder builder2 = new StringBuilder();
        StringBuilder builder3 = new StringBuilder();
        builder.AppendFormat("<graph xaxisname='{0}'  decimalPrecision ='0'   formatNumberScale='0'   caption='流程效能分析（" + wname + "）' subcaption='' basefontsize='12'  yAxisMinValue='0' yAxisMaxValue='10' >", string.Empty);
        builder2.Append("<categories font='Arial' fontSize='11' fontColor='000000'>");
        builder3.Append("<dataset seriesname='承诺用时' color='56B9F9'>");
        int num = 0;
        foreach (DataRow row in stepInfo.Rows)
        {
            decimal num2;
            decimal.TryParse(row["timeout"].ToString(), out num2);
            builder2.AppendFormat("<category name='{0}' hoverText='{0}'/>", row["sname"]);
            builder3.AppendFormat("<set value='{0}'/>", num2);
            if (num2 <= 0M)
            {
                num++;
            }
        }
        builder2.Append("</categories>");
        builder3.Append("</dataset>");
        builder3.Append("<dataset seriesname='实际用时' color='C9198D'>");
        foreach (DataRow row2 in stepInfo.Rows)
        {
            DataRow[] rowArray = timeInfo.Select(string.Format("stepctlid='{0}'", row2["ctlid"]));
            decimal num3 = 0M;
            if (rowArray.Length > 0)
            {
                foreach (DataRow row3 in rowArray)
                {
                    decimal num4;
                    decimal.TryParse(row3["exusedtime"].ToString(), out num4);
                    num3 += num4;
                }
                num3 = Math.Ceiling((decimal) (num3 / rowArray.Length));
            }
            builder3.AppendFormat("<set value='{0}'/>", num3);
            if (num3 <= 0M)
            {
                num++;
            }
        }
        builder3.Append("</dataset>");
        builder.Append(builder2);
        string str4 = "";
        builder.Append(builder3);
        builder.Append("</graph>");
        str4 = FusionCharts.RenderChart("../ScriptFile/FusionCharts/FCF_MSBar2D.swf", "", builder.ToString(), "wrokflowsum", "600", "500", false, true);
        this.FCLiteral1.InnerHtml = str4;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!base.IsPostBack)
        {
           PageBase.SetDefineText(this.Controls);
            this.BindSelect();
        }
        if (base.Request.Form.Count > 0)
        {
            this.ico = 1;
            this.GetData2();
        }
    }
}

 
 
