using System.Web.UI;
using System.Data;
using System.Text;
using System.Collections;
using System;
using Business.FlowOperation;
using InfoSoftGlobal;
using Business.Common;

public partial class Xnjc_WorkflowQuantity : Page
{
    public int ico;

    // Methods
    private void BindSelect()
    {
        DataTable newWorkflowNoFree = ClsUserWorkFlow.GetNewWorkflowNoFree();
        DataRow row = newWorkflowNoFree.NewRow();
        row["wid"] = "-1";
        row["wname"] = "全部流程";
        newWorkflowNoFree.Rows.InsertAt(row, 0);
        this.selYwlx.DataSource = newWorkflowNoFree;
        this.selYwlx.DataTextField = "wname";
        this.selYwlx.DataValueField = "wid";
        this.selYwlx.DataBind();
    }

    private int Calculate(DataTable dt, string wname, string field, string v)
    {
        int num = 0;
        foreach (DataRow row in dt.Rows)
        {
            if ((row["wname"].ToString() == wname) && (row[field].ToString() == v))
            {
                num++;
            }
        }
        return num;
    }

    public string CreateChart()
    {
        string str = "";
        return FusionCharts.RenderChart("../ScriptFile/FusionCharts/FCF_MSColumn3D.swf", "", str, "FactorySum", "400", "300", false, true);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!base.IsPostBack)
        {
            PageBase.SetDefineText(this.Controls);
            this.BindSelect();
        }
        if (base.Request.Form.Count > 0)
        {
            this.ico = 1;
            this.Statistics();
        }
    }

    private void Statistics()
    {
        DataTable businessCount = BusinessStatistics.GetBusinessCount(base.Request);
        DataTable wname = BusinessStatistics.GetWname(base.Request);
        ColorPicker picker = new ColorPicker();
        string str = "lcmc";
        string str2 = "zzt";
        if (!string.IsNullOrEmpty(base.Request["selTjfs"]))
        {
            str = base.Request["selTjfs"];
            this.selTjfs.Value = str;
        }
        if (!string.IsNullOrEmpty(base.Request["selTx"]))
        {
            str2 = base.Request["selTx"];
            this.selTx.Value = str2;
        }
        StringBuilder builder = new StringBuilder();
        StringBuilder builder2 = new StringBuilder();
        builder.Append("<graph caption='流程业务量分析'   showNames='1'  decimalPrecision='0' xAxisName='流程' yAxisMinValue='0' yAxisMaxValue='10'  pieRadius='150' pieSliceDepth='20' basefontsize='12' rotateNames='0' shadowXShift='4' shadowYShift='4' slicingDistance='50' pieYScale='100' showPercentValues='1' >");
        Hashtable hashtable = new Hashtable();
        decimal num = 0.02M;
        bool flag = true;
        if (str == "lcmc")
        {
            int num2 = 0;
            new ArrayList();
            foreach (DataRow row in wname.Rows)
            {
                DataRow[] rowArray = businessCount.Select(string.Format(" wname='{0}'", row["wname"]));
                if (str2 == "bt")
                {
                    if (businessCount.Rows.Count > 0)
                    {
                        if (Math.Round((decimal) (rowArray.Length / businessCount.Rows.Count), 2) < num)
                        {
                            if (!hashtable.ContainsKey("其他"))
                            {
                                hashtable.Add("其他", rowArray.Length);
                            }
                            else
                            {
                                int num4 = int.Parse(hashtable["其他"].ToString()) + rowArray.Length;
                                hashtable["其他"] = num4;
                            }
                        }
                        else if (!hashtable.ContainsKey(row["wname"]))
                        {
                            hashtable.Add(row["wname"], rowArray.Length);
                        }
                    }
                }
                else
                {
                    builder2.AppendFormat("<set name='{0}' value='{1}' color='{2}' />", row["wname"], rowArray.Length, picker.getFCColor());
                }
                if (rowArray.Length <= 0)
                {
                    num2++;
                }
            }
            if (str2 == "bt")
            {
                foreach (DictionaryEntry entry in hashtable)
                {
                    builder2.AppendFormat("<set name='{0}' value='{1}' color='{2}' />", entry.Key, entry.Value, picker.getFCColor());
                }
            }
            if (str2 == "bt")
            {
                if (num2 < wname.Rows.Count)
                {
                    builder.Append(builder2);
                }
                else
                {
                    flag = false;
                }
            }
            else
            {
                builder.Append(builder2);
            }
        }
        else if (str == "yxj")
        {
            Hashtable hashtable2 = new Hashtable();
            hashtable2.Add("普通", "0");
            hashtable2.Add("加急", "1");
            hashtable2.Add("特急", "2");
            int num5 = 0;
            foreach (DictionaryEntry entry2 in hashtable2)
            {
                DataRow[] rowArray2 = businessCount.Select(string.Format(" priority='{0}'", entry2.Value));
                if (str2 == "bt")
                {
                    if (businessCount.Rows.Count > 0)
                    {
                        if (Math.Round((decimal) (rowArray2.Length / businessCount.Rows.Count), 2) < num)
                        {
                            if (!hashtable.ContainsKey("其他"))
                            {
                                hashtable.Add("其他", rowArray2.Length);
                            }
                            else
                            {
                                int num7 = int.Parse(hashtable["其他"].ToString()) + rowArray2.Length;
                                hashtable["其他"] = num7;
                            }
                        }
                        else if (!hashtable.ContainsKey(entry2.Key))
                        {
                            hashtable.Add(entry2.Key, rowArray2.Length);
                        }
                    }
                }
                else
                {
                    builder2.AppendFormat("<set name='{0}' value='{1}' color='{2}'/>", entry2.Key, rowArray2.Length, picker.getFCColor());
                }
                if (rowArray2.Length <= 0)
                {
                    num5++;
                }
            }
            if (str2 == "bt")
            {
                foreach (DictionaryEntry entry3 in hashtable)
                {
                    builder2.AppendFormat("<set name='{0}' value='{1}' color='{2}' />", entry3.Key, entry3.Value, picker.getFCColor());
                }
            }
            if (str2 == "bt")
            {
                if (num5 < hashtable2.Count)
                {
                    builder.Append(builder2);
                }
                else
                {
                    flag = false;
                }
            }
            else
            {
                builder.Append(builder2);
            }
        }
        else if (str == "blqk")
        {
            Hashtable hashtable3 = new Hashtable();
            hashtable3.Add("正常", " exusedtime < exalarmtime ");
            hashtable3.Add("超时", " exusedtime > extotaltime ");
            hashtable3.Add("警告", " exusedtime>=exalarmtime and exusedtime <= extotaltime ");
            int num8 = 0;
            foreach (DictionaryEntry entry4 in hashtable3)
            {
                DataRow[] rowArray3 = businessCount.Select(entry4.Value.ToString());
                if (str2 == "bt")
                {
                    if (businessCount.Rows.Count > 0)
                    {
                        if (Math.Round((decimal) (rowArray3.Length / businessCount.Rows.Count), 2) < num)
                        {
                            if (!hashtable.ContainsKey("其他"))
                            {
                                hashtable.Add("其他", rowArray3.Length);
                            }
                            else
                            {
                                int num10 = int.Parse(hashtable["其他"].ToString()) + rowArray3.Length;
                                hashtable["其他"] = num10;
                            }
                        }
                        else if (!hashtable.ContainsKey(entry4.Key))
                        {
                            hashtable.Add(entry4.Key, rowArray3.Length);
                        }
                    }
                }
                else
                {
                    builder2.AppendFormat("<set name='{0}' value='{1}' color='{2}'/>", entry4.Key, rowArray3.Length, picker.getFCColor());
                }
                if (rowArray3.Length <= 0)
                {
                    num8++;
                }
            }
            if (str2 == "bt")
            {
                foreach (DictionaryEntry entry5 in hashtable)
                {
                    builder2.AppendFormat("<set name='{0}' value='{1}' color='{2}' />", entry5.Key, entry5.Value, picker.getFCColor());
                }
            }
            if (str2 == "bt")
            {
                if (num8 < hashtable3.Count)
                {
                    builder.Append(builder2);
                }
                else
                {
                    flag = false;
                }
            }
            else
            {
                builder.Append(builder2);
            }
        }
        builder.Append("</graph>");
        string str3 = "";
        if (flag)
        {
            switch (str2)
            {
                case "bt":
                    str3 = FusionCharts.RenderChart("../ScriptFile/FusionCharts/FCF_Pie3D.swf", "", builder.ToString(), "wrokflowsum", "700", "500", false, true);
                    break;

                case "zzt":
                    str3 = FusionCharts.RenderChart("../ScriptFile/FusionCharts/FCF_Bar2D.swf", "", builder.ToString(), "wrokflowsum", "700", "500", false, true);
                    break;
            }
        }
        else
        {
            str3 = "无数据，无法加载!";
        }
        this.FCLiteral1.InnerHtml = str3;
    }
}

 
 
