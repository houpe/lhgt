﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Business.FlowOperation;
using Business;
using Business.Admin;

public partial class Xnjc_Bjdb : PageBase
{
    public String s_worktype = string.Empty;
    public String d_s = string.Empty;
    public String d_e = string.Empty;
    public String type = string.Empty;
    public string strHtml = string.Empty;

    protected void Page_Load(object sender, System.EventArgs e)
    {
        BindWorkFlow();
        if(!IsPostBack)
        {            
            BindData();
        }
    }

    private void BindWorkFlow()
    {
        ClsUserWorkFlow clsWorkFlow = new ClsUserWorkFlow();
        DataTable dtSource = clsWorkFlow.GetFlowNameByUserID(UserId);

        if (s_worktype.Equals("1"))
        {
            strHtml += "<option value= 1 SELECTED>所有类型</option>" + "\r\n";
        }
        else
        {
            strHtml += "<option value= 1 >所有类型</option>" + "\r\n";
        }

        foreach (System.Data.DataRow drSource in dtSource.Rows)
        {
            String name = drSource[0].ToString();

            if (s_worktype.Equals(name))
            {
                strHtml += "<option value=\"" + name + "\" selected>" + name + "</option>" + "\r\n";
            }
            else
            {
                strHtml += "<option value=\"" + name + "\" >" + name + "</option>" + "\r\n";
            }
        }
    }

    private void BindData()
    {
        s_worktype = Request["s_worktype"];
        d_s = Request["Date_begin"];
        d_e = Request["Date_end"];
        type = Request["st_lctype"];
        if (string.IsNullOrEmpty(d_s))
        {
            d_s = string.Empty;
        }
        if (string.IsNullOrEmpty(d_e))
        {
            d_e = string.Empty;
        }


        if (string.IsNullOrEmpty(type))
        {
            type = "2";
        }
        if (string.IsNullOrEmpty(s_worktype))
        {
            s_worktype = "1";
        }

        ClsUserWorkFlow clsWorkFlow = new ClsUserWorkFlow();
        DataTable dtSerial = clsWorkFlow.GetAllInstanceStatusInfo(s_worktype, d_s, d_e, type, UserId);

        CustomGridView1.DataSource = dtSerial;
        CustomGridView1.RecordCount = dtSerial.Rows.Count;
        CustomGridView1.DataBind();
    }

    protected void CustomGridView1_OnLoadData(object sender, EventArgs e)
    {
        BindData();
    }

    protected void CustomGridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        switch (e.Row.RowType)
        {
            case DataControlRowType.DataRow:
                DataRowView dtrv = e.Row.DataItem as DataRowView;
                if (dtrv == null)
                {
                    break;
                }
                break;
        }
    }

}
