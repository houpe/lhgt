using System.Web.UI;
using System.Data;
using System.Collections;
using System.Text;
using WF_Business;
using System;
using Business.FlowOperation;
using Business.Admin;
using Common;

public partial class Xnjc_WorkflowUserPotency : Page
{
    public string html;
    public int ico;
    // Methods
    private void BindData()
    {
        string str = base.Request["hidYwlx"];
        DataTable usersByWids = BusinessStatistics.GetUsersByWids(str);
        DataTable businessByWids = BusinessStatistics.GetBusinessByWids(base.Request);
        DataTable workitemByWids = BusinessStatistics.GetWorkitemByWids(base.Request);
        DataTable workitemHistByWids = BusinessStatistics.GetWorkitemHistByWids(base.Request);
        int num = 0;
        int num2 = 0;
        int num3 = 0;
        Hashtable hashtable = new Hashtable();
        StringBuilder builder = new StringBuilder();
        foreach (DataRow row in usersByWids.Rows)
        {
            num = 0;
            num3 = 0;
            num3 = 0;
            hashtable.Clear();
            DataRow[] rowArray = businessByWids.Select("status=2");
            DataRow[] rowArray2 = workitemByWids.Select(string.Format("userid='{0}'", row["userid"]));
            DataRow[] rowArray3 = workitemHistByWids.Select(string.Format("userid='{0}'", row["userid"]));
            builder.Append("<tr>");
            builder.AppendFormat("<td>{0}</td>", row["username"]);
            builder.Append("<td>");
            foreach (DataRow row2 in rowArray)
            {
                foreach (DataRow row3 in rowArray2)
                {
                    if ((row2["iid"].ToString() == row3["iid"].ToString()) && !hashtable.Contains(row2["iid"]))
                    {
                        num++;
                        hashtable.Add(row2["iid"], row2["iid"]);
                    }
                }
                foreach (DataRow row4 in rowArray3)
                {
                    if ((row2["iid"].ToString() == row4["iid"].ToString()) && !hashtable.Contains(row2["iid"]))
                    {
                        num++;
                        hashtable.Add(row2["iid"], row2["iid"]);
                    }
                }
            }
            builder.Append(this.Getformat(num));
            builder.Append("</td>");
            builder.Append("<td>");
            num = 0;
            hashtable.Clear();
            foreach (DataRow row5 in businessByWids.Select("status<>2"))
            {
                foreach (DataRow row6 in rowArray2)
                {
                    if ((row5["iid"].ToString() == row6["iid"].ToString()) && !hashtable.Contains(row5["iid"]))
                    {
                        num++;
                        hashtable.Add(row5["iid"], row5["iid"]);
                    }
                }
                foreach (DataRow row7 in rowArray3)
                {
                    if ((row5["iid"].ToString() == row7["iid"].ToString()) && !hashtable.Contains(row5["iid"]))
                    {
                        num++;
                        hashtable.Add(row5["iid"], row5["iid"]);
                    }
                }
            }
            builder.Append(this.Getformat(num));
            builder.Append("</td>");
            builder.Append("<td>");
            foreach (DataRow row8 in rowArray3)
            {
                num2 += DataFormat.S2Int(row8["exusedtime"].ToString());
                num3 += DataFormat.S2Int(row8["extotaltime"].ToString());
            }
            if (rowArray3.Length > 0)
            {
                string surplusWorkDay = ClsWorkDaySet.GetSurplusWorkDay(Math.Ceiling((decimal) (num2 / rowArray3.Length)));
                builder.Append(this.Getformat(surplusWorkDay));
            }
            else
            {
                builder.Append("");
            }
            builder.Append("</td>");
            builder.Append("<td>");
            if (rowArray3.Length > 0)
            {
                string str3 = ClsWorkDaySet.GetSurplusWorkDay(Math.Ceiling((decimal)(num3 / rowArray3.Length)));
                builder.Append(this.Getformat(str3));
            }
            else
            {
                builder.Append("");
            }
            builder.Append("</td>");
            builder.Append("</tr>");
        }
        this.html = builder.ToString();
    }

    private void BindSelect()
    {
        DataTable newWorkflowNoFree = ClsUserWorkFlow.GetNewWorkflowNoFree();
        DataRow row = newWorkflowNoFree.NewRow();
        row["wid"] = "-1";
        row["wname"] = "ȫ������";
        newWorkflowNoFree.Rows.InsertAt(row, 0);
        this.selYwlx.DataSource = newWorkflowNoFree;
        this.selYwlx.DataTextField = "wname";
        this.selYwlx.DataValueField = "wid";
        this.selYwlx.DataBind();
    }

    private string Getformat(object num)
    {
        string str = string.Empty;
        if (num.ToString() != "0")
        {
            return num.ToString();
        }
        return str;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!base.IsPostBack)
        {
            PageBase.SetDefineText(this.Controls);
            this.BindSelect();
        }
        if (base.Request.Form.Count > 0)
        {
            this.ico = 1;
            this.BindData();
        }
    }
}


 
