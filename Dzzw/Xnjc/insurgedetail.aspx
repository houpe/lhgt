﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="insurgedetail.aspx.cs" Inherits="Xnjc_insurgedetail" %>

<%@ Import Namespace="Business" %>
<html>
<head runat="server">
    <title>督办明细信息查看</title>
    <link href="../css/page.css" rel="stylesheet" type="text/css" />
    <script language="javascript">
        function ViewInstance(Instance) {
            var strURL = "../QueryAndCollect/TaskProcessView.aspx?iid=" + Instance;
            window.open(strURL, 'viewer', "fullscreen=1,scrollbars=0;");
        }
        function ViewInstanceFrm(Instance) {
            var strURL = "../QueryAndCollect/viewworkflow.aspx?IID=" + Instance;
            window.location.href = strURL;
        }
    </script>
</head>
<body>
    <form name="del" method="post" action="" onsubmit="">
    <table class="tab_top" width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td width="3%">
                <img src="../images/tab_top_left.gif" width="17" height="35" />
            </td>
            <td>
                <div class="tab_top_text3">
                    详细信息</div>
                <div class="tab_top_text2" onclick="ViewInstanceFrm(<%=id%>)" style="cursor: hand;">
                    流程明细</div>
            </td>
            <td align="right">
                <label>
                    <input class="input" type="button" value="案件信息" onclick="ViewInstance(<%=id%>)" id="butonn">
                </label>
                <label>
                    <input type="button" class="input" value="返回" onclick='window.navigate("Bjdb.aspx?s_worktype=<%=s_worktype1%>&Date_begin=<%=d_s%>&Date_end=<%=d_e%>&st_lctype=<%=s_type%>")'>
                </label>
            </td>
            <td width="1%" align="right">
                <img src="../images/tab_top_right.gif" width="5" height="35" />
            </td>
        </tr>
    </table>
    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="tab">
        <tr>
            <th nowrap>
                岗位
            </th>
            <th nowrap>
                经办人
            </th>
            <th nowrap>
                提交时间
            </th>
            <th nowrap>
                办理时间
            </th>
            <th nowrap>
                是否超时
            </th>
        </tr>
        <%=strhtml%>
    </table>
    </form>
</body>
</html>
