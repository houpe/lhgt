﻿using SbBusiness.Wsbs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Xnjc_WorkflowStatus : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if( !IsPostBack)
        {
            InitData();
        }
    }

    public void InitData()
    {
        //设置待办件的地址
        string strMsg = "<script>";

        string strUserId = "";// Session["UserId"].ToString();
        //定义定时获取待办事件
        string strNewestDkxx = "function GetNewestDkxx(){WebService(Common.GetServicePath() + \"MOAService.asmx/GetWorkflowStatus\", AppearNewestDkxx, \"{'strUserId':'" + strUserId + "'}\");}";
        strMsg += strNewestDkxx;

        //脚本结束信息
        strMsg += "</script>";
        Response.Write(strMsg);
    }

}