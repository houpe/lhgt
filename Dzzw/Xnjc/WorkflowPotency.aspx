<%@ Page Language="C#" AutoEventWireup="true" Inherits="Xnjc_WorkflowPotency"
    CodeFile="WorkflowPotency.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>业务效能分析</title>
    <link rel="stylesheet" type="text/css" href="../css/page.css" />
    <link rel="stylesheet" type="text/css" href="../css/default/easyui.css" />
    <link rel="stylesheet" type="text/css" href="../css/icon.css" />
    <script type="text/javascript" src="../ScriptFile/jquery.js"></script>
    <script type="text/javascript" src="../ScriptFile/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../ScriptFile/FusionCharts/FusionCharts.js"></script>
    <script src="../ScriptFile/easyui-lang-zh_CN.js" type="text/javascript"></script>
    <script src="../ScriptFile/DyForm/commonFun.js" type="text/javascript"></script>
</head>
<body>
    <script type="text/javascript">
    function changeSelected(cur){
           // $(cur).children("option:selected").attr("selected",false);
            $(cur).val(cur.value);
            $("#hidLcmc").val("'"+cur.value+"'");
    }
    function formSubmit()
    {
        var ywlx = $("#selYwlx").val();
        if(null == ywlx || ywlx==''){  
            alert('请选择案件类型');
            return;
        }
        if(ywlx.length>=2){
            alert('只能选择一个案件');
            return;
        }
         $("#hidLcmc").val("'"+ywlx+"'");
        $("#form1").submit();
    }
    function onKeyDown(event)
    {
        event = event ? event : window.event;
        var target =  event.srcElement ? event.srcElement : event.target;
        var v = target.value;
        if (event.ctrlKey)
        {
    //        event.keyCode=0;     
    //        event.onkeydown=false;
            $(target).children("option:selected").attr("selected",false);
            $(target).val(v);
        }
    }
    $(function(){
        SetDateBox($("#txtBtime"));
        SetDateBox($("#txtEtime"));
         $(".datebox :text").attr("readonly","readonly");
        if(<%=ico %>==0){
            if($("#selYwlx").children().size()>0){
                if($("#selYwlx").children("option:selected").size()<=0){
                    $("#selYwlx").children().eq(0).attr("selected",true);
                }
               formSubmit();
            }
        }
    });
    </script>
    <form id="form1" runat="server">
    <table class="tab" style="width: 100%" border="1" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="2" style="vertical-align: top; width: 200px;">
                <table style="width: 100%;" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <b>
                                <asp:Label ID="WorkflowUserPotency_WorkFlowType" runat="server" Text="案件类型"></asp:Label></b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <select style="width: 100%; min-height: 300px;" size="20" id="selYwlx" name="selYwlx"
                                multiple="true" runat="server" onchange="changeSelected(this)" onkeydown="onKeyDown(event);">
                            </select>
                            <input type="hidden" id="hidLcmc" name="hidLcmc" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>
                                <asp:Label ID="WorkflowUserPotency_timespan" runat="server" Text="案件接收时间"></asp:Label></b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            从<input id="txtBtime" runat="server" name="txtBtime" class="easyui-datebox" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            至<input id="txtEtime" runat="server" class="easyui-datebox" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="button" id="btnClear" name="btnClear" class="input" value="清空" onclick="clearDate()" />
                            <script type="text/ScriptFile">
                                function clearDate() {
                                    $("#txtBtime").datebox('clear');
                                    $("#txtEtime").datebox('clear');
                                }
                            </script>
                            <input type="button" id="btnSub" name="btnSub" class="input" value="提交" onclick="return formSubmit();" />
                        </td>
                    </tr>
                </table>
            </td>
            <td style="vertical-align: top; text-align: center; vertical-align: middle;" runat="server"
                id="FCLiteral1">
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
