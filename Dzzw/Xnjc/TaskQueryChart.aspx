﻿<%@ Page Language="c#" Inherits="Xnjc_TaskQueryChart" CodeFile="TaskQueryChart.aspx.cs" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head runat="server">
    <title>查询图表</title>
    <script type="text/javascript" src="../ScriptFile/jquery.js"></script>
    <script type="text/javascript" src="../ScriptFile/DataCollect/echarts/esl.js"></script>
    <script type="text/javascript" src="../ScriptFile/DataCollect/MyEcharts.js"></script>
    <script type="text/javascript" src="../ScriptFile/DataCollect/WapCharts.js"></script>
    <script type="text/javascript" src="../ScriptFile/RequestParams.js"></script>
</head>
<body>
    <script type="text/javascript">

        function getCollectData(strDivId, strType, strCollectType, strWname) {
            $.ajax({
                url: "DataCollectHandler.ashx",
                data: { collecttype: strCollectType, dataType: strType, wname: strWname },
                cache: false,
                async: false,
                dataType: 'json',

                success: function (data) {
                    if (data) {
                        DrawPie(data, strDivId);
                    }
                },

                error: function (msg) {
                    alert("系统发生错误");
                }

            });
        }

        //采用js统计数据
        $(document).ready(function () {
            var strWname = "";
            if (request("wname") != "" && request("wname") != "undefined") {
                strWname = decodeURI(request("wname"));
            }
            getCollectData("echart1", "1", "pie", strWname);
            getCollectData("echart2", "2", "pie", strWname);
            getCollectData("echart3", "3", "pie", strWname);
        });
    </script>
    <div style="width: 600px; height: 300px; border: solid 1px; float: left">
        <div class='tab_top'>业务类型统计</div>
        <div id="echart1" style="width: 100%; height: 300px;"></div>

    </div>
    <div style=" padding-left:10px; width: 450px; height: 300px; border: solid 1px; float: left">
        <div class='tab_top'>优先级统计</div>
        <div id="echart2" style="width: 100%; height: 300px;"></div>
    </div>
    <div style="width: 1060px; height: 300px; border: solid 1px; float: left">
        <div class='tab_top'>月接件量统计</div>
        <div id="echart3" style="width: 100%; height: 300px;"></div>
    </div>

</body>
</html>
