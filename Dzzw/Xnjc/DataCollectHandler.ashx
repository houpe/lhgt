﻿<%@ WebHandler Language="C#" Class="DataCollectHandler" %>

using System;
using System.Web;
using System.Web.Script.Serialization;
using System.Data;
using System.Collections;
using System.Collections.Generic;

public class DataCollectHandler : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/plain";
        GetChartData(context);        
    }

    public void GetChartData(HttpContext context)
    {
        Business.FlowOperation.ClsUserWorkFlow Show = new Business.FlowOperation.ClsUserWorkFlow();
        DataTable dtSource = new DataTable();

        string strCollectType = context.Request["collecttype"];
        string strDataType = context.Request["dataType"];
        string strWname = context.Request["wname"];
        string strSqlWhere = string.Empty;
        List<object> lists = new List<object>();
        
        if( !string.IsNullOrEmpty(strWname))
        {
            strSqlWhere = string.Format(" and w.wname='{0}'",strWname);
        }

        dtSource = Show.ShowCollectDataByChart(strSqlWhere,strDataType);
        
        switch (strDataType)
        {
            case "1"://按流程统计
                foreach (System.Data.DataRow dr in dtSource.Rows)
                {
                    var obj = new { name = dr["wname"], value = dr["count"] };
                    lists.Add(obj);
                }
                break;
            case "2"://按优先级统计
                foreach (System.Data.DataRow dr in dtSource.Rows)
                {
                    var obj = new { name = dr["priority"], value = dr["count"] };
                    lists.Add(obj);
                }
                break;
            case "3"://按月统计
                foreach (System.Data.DataRow dr in dtSource.Rows)
                {
                    var obj = new { name = dr["themonth"], value = dr["count"] };
                    lists.Add(obj);
                }
                break;
            case "4"://按状态统计
                foreach (System.Data.DataRow dr in dtSource.Rows)
                {
                    var obj = new { name = dr["status"], value = dr["count"] };
                    lists.Add(obj);
                }
                break;
        };

        JavaScriptSerializer jsS = new JavaScriptSerializer();
        string result = jsS.Serialize(lists);
        context.Response.Write(result);
    }

    public bool IsReusable {
        get {
            return false;
        }
    }

}