﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Xnjc_WorkflowQuantity"
    CodeFile="WorkflowQuantity.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>业务量分析</title>
    <link rel="stylesheet" type="text/css" href="../css/page.css" />
    <link rel="stylesheet" type="text/css" href="../css/default/easyui.css" />
    <link rel="stylesheet" type="text/css" href="../css/icon.css" />
    <script type="text/javascript" src="../ScriptFile/jquery.js"></script>
    <script type="text/javascript" src="../ScriptFile/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../ScriptFile/FusionCharts/FusionCharts.js"></script>
    <script src="../ScriptFile/easyui-lang-zh_CN.js" type="text/javascript"></script>
    <script src="../ScriptFile/DyForm/commonFun.js" type="text/javascript"></script>
</head>
<body>
    <script type="text/javascript">
    function changeSelected(cur){
        if(cur.value=="-1"){
            $("#selYwlx option:selected").attr("selected",false);
            $("#selYwlx").children().eq(0).attr("selected",true);
        }else{
            $("#selYwlx").children().eq(0).attr("selected",false);
        }
            
    }
    function formSubmit()
    {
        var str='';
        $("#selYwlx option:selected").each(function(){
            if($(this).val()=='-1'){
                str = ',-1';
                return;
            }else{
                str += ",'"+$(this).val()+"'";
            }
        });
        if(str.length>0)
            str = str.substring(1);
        $("#hidYwlx").val(str);
        //var date1 = $("#txtBtime").datebox('getValue');
        //var date2 = $("#txtEtime").datebox('getValue');
        
        $("#form1").submit();
    }
    $(function(){
        SetDateBox($("#txtBtime"));
        SetDateBox($("#txtEtime"));
        $(".datebox :text").attr("readonly","readonly");
        if(<%=ico %>==0){
            if($("#selYwlx").children().size()>0){
                if($("#selYwlx").children("option:selected").size()<=0){
                $("#selYwlx").children().eq(0).attr("selected",true);
            }
            formSubmit();
            }
        }

    });
    </script>
    <form id="form1" runat="server">
    <table class="tab" style="width: 100%" border="1" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="2" style="width: 20%; vertical-align: top;">
                <table style="width: 200px;" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <b>
                                <asp:Label ID="WorkflowUserPotency_WorkFlowType" runat="server" Text="案件类型"></asp:Label></b>
                        </td>
                    </tr>
                    <tr>
                        <td style="min-height: 310px;">
                            <select style="width: 100%; min-height: 300px;" size="20" id="selYwlx" name="selYwlx"
                                multiple="true" runat="server" onchange="changeSelected(this)">
                            </select>
                            <input type="hidden" id="hidYwlx" name="hidYwlx" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>
                                <asp:Label ID="WorkflowUserPotency_timespan" runat="server" Text="案件接收时间"></asp:Label></b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            从<input id="txtBtime" runat="server" name="txtBtime" class="easyui-datebox"></input>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            至<input id="txtEtime" runat="server" class="easyui-datebox"></input>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="button" id="btnClear" name="btnClear" class="input" value="清空" onclick="clearDate()" />
                            <script type="text/ScriptFile">
                                function clearDate() {
                                    $("#txtBtime").datebox('clear');
                                    $("#txtEtime").datebox('clear');
                                }
                            </script>
                            <input type="button" id="btnSub" name="btnSub" class="input" value="提交" onclick="formSubmit();" />
                        </td>
                    </tr>
                </table>
            </td>
            <td style="vertical-align: top; text-align: left; height: 20px;">
                <div style="margin-top: 5px; margin-left: 20px; margin-bottom: 5px;">
                    统计方式<select id="selTjfs" name="selTjfs" runat="server" onchange="formSubmit();">
                        <option value="lcmc">流程名称</option>
                        <option value="yxj">优先级</option>
                        <option value="blqk">办理情况</option>
                    </select>
                    图形<select id="selTx" name="selTx" runat="server" onchange="formSubmit();">
                        <option value="zzt">柱状图</option>
                        <option value="bt">饼图</option>
                    </select>
                </div>
            </td>
        </tr>
        <tr>
            <td style="vertical-align: top; text-align: center; vertical-align: middle;" runat="server"
                id="FCLiteral1">
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
