﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WorkflowStatus.aspx.cs" Inherits="Xnjc_WorkflowStatus" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>业务情况一览</title>
    <link href="../css/page.css" rel="stylesheet" />

    <script type="text/javascript" src="../ScriptFile/jquery.js"></script>

    <script type="text/javascript" src="../ScriptFile/JqueryUi/jquery.ui.core.js"></script>
    <script type="text/javascript" src="../ScriptFile/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../ScriptFile/cookie.js"></script>
    <script type="text/javascript" src="../ScriptFile/Common/JqueryDragInterface.js"></script>

    <script src="../ScriptFile/Common/Jquery.Common.js"></script>
    <script type="text/javascript" src="../ScriptFile/Common/Jquery.Global.js"></script>

    <script type="text/javascript">
    
        //显示最新的投标信息(竖向列表)
        function AppearNewestDkxx(result) {
            if (result != "") {
                json = result.toJson(); //格式化
                $("#divCrMsg").html("");

                for (var i = 0; i < json.length; i++) {
                    o1 = json[i];

                    var divTemp = $("<div id='div_" + i + "' style='margin:10px;width:320px;float:left; text-align:center'></div>");
                    var table = $("<table cellspacing='0' style='width:100%;' cellpadding='1' class='tab'></table>");
                    //添加序号抬头
                    var headTitle = $("<div class='tab_top'>[" + (i + 1) + "]</div>");
                    headTitle.appendTo(divTemp);
                    for (key in o1) {
                        var row = $("<tr></tr>");

                        var tdHead = $("<td style='width:150px;'></td>");
                        var tdContent = $("<td></td>");

                        if (key == "wname") {
                            tdHead.text("业务名称：");
                            //显示报名信息或报价信息
                            if (o1["blzt"].toString() == "正在办理") {
                                var aBaoj = $("<a href='../MyWindow/TaskInStepList.aspx?wname=" + o1["wname"].toString() + "')\"></a>");
                                aBaoj.text(o1[key]);
                                aBaoj.appendTo(tdContent);
                            }
                            else
                            {
                                tdContent.text(o1[key].toString());
                            }
                        }
                        else if (key == "blzt") {
                            tdHead.text("办理状态：");
                            tdContent.text(o1[key].toString());
                        }
                        else if (key == "bjs") {
                            tdHead.text("办件数：");
                            tdContent.text(o1[key].toString());
                        }
                        else if (key == "pjblsj") {
                            tdHead.text("平均办理时间：");
                            tdContent.text(o1[key].toString());
                        }
                        tdHead.appendTo(row);

                        //添加内容           
                        tdContent.appendTo(row);

                        //行加入到表格
                        row.appendTo(table);
                    }

                    table.appendTo(divTemp);
                    divTemp.appendTo($("#divCrMsg"));
                }

                //提取cookie的值，并按照保存的顺序移动div，重设html的代码顺序
                ChangeDiv("divCrMsg");
            }
        }

        $(document).ready(function () {
            //设置可拖动层
            var leftmod = new Array();
            $(".tab_top").css("cursor", "move");
            $("#divCrMsg").sortable({
                //connectWith: '.box_box01',
                placeholder: 'box_boxNew',

                opacity: 0.7, //模糊效果值
                //revert: true,
                stop: function (event, ui) {
                    saveLayout("divCrMsg");
                }
            });
            $("#divCrMsg").disableSelection();

            //展示統計表
            GetNewestDkxx();
            //var MyMar = setInterval(GetNewestDkxx, 1000); //定时(1分钟)扫描待办件
        });

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="divCrMsg" style="text-align: center; margin-top:10px; margin-left:10px;">
        </div>
    </form>
</body>
</html>
