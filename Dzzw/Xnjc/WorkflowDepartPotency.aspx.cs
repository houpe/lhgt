using System.Web.UI;
using System.Data;
using System.Text;
using System.Collections;
using System;
using Business.FlowOperation;
using InfoSoftGlobal;
using Business.Common;
using Business;
using Business.Admin;

public partial class Xnjc_WorkflowDepartPotency : Page
{
    // Fields
    protected string html = string.Empty;
    protected int ico;

    // Methods
    private void BindData()
    {
        DataTable depList = RequestFlowOperation.GetDepList();
        Hashtable hashtable = new Hashtable();
        foreach (DataRow row in depList.Rows)
        {
            if (!hashtable.ContainsKey(row["DEPARTID"]))
            {
                hashtable.Add(row["DEPARTID"], row["DEPART_NAME"]);
            }
        }
        string wids = base.Request["hidYwlx"];
        DataTable usersDepByWids = BusinessStatistics.GetUsersDepByWids(wids);
        DataTable businessByWids = BusinessStatistics.GetBusinessByWids(base.Request);
        DataTable workitemByWids = BusinessStatistics.GetWorkitemByWids(base.Request);
        DataTable workitemHistByWids = BusinessStatistics.GetWorkitemHistByWids(base.Request);
        Hashtable hashtable2 = new Hashtable();
        int num = 0;
        int num2 = 0;
        int num3 = 0;
        int num4 = 0;
        int num5 = 0;
        StringBuilder builder = new StringBuilder();
        Hashtable hashtable3 = new Hashtable();
        foreach (DataRow row2 in usersDepByWids.Rows)
        {
            if (!hashtable3.ContainsKey(row2["order_id"]) && hashtable.ContainsKey(row2["order_id"]))
            {
                hashtable3.Add(row2["order_id"], row2["order_id"]);
            }
        }
        foreach (DictionaryEntry entry in hashtable3)
        {
            DataRow[] rowArray4 = usersDepByWids.Select(string.Format("order_id='{0}'", entry.Key));
            hashtable2.Clear();
            num = 0;
            num2 = 0;
            num4 = 0;
            num3 = 0;
            num5 = 0;
            DataRow[] rowArray = businessByWids.Select("status=2");
            foreach (DataRow row3 in rowArray4)
            {
                DataRow[] rowArray2 = workitemByWids.Select(string.Format("userid='{0}'", row3["userid"]));
                DataRow[] rowArray3 = workitemHistByWids.Select(string.Format("userid='{0}'", row3["userid"]));
                foreach (DataRow row4 in rowArray)
                {
                    foreach (DataRow row5 in rowArray2)
                    {
                        if ((row4["iid"].ToString() == row5["iid"].ToString()) && !hashtable2.Contains(row4["iid"]))
                        {
                            num++;
                            hashtable2.Add(row4["iid"], row4["iid"]);
                        }
                    }
                    foreach (DataRow row6 in rowArray3)
                    {
                        if ((row4["iid"].ToString() == row6["iid"].ToString()) && !hashtable2.Contains(row4["iid"]))
                        {
                            num++;
                            hashtable2.Add(row4["iid"], row4["iid"]);
                        }
                    }
                }
                hashtable2.Clear();
                rowArray = businessByWids.Select("status<>2");
                foreach (DataRow row7 in rowArray)
                {
                    foreach (DataRow row8 in rowArray2)
                    {
                        if ((row7["iid"].ToString() == row8["iid"].ToString()) && !hashtable2.Contains(row7["iid"]))
                        {
                            num2++;
                            hashtable2.Add(row7["iid"], row7["iid"]);
                        }
                    }
                    foreach (DataRow row9 in rowArray3)
                    {
                        if ((row7["iid"].ToString() == row9["iid"].ToString()) && !hashtable2.Contains(row7["iid"]))
                        {
                            num2++;
                            hashtable2.Add(row7["iid"], row7["iid"]);
                        }
                    }
                }
                foreach (DataRow row10 in rowArray3)
                {
                    num3 += Convert.ToInt32(row10["exusedtime"].ToString());
                    num4 += Convert.ToInt32(row10["extotaltime"].ToString());
                }
                num5++;
            }
            builder.Append("<tr>");
            builder.AppendFormat("<td>{0}</td>", hashtable[entry.Key]);
            builder.Append("<td>");
            builder.Append(this.Getformat(num));
            builder.Append("</td>");
            builder.Append("<td>");
            builder.Append(this.Getformat(num2));
            builder.Append("</td>");
            builder.Append("<td>");
            if (num5 > 0)
            {
                string surplusWorkDay = ClsWorkDaySet.GetSurplusWorkDay(Math.Ceiling((decimal) (num4 / num5)));
                builder.Append(this.Getformat(surplusWorkDay));
            }
            else
            {
                builder.Append("&nbsp;");
            }
            builder.Append("</td>");
            builder.Append("<td>");
            if (num5 > 0)
            {
                string str3 = ClsWorkDaySet.GetSurplusWorkDay(Math.Ceiling((decimal) (num3 / num5)));
                builder.Append(this.Getformat(str3));
            }
            else
            {
                builder.Append("&nbsp;");
            }
            builder.Append("</td>");
            builder.Append("</tr>");
        }
        this.html = builder.ToString();
    }

    private void BindSelect()
    {
        DataTable newWorkflowNoFree = ClsUserWorkFlow.GetNewWorkflowNoFree();
        DataRow row = newWorkflowNoFree.NewRow();
        row["wid"] = "-1";
        row["wname"] = "ȫ������";
        newWorkflowNoFree.Rows.InsertAt(row, 0);
        this.selYwlx.DataSource = newWorkflowNoFree;
        this.selYwlx.DataTextField = "wname";
        this.selYwlx.DataValueField = "wid";
        this.selYwlx.DataBind();
    }

    private string Getformat(object num)
    {
        if (num.ToString() != "0")
        {
            return num.ToString();
        }
        return "&nbsp;";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!base.IsPostBack)
        {
            PageBase.SetDefineText(this.Controls);
            this.BindSelect();
        }
        if (base.Request.Form.Count > 0)
        {
            this.ico = 1;
            this.BindData();
        }
    }
}

