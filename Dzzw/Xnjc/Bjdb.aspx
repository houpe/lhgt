﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Bjdb.aspx.cs" Inherits="Xnjc_Bjdb" %>

<html>
<head runat="server">
    <title>办件督办</title>
    <link href="../css/page.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" id="skinCss" type="text/css" href="../css/default/easyui.css" />
    <link rel="stylesheet" type="text/css" href="../css/icon.css" />
    <script type="text/javascript" src="../ScriptFile/jquery.js"></script>
    <script type="text/javascript" src="../ScriptFile/jquery.easyui.min.js"></script>
    <script src="../ScriptFile/easyui-lang-zh_CN.js" type="text/javascript"></script>
    <script type="text/javascript">

        function tosubmit() {
            var strUrl = "Bjdb.aspx";
            strUrl += "?Date_begin=" + $("#Date_begin").datebox('getValue');
            strUrl += "&Date_end=" + $("#Date_end").datebox('getValue');
            strUrl += "&s_worktype=" + document.all.s_worktype.value;
            strUrl += "&st_lctype=" + document.all.st_lctype.value;
            window.location.href = strUrl;
        }

        function ViewChart() {
            var URL = "TaskQueryChart.aspx";
            this.location.href = URL;
            //window.navigate(URL);
        }
    </script>
</head>
<body>
    <form runat="server">
        <div class="tab_top">
            <div>
                业务类型：<select name="s_worktype" style="width: 150px">
                    <%=strHtml%>
                </select>&nbsp;
                从<input class="easyui-datebox" style="width: 200px;" id="Date_begin" />
                到<input class="easyui-datebox" style="width: 200px;" id="Date_end" />
                <select name="st_lctype">
                    <option value="1" <%=((type.Equals("1"))?"selected":"")%>>办结</option>
                    <option value="2" <%=((type.Equals("2"))?"selected":"")%>>未办结</option>
                    <option value="21" <%=((type.Equals("21"))?"selected":"")%>>业务延期</option>
                    <option value="31" <%=((type.Equals("31"))?"selected":"")%>>岗位延期</option>
                    <option value="41" <%=((type.Equals("41"))?"selected":"")%>>挂起</option>
                </select>
                <input type="button" onclick="tosubmit()" class="NewButton" id="btnSubmit" value="查询" />
                <input type="button" value="显示图表" class="NewButton" onclick="ViewChart()" />

            </div>
        </div>
        <controlDefine:CustomGridView ID="CustomGridView1" runat="server" AllowPaging="True"
            SkinID="List" AutoGenerateColumns="false"
            ShowItemNumber="true"  ShowHideColModel="None" OnOnLoadData="CustomGridView1_OnLoadData"
            OnRowCreated="CustomGridView1_RowCreated" >
            <Columns>
                <asp:BoundField DataField="iid" HeaderText="业务编号"></asp:BoundField>
                <asp:BoundField DataField="wname" HeaderText="业务类型"></asp:BoundField>
                <asp:BoundField DataField="name" HeaderText="申请人"></asp:BoundField>
                <asp:BoundField DataField="accepted_time" HeaderText="收件时间"></asp:BoundField>
                <asp:BoundField DataField="curStepName" HeaderText="当前岗位"></asp:BoundField>
                <asp:BoundField DataField="curUserName" HeaderText="在办人"></asp:BoundField>
                <asp:BoundField DataField="ExTotalTime" HeaderText="办理时限"></asp:BoundField>
                <asp:BoundField DataField="Exusedtime" HeaderText="已用时间"></asp:BoundField>
                <asp:TemplateField HeaderText="详细信息">
                    <ItemTemplate>
                        <a href='InsUrgeDetail.aspx?id=<%#Eval("iid") %>&search=detail?'>
                            <%#Eval("iid") %></a>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerSettings Visible="False" />
        </controlDefine:CustomGridView>
    </form>
</body>
</html>
