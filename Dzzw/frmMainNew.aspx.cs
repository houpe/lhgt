﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Business.Common;

public partial class frmMainNew : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(UserName))
            {
                //获取设置标题头
                string strMsg = "<script>function initWinInfo(){";

                strMsg += string.Format("loadTitle('divCenter','业务办公区【欢迎您，{0}，今天是{1}】');HideLoading();", UserName, DateTime.Now.ToShortDateString());

                //获取待接收短消息
                Message msg = new Message();
                DataTable dt = msg.GetReceiveMessage(UserName, "", "", "");
                dt.DefaultView.RowFilter = "issended=0";
                
                string strPopContent = "<table>";
                strPopContent += string.Format("<tr><td><a href=javascript:addTab('ManageInside/getMessage.aspx','接收即时消息')>您有{0}条短消息</a></td></tr>", dt.DefaultView.ToTable().Rows.Count);

                //获取来见查阅信息
                dt = msg.GetLjcy(RealUserId);
                strPopContent += string.Format("<tr><td><a href=javascript:addTab('ManageInside/SubBrowseFile.aspx','来件查阅')>您有{0}个待查阅文件</a></td></tr>",dt.Rows.Count);
                strPopContent += "</table>";
                strMsg += string.Format("loadMsg(\"{0}\");", strPopContent);

                //针对
                //strMsg += "addTab('MyWindow/TaskInStepList.aspx', '我的待办');";
                strMsg += "}</script>";

                Response.Write(strMsg);
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "setTitle", strMsg);                
            }
        }
    }

    
}
