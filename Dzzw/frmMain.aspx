﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frmMain.aspx.cs" Inherits="frmMain" %>

<!-- 注意以下标题头-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>欢迎页</title>
    <link href="css/Style.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="scriptFile/MdiWin.js"></script>

</head>
<body leftmargin="0" topmargin="0" onload="init()" scroll="no">
    <form id="form1" runat="server">
    
    <controlDefine:PopupBox ID="PopupBox1" runat="server">
    </controlDefine:PopupBox>
    
    <table height="100%" width="100%" border="0" cellspacing="0" cellpadding="0" onselectstart="return false"
        style="table-layout: fixed">
        <tr height="25" bgcolor="#6E8ADE">
            <td valign="bottom" nowrap>
                <div style="overflow: hidden; width: 100%">
                    <div id="titlelist" style="margin-left: 0; z-index: -1">
                    </div>
                </div>
            </td>
            <td width="25" style="color: #FFFFFF; cursor: hand;">
                <span onmousedown="tabScroll('left')" onmouseup="tabScrollStop()">&lt;</span><span
                    onmousedown="tabScroll('right')" onmouseup="tabScrollStop()">&gt;</span>&nbsp;&nbsp;
            </td>
            <td width="25">
                <img src="images/close_all.gif" width="21" height="21" border="0" alt="" onclick="win.removeall()"
                    align="middle" onmouseover="this.src='images/close_all_over.gif'" onmouseout="this.src='images/close_all.gif'"
                    vspace="2" />
            </td>
            <td width="25">
                <img src="images/close.gif" width="21" height="21" border="0" alt="" onclick="win.removewin(win.currentwin)"
                    align="middle" onmouseover="this.src='images/close_over.gif'" onmousedown="this.src='images/close_down.gif'"
                    onmouseout="this.src='images/close.gif'" vspace="2" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <table style="height: 100%; width: 100%" border="0" cellspacing="5" cellpadding="0"
                    bgcolor="#ECE9D8">
                    <tr>
                        <td bgcolor="white" id="mywindows" align="center">
                            <h1 style="color: red">
                                Welcome!!
                            </h1>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    
    </form>
</body>
</html>
