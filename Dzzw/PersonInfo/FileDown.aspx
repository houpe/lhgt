﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FileDown.aspx.cs" Inherits="usermis_FileDown" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>文件下载</title>
    <link href="../css/page.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div style="text-align: center">
        <table style="width: 100%" class="tab">
            <tr>
                <th>
                    文件名称
                </th>
                <th>
                    下载
                </th>
            </tr>
            <%=strFileContent%>
        </table>
    </div>
    </form>
</body>
</html>
