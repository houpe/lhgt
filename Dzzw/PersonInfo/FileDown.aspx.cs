﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Collections.Generic;
using Business.Common;

public partial class usermis_FileDown : System.Web.UI.Page
{
    public string strFileContent;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            List<string> lstFiles = FileOperation.GetDir(Server.MapPath("../FileDown/"));            
           
            foreach (string strFile in lstFiles)
            {
                strFileContent += string.Format(@"<tr>
                    <td>
                        {0}
                    </td>
                    <td>
                        <a href='../FileDown/{0}'>下载</a>
                    </td>
                </tr>", strFile);
            }
        }
    }
}
