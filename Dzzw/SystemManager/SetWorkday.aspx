<%@ Page Language="c#" Inherits="SystemManager_SetWorkday" CodeFile="SetWorkday.aspx.cs" %>

<%@ Register Src="../UserControls/Calendar.ascx" TagName="Calendar" TagPrefix="uc1" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head runat="server">
    <title>设置工作日</title>
    <link rel="stylesheet" type="text/css" href="../css/page.css" />
</head>
<body>
    <form runat="server">
    <div align="center">
        <controlDefine:MessageBox ID="msgAppear" runat="server"></controlDefine:MessageBox>
        &nbsp; &nbsp;</div>
    <div align="center">
        <asp:Calendar ID="Calendar1" runat="server" BorderStyle="None" CaptionAlign="Top"
            DayNameFormat="Full" Height="333px" NextMonthText="下一月" OnSelectionChanged="Calendar1_SelectionChanged"
            PrevMonthText="上一月" ShowGridLines="True" UseAccessibleHeader="False" Width="661px">
            <SelectedDayStyle BackColor="SkyBlue" />
            <TodayDayStyle BackColor="Blue" />
            <SelectorStyle BackColor="SteelBlue" BorderStyle="Inset" />
            <DayStyle BackColor="SkyBlue" BorderColor="Gray" BorderStyle="Double" />
            <WeekendDayStyle BackColor="Aqua" BorderStyle="Inset" />
            <NextPrevStyle BackColor="LightBlue" BorderStyle="None" Font-Bold="True" />
            <DayHeaderStyle BackColor="LightBlue" BorderColor="#404040" BorderStyle="Solid" ForeColor="Maroon" />
            <TitleStyle BackColor="LightBlue" BorderColor="#404040" Font-Bold="True" Font-Italic="False" />
            <OtherMonthDayStyle BackColor="AliceBlue" ForeColor="LightGray" />
        </asp:Calendar>
        周末：
        <asp:TextBox ID="TextBox1" runat="server" BackColor="Cyan" BorderColor="Cyan" BorderStyle="None"
            Width="71px"></asp:TextBox>&nbsp; &nbsp; 该月已设置上班时间的日期：&nbsp;
        <asp:TextBox ID="TextBox4" runat="server" BackColor="red" BorderColor="SkyBlue" BorderStyle="None"
            ForeColor="Red" Width="61px"></asp:TextBox><br />
        <div style="width: 80%" align="center">
            <div class="box_top_bar">
                <div class="box_top_left">
                    <div class="box_top_right">
                        <div class="box_top_text">
                            工作日设置</div>
                    </div>
                </div>
            </div>
            <div class="box_middle" align="center">
                <table width="80%" border="0" cellpadding="0" cellspacing="0" class="box_middle_tab">
                    <tr>
                        <td>
                            上午上班时间：
                        </td>
                        <td>
                            <asp:TextBox ID="AmHour1" runat="server" Width="50px"></asp:TextBox>：<asp:TextBox
                                ID="AmMinute1" runat="server" Width="50px"></asp:TextBox>
                        </td>
                        <td style="width: 73px">
                            到
                        </td>
                        <td style="width: 195px">
                            <asp:TextBox ID="FmHour1" runat="server" Width="50px"></asp:TextBox>：<asp:TextBox
                                ID="FmMinute1" runat="server" Width="50px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 25px">
                            下午上班时间：
                        </td>
                        <td style="height: 25px">
                            <asp:DropDownList Width="0px" ID="FmUptime" runat="server">
                                <asp:ListItem Selected="True">13:30:00</asp:ListItem>
                                <asp:ListItem>14:00:00</asp:ListItem>
                                <asp:ListItem>14:15:00</asp:ListItem>
                                <asp:ListItem>14:30:00</asp:ListItem>
                                <asp:ListItem>15:00:00</asp:ListItem>
                            </asp:DropDownList>
                            <asp:TextBox ID="AmHour2" runat="server" Width="50px"></asp:TextBox>：<asp:TextBox
                                ID="AmMinute2" runat="server" Width="50px"></asp:TextBox>
                        </td>
                        <td style="width: 73px; height: 25px;">
                            &nbsp;到&nbsp;
                        </td>
                        <td style="width: 195px; height: 25px;">
                            <asp:DropDownList Width="0px" ID="FmdownTime" runat="server">
                                <asp:ListItem Selected="True">16:00:00</asp:ListItem>
                                <asp:ListItem>16:30:00</asp:ListItem>
                                <asp:ListItem>17:00:00</asp:ListItem>
                                <asp:ListItem>17:30:00</asp:ListItem>
                                <asp:ListItem>18:00:00</asp:ListItem>
                                <asp:ListItem>18:30:00</asp:ListItem>
                                <asp:ListItem>19:00:00</asp:ListItem>
                            </asp:DropDownList>
                            <asp:TextBox ID="FmHour2" runat="server" Width="50px"></asp:TextBox>：<asp:TextBox
                                ID="FmMinute2" runat="server" Width="50px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            请选择日期：
                        </td>
                        <td>
                            <uc1:Calendar ID="txtFromDate" runat="server" Width="112px" />
                        </td>
                        <td style="width: 73px">
                            日期类型：
                        </td>
                        <td style="width: 195px; height: 25px;">
                            <asp:RadioButton ID="workdays" GroupName="workday" Checked="true" runat="server"
                                Text="工作日" />
                            <asp:RadioButton ID="holiday" GroupName="workday" runat="server" Text="假期" />
                            &nbsp; &nbsp;<asp:Button ID="input" runat="server" CssClass="input" Text="保存设置" OnClick="input_Click" />
                        </td>
                    </tr>
                </table>
                <div>
                    往后<asp:TextBox Width="50" size="23" ID="days" runat="server"></asp:TextBox>天按此设置&nbsp;
                    &nbsp;<asp:CheckBox ID="saturday" runat="server" Text="周六休息" />&nbsp;
                    <asp:CheckBox ID="sunday" runat="server" Text="周日休息" />
                    &nbsp; &nbsp;&nbsp;&nbsp;<asp:Button ID="creatdays" runat="server" CssClass="input"
                        Text="批量生成" OnClick="creatdays_Click" />
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
