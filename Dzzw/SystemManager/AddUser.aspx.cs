using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Business;
using Business.Struct;
using Business.Admin;


/// <summary>
/// 
/// </summary>
public partial class SystemManager_AddUser : PageBase
{
    protected void Page_Load(object sender, System.EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                BindCompanyList();
            }
            catch (Exception ex)
            {
                msgAppear.Text = ex.Message;
            }
        }
    }
    /// <summary>
    /// 绑定单位
    /// </summary>
    private void BindCompanyList()
    {
        Business.Admin.DepartHandle dep = new Business.Admin.DepartHandle();
        DataTable dt = dep.GetDepartment();
        ddlCompany.DataSource = dt;
        ddlCompany.DataTextField = "depart_name";
        ddlCompany.DataValueField = "departid";
        ddlCompany.DataBind();
        BindDepartmentList(ddlCompany.SelectedValue);
    }

    /// <summary>
    /// 绑定部门
    /// </summary>
    /// <param name="depId"></param>
    private void BindDepartmentList(string depId)
    {
        Business.Admin.DepartHandle dep = new Business.Admin.DepartHandle();
        DataTable dt = dep.GetDepartment(depId);
        ddlDepartment.DataSource = dt;
        ddlDepartment.DataTextField = "depart_name";
        ddlDepartment.DataValueField = "departid";
        ddlDepartment.DataBind();
    }

    /// <summary>
    /// 添加用户
    /// </summary>
    public void AddNewUser()
    {
        UserStruct userInfo = new UserStruct();

        userInfo.UserName = txtLoginUser.Text.ToUpper();
        userInfo.UserPass = txtUserPass.Text;
        userInfo.UserRealName = txtRealName.Text;
        userInfo.UserDepartmentId = ddlDepartment.SelectedValue;

        try
        {
            if (!string.IsNullOrEmpty(userInfo.UserName) && !string.IsNullOrEmpty(userInfo.UserRealName)
                && !string.IsNullOrEmpty(userInfo.UserPass))
            {
                Business.Admin.StUserOperation.AddNewUser(userInfo);
                msgAppear.Text = "添加用户成功";
            }
            else
            {
                msgAppear.Text = "添加用户失败!用户名密码真实姓名不能为空";
            }
        }
        catch (Exception ex)
        {
            msgAppear.Text = ex.Message;
        }
    }

    /// <summary>
    /// 添加用户
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnAddUser_Click(object sender, EventArgs e)
    {
        AddNewUser();
    }

    /// <summary>
    /// 选择单位事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            BindDepartmentList(ddlCompany.SelectedValue);
        }
        catch (Exception ex)
        {
            msgAppear.Text = ex.Message;
        }
    }
}
