<%@ Page Language="c#" Inherits="SystemManager_EditGroup" CodeFile="EditGroup.aspx.cs" %>

<html>
<head runat="server">
    <title>编辑用户组</title>
    <link href="../css/page.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div style="text-align: center">
            <table>
                <tr>
                    <td align="center" style="height: 22px">
                        用户组名:</td>
                    <td>
                        <asp:TextBox ID="txtGroupName" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="text-align: center">
                        父节点角色ID:</td>
                    <td>
                        <asp:TextBox ID="txtGroupOrder" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="text-align: center">
                        可中止:</td>
                    <td style="text-align: center">
                        <asp:CheckBox ID="ckbIs_Stop" runat="server" />                    
                    </td>
                </tr>
                <tr>
                    <td align="center" style="height: 30px" colspan="2">
                        <asp:Button ID="btnOk" runat="server" Text="修改" CssClass="input" OnClick="btnOk_Click" />&nbsp;&nbsp;&nbsp;
                        <input type="button" value="放弃" class="newbutton" onclick='window.navigate("openusergrouplist.aspx")'
                            />
                    </td>
                </tr>
                <tr>
                    <td align="center" style="height: 30px">
                        <controlDefine:MessageBox ID="msgAppear" runat="server" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
