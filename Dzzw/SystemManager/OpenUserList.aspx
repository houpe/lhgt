<%@ Page Language="c#" Inherits="SystemManager_OpenUserList" CodeFile="OpenUserList.aspx.cs" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head runat="server">
    <title>用户列表</title>
    <link href="../css/page.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        function DelOK() {
            if (confirm("确定要删除该用户吗?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function AddUser() {
            window.location.href = "edituser.aspx";
        }
    </script>
</head>
<body>
    <form runat="server" action="">
    <div class="box_middle">
        <div style="text-align: right; height: 30px; padding-top: 5px;">
            &nbsp;用户姓名：<asp:TextBox ID="txtSeach" Text="" runat="server" Width="98px"></asp:TextBox>
            单位：<asp:DropDownList ID="danwei" runat="server" AutoPostBack="true" Width="155px"
                OnSelectedIndexChanged="danwei_SelectedIndexChanged">
            </asp:DropDownList>
            部门：<asp:DropDownList ID="bumen" runat="server" Width="155px">
            </asp:DropDownList>
            <asp:Button ID="btnQuery" class="input" Text="查询" runat="server"></asp:Button>
            <input type="button" class="NewButton" onclick="AddUser()" value="添加用户" />
            
    </div>
    <div id='context' runat="server">
    </div>
    </form>
</body>
</html>
