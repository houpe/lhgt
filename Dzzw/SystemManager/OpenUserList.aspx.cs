using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Business;
using WF_DataAccess;
using Business.Common;
using Business.Admin;
using System.Collections.Generic;
using System.Text;

/// <summary>
/// 人员维护功能代码迁移
/// 修改人：YZG
/// 时间：2010-03-01
/// </summary>
public partial class SystemManager_OpenUserList : PageBase
{
    Business.Admin.DepartHandle dhTemp = new Business.Admin.DepartHandle();
  
    protected void Page_Load(object sender, System.EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            BindDepList();
            DelUser();
        }
        BindUserList();
    }

    /// <summary>
    /// 绑定部门单位信息
    /// </summary>
    public void BindDepList()
    {
        if (Session["bumen"] != null)
        {
            bumen.Items.Add(Session["bumen"].ToString());
        }
        if (Session["danwei"] != null)
        {
            danwei.Items.Add(Session["danwei"].ToString());
        }

        bumen.Items.Add("部门");
        danwei.Items.Add("单位");

        DataTable dt;
        dt = dhTemp.GetDepartmentName();
        if (dt.Rows.Count > 0)
        {

            for (int i = 0; i < dt.Rows.Count; i++)
                this.danwei.Items.Add(new ListItem(dt.Rows[i][0].ToString(), dt.Rows[i][0].ToString()));
        }

    }

    /// <summary>
    /// 删除用户信息
    /// </summary>
    public void DelUser()
    {
        String deluserid = Request["deleteuser"];

        try
        {
            if (!string.IsNullOrEmpty(deluserid))
            {
                Business.Admin.StUserOperation.DeleteUserById(deluserid);
            }
        }
        catch
        {
            ExceptionManager.WriteAlert(Page, "该用户有处理业务存在，不能删除");
        }

    }

    /// <summary>
    /// 绑定用户信息
    /// </summary>
    public void BindUserList()
    {
        StringBuilder html = new StringBuilder();

        html.Append("<table class=\"tab\" width=\"100%\" border=\"1\">");
        html.Append("<tr class=\"title\" align=\"center\" valign=\"middle\">");
        html.Append("<th width=\"14%\">用户ID</th>");
        html.Append("<th width=\"14%\">用户姓名</th>");
        html.Append("<th width=\"14%\">排序</th>");
        html.Append("<th width=\"14%\">状态</th>");
        html.Append("<th width=\"14%\">编辑</th>");
        html.Append("<th width=\"14%\">删除用户</th></tr>");

       List<Business.Struct.UserStruct> lstUser;

        if (danwei.Text != "单位")
        {
            string strBumen = bumen.Text;
            lstUser = StUserOperation.GetAllUserTemp("", txtSeach.Text, strBumen);
        }
        else
        {
            lstUser = StUserOperation.GetAllUserTemp("", txtSeach.Text,0);
        }

        foreach (Business.Struct.UserStruct user in lstUser)
        {

            html.Append("<tr align=\"center\" valign=\"middle\">");
            html.Append(string.Format("<td>{0}</td>", user.UserName));
            html.Append(string.Format("<td>{0}</td>", user.UserRealName));
            html.Append(string.Format("<td>{0}</td>", user.UserOderbyid));
            html.Append("<td>");

            if (user.UserInvalid.Trim() == "1")
            {
                html.Append("<font color='blue'>用户登陆时显示</font>");
            }
            else { html.Append("<font color='red'>用户登陆时不显示</font>"); }

            html.Append("</td>");
            html.Append(string.Format("<td><a href=\"edituser.aspx?userid={0}\">编辑</a></td>", user.UserId));
            html.Append(string.Format("<td><a href=\"openuserlist.aspx?deleteuser={0}\" onclick='return DelOK();'>删除</a></td>", user.UserId));

            html.Append("</tr>");

        }
        lstUser.Clear();
        html.Append("</table>");
        context.InnerHtml = html.ToString();

    }

    #region 单位的selectedIndexChanged事件
    protected void danwei_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (danwei.Text != "单位")
        {
            DataTable dtbumen;
            dtbumen = dhTemp.GetDepartment(danwei.Text);
            bumen.Items.Clear();
            if (dtbumen.Rows.Count > 0)
            {
                bumen.Items.Add("部门");
                for (int i = 0; i < dtbumen.Rows.Count; i++)
                {
                    this.bumen.Items.Add(dtbumen.Rows[i][0].ToString());
                }
            }
            else
            {
                bumen.Items.Add("部门");
                bumen.Items.Add(danwei.Text);
            }
        }
    }
    #endregion
}
