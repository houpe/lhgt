﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ParamNameManager.aspx.cs"
    Inherits="SystemManager_ParamNameManager" Title="" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>数据字典子项管理</title>
    <link href="../css/page.css" rel="Stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div style="text-align: center">
        <controlDefine:CustomGridView ID="CustomGridView1" runat="server" AllowPaging="false" AutoGenerateColumns="false"
            Width="100%" SkinID="List" ShowHideColModel="None" OnOnLoadData="CustomGridView1_OnLoadData">
            <Columns>
                <asp:BoundField DataField="name" HeaderText="数据字典类名"></asp:BoundField>
                <asp:BoundField DataField="keyvalue" HeaderText="数据字典项名"></asp:BoundField>
                <asp:BoundField DataField="keycode" HeaderText="数据字典项值"></asp:BoundField>
                <asp:TemplateField HeaderText="编辑">
                    <ItemTemplate>
                        <asp:HyperLink ID="hlEdit" runat="server" NavigateUrl='<%#string.Format("ParamItemManager.aspx?action=edit&rowid={0}", Server.UrlEncode(Eval("rowid").ToString())) %>'>编辑</asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="删除">
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    <ItemTemplate>
                        <asp:HyperLink ID="hlDelete" runat="server" NavigateUrl='<%# string.Format("ParamNameManager.aspx?action=delete&rowid={0}&name={1}&keyvalue={2}", Server.UrlEncode(Eval("rowid").ToString()), Eval("name").ToString(),Eval("keyvalue").ToString())%>'>删除</asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerSettings Visible="False" />
            <HeaderStyle CssClass="top_table" Height="30px" Width="50px" />
            <RowStyle CssClass="inputtable" />
        </controlDefine:CustomGridView>
        <asp:Button ID="btnSave" runat="server" Text="新增" OnClick="btnSave_Click" SkinID="LightGreen" />
        <asp:Button ID="btnReturn" runat="server" Text="返回" CausesValidation="False" OnClick="btnReturn_Click"
            SkinID="LightGreen" />
    </div>
    </form>
</body>
</html>
