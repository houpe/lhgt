﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Business;
using WF_DataAccess;
using Business.Admin;

public partial class SystemManager_DepartmentEditor : System.Web.UI.Page
{
    #region 窗体加载事件
    protected void Page_Load(object sender, EventArgs e)
    {
        /****单击部门时，获取该部门的信息****/
        if (!Page.IsPostBack)
        {
            DepartHandle DepartHandle = new DepartHandle();
            DataTable dtlist = DepartHandle.GetDepartment();
            
            if (dtlist.Rows.Count > 0)
            {
                DataRow dr = dtlist.NewRow();
                dr["departid"] = "0";
                dr["depart_Name"] = "无";
                dtlist.Rows.InsertAt(dr,0);
                sjbmlist.DataSource = dtlist;
                sjbmlist.DataTextField = "depart_name";
                sjbmlist.DataValueField = "departid";
                sjbmlist.DataBind();             
            }

            if (Request["depName"] != "" && Request["departId"] != "" && Request["orderId"] != "")
            {
                editdata.Text = "更新";
                this.bmmc.Text = Request["depName"];
                this.bmid.Text = Request["departId"];
                orderId.Text = Request["orderId"];
                string sjbm = string.Empty;
                DataTable dt = DepartHandle.SearchDepartment(Request["departId"]);
                if (dt.Rows.Count > 0)
                {
                    sjbmlist.SelectedValue = dt.Rows[0][0].ToString();                    
                }          
            }

        }

    }
    #endregion

    #region 插入新部门
    protected void inputdata_Click(object sender, EventArgs e)
    {
        //清空原有数据
        bmid.Text = "";
        bmmc.Text = "";
        orderId.Text = "";
        editdata.Text = "保存";
    }
    #endregion    

    #region 编辑部门信息
    protected void editdata_Click(object sender, EventArgs e)
    {
        string sjbm = sjbmlist.SelectedValue;//上级部门
        DepartHandle dhTemp = new DepartHandle();
        string orderId = dhTemp.GetMaxOrderId();

        if (String.IsNullOrEmpty(bmid.Text))//新增
        {
            string departId = dhTemp.GetMaxDepart();

            dhTemp.AddDepartment(bmmc.Text, sjbmlist.SelectedValue, orderId);
            msgAppear.Text = "新增成功";
            bmid.Text = departId; //用新ID赋值
        }
        else//修改
        {
            dhTemp.UpdateDepartment(bmmc.Text, sjbm, orderId, Request["departid"]);
            msgAppear.Text = "修改成功";
        }   
    }
    #endregion

    #region 删除部门信息
    /// <summary>
    /// 删除部门信息
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void deldata_Click(object sender, EventArgs e)
    {
        DepartHandle dhTemp = new DepartHandle();
        int i = dhTemp.DeleteDepartment(bmid.Text);

        if (i > 0)
            msgAppear.Text = "删除成功";
        else
            msgAppear.Text = "删除失败";
    }
    #endregion

    #region 对部门的人员进行重新设置
    protected void submit_Click(object sender, EventArgs e)
    {
        string[] selecteduser = Request.Params.GetValues("check");
        string[] sortID = txtOrderByID.Text.Split(',');

        DepartHandle.ResetDepartment(orderId.Text, selecteduser, sortID, bmid.Text);
        msgAppear.Text = "部门人员设置成功";

    }
    #endregion
}
