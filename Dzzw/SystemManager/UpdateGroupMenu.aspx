﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UpdateGroupMenu.aspx.cs"
    Inherits="SystemManager_UpdateGroupMenu" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>编辑菜单组</title>
    <link rel="stylesheet" type="text/css" href="../css/page.css" />
</head>
<body>
    <form id="Form1" name="queryForm" action="" runat="server">
    <div style="margin: 0px auto; text-align: center; width: 80%">
        <div style="margin-left: auto; margin-right: auto;">
            <controlDefine:MessageBox ID="msgAppear" runat="server" />
        </div>
        <div class="box_top_bar">
            <div class="box_top_left">
                <div class="box_top_right">
                    <div class="box_top_text">
                        修改菜单组</div>
                </div>
            </div>
        </div>
        <div class="box_middle">
            <table width="70%" style="margin-left: auto; margin-right: auto;" border="1" cellpadding="0" cellspacing="0" class="box_middle_tab">
                <tr>
                    <td style="width: 374px; height: 33px;">
                        菜单组名称：
                    </td>
                    <td style="width: 270px; height: 33px;">
                        <asp:TextBox MaxLength="100" size="23" ID="menuname" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="width: 374px">
                        菜单组背景图片：
                    </td>
                    <td style="width: 270px">
                        <asp:TextBox MaxLength="100" size="23" ID="picture" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        排序编号：
                    </td>
                    <td>
                        <asp:TextBox MaxLength="100" size="23" ID="txtShowNo" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="height: 40px">
                        <asp:Button ID="updatemenu" class="NewButton" Text="保存" runat="server" OnClick="updatemenu_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Button ID="Button1" Text="返回" class="NewButton" runat="server" OnClick="Button1_Click" />
                    </td>
                </tr>
            </table>
             <asp:HiddenField ID="parentid" runat="server" />
        </div>
    </div>
    </form>
</body>
</html>
