﻿<%@ Page Language="c#" Inherits="SystemManager_EditAttachment" CodeFile="EditAttachment.aspx.cs" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head runat="server">
    <title>附件管理</title>
    <link href="../css/page.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <table style="text-align: left" class="tab_top" width="100%" border="0" cellpadding="0"
        cellspacing="0">
        <tr>
            <td>
                <img alt="" src="../images/tab_top_left.gif" />
            </td>
            <td style="text-align: left">业务类型：
                <asp:DropDownList ID="ddlSerial" runat="server">
                </asp:DropDownList>
                优先级：
                <asp:DropDownList ID="ddlYxj" runat="server">
                    <asp:ListItem Value=""></asp:ListItem>
                    <asp:ListItem Value="0">普通</asp:ListItem>
                    <asp:ListItem Value="1">加急</asp:ListItem>
                    <asp:ListItem Value="2">特急</asp:ListItem>
                </asp:DropDownList>
                编号：<asp:TextBox runat="server" ID="txtNum"></asp:TextBox>
                申请单位：<asp:TextBox runat="server" ID="txtSqdw"></asp:TextBox>
                <asp:Button runat="server" ID="btnSubmit" Text="查询" OnClick="btnSubmit_Click" />
            </td>
            <td align="right">
                <img alt="" src="../images/tab_top_right.gif" width="5" />
            </td>
        </tr>
    </table>
    <controlDefine:CustomGridView runat="server" ID="gvSerial" SkinID="List" AutoGenerateColumns="false"
        ShowItemNumber="true" OnOnLoadData="gvSerial_OnLoadData" ShowHideColModel="None"
        AllowPaging="true" OnRowCreated="gvSerial_RowCreated" Width="100%">
        <Columns>
            <asp:BoundField DataField="编号" HeaderText="编号" ItemStyle-Width="20%" ItemStyle-Height="20px" />
            <asp:BoundField DataField="业务类型" HeaderText="办件类型" />
            <asp:BoundField DataField="申请单位" HeaderText="申请单位" />
            <asp:BoundField DataField="联系人" HeaderText="联系人" />
            <asp:BoundField DataField="接件时间" HeaderText="接收时间" />
            <asp:BoundField DataField="附件总数" HeaderText="附件总数" />
            <asp:TemplateField HeaderText="优先级" ShowHeader="False">
                <ItemTemplate>
                    <asp:Image runat="server" ID="imgYxj" ImageUrl="" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="操作" ShowHeader="False">
                <ItemTemplate>
                    <asp:HyperLink ID="hlDetail" runat="server" Text="详细" />
                    <asp:HyperLink ID="hlDelete" runat="server" Text="删除所有附件" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </controlDefine:CustomGridView>
    </form>
</body>
</html>
