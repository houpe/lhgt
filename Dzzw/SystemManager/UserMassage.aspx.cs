﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using Business.Admin;
using SbBusiness.User;

public partial class WebPubManager_UserMassage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string strUserID = "";
        if (Request.Params["id"] != null)
        {
            strUserID = Request.Params["id"].ToString();
            GridviewDatabind(strUserID);
        }
    }
    #region 数据绑定
    /// <summary>
    /// 绑定用户信息
    /// </summary>
    /// <!--addby zhongjian 20090927-->
    private void GridviewDatabind(string strUserID)
    {
        DataTable dt = SysUserHandle.GetUser(strUserID);
        if (dt.Rows.Count > 0)
        {
            lblUserID.Text = dt.Rows[0]["USERID"].ToString();
            lblUserName.Text = dt.Rows[0]["USERNAME"].ToString();
            lblUserType.Text = dt.Rows[0]["type"].ToString();
            ddlZjlx.Value = dt.Rows[0]["IDTYPE"].ToString();
            lblZjhm.Text = dt.Rows[0]["IDNUMBER"].ToString();
            lblTel.Text = dt.Rows[0]["TEL"].ToString();
            lblCz.Text = dt.Rows[0]["FAX"].ToString();
            lblMobile.Text = dt.Rows[0]["MOBILE"].ToString();
            lblEmail.Text = dt.Rows[0]["EMAIL"].ToString();
            lblYZBM.Text = dt.Rows[0]["POSTCODE"].ToString();
            lblAddress.Text = dt.Rows[0]["ADDRESS"].ToString();
        }
    }
    #endregion 

}
