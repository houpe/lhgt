﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DetailAttachment.aspx.cs"
    Inherits="SystemManager_DetailAttachment" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>附件管理---可参照或删除</title>
    <link href="../css/page.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <script language="javascript" type="text/javascript">
        function Attach() {
            var nLeft = (screen.width - 625) / 2;
            var nTop = (screen.height - 400) / 2;
            window.open("../UserControls/MutiFilesUpload.aspx?iid=<%=Request["iid"] %>&wiid=-1&ctlid=", "submit", "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=auto,resizable=yes,width=400,height=300,left=" + nLeft + ",top=" + nTop, true);
        }

        function RefreshParentPage() {
            window.location.href = window.location.href;
        }

        function OpenWindow(url) {
            window.open(url, '', '');
        }
    </script>
    <form id="form1" runat="server">
        <div class="box_middle">
            <div style="text-align: right; height: 30px; padding-top: 5px;">
                <a href="javascript:Attach();">上传附件</a> <a href="EditAttachment.aspx">返回</a>
            </div>
        </div>
        <asp:DataGrid ID="dgWorkflowInfo" runat="server" Width="100%" CssClass="tab"
            AutoGenerateColumns="False" PageSize="100" AllowPaging="True" OnCancelCommand="dgWorkflowInfo_CancelCommand"
            OnDeleteCommand="dgWorkflowInfo_DeleteCommand" OnEditCommand="dgWorkflowInfo_EditCommand"
            OnUpdateCommand="dgWorkflowInfo_UpdateCommand" DataKeyField="res_value">
            <ItemStyle Height="20px"></ItemStyle>
            <HeaderStyle CssClass="td_e"></HeaderStyle>
            <Columns>
                <asp:TemplateColumn HeaderText="编号">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "iid") %>'>
                        </asp:Label>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "iid") %>'
                            Width="30px" Style="display: none">
                        </asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="路径">
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "路径") %>'>
                        </asp:Label>
                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "路径") %>'
                            Width="240px" ReadOnly="True" Style="display: none">
                        </asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="附件名称">
                    <ItemTemplate>
                        <asp:Label ID="Label3" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "附件名称") %>'>
                        </asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox3" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "附件名称") %>'
                            Width="240px">
                        </asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="单个附件">
                    <ItemTemplate>
                        <a href="javascript:void(0);" onclick="OpenWindow('attachment_show.aspx?aID=<%# DataBinder.Eval(Container.DataItem, "res_value") %>');">查看附件</a>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:ButtonColumn Text="删除" HeaderText="删除" CommandName="Delete"></asp:ButtonColumn>
                <asp:EditCommandColumn UpdateText="更新" HeaderText="改附件名称" CancelText="取消" EditText="改名"></asp:EditCommandColumn>
            </Columns>
            <PagerStyle Visible="False" Mode="NumericPages"></PagerStyle>
        </asp:DataGrid>

    </form>
</body>
</html>
