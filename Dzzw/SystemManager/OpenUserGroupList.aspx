<%@ Page Language="c#" Inherits="SystemManager_OpenUserGroupList" CodeFile="OpenUserGroupList.aspx.cs" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>用户组列表</title>
    <link href="../css/page.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function DelOK() {
            if (confirm('确定要删除该角色?') == true) {
                return true;
            }
            else {
                return false;
            }

        }
    
    </script>
</head>
<body>
    <form runat="server" action="">
    <div class="box_middle">
        <div style="text-align: left; height: 30px; padding-top: 5px;">
            组名：<asp:TextBox ID="txtSeach" Text="" runat="server"></asp:TextBox>
            <asp:Button ID="btnQuery" class="input" Text="查询" runat="server"></asp:Button>
            <input type="button" class="NewButton" value="添加组" onclick='window.navigate("addgroup.aspx")' />
        </div>
    </div>
    <div id='context' runat="server">
    </div>
    </form>
</body>
</html>
