using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;


public partial class SystemManager_AddGroup : PageBase
{
    protected void Page_Load(object sender, System.EventArgs e)
    {
    }

   

    /// <summary>
    /// 添加组
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnAddGroup_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(txtGroupName.Text))
            {
                string isStop = "1";
                if (ckbIs_Stop.Checked)
                {
                }
                else
                {
                    isStop = "0";
                }
                Business.Admin.StUserGroupHandle.AddGroup(txtGroupName.Text, txtGroupDescrip.Text, txtGroupNum.Text, isStop);
                msgAppear.Text = "添加成功";
            }
            else
            {
                msgAppear.Text = "添加组失败!组名不能为空";
            }
        }
        catch (Exception ex)
        {
            msgAppear.Text = ex.Message;
        }
    }
}
