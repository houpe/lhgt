<%@ Page Language="c#" Inherits="SystemManager_PriorityMain" CodeFile="PriorityMain.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>业务优先级设置</title>
    <link rel="stylesheet" id="skinCss" type="text/css" href="../css/default/easyui.css" />
    <link rel="stylesheet" type="text/css" href="../css/icon.css" />
    <link href="../css/page.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../ScriptFile/jquery.js"></script>
    <script type="text/javascript" src="../ScriptFile/jquery.easyui.min.js"></script>
    <script src="../ScriptFile/easyui-lang-zh_CN.js" type="text/javascript"></script>
    <script type="text/javascript">
        function tosubmit(strYwNo, strYwStatus, strPageNo) {
            var strUrl = "prioritymain.aspx?";
            strUrl += "startDate=" + $("#startDate").datebox('getValue');
            strUrl += "&endDate=" + $("#endDate").datebox('getValue');
            strUrl += "&ywlx=" + document.all.ywlx.value;
            strUrl += "&sqdw=" + document.all.sqdw.value;
            strUrl += "&ywno=" + strYwNo;
            if (strYwNo != "")
                strUrl += "&status=" + strYwStatus.value;
            else
                strUrl += "&status=";
            //alert(strUrl);
            window.location.href = strUrl;
        }

        function hrefonclic(strValue) {
            tosubmit("", null, "");
        }
        function init() {
            initPlan();
            iframeresize();
        }
    </script>
</head>
<body>
    <form id="from1" runat="server">
    <div class="box_middle">
        <div style="text-align: right; height: 30px; padding-top: 5px;">
            开始时间:
            <input class="easyui-datebox" id="startDate" value="<%=Request["startDate"]%>" />
            结束时间:
            <input class="easyui-datebox" id="endDate" value="<%=Request["endDate"]%>" />业务类型：<span
                id="spywlx"><select name='ywlx' id='ywlx' style='width: 130px' runat='server'></select></span>
            申请单位：
            <input maxlength="100" size="18" name="sqdw" style="width: 100px;" runat="server"
                id="sqdw" />
            <input type="button" class="NewButton" value="查询" name="button1" onclick="hrefonclic('1')" />
            </td>
        </div>
    </div>
    <controlDefine:CustomGridView ID="CustomGridView1" runat="server" AllowPaging="True"
        SkinID="List" AutoGenerateColumns="false" ShowItemNumber="true" Width="100%"
        ShowHideColModel="None" CssClass="tab" OnOnLoadData="CustomGridView1_OnLoadData"
        OnRowCreated="CustomGridView1_RowCreated" EmptyDataText=".">
        <Columns>
            <asp:BoundField DataField="iid" HeaderText="业务编号" ItemStyle-Width="20%" ItemStyle-Height="20px" />
            <asp:BoundField DataField="wname" HeaderText="业务类型" />
            <asp:BoundField DataField="Name" HeaderText="申请单位" />
            <asp:BoundField DataField="accptime" HeaderText="开始时间" />
            <asp:TemplateField HeaderText="处理">
                <ItemTemplate>
                    <%# strPriority(Container)%>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </controlDefine:CustomGridView>
    </form>
</body>
</html>
