using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for dep_tree.
/// </summary>
public partial class SystemManager_DepartmentTree : PageBase
{
    public string strHtmlCode = string.Empty;

    protected void Page_Load(object sender, System.EventArgs e)
    {
        Business.Admin.DepartHandle depart = new Business.Admin.DepartHandle();
        DataTable dtSource = depart.GetDepartmentTree();

        string strScriptName = "clickForUser";
        if (Request["flag"] == "2")
        {
            strScriptName = "clickForDept";
        }

        GenerateTree(strScriptName, dtSource, "", ref strHtmlCode);
    }

    /// <summary>
    /// ���ɲ�����
    /// </summary>
    /// <param name="strScriptName"></param>
    /// <param name="dtSource"></param>
    /// <param name="strFilterConditon"></param>
    /// <param name="strInitScript"></param>
    public void GenerateTree(string strScriptName, DataTable dtSource,
        string strFilterConditon, ref string strHtmlCode)
    {
        if (string.IsNullOrEmpty(strFilterConditon))
        {
            strFilterConditon = "parent_id='0'";
        }

        DataRow[] drsSelected = dtSource.Select(strFilterConditon);
        if (!string.IsNullOrEmpty(strFilterConditon) && drsSelected.Length > 0)
        {
            if (string.IsNullOrEmpty(strHtmlCode))
            {
                strHtmlCode += "<ul class='easyui-tree'>";
            }
            else
            {
                strHtmlCode += "<ul>";
            }
        }

        foreach (System.Data.DataRow drParent in drsSelected)
        {
            string depName = drParent["DEPART_NAME"].ToString();
            string orderId = drParent["ORDER_ID"].ToString();
            string departId = drParent["DEPARTID"].ToString();

            strHtmlCode += "<li>";
            strHtmlCode += string.Format("<span><a href=\"javascript:{4}('{0}','{1}','{2}');\">{3}</a></span>", orderId, depName, departId, depName, strScriptName);

            string strFilter = string.Format("parent_id='{0}'", departId);
            GenerateTree(strScriptName, dtSource, strFilter, ref strHtmlCode);

            strHtmlCode += "</li>";
        }

        if (!string.IsNullOrEmpty(strFilterConditon) && drsSelected.Length > 0)
        {
            strHtmlCode+="</ul>";
        }
    }
}
