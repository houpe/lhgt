<%@ Page Language="c#" Inherits="SystemManager_UserEditor" CodeFile="UserEditor.aspx.cs" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head runat="server">
    <title>用户 信息编辑</title>
    <link href="../css/page.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <div id="style" runat="server">
    </div>
    <form method="post" action="" enctype="multipart/form-data" name="aform">
    <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
        <input type="hidden" name="userID" id="userID" runat="server">
        <tr>
            <td>
                上传文件：
            </td>
            <td>
                <input type="file" name="upfile">
            </td>
        </tr>
        <tr>
            <td>
                <img runat="server" id="USERIMG" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div id="message" runat="server">
                </div>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <input type="Submit" class="NewButton" value="提交" />
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
