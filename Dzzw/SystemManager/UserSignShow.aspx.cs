using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using WF_Business;

/// <summary>
/// Summary description for img_show.
/// </summary>
public partial class SystemManager_UserSignShow : PageBase
{
    protected void Page_Load(object sender, System.EventArgs e)
    {
        String userID = Request["userID"];
        if (string.IsNullOrEmpty(userID))
        {
            return;
        }

        string strSql = "select SIGNPIC from ST_USER_SIGN where USERID='" + userID + "' order by savedate desc";
        IDataReader idrGet =SysParams.OAConnection().GetDataReader(strSql);

        if (idrGet.Read())
        {
            System.IO.Stream strmRead = new System.IO.MemoryStream((byte[])idrGet["SIGNPIC"]);

            byte[] buffer = new byte[strmRead.Length];
            strmRead.Read(buffer, 0, buffer.Length);
            idrGet.Close();
            Response.OutputStream.Write(buffer, 0, buffer.Length);
        }
        else
        {
            idrGet.Close();
            byte[] buffer = new byte[] { 66, 77, 58, 0, 0, 0, 0, 0, 0, 0, 54, 0, 0, 0, 40, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 24, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            Response.OutputStream.Write(buffer, 0, buffer.Length);
        }
	
    }
}
