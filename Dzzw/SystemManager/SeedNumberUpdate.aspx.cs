﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SystemManager_SeedNumberUpdate : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //string action = Request.Params["action"];
            if (!string.IsNullOrEmpty(ddlSEED_NAME.SelectedValue))
            {
                PersistenceControl1.KeyValue = ddlSEED_NAME.SelectedValue;
            }

            PersistenceControl1.PageControl = this.form1;
            PersistenceControl1.Render();
            PersistenceControl1.UnlockControl();
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        PersistenceControl1.KeyValue = ddlSEED_NAME.SelectedValue;
        PersistenceControl1.PageControl = this.form1;
        PersistenceControl1.Update();
        WindowAppear.WriteAlert(this.Page, "保存成功");
    }

    protected void ddlSEED_NAME_SelectedIndexChanged(object sender, EventArgs e)
    {
        PersistenceControl1.KeyValue = ddlSEED_NAME.SelectedValue;
        PersistenceControl1.PageControl = this.form1;
        PersistenceControl1.Render();
    }
}