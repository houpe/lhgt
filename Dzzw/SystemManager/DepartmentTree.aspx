<%@ Page Language="c#" Inherits="SystemManager_DepartmentTree" CodeFile="DepartmentTree.aspx.cs" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head runat="server">
    <title>部门工作情况</title>
    <link rel="stylesheet" id="skinCss" type="text/css" href="../css/default/easyui.css" />
    <link rel="stylesheet" type="text/css" href="../css/icon.css" />
    <script type="text/javascript" src="../ScriptFile/jquery.js"></script>
    <script type="text/javascript" src="../ScriptFile/jquery.easyui.min.js"></script>
    <script type="text/javascript">
        function clickForUser(orderId, depName, departId) {
            openDivDlg("DepartUserList.aspx?departId=" + departId);
        }

        function clickForDept(orderId, depName, departId) {
            openDivDlg("DepartmentEditor.aspx?orderId=" + orderId + "&depName=" + encodeURI(depName) + "&departId=" + departId);
        }

        //打开内嵌网页的通用方法1（window）
        function openDivDlg(strUrl) {
            $('#ifmContent').attr("src", strUrl);
        }
    </script>
</head>
<body class="easyui-layout" id="divBody">
    <div id="divwest" region="west" title='部门列表' split="true" style="width: 250px;">
        <div class="easyui-panel" style="padding: 5px">
            <%=strHtmlCode %>
        </div>
    </div>
    <div region="center" title="内容区" id="divCenter">
        <iframe src="" width="100%" height="100%" frameboder="0" id="ifmContent"></iframe>
    </div>
</body>
</html>
