﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SeedNumberUpdate.aspx.cs" Inherits="SystemManager_SeedNumberUpdate" %>

<%@ Register Src="../UserControls/PersistenceControl.ascx" TagName="PersistenceControl"
    TagPrefix="uc2" %>
<%@ Register Src="~/UserControls/TextBoxWithValidator.ascx" TagPrefix="uc1" TagName="TextBoxWithValidator" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>种子表维护</title>
    <link href="../css/page.css" rel="Stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">

        <div style="text-align: center">
            <table border="0" cellpadding="0" cellspacing="0" class="tab" style="width: 60%">
                <tr>
                    <td style=" text-align:rgiht;">编号类型：</td>
                    <td style=" text-align:left;">
                       <asp:DropDownList runat="server" AutoPostBack="true" ID="ddlSEED_NAME" OnSelectedIndexChanged="ddlSEED_NAME_SelectedIndexChanged">
                           <asp:ListItem Value="webcode">办件初始编号</asp:ListItem>
                            <asp:ListItem Value="webseed">发文初始编号</asp:ListItem>
                       </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td style=" text-align:right;">编号值：</td>
                    <td style=" text-align:left;">
                        <uc1:TextBoxWithValidator ID="txtSEED_VALUE" runat="server" ErrorMessage="值不能为空"
                            Width="300pt" />
                    </td>
                </tr>
            </table>
            <asp:Button ID="btnSave" runat="server" Text="保存" OnClick="btnSave_Click" SkinID="LightGreen" />
        </div>
        <uc2:PersistenceControl ID="PersistenceControl1" runat="server" Key="seed_name" Table="st_number_seed" />

    </form>
</body>
</html>
