﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Business;
using Business.Admin;

public partial class SystemManager_QueryRight  :PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DataTable dt;
            Business.FlowOperation.ClsUserWorkFlow UserWorkFlow = new Business.FlowOperation.ClsUserWorkFlow();
            dt=UserWorkFlow.GetDisnictWorkFlow();
            ddlProcessName.DataSource = dt;
            ddlProcessName.DataTextField = "wname";
            ddlProcessName.DataValueField = "wname";
            ddlProcessName.DataBind();
            BindData();
            BindUnit();
        }
    }
    /// <summary>
    /// 绑定表格数据
    /// </summary>
    private void BindData()
    {
        DataTable dt = StUserGroupHandle.GetUserRight();
        gv.DataSource = dt;
        gv.DataBind();
    }
    private void BindUnit()
    {
        DepartHandle dep=new DepartHandle();
        DataTable dt=dep.GetDepartment();      
        ddlUnit.DataSource = dt;
        ddlUnit.DataTextField = "depart_name";
        ddlUnit.DataValueField = "departid";
        ddlUnit.DataBind();
        BindDep(ddlUnit.SelectedValue); 
    }
    /// <summary>
    /// 绑定部门数据
    /// </summary>
    private void BindDep(string unitId)
    {
        DepartHandle dep = new DepartHandle();
        DataTable dt = dep.GetDepartmentByParentId(unitId);       
        ddlDep.DataSource = dt;
        ddlDep.DataTextField = "depart_name";
        ddlDep.DataValueField = "departid";
        ddlDep.DataBind();
        BindUser(ddlDep.SelectedValue); 
    }
    private void BindUser(string depId)
    {
        DataTable dt;
        dt = DepartHandle.GeDepartmentByDepartId(depId);
        ddlUserName.DataSource = dt;
        ddlUserName.DataTextField = "user_name";
        ddlUserName.DataValueField = "userid";
        ddlUserName.DataBind();
    }
    protected void ddlUnit_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindDep(ddlUnit.SelectedValue);
    }
    protected void ddlDep_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindUser(ddlDep.SelectedValue);
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        StUserGroupHandle.UpdateUserRight(ddlUserName.SelectedValue, ddlProcessName.SelectedValue);
        BindData();
    }
    protected void gv_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        LinkButton lb = (LinkButton)gv.Rows[e.RowIndex].FindControl("ImageButton1");
        string id = lb.CommandArgument;
        StUserGroupHandle.DeleteUserRight(id);
        BindData();
    }
}
