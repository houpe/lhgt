﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Business.Menu;

public partial class SystemManager_UpdateGroupMenu : System.Web.UI.Page
{
    #region 窗体加载
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request["parentMname"] != "")
                menuname.Text = Request["parentMname"];
            if (Request["parentMpic"] != "")
                picture.Text = Request["parentMpic"];
            if (Request["parentMid"] != "")
                parentid.Value = Request["parentMid"];
            if (Request["showno"] != "")
                this.txtShowNo.Text = Request["showno"];
        }
    }
    #endregion

    #region 修改菜单
    protected void updatemenu_Click(object sender, EventArgs e)
    {
        XtMenuOperation.UpdateChileMenu(menuname.Text, parentid.Value,picture.Text.Trim());
        msgAppear.Text = "修改完成";
    }
    #endregion

    #region 返回菜单管理主界面
    protected void Button1_Click(object sender, EventArgs e)
    {
        Response.Redirect("Menumanage.aspx");
    }
    #endregion
}
