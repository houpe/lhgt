using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Business.Struct;

/// <summary>
/// Summary description for editgroup.
/// </summary>
public partial class SystemManager_EditGroup : PageBase
{
    /// <summary>
    /// 窗体加载
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, System.EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                String groupid = Request["groupid"];

                GroupStruct grpsTemp = Business.Admin.StUserGroupHandle.GetGroupById(groupid);
                txtGroupName.Text = grpsTemp.GroupName;
                txtGroupOrder.Text = grpsTemp.OrderNum;
                if (grpsTemp.IS_STOP == "1")
                {
                    ckbIs_Stop.Checked = true;
                }
                else
                {
                    ckbIs_Stop.Checked = false;
                }
            }
            catch (Exception ex)
            {
                msgAppear.Text = ex.Message;
            }
        }
    }

    

    /// <summary>
    /// 修改
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOk_Click(object sender, EventArgs e)
    {
        try
        {
            GroupStruct grpsTemp = new GroupStruct();
            grpsTemp.GroupId = Request["groupid"];
            grpsTemp.GroupName = txtGroupName.Text.Trim();
            grpsTemp.OrderNum = txtGroupOrder.Text.Trim();

            if (ckbIs_Stop.Checked)
            {
                grpsTemp.IS_STOP = "1";
            }
            else
            {
                grpsTemp.IS_STOP = "0";
            }

            Business.Admin.StUserGroupHandle.UpdateGroupById(grpsTemp);

            msgAppear.Text = "修改成功";
        }
        catch (Exception ex)
        {
            msgAppear.Text = ex.Message;
        }
    }
}
