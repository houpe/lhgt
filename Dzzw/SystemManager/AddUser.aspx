<%@ Page  Language="c#" Inherits="SystemManager_AddUser"
    CodeFile="AddUser.aspx.cs" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head runat="server">
    <title>添加用户</title>
    <link rel="stylesheet" type="text/css" href="../css/page.css" />
      <link href ="../css/page.css" rel = "stylesheet" type = "text/css" />
</head>
<body>
    <form runat="server">
        <div align="center">
            <table border="0" cellspacing="1" cellpadding="2">
                <tr>
                    <td align="center">
                        用户登陆名：</td>
                    <td align="center" style="width: 137px">
                        <asp:TextBox ID="txtLoginUser" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td align="center">
                        用户密码：</td>
                    <td align="center" style="width: 137px">
                        <asp:TextBox ID="txtUserPass" runat="server" TextMode="Password"></asp:TextBox></td>
                </tr>
                <tr>
                    <td align="center" style="height: 28px">
                        用户真实姓名：</td>
                    <td align="center" style="height: 28px; width: 137px;">
                        <asp:TextBox ID="txtRealName" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td align="center">
                        所在单位：</td>
                    <td>
                        <asp:DropDownList ID="ddlCompany" runat="server" OnSelectedIndexChanged="ddlCompany_SelectedIndexChanged" Width="129px" AutoPostBack="True">
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td align="center">
                        所在部门：</td>
                    <td>
                        <asp:DropDownList ID="ddlDepartment" runat="server" Width="129px">
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td colspan="2" align="center" style="height: 24px">
                        <asp:Button ID="btnAddUser" CssClass="input" runat="server" Text="添加用户" OnClick="btnAddUser_Click" />
                        <input type="button" value="返回" class="input"  onclick='window.navigate("openuserlist.aspx")' />
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <controlDefine:MessageBox ID="msgAppear" runat="server" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
