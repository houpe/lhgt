﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UpdateChildMenu.aspx.cs"
    Inherits="SystemManager_UpdateChildMenu" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>修改子菜单</title>
    <link rel="stylesheet" type="text/css" href="../css/page.css" />
</head>
<body>
    <form id="Form1" name="queryForm" action="" runat="server">
    <div style="margin: 0px auto; text-align: center; width: 80%">
        <div style="margin-left: auto; margin-right: auto;">
            <controlDefine:MessageBox ID="msgAppear" runat="server" />
        </div>
        <div class="box_top_bar" style="height: 30px; margin-top: 20px;">
            <div class="box_top_left">
                <div class="box_top_right">
                    <div class="box_top_text">
                        修改子菜单</div>
                </div>
            </div>
        </div>
        <%--<div class="box_middle" style="height: 30px; padding-top:10px;">
        修改子菜单
        </div>--%>
        <div class="box_middle">
            <table width="70%" style="margin-left: auto; margin-right: auto;" border="1" cellpadding="0"
                cellspacing="0" class="box_middle_tab">
                <tr>
                    <td style="text-align: right">
                        子菜单名称：
                    </td>
                    <td>
                        <asp:TextBox MaxLength="100" size="23" ID="menuname" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right">
                        子菜单背景图片：
                    </td>
                    <td>
                        <asp:TextBox MaxLength="100" size="23" ID="picture" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right; height: 25px;">
                        子菜单链接路径：
                    </td>
                    <td>
                        <asp:TextBox MaxLength="100" size="23" ID="menuUrl" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right">
                        子菜单所属：
                    </td>
                    <td>
                        <asp:DropDownList ID="parentmenu" runat="server" Width="158px">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right">
                        排序编号：
                    </td>
                    <td>
                        <asp:TextBox MaxLength="100" size="23" ID="txtShowNo" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Button ID="updatemenu" class="NewButton" Text="保存" runat="server" OnClick="updatemenu_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Button ID="Button1" Text="返回" class="NewButton" runat="server" OnClick="Button1_Click" />
                    </td>
                </tr>
            </table>
            <asp:HiddenField ID="childid" runat="server" />
        </div>
    </div>
    </form>
</body>
</html>
