using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for user_editor.
/// </summary>
public partial class SystemManager_UserEditor : PageBase
{
    protected void Page_Load(object sender, System.EventArgs e)
    {
        //Put user code to initialize the page here
        if (!IsPostBack)
        {
            string userID = Request["userid"];
            string strMsg = string.Empty;

            if (Request.ContentType != null && Request.ContentType.ToLower().StartsWith("multipart/form-data"))
            {
                System.IO.Stream streamTemp = Request.Files["upfile"].InputStream;
                byte[] bFileContent = new byte[streamTemp.Length];
                streamTemp.Read(bFileContent, 0, bFileContent.Length);

                Business.Admin.StUserOperation.InsertUserSign(userID, bFileContent);

                strMsg = "ִ�гɹ�";
            }

            this.userID.Value = userID;
            this.USERIMG.Src = string.Format("UserSignShow.aspx?userID={0}", userID);
            this.message.InnerHtml = strMsg;
        }

        System.Text.StringBuilder html = new System.Text.StringBuilder();
        html.Append("<style>");
        html.Append(".tdLeft{");
        html.Append("text-align: right;");
        html.Append("width: 10%;");
        html.Append("nowrap;}");

        if ("show".Equals(Request["act"]))
        {
            html.Append(".textcss{");
            html.Append("width : 100%;}");
            html.Append("border : 0;}");
        }
        else
        {
            html.Append(".textcss{");
            html.Append("width : 100%;}");
        }
        html.Append("</style>");

        this.style.InnerHtml = html.ToString();

    }

    
}