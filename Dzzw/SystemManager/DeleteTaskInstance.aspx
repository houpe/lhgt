<%@ Page Language="c#" Inherits="Office_DeleteTaskInstance" CodeFile="DeleteTaskInstance.aspx.cs" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head runat="server">
    <title>流程管理</title>
    <link href="../css/page.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function deleteRecord(varIid) {
            if (confirm('确认删除该流程?') == true) {
                window.location.href = "DeleteTaskInstance.aspx?iid=" + varIid;
            }
        }
    </script>
</head>
<body>
    <form runat="server">
    <div class="box_middle">
        <div style="text-align: right; height: 30px; padding-top: 5px;">
            业务类型：
            <asp:DropDownList ID="ddlSerial" runat="server">
            </asp:DropDownList>
            优先级：
            <asp:DropDownList ID="ddlYxj" runat="server">
                <asp:ListItem Value=""></asp:ListItem>
                <asp:ListItem Value="0">普通</asp:ListItem>
                <asp:ListItem Value="1">加急</asp:ListItem>
                <asp:ListItem Value="2">特急</asp:ListItem>
            </asp:DropDownList>
            编号：<asp:TextBox runat="server" ID="txtNum"></asp:TextBox>
            申请单位：<asp:TextBox runat="server" ID="txtSqdw"></asp:TextBox>
            <asp:Button runat="server" ID="btnSubmit" Text="查询" OnClick="btnSubmit_Click" />
        </div>
    </div>
    <controlDefine:CustomGridView runat="server" ID="gvSerial" SkinID="List" AutoGenerateColumns="false"
        ShowItemNumber="true" OnOnLoadData="gvSerial_OnLoadData" ShowHideColModel="None"
        AllowPaging="true" OnRowCreated="gvSerial_RowCreated" Width="100%">
        <Columns>
            <asp:BoundField DataField="iid" HeaderText="编号" ItemStyle-Width="20%" ItemStyle-Height="20px" />
            <asp:BoundField DataField="业务类型" HeaderText="案件类型" />
            <asp:BoundField DataField="申请单位" HeaderText="申请单位" />
            <asp:BoundField DataField="联系人" HeaderText="联系人" />
            <asp:BoundField DataField="接件时间" HeaderText="接收时间" />
            <asp:TemplateField HeaderText="优先级" ShowHeader="False">
                <ItemTemplate>
                    <asp:Image runat="server" ID="imgYxj" ImageUrl="" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="操作" ShowHeader="False">
                <ItemTemplate>
                    <asp:HyperLink ID="hlDelete" runat="server" Text="删除" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </controlDefine:CustomGridView>
    </form>
</body>
</html>
