<%@ Page Language="c#" Inherits="SystemManager_MemberMIS" CodeFile="MemberMIS.aspx.cs" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head runat="server">
    <title>组成员管理</title>
    <link rel="stylesheet" type="text/css" href="../css/page.css" />
</head>
<body>
    <form name="form1" action="" runat="server">
    <center>
        <asp:Label ID="lblGroupName" runat="server" Text=""></asp:Label>
    </center>
    <input type="hidden" name="groupid" runat="server" id="groupid" />
    <input type="hidden" name="groupname" runat="server" id="groupname" />
    <table width="100%" cellspacing="0" cellpadding="3" class="tab">
        <tr align="center" valign="middle">
            <td style="width: 21%">
                用户ID
            </td>
            <td style="width: 21%">
                用户姓名：<asp:TextBox ID="txtName" runat="server"></asp:TextBox>
            </td>
            <td style="width: 21%">
                <asp:Button ID="btnQuery" runat="server" Text="查询" />
                <input type="submit" name="cmd" class="newbutton" value="保存设置" />
                <input type="button" class="newbutton" value="返回" onclick='javascript:history.back(-1);' />
            </td>
        </tr>
        <div id='context' runat="server">
        </div>
    </table>
    </form>
</body>
</html>
