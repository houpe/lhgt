﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Business.Menu;
using System.Text;

public partial class SystemManager_MenuManage :PageBase
{
    /// <summary>
    /// 菜单及菜单组
    /// 修改人：YZG
    /// 时间：2010-03-01
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            XtMenuOperation Menu = new XtMenuOperation();

            /***************删除子菜单*****************/
            if (Request["delmenuchildid"] != "")
            {
                Menu.DeleteChildMenu(Request["delmenuchildid"]);
                //msgAppear.Visible = true;
                msgAppear.Text = "子菜单删除成功";
            }

            /***************删除菜单组****************/
            if (Request["delmenuparentid"] != "")
            {
                Menu.DeleteMenuGroup(Request["delmenuparentid"]);
                msgAppear.Text = "菜单组删除成功";
            }

            /***************子菜单上移****************/
            if (Request["ChildMupId"] != "" && Convert.ToInt32(Request["ChildMno"]) >= 1)
            {
                Menu.MoveMenu(Convert.ToInt32(Request["ChildMno"]), Request["ChildMupParId"], Request["ChildMupId"]);
            }
            else
            {
                //msgAppear.Text = "该菜单已经是改组的第一个菜单，不能上移";
            }

            /**************子菜单下移*****************/
            if (Request["ChildMdownId"] != "")
            {
                Menu.MoveDownMenu(Convert.ToInt32(Request["ChildMdownno"]), Request["ChildMdownParId"], Request["ChildMdownId"]);
            }

            /***************菜单组上移******************/
            if (Request["ParUpId"] != "" && Convert.ToInt32(Request["ParUpNo"]) >= 1)
            {
                Menu.MoveUpMenuGroup(Convert.ToInt32(Request["ParUpNo"]), Request["ParUpId"]);
            }
            else
            {
                //msgAppear.Text = "该菜单组已经是第一个，不能再上移";
            }

            /***************菜单组下移******************/
            if (Request["ParDownId"] != "")
            {
                Menu.MoveDownMenuGroup(Convert.ToInt32(Request["ParDownNo"]), Request["ParDownId"]);
            }

            StringBuilder html = new StringBuilder();
            html.Append("<table class=\"tab\" width=\"100%\" border=\"1\"><tr class=\"title\" align=\"center\" valign=\"middle\">");
            html.Append("<th width=\"10%\">菜单组名称</th><th width=\"13%\">子菜单名称</th><th width=\"52%\">链接页面</th><th width=\"25%\">操作</th></tr>");
            DataTable dt= Menu.GenerateParentMenu();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                html.Append("<tr>");
                html.Append(string.Format("<td>{0}</td><td></td><td></td>", dt.Rows[i]["menu_parent_desc"]));
                html.Append(string.Format("<td><a href=\"MenuManage.aspx?ParUpId={0}&ParUpNo={1}\">上移</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", dt.Rows[i]["menu_parent_id"], dt.Rows[i]["show_no"]));
                html.Append(string.Format("<a href=\"MenuManage.aspx?ParDownId={0}&ParDownNo={1}\">下移</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", dt.Rows[i]["menu_parent_id"], dt.Rows[i]["show_no"]));
                html.Append(string.Format("<a href=\"UpdateGroupMenu.aspx?parentMname={0}&parentMpic={1}&parentMid={2}&showno={3}\">编辑</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", dt.Rows[i]["menu_parent_desc"], dt.Rows[i]["picpath"], dt.Rows[i]["menu_parent_id"], dt.Rows[i]["show_no"]));
                html.Append(string.Format("<a href=\"MenuManage.aspx?delmenuparentid={0}\" onclick=\"return confirm('确定删除？')\">删除</a></td>", dt.Rows[i]["menu_parent_id"]));
                html.Append("</tr>");

                DataTable dtchild = Menu.GetChildMenu(dt.Rows[i]["menu_parent_id"].ToString());

                for (int j = 0; j < dtchild.Rows.Count; j++)
                {

                    html.Append("<tr><td></td>");
                    html.Append(string.Format("<td>{0}</td>", dtchild.Rows[j]["menu_child_desc"]));
                    html.Append(string.Format("<td>{0}</td>", dtchild.Rows[j]["linkhref"]));

                    html.Append(string.Format("<td><a href=\"MenuManage.aspx?ChildMupId={0}&ChildMno={1}&ChildMupParId={2}\">上移</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", dtchild.Rows[j]["menu_child_id"], dtchild.Rows[j]["show_no"], dtchild.Rows[j]["ancestor"]));
                    html.Append(string.Format("<a href=\"MenuManage.aspx?ChildMdownId={0}&ChildMdownno={1}&ChildMdownParId={2}\">下移</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", dtchild.Rows[j]["menu_child_id"], dtchild.Rows[j]["show_no"], dtchild.Rows[j]["ancestor"]));
                    html.Append(string.Format("<a href=\"UpdateChildMenu.aspx?childMname={0}&childMpic={1}&childMurl={2}&childMpar={3}&childMid={4}&showno={5}\">编辑</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", dtchild.Rows[j]["menu_child_desc"], dtchild.Rows[j]["overpic"], dtchild.Rows[j]["linkhref"], dtchild.Rows[j]["ancestor"], dtchild.Rows[j]["menu_child_id"], dtchild.Rows[j]["show_no"]));
                    html.Append(string.Format("<a href=\"MenuManage.aspx?delmenuchildid={0}\" onclick=\"return confirm('确定删除？')\">删除</a></td>", dtchild.Rows[j]["menu_child_id"]));
                    html.Append("</tr>");
                }

            }
            html.Append("</table>");

            this.context.InnerHtml = html.ToString();
        }
    }

}
