﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Business.FlowOperation;
using Business;
using WF_Business;

public partial class SystemManager_DetailAttachment : PageBase 
{
    public string strWiid = "";
    public string iid = "";
   
    protected void Page_Load(object sender, EventArgs e)
    {
        iid = Request["iid"];
        DataTable dt = ClsUserWorkFlow.GetDetailFlow(iid);
        if (dt.Rows.Count > 0)
        {
            strWiid = dt.Rows[0]["wiid"].ToString();
        }

        if (!Page.IsPostBack)
        {
            BindGrid(false);
        }
    }

    #region 绑定DataGrid控件
    public void BindGrid(bool bFlag)
    {
        string SqlInstance = "";
        SqlInstance = "select h.*,i.*,substr(h.path,0,Instr(h.path, '/', -1)) 路径,substr(h.path,Instr(h.path, '/', -1)+1) 附件名称  from st_dynamic_resource h, st_attachment i  where h.iid = '" + Request["iid"] + "'  and h.type ='2' and trim(h.res_value) = trim(i.aid) order by h.path";
        System.Data.DataTable dtWork;
        SysParams.OAConnection().RunSql(SqlInstance, out dtWork);
        dgWorkflowInfo.DataSource = dtWork;
        dgWorkflowInfo.DataBind();

        if (bFlag)
        {
            string strScript = "<script>RefreshParentPage();</script>";
            ClientScript.RegisterStartupScript(this.GetType(), "refreshPage", strScript);
        }
    }
    #endregion

    #region 取消按钮
    protected void dgWorkflowInfo_CancelCommand(object source, DataGridCommandEventArgs e)
    {
        dgWorkflowInfo.EditItemIndex = -1;
        BindGrid(false);
    }
    #endregion

    #region 删除按钮
    protected void dgWorkflowInfo_DeleteCommand(object source, DataGridCommandEventArgs e)
    {
        string strid = dgWorkflowInfo.DataKeys[e.Item.ItemIndex].ToString();
        string delinstance = "";
        delinstance = "delete from st_dynamic_resource where res_value='" + strid + "'";

        int j = SysParams.OAConnection().RunSql(delinstance);

        delinstance = "delete from st_attachment where aid='" + strid + "'";
        int k = SysParams.OAConnection().RunSql(delinstance);

        BindGrid(true);
    }
    #endregion

    #region 编辑按钮
    protected void dgWorkflowInfo_EditCommand(object source, DataGridCommandEventArgs e)
    {
        dgWorkflowInfo.EditItemIndex = e.Item.ItemIndex;
        BindGrid(false);
    }
    #endregion

    #region 更新按钮
    protected void dgWorkflowInfo_UpdateCommand(object source, DataGridCommandEventArgs e)
    {
        TextBox TextBox1 = (TextBox)(e.Item.Cells[0].FindControl("TextBox1"));
        TextBox TextBox2 = (TextBox)(e.Item.Cells[1].FindControl("TextBox2"));
        TextBox TextBox3 = (TextBox)(e.Item.Cells[2].FindControl("TextBox3"));

        string strId = dgWorkflowInfo.DataKeys[e.Item.ItemIndex].ToString();

        string strIId = TextBox1.Text.Trim();
        string strDepart = TextBox2.Text.Trim();
        string strCurrent = TextBox3.Text.Trim();
        string updateinstance = "";

        updateinstance = "update st_dynamic_resource set path = '" + strDepart + strCurrent + "'  where res_value = '" + strId + "'";

        int updateNum = SysParams.OAConnection().RunSql(updateinstance);
        dgWorkflowInfo.EditItemIndex = -1;
        BindGrid(true);

        string strScript = "<script>RefreshParentPage();</script>";
        ClientScript.RegisterStartupScript(this.GetType(), "refreshPage", strScript);
    }
    #endregion

}
