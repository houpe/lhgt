﻿<%@ Page ContentType="text/html;" Language="c#" Inherits="SystemManager_EditUser"
    CodeFile="EditUser.aspx.cs" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>编辑</title>
    <link href="../css/page.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <script type="text/javascript">
        function WindowBack() {
            var type = "<%=type %>"
            if (type == "AddressBook") {
                window.navigate("AddressBook.aspx");
            }
            else {
                window.navigate("openuserlist.aspx");
            }
        }
    
    </script>
    <form id="form1" runat="server">
    <table border="0" align="center" style="90%">
        <tr>
            <td>
                <div class="box_top_bar">
                    <div class="box_top_left">
                        <div class="box_top_right">
                            <div class="box_top_text">
                                用户信息</div>
                        </div>
                    </div>
                </div>
                <div class="box_middle">
                    <table border="0" cellpadding="0" cellspacing="0" class="box_middle_tab">
                        <tr>
                            <td>
                                登录名：
                            </td>
                            <td>
                                <asp:TextBox ID="txtLoginName" Width="153px" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                用户名：
                            </td>
                            <td>
                                <asp:TextBox ID="txtRealName" runat="server" Width="153px"></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                排序号：
                            </td>
                            <td>
                                <asp:TextBox ID="txtOrderbyid" runat="server" Width="153px"></asp:TextBox>
                            </td>
                            <td>
                                手机号：
                            </td>
                            <td>
                                <asp:TextBox ID="txtMobile" runat="server" Width="153px"></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                办公室电话：
                            </td>
                            <td>
                                <asp:TextBox ID="txtOfficeTel" runat="server" Width="153px"></asp:TextBox>
                            </td>
                            <td>
                                内线：
                            </td>
                            <td>
                                <asp:TextBox ID="txtExtNo" runat="server" Width="153px"></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                住宅电话：
                            </td>
                            <td>
                                <asp:TextBox ID="txtHouseTel" runat="server" Width="153px"></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;&nbsp;
                            </td>
                            <td>
                                &nbsp;&nbsp;
                            </td>
                            <td>
                                &nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr id="tr0" runat="server" visible="false">
                            <td align="center">
                                所在单位：
                            </td>
                            <td style="width: 137px">
                                <asp:DropDownList ID="ddlCompany0" runat="server" AutoPostBack="True" Width="153px"
                                    OnSelectedIndexChanged="ddlCompany0_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                            <td align="center">
                                所在部门：
                            </td>
                            <td style="width: 137px">
                                <asp:DropDownList ID="ddlDepartment0" runat="server" Width="153px">
                                </asp:DropDownList>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr id="tr1" runat="server" visible="false">
                            <td align="center">
                                所在单位：
                            </td>
                            <td style="width: 137px">
                                <asp:DropDownList ID="ddlCompany1" runat="server" AutoPostBack="True" Width="153px"
                                    OnSelectedIndexChanged="ddlCompany1_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                            <td align="center">
                                所在部门：
                            </td>
                            <td style="width: 137px">
                                <asp:DropDownList ID="ddlDepartment1" runat="server" Width="153px">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:Button ID="btnDel1" runat="server" CssClass="input" Text="删除" OnClick="btnDel1_Click" />
                            </td>
                        </tr>
                        <tr id="tr2" runat="server" visible="false">
                            <td align="center">
                                所在单位：
                            </td>
                            <td style="width: 137px">
                                <asp:DropDownList ID="ddlCompany2" runat="server" AutoPostBack="True" Width="153px"
                                    OnSelectedIndexChanged="ddlCompany2_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                            <td align="center">
                                所在部门：
                            </td>
                            <td style="width: 137px">
                                <asp:DropDownList ID="ddlDepartment2" runat="server" Width="153px">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:Button ID="btnDel2" runat="server" CssClass="input" Text="删除" OnClick="btnDel2_Click" />
                            </td>
                        </tr>
                        <tr id="tr3" runat="server" visible="false">
                            <td align="center">
                                所在单位：
                            </td>
                            <td style="width: 137px">
                                <asp:DropDownList ID="ddlCompany3" runat="server" AutoPostBack="True" Width="153px"
                                    OnSelectedIndexChanged="ddlCompany3_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                            <td align="center">
                                所在部门：
                            </td>
                            <td style="width: 137px">
                                <asp:DropDownList ID="ddlDepartment3" runat="server" Width="153px">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:Button ID="btnDel3" runat="server" CssClass="input" Text="删除" OnClick="btnDel3_Click" />
                            </td>
                        </tr>
                        <tr id="tr4" runat="server" visible="false">
                            <td align="center">
                                所在单位：
                            </td>
                            <td style="width: 137px">
                                <asp:DropDownList ID="ddlCompany4" runat="server" AutoPostBack="True" Width="153px"
                                    OnSelectedIndexChanged="ddlCompany4_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                            <td align="center">
                                所在部门：
                            </td>
                            <td style="width: 137px">
                                <asp:DropDownList ID="ddlDepartment4" runat="server" Width="153px">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:Button ID="btnDel4" runat="server" CssClass="input" Text="删除" OnClick="btnDel4_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                用户密码：
                            </td>
                            <td>
                                <asp:TextBox ID="txtNewPass" runat="server" Width="153px"></asp:TextBox>
                            </td>
                            <td style="height: 28px">
                                重复密码 :
                            </td>
                            <td style="height: 28px">
                                <asp:TextBox ID="txtNewPassAgain" runat="server" Width="153px"></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                用户状态：
                            </td>
                            <td style="width: 137px">
                                <asp:DropDownList ID="ddlInvalid" runat="server" AutoPostBack="True" Width="153px">
                                    <asp:ListItem Value="1">用户登陆时显示</asp:ListItem>
                                    <asp:ListItem Value="0">用户登陆时不显示</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                                &nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="5">
                                <asp:Button ID="btnUpdate" CssClass="button" runat="server" OnClick="btnUpdate_Click"
                                    Text="保存" />
                                &nbsp;&nbsp;&nbsp;<input id="backlist" type="button" class="newbutton" value="返回"
                                    onclick="WindowBack()" />
                                &nbsp;&nbsp;&nbsp;<asp:Button ID="btnAddDepSel" runat="server" CssClass="button"
                                    Text="新增部门" OnClick="btnAddDepSel_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                <controlDefine:MessageBox ID="msgAppear" runat="server" />
                                <asp:Label ID="labRole" runat="server" ForeColor="#FF8080"></asp:Label>
                                <controlDefine:CustomGridView ID="gridview" runat="server" AllowPaging="True" Width="500"
                                    SkinID="List" HideTextLength="20" CustomSortExpression="" EmptyDataText="暂时未绑定任何角色"
                                    AutoGenerateColumns="False" AutoGenerateDeleteConfirm="True" ShowHideColModel="None"
                                    PageSize="10" AllowOnMouseOverEvent="True" OnRowCreated="gridview_RowCreated"
                                    OnOnLoadData="gridview_OnLoadData">
                                    <PagerSettings Visible="False" />
                                    <Columns>
                                        <asp:BoundField HeaderText="角色名称" DataField="group_name" />
                                        <asp:TemplateField HeaderText="删除">
                                            <ItemTemplate>
                                                <asp:HyperLink ID="hlDelete" runat="server">删除</asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </controlDefine:CustomGridView>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
