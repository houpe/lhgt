<%@ Page Language="c#" Inherits="SystemManager_attachment_show" CodeFile="attachment_show.aspx.cs" %>
<html>
<head runat="server">
<title></title>
</head>
<body>
<%
    String aID = Request["aID"];
    if (string.IsNullOrEmpty(aID))
    {
        return;
    }

    string strSql = "select aid,magic_number,ext_name,data,name,ftp_path from st_attachment where aid='" + aID + "' order by magic_number desc";
    System.Data.IDataReader idrGet = WF_Business.SysParams.OAConnection().GetDataReader(strSql);

    if (idrGet.Read())
    {
        //修改文件读取方式 update by zhongjian 20100317
        Response.ClearContent();
        byte[] bArr = idrGet["data"] as Byte[];//文件数据
        string strFileType = idrGet["ext_name"].ToString();//文件类型
        string strFileName = idrGet["name"].ToString();//文件名称
        Response.Clear();
        Response.Buffer = true;
        Response.Charset = "gb2312";
        Response.ContentEncoding = System.Text.Encoding.GetEncoding("GB2312");
        Response.AppendHeader("content-disposition", "attachment;filename=\"" + System.Web.HttpUtility.UrlEncode(strFileName, System.Text.Encoding.UTF8) + strFileType+"\"");
        Response.BinaryWrite(bArr);
        Response.End();
    }
    else
    {
        idrGet.Close();
        byte[] buffer = new byte[] { 66, 77, 58, 0, 0, 0, 0, 0, 0, 0, 54, 0, 0, 0, 40, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 24, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        Response.OutputStream.Write(buffer, 0, buffer.Length);
    }
	
%></body>
</html>
