﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ParamItemManager.aspx.cs"
    Inherits="SystemManager_ParamItemManager" %>

<%@ Register Src="../UserControls/PersistenceControl.ascx" TagName="PersistenceControl"
    TagPrefix="uc2" %>
<%@ Register Src="../UserControls/TextBoxWithValidator.ascx" TagName="TextBoxWithValidator"
    TagPrefix="uc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>数据字典编辑</title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="text-align: center">

        <script type="text/javascript" src="../ScriptFile/Regex.js"></script>

        <table border="0" cellpadding="0" cellspacing="0" class="window_tab" style="width: 80%">
            <tr>
                <td class="txtrighttd">
                    <asp:Label ID="Label2" runat="server" Text="数据字典类名"></asp:Label>
                </td>
                <td class="txtlefttd">
                    <uc1:TextBoxWithValidator ID="txtName" runat="server" ErrorMessage="数据字典类名不能为空" Type="Required"
                        ValidateEmptyText="true" Width="300pt" />
                </td>
            </tr>
            <tr>
                <td class="txtrighttd">
                    <asp:Label ID="Label3" runat="server" Text="数据字典项名"></asp:Label>
                </td>
                <td class="txtlefttd">
                    <uc1:TextBoxWithValidator ID="txtKeyvalue" runat="server" ErrorMessage="数据字典项名不能为空"
                        Width="300pt" />
                </td>
            </tr>
            <tr>
                <td class="txtrighttd">
                    <asp:Label ID="Label1" runat="server" Text="数据字典项值"></asp:Label>
                </td>
                <td class="txtlefttd">
                    <uc1:TextBoxWithValidator ID="txtKeycode" runat="server" ErrorMessage="数据字典项值不能为空"
                        Type="integer" Width="300pt" />
                </td>
            </tr>
        </table>
        <asp:Button ID="btnSave" runat="server" Text="保存" OnClick="btnSave_Click" SkinID="LightGreen" />
        <asp:Button ID="btnReturn" runat="server" Text="返回" CausesValidation="False" OnClick="btnReturn_Click"
            SkinID="LightGreen" />
    </div>
    <uc2:PersistenceControl ID="PersistenceControl1" runat="server" Key="rowid" Table="SYS_PARAMS" />
    </form>
</body>
</html>
