﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FlowDetailTimeMain.aspx.cs"
    Inherits="SystemManager_FlowDetailTimeMain" %>

<html>
<head runat="server">
    <link href="../css/page.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form name="form1" method="post" action="FlowDetailTimeMain.aspx?FlowID=<%=id%>">
    <table class="tab_top" width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td width="3%">
                <img src="../images/tab_top_left.gif" width="17" height="35" />
            </td>
            <td>
                <div class="tab_top_text3">
                    详细信息</div>
            </td>
            <td align="right">
                <label>
                    <input type="button" class="NewButton" value="返回" onclick='window.navigate("FlowTimeMain.aspx")' />
                </label>
            </td>
            <td width="1%" align="right">
                <img src="../images/tab_top_right.gif" width="5" height="35" />
            </td>
        </tr>
    </table>
    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="tab">
        <tr>
            <th nowrap>
                岗位
            </th>
            <th nowrap>
                经办人
            </th>
            <th nowrap>
                接收时间
            </th>
            <th nowrap>
                提交时间
            </th>
        </tr>
        <div id="context" runat="server">
        </div>
    </table>
    <table width="100%">
        <tr>
            <td align="right">
                <input type="submit" class="NewButton" id="btnRefer" value="提交更改" />
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
