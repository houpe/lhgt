using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Business;
using System.Collections.Generic;
using Business.Admin;

/// <summary>
/// Summary description for workday.
/// </summary>
public partial class SystemManager_SetWorkday : Page
{
    public ClsWorkDaySet clsWorkDay = new ClsWorkDaySet();

    DataTable dt = new DataTable();

    #region 点击日期时间控件
    protected void Calendar1_SelectionChanged(object sender, EventArgs e)
    {
        this.txtFromDate.Text = this.Calendar1.SelectedDate.ToShortDateString();
        //string strSql = "select * from st_worktime where sdate like to_date('"+this.Calendar1.SelectedDate.ToShortDateString() + "','yyyy-mm-dd')";

        dt.Rows.Clear();

        dt = clsWorkDay.GetWorkDay(txtFromDate.Text);
    
        if (dt.Rows.Count > 0)    //如果有
        {
            msgAppear.Visible = false;
            if (dt.Rows[0]["timelengh"] != null && dt.Rows[0]["timelengh"].ToString() != "0")
            {
                DateTime dtMsTime = (DateTime)dt.Rows[0]["mstime"];
                DateTime dtMeTime = (DateTime)dt.Rows[0]["metime"];
                DateTime dtNsTime = (DateTime)dt.Rows[0]["nstime"];
                DateTime dtNeTime = (DateTime)dt.Rows[0]["netime"];

                this.AmHour1.Text = dtMsTime.Hour.ToString();
                this.AmMinute1.Text = dtMsTime.Minute.ToString();
                this.FmHour1.Text = dtMeTime.Hour.ToString();
                this.FmMinute1.Text = dtMeTime.Minute.ToString();
                this.AmHour2.Text = dtNsTime.Hour.ToString();
                this.AmMinute2.Text = dtNsTime.Minute.ToString();
                this.FmHour2.Text = dtNeTime.Hour.ToString();
                this.FmMinute2.Text = dtNeTime.Minute.ToString();

                workdays.Checked = true;
                holiday.Checked = false;
            }
            else      //如果该日为节假日
            {
                msgAppear.Visible = true;
                msgAppear.Text = "该日没有设置上班时间,系统默认为假期";
                this.AmHour1.Text = "";
                this.AmMinute1.Text = "";
                this.FmHour1.Text = "";
                this.FmMinute1.Text = "";
                this.AmHour2.Text = "";
                this.AmMinute2.Text = "";
                this.FmHour2.Text = "";
                this.FmMinute2.Text = "";

                workdays.Checked = false;
                holiday.Checked = true;
            }
        }
        else          //如果没有该日的记录
        {
            msgAppear.Visible = true;
            msgAppear.Text = "没有该日的上班时间记录";
            this.AmHour1.Text = "";
            this.AmMinute1.Text = "";
            this.FmHour1.Text = "";
            this.FmMinute1.Text = "";
            this.AmHour2.Text = "";
            this.AmMinute2.Text = "";
            this.FmHour2.Text = "";
            this.FmMinute2.Text = "";

            workdays.Checked = false;
            holiday.Checked = true;
        }
        GetsetDays();
    }
    #endregion

    #region 插入新的时间记录
    protected void input_Click(object sender, EventArgs e)
    {
        if (workdays.Checked == true)                                 //如果该日位工作日
        {
            if (txtFromDate.Text != "")
            {
                string setDate = txtFromDate.Text + " ";//设置的工作日
                string amuptimeH = AmHour1.Text;
                string amuptimeM = AmMinute1.Text;
                string amdowntimeH = FmHour1.Text;
                string amdowntimeM = FmMinute1.Text;
                string fmuptimeH = AmHour2.Text;
                string fmuptimeM = AmMinute2.Text;
                string fmdowntimeH = FmHour2.Text;
                string fmdowntimeM = FmMinute2.Text;
                if (checkdate(ref amuptimeH, ref amuptimeM, ref amdowntimeH, ref amdowntimeM, ref fmuptimeH, ref fmuptimeM, ref fmdowntimeH, ref fmdowntimeM))
                {
                    string monuptime = setDate + amuptimeH + ":" + amuptimeM + ":" + "00";       //
                    string mondowntime = setDate + amdowntimeH + ":" + amdowntimeM + ":" + "00";     //按照表中字段的格式插入值
                    string afuptime = setDate + fmuptimeH + ":" + fmuptimeM + ":" + "00";        //
                    string afdowntime = setDate + fmdowntimeH + ":" + fmdowntimeM + ":" + "00";      //
                    
                   
                    DataTable dt;
                    dt = clsWorkDay.GetWorkTime(txtFromDate.Text);
                   
                    if (dt.Rows.Count > 0)
                    {
                       
                        int m = clsWorkDay.DelWorkTime(txtFromDate.Text);
                        
                        int n = clsWorkDay.InsWorkTime(txtFromDate.Text, monuptime, mondowntime, afuptime, afdowntime);
                        if (m > 0 && n > 0)
                        {
                            msgAppear.Visible = true;
                            msgAppear.Text = txtFromDate.Text + "日的工作时间设置完成";
                        }
                        else
                        {
                            msgAppear.Visible = true;
                            msgAppear.Text = "请重新设置";
                        }
                    }
                    else
                    {                                                     //如果没有，则直接插入新纪录
                        int i = clsWorkDay.InsWorkTime(txtFromDate.Text, monuptime, mondowntime, afuptime, afdowntime);
                        if (i > 0)
                        {
                            msgAppear.Visible = true;
                            msgAppear.Text = txtFromDate.Text + "日的工作时间设置完成";
                        }
                        else
                        {
                            msgAppear.Visible = true;
                            msgAppear.Text = "请重新设置";
                        }
                    }
                }

            }
            else
            {
                msgAppear.Visible = true;
                msgAppear.Text = "请选择要设置的日期";
            }

        }
        else                                                          //如果该日为假期
        {
            if (txtFromDate.Text != "")                               //插入过程同上
            {              
                DataTable dt;
                dt = clsWorkDay.GetWorkTime(txtFromDate.Text);
                if (dt.Rows.Count > 0)
                {                 
                    int m = clsWorkDay.DelWorkTime(txtFromDate.Text);
                    int n = clsWorkDay.InsWorkTime(txtFromDate.Text);
                    if (m > 0 && n > 0)
                    {
                        msgAppear.Visible = true;
                        msgAppear.Text = txtFromDate.Text + "日的工作时间设置完成";
                    }
                    else
                    {
                        msgAppear.Visible = true;
                        msgAppear.Text = "请重新设置";
                    }
                }
                else
                {                
                    int i = clsWorkDay.InsWorkTime(txtFromDate.Text);
                    if (i > 0)
                    {
                        msgAppear.Visible = true;
                        msgAppear.Text = txtFromDate.Text + "日的工作时间设置完成";
                    }
                    else
                    {
                        msgAppear.Visible = true;
                        msgAppear.Text = "请重新设置";
                    }
                }
            }
            else
            {
                msgAppear.Visible = true;
                msgAppear.Text = "请选择要设置的日期";
            }
        }

    }
    #endregion

    #region 判断日期格式
    public bool checkdate(ref string amuptimeH, ref string amuptimeM, ref string amdowntimeH, ref string amdowntimeM, ref string fmuptimeH, ref string fmuptimeM, ref string fmdowntimeH, ref string fmdowntimeM)
    {

        if (checkhour(ref amuptimeH) && checkhour(ref amdowntimeH) && checkhour(ref fmuptimeH) && checkhour(ref fmdowntimeH) && checkmin(ref amuptimeM) && checkmin(ref amdowntimeM) && checkmin(ref fmuptimeM) && checkmin(ref fmdowntimeM))
            return true;
        else
            return false;
    }
    #endregion

    #region 判断小时格式
    public bool checkhour(ref string hour)
    {
        bool returnH = false;
        if (hour != "")
        {
            if (Convert.ToInt32(hour) < 0 || Convert.ToInt32(hour) > 23)
            {
                msgAppear.Visible = true;
                msgAppear.Text = "小时必须介于0～23之间";

            }
            else
            {
                if (hour == "0")
                {
                    returnH = true;
                    hour = "00";

                }
                else
                {
                    returnH = true;
                }
            }
        }
        else
        {
            msgAppear.Visible = true;
            msgAppear.Text = "小时不能为空";
        }
        return returnH;
    }
    #endregion

    #region 判断分钟格式
    public bool checkmin(ref string minute)
    {
        bool returnM = false;
        if (minute != "")
        {
            if (Convert.ToInt32(minute) < 0 || Convert.ToInt32(minute) > 59)
            {
                msgAppear.Visible = true;
                msgAppear.Text = "分钟必须介于0～59之间";
            }
            else
            {
                if (minute == "0")
                {
                    returnM = true;
                    minute = "00";

                }
                else
                {
                    returnM = true;
                }
            }
        }
        else
        {
            msgAppear.Visible = true;
            msgAppear.Text = "分钟不能为空";
        }
        return returnM;
    }
    #endregion

    #region 批量生成时间记录  --其过程与插入新的时间记录大同小异
    protected void creatdays_Click(object sender, EventArgs e)
    {
        if (txtFromDate.Text != "")
        {
            if (days.Text != "")
            {
                int daynum = Convert.ToInt32(days.Text);
                DateTime dtFromDate = Convert.ToDateTime(txtFromDate.Text);

                for (int i = 1; i <= daynum; i++)
                {
                    //string monuptime = "1900-1-1 " + AmHour1.Text + ":" + AmMinute1.Text + ":" + "00";       //
                    //string mondowntime = "1900-1-1 " + FmHour1.Text + ":" + FmMinute1.Text + ":" + "00";     //按照表中字段的格式插入值
                    //string afuptime = "1900-1-1 " + AmHour2.Text + ":" + AmMinute2.Text + ":" + "00";        //
                    //string afdowntime = "1900-1-1 " + FmHour2.Text + ":" + FmMinute2.Text + ":" + "00";      //                    
                    DataTable dt;
                    dt = clsWorkDay.GetWorkTime(dtFromDate.AddDays(i).ToShortDateString());                    
                    if (dt.Rows.Count > 0)
                    {                        
                        int delNum = clsWorkDay.DelWorkTime(dtFromDate.AddDays(i).ToShortDateString());
                    }                    
                    int k = 0;
                    if (workdays.Checked == true)                    
                    {
                        string amuptimeH = AmHour1.Text;
                        string amuptimeM = AmMinute1.Text;
                        string amdowntimeH = FmHour1.Text;
                        string amdowntimeM = FmMinute1.Text;
                        string fmuptimeH = AmHour2.Text;
                        string fmuptimeM = AmMinute2.Text;
                        string fmdowntimeH = FmHour2.Text;
                        string fmdowntimeM = FmMinute2.Text;
                        if (checkdate(ref amuptimeH, ref amuptimeM, ref amdowntimeH, ref amdowntimeM, ref fmuptimeH, ref fmuptimeM, ref fmdowntimeH, ref fmdowntimeM))
                        {
                            string nDateString = dtFromDate.AddDays(i).ToShortDateString() + " ";
                            string monuptime = nDateString + amuptimeH + " :" + amuptimeM + ":" + "00";           //
                            string mondowntime = nDateString + amdowntimeH + " :" + amdowntimeM + ":" + "00";     //按照表中字段的格式插入值
                            string afuptime = nDateString + fmuptimeH + " :" + fmuptimeM + ":" + "00";            //
                            string afdowntime = nDateString + fmdowntimeH + " :" + fmdowntimeM + ":" + "00";      //

                            /******插入批量生成的数据******/   
                            if ((saturday.Checked == true) && (Convert.ToInt32(dtFromDate.AddDays(i).DayOfWeek) == 6 || Convert.ToInt32(dtFromDate.AddDays(i).DayOfWeek) == 0))
                            {
                                k = clsWorkDay.InsWorkTime(dtFromDate.AddDays(i).ToShortDateString());
                            }
                            else
                            {
                                k = clsWorkDay.InsWorkTime(nDateString, monuptime, mondowntime, afuptime, afdowntime);
                            }
                        }
                    }
                    else
                    {
                        k = clsWorkDay.InsWorkTime(dtFromDate.AddDays(i).ToShortDateString());
                    }                   
                    if (k > 0)
                    {
                        msgAppear.Visible = true;
                        msgAppear.Text = "批量设置的工作时间设置完成";
                    }
                    else
                    {
                        msgAppear.Visible = true;
                        msgAppear.Text = "请重新设置";
                    }
                }
            }

            else
            {
                msgAppear.Visible = true;
                msgAppear.Text = "请输入批量设置的天数";
            }
        }
        else
        {
            msgAppear.Visible = true;
            msgAppear.Text = "请选择要设置的日期";
        }


    }
    #endregion

    #region 获取当前月已经设置工作时间的天
    public void GetsetDays()
    {
       
        DataTable dt;
        dt = clsWorkDay.GetsetDays();
        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (Convert.ToDateTime(dt.Rows[i]["sdate"]).Month == Calendar1.TodaysDate.Month)
                {
                    Calendar1.SelectedDates.Add(Convert.ToDateTime(dt.Rows[i]["sdate"]));
                }
            }
        }

        //Calendar1.SelectedDate =DateTime.Now;
        Calendar1.SelectedDayStyle.ForeColor = Color.Red;
    }
    #endregion
}