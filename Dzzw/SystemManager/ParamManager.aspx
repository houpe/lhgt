﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ParamManager.aspx.cs" Inherits="SystemManager_ParamManager" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>数据字典管理</title>
    <link href="../css/page.css" rel="Stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <script type="text/javascript">
        function newParam() {
            var name = prompt('请输入数据字典类别名', '');
            if (name != "") {
                window.location.href = "ParamItemManager.aspx?action=add&name=" + name;
            }
            return false;
        }
    </script>
    <div style="text-align: center">
        <asp:Label ID="lblLabel1" runat="server" Text="请输入数据字典名"></asp:Label>
        &nbsp;
        <asp:TextBox ID="txtParamItem" runat="server"></asp:TextBox>
        &nbsp;
        <asp:Button ID="btnQuery" runat="server" OnClick="btnQuery_Clicked" Text="查询" />
        &nbsp;
        <input type="button" id="btnInsertType" class="NewButton" onclick="newParam()" value="新增类别" />
    </div>
    <br />
    <controlDefine:CustomGridView ID="CustomGridView1" runat="server" AllowPaging="false"
        AutoGenerateColumns="false" Width="100%" ShowHideColModel="None" SkinID="List"
        OnOnLoadData="CustomGridView1_OnLoadData">
        <Columns>
            <asp:BoundField DataField="name" HeaderText="数据字典类名"></asp:BoundField>
            <asp:BoundField DataField="item_count" HeaderText="个数"></asp:BoundField>
            <asp:TemplateField HeaderText="字典项">
                <ItemTemplate>
                    <asp:HyperLink ID="hlChildren" runat="server" NavigateUrl='<%#string.Format("ParamNameManager.aspx?name={0}", Eval("name")) %>'>查看</asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="删除">
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                <ItemTemplate>
                    <asp:HyperLink ID="hlDelete" runat="server" NavigateUrl='<%# string.Format("ParamManager.aspx?action=delete&name={0}", Eval("name"))%>'>删除</asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerSettings Visible="False" />
        <HeaderStyle CssClass="top_table" Height="30px" Width="50px" />
        <RowStyle CssClass="inputtable" />
    </controlDefine:CustomGridView>
    </form>
</body>
</html>
