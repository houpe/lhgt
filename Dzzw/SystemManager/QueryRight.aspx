﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="QueryRight.aspx.cs" Inherits="SystemManager_QueryRight" %>

<%@ Register Src="~/UserControls/Calendar.ascx" TagName="Calendar" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>查询权限授权</title>
    <link href="../css/page.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div class="box_middle">
        <div style="text-align: right; height: 30px; padding-top: 5px;">
            待授权流程:
            <asp:DropDownList ID="ddlProcessName" runat="server" Width="200">
            </asp:DropDownList>
            授权给:
            <asp:DropDownList ID="ddlUnit" runat="server" Width="150" AutoPostBack="True" OnSelectedIndexChanged="ddlUnit_SelectedIndexChanged">
            </asp:DropDownList>
            <asp:DropDownList ID="ddlDep" runat="server" Width="150" AutoPostBack="True" OnSelectedIndexChanged="ddlDep_SelectedIndexChanged">
            </asp:DropDownList>
            <asp:DropDownList ID="ddlUserName" runat="server" Width="100">
            </asp:DropDownList>
        </div>
        </div>
        <div style=" text-align:center;">
        <asp:Button Text="新增授权" ID="btnUpdate" class="NewButton" runat="server" OnClick="btnUpdate_Click" />
        <controlDefine:MessageBox ID="mbInfo" runat="server" Width="100%" BoxType="Info" />
    </div>
    <asp:GridView ID="gv" runat="server" DataKeyNames="ID" AutoGenerateColumns="False"
        BorderColor="#3366CC" Width="100%" CssClass="tab" OnRowDeleting="gv_RowDeleting">
        <Columns>
            <asp:BoundField HeaderText="用户" DataField="UserName" />
            <asp:BoundField DataField="task" HeaderText="有权限的业务" />
            <asp:TemplateField HeaderText="删除" ShowHeader="False">
                <ItemTemplate>
                    <asp:LinkButton ID="ImageButton1" runat="server" CausesValidation="false" CommandName="Delete"
                        CommandArgument='<%#DataBinder.Eval(Container.DataItem,"ID")%>' Text="删除" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <controlDefine:Pager ID="gvPage" runat="server" ControlToPager="gv" Width="100%" />
    </form>
</body>
</html>
