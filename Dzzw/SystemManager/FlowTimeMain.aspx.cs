﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Business.FlowOperation;

public partial class SystemManager_FlowTimeMain : PageBase
{
    
    protected void Page_Load(object sender, EventArgs e)
    {        
        if (!IsPostBack)
        {
            BindWorkFlow();
            BindData();
        }
    }

    #region 绑定所有业务
    /// <summary>
    /// 绑定所有业务
    /// </summary>
    public void BindWorkFlow()
    {
        ClsUserWorkFlow clsWorkFlow = new ClsUserWorkFlow();
        System.Data.DataTable dtSource = clsWorkFlow.GetFlowName();
        ywlx.DataSource = dtSource;
        ywlx.DataTextField = "WNAME";
        ywlx.DataValueField = "WNAME";
        ywlx.DataBind();
        this.ywlx.Items.Insert(0,new ListItem("所有业务", ""));
    }
    #endregion

    /// <summary>
    /// 绑定业务记录
    /// </summary>
    public void BindData()
    {
        string strYwlx = Request["ywlx"];//业务类型	
        string strStartDate = Request["startDate"];//开始时间	
        string strEndDate = Request["endDate"];//结束时间	
        string strSqdw = Request["sqdw"];//申请单位
        string strYwNo = Request["ywno"];//内部业务编号	
        string strYwStatus = Request["status"];//业务状态
        string strSql = string.Empty;
        DataTable dtOut = new DataTable();
        dtOut = Business.Admin.ClsWorkDaySet.GetWorkTime(strSqdw, strStartDate, strEndDate, strYwlx);

        this.gvSerial.DataSource = dtOut;
        gvSerial.PageSize = Business.SystemConfig.PageSize;
        gvSerial.RecordCount = dtOut.Rows.Count;
        this.gvSerial.DataBind();

    }

    /// <summary>
    /// 绑定数据
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvSerial_OnLoadData(object sender, EventArgs e)
    {
        BindData();
    }

    /// <summary>
    /// 提交方法
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        int nStart = gvSerial.PageIndex*gvSerial.PageSize;
        int nTag = nStart+gvSerial.PageSize;
        nTag = (nTag < gvSerial.RecordCount) ? nTag : gvSerial.RecordCount;
        for (int i=nStart; i < nTag; i++)
        {
            int nRow = i % gvSerial.PageSize;
            string strIID = gvSerial.Rows[nRow].Cells[3].Text;

            TextBox txtTemp = gvSerial.Rows[nRow].FindControl("txtTime") as TextBox;
            string strValue = txtTemp.Text;

            Business.FlowOperation.ClsUserWorkFlow clsWorkFlow = new Business.FlowOperation.ClsUserWorkFlow();
            clsWorkFlow.UpdateTime(strIID, strValue);
        }
    }

}
