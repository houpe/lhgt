﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MenuManage.aspx.cs" Inherits="SystemManager_MenuManage" %>

<html>
<head runat="server">
    <title>菜单列表</title>
    <link href="../css/page.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function t() {
            if (window.confirm('确认删除?')) {
                return true;
            }
            else
                return false;
        }

        function clickUrl(strUrl) {
            this.location.href = strUrl;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <controlDefine:MessageBox ID="msgAppear" Visible="false" runat="server"></controlDefine:MessageBox>
    <div class="box_middle">
        <div style="text-align: right; height: 30px; padding-top: 5px;">
            <input type="button" id="btnAddMenuGroup" class="NewButton" value="添加菜单组" onclick="clickUrl('AddGroupMenu.aspx')" />
            &nbsp; &nbsp; &nbsp;
            <input type="button" id="btnAddChildMenu" class="NewButton" value="添加子菜单" onclick="clickUrl('AddChildMenu.aspx')" />&nbsp;
            &nbsp; &nbsp;
        </div>
    </div>
    <div id='context' runat="server">
    </div>
    </form>
</body>
</html>
