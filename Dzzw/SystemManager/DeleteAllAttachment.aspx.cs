﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using WF_DataAccess;
using Business;
using WF_Business;


/// <summary>
/// Summary description for delete_all_attachment.
/// </summary>
public partial class SystemManager_DeleteAllAttachment : System.Web.UI.Page
{
    protected void Page_Load(object sender, System.EventArgs e)
    {
        //Put user code to initialize the page here
        if (!IsPostBack)
        {
            string iid = Request["iid"];
            string strSql = "";

            strSql = "select table_name from user_tables where table_name like 'UT_%'";
            DataTable rs;
            SysParams.OAConnection().RunSql(strSql, out rs);

            IDataAccess ida = SysParams.OAConnection(true);
            try
            {
                strSql = "select res_value from st_dynamic_resource where type=2 and iid=" + iid;
                SysParams.OAConnection().RunSql(strSql, out rs);
                for (int i = 0; i < rs.Rows.Count; i++)
                {
                    strSql = "delete from st_attachment where aid='" + System.Convert.ToString(rs.Rows[i]["res_value"]) + "'";
                    ida.RunSql(strSql);
                }
                strSql = "delete from st_dynamic_resource where iid=" + iid;
                ida.RunSql(strSql);
                ida.Close(true);
            }
            catch (Exception ex)
            {
                ida.Close(false);
                Response.Write("javascript:alert('删除失败，原因是" + ex.Message + "')");
            }
            Response.Redirect("editattachment.aspx");
        }
    }

    
}
