﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Business.Menu;

public partial class SystemManager_AddGroupMenu : System.Web.UI.Page
{

    #region 窗体加载
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request["parentMname"] != "")
                menuname.Text = Request["parentMname"];
            if (Request["parentMpic"] != "")
                picture.Text = Request["parentMpic"];
            if (Request["parentMid"] != "")
                parentid.Value = Request["parentMid"];
        }
    }
    #endregion

    protected void editdata_Click(object sender, EventArgs e)
    {
        menuname.Text = "";
        picture.Text = "";
    }

    #region 返回菜单管理主界面
    protected void Button1_Click(object sender, EventArgs e)
    {
        Response.Redirect("MenuManage.aspx");
    }
    #endregion

    #region  插入菜单组
    protected void inputdata_Click(object sender, EventArgs e)
    {
        if(menuname.Text!="")
        {
            XtMenuOperation.InserChildMenu(menuname.Text.Trim(), picture.Text.Trim());           
            msgAppear.Text = "插入菜单组成功";
        }
    }
    #endregion

}
