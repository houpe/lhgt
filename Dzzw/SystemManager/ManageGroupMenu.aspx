<%@ Page Language="c#" Inherits="SystemManager_ManageGroupMenu" CodeFile="ManageGroupMenu.aspx.cs" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>组菜单管理</title>
    <link href="../css/page.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        function checkall(parent, sender, count) {
            for (i = 0; i < count; i++) {
                var Childobj = document.getElementById(parent + i);
                Childobj.checked = sender.checked;
            }
        }
    </script>

</head>
<body>
    <form name="form1" method="post" action="SaveGroupMenu.aspx">
    <table width="100%" height="391" class="tab">
        <%
            
            String groupid = Request["groupid"];

            Business.Menu.XtMenuOperation menuOperation = new Business.Menu.XtMenuOperation();
            System.Data.DataTable dtParent = menuOperation.GetParentMenu();

            String countchild = "0";

            String isChecked = string.Empty;
            System.Data.DataTable dtChild = null;

            foreach (System.Data.DataRow drRecord in dtParent.Rows)
            {
                Response.Write("<tr>");
                dtChild = menuOperation.GetChildMenuInfo(drRecord["MENU_PARENT_DESC"].ToString());
                //countchild = menuOperation.GetChildMenuCount(drRecord["MENU_PARENT_DESC"].ToString());
                countchild = dtChild.Rows.Count.ToString();
    
        %>
        <td>
            <span>
                <%=drRecord["MENU_PARENT_DESC"]%>
                <input type="checkbox" id="<%=drRecord["MENU_PARENT_DESC"]%>" onclick="checkall('<%=drRecord["MENU_PARENT_DESC"]%>',this,<%=countchild%>)" />
            </span>
        </td>
        <%
		
            for (int i = 0; i < dtChild.Rows.Count; i++)
            {
                string strReturn = menuOperation.GetCheckedMenuId(groupid,
                    dtChild.Rows[i]["MENU_CHILD_ID"].ToString());

                if (!string.IsNullOrEmpty(strReturn))
                    isChecked = "checked";
                else
                    isChecked = string.Empty;
			
        %>
        <td>
            <input type="checkbox" name="<%=dtChild.Rows[i]["MENU_CHILD_ID"]%>" id="<%=drRecord["MENU_PARENT_DESC"]%><%=i%>"
                value="checkbox" <%=isChecked%> />
            <%=dtChild.Rows[i]["MENU_CHILD_DESC"]%>
        </td>
        <%
            }
            Response.Write("</tr>");
           }
        %>
    </table>
    <div style="text-align: center">
        <input name="groupid" type="hidden" value="<%=groupid%>" />
        <input type="submit" class="NewButton" name="cmd" value="提交" />
        <input type="button" class="NewButton" name="cancel" onclick="javascript:history.go(-1)"
            value="返回" />
    </div>
    </form>
</body>
</html>
