﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DepartmentEditor.aspx.cs"
    Inherits="SystemManager_DepartmentEditor" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>部门设置</title>
    <link rel="stylesheet" type="text/css" href="../css/page.css" />
</head>
<body>
    <form name="queryForm" action="" runat="server">
    <div align="center">
        <controlDefine:MessageBox ID="msgAppear" runat="server" />
    </div>
    <div class="box_top_bar">
        <div class="box_top_left">
            <div class="box_top_right">
                <div class="box_top_text">
                    设置部门信息</div>
            </div>
        </div>
    </div>
    <div class="box_middle" style="height:130px;">
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="box_middle_tab">
            <tr>
                <td>
                    部门名称：
                </td>
                <td style="width: 253px">
                    <asp:TextBox MaxLength="100" size="23" ID="bmmc" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    上级单位：(如果没有下属单位，该处为空)
                </td>
                <td style="width: 253px">
                    <asp:DropDownList ID="sjbmlist" runat="server">
                    </asp:DropDownList>
                    <asp:TextBox Width="0px" size="23" ID="bmid" runat="server" Visible="false"></asp:TextBox>
                    <asp:TextBox Width="0px" size="23" ID="sjbm" runat="server" Visible="false"></asp:TextBox>
                    <asp:TextBox ID="orderId" runat="server" size="23" Visible="false" Width="0px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Button ID="inputdata" class="input" Text="新增部门" runat="server" OnClick="inputdata_Click" />
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="editdata" Text="保存" class="input" runat="server" OnClick="editdata_Click" />
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="deldata" Text="删除" class="input" runat="server" OnClick="deldata_Click" />
                </td>
            </tr>
        </table>
    </div>
    <div style="text-align: left; color: Blue; font-size:10pt;">
        注：修改部门名称时，如果有上级部门，必须选择上级部门；没有的可不选在左边部门树中选择需要修改的部门，修改后点保存，如果需要新增，则先点新增部门按钮切换为新增状态，然后录入部门数据点保存便可.
    </div>
    <br />
    <%
        System.Data.DataTable dt;
        dt = Business.Admin.StUserOperation.GetUserLoginInfo(bmid.Text, sjbmlist.SelectedValue, QueCon.Text);
        if (dt.Rows.Count > 0)
        {
    %>
    <div class="box_top_bar">
        <div class="box_top_left">
            <div class="box_top_right">
                <div class="box_top_text">
                    部门人员维护</div>
            </div>
        </div>
    </div>
    <div class="box_middlenew">
        <table width="90%" border="0" cellpadding="0" cellspacing="0" class="box_middle_tab">
            <tr>
                <td style="height: 25px; text-align: left" colspan="3">
                    用户ID/姓名:<asp:TextBox ID="QueCon" runat="server"></asp:TextBox>
                    <asp:Button ID="btnQuery" Text="查询" runat="server" CssClass="input" />
                </td>
                <td style="height: 25px" align="left">
                    <asp:Button ID="submit" Text="保存设置" CssClass="input" runat="server" OnClick="submit_Click">
                    </asp:Button>
                </td>
            </tr>
            <tr>
                <td>
                    用户ID
                </td>
                <td align="left">
                    用户姓名
                </td>
                <td>
                    状态
                </td>
                <td>
                    排序号
                </td>
            </tr>
            <%
                for (int j = 0; j < dt.Rows.Count; j++)
                {
                    string selected = "";
                    if (dt.Rows[j]["checked"].ToString() == "1") selected = "checked";
                    txtOrderByID.Text = dt.Rows[j]["orderbyid"].ToString();
            %>
            <tr>
                <td>
                    <% =dt.Rows[j]["login_name"].ToString()%>
                </td>
                <td>
                    <% =dt.Rows[j]["user_name"].ToString()%>
                </td>
                <td>
                    <input name="check" type="checkbox" <%=selected %> value="<%=dt.Rows[j]["userid"].ToString()%>" />
                </td>
                <td>
                    <asp:TextBox ID="txtOrderByID" runat="server" Width="25px" Height="10px"></asp:TextBox>
                </td>
            </tr>
            <%               
                }
                                            
            %>
            <tr>
                <td>
                    <input name="check" type="checkbox" style="visibility: hidden" checked value="" />
                </td>
            </tr>
        </table>
    </div>
    <% 
}
    %>
    </form>
</body>
</html>
