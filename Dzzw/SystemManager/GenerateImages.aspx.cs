﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Drawing.Imaging;
using System.Data.OracleClient;
using WF_DataAccess;
using Business;
using WF_Business;

public partial class GenerateImages : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            GenerateNameSign();
        }
    }

    public void GenerateNameSign()
    {
        IDataAccess ida = SysParams.OAConnection(false);
        string strSql = "select userid,user_name  from st_user";

        DataTable dtTemp = null;
        ida.RunSql(strSql, out dtTemp);

        foreach (DataRow drOne in dtTemp.Rows)
        {
            string strName = drOne["user_name"].ToString();
            string strUserId = drOne["userid"].ToString();

            Bitmap bt = new Bitmap(60, 40, PixelFormat.Format24bppRgb);
            Graphics g = Graphics.FromImage(bt);
            g.FillRectangle(new SolidBrush(Color.White), 0, 0, 60, 40);

            Font fn1 = new Font("Arial", 12, FontStyle.Bold);
            g.DrawString(strName, fn1, Brushes.Black, 0, 15);

            System.IO.MemoryStream ms = new System.IO.MemoryStream();
            bt.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
            byte[] buffer = ms.GetBuffer();

            g.Dispose();
            bt.Dispose();
            //Response.OutputStream.Write(buffer, 0, buffer.Length);

            strSql = string.Format("select count(userid) from ST_USER_SIGN t where USERID='{0}'",strUserId);
            string strCount = ida.GetValue(strSql);

            //更新签名
            if (strCount != "0")
            {
                strSql = string.Format("update ST_USER_SIGN t set t.SIGNPIC=:usersign where USERID='{0}'",
                    strUserId);
            }
            else
            {
                strSql = string.Format("insert into ST_USER_SIGN(userid,SIGNPIC,savedate) values('{0}',:usersign,sysdate)",
                    strUserId);
            }

            System.Data.IDataParameter[] idp = new System.Data.OracleClient.OracleParameter[1];
            idp[0] = new OracleParameter(":usersign", OracleType.Blob);
            idp[0].Value = buffer;

            ida.RunSql(strSql, ref idp);
        }
    }


    protected void btnGenerate_Click(object sender, EventArgs e)
    {
        GenerateNameSign();
    }
}
