﻿ 
// 创建人  ：Wu Hansi
// 创建时间：2007年7月26日
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Business.Admin;

/// <!--
/// 功能描述  : 功能描述
/// 创建人  : Wu Hansi
/// 创建时间: 2007年7月26日
/// -->
public partial class SystemManager_ParamNameManager : System.Web.UI.Page
{
    DictOperation dicOperation = new DictOperation();
    protected void Page_Load(object sender, EventArgs e)
    {
        string action = Request.QueryString["action"];
        if (action == "delete")
        {
            string rowid = Request.QueryString["rowid"];
            if (rowid != null && rowid != string.Empty)
            {
                string name = Request.QueryString["name"];
                dicOperation.DeleteDictItem(rowid);
                Response.Redirect("ParamNameManager.aspx?name=" + name);
            }
        }
        if (!IsPostBack)
        {
            BindData();
        }
    }
    protected void CustomGridView1_OnLoadData(object sender, EventArgs e)
    {
        BindData();
    }
    private void BindData()
    {
        DataTable dtSource = dicOperation.GetDictFromName(Request.QueryString["name"]);
        CustomGridView1.DataSource = dtSource;
        CustomGridView1.RecordCount = dtSource.Rows.Count;
        CustomGridView1.DataBind();
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        Response.Redirect("ParamItemManager.aspx?name=" + Request.QueryString["name"] + "&action=add");
    }
    protected void btnReturn_Click(object sender, EventArgs e)
    {
        Response.Redirect("ParamManager.aspx");
    }
}
