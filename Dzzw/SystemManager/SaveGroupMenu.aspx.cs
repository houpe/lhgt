using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Business;
using WF_DataAccess;
using WF_Business;

/// <summary>
/// Summary description for manager_group_menu_save.
/// </summary>
public partial class SystemManager_SaveGroupMenu : PageBase
{
    protected void Page_Load(object sender, System.EventArgs e)
    {
        string strGroupId = Request["groupid"];
        IDataAccess dataAccess = SysParams.OAConnection(true);

        try
        {
            String strSql = string.Format("delete from xt_group_menu where right_id='{0}'", strGroupId);
            dataAccess.RunSql(strSql);

            strSql = "select * from xt_menu_child";

            System.Data.DataTable dtOut;
            dataAccess.RunSql(strSql, out dtOut);

            foreach (System.Data.DataRow drRecord in dtOut.Rows)
            {
                string strChildId = drRecord["MENU_CHILD_ID"].ToString();
                string strChecked = Request[strChildId];

                if (!string.IsNullOrEmpty(strChecked))
                {
                    if (strChecked.Equals("checkbox"))
                    {
                        strSql = string.Format("insert into xt_group_menu(right_id,menu_child_id)" +
                            " values('{0}','{1}')", strGroupId, strChildId);
                        dataAccess.RunSql(strSql);
                    }
                }
            }

            dataAccess.Close(true);
        }
        catch
        {
            dataAccess.Close(false);
            ClientScript.RegisterStartupScript(Page.GetType(), "returnHis", "window.location.href='manage_group_menu.aspx'");
        }	
    }

    
}
