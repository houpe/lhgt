﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Business;

public partial class SystemManager_ParamItemManager : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        PersistenceControl1.PageControl = this.form1;

        if (!IsPostBack)
        {
            string action = Request.QueryString["action"];
            if (action == "add")
            {
                txtName.Value = Request.QueryString["name"];
                PersistenceControl1.Status = "insert";
            }
            else//编辑
            {
                txtName.Enabled = false;
                PersistenceControl1.KeyValue = Request.QueryString["rowid"];
                PersistenceControl1.Status = "edit";
            }

           
            PersistenceControl1.Render();
        }
        
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        PersistenceControl1.Update();

        if (txtName.Enabled)
        {
            Response.Redirect("ParamManager.aspx");
        }
        else
        {
            Response.Redirect("ParamNameManager.aspx?name=" + txtName.Text);
        }
    }

    protected void btnReturn_Click(object sender, EventArgs e)
    {
        if (txtName.Enabled)
        {
            Response.Redirect("ParamManager.aspx");
        }
        else
        {
            Response.Redirect("ParamNameManager.aspx?name=" + txtName.Text);
        }
    }
}
