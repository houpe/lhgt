<%@ Page Language="c#" Inherits="SystemManager_AddGroup" CodeFile="AddGroup.aspx.cs" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>添加组</title>
    <link rel="stylesheet" type="text/css" href="../css/page.css" />
</head>
<body>
    <form runat="server" action="">
    <div align="center">
        <table border="0" cellspacing="1" cellpadding="2">
            <tr>
                <td align="center">
                    组名：
                </td>
                <td>
                    <asp:TextBox ID="txtGroupName" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="center">
                    描述：
                </td>
                <td>
                    <asp:TextBox ID="txtGroupDescrip" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    父节点角色ID：
                </td>
                <td>
                    <asp:TextBox ID="txtGroupNum" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    可中止:
                </td>
                <td style="text-align: center">
                    <asp:CheckBox ID="ckbIs_Stop" runat="server" Checked="true" />
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center" style="height: 28px">
                    <asp:Button ID="btnAddGroup" runat="server" OnClick="btnAddGroup_Click" Text="添加组" />
                    <input type="button" value="返回" onclick='window.navigate("openusergrouplist.aspx")' class="newbutton" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <controlDefine:MessageBox ID="msgAppear" runat="server" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
