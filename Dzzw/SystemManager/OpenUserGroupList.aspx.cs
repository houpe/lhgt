using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Business.Common;

/// <summary>
/// 角色维护代码迁移
/// 修改人：YZG
/// 时间：2010-03-01
/// </summary>
public partial class SystemManager_OpenUserGroupList : PageBase
{
    protected void Page_Load(object sender, System.EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            DelUserGroup();
        }
        BindUserGroup();
    }

    /// <summary>
    /// 删除用户组
    /// </summary>
    protected void DelUserGroup()
    {
        //删除指定用户组信息
        string delgroupid = Request["deletegroup"];

        try
        {
            if (!string.IsNullOrEmpty(delgroupid))
            {
                Business.Admin.StUserGroupHandle.DeleteGroupById(delgroupid);
            }
        }
        catch
        {
            ExceptionManager.WriteAlert(Page, "改组已被分配到一个岗位上，不能被删除！");
        }
    }

    /// <summary>
    /// 绑定用户组
    /// </summary>
    protected void BindUserGroup()
    {
                //获取所有用户组信息
        System.Collections.Generic.List<Business.Struct.GroupStruct> lstGroup =
            Business.Admin.StUserGroupHandle.GetAllGroupInfo(txtSeach.Text);
        System.Text.StringBuilder html = new System.Text.StringBuilder();

        html.Append("<table class=\"tab\" width=\"100%\" border=\"1\">");
        html.Append("<tr class=\"title\" align=\"center\" valign=\"middle\">");
        html.Append("<th>组名</th><th>序号</th><th>组用户管理</th><th>编辑</th><th>设置权限</th><th>删除</th></tr>");

        foreach (Business.Struct.GroupStruct group in lstGroup)
        {
            html.Append("<tr align=\"center\" valign=\"middle\">");
            html.Append(string.Format("<td>{0}</td>", group.GroupName));
            html.Append(string.Format("<td>{0}</td>", group.OrderNum));

            html.Append(string.Format("<td><a href=\"MemberMIS.aspx?groupid={0}&groupname={1}\" target='_self'>成员管理</a></td>", group.GroupId, group.GroupName));
            html.Append(string.Format("<td><a href=\"EditGroup.aspx?groupid={0}\">编辑</a></td>", group.GroupId));
            html.Append(string.Format("<td><a href=\"ManageGroupMenu.aspx?groupid={0}\">设置权限</a></td>", group.GroupId));
            html.Append(string.Format("<td><a href=\"OpenUserGroupList.aspx?deletegroup={0}\" onclick=\"return DelOK();\">删除</a></td>", group.GroupId));

            html.Append("</tr>");
        }
        html.Append("</table>");

        this.context.InnerHtml = html.ToString();
    }    
}
