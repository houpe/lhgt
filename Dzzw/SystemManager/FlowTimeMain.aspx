﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FlowTimeMain.aspx.cs" Inherits="SystemManager_FlowTimeMain" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head runat="server">
    <title>业务优先级设置</title>
    <link rel="stylesheet" id="skinCss" type="text/css" href="../css/default/easyui.css" />
    <link rel="stylesheet" type="text/css" href="../css/icon.css" />
    <link href="../css/page.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../ScriptFile/jquery.js"></script>
    <script type="text/javascript" src="../ScriptFile/jquery.easyui.min.js"></script>
    <script src="../ScriptFile/easyui-lang-zh_CN.js" type="text/javascript"></script>
    <script type="text/javascript">
        function tosubmit(strYwNo, strYwStatus, strPageNo) {
            var strUrl = "FlowTimeMain.aspx?";
            strUrl += "startDate=" + $("#startDate").datebox('getValue');
            strUrl += "&endDate=" + $("#endDate").datebox('getValue');
            strUrl += "&ywlx=" + document.all.ywlx.value;
            strUrl += "&sqdw=" + document.all.sqdw.value;
            strUrl += "&ywno=" + strYwNo;
            if (strYwNo != "")
                strUrl += "&status=" + strYwStatus.value;
            else
                strUrl += "&status=";
                
            window.location.href = strUrl;

        }
        function hrefonclic() {
            tosubmit("", null, "");
        }

        function OpenUrl(id) {
            window.location.href = "FlowDetailTimeMain.aspx?FlowID=" + id;
        }
    </script>
</head>
<body runat="server">
    <form name="form1" method="post" runat="server">
    <div style="width: 100%; margin: 0px auto; text-align: center;">
    <table class="tab_top" width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <img src="../images/tab_top_left.gif" width="17"  height="35"/>
            </td>
            <td>
                开始时间:
                <input class="easyui-datebox" style="width:100px;" id="startDate" value="<%=Request["startDate"]%>" />
                结束时间:
                <input class="easyui-datebox"  style="width:100px;" id="endDate"  value="<%=Request["endDate"]%>"/>
                业务类型
                <select name="ywlx" id="ywlx" style="width: 130px" runat="server">
                </select>
                申请单位<input style="width: 100px" name="sqdw" id="sqdw" runat="server">
                <input type="button" value="查询" class="NewButton" name="button1" onclick="hrefonclic()">
                <asp:Button runat="server" ID="btnSubmit" Text="提交" CssClass="newbutton" OnClick="btnSubmit_Click" />
            </td>
            <td align="right">
                <img alt="" src="../images/tab_top_right.gif" width="5" />
            </td>
        </tr>
    </table>
    <controlDefine:CustomGridView runat="server" ID="gvSerial" SkinID="List" AutoGenerateColumns="false" OnOnLoadData="gvSerial_OnLoadData" ShowHideColModel="None" AllowPaging="true"
        ShowItemNumber="true" Width="100%">
        <Columns>
            <asp:BoundField DataField="iid" HeaderText="业务编号" />
            <asp:BoundField DataField="wname" HeaderText="业务类型" />
            <asp:BoundField DataField="name" HeaderText="申请单位" />
            <asp:TemplateField HeaderText="开始时间" ShowHeader="False">
                <ItemTemplate>
                    <asp:TextBox ID="txtTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"time")%>'></asp:TextBox>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="处理" ShowHeader="False">
                <ItemTemplate>
                    <a href='#' onclick="OpenUrl('<%#DataBinder.Eval(Container.DataItem,"iid")%>')">时间维护
                    </a>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </controlDefine:CustomGridView>
    </div>
    </form>
</body>
</html>
