<%@ Page Language="c#" Inherits="SystemManager_DepartUserList" CodeFile="DepartUserList.aspx.cs" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head runat="server">
    <title>部门工作人员列表</title>
    <link href="../css/page.css" rel="stylesheet" type="text/css" />
    <link href="../css/default/easyui.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../ScriptFile/jquery.js"></script>
    <script type="text/javascript" src="../ScriptFile/jquery.easyui.min.js"></script>

   <script type="text/javascript">
       //关闭窗口
       function CloseDivDlg() {
           $('#dlgConfig').window('close');
       }

       //打开内嵌网页的通用方法1（window）
       function openDivDlg(strTitle, strUrl) {
           var divWidth = 400;
           var divHeight = 200;

           //增加参数queryid
           $('#dlgConfig').window({
               title: strTitle, width: divWidth, height: divHeight
           });

           $('#iframeConfig').attr("src", strUrl);

           // $('#dlgConfig').window({ maximized: true });
           $('#dlgConfig').window('open');
       }

       function UploadUserSign(strUserId) {
           openDivDlg("用户签名上传", "UserEditor.aspx?userid=" + strUserId);
       }
    </script>
</head>
<body>
    <table width="100%" cellspacing="0" cellpadding="0" align="center" border="0" class="tab">
        <tr>
            <th>
                姓名
            </th>
            <th>
                签名图片
            </th>
            <th>
                更改图片
            </th>
        </tr>
        <%
            string strDepartId = Request["departId"];

            Business.Admin.DepartHandle userOperation = new Business.Admin.DepartHandle();
            System.Data.DataTable dtUser = userOperation.GetDepartUser(strDepartId);


            foreach (System.Data.DataRow drInfo in dtUser.Rows)
            {
                String userid = drInfo["USERID"].ToString();
                String username = drInfo["USER_NAME"].ToString();
			
        %>
        <tr>
            <td>
                <%=username%>
            </td>
            <td>
                <img src="UserSignShow.aspx?userID=<%=userid%>" />
            </td>
            <td>
                <a href="javascript:void(0);" onclick="UploadUserSign('<%=userid%>','edit',event);void(0)">
                    <img style="cursor: hand" src="../images/tab_end_ico.gif" border="0" /></a>
            </td>
        </tr>
        <%
            }
        %>
    </table>
    <div id="dlgConfig" class="easyui-window" title="文件上传" closed="true" modal="false"
        minimizable="false" style="width: 400px;height: 300px; top:50px; left:200px; background: #fafafa;">
        <iframe id="iframeConfig" scrolling="yes" frameborder="0" src="" style="width: 100%;
            height: 100%;"></iframe>
    </div>
</body>
</html>
