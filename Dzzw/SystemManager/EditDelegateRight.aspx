﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EditDelegateRight.aspx.cs" Inherits="SystemSetting_EditDelegateRight" %>
<%@ Register Src="~/UserControls/Calendar.ascx" TagName="Calendar" TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>查询权限设置</title>
    <link href="../css/page.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table class="box_middle_tab" width="80%">
                <tr>
                    <td colspan="6" style="text-align: center">
                        <controlDefine:MessageBox ID="mbInfo" runat="server" Width="100%" BoxType="Info" />
                        </td>                       
                </tr>
                 <tr>
                    <td style="width: 10%">
                        待委托流程
                    </td>
                    <td style="width: 30%" align="left">
                       <%=this.ProcessName%>
                    </td>
                    <td style="width: 10%">
                        委托给
                    </td>
                    <td style="width: 50%">
                        <asp:DropDownList ID="ddlUnit" runat="server" Width="30%" AutoPostBack="True" OnSelectedIndexChanged="ddlUnit_SelectedIndexChanged">
                        </asp:DropDownList>
                        <asp:DropDownList ID="ddlDep" runat="server" Width="30%" AutoPostBack="True" OnSelectedIndexChanged="ddlDep_SelectedIndexChanged">
                        </asp:DropDownList>
                        <asp:DropDownList ID="ddlUserName" runat="server" Width="30%">
                        </asp:DropDownList>
                    </td>
                </tr>
                 <tr>
                    <td style="width: 10%" nowrap="noWrap">
                        委托开始时间
                    </td>
                    <td style="width: 30%">                        
                        <uc1:Calendar ID="startDate" runat="server" style="width: 70%" maxlength="20" ReadOnly="false" TimeUsing="true"/>
                    </td>
                    <td style="width: 10%" nowrap="noWrap">
                        委托结束时间
                    </td>
                    <td style="width: 50%">
                        <uc1:Calendar ID="endDate" runat="server" style="width: 70%" maxlength="20" TimeUsing="true"/>
                    </td>
                     
                </tr>
                <tr>
                    <td colspan="6" align="right">
                        &nbsp;
                        <asp:Button Text="确定" ID="btnUpdate" class="input" runat="server" OnClick="btnUpdate_Click" />
                        &nbsp;&nbsp;
                        <asp:Button Text="取消" ID="btnCancel" class="input" runat="server" OnClick="btnCancel_Click" /></td>
                </tr>                
            </table>
        </div>
    </form>
</body>
</html>
