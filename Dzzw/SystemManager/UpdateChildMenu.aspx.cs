﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Business.Menu;

public partial class SystemManager_UpdateChildMenu : System.Web.UI.Page
{
    #region 窗体加载
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            XtMenuOperation Menu = new XtMenuOperation();
            DataTable dtparentmenu = Menu.GetParentMenu();
            if (dtparentmenu.Rows.Count > 0)
            {
                parentmenu.Items.Add("请选择...");
                for (int i = 0; i < dtparentmenu.Rows.Count; i++)
                {
                    parentmenu.Items.Add(new ListItem(dtparentmenu.Rows[i]["menu_parent_desc"].ToString(), dtparentmenu.Rows[i]["menu_parent_id"].ToString()));
                }
            }
            if (Request["childMname"] != "")
                this.menuname.Text = Request["childMname"];
            if (Request["childMpic"] != "")
                this.picture.Text = Request["childMpic"];
            if (Request["childMurl"] != "")
                this.menuUrl.Text = Request["childMurl"];
            if (Request["childMpar"] != "")
                this.parentmenu.Text = Request["childMpar"];
            if (Request["showno"] != "")
                this.txtShowNo.Text = Request["showno"];
            childid.Value = Request["childMid"];

        }
    }
    #endregion

    #region 修改子菜单
    protected void updatemenu_Click(object sender, EventArgs e)
    {
        XtMenuOperation Menu = new XtMenuOperation();
        Menu.UpdateChildMenu(menuname.Text, parentmenu.Text, menuUrl.Text, picture.Text, this.childid.Value,txtShowNo.Text);
        msgAppear.Text = "修改完成";
    }
    #endregion

    #region 返回菜单管理主界面
    protected void Button1_Click(object sender, EventArgs e)
    {
        Response.Redirect("MenuManage.aspx");
    }
    #endregion
}
