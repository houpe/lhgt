﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Business.FlowOperation;
using Business;

/// <summary>
/// Summary description for edit_attachment.
/// </summary>
public partial class SystemManager_EditAttachment : PageBase
{
    ClsUserWorkFlow clsUserWorkFlow = new ClsUserWorkFlow();

    protected void Page_Load(object sender, System.EventArgs e)
    {
        if (!IsPostBack)
        {
            BindDropDownListData();
            BindData();
        }
    }


    /// <summary>
    /// 绑定下拉框
    /// </summary>
    public void BindDropDownListData()
    {
        //绑定下拉框
        DataTable dtSource = clsUserWorkFlow.GetFlowNameByUser(UserId);
        ddlSerial.Items.Add("");
        foreach (DataRow drTemp in dtSource.Rows)
        {
            ListItem liTemp = new ListItem();
            liTemp.Text = drTemp["wname"].ToString();
            ddlSerial.Items.Add(liTemp);
        }
    }

    /// <summary>
    /// 绑定数据
    /// </summary>
    private void BindData()
    {
        DataTable dtSource = ClsUserWorkFlow.GetInstanceInfo(ddlSerial.Text, ddlYxj.SelectedValue, txtSqdw.Text, txtNum.Text); 
        gvSerial.DataSource = dtSource;
        gvSerial.PageSize = SystemConfig.PageSize;
        gvSerial.RecordCount = dtSource.Rows.Count;
        gvSerial.DataBind();
    }

    /// <summary>
    /// 绑定数据
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvSerial_OnLoadData(object sender, EventArgs e)
    {
        BindData();
    }

    /// <summary>
    /// 数据列创建事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvSerial_RowCreated(object sender, GridViewRowEventArgs e)
    {
        switch (e.Row.RowType)
        {
            case DataControlRowType.Header:
                break;
            case DataControlRowType.DataRow:
                {
                    DataRowView dtr = (DataRowView)e.Row.DataItem;
                    if (dtr != null)
                    {
                        HyperLink hlDelete = e.Row.FindControl("hlDelete") as HyperLink;
                        HyperLink hlDetail = e.Row.FindControl("hlDetail") as HyperLink;
                        System.Web.UI.WebControls.Image imgYxj = e.Row.FindControl("imgYxj") as System.Web.UI.WebControls.Image;

                        string strImgeSrc = "";
                        string strYxj = dtr["优先级"].ToString();
                        switch (strYxj)
                        {
                            case "0":
                                strImgeSrc = "../images/top_ico_yxj1.gif";
                                break;
                            case "1":
                                strImgeSrc = "../images/top_ico_yxj2.gif";
                                break;
                            default:
                                strImgeSrc = "../images/top_ico_yxj3.gif";
                                break;
                        }
                        imgYxj.ImageUrl = strImgeSrc;

                        if (null != hlDelete)
                        {
                            hlDelete.NavigateUrl = string.Format("deleteallattachment.aspx?iid={0}", dtr["编号"]);
                        }
                        if (null != hlDetail)
                        {
                            hlDetail.NavigateUrl = string.Format("DetailAttachment.aspx?iid={0}", dtr["编号"]);
                        }
                    }
                    break;
                }
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        BindData();
    }

}
