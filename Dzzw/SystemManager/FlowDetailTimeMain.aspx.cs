﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class SystemManager_FlowDetailTimeMain : System.Web.UI.Page
{
    protected string id = string.Empty;//业务流水号
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request["FlowID"] != null)
        {
            id = Request["FlowID"];
        }
        if (!IsPostBack)
        {
            //根据输入的业务ID,查找步骤信息
            System.Data.DataTable rs = Business.Admin.ClsWorkDaySet.SearchWorkItemInfo(id);

            for (int cursor = 0; cursor < rs.Rows.Count; cursor++)
            {
                string controlName = rs.Rows[cursor][4].ToString();
                string aTime = Request.Form["a" + controlName];//接收时间
                string sTime = Request.Form["s" + controlName];//提交时间

                if (!string.IsNullOrEmpty(aTime))
                {
                    Business.FlowOperation.ClsUserWorkFlow clsWorkFlow = new Business.FlowOperation.ClsUserWorkFlow();
                    clsWorkFlow.UpdateStepTime(controlName, aTime, sTime);
                }
            }

            System.Text.StringBuilder html = new System.Text.StringBuilder();
            html.Append("<td>");
            string step = string.Empty;
            string username = string.Empty;
            string AccTime = string.Empty;
            string SubTime = string.Empty;
            string keyID = string.Empty;
            for (int i = 0; i < rs.Rows.Count; i++)
            {
                step = rs.Rows[i][0].ToString();
                username = rs.Rows[i][1].ToString();
                AccTime = rs.Rows[i][2].ToString();
                SubTime = rs.Rows[i][3].ToString();
                keyID = rs.Rows[i][4].ToString();

                html.Append("<tr bgcolor=\"#ffffff\" onmouseover=\"this.style.backgroundColor='#caf7fd'\" onMouseOut=\"this.style.backgroundColor='#ffffff'\">" + "\r\n");

                html.Append("<td align=\"center\">" + step + "</td>" + "\r\n");
                html.Append("<td align=\"center\">" + username + "</td>" + "\r\n");
                html.Append("<td align=\"center\"><input type=\"text\" name=\"a" + keyID + "\" value=\"" + AccTime + "\"/></td>" + "\r\n");
                html.Append("<td align=\"center\"><input type=\"text\" name=\"s" + keyID + "\" value=\"" + SubTime + "\"/></td>" + "\r\n");
                html.Append("</tr>" + "\r\n");
            }
            html.Append("</td>");
            this.context.InnerHtml = html.ToString();
        }
    }
}
