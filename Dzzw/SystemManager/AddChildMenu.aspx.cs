﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Business.Menu;
public partial class SystemManager_AddChildMenu : System.Web.UI.Page
{
    #region 窗体加载
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            XtMenuOperation _parentMenu = new XtMenuOperation();
            DataTable dtparentmenu = _parentMenu.GenerateParentMenu();

            if (dtparentmenu.Rows.Count > 0)
            {
                parentmenu.Items.Add("请选择...");
                for (int i = 0; i < dtparentmenu.Rows.Count; i++)
                {
                    parentmenu.Items.Add(new ListItem(dtparentmenu.Rows[i]["menu_parent_desc"].ToString(), dtparentmenu.Rows[i]["menu_parent_id"].ToString()));
                }
            }
        }
    }
    #endregion

    #region 添加子菜单
    protected void inputdata_Click(object sender, EventArgs e)
    {
        if (menuname.Text == "" || picture.Text == "" || menuUrl.Text == "")
        {
            msgAppear.Text = "请输入菜单的完整信息";
        }
        else if (parentmenu.Text == "请选择...")
        {
            msgAppear.Text = "请选择所属的主菜单";
        }
        else
        {
            XtMenuOperation Menu = new XtMenuOperation();
            Menu.AddChildMenu(parentmenu.Text, menuname.Text, menuUrl.Text, picture.Text);
            msgAppear.Text = "新菜单插入完成";
        }

    }
    #endregion

    #region 重新输入
    protected void editdata_Click(object sender, EventArgs e)
    {
        menuname.Text = "";
        picture.Text = "";
        menuUrl.Text = "";

    }
    #endregion

    #region 返回菜单管理主界面
    protected void Button1_Click(object sender, EventArgs e)
    {
        Response.Redirect("MenuManage.aspx");
    }
    #endregion

}
