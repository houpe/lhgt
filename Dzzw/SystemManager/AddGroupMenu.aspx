﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddGroupMenu.aspx.cs" Inherits="SystemManager_AddGroupMenu" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>添加编辑菜单组</title>
    <link rel="stylesheet" type="text/css" href="../css/page.css" />
</head>
<body>
    <form id="Form1" name="queryForm" action="" runat="server">
    <div style="margin: 0px auto; text-align: center; width: 80%">
        <div style="margin-left: auto; margin-right: auto;">
            <controlDefine:MessageBox ID="msgAppear" runat="server" />
        </div>
        <div class="box_top_bar" style="width:100%">
            <div class="box_top_left">
                <div class="box_top_right">
                    <div class="box_top_text"  style="width:100%">
                        添加菜单组</div>
                </div>
            </div>
        </div>
        <div class="box_middle">
            <table width="70%" style="margin-left: auto; margin-right: auto;" border="1" cellpadding="0" cellspacing="0" class="box_middle_tab">
                <tr>
                    <td style="width: 374px; height: 33px;">
                        菜单组名称：
                    </td>
                    <td style="width: 270px; height: 33px;">
                        <asp:TextBox MaxLength="100" size="23" ID="menuname" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="width: 374px">
                        菜单组背景图片：
                    </td>
                    <td style="width: 270px">
                        <asp:TextBox MaxLength="100" size="23" ID="picture" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="height: 40px">
                        <asp:Button ID="inputdata" class="NewButton" Text="保存" runat="server" OnClick="inputdata_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Button ID="editdata" Text="重写" class="NewButton" runat="server" OnClick="editdata_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Button ID="Button1" Text="返回" class="NewButton" runat="server" OnClick="Button1_Click" />
                    </td>
                </tr>
            </table>
            <asp:HiddenField ID="parentid" runat="server" />
        </div>
    </div>
    </form>
</body>
</html>
