﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Business;
using WF_Business;

public partial class SystemSetting_EditDelegateRight : System.Web.UI.Page
{
    private string strID = string.Empty;
    public string ProcessName = String.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        if ( ! this.Page.IsPostBack)
        {           
            
            this.BindUnit();         
            this.LoadData();
        }
    }


    private void BindUnit()
    {
        Business.Admin.DepartHandle dep = new Business.Admin.DepartHandle();
        DataTable dt = dep.GetDepartment();
        ddlUnit.DataSource = dt;
        ddlUnit.DataTextField = "depart_name";
        ddlUnit.DataValueField = "departid";
        ddlUnit.DataBind();
        BindDep(ddlUnit.SelectedValue);
    }
    /// <summary>
    /// 绑定部门数据
    /// </summary>
    private void BindDep(string unitId)
    {
        Business.Admin.DepartHandle dep = new Business.Admin.DepartHandle();
        DataTable dt = dep.GetDepartment(unitId);
        ddlDep.DataSource = dt;
        ddlDep.DataTextField = "depart_name";
        ddlDep.DataValueField = "departid";
        ddlDep.DataBind();
        BindUser(ddlDep.SelectedValue);
    }
    private void BindUser(string depId)
    {
        string sql = "select b.userid,b.user_name from st_user_department a,st_user b "
                + "Where a.userid=b.userid And a.order_id='" + depId + "'";
        DataTable dt;
        SysParams.OAConnection().RunSql(sql, out dt);
        ddlUserName.DataSource = dt;
        ddlUserName.DataTextField = "user_name";
        ddlUserName.DataValueField = "userid";
        ddlUserName.DataBind();
    }
    protected void ddlUnit_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindDep(ddlUnit.SelectedValue);
    }
    protected void ddlDep_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindUser(ddlDep.SelectedValue);
    }
     

    #region 数据加载功能
    private void LoadData()
    {       
        string sql = String.Empty;
        if (this.Request.Params["ID"] != null)
        {
            strID = this.Request.Params["ID"].Trim();
            sql = String.Format("select d.delegate_userid,d.delegate_task,d.delegate_time,d.begin_time,d.end_time from st_delegate d where d.id='{0}'", strID);
            
            DataTable dt;
            SysParams.OAConnection().RunSql(sql, out dt);
            if (dt.Rows.Count > 0)
            {
                this.ProcessName = dt.Rows[0]["delegate_task"].ToString();
                this.ddlUserName.SelectedValue = dt.Rows[0]["delegate_userid"].ToString();
                this.startDate.Text = dt.Rows[0]["begin_time"].ToString();
                this.endDate.Text = dt.Rows[0]["end_time"].ToString();

                //选定顶级部门
                sql = String.Format("select d.departid from st_department d where d.departid={0}", this.ddlUserName.SelectedValue);


                //人员的上级部门
                sql = String.Format("select ud.order_id from st_user_department ud where ud.userid='{0}' ", dt.Rows[0]["delegate_userid"].ToString());
                SysParams.OAConnection().RunSql(sql, out dt);
                if (dt.Rows.Count > 0)
                {
                    this.ddlDep.SelectedValue = dt.Rows[0]["order_id"].ToString();
                }           

                //顶级部门--首先选中顶级部门
                sql = String.Format("select d.departid from st_department d where d.departid={0}", this.ddlDep.SelectedValue);
                SysParams.OAConnection().RunSql(sql, out dt);
                if (dt.Rows.Count > 0)
                {
                    this.ddlUnit.SelectedValue = dt.Rows[0]["departid"].ToString();
                }
            }
        }
    }
    #endregion

    #region 确定操作
    protected void btnUpdate_Click(object sender, EventArgs e)
    {

    }
    #endregion

    #region 取消    
    protected void btnCancel_Click(object sender, EventArgs e)
    {

    }
    #endregion
   
}
