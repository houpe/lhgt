using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using Business.Admin;
using Business.FlowOperation;
using Business;

/// <summary>
/// Summary description for priority_main.
/// </summary>
public partial class SystemManager_PriorityMain : PageBase
{
    protected void Page_Load(object sender, System.EventArgs e)
    {
        if (!IsPostBack)
        {
            BindData(SetPriority());
        }
    }

    # region 绑定优先级
    /// <summary>
    /// 绑定优先级信息
    /// </summary>
    public void BindData(string ywlx)
    {
        SystemManager sysMage = new SystemManager();
        DataTable dtTemp = sysMage.GetPriority(ywlx, Request["sqdw"], Request["startDate"], Request["endDate"]);
        CustomGridView1.DataSource = dtTemp;
        CustomGridView1.PageSize = SystemConfig.PageSize;
        CustomGridView1.RecordCount = dtTemp.Rows.Count;
        CustomGridView1.DataBind();
        sqdw.Value = Request["sqdw"];
    }

    protected void CustomGridView1_OnLoadData(object sender, EventArgs e)
    {
        BindData(ywlx.Value);
    }

    /// <summary>
    /// 行创建
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void CustomGridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HyperLink hlTemp = e.Row.FindControl("hlDelete") as HyperLink;
            if (hlTemp != null)
            {
                DataRowView dtr = (DataRowView)e.Row.DataItem;

                if (dtr != null)
                {
                    hlTemp.Attributes.Add("onclick", "return confirm('您确定要删除吗？')");//为删除添加提示
                    hlTemp.NavigateUrl = string.Format("submit.aspx?flag={1}&iid={0}&action=delete",
                        dtr["iid"], Request["flag"]);
                }
            }
        }
    }
    #endregion

    #region 设置优先级
    /// <summary>
    /// 设置优先级
    /// </summary>
    public string SetPriority()
    {
        string strStartDate = Request["startDate"];//开始时间	
        string strEndDate = Request["endDate"];//结束时间	
        string strSqdw = Request["sqdw"];//申请单位
        string strPageNow = Request["PageNo"];//第几页	

        int nPageNow = 1;
        if (!string.IsNullOrEmpty(strPageNow))
        {
            nPageNow = int.Parse(strPageNow);
        }

        string strYwNo = Request["ywno"];//内部业务编号	
        string strYwStatus = Request["status"];//业务状态
        string strSql = string.Empty;

        ClsUserWorkFlow clsWorkFlow = clsWorkFlow = new ClsUserWorkFlow();

        if (!string.IsNullOrEmpty(strYwNo) && !string.IsNullOrEmpty(strYwStatus))
        {
            clsWorkFlow.UpdateInstancePriority(strYwNo, strYwStatus);
        }

        DataTable dtSource = clsWorkFlow.GetFlowName();

        ListItem liFirst = new ListItem();

        liFirst.Value = "";
        liFirst.Text = "所有业务";
        ywlx.Items.Add(liFirst);
        foreach (System.Data.DataRow drTemp in dtSource.Rows)
        {
            ListItem liTemp = new ListItem();
            liTemp.Value = drTemp["wname"].ToString();
            liTemp.Text = drTemp["wname"].ToString();
            ywlx.Items.Add(liTemp);
        }

        ywlx.Value = Request["ywlx"];
        string strYwlx = ywlx.Value;//业务类型  

        return strYwlx;
    }
    #endregion

    #region 页面优先级内容绑定
    /// <summary>
    /// 优先级内容绑定
    /// </summary>
    /// <param name="row"></param>
    /// <returns></returns>
    public string strPriority(IDataItemContainer row)
    {
        StringBuilder html = new StringBuilder();
        string strIID = DataBinder.Eval(row.DataItem, "IID").ToString();
        string strPri = DataBinder.Eval(row.DataItem, "priority").ToString();

        html.Append(string.Format("<select name='status{0}' onchange=\"tosubmit('{0}',this,'')\">", strIID));
        if (strPri.Equals("0"))
        {
            html.Append("<option value=0 selected STYLE=\"color:black\">普通</option> ");
            html.Append("<option value=1 STYLE=\"color:blue\">加急</option> ");
            html.Append("<option value=2 STYLE=\"color:red\">特急</option> ");
        }
        else if (strPri.Equals("1"))
        {
            html.Append("<option value=0 STYLE=\"color:black\">普通</option> ");
            html.Append("<option value=1 STYLE=\"color:blue\" selected>加急</option> ");
            html.Append("<option value=2 STYLE=\"color:red\">特急</option> ");
        }
        else if (strPri.Equals("2"))
        {
            html.Append("<option value=0 selected STYLE=\"color:black\">普通</option> ");
            html.Append("<option value=1 STYLE=\"color:blue\">加急</option> ");
            html.Append("<option value=2 STYLE=\"color:red\" selected>特急</option> ");
        }
        else
        {
            html.Append("<option value=0 selected STYLE=\"color:black\">普通</option> ");
            html.Append("<option value=1 STYLE=\"color:blue\">加急</option> ");
            html.Append("<option value=2 STYLE=\"color:red\">特急</option> ");
        }
        html.Append("</select>");
        return html.ToString();
    }
    #endregion
}
