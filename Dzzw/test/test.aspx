﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="test.aspx.cs" Inherits="test_test" %>

<%@ Register Src="../UserControls/JqueryDataGrid.ascx" TagName="JqueryDataGrid" TagPrefix="uc1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>测试jquery grid</title>

    <link href="../CSS/page.css" rel="stylesheet" type="text/css" />
    <link href="../css/icon.css" rel="stylesheet" type="text/css" />
    <link type="text/css" href="../css/default/easyui.css" rel="stylesheet" />
    <script src="../ScriptFile/jquery.js" type="text/javascript"></script>
    <script src="../ScriptFile/jquery.easyui.min.js" type="text/javascript"></script>
</head>
<body onload="ShowTable()">
    <form id="form1" runat="server">
        <div>

            <%--<uc1:JqueryDataGrid ID="JqueryDataGrid1" DgTitle="test" PageSize="15" TableName="st_user" AppearColumnNames="login_name,user_name,password" QueryWhere=" login_name like 'Z%'" runat="server" />--%>

            <uc1:JqueryDataGrid ID="JqueryDataGrid1" DgTitle="test" PageSize="10" PostSql="select * from st_user"  runat="server" AddFormatterColumnNames="USERID" frozenColumnsName="field:'USERID',title:'USERID'" AddFormatterContent="<a href=test.aspx?id='+value+'>'+value+'</a>" />

        </div>
    </form>
</body>
</html>
