﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Notice.aspx.cs" Inherits="ManageInside_Notice" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>通知</title>
    <link href="../css/page.css" type="text/css" rel="Stylesheet" />
    <link href="../css/other.css" rel="Stylesheet" type="text/css" />
    <script type="text/javascript">
    function tNotice()
    {
        var id="<%=id %>"
        window.open("detail.aspx?id="+id);
        window.close();
    }
    function tClose()
    {
        self.close();
    }
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div style="text-align: center; overflow: auto; height: 350px">
            <table style="width: 97%;" class="tab" id="tNotice">
                <tr>
                    <th>
                        <a href="javascript:void(0)" onclick="tNotice();">
                            <%=title %>
                        </a>
                    </th>
                </tr>
                <tr>
                    <td>
                        <%=content %>
                    </td>
                </tr>
                <tr>
                    <td>
                     <a class="font1" href="#" onclick="self.close();">[关闭窗口]</a></td>                   
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
