﻿<%@ Page Language="c#" Inherits="ManageInside_More" CodeFile="More.aspx.cs" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>更多信息</title>
    <link href="../css/page.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .headstyle
        {
            background-image: url(../images/box_top_fill.gif);
            background-repeat: repeat-x;
        }
    </style>
</head>

<script type="text/javascript">
    //添加
    function OpenNewAdd()
    {
        var type="<%=ViewState["type"]%>";
        if(type!="")
        {
            window.location.href="WebPubManager.aspx?type="+type;
        }
        else
        {
            return;
        }
    }
    //父窗口的打开事件
    function OpenParentDialog(title,url)
    {
        window.parent.openDivDlg(title,url);
    }
    
</script>

<body>
    <form id="Form1" runat="server">
    <div style="text-align: center">
        <%-- <controlDefine:CustomGridView runat="server" ID="DataGrid2" SkinID="List" AutoGenerateColumns="false"
            OnOnLoadData="gvSerial_OnLoadData" ShowHideColModel="None" AllowPaging="true"
            CssClass="tab" OnRowCreated="gvSerial_RowCreated" OnRowCommand="DataGrid2_ItemCommand">
            <Columns>
                <asp:BoundField DataField="ID" Visible="False"></asp:BoundField>
                <asp:BoundField DataField="rownum" HeaderText="序号"></asp:BoundField>
                <asp:BoundField DataField="TYPE" HeaderText="所属名称"></asp:BoundField>
                <asp:TemplateField HeaderText="标题">
                    <itemtemplate>
                                    <a href="detail.aspx?id=<%#Eval("id") %>" target="_blank">
                                        <%#Eval("TITLE")%></a>
                                </itemtemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="user_name" HeaderText="发布人"></asp:BoundField>
                <asp:BoundField DataField="TIME" HeaderText="发布时间"></asp:BoundField>
                <asp:TemplateField>
                    <itemtemplate>
                                    <asp:HyperLink ID="hlmodify" runat="server" Visible="false">修改</asp:HyperLink>
                                    <asp:HyperLink ID="hlDel" runat="server" Visible="false">删除</asp:HyperLink>
                                    <asp:HyperLink ID="Viewstatus" runat="server" Visible="false">收阅情况</asp:HyperLink>
                                </itemtemplate>
                </asp:TemplateField>
            </Columns>
        </controlDefine:CustomGridView>--%>
        <table style="width: 100%">
            <tr>
                <td>
                    <asp:DataGrid ID="DataGrid2" runat="server" BorderColor="#66FF66" BorderStyle="Solid"
                        BorderWidth="1px" BackColor="White" CellPadding="4" GridLines="Horizontal" AutoGenerateColumns="False"
                        Width="100%" PageSize="12" AllowPaging="True" Font-Bold="False" Font-Italic="False"
                        CssClass="tab" Font-Overline="False" Font-Size="Smaller" Font-Strikeout="False"
                        Font-Underline="False" HorizontalAlign="Left" OnItemCommand="DataGrid2_ItemCommand"
                        OnItemDataBound="DataGrid2_ItemDataBound">
                        <SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#339966"></SelectedItemStyle>
                        <HeaderStyle CssClass="headstyle" />
                        <FooterStyle ForeColor="#333333" BackColor="White"></FooterStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ID" Visible="False"></asp:BoundColumn>
                            <asp:BoundColumn DataField="rownum" HeaderText="序号"></asp:BoundColumn>
                            <asp:BoundColumn DataField="TYPE" HeaderText="所属名称"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="标题">
                                <ItemTemplate>
                                    <a href="javascript:OpenParentDialog('信息查看','ManageInside/detail.aspx?id=<%#Eval("id") %>')" >
                                        <%#Eval("TITLE")%></a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="user_name" HeaderText="发布人"></asp:BoundColumn>
                            <asp:BoundColumn DataField="TIME" HeaderText="发布时间"></asp:BoundColumn>
                            <asp:TemplateColumn>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hlmodify" runat="server" Visible="false">修改</asp:HyperLink>
                                    <asp:HyperLink ID="hlDel" runat="server" Visible="false">删除</asp:HyperLink>
                                    <asp:HyperLink ID="Viewstatus" runat="server" Visible="false">收阅情况</asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle HorizontalAlign="Right" ForeColor="Black" BackColor="#E6F7FC" Mode="NumericPages"
                            Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                            Font-Underline="False"></PagerStyle>
                        <AlternatingItemStyle BackColor="#E6F7FC" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                            Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" />
                    </asp:DataGrid>
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <input type="button" id="add" value="新增" onclick="OpenNewAdd();" class="NewButton" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
