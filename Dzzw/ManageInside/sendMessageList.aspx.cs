﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using Business.Common;
using Business;


public partial class ManageInside_sendMessageList : PageBase
{
    Message msg = new Message();
    public StringBuilder strbuilderHTML = new StringBuilder();
    public StringBuilder strPageHTML = new StringBuilder();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string type = Request["type"];
            string id = Request["id"];
            if (type == "delete")
            {
                msg.DeleteMessage(id);
            }
            GetData();
            CreatHTML();
        }
    }

    #region BODY
    private void CreatHTML()
    {
        string strStart = calStart.Value;
        string strEnd = calEnd.Value;
        string title = txtTitle.Text.Trim();
        DataTable dt = msg.GetSendMessage(UserName, strStart, strEnd, title);

        int pagesize = 15;//页面记录数
        int pagenow = 1;//当前页
        int pagecount = 0;//总页数
        string mpage = Request["pagenow"];

        if (!string.IsNullOrEmpty(mpage))
        {
            pagenow = System.Int32.Parse(mpage); ;
            if (pagenow < 1)
            {
                pagenow = 1;
            }
        }

        PageOperation mypage = new PageOperation(dt, pagenow, pagesize);
        pagecount = mypage.PageCount;
        List<DataRow> listwork = mypage.CurrentPageList;

        int nCount = listwork.Count;
        if (nCount > 0)
        {
            for (int i = 0; i < nCount; i++)
            {
                DataRow myArray = listwork[i] as DataRow;
               
                string delete = string.Format("<a href=\"sendMessageList.aspx?id={0}&type=delete&pagenow={1}\" onclick='return SureDelete();'>删除</a>", myArray[0], pagenow);
                string viewMessage = string.Format("<a href='#' onclick=\"ViewMessage('{0}')\">", myArray[0].ToString());

                string[] sShowFiled ={ myArray[6].ToString(), myArray[5].ToString(), myArray[3].ToString(), delete, viewMessage };

                strbuilderHTML.AppendFormat(@"<tr><td style='text-align:center'>{4}{0}</a></td> <td>{1}</td> <td>{2}</td> <td>{3}</td> ", sShowFiled);

            }
        }
        else
        {
            strbuilderHTML.Append("<tr><td colspan='9'>对不起，没有找到相关数据！</td></tr>");
        }

        strPageHTML = PageOperation.CreatPageChanges(mypage, pagenow, pagecount, pagesize);
    }
    #endregion

    #region 查询
    protected void btnQuery_Click(object sender, EventArgs e)
    {        
        Session["start"] = calStart.Value;
        Session["end"] = calEnd.Value;
        Session["title"] = txtTitle.Text;        
        CreatHTML();
    }
    #endregion

    #region GetData
    private void GetData()
    {
        if (Session["start"] != null && !string.IsNullOrEmpty(Session["start"].ToString()))
        {
            calStart.Value = Session["start"].ToString();
        }
        if (Session["end"] != null && !string.IsNullOrEmpty(Session["end"].ToString()))
        {
            calEnd.Value = Session["end"].ToString();
        }
        if (Session["title"] != null && !string.IsNullOrEmpty(Session["title"].ToString()))
        {
            txtTitle.Text = Session["title"].ToString();
        }
    }
    #endregion
}
