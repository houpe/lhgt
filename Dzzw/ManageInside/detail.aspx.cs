﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Business.Common;
using WF_Business;

public partial class ManageInside_detail : PageBase
{
    public string txt, title, time, fujian, strPublier;
    public int loadcount;
    private void Page_Load(object sender, System.EventArgs e)
    {
        if (!this.Page.IsPostBack)
        {
            if (Request.QueryString["id"] != null && loadcount == 0)
            {
                txt = GetTxt(Request.QueryString["id"]);
                UpdateReadFlag();
                loadcount++;
            }
        }
    }

    private string GetTxt(string id)
    {
        string temp = "";
        try
        {
            DataTable dt;
            string sql = string.Format("select title,content,time,(select user_name from st_user b where a.userid=b.userid) user_name from xt_other a  where a.id='{0}'",id);
            SysParams.OAConnection().RunSql(sql, out dt);
            if (dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    temp = dr["content"].ToString();
                    title = dr["title"].ToString();
                    time = dr["time"].ToString();
                    strPublier = dr["user_name"].ToString();
                }
            }
            sql = "select filename from xt_uploadfile where archivesno='{0}' and flag='2'";
            sql = string.Format(sql, id);
            dt = null;
            SysParams.OAConnection().RunSql(sql, out dt);
            if (dt != null)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    fujian += "<a href='" + dr[0].ToString() + "' target='_blank'>" + dr[0].ToString() + "</a><br>";
                }
            }

        }
        catch (Exception ex)
        {
            throw new Exception("发生异常" + ex.Message);
        }
        return temp;
    }

    //更新阅读标志位
    private void UpdateReadFlag()
    {
        Notice notice = new Notice();
        notice.UpdateNotiyReadFlag(RealUserId, Request["id"]);
    }

    protected string GetTitleHtml()
    {
        string html = "";
        if (!string.IsNullOrEmpty(title))
        {
            if (title.Length <= 31)
            {
                html = "<div style=\" width:838px; height:98px; \">  <div style=\" width:838px; height:30px; font-size:25px; font-weight:bold; padding-top:15px; text-align:center;\"> " + title + "</div> <div style=\" width:808px; height:28px; font-size:12px; text-align:right; padding-top:20px; padding-right:30px;\">时间：" + time + "  发布人：" + strPublier + " </div> </div>";
            }
            else if (title.Length > 31)
            {
                html = "<div style=\" width:838px; height:100px; \">  <div style=\" width:838px; height:30px;font-size:25px; font-weight:bold; padding-top:15px; text-align:center;\"> " + title.Substring(0, 31) + "</div> <div style=\" width:838px; height:30px; font-size:25px; font-weight:bold; padding-top:15px; text-align:center;\"> " + title.Substring(31) + "</div></div><div style=\" width:808px; height:28px; font-size:12px; text-align:right; padding-top:20px; padding-right:30px;\">时间：" + time + "  发布人：" + strPublier + " </div>";
            }
        }

        return html;
    }
}
