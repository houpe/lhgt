﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Business.Common;

public partial class ManageInside_sendMessage : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            psUser.Name = "所有用户";
        }
    }

    protected void btnSend_Click(object sender, EventArgs e)
    {
        string strTitle = txtTitle.Text.Trim();
        string strContents = txtContents.Text.Trim();

        if (String.IsNullOrEmpty(strContents))
        {
            Common.WindowAppear.WriteAlert(this.Page, "消息内容不能为空");
            return;
        }

        Message msg = new Message();
        int i = ckbMessage.Checked ? 1 : 0;

        msg.InsertInfoMessage(UserId, psUser.Value, strContents, strTitle, i);
        Page.ClientScript.RegisterStartupScript(this.GetType(), "sendok", "<script>sendOK();</script>");
    }
}
