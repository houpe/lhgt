﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Business.Common;
using Business.FlowOperation;
using WF_Business;

/// <summary>
/// More 的摘要说明。
/// </summary>
public partial class ManageInside_More : PageBase
{
    protected void Page_Load(object sender, System.EventArgs e)
    {
        if (!this.Page.IsPostBack)
        {
            string action = Request["action"];
            ViewState["type"] = Request["type"];

            if (!string.IsNullOrEmpty(RealUserId))
            {
                BindDG();
            }
            if (action == "del")
            {
                string id = Request["id"];
                DataDelete(id);
            }
        }
    }

    /// <summary>
    /// 绑定
    /// </summary>
    private void BindDG()
    {
        string strSql = string.Format(@"select rownum,p.* from (select distinct t.id as id,t.show_no,t.title as title,t.time as time,
                    t.type as type,t.userid,readflag,u.user_name from xt_other t,XT_NOTIFYUSER t1,st_user u where u.userid=t.userid 
                    and t.id=t1.sid and t1.LOGIN_NAME='{0}' and t.type='{1}' order by readflag asc,t.time desc ) p", 
                    RealUserId, ViewState["type"]);
        DataTable ds ;
        SysParams.OAConnection().RunSql(strSql, out ds);
        if (ds != null)
        {
            this.DataGrid2.DataSource = ds;
            this.DataGrid2.DataBind();
        }
    }

    /// <summary>
    /// 分页
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    private void DataGrid2_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
    {
        if (this.DataGrid2.Items.Count == 1 && this.DataGrid2.CurrentPageIndex > 0)
        {
            this.DataGrid2.CurrentPageIndex--;
        }
        this.DataGrid2.CurrentPageIndex = e.NewPageIndex;
        BindDG();
    }

    /// <summary>
    /// 行命令
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void DataGrid2_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.Item.ItemIndex != -1 && e.Item.ItemIndex >= 0)
        {
            if (e.CommandName == "Delete")
            {
                string id = DataGrid2.Items[e.Item.ItemIndex].Cells[0].Text;
                DataDelete(id);
            }
        }
    }
    protected void DataDelete(string id)
    {
        if (!string.IsNullOrEmpty(id))
        {
            InfoManage aM = new InfoManage();
            aM.DeleteArticle(id);

            BindDG();
        }
        else
        {
            Response.Write("<script>alert('删除失败');</script>");
        }
    }

    /// <summary>
    /// 行绑定
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void DataGrid2_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemIndex != -1 && e.Item.ItemIndex >= 0)
        {
            string strUrl = string.Empty;

            string s = e.Item.ToString();
            DataRowView dRowView = e.Item.DataItem as DataRowView;
            String id = dRowView["ID"].ToString();

            HyperLink hylkDel = e.Item.FindControl("hldel") as HyperLink;
            hylkDel.NavigateUrl = "More.aspx?id=" + id + "&action=del&type=" + ViewState["type"].ToString();
            hylkDel.Visible = true;
            HyperLink hylkmodify = e.Item.FindControl("hlmodify") as HyperLink;

            hylkmodify.NavigateUrl = "webpubmanager.aspx?id=" + id + "&action=modify&type=" + ViewState["type"].ToString();
            hylkmodify.Visible = true;

            HyperLink Viewstatus = e.Item.FindControl("Viewstatus") as HyperLink;
            Viewstatus.NavigateUrl = "sendlisthistory.aspx?id=" + id;
            Viewstatus.Visible = true;

            if (dRowView["readflag"].ToString() == "1")
            {
                e.Item.BackColor = System.Drawing.Color.PaleGreen;
            }
        }
    }
}