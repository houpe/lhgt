﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="detail.aspx.cs" Inherits="ManageInside_detail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>信息查看</title>
    <style type="text/css">
        .fj
        {
            font-size: 12px;
        }
        .fj a
        {
            text-decoration: none;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div style="width: 840px; height: auto; margin: 30px auto 0px auto;">
        <%=GetTitleHtml() %>
        <div style="width: 760px; height: auto; padding-left: 40px; padding-right: 40px;
            background-image: url(images/info.jpg);">
            <div style="width: 760px; height: auto;">
                <%=txt%>
            </div>
            <div class="fj" style="width: 755px; height: auto; background-color: #D9F29E; margin-top: 20px;
                padding-top: 2px; padding-left: 5px;">
                <%
                    if (!string.IsNullOrEmpty(fujian))
                    {
                        Response.Write("附件：<br>" + fujian);
                    }
                %>
            </div>
        </div>
        <div style="width: 840px; height: 30px; text-align: center; line-height: 30px;">
            <a class="font1" href="#" onclick="window.parent.CloseDivDlg();">[关闭窗口]</a></div>
    </div>
    </form>
</body>
</html>
