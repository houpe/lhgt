﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using Business.Admin;

public partial class ManageInside_ResponesValue : System.Web.UI.Page
{
    public StringBuilder sbUnitNameHtml = new StringBuilder();
    public StringBuilder sbUserNameHtml = new StringBuilder();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string action = Request["action"];
            string departMent = Request["dpart"];

            if (action == "")
            {
                GetUnitName();
            }
            else
            {
                GetUserName(departMent);
            }
        }
    }

    //获取部门名称
    private void GetUnitName()
    {
        DepartHandle dep = new DepartHandle();
        DataTable dt = dep.GetDepartmentByParentId("35");
        sbUserNameHtml = null;
        sbUnitNameHtml.Append("<select name='Department1'  id='Department1' onChange='initUser(this);'  width='136px' height='100%'>");
        if (dt.Rows.Count > 0)
        {           
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string value = dt.Rows[i]["departid"].ToString().Trim();
                string vName = dt.Rows[i]["depart_name"].ToString().Trim();
                sbUnitNameHtml.AppendFormat("<option value=\"{0}\">{1}</option>", value, vName);
            }
        }
        sbUnitNameHtml.Append("</select>");
        Response.Write(sbUnitNameHtml.ToString());
    }

    //获取人员名称
    private void GetUserName(String deparementid)
    {
        DepartHandle dep = new DepartHandle();
        DataTable dt = null;
        dt = dep.GetUserList(deparementid);
        sbUserNameHtml.Append("<select name='userlist' id='userlist' multiple='multiple' ondblclick='chance()' style='width:136px; height:360px' >");
        if (dt.Rows.Count > 0)
        {           
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string value = dt.Rows[i]["login_name"].ToString();
                string vName = dt.Rows[i]["user_name"].ToString();
                sbUserNameHtml.AppendFormat("<option value=\"{0}\">{1}</option>", value, vName);
            }
        }
        sbUserNameHtml.Append("</select>");
        Response.Write(sbUserNameHtml.ToString());
    }
   
}
