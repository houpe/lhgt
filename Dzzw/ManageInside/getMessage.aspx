﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="getMessage.aspx.cs" Inherits="ManageInside_getMessage" %>

<%@ Register Src="../UserControls/Calendar.ascx" TagName="Calendar" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>接收即时消息</title>
    <link href="../css/page.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        function tosubmit(page)
		{
			var strUrl="getMessage.aspx";			
			if(page==""|| page==null)
			{
				strUrl+="?pagenow="+document.all.jumpPage.value;
			}
			else	
			{
				strUrl+="?pagenow="+page;
			}			
			window.location.href=strUrl;
		}
		function ViewMessage(id)
		{
		    var url = "ViewMessage.aspx?id="+id;
		    window.open(url,"view","top=200,left=200,toolbar=no,menubar=no,scrollbars=yes,location=no,status=no,width=500,height=300;");
		    window.location.href = window.location.href;
		}   
    
        function SureDelete()
        {   
            var sure = window.confirm("确定删除?");
            if(sure)
            {   
                return true;
            }
            else
            {   
                return false;
            }
            
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div style="text-align: center">
            <br />
            <table style="width: 100%;" cellpadding="0px" cellspacing="0" class="tabTop">
                <tr style="height:35px">
                    <td style="text-align: left;">
                        &nbsp;&nbsp;发送日期：
                        <uc2:Calendar ID="calStart" runat="server" Width="120px" />
                        至
                        <uc2:Calendar ID="calEnd" runat="server" Width="120px" />
                        标题
                        <asp:TextBox ID="txtTitle" runat="server"></asp:TextBox>
                        <asp:Button ID="btnQuery" runat="server" Text="查询" CssClass="NewButton" OnClick="btnQuery_Click" />
                    </td>
                </tr>
            </table>
            <br />
            <table style="width: 100%" class="tab">
                <tr>                    
                    <th style="width:30%">
                        标题
                    </th>
                    <th>
                        发送日期
                    </th>
                    <th>
                        发送人
                    </th>
                    <th>
                        操作
                    </th>
                </tr>
                <%=strbuilderHTML.ToString() %>
            </table>
            <%=strPageHTML.ToString()%>
        </div>
    </form>
</body>
</html>
