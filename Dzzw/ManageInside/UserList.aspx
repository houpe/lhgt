﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UserList.aspx.cs" Inherits="ManageInside_UserList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>用户列表</title>
    <link href="../css/page.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../ScriptFile/ajax.js"></script>

</head>
<body onload="GetD();">
    <form id="form1" runat="server">
    <div style="text-align: center">
        <table style="width: 50%">
            <tr>
                <td>
                    <div id="DepartmentList" style="top: 0px; width: 132px; height: 24px;">
                    </div>
                    <div id="userNameList" style="width: 136px; height: 364px">
                    </div>
                </td>
                <td style="width: 100px">
                    <input onclick="chance();" class="input" type="button" value=" 选择>" />
                    <br />
                    <br />
                    <input onclick="chanceAll();" class="input" type="button" value="全选>>" />
                    <br />
                    <br />
                    <input onclick="Delete();" class="input" type="button" value="<删除 " />
                    <br />
                    <br />
                    <input onclick="DeleteAll();" class="input" type="button" value="<<全删" />
                </td>
                <td>
                    <div id="Div1" style="top: 0px; width: 136px; height: 24px;">
                    </div>
                    <select id="sendlist" style="width: 136px; height: 364px;" multiple="multiple">
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="labCount" runat="server" Text=""></asp:Label>
                </td>
                <td>
                </td>
                <td>
                    <asp:Label ID="labSendCount" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <input id="btnOK" type="button" value="确定" class="NewButton" onclick="GetAllUserName();" />
                </td>
            </tr>
        </table>
        <asp:HiddenField ID="hidSendList" runat="server" />
        <asp:HiddenField ID="hidSendNameList" runat="server" />
    </div>
    </form>
</body>
</html>
