﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Business.Common;

public partial class ManageInside_SendListHistory : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Bindddl();
            DataBinder();
        }
    }

    protected void recvgrid_OnLoadData(object sender, EventArgs e)
    {
        DataBinder();
    }

    protected void recvgrid_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HyperLink getFile = e.Row.FindControl("getFile") as HyperLink;            
            DataRowView dtr = e.Row.DataItem as DataRowView;

            if (dtr != null)
            {
                getFile.Text = dtr["title"].ToString();

                if (dtr["readFlag"].ToString() == "1")
                {
                    e.Row.BackColor = System.Drawing.Color.PaleGreen;
                }
            }
        }
    }
    private void Bindddl()
    {
        GWOperation sData = new GWOperation();
        ListItem list=new ListItem("全部","0");
        ddlbm.DataSource = sData.Bindddl();
        ddlbm.DataTextField = "name"; ddlbm.DataValueField = "id";
        ddlbm.DataBind();
        ddlbm.Items.Insert(0, list);
    }
    private void DataBinder()
    {
        Notice sData = new Notice();
        string whereValue = "";        
        string sID = Request["id"];
        if (sID == "")
        {
            sID = ViewState["ID"].ToString();
        }
        else
        {
            ViewState["ID"] = sID;
        }
        if (!string.IsNullOrEmpty(sID))
        {
            whereValue += string.Format(" and a.id='{0}'", sID);
        }
        if (ddlbm.SelectedValue!="0")
        {
            whereValue += string.Format(" and ud.order_id='{0}'",ddlbm.SelectedValue);
        }
        DataTable dtTemp = sData.SendactListHistory(whereValue);
        if (dtTemp.Rows.Count > 0)
        {
            recvgrid.DataSource = dtTemp;
            recvgrid.RecordCount = dtTemp.Rows.Count;
            recvgrid.DataBind();
        }
    }
    protected void btnQuery_Click(object sender, EventArgs e)
    {
        DataBinder();
    }
    protected void ddlbm_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataBinder();
    }
}
