<%@ Page Language="c#" Inherits="ManageInside_SubUpLoad" CodeFile="SubUpload.aspx.cs" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../css/page.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../ScriptFile/ajax.js"></script>

</head>
<body onload="GetD();">
    <form id="Form1" runat="server">
    <%--<div class="box_top_bar" style="border-left: solid 1px #76D434; border-right: solid 1px #76D434;">
        <div class="box_top_text">
            文件传送</div>
    </div>--%>
    <div class="box_middle" style="width: 100%; margin: 0px auto; text-align: center;">
        <table width="90%" border="0" style="padding-left: auto; margin-left: auto;">
            <tr>
                <td style=" height:50px;" colspan="3">
                    1. 通过选择部门然后选择人员：
                    <div id="departMentList" style="width: 136px; height: 24px;">
                    </div>
                </td>
                <td>
                    2. 点“浏览”按钮, 找到您所要上传的电子文件：
                    <input id="myfile" style="width: 317px; height: 25px" type="file" size="33" name="myfile"
                        runat="server" />
                </td>
            </tr>
            <tr>
                <td rowspan="2">
                    <div id="userNameList" style="width: 136px; height: 360px">
                    </div>
                </td>
                <td style="width: 40px;" rowspan="2">
                    <input type="button" class="NewButton" value="<删除 " onclick="Delete();" /><br />
                    <br />
                    <input type="button" class="NewButton" value="<<全删" onclick="DeleteAll();" /><br />
                    <br />
                    <input type="button" class="NewButton" value="全选>>" onclick="chanceAll();" /><br />
                    <br />
                    <input type="button" class="NewButton" value=" 选择>" onclick="chance();" />
                </td>
                <td rowspan="2">
                    <select id="sendlist" style="width: 136px; height: 357px;" multiple="multiple">
                    </select>
                </td>
                <td style=" margin-top:0px; padding-top:0px; vertical-align:top;">
                    3.选择材料类型：<asp:DropDownList ID="DropDownList1" runat="server">
                        <asp:ListItem Selected="True" Value="SubData">普通材料</asp:ListItem>
                        <asp:ListItem Value="HDJH">活动计划</asp:ListItem>
                        <asp:ListItem Value="GZDT">工作动态</asp:ListItem>
                        <asp:ListItem Value="XDTH">心得体会</asp:ListItem>
                        <asp:ListItem Value="XXZL">学习资料</asp:ListItem>
                        <asp:ListItem Value="ZTDY">专题调研</asp:ListItem>
                    </asp:DropDownList>
                    <br />
                    4.添加备注：<br />
                    <asp:TextBox ID="txtNotes" runat="server" Height="72px" TextMode="MultiLine" Width="318px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Button ID="UpFileBtn" runat="server" Text="上报数据" OnClick="UpFileBtn_Click" OnClientClick="return checkNull();"
                        CssClass="longButton"></asp:Button>
                    &nbsp; &nbsp; &nbsp;&nbsp;
                    <asp:Button ID="btn_postedfile" CssClass="longButton" runat="server" Text="已上报数据"
                        OnClick="btn_postedfile_Click"></asp:Button>
                    <asp:Button ID="btn_createfile" runat="server" CssClass="longButton" Text="创建文件夹"
                        Visible="False"></asp:Button>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <asp:HiddenField ID="hidSendList" runat="server" />
                    <asp:Label ID="labmsg" runat="server" ForeColor="#FF0066"></asp:Label>
                    <asp:Label ID="labCount" runat="server" Font-Size="X-Small" ForeColor="Red" Text="" />
                    <asp:Label ID="labSendCount" runat="server" Font-Size="X-Small" ForeColor="Red" Text="" />
                    <asp:HiddenField ID="hidSendNameList" runat="server" />
                    <asp:TextBox ID="hidDepartment" runat="server" Style="display: none"></asp:TextBox>
                    <asp:TextBox ID="hidUser" runat="server" Style="display: none"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
