﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="sendMessage.aspx.cs" Inherits="ManageInside_sendMessage" %>

<%@ Register Src="../UserControls/PopupSelect.ascx" TagName="PopupSelect" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>发送即时消息</title>
    <link href="../css/page.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../css/default/easyui.css" />
    <link rel="stylesheet" type="text/css" href="../css/icon.css" />

    <script type="text/javascript" src="../ScriptFile/jquery-1.7.2.min.js"></script>

    <script type="text/javascript" src="../ScriptFile/jquery.easyui.min.js"></script>

</head>

<script type="text/javascript">
    function sendOK() {
        alert("发送成功");
        window.location.href = "sendMessageList.aspx";
    }
    function Close() {
        window.location.href = "sendMessageList.aspx"
    }

    //关闭窗口
    function CloseDivDlg() {
        $('#dlgConfig').window('close');
    }

    //打开内嵌网页的通用方法1（window）
    function openDivDlg(strTitle, strUrl) {
        //增加参数queryid
        var divWidth = 400;
        var divHeight = 480;

        $('#dlgConfig').window({
            title: strTitle, width: divWidth, height: divHeight
        });

        $('#iframeConfig').attr("src", strUrl);

        // $('#dlgConfig').window({ maximized: true });
        $('#dlgConfig').window('open');
    }
</script>

<body onload="SetDefaultValue()">
    <form id="form1" runat="server">
    <div style="text-align: center; width: 100%;">
        <br />
        <table style="width: 80%" class="tab" align="center">
            <tr>
                <th colspan="2">
                    发送即时消息
                </th>
            </tr>
            <tr>
                <td style="width: 30%; height: 30px">
                    消息标题
                </td>
                <td>
                    <asp:TextBox ID="txtTitle" runat="server" Width="98%" Height="28px"></asp:TextBox>
                </td>
            </tr>
            <tr style="height: 50px">
                <td>
                    消息内容
                </td>
                <td>
                    <asp:TextBox ID="txtContents" runat="server" TextMode="MultiLine" Width="98%" Height="48px"></asp:TextBox>
                </td>
            </tr>
            <tr style="height: 60px">
                <td>
                    接收人员
                </td>
                <td id="sUserList" title="选择接收人员">
                    <uc1:PopupSelect ID="psUser" height="30" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:CheckBox ID="ckbMessage" runat="server" Text="发送手机短信" Visible="false" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                     <asp:Button ID="btnSend" runat="server" Text="发送" CssClass="NewButton" OnClick="btnSend_Click"/>
                    <input id="btnClose" type="button" value="关闭" class="NewButton" onclick="Close();" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
