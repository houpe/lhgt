﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Business.Common;

public partial class ManageInside_Notice : System.Web.UI.Page
{
    public string title, content, id;
    protected void Page_Load(object sender, EventArgs e)
    {
        GetNotice();
    }

    private void GetNotice()
    {
        DataTable dt = InfoManage.GetNotice();
        title = dt.Rows[0]["title"].ToString();
        content = dt.Rows[0]["content"].ToString();
        id = dt.Rows[0]["id"].ToString();        
    }    
}
