﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Business.Common;
using WF_Business;

public partial class ManageInside_SendList : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if(!string.IsNullOrEmpty(Request["action"])&&"delete".Equals(Request["action"] ))
            {
                deleteData();
            }
            Databinder();
        }
    }
    private void Databinder()
    {
        GWOperation sData = new GWOperation();
        string whereValue = "";
        if (!string.IsNullOrEmpty(txtFileName.Text.Trim()))
        {
            whereValue = string.Format(" and a.filename like '%{0}%' ", txtFileName.Text.Trim());
        }
        if (!string.IsNullOrEmpty(calTimeBig.Value.Trim()))
        {
            whereValue = string.Format(" and a.uploadtime >=to_date('{0}','yyyy-mm-dd') ", calTimeBig.Value.Trim());
        }
        if (!string.IsNullOrEmpty(calTimeLess.Value.Trim()))
        {
            string dtLess = Convert.ToDateTime(calTimeLess.Value).AddDays(1).ToShortDateString();
            whereValue = string.Format(" and a.uploadtime <=to_date('{0}','yyyy-mm-dd') ", dtLess);
        }
        if (!string.IsNullOrEmpty(RealUserId))
        {
            whereValue += string.Format(" and a.owner='{0}' ", RealUserId);
        }
        DataTable dtTemp = sData.SendMessageList(whereValue);
        recvgrid.DataSource = dtTemp;
        recvgrid.RecordCount = dtTemp.Rows.Count;
        recvgrid.DataBind();
    }
    protected void recvgrid_OnLoadData(object sender, EventArgs e)
    {
        Databinder();
    }
    protected void recvgrid_RowCreated(object sender, GridViewRowEventArgs e)
    {
       
    }
    protected void btnQuery_Click(object sender, EventArgs e)
    {
        Databinder();
    }
    public void deleteData()
    {
        string id = Request["id"];
        string sql = "delete from xt_uploadfile where id='" + id + "'";
        SysParams.OAConnection().RunSql(sql);
    }
}
