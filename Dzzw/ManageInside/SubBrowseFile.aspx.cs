using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using Business;
using WF_Business;

	/// <summary>
	/// BrowseFile 的摘要说明。
	/// </summary>
public partial class ManageInside_SubBrowseFile : PageBase
{
    /// <summary>
    /// 设置标志位
    /// </summary>
    /// <param name="strId"></param>
    /// <param name="bDelete"></param>
    private void setclick(String strId, bool bDelete)
    {
        string sql = null;  //这里默认应该是为文件名唯一

        if (!bDelete)
        {
            sql = string.Format("update XT_BROWSEFILE set readflag =1, READ_TIMES=READ_TIMES+1,READTIME=sysdate where id ='{0}'", strId);
        }
        else
        {
            sql = string.Format("delete from XT_BROWSEFILE where id ='{0}'", strId);
        }

        SysParams.OAConnection().RunSql(sql);
    }

    protected void Page_Load(object sender, System.EventArgs e)
    {
        //删除
        if (Request.Params["flag"] == "delete")
        {
            setclick(Request.Params["id"], true);
        }

        //更新
        if (Request.Params["flag"] == "update")
        {
            setclick(Request.Params["id"], false);
        }

        if (!IsPostBack)
        {
            BindGrid();
        }
    }

    #region 绑定数据源
    /// <summary>
    /// 绑定数据源
    /// </summary>
    /// <returns></returns>
    private void BindGrid()
    {
        DataTable dt = new DataTable();
        string sql = string.Format(@"select a.FILENAME,
               a.UPLOADTIME,(select user_name from st_user where login_name = a.owner) as owner,
               a.owner as ownerid,b.userid, b.readflag,b.id,a.type,b.notes,b.READTIME 
                  from xt_uploadfile a,  xt_browsefile b
                 where b.upload_id = a.id
                 and b.userid = '{0}' and a.flag='0' and b.flag='0' 
                order by readflag,uploadtime desc", RealUserId);

        SysParams.OAConnection().RunSql(sql, out dt);

        recvgrid.DataSource = dt;
        recvgrid.RecordCount = dt.Rows.Count;
        recvgrid.DataBind();
    }
    #endregion

    /// <summary>
    /// datagrid数据加载
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void recvgrid_OnLoadData(object sender, EventArgs e)
    {
        this.BindGrid();
    }

    #region 行创建事件
    /// <summary>
    /// 行创建事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void recvgrid_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HyperLink getFile = e.Row.FindControl("getFile") as HyperLink;
            HyperLink deleteFile = e.Row.FindControl("deleteFile") as HyperLink;
            DataRowView dtr = e.Row.DataItem as DataRowView;
            HyperLink download = e.Row.FindControl("download") as HyperLink;
            if (dtr != null)
            {
                string strFileUrl = string.Format("UpLoadFile/{0}/{1}",
                    dtr["OWNERID"], dtr["filename"]);
                getFile.NavigateUrl = "#";
                getFile.Text = dtr["filename"].ToString();

                string strClick = string.Format("OpenNewUrl('{0}','{1}')", strFileUrl, dtr["id"]);
                getFile.Attributes.Add("onclick", strClick);

                download.NavigateUrl = "#";// strFileUrl;
                download.Attributes.Add("onclick", strClick);

                deleteFile.NavigateUrl = "#";
                strClick = string.Format("return DeleteFileClick('{0}')", dtr["id"]);
                deleteFile.Attributes.Add("onclick", strClick);

                if (dtr["readFlag"].ToString() == "1")
                {
                    e.Row.BackColor = System.Drawing.Color.Gray;
                }
            }
        }
    }

    #endregion

}