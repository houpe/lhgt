﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WebPubManager.aspx.cs" validateRequest="false"  Inherits="ManageInside_WebPubManager" %>

<%@ Register Src="../UserControls/Calendar.ascx" TagName="Calendar" TagPrefix="uc1" %>
<%@ Register TagPrefix="ftb" Namespace="FreeTextBoxControls" Assembly="FreeTextBox" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>添加web页面的后台</title>
    <link href="../css/page.css" rel="Stylesheet" type="text/css" />
</head>
<script type="text/javascript">
    function closeWin() {
        //        if (window.opener != null) {
        //            window.opener.location.href = window.opener.location.href;
        //        }
        //        window.close();
        history.back(-1);
    }

    function NotNull() {
        var Title = document.getElementById("<%=txt_title.ClientID %>").value
        var Conent = document.getElementById("<%=FreeTextBox1.ClientID %>").value

        if (fntrimspace(Title) == "") {
            alert("请输入【标题】");
            return false;
        }
        else if (fntrimspace(Conent) == "") {
            alert("请输入【内容】");
            return false;
        }
        else {
            return true;
        }
    }
    function fntrimspace(str)//去除首尾空格函数
    {
        while (str.substring(0, 1) == " ") {
            str = str.substring(1);
        }
        while (str.substring(str.length - 1) == " ") {
            str = str.substring(0, str.length - 1);
        }
        return str;
    }

    function AddAttachments() {
        document.getElementById('attach').value = "继续添加附件";

        tb = document.getElementById('attAchments');
        newRow = tb.insertRow();
        newRow.insertCell().innerHTML = "<input name='File' size='50' type='file'>&nbsp;&nbsp;<input type=button value='删除' onclick='delFile(this.parentElement.parentElement.rowIndex)'>";
    }
    function delFile(index) {
        document.getElementById('attAchments').deleteRow(index);
        tb.rows.length > 0 ? document.getElementById('attach').value = "继续添加附件" : document.getElementById('attach').value = "添加附件";
    }
</script>
<body>
    <form id="form1" method="post" runat="server" enctype="multipart/form-data">
    <div style="text-align: center">
        <table cellspacing="0" border="1" style="border-color: #704300" cellpadding="0" width="100%">
            <tr>
                <td style="color: Red; font-weight: bold; text-align: left; width: 45%">
                    <h1>
                        添加类型：<asp:Label ID="labMsg" runat="server" Text=""></asp:Label></h1>
                </td>
            </tr>
            <tr>
                <td style="text-align: left;" align="center">
                    标题：<asp:TextBox ID="txt_title" runat="server" Width="593px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="text-align: left;">
                    <ftb:FreeTextBox ID="FreeTextBox1" runat="server" Width="100%" StartMode="DesignMode"
                        AutoConfigure="EnableAll" ButtonPath="../images/ftb/officeXP/" Height="300px"
                        ImageGalleryPath="images/uploads">
                    </ftb:FreeTextBox>
                </td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <div>
                        <table id="attAchments">
                        </table>
                    </div>
                    <input type="button" id="attach" onclick="AddAttachments()" value="添加附件" />登录后自动弹出<asp:CheckBox
                        ID="ckbNotice" runat="server" Checked="false" />
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Button ID="btnSubmit" runat="server" Text="提交" CssClass="input" OnClientClick="return NotNull();"
                        OnClick="btnSubmit_Click"></asp:Button>
                    <input type="button" id="btnClose" value="关闭" class="NewButton" onclick="closeWin();" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
