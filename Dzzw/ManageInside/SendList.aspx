﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SendList.aspx.cs" Inherits="ManageInside_SendList" %>

<%@ Register Src="../UserControls/Calendar.ascx" TagName="Calendar" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>发送列表</title>
    <link href="../css/page.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div style="text-align: center;">
        <table style="width: 100%" class="tab">
            <tr>
                <th colspan="6">
                    <font size="4px">文件传送查看记录</font>
                </th>
            </tr>
            <tr>
                <td style="width: 100px">
                    公文名称
                </td>
                <td style="width: 20%">
                    <asp:TextBox ID="txtFileName" runat="server" Width="90%"></asp:TextBox>
                </td>
                <td style="width: 120px">
                    上传日期大于：
                </td>
                <td>
                    <uc2:Calendar ID="calTimeBig" runat="server" />
                </td>
                <td style="width: 120px">
                    上传日期小于：
                </td>
                <td>
                    <uc2:Calendar ID="calTimeLess" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="6">
                    <asp:Button ID="btnQuery" runat="server" Text="查询" OnClick="btnQuery_Click" CssClass="NewButton" />
                    <input id="btnBack" type="button" value="返回" onclick="history.back(-1);" class="NewButton" />
                </td>
            </tr>
            <tr>
                <td colspan="6">
                    <controlDefine:CustomGridView ID="recvgrid" runat="server" AllowPaging="True" Width="100%"
                        HideTextLength="30" OnOnLoadData="recvgrid_OnLoadData" OnRowCreated="recvgrid_RowCreated"
                        SkinID="List"  PageSize="20">
                        <PagerSettings Visible="False" />
                        <Columns>
                            <asp:BoundField DataField="filename" HeaderText="文件名">
                                <ItemStyle Width="400px" />
                                <HeaderStyle Width="400px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="UPLOADTIME" HeaderText="传送时间"></asp:BoundField>
                            <asp:BoundField DataField="OWNER" HeaderText="上传人"></asp:BoundField>
                            <asp:HyperLinkField HeaderText="收阅情况" DataNavigateUrlFields="id" DataNavigateUrlFormatString="SendListHistory.aspx?id={0}"
                                Text="收阅情况" />
                            <asp:HyperLinkField HeaderText="删除" DataNavigateUrlFields="id" DataNavigateUrlFormatString="sendlist.aspx?action=delete&id={0}"
                                Text="删除" />
                        </Columns>
                    </controlDefine:CustomGridView>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
