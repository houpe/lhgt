<%@ Page Language="c#" Inherits="ManageInside_SubBrowseFile" CodeFile="SubBrowseFile.aspx.cs" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../css/page.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        //删除
        function DeleteFileClick(strId) {
            var vRet = window.confirm("确定删除吗?");

            if (vRet) {
                window.location.href = "SubBrowseFile.aspx?flag=delete&id=" + strId;
            }
        }

        function OpenNewUrl(strUrl, strId) {
            window.open(strUrl);
            window.location.href = "SubBrowseFile.aspx?flag=update&id=" + strId;
        }
    </script>

</head>
<body>
    <form id="BrowseFile" method="post" runat="server">
        <div style="text-align: center;">
            <table style="width: 100%" class="tab">
                <tr>
                    <th>
                        <font size="4px">来件查阅</font>
                    </th>
                </tr>
                <tr>
                    <td>
                        <controlDefine:CustomGridView ID="recvgrid" runat="server" AllowPaging="True" Width="100%"
                            HideTextLength="30" OnOnLoadData="recvgrid_OnLoadData"  SkinID="List"
                            OnRowCreated="recvgrid_RowCreated" PageSize="20">
                            <PagerSettings Visible="False" />
                            <Columns>
                                <asp:TemplateField HeaderText="文件名">
                                    <ItemStyle Width="200px" />
                                    <HeaderStyle Width="200px" />
                                    <ItemTemplate>
                                        <asp:HyperLink ID="getFile" runat="server" Text="获取文件" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="OWNER" HeaderText="上传人"></asp:BoundField>
                                <asp:BoundField DataField="UPLOADTIME" HeaderText="传送时间"></asp:BoundField>
                                <asp:BoundField DataField="READTIME" HeaderText="阅读时间"></asp:BoundField>
                                <asp:BoundField DataField="notes" HeaderText="备注">
                                    <ItemStyle Width="170px" />
                                    <HeaderStyle Width="170px" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="下载">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="download" runat="server" Text="下载"></asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="编辑">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="deleteFile" runat="server" Text="删除"></asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </controlDefine:CustomGridView>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
