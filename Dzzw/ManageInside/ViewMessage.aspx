﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ViewMessage.aspx.cs" Inherits="SubData_ViewMessage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>查看即时消息</title>
    <link href="../css/page.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
   //关闭子窗口刷新父窗口
    function windowClose1()
    { 
        if (!window.opener.closed) 
	        {	           
	            window.opener.location.href = window.opener.location.href;
	            window.close();
	        }
    } 

    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div style="text-align: center">
            <br />
            <table style="width: 100%" class="tab">
                <tr>
                    <th colspan="4" style="height: 27px">
                        查看即时消息
                    </th>
                </tr>
                <tr>
                    <td style="width: 20%; height: 30px">
                        发送人
                    </td>
                    <td style="width: 25%">
                        <asp:Label ID="labSendUser" runat="server" Text=""></asp:Label>
                    </td>
                    <td style="width: 25%">
                        发送时间
                    </td>
                    <td style="width: 30%">
                        <asp:Label ID="labSendTime" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        标题
                    </td>
                    <td colspan="3">
                     <asp:Label ID="labTitle" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr style="height: 100px">
                    <td>
                        消息内容
                    </td>
                    <td colspan="3" style="text-align: left; vertical-align: top">
                        <asp:Label ID="labContents" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
            </table>
            <br />
            <table style="width: 100%;" class="tab">
                <tr style="display: none">
                    <td style="width: 20%">
                        附件
                    </td>
                    <td colspan="2">
                    </td>
                    <td style="width: 20%">
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <input id="btnClose" type="button" value="关闭" class="NewButton" onclick="windowClose1();" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
