using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using Business;
using WF_Business;

/// <summary>
/// 文件上传
/// </summary>
public partial class ManageInside_SubUpLoad : PageBase
{
    /// <summary>
    /// 页面加载
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, System.EventArgs e)
    {

    }

    protected void UpFileBtn_Click(object sender, EventArgs e)
    {
        bool IsFileExist;
        CreatFile();
        string FileName = PathToName(myfile.PostedFile.FileName);
        string ftype = DropDownList1.SelectedValue;
        IsFileExist = IsFileUploaded(FileName);  //检查在XT_UPLOADFILE表中，当前用户是否上传过该文件。以文件名和用户ID组合为准
        string path = Server.MapPath(".") + "\\UpLoadFile\\";

        myfile.PostedFile.SaveAs(path + RealUserId + "\\" + FileName);
        AppendFileRecord(FileName, IsFileExist, ftype);
        AppendBrowseRecord(FileName, IsFileExist);
        Page.ClientScript.RegisterStartupScript(this.GetType(), "loadOk", "<script>upfileSucceed('" + FileName + "');</script>");
    }

    #region 添加文件记录
    private void AppendFileRecord(String filename, bool fileexists, string ftype)
    {
        String sql1 = null;
        if (fileexists)
        {
            sql1 = string.Format(@"update xt_uploadfile set UPLOADTIME = SYSDATE,type='{2}' where FILENAME='{0}'
                     and OWNER='{1}'", filename, RealUserId, ftype);
        }
        else
        {
            sql1 = string.Format(@"insert into xt_uploadfile  (FILENAME, UPLOADTIME, OWNER,flag,type) values('{0}',
                      SYSDATE,'{1}','0','{2}')", filename, RealUserId, ftype);
        }

        SysParams.OAConnection().RunSql(sql1);
    }
    #endregion

    #region  判断文件是否上传
    private bool IsFileUploaded(String filename)
    {
        String sql1 = null;

        sql1 = string.Format("select count(*) from xt_uploadfile where filename='{0}' and owner = '{1}' and flag='0'",
            filename, RealUserId);

        int mycount = Convert.ToInt32(SysParams.OAConnection().GetValue(sql1));

        if (mycount > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    #endregion

    public string PathToName(string path)
    {
        int pos = path.LastIndexOf("\\");
        return path.Substring(pos + 1);
    }

    private void AppendBrowseRecord(String filename, bool IsFileExist)
    {
        string sql = null;
        string Notes = txtNotes.Text;
        string sendList = hidSendList.Value.TrimEnd('|');
        string[] arrSend = sendList.Split('|');
        for (int i = 0; i < arrSend.Length; i++)
        {
            sql = string.Format(@"insert into xt_browsefile (UserId, UPLOAD_ID,NOTES) values('{0}',
                    (select id from xt_uploadfile where owner='{1}' and filename='{2}' and rownum <2),'{3}')", arrSend[i]
                , RealUserId, filename, Notes);
            SysParams.OAConnection().GetValue(sql);
        }
    }

    protected void btn_postedfile_Click(object sender, EventArgs e)
    {
        Response.Redirect("SendList.aspx");
    }

    #region 创建文件
    /// <summary>
    /// 创建文件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_createfile_Click(object sender, System.EventArgs e)
    {

    }

    private void CreatFile()
    {
        string path = Server.MapPath(".") + "\\UpLoadFile\\";

        if (!System.IO.Directory.Exists(path + RealUserId))
        {
            Directory.CreateDirectory(path + RealUserId);
        }
    }

    #endregion
}
