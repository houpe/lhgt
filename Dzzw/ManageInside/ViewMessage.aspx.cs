﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Business.Common;

public partial class SubData_ViewMessage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Databinder();
        }
    }
    private void Databinder()
    {
        string messageid = Request["id"];
        Message msg = new Message();
        DataTable dtTemp = msg.GetMessageFromID(messageid);
        if (dtTemp.Rows.Count > 0)
        {
            labSendUser.Text = dtTemp.Rows[0]["senduser"].ToString();
            labSendTime.Text = dtTemp.Rows[0]["senddata"].ToString();
            labContents.Text = dtTemp.Rows[0]["messagetext"].ToString();
            labTitle.Text = dtTemp.Rows[0]["messagetitle"].ToString();
        }
    }

}
