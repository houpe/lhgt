<%@ Page Language="c#" Inherits="MyWindow_suspend_apply" CodeFile="suspend_apply.aspx.cs" %>

<html>
<head runat="server">
    <title>挂起原因录入</title>
    <link href="../css/page.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        function check() {//
            if (document.forms["add"].s_gqInfo.value == "") {
                alert("挂起原因不能为空");
                document.forms["add"].s_gqInfo.focus();
                return false;

            } else {
                return true;

            }
        }
 
    </script>
</head>
<body>
    <form name="add" method="post" action="suspend_add.aspx">
    <br>
    <table width="70%" border="0" align="center">
        <tr>
            <td>
                <div class="box_top_bar">
                    <div class="box_top_left">
                        <div class="box_top_right">
                            <div class="box_top_text">
                                案件挂起申请 &nbsp
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box_middle" style="width: 100%">
                    <br>
                    <table border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                业务编号:<%=id%>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                申请人:<%=s_username%>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                岗位名称:<%=s_gwname%>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                申请人原因;
                                <select name="suspend_type">
                                    <option value="0" <%=("0"==suspend_type?"selected":"")%>>用户原因</option>
                                    <option value="1" <%=("1"==suspend_type?"selected":"")%>>上报上级部门</option>
                                    <option value="2" <%=("2"==suspend_type?"selected":"")%>>其他原因</option>
                                </select>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                    <br>
                    <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" class="tab">
                        <tr>
                            <td>
                                挂起原因
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <div align="center">
                                    <textarea name="s_gqInfo" cols="90" rows="6" id="s_gqInfo"><%=s_memo%></textarea>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div align="right">
                                    <input name="type" type="hidden" id="type" value="<%=s_typetag%>" />
                                    <input name="s_id" type="hidden" id="s_id" value="<%=s_id%>" />
                                    <input name="s_userid" type="hidden" id="s_userid" value="<%=UserId%>" />
                                    <input name="s_gwId" type="hidden" id="suspend_type" value="<%=s_gwId%>" />
                                    <input name="id" type="hidden" id="id" value="<%=id%>" />
                                    <input name="s_apple" class="input" type="submit" id="s_apple" value="挂起" onclick="return check()" />
                                    <input type="button" class="input" value="返回" onclick='window.navigate("../MyWindow/TaskInStepList.aspx")' />
                                </div>
                            </td>
                        </tr>
                    </table>
                    <br>
                </div>
            </td>
        </tr>
    </table>
    </form>
    <p align="left">
        &nbsp;</p>
</body>
</html>
