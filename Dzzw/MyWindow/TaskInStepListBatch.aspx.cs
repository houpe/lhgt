﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Business.FlowOperation;
using Business;
using System.Drawing;
using Common;

public partial class MyWindow_TaskInStepListBatch : PageBase
{
    RequestFlowOperation flowOperation = new RequestFlowOperation();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            gvSerial.DataKeyNames = new string[] { "顺序号", "wiid", "stepctlid" };

            BindData();
        }
    }

    /// <summary>
    /// 绑定数据
    /// </summary>
    private void BindData()
    {
        string strParamsSql = string.Empty;
        try
        {
            DataTable dtSource = flowOperation.AppearWorkItem(UserId, "");
            gvSerial.DataSource = dtSource;
            gvSerial.PageSize = SystemConfig.PageSize;
            gvSerial.RecordCount = dtSource.Rows.Count;
            gvSerial.DataBind();
        }
        catch
        {
            Response.Clear();
            Response.Write(strParamsSql);
        }
    }

    /// <summary>
    /// 绑定数据
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvSerial_OnLoadData(object sender, EventArgs e)
    {
        BindData();
    }

    /// <summary>
    /// 数据列创建事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvSerial_RowCreated(object sender, GridViewRowEventArgs e)
    {
        switch (e.Row.RowType)
        {
            case DataControlRowType.Header:
                break;
            case DataControlRowType.DataRow:
                {
                    DataRowView dtr = (DataRowView)e.Row.DataItem;
                    if (dtr != null)
                    {
                        Int64 nDbgq = Convert.ToInt64(dtr["督办过期"]);
                        Int64 nJdgq = Convert.ToInt64(dtr["结点过期"]);

                        if (nJdgq < 0)
                        {
                            e.Row.BackColor = Color.Red;
                        }
                        else if (nDbgq < 0)
                        {
                            e.Row.BackColor = Color.YellowGreen;
                        }

                        HyperLink hlProcess = e.Row.FindControl("hlProcess") as HyperLink;
                        if (null != hlProcess)
                        {
                            hlProcess.NavigateUrl = string.Format("javascript:HandleWorkItem('{0}','{1}','{2}','{3}')",
                                dtr["wiid"], e.Row.RowIndex, dtr["顺序号"], dtr["所在岗位"]);
                        }

                        HyperLink hlSpecailProcess = e.Row.FindControl("hlSpecailProcess") as HyperLink;
                        if (null != hlSpecailProcess)
                        {
                            hlSpecailProcess.NavigateUrl = string.Format("suspend_apply.aspx?ID={0}&s_gwid={1}&PageRedirect=../MyWindow/TaskInStepList.aspx", dtr["顺序号"], dtr["wiid"]);
                        }

                        HyperLink hlBack = e.Row.FindControl("hlBack") as HyperLink;
                        if (dtr["isback"].ToString().Equals("1"))
                        {
                            gvSerial.Columns[9].Visible = true;
                            gvSerial.Columns[10].Visible = false;

                            if (null != hlBack)
                            {
                                hlBack.NavigateUrl = string.Format("javascript:openRemark('{0}')", dtr["wiid"]);
                                hlBack.Text = string.Format("{0}：退", dtr["前一用户"]); 
                            }
                        }
                        else
                        {
                            hlBack.Text = dtr["前一用户"].ToString(); 
                        }
                    }
                    break;
                }
        }
    }

    /// <summary>
    /// 优先级图标
    /// </summary>
    /// <param name="str"></param>
    /// <returns></returns>
    protected string Getyou(string str)
    {
        if (str.EndsWith("0"))
            str = "top_ico_yxj1.gif";
        else if (str.Equals("1"))
            str = "top_ico_yxj2.gif";
        else
            str = "top_ico_yxj3.gif";

        return str;
    }

    /// <summary>
    /// 提取数据并批量提交
    /// </summary>
    /// <param name="keys"></param>
    private void ProcessNameAndValue(DataKey[] keys)
    {
        string[] arrIID = new string[keys.Length];
        string[] arrWiid = new string[keys.Length];
        string strStepId = string.Empty;

        //关键值："顺序号", "wiid", "所在岗位", "stepctlid", "案件类型"
        for (int i = 0; i < keys.Length; i++)
        {
            DataKey key = keys[i];

            arrIID[i] = key.Values["顺序号"].ToString();
            arrWiid[i] = key.Values["wiid"].ToString();

            if (i == 0)
            {
                strStepId = key.Values["stepctlid"].ToString();
            }
        }

        string strWiids = string.Join(",", arrWiid);
        string strResult = flowOperation.IsHaveSameSerialNameAndStep(strWiids);

        if (!string.IsNullOrEmpty(strResult))
        {
            WindowAppear.WriteAlert(this.Page, strResult);
            return;
        }
        else
        {
            string strScript = string.Format(" window.open(\"../office/DoBeforeTaskBatchSubmit.aspx?iid={0}&ItemId={1}&step={2}&ctlid={2}\", \"showSubmit\", \"left=50,top=50,width=500,height=200\");", string.Join(",", arrIID), strWiids, strStepId);
            WindowAppear.ExecuteScript(this.Page, strScript);
        }
    }

    /// <summary>
    /// 完成事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvSerial_OnComplelteClick(object sender, WebControls.CustomGridViewEventArgs e)
    {
        if (e == null)
        {
            return;
        }
        if (e.MultiSelectKeys.Length == 0)
        {
            return;
        }

        ProcessNameAndValue(e.MultiSelectKeys);
    }
}
