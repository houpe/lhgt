<%@ Page Language="c#" Inherits="MyWindow_del_suspend_query" CodeFile="del_suspend_query.aspx.cs" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>申请案件解挂</title>
    <link rel="stylesheet" id="skinCss" type="text/css" href="../css/default/easyui.css" />
    <link rel="stylesheet" type="text/css" href="../css/icon.css" />
    <link href="../css/page.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../ScriptFile/jquery.js"></script>
    <script type="text/javascript" src="../ScriptFile/jquery.easyui.min.js"></script>
    <script src="../ScriptFile/easyui-lang-zh_CN.js" type="text/javascript"></script>
</head>
<body onload="ShowOK();">
    <script type="text/javascript">
        function tosubmit(page) {
            var strUrl = "del_suspend_query.aspx";
            strUrl += "?Date_begin=" + $("#startDate").datebox('getValue'); ;
            strUrl += "&strIID=" + document.all.strIID.value;
            strUrl += "&Date_end=" + $("#endDate").datebox('getValue'); ;
            strUrl += "&dis=" + document.all.dis.value;
            strUrl += "&xmmc=" + document.all.xmmc.value;
            strUrl += "&applyer=" + document.all.applyer.value;
            strUrl += "&serialapplyer=" + document.all.serialapplyer.value;
            if (page == "" || page == null)
                strUrl += "&pagenow=" + document.all.jumpPage.value;
            else
                strUrl += "&pagenow=" + page;

            window.location.href = strUrl;
        }
        function sendNotice(s_id, type, iid) {
            if (window.confirm("确认申请解挂？")) {
                window.location.href = "suspend_add.aspx?s_id=" + s_id + "&type=" + type + "&id=" + iid;

            }
            else {
                return false;
            }
        }

        function ShowOK() {
            var type = "<%=Request["type"]%>";
            if (type == "ok") {
                alert("已经发送短信通知审批人，请等待！");
            }
        }
        //重置
        function resetValue() {
            document.getElementById("Date_begin").value = "";
            document.getElementById("Date_end").value = "";
            document.getElementById("strIID").value = "";
        }
        function SetValue(querylist) {
            var query = querylist.split('|');
            if (query.length > 0) {
                document.getElementById("dis").value = query[0];
                document.getElementById("xmmc").value = query[1];
                document.getElementById("applyer").value = query[2];
                document.getElementById("strIID").value = query[3];
                document.getElementById("serialapplyer").value = query[4];
                document.getElementById("Date_begin").value = query[5];
                document.getElementById("Date_end").value = query[6];

            }

            var btnque = document.getElementById("btnSubmit");
            btnque.click();
        }    
    </script>
    <form id="form1" runat="server">
    <table width="100%" class="tab" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                挂起时间
                <input class="easyui-datebox" id="startDate" />
                至
                <input class="easyui-datebox" id="endDate" />
                案件编号:<input name="strIID" type="text" value="<%=strIID %>" size="12" />
                <input type="button" id="btnSubmit" class="NewButton" value="查询" onclick="tosubmit(0)" />
            </td>
        </tr>
    </table>
    <div>
        <%=strtable    %>
        <table width="100%" border="0" cellspacing="0" cellpadding="4">
            <tr>
                <td style="text-align: right; height: 20px">
                    每页<%=pagesize%>行 共<%=mypage.RecordCount %>行 当前第<%=pagenow%>页 共<%=pagecount %>页
                    <%
                        if (pagenow == 1)
                        {
                            Response.Write(" 首页 上一页");
                        }
                        else
                        {
		
                    %>
                    <a href="javascript:tosubmit('1')">首页</a> <a href="javascript:tosubmit('<%=pagenow - 1%>')">
                        上一页</a>
                    <%
                        }
	
                    %>
                    <%
                        if (pagenow == pagecount)
                        {
                            Response.Write("下一页 尾页");
                        }
                        else
                        {
		
                    %>
                    <a href="javascript:tosubmit('<%=pagenow + 1%>')">下一页</a> <a href="javascript:tosubmit('<%=pagecount%>')">
                        尾页</a>
                    <%
                        }
	
                    %>
                    转到第<select name="jumpPage" id="jumpPage" onchange="tosubmit()">
                        <%
                            for (int i = 1; i <= pagecount; i++)
                            {
                                if (i == pagenow)
                                {
			
                        %>
                        <option selected="selected" value="<%=i%>">
                            <%=i%>
                        </option>
                        <%
}
                                            else
                                            {
			
                        %>
                        <option value="<%=i%>">
                            <%=i%>
                        </option>
                        <%
                            }
                                        }
	
                        %>
                    </select>
                    页
                </td>
            </tr>
        </table>
    </div>
    <input id="dis" name="dis" value="<%=strDis%>" style="width: 0px; height: 0px" />
    <input id="xmmc" name="xmmc" value="<%=strXmmc%>" style="width: 0px; height: 0px" />
    <input id="applyer" name="applyer" value="<%=strapplyer %>" style="width: 0px; height: 0px;" />
    <input id="serialapplyer" name="serialapplyer" value="<%=strserialapplyer%>" style="width: 0px;
        height: 0px" />
    </form>
</body>
</html>
