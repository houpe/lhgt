﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RightDelegated.aspx.cs" Inherits="MyWindow_DelegateRight" %>

<%@ Register Src="../UserControls/Calendar.ascx" TagName="Calendar" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>权利委托</title>
    <link href="../css/page.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div style="text-align: center; margin: 0px auto;">
        <%--<div class="box_top_bar">
            <div class="box_top_left">
                <div class="box_top_right">
                    <div class="box_top_text">
                        权利委托</div>
                </div>
            </div>
        </div>--%>
        <div class="box_middle">
            <table width="92%" border="0" cellpadding="0" cellspacing="0" class="box_middle_tab">
                <tr>
                    <td style="text-align: right;">
                        待委托流程：
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlProcessName" runat="server" Width="180px">
                        </asp:DropDownList>
                    </td>
                    <td style="text-align: right;">
                        委托给：
                    </td>
                    <td style="width: 60%;text-align: left;">
                        <asp:DropDownList ID="ddlUnit" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlUnit_SelectedIndexChanged">
                        </asp:DropDownList>
                        <asp:DropDownList ID="ddlDep" runat="server" Width="180" AutoPostBack="True" OnSelectedIndexChanged="ddlDep_SelectedIndexChanged">
                        </asp:DropDownList>
                        <asp:DropDownList ID="ddlUserName" runat="server">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td nowrap="noWrap">
                        委托开始时间：
                    </td>
                    <td style="text-align: left;">
                        <uc1:Calendar ID="startDate" runat="server" />
                    </td>
                    <td nowrap="noWrap">
                        委托结束时间：
                    </td>
                    <td style="text-align: left;">
                        <uc1:Calendar ID="endDate" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:Button Text="新增委托" ID="btnUpdate" class="NewButton" runat="server" OnClick="btnUpdate_Click" />
                    </td>
                </tr>
            </table>
        </div>
        <controlDefine:MessageBox ID="mbInfo" runat="server" Width="100%" BoxType="Info" />
        <controlDefine:CustomGridView runat="server" ID="gvSerial" SkinID="List" AutoGenerateColumns="false"
            OnOnLoadData="gvSerial_OnLoadData" ShowHideColModel="None" AllowPaging="true"
            OnRowDeleting="gvSerial_RowDeleting" Width="100%">
            <Columns>
                <asp:BoundField HeaderText="委托人" DataField="UserName" />
                <asp:BoundField DataField="delegate_UserName" HeaderText="被委托人" />
                <asp:BoundField DataField="delegate_task" HeaderText="委托的业务" />
                <asp:BoundField DataField="begin_time" HeaderText="开始时间" />
                <asp:BoundField DataField="end_time" HeaderText="结束时间" />
                <asp:TemplateField HeaderText="删除" ShowHeader="False">
                    <ItemTemplate>
                        <asp:LinkButton ID="ImageButton1" runat="server" CausesValidation="false" CommandName="Delete" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"ID")%>' Text="删除" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </controlDefine:CustomGridView>
    </div>
    </form>
</body>
</html>
