﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TaskBuild.aspx.cs" Inherits="MyWindow_TaskBuild" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>业务受理</title>
    <link href="../css/page.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="javascript">
        function NameNotNull() {
            var UserName = document.getElementById("txtUserName").value
            if (UserName.length == 0) {
                alert("申请人不能为空.");
                return false;
            }
            else {
                return true;
            }
        }
        //清空
        function Cleaner() {
            document.form1.ddlSerialType.selectedIndex = 0;
            document.form1.txtUserName.value = "";
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table width="60%" border="0" align="center" style="margin-top: 25px">
            <tr>
                <td>
                    <div class="box_top_bar">
                        <div class="box_top_left">
                            <div class="box_top_right">
                                <div class="box_top_text">
                                    新建流程</div>
                            </div>
                        </div>
                    </div>
                    <div class="box_middle" style="width:100%">
                        <table width="89%" border="0" cellpadding="0" cellspacing="0" class="box_middle_tab">
                            <tr>
                                <td style="text-align: right">
                                    流程类型：
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlSerialType" runat="server" Style="width: 180px">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right">
                                    申请人：
                                </td>
                                <td>
                                    <input id="txtUserName" name="txtUserName" runat="server" type="text" style="width: 180px" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:RadioButtonList ID="radioArray" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem Selected="True" Value="0">转到列表</asp:ListItem>
                                        <asp:ListItem Value="1">继续接件</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="text-align: center">
                                    <asp:Button ID="btnProcess" runat="server" Text="提交" OnClick="btnProcess_Click" class="input"
                                        OnClientClick="NameNotNull()" />
                                    <asp:Button ID="btnReset" runat="server" Text="重置" OnClientClick="Cleaner()" CssClass="input" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
