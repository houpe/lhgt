<%@ Page Language="c#" Inherits="MyWindow_suspend_promise" CodeFile="suspend_promise.aspx.cs" %>

<html>
<head runat="server">
    <title>办件挂起审批</title>
    <link href="../css/page.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" id="skinCss" type="text/css" href="../css/default/easyui.css" />
    <link rel="stylesheet" type="text/css" href="../css/icon.css" />
    <script type="text/javascript" src="../ScriptFile/jquery.js"></script>
    <script type="text/javascript" src="../ScriptFile/jquery.easyui.min.js"></script>
    <script src="../ScriptFile/easyui-lang-zh_CN.js" type="text/javascript"></script>
</head>
<body>
    <script type="text/javascript">
		function tosubmit(page)
		{
			var strUrl="suspend_promise.aspx";
			strUrl+="?Date_begin="+$("#Date_begin").datebox('getValue');
			strUrl+="&Date_end="+$("#Date_end").datebox('getValue');
			strUrl+="&strIID="+document.all.strIID.value;
			strUrl+="&flag=<%=Request["flag"]%>";
			if(page==""|| page==null)
				strUrl+="&pagenow="+document.all.jumpPage.value;
			else	
			 		
				strUrl+="&pagenow="+page;
			
			window.location.href=strUrl;
		}
		
		function CancelSuspend(strId,strType,strIID)
		{
		    var varResult = window.confirm('确定要取消吗？');
		    if( varResult )
		    {
		        window.location.href="../mywindow/suspend_add.aspx?s_id=" +strId + "&type="+strType+"&id="+strIID;
		    }
		}
		
		function SetSuspend(strIid,strType)
		{
		    var varResult = window.confirm('确定要挂起吗？');
		    if( varResult )
		    {
		        window.location.href="../mywindow/suspend_add.aspx?s_id=" +strIid + "&type="+strType;
		    }
		}
	
    </script>
    <form name="del" method="post" action="" onsubmit="">
    <table width="100%" border="0" align="center">
        <tr>
            <td>
                <div class="tab_top">
                    <div class="tab_top_left">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="background-image: url(../images/top_button_03.gif); background-repeat: no-repeat;
                                    text-align: center; height: 35px; width: 93px;">
                                    <a href="suspend_promise.aspx?flag=nogq">未挂起</a>
                                </td>
                                <td style="background-image: url(../images/top_button_03.gif); background-repeat: no-repeat;
                                    text-align: center; height: 35px; width: 93px;">
                                    <a href="suspend_promise.aspx?flag=ygq">已挂起</a>
                                </td>
                                <td style="background-image: url(../images/top_button_03.gif); background-repeat: no-repeat;
                                    text-align: center; height: 35px; width: 93px;">
                                    <a href="suspend_promise.aspx?flag=jcgq">解除挂起</a>
                                </td>
                                <td>
                                    从<input class="easyui-datebox" id="Date_begin" />
                                    到<input class="easyui-datebox" id="Date_end" />
                                    案件编号:<input name="strIID" type="text" value="<%=strIID %>" size="12" />
                                    <input type="submit" name="Submit" class="input" value="查询" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="tab">
                    <tr>
                        <th>
                            业务编号
                        </th>
                        <th>
                            岗位名称
                        </th>
                        <th>
                            申请类型
                        </th>
                        <th>
                            申请人
                        </th>
                        <th>
                            挂起原因
                        </th>
                        <th>
                            申请时间
                        </th>
                        <th>
                            详细信息
                        </th>
                        <%
                            if (flag == "nogq")
                            {
                        %>
                        <th>
                            挂起
                        </th>
                        <%
                            }
                            else if (flag == "jcgq")
                            {
                        %>
                        <th>
                            解除挂起
                        </th>
                        <%
                            }
                        %>
                    </tr>
                    <%=htmlCode%>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="4">
                    <tr>
                        <td align="right" colspan="8" height="20">
                            每页<%=pagesize%>行 共<%=mypage.RecordCount%>行 当前第<%=pagenow%>页 共<%=pagecount%>页
                            <%
                                if (pagenow == 1)
                                {
                                    Response.Write(" 首页 上一页");
                                }
                                else
                                {
		
                            %>
                            <a href=" javascript:tosubmit('1')">首页</a> <a href=" javascript:tosubmit('<%=(pagenow - 1)%>')">
                                上一页</a>
                            <%
                                }
	
                            %>
                            <%
                                if (pagecount == pagenow)
                                {
                                    Response.Write("下一页 尾页");
                                }
                                else
                                {
		
                            %>
                            <a href="javascript:tosubmit('<%=pagenow + 1%>')">下一页</a> <a href="javascript:tosubmit('<%=pagecount%>')">
                                尾页</a>
                            <%
                                }
	
                            %>
                            转到第<select name="jumpPage" id="jumpPage" onchange="tosubmit()">
                                <%
                                    for (int i = 1; i <= pagecount; i++)
                                    {
                                        if (i == pagenow)
                                        {
			
                                %>
                                <option selected value="<%=i%>">
                                    <%=i%>
                                </option>
                                <%
}
                                        else
                                        {
			
                                %>
                                <option value="<%=i%>">
                                    <%=i%>
                                </option>
                                <%
                                    }
                                    }
	
                                %>
                            </select>
                        页
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
