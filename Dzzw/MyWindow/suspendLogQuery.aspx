﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="suspendLogQuery.aspx.cs"
    Inherits="MyWindow_suspendLogQuery" %>

<%@ Register Src="../UserControls/Calendar.ascx" TagName="Calendar" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>挂起日志查询</title>
    <link href="../css/page.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function tosubmit(page) {
            var strUrl = "suspendLogQuery.aspx";
            if (page == "" || page == null) {
                strUrl += "?pagenow=" + document.all.jumpPage.value;
            }
            else {
                strUrl += "?pagenow=" + page;
            }
            window.location.href = strUrl;
        }
    
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div style="text-align: center">
        <table width="100%" class="tab" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td style="text-align: left">
                    开始时间：
                    <uc1:Calendar ID="calStartData" runat="server" Width="110px" />
                    结束时间：
                    <uc1:Calendar ID="calEndData" runat="server" Width="110px" />
                    业务编号：
                    <asp:TextBox ID="txtIID" runat="server" Width="100px"></asp:TextBox>
                    <asp:Button ID="btnQuery" runat="server" Text="查询" CssClass="input" OnClick="btnQuery_Click" />
                </td>
            </tr>
        </table>
        <table width="100%" border="0" style="text-align: center" cellpadding="0" cellspacing="0"
            class="tab">
            <tr>
                <th>
                    业务编号
                </th>
                <th>
                    申请人
                </th>
                <th>
                    批准人
                </th>
                <th>
                    挂起原因
                </th>
                <th>
                    挂起时间
                </th>
                <th>
                    解挂时间
                </th>
            </tr>
            <%=strbuilderHTML.ToString() %>
        </table>
        <%=strPageHTML.ToString() %>
    </div>
    <div style="text-align: left; background-color: #F5C3FB; color: #E522FD">
        申请人取消挂起申请
    </div>
    </form>
</body>
</html>
