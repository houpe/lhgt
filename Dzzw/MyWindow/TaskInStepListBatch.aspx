﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TaskInStepListBatch.aspx.cs" Inherits="MyWindow_TaskInStepListBatch" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>案件办理列表</title>
    <link href="../css/page.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        function HandleWorkItem(WorkItem, index, iid, step) {
            var strURL = "../Office/TaskProcess.aspx?ItemId=" + WorkItem + "&iid=" + iid + "&step=" + encodeURI(step);
            window.open(strURL, '_blank', "height=" + (screen.availHeight - 30) + " , width=" + screen.availWidth + ", top=0, left=0,toolbar=no, menubar=no, scrollbars=no, location=no, status=no");
        }
        function openRemark(IID) {
            var strURL = "listremark.aspx?IID=" + IID;
            window.open(strURL, 'newwindow', 'height=255, width=520, top=250, left=250, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=n o, status=no')
        }
    </script>

    <style type="text/css">
        td {
            white-space: nowrap;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div style="text-align:left">
            注：<img src="../images/top_ico_yxj1.gif" />普通件 &nbsp;&nbsp;<img src="../images/top_ico_yxj2.gif" />加急件
        &nbsp;&nbsp;<img src="../images/top_ico_yxj3.gif" />特急件
            &nbsp;&nbsp;<span style="color:YellowGreen">预警</span>&nbsp;&nbsp;<span style="color:red">超期</span>
            </div>
        <div style="text-align: center">
            <controlDefine:CustomGridView runat="server" ID="gvSerial" SkinID="List" AutoGenerateColumns="false" ShowGridViewSelect="MultiSelect" OnOnComplelteClick="gvSerial_OnComplelteClick" CancelButtonStyle="display:none"
                CompleteButtonText="批量提交" AllowOnMouseOverEvent="false"
                OnOnLoadData="gvSerial_OnLoadData" ShowHideColModel="None" AllowPaging="true"
                CssClass="tab" OnRowCreated="gvSerial_RowCreated">
                <Columns>
                    <asp:TemplateField HeaderText="处理" ShowHeader="False">
                        <ItemTemplate>
                            <asp:HyperLink ID="hlProcess" runat="server">
                            <img alt="" src="../images/tab_top_ico.gif" style="border: 0px" width="19" height="20" /></asp:HyperLink>
                        </ItemTemplate>
                        <HeaderStyle Width="35px" />
                    </asp:TemplateField>
                    <asp:BoundField DataField="顺序号" HeaderText="编号" />
                    <asp:BoundField DataField="案件类型" HeaderText="案件类型" />
                    <asp:BoundField DataField="申请单位" HeaderText="申请人" />
                    <asp:BoundField DataField="接收时间" HeaderText="接收时间" />
                    <asp:TemplateField HeaderText="前一用户">
                        <ItemTemplate>
                            <asp:HyperLink ID="hlBack" runat="server"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="剩余工作日" HeaderText="剩余工作日" />
                    <asp:BoundField DataField="所在岗位" HeaderText="当前岗位" />
                    <asp:TemplateField HeaderText="优先级">
                        <ItemTemplate>
                            <img src="../images/<%#Getyou(Eval("优先级").ToString())%>" alt="" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="申请挂起" ShowHeader="False" Visible="false">
                        <ItemTemplate>
                            <asp:HyperLink ID="hlSpecailProcess" runat="server"><img alt="" src="../images/tab_end_ico.gif" style="border: 0px; width: 24px; height: 23px" /></asp:HyperLink>
                        </ItemTemplate>
                        <HeaderStyle Width="35px" />
                    </asp:TemplateField>
                </Columns>
            </controlDefine:CustomGridView>
        </div>
    </form>
</body>
</html>
