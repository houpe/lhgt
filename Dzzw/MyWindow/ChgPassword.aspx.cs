﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Business.Common;

public partial class MyWindow_ChgPassword : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            labUser.Text = UserName;
        }
    }

    /// <summary>
    /// 修改密码
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnChangePass_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(this.txtOldPass.Value))
        {
            ExceptionManager.WriteAlert(Page, "原密码没有输入！");
        }
        if (string.IsNullOrEmpty(this.txtNewPass.Value))
        {
            ExceptionManager.WriteAlert(Page, "新密码没有输入！");
        }
        if (string.IsNullOrEmpty(this.txtNewPassAgain.Value))
        {
            ExceptionManager.WriteAlert(Page, "重复密码没有输入！");
        }
        if (!string.IsNullOrEmpty(this.txtNewPassAgain.Value) && !string.IsNullOrEmpty(this.txtNewPass.Value))
        {
            if (this.txtNewPassAgain.Value != this.txtNewPass.Value)
            {
                ExceptionManager.WriteAlert(Page, "两次输入的密码不一致！");
            }
        }

        Business.Admin.StUserOperation user = new Business.Admin.StUserOperation();
        bool bReturn = user.ChangePassword(UserId, txtOldPass.Value, txtNewPass.Value);

        if (bReturn)
        {
            ExceptionManager.WriteAlert(Page, "修改成功。");
        }
        else
        {
            ExceptionManager.WriteAlert(Page, "原密码输入错误，修改失败！");
        }
    }
}
