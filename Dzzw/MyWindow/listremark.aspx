<%@ Page Language="c#" Inherits="MyWindow_listremark" CodeFile="listremark.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head runat="server">
    <title>备注信息</title>
    <link href="../css/page.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <div style="text-align: center; margin: 0px auto; width: 98%;">
        <div class="box_top_bar">
            <div class="box_top_left">
                <div class="box_top_right">
                    <div class="box_top_text">
                        备注信息</div>
                </div>
            </div>
        </div>
        <div class="box_middle" style="width: 100%; text-align: center;">
            <textarea id="textarea1" rows="11" runat="server" cols="60" style="width: 100%"></textarea>
        </div>
        <div>
            <input type="Button" class="input" name="close" onclick="window.close()" value="关闭" />
        </div>
    </div>
</body>
</html>
