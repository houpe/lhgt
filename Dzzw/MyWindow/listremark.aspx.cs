using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Business.FlowOperation;

/// <summary>
/// Summary description for listremark.
/// </summary>
public partial class MyWindow_listremark : System.Web.UI.Page
{
    protected void Page_Load(object sender, System.EventArgs e)
    {
        String iid = Request["IID"];
        SuspendQueryService handleCase = new SuspendQueryService();
        string Content = handleCase.ShowRemarkByIId(iid);
        if (string.IsNullOrEmpty(Content))
        {
            Content = "没有备注信息";
        }
        textarea1.Value = Content;
    }
}
