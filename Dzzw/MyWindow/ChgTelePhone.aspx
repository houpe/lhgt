﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ChgTelePhone.aspx.cs" Inherits="MyWindow_ChgTelePhone" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>修改电话信息</title>
    <link href="../css/page.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">

        function DelValue() {
            var txtOfficeTel = document.getElementById("txtOfficeTel");
            var txtExtNo = document.getElementById("txtExtNo");
            var txtMobile = document.getElementById("txtMobile");
            var txtHouseTel = document.getElementById("txtHouseTel");

            txtOfficeTel.value = "";
            txtExtNo.value = "";
            txtMobile.value = "";
            txtHouseTel.value = "";
        }

    </script>

</head>
<body>
    <form id="form1" runat="server">

        <div class="box_top_bar">
            <div class="box_top_left">
                <div class="box_top_right">
                    <div class="box_top_text">
                        修改电话信息
                    </div>
                </div>
            </div>
        </div>
        <div class="box_middle">
            <table width="90%" border="0" align="center" class="box_middle_tab">
                <tr>
                    <td>用户名称：
                    </td>
                    <td colspan="3" style="text-align: left">
                        <asp:Label ID="labUser" runat="server" Text="" Style="width: 120px"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>办公电话：
                    </td>
                    <td style="text-align: left">
                        <input id="txtOfficeTel" type="text" runat="server" style="width: 120px" />
                    </td>
                    <td>内线号码：
                    </td>
                    <td style="text-align: left">
                        <input id="txtExtNo" type="text" runat="server" style="width: 120px" />
                    </td>
                </tr>
                <tr>
                    <td>手机号码：
                    </td>
                    <td style="text-align: left">
                        <input id="txtMobile" type="text" runat="server" style="width: 120px" />
                    </td>
                    <td>住宅电话：
                    </td>
                    <td style="text-align: left">
                        <input id="txtHouseTel" type="text" runat="server" style="width: 120px" />
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:Button CssClass="NewButton" ID="btnChangePass" runat="server" OnClick="btnChangePass_Click"
                            Text="确定" />
                        <asp:Button CssClass="NewButton" ID="btnCancel" runat="server" OnClientClick="DelValue()"
                            Text="取消" />
                        <asp:Label ID="labMsg" runat="server" ForeColor="#FF6633"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>

    </form>
</body>
</html>
