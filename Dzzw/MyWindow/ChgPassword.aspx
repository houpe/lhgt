﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ChgPassword.aspx.cs" Inherits="MyWindow_ChgPassword" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>修改密码</title>
    <link href="../css/page.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <script type="text/javascript">
        function ComparePass(source, arguments) {
            var newPass = document.getElementById("<%=txtNewPass.ClientID%>");

            if (arguments.Value == newPass.value) {
                arguments.IsValid = true;
            }
            else {
                arguments.IsValid = false;
            }
        }

        function DelValue() {
            var password1 = document.getElementById("<%=txtOldPass.ClientID%>");
            var password2 = document.getElementById("<%=txtNewPass.ClientID%>");
            var password3 = document.getElementById("<%=txtNewPassAgain.ClientID%>");

            password1.value = "";
            password2.value = "";
            password3.value = "";
        }

    </script>
    <form id="form1" runat="server" style="text-align:center">
    <div style="width: 70%;">
        <div class="box_top_bar">
            <div class="box_top_left">
                <div class="box_top_right">
                    <div class="box_top_text">
                        修改密码
                    </div>
                </div>
            </div>
        </div>
        <div class="box_middle" style="width: 100%">
            <table width="90%" border="0" align="center" class="box_middle_tab">
                <tr>
                    <td>
                        用户名：
                    </td>
                    <td>
                        <asp:Label ID="labUser" runat="server" Text="Label" Width="180px"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        原有密码：
                    </td>
                    <td>
                        <input id="txtOldPass" type="password" runat="server" style="width: 180px" />
                    </td>
                </tr>
                <tr>
                    <td>
                        输入密码：
                    </td>
                    <td>
                        <input id="txtNewPass" type="password" runat="server" style="width: 180px" />
                    </td>
                </tr>
                <tr>
                    <td>
                        重复密码：
                    </td>
                    <td>
                        <input id="txtNewPassAgain" type="password" runat="server" style="width: 180px" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Button CssClass="input" ID="btnChangePass" runat="server" OnClick="btnChangePass_Click"
                            Text="确定" />
                        <asp:Button CssClass="input" ID="btnCancel" runat="server" OnClientClick="DelValue()"
                            Text="取消" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </form>
</body>
</html>
