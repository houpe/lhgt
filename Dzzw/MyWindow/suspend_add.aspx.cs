using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Business;
using Business.FlowOperation;

/// <summary>
/// Summary description for suspend_add.
/// </summary>
public partial class MyWindow_suspend_add : PageBase
{
    protected void Page_Load(object sender, System.EventArgs e)
    {
        SuspendQueryService handlecase=new SuspendQueryService();
        System.String temppage = Session["temppage"] != null ? Session["temppage"].ToString() : "";//得到页面
        System.String s_userid = UserId;//用户id	
        System.String id = Request["id"];//业务编号	
        System.String s_gwId = Request["s_gwId"];//岗位id
        System.String s_memo = Request["s_gqInfo"];//挂起原因
        string Wname = string.Empty;
        System.String s_suspentype = Request["suspend_type"];//挂起类型

        System.String s_id = Request["s_id"];//唯一ID号自增长
        System.String s_gwname = "";
        //获得当前时间				
        System.String s_type = Request["type"];//动作类型

        System.String jsHtml = "";

        int nextUserStepNo = -1;
        // 添加业务删除标识条件 and i.isdelete <>1
        Wname = handlecase.GetWnameByCondi(id);

        try
        {
            string sql=handlecase.HandleOperate(UserId,Wname, s_userid, ref id, ref s_gwId, ref s_memo, Wname, ref s_suspentype, s_id, ref s_gwname, s_type, ref jsHtml, ref nextUserStepNo);
            Response.Write(jsHtml);
            handlecase.OperatSuspend_Add(id, Wname, s_id, s_type, nextUserStepNo,sql);
            Response.Write("<script type=\"text/javascript\" language=\"javascript\">" + "\r\n");
            Response.Write("alert(\"操作成功!\");" + "\r\n");
            if (s_type.Equals("gqsq"))
            {
                Response.Write("document.location = '" + temppage + "';" + "\r\n");
            }
            else if (s_type.Equals("jcgq"))
            {
                Response.Write("document.location = \"../xnjc/suspend_query.aspx?flag=jcgq\";" + "\r\n");
            }
            else if (s_type.Equals("pzgq"))
            {
                Response.Write("document.location = \"../xnjc/suspend_query.aspx?flag=nogq\";" + "\r\n");
            }
            else if (s_type.Equals("qxsq") || s_type.Equals("sqjg"))
            {
                Response.Write("document.location = '../mywindow/del_suspend_query.aspx';" + "\r\n");
            }
            Response.Write("</script>" + "\r\n");
        }
        catch
        {
            Response.Write("<script type=\"text/javascript\" language=\"javascript\">" + "\r\n");
            Response.Write("alert(\"输入数据异常，请检查后重试!\");" + "\r\n");
            Response.Write("</script>" + "\r\n");
        }
        finally
        {

        }
    }

}
