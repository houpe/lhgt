﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Business;
using Business.Admin;
using Business.FlowOperation;

public partial class MyWindow_DelegateRight : PageBase 
{
    private StUserGroupHandle UserRight = new StUserGroupHandle();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ClsUserWorkFlow cuwfTemp = new ClsUserWorkFlow();
            DataTable dt = cuwfTemp.GetWorkFlowByUserId(UserId);
            DataRow dr=dt.NewRow(); 
            dr["wname"]="全部";
            dt.Rows.InsertAt(dr,0);
            ddlProcessName.DataSource = dt;
            ddlProcessName.DataTextField = "wname";
            ddlProcessName.DataValueField = "wname";
            ddlProcessName.DataBind();
            BindData();
            BindUnit();
        }
    }
    /// <summary>
    /// 绑定表格数据
    /// </summary>
    private void BindData()
    {
        DataTable dtSource;
        dtSource = UserRight.GetDelegate(UserId);
        gvSerial.DataSource = dtSource;
        gvSerial.PageSize = SystemConfig.PageSize;
        gvSerial.RecordCount = dtSource.Rows.Count;
        gvSerial.DataBind();
    }
    private void BindUnit()
    {
        DepartHandle dep=new DepartHandle();
        DataTable dt=dep.GetDepartment();      
        ddlUnit.DataSource = dt;
        ddlUnit.DataTextField = "depart_name";
        ddlUnit.DataValueField = "departid";
        ddlUnit.DataBind();
        BindDep(ddlUnit.SelectedValue); 
    }
    /// <summary>
    /// 绑定部门数据
    /// </summary>
    private void BindDep(string unitId)
    {
        DepartHandle dep = new DepartHandle();
        DataTable dt=dep.GetDepartment(unitId);       
        ddlDep.DataSource = dt;
        ddlDep.DataTextField = "depart_name";
        ddlDep.DataValueField = "departid";
        ddlDep.DataBind();
        BindUser(ddlDep.SelectedValue); 
    }
    private void BindUser(string depId)
    {
        
        DataTable dt= DepartHandle.GeDepartmentByDepartId(depId);
        ddlUserName.DataSource = dt;
        ddlUserName.DataTextField = "user_name";
        ddlUserName.DataValueField = "userid";
        ddlUserName.DataBind();
    }
    protected void ddlUnit_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindDep(ddlUnit.SelectedValue);
    }
    protected void ddlDep_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindUser(ddlDep.SelectedValue);
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        string strGetUserId = UserRight.GetDelegateByUserId(UserId,ddlProcessName.SelectedValue);

        if (string.IsNullOrEmpty(strGetUserId))
        {
            UserRight.InserDelegate(UserId, ddlUserName.SelectedValue, ddlProcessName.SelectedValue, startDate.Value, endDate.Value);
            BindData();
        }
        else
        {
            //Response.Write("<script>alert('如果要新增委托请删除旧的委托');</script>");
            //修改为客户端弹出框模式,以避免在弹出对话框时,本页面变为空白页面 
            Page.ClientScript.RegisterStartupScript(this.GetType(), "js", "<script>alert('如果要新增委托请删除旧的委托')</script>");
        }
        
    }
    protected void gvSerial_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        LinkButton lb = (LinkButton)gvSerial.Rows[e.RowIndex].FindControl("ImageButton1");
        string id = lb.CommandArgument;
        UserRight.DeleteDelegate(id);
        BindData();
    }
    protected void gvSerial_OnLoadData(object sender, EventArgs e)
    {
        BindData();
    }
}
