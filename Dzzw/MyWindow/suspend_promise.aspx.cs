using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Business.Inspect;
using Business.Admin;
using System.Collections.Generic;
using WF_Business;

public partial class MyWindow_suspend_promise : PageBase
{
    protected string tempsql = string.Empty;
      //定义分页变量
    protected int pagesize = 15;//页面记录数
    protected int pagenow = 1;//当前页
    protected int pagecount = 0;//总页数
    protected string d_s = "";
    protected string d_e = "";
    protected string strIID = "";
    protected string s_username = string.Empty;
    protected string s_gwname = string.Empty;
    protected string s_suspendtype = string.Empty;
    protected string flag = "";
    protected string htmlCode = "";
    protected Business.Common.PageOperation mypage = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        String mpage = Request["pagenow"];

        if (!string.IsNullOrEmpty(mpage))
        {
            pagenow = System.Int32.Parse(mpage); ;
            if (pagenow < 1)
                pagenow = 1;
        }

        //获得穿过来的查询参数
        String s_checkuserid = UserId;//得到当前用户ID
        if (!string.IsNullOrEmpty(s_checkuserid))
        {
            Session.Add("s_checkuserid", s_checkuserid);//
        }
        d_s = Request["Date_begin"];
        d_e = Request["Date_end"];
        strIID = Request["strIID"];
        if (string.IsNullOrEmpty(d_s))
        {
            d_s = string.Empty;
        }
        if (string.IsNullOrEmpty(d_e))
        {
            d_e = string.Empty;
        }
        flag = Request["flag"];
        if (flag == null)
        {
            flag = "nogq";
        }

        string sql = string.Empty;
        string Username = string.Empty;

        //*********************************//
        string userid = UserId;
        bool ISsystemUser = SystemManager.IsSystemUser(userid);
        Xnjc xnjc = new Xnjc();
        DataTable dtOut = xnjc.suspend_query_list(sql, userid,flag, ISsystemUser);

        mypage = new Business.Common.PageOperation(dtOut, pagenow, pagesize);
        pagecount = mypage.PageCount;

        List<System.Data.DataRow> listwork = mypage.CurrentPageList;

        foreach (System.Data.DataRow myArray in listwork)
        {
            if (!Convert.IsDBNull(myArray[3]))
            {
                //获得当前用户的姓名
                sql = "select user_name from st_user where userid='" + myArray[3].ToString() + "' ";
                s_username = SysParams.OAConnection().GetValue(sql);
            }
            if (!Convert.IsDBNull(myArray[1]))
            {
                //获得当前岗位的名称
                sql = "select step from st_work_item where wiid='" + myArray[1].ToString() + "' ";
                s_gwname = SysParams.OAConnection().GetValue(sql);
            }
            s_suspendtype = myArray[2].ToString();

            if (s_suspendtype.Equals("0"))
            {
                s_suspendtype = "用户原因";
            }
            else if (s_suspendtype.Equals("1"))
            {
                s_suspendtype = "上报上级部门";
            }
            else if (s_suspendtype.Equals("2"))
            {
                s_suspendtype = "其他原因";
            }
            htmlCode+="<tr bgcolor=\"#ffffff\" onmouseover=\"this.style.backgroundColor='#caf7fd'\" onMouseOut=\"this.style.backgroundColor='#ffffff'\">" + "\r\n";

            htmlCode+="<td align=\"center\">" + myArray[0].ToString() + "</td>\r\n";
            htmlCode+="<td align=\"center\">" + s_gwname + "</td>\r\n";
            htmlCode += "<td align=\"center\">" + s_suspendtype + "</td>\r\n";
            htmlCode+="<td align=\"center\">" + s_username + "</td>\r\n";
            htmlCode+="<td align=\"center\" style=\"width:250px\">" + myArray[5].ToString() + "</td>\r\n";

            htmlCode+="<td align=\"center\">" + myArray[4].ToString() + "</td>\r\n";

            htmlCode+="<td style=\"width:60px\" ><a href=\"../QueryAndCollect/TaskQueryDetail.aspx?iid=" + myArray[0].ToString() + "&search=\">详细</a></td>\r\n";

            if (flag == "jcgq")
            {
                htmlCode+="<td ><a href='#' onclick=\"CancelSuspend('" + myArray[6].ToString() + "','jcgq','" + myArray[0].ToString() + "')\">解挂</a></td>\r\n";
            }
            else if (flag == "nogq")
            {
                htmlCode+="<td ><a href='#' onclick=\"SetSuspend('" + myArray[6].ToString() + "','pzgq')\">挂起</a></td>\r\n";
            }

            htmlCode+="</tr>\r\n";

        }
    }

   
}
