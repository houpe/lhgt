﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="suspend_DetailQuery.aspx.cs"
    Inherits="OfficeOperation_MyWindow_suspend_DetailQuery" %>

<%@ Register Src="../UserControls/GeneralSelect.ascx" TagName="GeneralSelect" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>挂起办件明细查看</title>
    <link rel="stylesheet" type="text/css" href="../css/page.css" />
     <link rel="stylesheet" id="skinCss" type="text/css" href="../css/default/easyui.css" />
    <link rel="stylesheet" type="text/css" href="../css/icon.css" />
    <script type="text/javascript" src="../ScriptFile/jquery.js"></script>
    <script type="text/javascript" src="../ScriptFile/jquery.easyui.min.js"></script>
    <script src="../ScriptFile/easyui-lang-zh_CN.js" type="text/javascript"></script>
    <script type="text/javascript">
        function setValue() {
            dis = document.getElementById("ddldistrict_ddlControl").value; //区属
            xmmc = document.getElementById("xmmc").value; //项目名称
            applyer = document.getElementById("applyer").value; //挂起申请人
            ajbh = document.getElementById("ajbh").value; //案件编号
            ajapplyer = document.getElementById("serialapplyer").value; //案件申请人
            startdate =$("#Date_begin").datebox('getValue'); //挂起时间从
            enddate =$("#Date_end").datebox('getValue'); //挂起时间到
            var querylist = dis + "|" + xmmc + "|" + applyer + "|" + ajbh + "|" + ajapplyer + "|" + startdate + "|" + enddate;
            window.opener.SetValue(querylist);
            window.close();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div style="text-align: center">
        <table class="tab" width="98%">
            <tr>
                <td style="text-align: right">
                    区属：
                </td>
                <td style="text-align: left">
                    <uc2:GeneralSelect ID="ddldistrict" runat="server" Name="区属" TextOnly="true" FirstEmpty="true" />
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    项目名称：
                </td>
                <td style="text-align: left">
                    <input type="text" id="xmmc" style="width: 150px" />
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    案件编号：
                </td>
                <td style="text-align: left">
                    <input type="text" id="ajbh" style="width: 150px" />
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    挂起申请人姓名：
                </td>
                <td style="text-align: left">
                    <input type="text" id="applyer" style="width: 150px" />
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    案件申请人姓名：
                </td>
                <td style="text-align: left">
                    <input type="text" id="serialapplyer" style="width: 150px" />
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    挂起时间：
                </td>
                <td style="text-align: left">
                    从<input class="easyui-datebox" id="Date_begin" />
                    到<input class="easyui-datebox" id="Date_end" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="button" value="确定" class="input" id="btnYes" onclick="setValue();" />
                    &nbsp;&nbsp;
                    <input type="button" value="关闭" class="input" id="btnClose" onclick="javascript:window.close();" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
