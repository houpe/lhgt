﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Business.Admin;

public partial class MyWindow_ChgTelePhone : PageBase
{
    UserKaoQing uI = new UserKaoQing();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)        
        {
            DataBinder();
            labMsg.Text = "";
        }
    }
    protected void btnChangePass_Click(object sender, EventArgs e)
    {
        string OfficeTel = txtOfficeTel.Value.Trim();
        string ExtNo = txtExtNo.Value.Trim();
        string Mobile = txtMobile.Value.Trim();
        string HouseTel = txtHouseTel.Value.Trim();
        try
        {
            uI.UpdateUserTelePhoneInfo(OfficeTel, ExtNo, Mobile, HouseTel,UserId);
            labMsg.Text = "更新成功!";
        }
        catch
        {
            throw;
        }
    }
    //绑定
    private void DataBinder()
    {
       
        string userID = UserId;
        DataTable dtTemp = new DataTable();
        dtTemp = uI.GetUserBasicInfo(userID);
        if (dtTemp.Rows.Count > 0)
        {
            txtOfficeTel.Value = dtTemp.Rows[0]["OFFICETEL"].ToString();
            txtExtNo.Value = dtTemp.Rows[0]["EXTNO"].ToString();
            txtMobile.Value = dtTemp.Rows[0]["MOBILE"].ToString();
            txtHouseTel.Value = dtTemp.Rows[0]["HOUSETEL"].ToString();
            labUser.Text = dtTemp.Rows[0]["USER_NAME"].ToString();
        }
    }

   
}
