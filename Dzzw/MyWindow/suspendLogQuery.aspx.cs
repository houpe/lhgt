﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using Business.Common;
using System.Collections.Generic;
using Business;
using Business.FlowOperation;

public partial class MyWindow_suspendLogQuery : PageBase
{
    public StringBuilder strbuilderHTML = new StringBuilder();
    public StringBuilder strPageHTML = new StringBuilder();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            GetData();
            CreatHTML();
        }
    }

    #region Body
    private void CreatHTML()
    {
        string strStart = calStartData.Value;
        string strEnd = calEndData.Value;
        string iid = txtIID.Text;
        SuspendLogInfo suInfo = new SuspendLogInfo();
        DataTable dt = suInfo.GetSuspendLog(UserId, strStart, strEnd, iid);

        int pagesize = 15;//页面记录数
        int pagenow = 1;//当前页
        int pagecount = 0;//总页数
        string mpage = Request["pagenow"];

        if (!string.IsNullOrEmpty(mpage))
        {
            pagenow = System.Int32.Parse(mpage); ;
            if (pagenow < 1)
            {
                pagenow = 1;
            }
        }

        PageOperation mypage = new PageOperation(dt, pagenow, pagesize);
        pagecount = mypage.PageCount;
        List<DataRow> listwork = mypage.CurrentPageList;

        int nCount = listwork.Count;
        if (nCount > 0)
        {
            for (int i = 0; i < nCount; i++)
            {
                DataRow myArray = listwork[i] as DataRow;
                string[] sShowFiled = { myArray[0].ToString(), myArray[4].ToString(), myArray[5].ToString(), myArray[3].ToString(), myArray[1].ToString(), myArray[2].ToString() };
                if (myArray[6].ToString() == "2")
                {
                    strbuilderHTML.AppendFormat("<tr style='background-color:#F5C3FB';>");
                }
                else
                {
                    strbuilderHTML.AppendFormat("<tr>");
                }
                if (myArray[3].ToString().Length < 14)
                {
                    strbuilderHTML.AppendFormat(@"<td>{0}</td> <td>{1}</td> <td>{2}</td> <td>{3}</td> <td>{4}</td> <td>{5}</td> </tr>", sShowFiled);
                }
                else
                {
                    strbuilderHTML.AppendFormat(@"<td>{0}</td> <td>{1}</td> <td>{2}</td> <td title='{3}'>{6}</td> <td>{4}</td> <td>{5}</td> </tr>",
                        myArray[0].ToString(), myArray[4].ToString(), myArray[5].ToString(), myArray[3].ToString(), myArray[1].ToString(), myArray[2].ToString(), myArray[3].ToString().Substring(0, 12) + "...");
                }
            }
        }
        else
        {
            strbuilderHTML.Append("<tr><td colspan='6'>对不起，没有找到相关数据！</td></tr>");
        }

        strPageHTML = PageOperation.CreatPageChanges(mypage, pagenow, pagecount, pagesize);
    }
    #endregion

    #region 查询
    protected void btnQuery_Click(object sender, EventArgs e)
    {
        CreatHTML();
        Session["start"] = calStartData.Value;
        Session["end"] = calEndData.Value;
        Session["IID"] = txtIID.Text;
    }
    #endregion


    private void GetData()
    {
        if (Session["start"] != null && !string.IsNullOrEmpty(Session["start"].ToString()))
        {
            calStartData.Value = Session["start"].ToString();
        }
        if (Session["end"] != null && !string.IsNullOrEmpty(Session["end"].ToString()))
        {
            calEndData.Value = Session["end"].ToString();
        }
        if (Session["IID"] != null && !string.IsNullOrEmpty(Session["IID"].ToString()))
        {
            txtIID.Text = Session["IID"].ToString();
        }
    }
}
