﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business.FlowOperation;
using System.Data;

public partial class MyWindow_TaskBuild : PageBase
{
    public RequestFlowOperation flowOperation = new RequestFlowOperation();

    /// <summary>
    /// 页面加载
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindData();
        }
    }

    /// <summary>
    /// 数据绑定
    /// </summary>
    public void BindData()
    {
        DataTable dtTemp = flowOperation.GetWorkFlowByUserId(UserId);
        foreach (DataRow drTemp in dtTemp.Rows)
        {
            ListItem liTemp = new ListItem(drTemp["wname"].ToString(), drTemp["wid"].ToString());
            ddlSerialType.Items.Add(liTemp);
        }
        txtUserName.Value = UserName;
    }

    /// <summary>
    /// 业务受理
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnProcess_Click(object sender, EventArgs e)
    {
        string strWiid = string.Empty;
        string strErrorMsg = string.Empty;
        int nResult = flowOperation.BuildTask(ddlSerialType.SelectedValue, UserId, txtUserName.Value, ref strWiid,ref strErrorMsg);
        if (nResult == 1)
        {
            if (radioArray.SelectedIndex == 0)
            {
                Response.Redirect(string.Format("TaskInStepList.aspx?wid={0}&WorkFlowName={1}",
                    ddlSerialType.SelectedValue, ddlSerialType.Text));
            }
        }
        else//创建流程失败
        {
            string strScript = string.Format("<script>alert(\"创建流程失败：{0}\")</script>", strErrorMsg);
            Page.ClientScript.RegisterStartupScript(this.GetType(), "alertMsg", strScript);
        }
    }
}
