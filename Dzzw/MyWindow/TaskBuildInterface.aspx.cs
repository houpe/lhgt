﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business.FlowOperation;
using Business.Admin;

public partial class MyWindow_TaskBuildInterface :Page
{
    public RequestFlowOperation flowOperation = new RequestFlowOperation();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string strWiid = string.Empty;
            string strWid = Request["wid"];
            string strUserId = Request["userid"];
            string strUserName = string.Empty;

            StUserOperation userManage = new StUserOperation();
            System.Data.DataSet dsOut = userManage.ValideUser(strUserId);

            if (dsOut.Tables[0].Rows.Count > 0)//登陆成功
            {
                strUserName = dsOut.Tables[0].Rows[0]["user_name"].ToString();

                if (!string.IsNullOrEmpty(strUserName))
                {
                    string strErrorMsg = string.Empty;
                    int nResult = flowOperation.BuildTask(strWid, strUserId, strUserName, ref strWiid,ref strErrorMsg);

                    if (nResult == 1)
                    {
                        string strStep = flowOperation.GetStep(strWiid);
                        string strIID = flowOperation.GetIidFromWiid(strWiid);

                        string strScript = string.Format("<script>HandleWorkItem('{0}','{1}','{2}')</script>", strWiid, strIID, strStep);
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "openTask", strScript);
                    }
                    else//创建流程失败
                    {
                        string strScript = string.Format("<script>alert(\"创建流程失败：{0}\")</script>",strErrorMsg);
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "alertMsg", strScript);
                    }
                }
                else//无用户名信息
                {
                    string strScript = "<script>alert('OA系统用户的注释信息')</script>";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "alertMsg", strScript);
                }
            }
            else//登录失败
            {
                string strScript = "<script>alert('OA系统暂无此用户信息')</script>";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "alertMsg", strScript);
            }
        }
    }
}
