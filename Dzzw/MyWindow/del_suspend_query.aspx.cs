using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Business;
using Business.FlowOperation;
using System.Collections.Generic;
using Common;

/// <summary>
/// Summary description for del_suspend_query.
/// </summary>
public partial class MyWindow_del_suspend_query : PageBase
{
    //定义分页变量
    public int pagesize = 15;//页面记录数
    public int pagenow = 1;//当前页
    public int pagecount = 0;//总页数
    public string s_username = "";
    public string s_gwname = "";
    public string s_suspendtype = "";
    public Business.Common.PageOperation mypage = null;
    public List<System.Data.DataRow> listwork = null;
    public DataTable dtTemp = null;
    public string d_s = string.Empty;
    public string d_e = string.Empty;
    public string strIID = string.Empty;
    public string strDis = string.Empty;
    public string strXmmc = string.Empty;
    public string strapplyer = string.Empty;
    public string strserialapplyer = string.Empty;
    public string strtable = string.Empty;

    protected void Page_Load(object sender, System.EventArgs e)
    {
        SuspendQueryService handleCase = new SuspendQueryService();

        string mpage = Request["pagenow"];

        strtable = @" <table width='100%' border='0' style='text-align: center' cellpadding='0' cellspacing='0'
                            class='tab'>
                            <tr>
                                <th>
                                    业务编号</th>
                                <th>
                                    岗位名称</th>
                                <th>
                                    申请类型</th>
                                <th>
                                    申请人</th>
                                <th>
                                    挂起原因</th>
                                <th>
                                    挂起时间</th>
                                <th>
                                    操作</th>
                                <th>
                                    查看</th>
                            </tr>";

        if (!string.IsNullOrEmpty(mpage))
        {
            pagenow = System.Int32.Parse(mpage); ;
            if (pagenow < 1)
                pagenow = 1;
        }

        d_s = Request["Date_begin"];
        d_e = Request["Date_end"];
        strIID = Request["strIID"];
        strDis = Request["dis"];
        strXmmc = Request["xmmc"];
        strapplyer = Request["applyer"];
        strserialapplyer = Request["serialapplyer"];

        if (d_s == null)
        {
            d_s = "";
        }
        if (d_e == null)
        {
            d_e = "";
        }
        DataTable rs1;

        handleCase.GetIidByOverTimeAndUserId(UserId, out dtTemp);

        try
        {
            rs1 = handleCase.GetHangCaseList(strXmmc, strDis, strserialapplyer, strapplyer, d_s, d_e, strIID, UserId);


            mypage = new Business.Common.PageOperation(rs1, pagenow, pagesize);
            pagecount = mypage.PageCount;
            listwork = mypage.CurrentPageList;

            for (int i = 0; i < listwork.Count; i++)
            {

                System.Data.DataRow myArray = listwork[i] as System.Data.DataRow;
                if (myArray[3] != null)
                {
                    s_username = handleCase.GetCurrentUserName(myArray[3].ToString());
                }
                if (myArray[1] != null)
                {
                    s_gwname = handleCase.GetCurrentGwName(myArray[1].ToString());
                }
                s_suspendtype = myArray[2].ToString();
                if (!String.IsNullOrEmpty(s_suspendtype) && s_suspendtype.Equals("0"))
                {
                    s_suspendtype = "用户原因";
                }
                else if (!String.IsNullOrEmpty(s_suspendtype) && s_suspendtype.Equals("1"))
                {
                    s_suspendtype = "上报上级部门";
                }
                else if (!String.IsNullOrEmpty(s_suspendtype) && s_suspendtype.Equals("2"))
                {
                    s_suspendtype = "其他原因";
                }
                strtable += "<tr bgcolor=\"#ffffff\" onmouseover=\"this.style.backgroundColor='#caf7fd'\" onMouseOut=\"this.style.backgroundColor='#ffffff'\">" + "\r\n";

                strtable += "<td align=\"center\">" + myArray[0] + "</td>" + "\r\n";
                strtable += "<td align=\"center\">" + s_gwname + "</td>" + "\r\n";
                strtable += "<td align=\"center\">" + s_suspendtype + "</td>" + "\r\n";
                strtable += "<td align=\"center\">" + s_username + "</td>" + "\r\n";
                strtable += "<td align=\"center\">" + myArray[5] + "</td>" + "\r\n";
                strtable += "<td align=\"center\">" + myArray[7] + "</td>" + "\r\n";
                if (string.IsNullOrEmpty(myArray[7].ToString()))
                {
                    strtable += "<td ><a href=# onclick=\"sendNotice(" + myArray[6] + ",'qxsq'," + myArray[0] + ");\">取消申请</a></td>" + "\r\n";
                }
                else
                {
                    strtable += "<td ><a href=# onclick=\"sendNotice('" + myArray[6] + "','sqjg'," + myArray[0] + ");\">申请解挂</a></td>" + "\r\n";
                }
                strtable += "<td><a href=\"../QueryAndCollect/TaskQueryDetail.aspx?iid=" + myArray[0] + "&name=" + s_gwname + "\" >查看</td>" + "\r\n";
                strtable += "</tr>" + "\r\n";
            }
            strtable += "</table>";

        }
        catch (System.Exception ex)
        {
            WindowAppear.WriteAlert(Page, ex.Message);
        }

    }
}
