using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Business;
using Business.FlowOperation;

/// <summary>
/// Summary description for suspend_apply.
/// </summary>
public partial class MyWindow_suspend_apply : PageBase
{
    //获得参数	
    public string id = string.Empty;
    public string s_gwId = string.Empty;
    public string s_gwname = "";
    public string s_username = "";
    public string s_memo = "";
    public string suspend_type = "";
    public string s_typetag = "gqsq";
    public string s_id = "";

    protected void Page_Load(object sender, System.EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindValues();
        }
    }

    protected void BindValues()
    {
        SuspendQueryService handleCase = new SuspendQueryService();
        //获得参数	
        id = Request["id"];//业务编号	
        s_gwId = Request["s_gwid"];//岗位id
        s_gwname = "";
        s_username = "";

        if (id == null)
        {
            id = "";
        }
        if (s_gwId == null)
        {
            s_gwId = "";
        }
        //获得当前用户的姓名
        s_username = handleCase.GetCurrentUserName(UserId);
        //获得当前岗位的名称
        s_gwname = handleCase.GetCurrentGwName(s_gwId);

        //System.String tempvalue = "挂起申请";
        System.String temppage = Request["PageRedirect"];
        if (temppage == null)
        {
            temppage = Session["temppage"].ToString();//得到页面
        }
        else
        {
            Session.Add("temppage", temppage);//
        }
        //取得当前记录
        DataTable rs;
        rs = handleCase.GetCurrentListByIdAndHandleId(id,s_gwId);
        if (rs.Rows.Count > 0)
        {
            suspend_type = rs.Rows[0][0].ToString();
            s_memo = rs.Rows[0][1].ToString();
            s_id = rs.Rows[0][2].ToString();
            //tempvalue = "取消申请";
            s_typetag = "qxsq";
        }
    }
}
