﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Business.Admin;

public partial class CarManage_CarBasicEdit : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        PersistenceControl1.PageControl = this.form1;
        if (!IsPostBack)
        {
            BindList();

            string action = Request["action"];
            if (!string.IsNullOrEmpty(action))
            {
                ViewState["action"] = action;
            }
           
            if (action == "add")
            {
                PersistenceControl1.KeyValue = System.Guid.NewGuid().ToString();
                PersistenceControl1.Status = "insert";
            }
            else//查看或编辑
            {
                PersistenceControl1.KeyValue = Request.QueryString["id"];
                PersistenceControl1.Status = action;

                if (action == "add")
                {
                    btnSave.Visible = false;
                }
            }

            PersistenceControl1.Render();
        }
    }

    /// <summary>
    /// 绑定下拉框
    /// </summary>
    protected void BindList()
    {
        DataTable dtSource = CarManage.GetJsy();
        for (int i = 0; i < dtSource.Rows.Count; i++)
        {
            ListItem liTemp = new ListItem(dtSource.Rows[i]["user_name"].ToString(),
                dtSource.Rows[i]["userid"].ToString());
            ddlJSYXM.Items.Add(liTemp);
        }
    }

    #region 保存
    protected void btnSave_Click(object sender, EventArgs e)
    {
        PersistenceControl1.Update();

        string strScript = "closeWindow();";
        Common.WindowAppear.ExecuteScript(this.Page, strScript);
        
    }
    #endregion 
}
