﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Business.Admin;

public partial class CarManage_CarBasicManage : System.Web.UI.Page
{   
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string action = Request["action"];
            if (action == "del")
            {
                CarManage.DelCarInfo(Request["id"]);
            }
            Databinder();
        }
    }   

    #region 绑定数据
    private void Databinder()
    {
        CarManage carM = new CarManage();

        string strWhere = "";
        if (!string.IsNullOrEmpty(txtCph.Text))
        {
            strWhere = string.Format(" and cph like '%{0}%'",txtCph.Text);
        }

        DataTable dt = carM.GetCarBasic(strWhere);
        gridviewCar.DataSource = dt;
        gridviewCar.RecordCount = dt.Rows.Count;
        gridviewCar.DataBind();
    }
    #endregion

    #region 行创建
    protected void gridviewCar_RowCreated(object sender, GridViewRowEventArgs e)
    {
        switch (e.Row.RowType)
        {
            case DataControlRowType.Header:
                break;
            case DataControlRowType.DataRow:
                string strID = string.Empty;
                DataRowView rowView = e.Row.DataItem as DataRowView;
                if (rowView != null)
                {
                    strID = rowView["id"].ToString();
                }
                string strUrl = string.Empty;
                string strUrlAppend = "top=200,left=200,toolbar=no,menubar=no,scrollbars=yes,location=no,status=no,width=500,height=230;";
                View(e, strID, strUrl, strUrlAppend);
                Edit(e, strID, strUrl, strUrlAppend);
                Delete(e, strID);
                break;
        }
    }
    #endregion

    #region 查看
    private void View(GridViewRowEventArgs e, string strID, string strUrl, string strUrlAppend)
    {
        HyperLink hylkView = e.Row.FindControl("hyview") as HyperLink;
        hylkView.Attributes.Add("href", "#");
        strUrl = "CarBasicEdit.aspx?id=" + strID + "&action=view";
        hylkView.Attributes.Add("onclick", string.Format("javascript:window.open('{0}','','{1}');", strUrl, strUrlAppend));
    }
    #endregion

    #region 编辑
    private void Edit(GridViewRowEventArgs e, string strID,  string strUrl, string strUrlAppend)
    {
        HyperLink hylkEdit = e.Row.FindControl("hyedit") as HyperLink;
        hylkEdit.Attributes.Add("href", "#");
        strUrl = "CarBasicEdit.aspx?id=" + strID + "&action=edit";
        hylkEdit.Attributes.Add("onclick", string.Format("javascript:window.open('{0}','','{1}');", strUrl, strUrlAppend));
    }
    #endregion

    #region 删除
    private void Delete(GridViewRowEventArgs e, string strID)
    {
        HyperLink hylkDel = e.Row.FindControl("hydel") as HyperLink;
        hylkDel.NavigateUrl = "CarBasicManage.aspx?id=" + strID + "&action=del";
    }
    #endregion

    #region 查询
    protected void btnQuery_Click(object sender, EventArgs e)
    {
        Databinder();
    }
    #endregion
}
