﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CarBasicEdit.aspx.cs" Inherits="CarManage_CarBasicEdit" %>

<%@ Register Src="../UserControls/GeneralSelect.ascx" TagName="GeneralSelect" TagPrefix="uc2" %>
<%@ Register Src="../UserControls/Calendar.ascx" TagName="Calendar" TagPrefix="uc1" %>
<%@ Register Src="../UserControls/PersistenceControl.ascx" TagName="PersistenceControl"
    TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>编辑车辆信息</title>
    <link type="text/css" href="../css/page.css" rel="stylesheet" />
    <script type="text/javascript">

        function closeWindow() {
            window.opener.location.href = window.opener.location.href;
            window.close();
        }
   
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div style="text-align: center">
        <br />
        <table style="width: 100%" class="tab">
            <caption style="font-size: 14px; font-weight: bold">
                车辆信息管理</caption>
            <tr>
                <td>
                    车牌号
                </td>
                <td>
                    <asp:TextBox ID="txtCPH" runat="server" Width="150px"></asp:TextBox>
                </td>
                <td>
                    车辆型号
                </td>
                <td>
                    <asp:TextBox ID="txtCLXH" runat="server" Width="150px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    驾驶员姓名
                </td>
                <td>
                    <asp:DropDownList ID="ddlJSYXM" runat="server" Width="155px">
                    </asp:DropDownList>
                </td>
                <td>
                    驾驶证号
                </td>
                <td>
                    <asp:TextBox ID="txtJSZH" runat="server" Width="150px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    投入使用时间
                </td>
                <td>
                    <uc1:Calendar ID="calTRSYSJ" runat="server" Width="150px" />
                </td>
                <td>
                    油耗标准
                </td>
                <td>
                    <asp:TextBox ID="txtYHBZ" runat="server" Width="150px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    备注
                </td>
                <td colspan="3">
                    <asp:TextBox ID="txtBZ" runat="server" TextMode="MultiLine" Width="96%"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>
    <div style="text-align: center;">
        <asp:Button ID="btnSave" runat="server" Text="保存" CssClass="input" OnClick="btnSave_Click" />
        <input id="btnBack" type="button" value="关闭" class="NewButton" onclick="closeWindow();" />
    </div>
    <uc3:PersistenceControl ID="PersistenceControl1" runat="server" Key="id" Table="XT_CLGL_BASIC" />
    </form>
</body>
</html>
