﻿<%@ WebHandler Language="C#" Class="AjaxInfoProcess" %>

using System;
using System.Web;
using System.Web.SessionState;

public class AjaxInfoProcess : IHttpHandler,IRequiresSessionState
{

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
    
    /// <summary>
    /// 用户校验逻辑
    /// </summary>
    /// <param name="context"></param>
    public void ProcessRequest(HttpContext context)
    {
        string[] args = context.Request["params"].Split(',');
        string userid = args[0].ToUpper();
        //string userid = args[0];//区分大小写
        string password = args[1];
        string checkcode = args[2];
        string strReturn = string.Empty;

        if (checkcode.ToUpper() != HttpContext.Current.Session["CheckCode"].ToString())//验证码
        {
            strReturn = "验证码错误！";
        }
        else
        {
            Business.Admin.StUserOperation userManage = new Business.Admin.StUserOperation();
            System.Data.DataTable dtOut = userManage.GetLoginUserInfo(userid, password);

            if (dtOut.Rows.Count > 0)//登陆成功
            {
                System.Data.DataRow dr = dtOut.Rows[0];

                Business.Admin.StUserGroupHandle groupManage = new Business.Admin.StUserGroupHandle();
                string gid = groupManage.GetGroupId(dr["userid"].ToString());

                HttpContext.Current.Session.Add("UserId", dr["userid"]);
                HttpContext.Current.Session.Add("UserName", dr["user_name"]);
                HttpContext.Current.Session.Add("RealUserId", userid);
                HttpContext.Current.Session.Add("Password", password);
                HttpContext.Current.Session.Add("GroupId", gid);

                //添加考勤记录
                //Business.Admin.UserKaoQing.SaveUserLog(dr["userid"].ToString());
            }
            else //失败
            {
                strReturn = "密码不正确,请确认并重新输入正确的密码！";
            }
        }

        context.Response.Write(strReturn);
        
    }
}