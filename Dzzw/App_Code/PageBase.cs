﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Business;
using Business.FlowOperation;

/// <summary>
/// PageBase 的摘要说明
/// </summary>
public class PageBase : System.Web.UI.Page
{
    public PageBase()
    {
    }


    /// <summary>
    /// 设置页面的主题
    /// </summary>
    /// <param name="e"></param>
    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);
    }


    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        if (Session["UserId"] == null)
        {
            if ((Session["UserName"] == null || Session["UserName"].ToString() == String.Empty)
                 && Request.Url.ToString().ToLower().IndexOf(FormsAuthentication.LoginUrl.ToLower()) < 0)
            {
                Response.Redirect("http://" + Request.ServerVariables["HTTP_HOST"] + FormsAuthentication.LoginUrl);
            }
        }
    }

    #region 公有属性
    /// <summary>
    /// 用户名称，存贮在Session中
    /// </summary>
    protected string UserName
    {
        get
        {
            if (Session["UserName"] != null)
            {
                return Session["UserName"].ToString();
            }
            else
            {
                return string.Empty;
            }
        }
    }
    /// <summary>
    /// 用户名称（ＩＤ），存贮在Session中
    /// </summary>
    protected string UserId
    {
        get
        {
            if (Session["UserId"] != null)
            {
                return Session["UserId"].ToString();
            }
            else
            {
                return string.Empty;
            }
        }
    }
    /// <summary>
    /// 部门ＩＤ，存贮在Session中
    /// </summary>
    protected string GroupId
    {
        get
        {
            if (Session["GroupId"] != null)
            {
                return Session["GroupId"].ToString();
            }
            else
            {
                return string.Empty;
            }
        }
    }

    /// <summary>
    /// 部门ＩＤ，存贮在Session中
    /// </summary>
    protected string RealUserId
    {
        get
        {
            if (Session["RealUserId"] != null)
            {
                return Session["RealUserId"].ToString();
            }
            else
            {
                return string.Empty;
            }
        }
    }

    /// <summary>
    /// 用户密码
    /// </summary>
    protected string Password
    {
        get
        {
            if (Session["Password"] != null)
            {
                return Session["Password"].ToString();
            }
            else
            {
                return string.Empty;
            }
        }
    }
    #endregion

    public static void SetDefineText(ControlCollection Controls)
    {
        foreach (Control control in Controls)
        {
            if (control.HasControls())
            {
                SetDefineText(control.Controls);
            }
            else if (control is Label)
            {
                Label label = (Label)control;
                string comment = "";
                string defineText = "-1";
                if (!string.IsNullOrEmpty(label.ID))
                {
                    defineText = UserDefneText.GetDefineText(label.ID, out comment);
                }
                if (defineText != "-1")
                {
                    label.Text = defineText;
                }
            }
        }
    }



    #region 客户端IP
    /// <summary>
    /// 客户端IP 
    /// </summary>
    protected string HostIP
    {
        get
        {
            if (Request.PathInfo != null)
            {
                return Page.Request.UserHostAddress.ToString();
            }
            else
            {
                return string.Empty;
            }

        }

    }
    #endregion
}
