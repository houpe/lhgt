﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Business;
using Business.Admin;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ValideGetUser();
        if (!IsPostBack)
        {
            //Session.Clear();
        }
    }

    public void ValideGetUser()
    {
        string strParams = Request["params"];
        string strUrl = Request["url"];
        string strReturn = string.Empty;
        bool bHave = false;

        if (strUrl.Contains("@"))
        {
            strUrl = strUrl.Replace("@", "&");
        }

        if (Session["UserId"] != null)
        {
            if (Session["UserId"].ToString() == strParams)
            {
                Response.Redirect(strUrl);
                bHave = true;
            }
        }

        //如果没有对应的session则先获取session
        if (!string.IsNullOrEmpty(strParams) && !bHave)
        {
            //string[] args = strParams.Split(',');
            //string userid = args[0];
            //string password = args[1];

            HttpContext.Current.Session.Clear();

            StUserOperation userManage = new StUserOperation();
            System.Data.DataSet dsOut = userManage.ValideUser(strParams);

            if (dsOut.Tables[0].Rows.Count > 0)//登陆成功
            {
                System.Data.DataRow dr = dsOut.Tables[0].Rows[0];

                StUserGroupHandle stgTemp = new StUserGroupHandle();
                string gid = stgTemp.GetGroupId(dr["userid"].ToString());

                HttpContext.Current.Session.Add("UserId", dr["userid"]);
                HttpContext.Current.Session.Add("UserName", dr["user_name"]);
                HttpContext.Current.Session.Add("RealUserId", dr["login_name"]);
                HttpContext.Current.Session.Add("Password", dr["PASSWORD"]);
                HttpContext.Current.Session.Add("GroupId", gid);

                //添加考勤记录
                //Business.Admin.UserKaoQing.SaveUserLog(dr["userid"].ToString());

                //Response.Redirect("frmMainNew.aspx");
                Response.Redirect(strUrl);
            }
            else //失败
            {
                strReturn = string.Format("<script>alert('密码不正确,请确认并重新输入正确的密码！');window.close();</script>");
                Page.ClientScript.RegisterStartupScript(this.GetType(), "shibai", strReturn);
            }
        }
    }

}
