﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Business.Menu;

public partial class frmLeft : System.Web.UI.Page
{
    public string strHtml = string.Empty;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindPageData();
        }
    }

    protected void BindPageData()
    {
        XtMenuOperation appearOaMenu = new XtMenuOperation();
        DataTable dtParent = appearOaMenu.GenerateParentMenu();
        int i = 0;
        string userid =Convert.ToString( Session["userid"]);
        for (i=0;i<dtParent.Rows.Count;i++)
        {
            DataTable dtChild = appearOaMenu.GenerateChildMenu(dtParent.Rows[i]["menu_parent_id"].ToString(),userid);
            

            if (dtChild.Rows.Count > 0)
            {
                strHtml += string.Format(@"<div class='left_menu'>
            <div class='left_menu_bar' style='cursor: hand;' id='imgMenu{0}' onclick=""openOrCloseMenu('{0}')"">
                &nbsp;&nbsp;{1}</div><ul class='left_menu_ul' id='ulMenu{0}'>",
                i, dtParent.Rows[i]["menu_parent_desc"].ToString().Trim());
                for (int j = 0; j < dtChild.Rows.Count; j++)
                {
                    strHtml += string.Format(@"<li id='liMenu{0}{1}' class='left_menu_ul_li' style='cursor: hand' onmouseover='mouseMove({0},{1})'
                    onmouseout='mouseOut({0},{1})'><img src='{4}'/><a href='{2}' onclick='mouseMove({0},{1})' target='tabWin' title='{3}'>{3}</a></li>",
                    i,j,dtChild.Rows[j]["linkhref"],dtChild.Rows[j]["menu_child_desc"], dtChild.Rows[j]["overpic"]);
                }
            }
            strHtml += "</ul></div>";
        }
        hfdMenuValue.Value = Convert.ToString(i - 1);
    }
}
