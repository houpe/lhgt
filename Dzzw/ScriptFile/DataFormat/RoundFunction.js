﻿// JScript 文件---包含四舍五入的函数

//里面的两个参数：num就是要转换的数据。n为要转换的位数 
function cheng(num,n) 
{
    var dd=1; 
    var tempnum; 
    for(i=0;i<n;i++) 
    { 
        dd*=10; 
    } 
    tempnum=num*dd; 
    tempnum=Math.round(tempnum); 
    return tempnum;
} 


/* 
* ForDight(Dight,How):数值格式化函数，Dight要 
* 格式化的 数字，How要保留的小数位数。 
*/ 
function ForDight(Dight,How) 
{ 
    Dight = Math.round (Dight*Math.pow(10,How))/Math.pow(10,How); 
    return Dight; 
} 
