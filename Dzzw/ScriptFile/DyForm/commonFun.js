﻿var commFun = {};

commFun.getInnerText = function(e) {
    //若浏览器支持元素的innerText属性，则直接返回该属性
    if (e.innerText) { return e.innerText; }
    //不支持innerText属性时，用以下方法处理
    var t = "";
    //如果传入的是一个元素对象，则继续访问其子元素
    e = e.childNodes || e;
    //遍历子元素的所有子元素
    for (var i = 0; i < e.length; i++) {
        //若为文本元素，则累加到字符串t中。
        if (e[i].nodeType == 3) { t += e[i].nodeValue; }
        else if (e[i].nodeType == 1) { t += e[i].tagName == "BR" ? '\n' : commFun.getInnerText(e[i]); }
        //否则递归遍历元素的所有子节点
        else { t += commFun.getInnerText(e[i]); }
    }
    return t;
} 

commFun.GetLength = function(str) {
    ///<summary>获得字符串实际长度，中文2，英文1</summary>
    ///<param name="str">要获得长度的字符串</param>
    var realLength = 0, len = str.length, charCode = -1;
    for (var i = 0; i < len; i++) {
        charCode = str.charCodeAt(i);
        if (charCode >= 0 && charCode <= 128) realLength += 1;
        else realLength += 2;
    }
    return realLength;
};

//取ajax返回字符串
commFun.GetResultStr = function(result) {
    if (result && result.d) result = result.d;
    return result;
}

function GetAnonyMousFunBody(str) {
    var body = "";
    if (str) body = str.toString().replace(/^\s*function\s*\S+\s*\([^)]*\)\s*\{\s*|\s*\}\s*$/img, "");
    return body;
}

commFun.RefreshWin = function() {
    if (window.navigator.userAgent.indexOf("Firefox") >= 1)
        window.location.href = "";
    else
        document.location.reload();
}

//ie6 url get 参数问题
commFun.DoubleStr = function(str) {
    if ($.browser.msie && parseInt($.browser.version) <= 6) {
        //if (GetChinessCnt(str) % 2 != 0)
        //    str += '_';
        str = encodeURIComponent(str);
    }
    return str;
}

function GetManulDateStr(s) {
    s = RestoreDateTimeStr(s);
    var ss = s.split('-');
    if (ss.length != 3)
        ss = s.split('/');
    if (ss.length != 3)
        ss = s.split('.');
    //if (ss.length != 3)
    //    ss = s.split('_');

    var y = parseInt(ss[0], 10);
    var m = parseInt(ss[1], 10);
    var d = parseInt(ss[2], 10);
    var ndate = new Date(y, m - 1, d);
    if (s.match(/((^((1[8-9]\d{2})|([2-9]\d{3}))([-\/\._])(10|12|0?[13578])([-\/\._])(3[01]|[12][0-9]|0?[1-9])$)|(^((1[8-9]\d{2})|([2-9]\d{3}))([-\/\._])(11|0?[469])([-\/\._])(30|[12][0-9]|0?[1-9])$)|(^((1[8-9]\d{2})|([2-9]\d{3}))([-\/\._])(0?2)([-\/\._])(2[0-8]|1[0-9]|0?[1-9])$)|(^([2468][048]00)([-\/\._])(0?2)([-\/\._])(29)$)|(^([3579][26]00)([-\/\._])(0?2)([-\/\._])(29)$)|(^([1][89][0][48])([-\/\._])(0?2)([-\/\._])(29)$)|(^([2-9][0-9][0][48])([-\/\._])(0?2)([-\/\._])(29)$)|(^([1][89][2468][048])([-\/\._])(0?2)([-\/\._])(29)$)|(^([2-9][0-9][2468][048])([-\/\._])(0?2)([-\/\._])(29)$)|(^([1][89][13579][26])([-\/\._])(0?2)([-\/\._])(29)$)|(^([2-9][0-9][13579][26])([-\/\._])(0?2)([-\/\._])(29)$))/))
        return y + "-" + m + "-" + d;

    return "";
}

function GetManualDateTimeStr(s) {
    s = RestoreDateTimeStr(s);
    var dt = s.split(" ");
    var sdate = "", stime = "";
    for (var i = 0; i < dt.length; i++) {
        if (dt[i]) {
            if (!sdate)
                sdate = dt[i];
            else if (!stime)
                stime = dt[i];
            else
                return "";
        }
    }
    sdate = GetManulDateStr(sdate);
    if (!sdate)
        return "";

    var ss = sdate.split('-');
    var y = parseInt(ss[0], 10);
    var m = parseInt(ss[1], 10);
    var d = parseInt(ss[2], 10);

    var hour = 0, minute = 0, second = 0;
    var tt = stime.split(":");
    hour = parseInt(tt[0], 10);
    minute = parseInt(tt[1], 10);
    if (tt.length > 2)
        second = parseInt(tt[2], 10);
    var ntime = new Date(y, m, d, hour, minute, second);
    //if (stime.match(/^(([1-9]{1})|([0-1][0-9])|([1-2][0-3])):([0-5][0-9])$/))
    if (stime.match(/^([0-1]?[0-9]|2[0-3]):([0-5]?[0-9])(:[0-5]?[0-9])?$/))
        return sdate + " " + hour + ":" + minute;
    return "";
}

function RestoreDateTimeStr(tstr) {
    var rstr = tstr.replace(/\s+/, " ");
    var pos = rstr.indexOf("年");
    for (var i = 0; i <= pos; i++) {
        var ic = rstr.substring(i, i+1);
        switch (ic) {
            case "年":
                rstr = rstr.replace(/年/, "-");
                break;
            default:
                rstr = rstr.replace(ic, RestoreNum(ic));
                break;
        }
    }
    var pos2 = rstr.indexOf("月");
    var pos3 = rstr.indexOf("日");
    var poshour = rstr.indexOf("时");
    var posminute = rstr.indexOf("分");
    if (pos3 > pos2 + 1 && pos2 > pos + 1 && pos > 0) {
        var smon = rstr.substring(pos + 1, pos2);
        var sday = rstr.substring(pos2 + 1, pos3);
        if (poshour > pos3 + 2 && posminute > poshour + 1) {
            var shour = rstr.substring(pos3 + 2, poshour);
            var sminute = rstr.substring(poshour + 1, posminute);
            rstr = rstr.replace(shour + "时", RestoreCount(shour) + ":");
            rstr = rstr.replace(sminute + "分", RestoreCount(sminute));
        }
        rstr = rstr.replace(smon + "月", RestoreCount(smon) + "-");
        rstr = rstr.replace(sday + "日", RestoreCount(sday));
    }
    return rstr;
}

function RestoreCount(nstr) {
    var dnum = 0;
    var needtenth = false;
    var needlas = true;
    if (nstr.length == 2) {
        needtenth = true;
        if (nstr.substring(1, 2) == "十") {
            needlas = false;
        }
        if (nstr.substring(0, 1) == "十") {
            needtenth = false;
            dnum = 10;
        }
    } else if (nstr.length == 3) {
        needtenth = true;
    }
    if (needtenth) dnum = RestoreNum(nstr.substring(0, 1)) * 10;
    if (needlas) dnum = RestoreNum(nstr.substring(nstr.length - 1, nstr.length)) * 1 + dnum;
    return dnum;
}

function RestoreNum(n) {
    var rstr = n;
    switch (n) {
        case "○":
            rstr = rstr.replace(/○/, "0");
            break;
        case "一":
            rstr = rstr.replace(/一/, "1");
            break;
        case "二":
            rstr = rstr.replace(/二/, "2");
            break;
        case "三":
            rstr = rstr.replace(/三/, "3");
            break;
        case "四":
            rstr = rstr.replace(/四/, "4");
            break;
        case "五":
            rstr = rstr.replace(/五/, "5");
            break;
        case "六":
            rstr = rstr.replace(/六/, "6");
            break;
        case "七":
            rstr = rstr.replace(/七/, "7");
            break;
        case "八":
            rstr = rstr.replace(/八/, "8");
            break;
        case "九":
            rstr = rstr.replace(/九/, "9");
            break;
        case "十":
            rstr = rstr.replace(/十/, "10");
            break;
    }
    return rstr;
}

function OpenNewWin(url, target, w, h, wrapId, senderId, dlgops) {
    var obj = document.getElementById(senderId);
    var wintitle = $(obj).attr("wintitle");
    var wmaximizable = $(obj).attr("wmaximizable") || (dlgops && dlgops.wmaximizable);
    var initMax = $(obj).attr("initMax") == "1" || (dlgops && dlgops.initMax);
    wintitle = wintitle ? wintitle : ((dlgops && dlgops.title) ? dlgops.title : "");
    var x = 0, y = 0;
    //if (!initMax) 
    {
        if (!w) { w = GetNumber($(obj).attr("hrefwidth"), 0); w = w ? w : 600; }
        if (!h) { h = GetNumber($(obj).attr("hrefheight"), 0); h = h ? h : 400; }
        x = GetNumber($(obj).attr("wleft"), 0);  y = GetNumber($(obj).attr("wtop"),0);
        if (!x && $(obj).attr("wleft") != "0") {
            x = ($(window).width() - w) / 2;
            x += eval($(window).scrollLeft()) + 3;
        }
        if (!y && $(obj).attr("wtop") != "0") {
            y = ($(window).height() - h) / 2;
            y += eval($(window).scrollTop()) + 3;
        }
        if (x < 0) x = 3;
        if (y < 0) y = 3;
    } 
    if (!wrapId) wrapId = "target2";

    if (target == 1) {
        var win = window.open(url, wintitle, "scrollbars=yes,status=no,toolbar=no,location=no,directories=no,menubar=no,resizable=yes,width=" + w + ",height=" + h + ",left=" + x + ",top=" + y, true);
        if (initMax) {
            win.moveTo(0, 0);
            win.resizeTo(window.screen.availWidth, window.screen.availHeight);
        }
        return false;
    } else if (target == 2) {
        if (document.getElementById(wrapId)) {//$("#target2").length == 0
            //$("#target2 > iframe").eq(0).attr('src', obj.href);
            $("#" + wrapId).remove();
        }

        var content = '<iframe scrolling="' + ((dlgops && !dlgops.scrolling) ? 'no' : 'auto') + '" frameborder="0"  src="' + url + '" style="width:100%;height:100%;"></iframe>';//<div style="overflow:hidden;width:'+(w-14)+'px;height:'+(h-35)+'px;"></div>
        var temp = $('<div id="' + wrapId + '" iconCls="" title="&nbsp;">' + content + '</div>');
        $('body').append(temp);
        //if (initMax) {
        //    x = y = 0;
        //    w = $(window).width(); h = $(window).height();
        //}
        var winOps = { left: x, top: y, width: w, height: h, modal: true, maximizable: wmaximizable ? true : false, minimizable: false, collapsible: false, //resizable: false,
            onClose: function() { },
            onMaximize: function() {
                var ops = $('#' + wrapId).panel('options');
                ops.fit = false;
                $('#' + wrapId).window('resize', { width: $(window).width(), height: $(window).height() });
            }
        };
        if (wintitle) winOps.title = wintitle;
        if (dlgops) $.extend(winOps, dlgops);
        $('#' + wrapId).window(winOps);
        if (initMax) $('#' + wrapId).window('maximize');
    }
}

function GetNumber(str, defval) {
    var num = parseFloat(str, 10);
    if (!isNaN(num)) return num;
    
    if (defval) return defval;
    return 0;
}

//检查flash
function flashChecker() {
    var hasFlash = 0;         //是否安装了flash
    var flashVersion = 0; //flash版本
    var isIE = navigator.userAgent.toLowerCase().indexOf("msie") >= 0;

    if (isIE) {
        var swf = null;
        try {
            swf = new ActiveXObject('ShockwaveFlash.ShockwaveFlash');
        } catch (e) {
        }
        if (swf) {
            hasFlash = 1;
            VSwf = swf.GetVariable("$version");
            flashVersion = parseInt(VSwf.split(" ")[1].split(",")[0]);
        }
    } else {
        if (navigator.plugins && navigator.plugins.length > 0) {
            var swf = navigator.plugins["Shockwave Flash"];
            if (swf) {
                hasFlash = 1;
                var words = swf.description.split(" ");
                for (var i = 0; i < words.length; ++i) {
                    if (isNaN(parseInt(words[i]))) continue;
                    flashVersion = parseInt(words[i]);
                }
            }
        }
    }
    return { f: hasFlash, v: flashVersion };
}

function isAcrobatPluginInstall() {
    var flag = false;
    if (navigator.plugins && navigator.plugins.length) {
        for (x = 0; x < navigator.plugins.length; x++) {
            if (navigator.plugins[x].name == "Adobe Acrobat")
                flag = true;
        }
    }
    else if (window.ActiveXObject) {
        for (x = 2; x < 19; x++) {
            try {
                oAcro = eval("new ActiveXObject('PDF.PdfCtrl." + x + "');");
                if (oAcro) {
                    flag = true;
                }
            } catch (e) {
                flag = false;
            }
        }
        if (!flag) {
            try {
                oAcro4 = new ActiveXObject("PDF.PdfCtrl.1");
                if (oAcro4)
                    flag = true;
            } catch (e) {
                flag = false;
            }
        }
        if (!flag) {
            try {
                oAcro7 = new ActiveXObject("AcroPDF.PDF.1");
                if (oAcro7)
                    flag = true;
            } catch (e) {
                flag = false;

            }
        }
    }
    return flag;
}


function TrimStr(str) {
    return str.replace(/(^\s*)|(\s*$)/g, "");
}

//日期设置清除事件
function SetDateBox(obj) {
    obj.next().find(".combo-arrow:eq(0)").click(function() {
        var clear = $(".panel:visible a[clear='1']");
        clear.one("click", function() {
            obj.datebox("clear");
            $(".panel:visible a.datebox-close").click();
        });
    });
}

//提交前替换符号
function replaceSymbol(tValue) {
    if (tValue) {
        tValue = tValue.replace(/\\/g, '\\\\')
        tValue = tValue.replace(/\&/g, '&amp;');
        tValue = tValue.replace(/\</g, '&lt;');
        tValue = tValue.replace(/\>/g, '&gt;');
        tValue = tValue.replace(/\"/g, "&quot;");
        tValue = tValue.replace(/\'/g, "&#039;");
    }
    return tValue;
}

function IsIdCodeValidate(id) {
    var sid = id;
    var IDReg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/; //15或18位
    if (!IDReg.test(sid)) {
        return false;
    }
    if (sid.length == 18) {//18位
        var ysid = sid;
        sid = sid.substring(0, 17);
        var jiaquan = new Array(7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2);
        var sigma = 0;
        for (var i = 0; i < 17; i++) {
            sigma += parseInt(parseInt(sid.substring(i, i + 1)) * jiaquan[i]); //取每一位上的数字和对应的权相乘并求和     
        }
        var jiaoyan = new Array('1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2');

        if (sid + jiaoyan[(sigma % 11)] == ysid) { return true; }
        else { return false; }
    } else {//15位
        var sReg;
        if ((parseInt(sid.substr(6, 2)) + 1900) % 4 == 0 || ((parseInt(sid.substr(6, 2)) + 1900) % 100 == 0 && (parseInt(sid.substr(6, 2)) + 1900) % 4 == 0)) {
            sReg = /^[1-9][0-9]{5}[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|[1-2][0-9]))[0-9]{3}$/; //测试出生日期的合法性 
        } else {
            sReg = /^[1-9][0-9]{5}[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|1[0-9]|2[0-8]))[0-9]{3}$/; //测试出生日期的合法性 
        }
        if (sReg.test(sid)) return true;
        else return false;
    }
}

function OpenWindow(url, w, h) {
    if (!w) w = 800;
    if (!h) h = 600;
    var x = ($(window).width() - w) / 2;
    var y = ($(window).height() - h) / 2;
    x += eval($(window).scrollLeft()) + 3;
    y += eval($(window).scrollTop()) + 3;
    if (x < 0) x = 3;
    if (y < 0) y = 3;
    window.open(url, "业务督办", "dependent=yes,scrollbars=1,toolbar=no,location=no,directories=no,status=yes,menubar=no,directories=no,location=no,resizable=yes,width=" + w + ",height=" + h + ",left=" + x + ",top=" + y, true);
}