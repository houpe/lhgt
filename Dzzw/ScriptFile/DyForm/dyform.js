﻿var GlobalDgGridLastIndex;
//保存--%>
        function getSelectedText() {
            if (window.getSelection) {
                // This technique is the most likely to be standardized. 
                // getSelection() returns a Selection object, which we do not document. 
                return window.getSelection().toString();
            }
            else if (document.getSelection) {
                // This is an older, simpler technique that returns a string 
                return document.getSelection();
            }
            else if (document.selection) {
                // This is the IE-specific technique. 
                // We do not document the IE selection property or TextRange objects. 
                return document.selection.createRange().text;
            }
        }
        $(function() {
            $(document).append("<div id='mm' class='easyui-menu' style='width: 120px; display: none'><div onclick='exp()'>导出</div></div>");
            $(document).bind('contextmenu', function(e) {
                var elm = arguments[0].target || e.srcElement;
                var a = getSelectedText();

                //   return true;
                if (elm.tagName == "TEXTAREA" || elm.tagName == "INPUT"
        || (a != null && a != "") && (elm.tagName == "TD" || elm.tagName == "DIV" || elm.tagName == "SPAN" || elm.tagName == "A")) return true;

                menuLeft = e.pageX;
                menuTop = e.pageY;
                $('#mm').menu('show', {
                    left: e.pageX,
                    top: e.pageY

                });
                /*why*/

                return false;
            });
        });
        var CanSave = true;
        var isSaved = false;
        var needSave = false;//取全部数据，但是根据此变量判断是否保存
        //var saveids = [];
        function SaveAll(showMsg, funName, url,isReload) {
            needSave = false;
            //alert("isSaved:" + isSaved + ";CanSave:" + CanSave);
            if (isSaved) {
                //showMsgBox("数据已经保存，无需再次保存！", false);
                return;
            }
            if (CanSave == false) {
                showMsgBox("未通过验证，不允许保存！", true);
                return;
            }
           
            var rfields = CheckRequired();
            if (rfields) {
                showMsgBox("请填写" + rfields, true);
                return;
            }
            var sendObj = {};
            sendObj.iid = $("#hiid").val(); //"<%=iid%>";
            sendObj.fid = $("#hfid").val(); //"<%=fid%>";
            sendObj.step = $("#hsctlid").val(); //"<%=sctlid%>";
            sendObj.userid = $("#huserid").val(); //"<%=userid%>";
            sendObj.input_index = $("#hinput_index").val(); //"<%=input_index%>";
            sendObj.wiid = $("#hwiid").val(); //"<%=wiid%>";
            sendObj.recordkey = $("#hUserPara").val(); //"<%=UserPara%>"; //这个参数是[][]|[][]形式的组合
            sendObj.strElements = getSimpleEle();
            sendObj.gridElements = getDataGrids();
            if (sendObj.gridElements != "") needSave = true;
            if (!needSave) {//sendObj.gridElements == "" && sendObj.strElements == ""
                if (showMsg) showMsgBox("您没有修改过数据，无需保存！", false);
                return;
            }

            if (!signatureLoaded) {
                alert("页面未加载完成，无法保存！");
                return;
            }
            //alert("保存"+sendObj.strElements);
            sendObj.firstWriteIds = "";
            $.ajax({
                type: "POST",
                contentType: "application/json",
                async: false,
                cache: false,
                url: "http://localhost/webservice2/WebFormService.asmx/SaveDyForm",
                data: JSON.stringify(sendObj),
                dataType: 'json',
                //beforeSend: showWait,
                //complete: hiddenWait,
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    if (showMsg) {
                        if (XMLHttpRequest) alert("a:" + JSON.stringify(XMLHttpRequest));
                        if (textStatus) alert("b:" + JSON.stringify(textStatus));
                        if (errorThrown) alert("c:" + JSON.stringify(errorThrown));
                    }
                },
                success: function(result) {
                    result = GetResultStr(result);
                    if (result == "表单保存成功！") {
                        var obj = dygrids;
                        for (var i = 0; i < obj.length; i++) {
                            var ig = obj[i][0];
                            $(ig).datagrid('acceptChanges');
                        }
                        isSaved = true;
                        saveEdit = false;
                        saveOk = true;
                        if (showMsg != false) showMsgBox(result);

                        if (funName != null && funName != "") {
                            var strfunName = funName + "();";
                            eval(strfunName);
                        }
                        if (url != null && url != "") {
                            var strOpenUrl = "window.open('" + url + "');";
                            eval(strOpenUrl);
                        }
                        if (isReload == true || isReload == null) {
                            if (window.navigator.userAgent.indexOf("Firefox") >= 1)
                                document.location.href = document.location.href;//document.location.href = "";
                            else
                                document.location.reload();
                        }
                    }
                    else {
                        var msg = "表单保存出错。" + result;
                        if (showMsg != false) showMsgBox(msg, true);
                        return msg;
                    }
                }
            });
        }

        function GetResultStr(result) {
            if (result && result.d) result = result.d;
            return result;
        }
        
        //暂时放在页面上面
        //inputId 任意一个可绑定更新数据集的表单控件的Id
        function deleteOperator(dsId, factor, showMsg, funName, url) {
            var sendObj = {};
            sendObj.iid = $("#hiid").val(); //"<%=iid%>";
            sendObj.fid = $("#hfid").val(); //"<%=fid%>";
            sendObj.step = $("#hsctlid").val(); //"<%=sctlid%>";
            sendObj.userid = $("#huserid").val(); //"<%=userid%>";
            sendObj.input_index = $("#hinput_index").val(); //"<%=input_index%>";
            sendObj.wiid = $("#hwiid").val(); //"<%=wiid%>";
            sendObj.recordkey = $("#hUserPara").val(); //"<%=UserPara%>"; //这个参数是[][]|[][]形式的组合
            sendObj.strElements = getSimpleEle(dsId, factor); //getDeleteById(dsId, factor); //不增加函数的参数个数，所以借用这个参数
            sendObj.gridElements = "";
            sendObj.firstWriteIds = "";
            $.ajax({
                type: "POST",
                contentType: "application/json",
                async: false,
                cache: false,
                url: "http://localhost/webservice2/WebFormService.asmx/SaveDyForm",
                data: JSON.stringify(sendObj),
                dataType: 'json',
                //beforeSend: showWait,
                //complete: hiddenWait,
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    if (showMsg) {
                        if (XMLHttpRequest) alert("a:" + JSON.stringify(XMLHttpRequest));
                        if (textStatus) alert("b:" + JSON.stringify(textStatus));
                        if (errorThrown) alert("c:" + JSON.stringify(errorThrown));
                    }
                },
                success: function(result) {
                    result = GetResultStr(result);
                    if (result == "表单保存成功！") {
                        var obj = dygrids;
                        for (var i = 0; i < obj.length; i++) {
                            var ig = obj[i][0];
                            $(ig).datagrid('acceptChanges');
                        }
                        isSaved = true;
                        saveEdit = false;
                        saveOk = true;
                        if (showMsg != false) showMsgBox("删除数据成功！");

                        if (funName != null && funName != "") {
                            var strfunName = funName + "();";
                            eval(strfunName);
                        }
                        if (url != null && url != "") {
                            var strOpenUrl = "window.open('" + url + "');";
                            eval(strOpenUrl);
                        }
                        document.location.reload();
                    }
                    else {
                        var msg = "删除操作出错：" + result;
                        if (showMsg != false) showMsgBox(msg, true);
                        return msg;
                    }
                }
            });
        }
        
        //传参数之后表示是构造删除数据
        function getSimpleEle(dsId, factor) {
//            if (!dycombos) return "";//说明页面还未加载完成
            var strResult = "";
            if (dsId && dsId != "") {
                if (!factor || factor == "") factor = " ";
                strResult += '{"id":"' + dsId + '",';
                strResult += '"input_value":"",';
                strResult += '"deleteFactor":"' + replaceSymbol(factor) + '",';
                strResult += '"type":"1"}';
            }
            $("[dytype]").each(function() {
                //if (!saveids.in_array(this.id)) return;
                var oldval = $(this).attr("oldval");
                if (dytype == 41)
                    oldval = jQuery.data($(this), "datas");
                var dytype = $(this).attr("dytype");
                if (dytype == 18 || dytype == 35) return;
                var newVal = formCtrlValOp(this.id);
                //SaveDefaultValue
                if (newVal == oldval) {
                    var initval = $(this).attr("defVal");
                    if (initval && initval.length > 0) newVal = initval;
                }

                var change = true;
                if (dytype == 19) { if (timeEqual(newVal, oldval)) change = false; }
                else if (newVal == oldval) { change = false; }
                else if (dytype == 34) { if (newVal == 0 && oldval == "") change = false; }
                if (change) needSave = true;
                //if (!change) return;
                if (strResult != "") strResult += ',';
                strResult += '{"id":"' + this.id + '",';
                strResult += '"input_value":"' + replaceSymbol(newVal) + '",';
                strResult += '"type":"' + dytype + '"}';
            });
            if (strResult == "") return "";
            return '[' + strResult + ']';
        }
        
        function getDataGrids() {
            var strResult = '[';
            var obj = dygrids; //$("table:hidden");
            if (obj.length == 0) return "";

            EndEditDataGrid();
            
            var haschange = false;
            for (var i = 0; i < obj.length; i++) {
                var ig = obj[i][0];
                var gid = ig.id;
                if (gid == "") continue;
                var rows;
                if ($(ig).attr("imped"))
                    rows = $(ig).datagrid('getData').rows; 
                else
                    rows = $(ig).datagrid('getChanges');
                var del = $.data(ig, "datagrid").deletedRows;
                var opts = $.data(ig, "datagrid").options;
                if (!opts.idField) continue; 
                var dataids = "";
                var ids = "";
                var values = '';
                var klst = [];
                for (var k = 0; k < rows.length; k++) {
                    if (k == 0) {
                        for (var key in rows[k]) {
                            if (key == 'rn') continue;
                            if (ids != "") ids += "："
                            ids += key;
                            klst.push(key);
                        }
                    }
                    haschange = true;
                    var eachval = "";
                    var first = true;
                    var hasvalue = false;
                    var idval = "";
                    for (var oder = 0; oder < klst.length; oder++) {
                        key = klst[oder];
                        for (var rk in rows[k]) {
                            if (key.toLowerCase() == rk.toLowerCase())
                                key = rk;
                        }
                        if (first == false) eachval += '|$|';
                        eachval += rows[k][key];
                        first = false;
                        if (rows[k][key] != "") hasvalue = true;
                        if (key.toLowerCase() == opts.idField.toLowerCase()) idval = rows[k][key];
                    }

                    if (hasvalue == false && idval == "") continue;

                    if (values != "") {
                        values += '-||';
                        dataids += "：";
                    }
                    values += replaceSymbol(eachval);

                    //var val = rows[k][opts.idField];
                    //if (!val) val = rows[k][opts.idField.toLowerCase()];
                    if (del.indexOf(rows[k]) >= 0)
                        dataids += "[[" + idval + "]]";
                    else dataids += idval;
                }

                if (values == "") continue;
                
                if (strResult != "") strResult += ',';
                strResult += '{"gid":"' + gid + '",';
                
                strResult += '"dataids":"' + dataids + '",';

                strResult += '"ids":"' + ids + '",';
                strResult += '"ctlIds":"",';
                strResult += '"types":"",';
                strResult += '"values":"' + values + '"}';
            }
            strResult += ']';
            if (haschange == false) return "";
            return strResult;
        }

        function getDeleteById(dsId, factor) {
            var strResult = "";
            //var dyObj = $("#" + inputId);
            var dytype = 1; //dyObj.attr("dytype");
            if (!factor || factor == "") factor = " ";
            strResult += '{"id":"' + dsId + '",';
            strResult += '"input_value":"",';
            strResult += '"deleteFactor":"' + factor + '",';
            strResult += '"type":"'+dytype+'"}';
            
            if (strResult == "") return "";
            return '[' + strResult + ']';
        }
    
//显示提示框 取值赋值函数--%>
    
        function showMsgBox(msg, isErr) {
            if (isErr)
                $("#msgBox").css({ "background-color": "red" });
            else
                $("#msgBox").css({ "background-color": "Green" });

            var t = $(window).scrollTop();

            $("#msgBox").text(msg).show().css({ "top": t });//.focus();

            //setTimeout(hideMsgBox, 5000);
        }
        
//var userValue = "";
//用于设计表单时的脚本调用,取值或赋值,type为1时为赋值
//field 当为数据表的时候，此参数为要获取列的读取字段;当为列控件时，为“列序号__列控件ID”
//caller 调用此函数的dom元素，当为数据表时需要此参数
function formCtrlValOp(id, type, field, caller, userValue) {
    //强制未通过验证情况下，不允许赋值
    if (CanSave == false && type == 1) { return false; }
    
    var dyObj = $("#" + id);
    var dyCat = dyObj.attr("dytype");
    if (type == 1) {//赋值
        if (dyCat == 12) dyObj.combobox('setValue', userValue);
        else if (dyCat == 38) {
            if (dyObj.attr("multiple"))
                dyObj.combotree('setValues', userValue.split(dyObj.attr('separator')));
            else
                dyObj.combotree('setValue', userValue);
        } else if (dyCat == 17) {
            var _69b = dyObj.tree("getChecked");
            for (var i = 0; i < _69b.length; i++) {
                var node = dyObj.tree("find", _69b[i].id);
                if (node) dyObj.tree("uncheck", node.target);
            }
            var vals = [];
            if (dyObj.attr("multiple"))
                vals = userValue.split(dyObj.attr('separator'));
            else vals[0] = userValue;
            for (var i = 0; i < vals.length; i++) {
                var node = dyObj.tree("find", vals[i]);
                if (node) {
                    dyObj.tree("check", node.target);
                    dyObj.tree("select", node.target);
                }
            }
        }
        else if (dyCat == 37) dyObj.val(userValue);
        else if (dyCat == 26) {
            dyObj.val(userValue); //.trigger("change")
            FormatUserInput(dyObj);
            ValidateUserInput(dyObj);
        } else if (dyCat == 19) {
            var tlen = userValue.split(':').length;
            if (tlen == 2)
                userValue += ':00';
            //var objinput = dyObj.next().find('input');
            //objinput.removeAttr("needValidate");
            //objinput.trigger("blur");
            dyObj.val(userValue);
            if (dyObj.attr('HaveTime') == "1") {//if (dyObj.attr('class').indexOf('easyui-datetimebox') >= 0) {
                dyObj.datetimebox('setValue', FormatDateTime(dyObj[0]));//userValue
            } else {
                dyObj.datebox('setValue', FormatDateTime(dyObj[0]));
            }
        } else if (dyCat == 34) {//单选赋值其值格式见后台组合方式
            if (dyObj.attr('on') == userValue)
                dyObj.attr("checked", "checked");
            else
                dyObj.attr("checked", "");
        } else if (dyCat == 41) {
            var oEditor;
            if (typeof (FCKeditorAPI) != 'undefined') {
                oEditor = FCKeditorAPI.GetInstance(id);
                oEditor.SetHTML(userValue);
            }
        } else {
            var parentobj = $(caller).parents("tr[datagrid-row-index]");
            var rowIndex = parentobj[0].rowIndex;
            var ig = dyObj[0];
            var opts = $.data(ig, "datagrid").options;
            var tr = opts.editConfig.getTr(ig, rowIndex);
            if (tr.hasClass("datagrid-row-editing")) {
                tr.find("div.datagrid-editable").each(function() {
                    var _433 = $(this).parent().attr("field");
                    if (_433 != field) return;
                    var ed = $.data(this, "datagrid.editor");
                    ed.actions.setValue(ed.target, userValue);
                });
            }
        }
    }
    else {//取值
        if (dyObj.attr("inited") == "0")
            return dyObj.attr("oldval");

        var value = "";
        switch (dyCat) {
            case "12":
                value = dyObj.combobox('getValue');
                break;
            case "38":
                if (dyObj.attr("multiple"))
                    value = dyObj.combotree('getValues').join(dyObj.attr('separator'));
                else
                    value = dyObj.combotree('getValue');
                break;
            case "17":
                if (dyObj.attr("multiple") && dyObj.tree('options').checkbox) {
                    var sep = dyObj.attr("separator");
                    var cks = dyObj.tree('getChecked');
                    for (var i = 0; i < cks.length; i++) {
                        if (value) value += sep;
                        value += cks[i].id;
                    }
                } else {
                    var sel = dyObj.tree('getSelected');
                    if (sel) value = sel.id;
                }
                break;
            case "37":
                value = dyObj.val();
                if (!value) value = "";
                break;
            case "26":
                var fstr = dyObj.attr("formatstr");
                if (fstr != "" && fstr != undefined)
                    value = dyObj.attr("realval");
                else
                    value = dyObj.val();
                break;
            case "40":
                if (dyObj.attr("ischange"))
                    value = dyObj.attr("newval");
                else
                    value = dyObj.attr("oldval");
                break;
            case "19":
                var str = dyObj.attr("date");
                var t = dyObj.attr("time");
                if (!str) str = t;
                if (!str) str = dyObj.datetimebox("getValue");
                value = str;
                break;
            case "34":
                if (dyObj.attr("checked") == "checked" || dyObj.attr("checked"))
                    value = dyObj.attr('on');
                else
                    value = '0';
                var rname = dyObj.attr("name");
                if (rname && rname.length > 0) {
                    var chked = $("input[name=" + rname + "]:checked");
                    if (chked.length) value = chked.attr('on');
                }
                break;
            case "35":
                value = GetTbGridCtrlValue(id, field, caller);
                break;
            case "41":
                //var editor = eval('CKEDITOR.instances.' + id);
                //value = editor.getData();
                var oEditor;
                if (typeof (FCKeditorAPI) != 'undefined') {
                    oEditor = FCKeditorAPI.GetInstance(id);
                    value = oEditor.GetXHTML();
                }
                break;
            default:
                var parentobj = $(caller).parents("tr[datagrid-row-index]");
                var rowIndex = parentobj[0].rowIndex;
                var ig = dyObj[0];
                var opts = $.data(ig, "datagrid").options;
                var row = opts.editConfig.getRow(ig, rowIndex);
                value = row[field];
                var tr = opts.editConfig.getTr(ig, rowIndex);
                if (tr.hasClass("datagrid-row-editing")) {
                    tr.find("div.datagrid-editable").each(function() {
                        var _433 = $(this).parent().attr("field");
                        if (_433 == field) {
                            var ed = $.data(this, "datagrid.editor");
                            value = ed.actions.getValue(ed.target);
                        }
                    });
                }
                break;
        }     
        return value;
    }
}
    
//    表单定义所需的函数--%>
    
        function GetFormatStr(value, itype, format) {
            if (!value) return "";
            var sendObj = {};
            sendObj.str = value;
            sendObj.iType = itype;
            sendObj.FormatStr = format;
            var restr = value;
            $.ajax({
                type: "POST",
                contentType: "application/json",
                async: false,
                cache: false,
                url: "http://localhost/webservice2/WebFormService.asmx/FormatField",
                data: JSON.stringify(sendObj),
                dataType: 'json',
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    alert("格式化数据时发生远程请求错误！");
                },
                success: function(result) {
                    result = GetResultStr(result);
                    restr = result;
                }
            });
            return restr;
        }

        function FormatUserInput(iobj) {
            if (CanSave == false && iobj.id != CanNotSaveId) { return false; }
            VChanged = true;
            var fStr = $(iobj).attr("formatstr");
            if (!fStr) return;
            var value = $(iobj).val();
            var formatted = GetFormatStr(value, "TextBox", $(iobj).attr("formatstr"));
            $(iobj).attr("realval", value);
            $(iobj).val(formatted);
        }

        function FormatDateTime(iobj) {
            //$(iobj).trigger("change");

            var objinput = $(iobj).next().find('input');
            objinput.removeAttr("needValidate");
            $('#msgBox').hide();
            
            var fStr = $(iobj).attr("formatstr");
            var value = $(iobj).val();
            if (!fStr) return value;
            
            var clss = $(iobj).attr('class');
            var itype = "TextBox";
            if ($(iobj).attr('HaveTime') == "1") {//if (clss.indexOf('easyui-datetimebox') >= 0) {
                itype = "DateTimeBox";
                $(iobj).attr("time", value);
            } else if ($(iobj).attr('HaveTime') == "0") {//else if (clss.indexOf('easyui-datebox') >= 0) {
                itype = "DateBox";
                $(iobj).attr("date", value);
            }

            var formatted = GetFormatStr(value, itype, fStr);

            return formatted;
        }

        var CanNotSaveId = "";
        var InValidateMsg = "";
        var VChanged = true; 
        function ValidateUserInput(iobj, way, action) {
            if (VChanged == false) return;
            if (!way) way = $(iobj).attr("validateway");
            if (!action) action = $(iobj).attr("validateaction");
            //alert(way + "   " + action);
            $("#msgBox").hide();
            CanSave = true;
            var str = "";
            if ($(iobj).attr("formatstr")) //!= ""
                str = $(iobj).attr("realval");
            else
                str = $(iobj).val();

            //if (CanNotSaveId == $(iobj).attr("id") && $(iobj).attr("prevtest") == str) return;

            var result = true;
            var patt1 = ""; //new RegExp("");js斜线
            var message = "";
            if (way == 1) {
                patt1 = /^-?\d+$/;
                message = "请输入整数！";
            } else if (way == 2) {
                patt1 = /^-?\d+(\.\d+)?$/;
                message = "请输入数字！";
            } else if (way == 3) {
                patt1 = /^(\(?0[1-9]\d{1,3}\)?[ -]?)?[2-9]\d{2,3}[ -]?\d{4}$/;
                message = "请输入正确的固定电话号码！";
            } else if (way == 4) {
                patt1 = /(^\d{11}$)|(^(\(?0[1-9]\d{1,3}\)?[ -]?)?[2-9]\d{2,3}[ -]?\d{4}$)/;
                message = "请输入正确的固定电话号码或手机号码！";
            } else if (way == 5) {
                patt1 = /^\d{11}$/;
                message = "请输入正确的手机号码！";
            } else if (way == 6) {
                patt1 = /^\d(9|[0-7])\d{4}$/;
                message = "请输入正确的邮政编码！";
            } else if (way == 7) {
                patt1 = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
                message = "请输入正确的电子邮件地址！";
            } else if (way == 8) {
                //patt1 = /^[1-8]\d{5}((18)|(19)|(20))?\d{2}[0-1]\d[0-3]\d{4}[\dx]?$/i;  //(^\d{15}$)|(^\d{17}(\d|x|X)$)
                result = IsIdCodeValidate(str);
                message = "请输入正确的身份证号码！";
            } else if (way == 9) {
                patt1 = jQuery.data($(iobj)[0], 'regexp');
                message = "数据校验未通过！";
            } else if (way == 99) {
                patt1 = /.+/;
                message = "请输入内容！";
            }
            if (patt1 != "" && str) { //和Required分开
                result = patt1.test(str);
                //$(iobj).attr("prevtest", str);
            }
            if (result != true) {
                //message += str + result+way;
                showMsgBox(message, true);
                if (action == 1) {
                    CanSave = false;
                    VChanged = false;
                    CanNotSaveId = $(iobj).attr("id");
                    InValidateMsg = message;
                    //$(iobj).focus();
                    window.setTimeout("showMsgBox('" + InValidateMsg + "', true);$('#" + CanNotSaveId + "').focus();", 100);
//                    $(iobj).bind("blur", function() {
//                        window.setTimeout("showMsgBox('" + message + "', true);$('#" + CanNotSaveId + "').focus();", 100);
                    //                    });
                    $(iobj).blur(ValidateLeave);
                }
            } else {
                if ($(iobj).attr("id") == CanNotSaveId) {
                    $(iobj).unbind("blur", ValidateLeave);
                }
            }
        }

        function ValidateLeave() {
            window.setTimeout("showMsgBox('" + InValidateMsg + "', true);$('#" + CanNotSaveId + "').focus();", 100);
        }
        /*
        Array.prototype.S=String.fromCharCode(2);
        Array.prototype.in_array=function(e)
        {
        var r=new RegExp(this.S+e+this.S);
        return (r.test(this.S+this.join(this.S)+this.S));
        }
        function GetSaveIds(iObj) {
        var id = iObj.id;
        var clss = $(iObj).attr('class');
        if (clss.indexOf('combo-text') >= 0) {
        var realobj = $(iObj).parent().prev();
        id = realobj.attr('id');
        }
        if (saveids.in_array(id)) return;
        saveids.push(id);alert(id);
        }*/
        function RequireFun(obj) {
            var area = $(obj);
            var errob = area.next("[forerr=1]");
            if (area.val().length > 0) {
                if(errob.length > 0)
                    errob.remove();
            } else {
                if(errob.length == 0)
                    area.after("<span forerr=\"1\" style=\"color: #FF0000;font-size: 14px;\">*</span>"); 
            }
        }

        //绑定日期框事件
        function BindEvent(id) {//formCtrlValOp
            var objVal = formCtrlValOp(id);
            if (!objVal) {
                DoSetCurrDateTime(id);
            }
        }

        function DoSetCurrDateTime(id) {
            var sendObj = {};
            sendObj.action = "sysdata";
            var clss = $("#" + id).attr('class');
            if (clss.indexOf('datetimebox') >= 0) //easyui-
                sendObj.datetype = "DateTimeBox";
            $.ajax({
                url: 'GetData.aspx',
                type: 'Post',
                async: false,
                cache: false,
                data: sendObj,
                dataType: 'text',
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    alert("获取服务器时间时发生远程请求错误！");
                },
                success: function(result) {
                    result = GetResultStr(result);
                    if (result.indexOf(backErrMsg) > 0) {
                        alert("请求错误！请联系管理员查看日志。");
                        return;
                    }
                    formCtrlValOp(id, 1, "", "", result);
                }
            });
        }
   
//可转移到easyui-lang-zh_CN.js中--%>
   
        function timeEqual(newt, oldt) {
            function _6f7(_6f8) {
                return (_6f8 < 10 ? "0" : "") + _6f8;
            };
            function getFullTime(s, havetime) {
                var dt = s.split(" ");
                var y = 0, m = 0, d = 0;
                if (dt[0]) {
                    var ss = dt[0].split('-');
                    y = parseInt(ss[0], 10);
                    m = parseInt(ss[1], 10);
                    d = parseInt(ss[2], 10);
                }
                var hour = 0, minute = 0, second = 0;
                var time = dt[1]; 
                if (time && havetime) {
                    var tt = time.split(":");
                    hour = parseInt(tt[0], 10);
                    minute = parseInt(tt[1], 10);
                    second = parseInt(tt[2], 10);
                } 
                var fdate = y + '-' + (m < 10 ? ('0' + m) : m) + '-' + (d < 10 ? ('0' + d) : d);
                var ftime = fdate + " " + _6f7(hour) + ":" + _6f7(minute) + ":" + _6f7(second);
                return ftime;
            }
            var t = newt.indexOf(" ") > 0;
            return getFullTime(newt, t) == getFullTime(oldt, t);
        }

        function GetRowIndex(tgid, rowcaller, rowIdx) {
            var dyObj = $('#' + tgid);
            var headRows = dyObj.find("tr[isHead='1']");
            if (!rowIdx || rowIdx < 0) {
                rowIdx = parseInt(rowcaller, 10);
                if (rowIdx == rowcaller && rowIdx > 0) {
                    rowIdx -= 1;
                } else if (rowcaller) {
                    var parentobj = rowcaller;
                    if (($(rowcaller).attr("tagName") || rowcaller.tagName) != "TR")
                        parentobj = $(rowcaller).parents("tr")[0];
                    if (parentobj)
                        rowIdx = parentobj.rowIndex - headRows.length;
                    else
                        rowIdx = -1;
                } else {
                    rowIdx = -1;
                }
            } else {
                var ridx = parseInt(rowIdx, 10);
                if (ridx == rowIdx && rowIdx > 0) {
                    rowIdx -= 1;
                } else {
                    rowIdx = -1;
                }
            }
            return rowIdx;
        }
//生成数据列表，url打开函数--%>
   
        //最后生成DyTbGrid
        function CreateDyTbGrid(objid, page, rows, keyword) {
            var sendObj = {};
            sendObj.iid = $("#hIidPlus").val(); //"<%=IidPlus%>";
            sendObj.fid = $("#hfid").val(); //"<%=fid%>";
            sendObj.action = "DataTbGrid";
            sendObj.input_index = $("#hinput_index").val(); //"<%=input_index%>";
            sendObj.objid = objid;
            sendObj.page = page;
            sendObj.rows = rows;
            sendObj.keyword = keyword;
            sendObj.hrefs = jQuery.data($("#" + objid)[0], "hrefs");
            sendObj.rightstr = jQuery.data($("#" + objid)[0], "rightstr");
            var t = eval({ action: "Validateion", name: "name" });//JSON.stringify(sendObj);
            $.ajax({
                url: 'GetData.aspx',
                type: 'Post',
                async: false,
                cache: false,
                data: sendObj,
                dataType: 'text',
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    alert("生成数据列表时候发生远程请求错误！");
                },
                success: function(result) {
                    result = GetResultStr(result);
                    if (result.indexOf(backErrMsg) > 0) {
                        alert("请求错误！请联系管理员查看日志。");
                        return;
                    }
                    var spstr = "［DyTbGrid］";
                    var i = result.indexOf(spstr);
                    $("#" + objid).html("");
                    $("#" + objid).append(result.substr(0, i));
                    jQuery.data($("#" + objid)[0], "datas", JSON.parse(result.substr(i + spstr.length)));
                    $("#" + objid).attr("dskeys", keyword);
                    SetTbImgSrc(objid);
                    BindEventFun(objid);
                }
            });
        }

        function BindEventFun(objid) {
            var dytb = $("#" + objid);
            var eventstr = jQuery.data(dytb[0], "eventstr");
            if (!eventstr) return;
            var arreven = eventstr.split("' ");
            for (var i = 0; i < arreven.length; i++) {
                var arrfun = arreven[i].split("='");
                if (arrfun.length != 2) continue;
                var ename = arrfun[0].replace(" ", "");
                dytb.find("tr[isrow='1']").each(function() {
                    var datas = jQuery.data(dytb[0], "datas");
                    var jdata = { rowIndex: GetRowIndex(objid, this) };
                    jdata.rowData = datas.rows[jdata.rowIndex];
                    if (ename == "RowCreated") {
                        eval(arrfun[1]);
                    }
                    else if (ename == "onClickRow" || ename == "onDblClickRow") {
                        var ckrow = arrfun[1];
                        var fn = ename == "onClickRow" ? "click" : "dblclick";
                        $(this).bind(fn, function() {
                            var isgc = jdata;
                            eval(ckrow);
                        });
                    }
                    else if (ename == "onClickCell" || ename == "onDblClickCell") {
                        $(this).find("td").each(function() {
                            var isgc = {};
                            isgc.rowIndex = jdata.rowIndex;
                            isgc.rowData = jdata.rowData;
                            isgc.field = $(this).attr("queryItem");
                            if (isgc.field) isgc.value = isgc.rowData[isgc.field];
                            else isgc.value = "";
                            var cktd = arrfun[1];
                            var fn = ename == "onClickCell" ? "click" : "dblclick";
                            $(this).bind(fn, function() {
                                eval(cktd);
                            });
                        });
                    }
                });
            }
        }
        
        //设置Image src
        function SetTbImgSrc(tbid) {
            $("#" + tbid).find("input[src*='urlValue']").each(function() {
                $(this).trigger('click');
            });
        }
        
        //用户设置了url变量后的操作
        function dyUrlAction(ids, cfields, obj) {
            var iimg = false;
            var idiv = false;
            if ($(obj).attr("tagName") == "DIV" || (obj && obj.tagName == "DIV"))
                idiv = true;
            else if ($(obj).attr("type") == "image")
                iimg = true;
            var target = $(obj).attr("target");
            if (target == 3) {
                if (!iimg)
                    return false;
            }

            var url = $(obj).attr("href");
            if (iimg || idiv) url = $(obj).attr("src");
            if (!url) return;

            var rawstr = $(obj).attr("rawstr");
            if (rawstr) url = rawstr;
            rawstr = url;
            for (var i = 0; i < ids.length; i++)
            {
                var urlValue = formCtrlValOp(ids[i], 0, cfields[i], obj);
                if ($("#" + ids[i]).attr("type") == "radio") {
                    urlValue = urlValue.substr(urlValue.indexOf("|") + 1);
                }
                url = url.replace("urlValue"+(i+1), urlValue);
            }
            if (rawstr != url)
                $(obj).attr("rawstr", rawstr);
            if (iimg || idiv) {
                //url = "http://img04.taobaocdn.com/tps/i4/T1UPFAXnNfXXXXXXXX-130-60.gif";
                $(obj).attr("src", url);
                return;
            }
            else {
                $(obj).attr("href", url);
            }

            var wintitle = $(obj).attr("wintitle");
            var wmaximizable = $(obj).attr("wmaximizable");
            var initMax = $(obj).attr("initMax") == "1";
            wintitle = wintitle ? wintitle : "";
            var x = 0, y = 0, w = 0, h = 0;
            //if (!initMax) 
            {
                w = GetNumber($(obj).attr("hrefwidth"), 0); h = GetNumber($(obj).attr("hrefheight"), 0);
                w = w ? w : 600; h = h ? h : 400;
                x = GetNumber($(obj).attr("wleft"), 0); y = GetNumber($(obj).attr("wtop"), 0);
                if (!x && $(obj).attr("wleft") != "0") {
                    x = ($(window).width() - w) / 2;
                    x += eval($(window).scrollLeft()) + 3;
                }
                if (!y && $(obj).attr("wtop") != "0") {
                    y = ($(window).height() - h) / 2;
                    y += eval($(window).scrollTop()) + 3;
                }
                if (x < 0) x = 3;
                if (y < 0) y = 3;
            }
            if (target == 1) {
                var win = window.open($(obj).attr("href"), wintitle, "scrollbars=yes,status=no,toolbar=no,location=no,directories=no,menubar=no,resizable=no,titlebar=no,width=" + w + ",height=" + h + ",left=" + x + ",top=" + y, true);
                if (initMax) {
                    win.moveTo(0, 0);
                    win.resizeTo(window.screen.availWidth, window.screen.availHeight);
                }
                return false;
            } else if (target == 2) {
                if (document.getElementById("target2")) {//$("#target2").length == 0
                    //$("#target2 > iframe").eq(0).attr('src', obj.href);
                    $("#target2").remove();
                }
                {
                    var content = '<iframe scrolling="auto" frameborder="0"  src="' + $(obj).attr("href") + '" style="width:100%;height:100%;"></iframe>';//<div style="overflow:hidden;width:100%;height:100%;"></div>
                    var temp = $('<div id="target2" iconCls="" title="&nbsp;">' + content + '</div>');
                    $('body').append(temp);
                }
                //if (initMax) {
                //    x = y = 0;
                //    w = $(window).width();h = $(window).height();
                //}
                var winOps = { left: x, top: y, width: w, height: h, modal: true, maximizable: wmaximizable ? true : false, minimizable: false, collapsible: false, resizable: false,
                    onClose: function() {},
                    onMaximize: function() {
                        var ops = $('#target2').panel('options');
                        ops.fit = false;
                        $('#target2').window('resize', { width: $(window).width(), height: $(window).height() });
                    }
                };
                if(wintitle) winOps.title = wintitle;
                $('#target2').window(winOps);
                if (initMax) $('#target2').window('maximize');
                return false;
            }
            
            document.location = $(obj).attr("href");
        }
        //对某条记录的具体操作
        //id 编辑的记录所在的table的id
        //operation 操作类型 0新增 1保存
        function pageRecordOperation(id, operation) {// 未使用
            var dytable;
            /*var obj = dytables;
            if (obj.length == 0) return;
            for (var i = 0; i < obj.length; i++) {
                var ig = obj[i][0];
                var tid = ig.id;
                if (tid == id) dytable = ig;
            }*/
            dytable = $("#"+id+"");
            if (operation == 0) {//新增
//                $("#" + id + " > input").each(function() {
//                    $(this).val("");
                //                });
                dytable.find("tr td input[dytype='1']").each(function() {
                    $(this).val("");
                });
            } else if (operation == 1) {//保存
                
            }
        }
   
//<%--from HtmlMainpage.aspx--%>
        function sPrint(act) {
            var obj = {};
            obj.value = act;
            if (navigator.userAgent.toLowerCase().indexOf("msie") >= 0) {
                if (obj.value == "Print" || obj.value == "Print2") {
                    WebBrowser.execwb(6, 1);

                    window.close();
                }
                else if (obj.value == "PrintView" || obj.value == "Print2View")//打印预览
                {
                    WebBrowser.execwb(7, 1);
                    //window.opener=null;  
                    window.close();
                }
            }
            else if (obj.value == "Print" || obj.value == "Print2" || obj.value == "PrintView" || obj.value == "Print2View") {
                window.print();
                window.close();
            }
        }
        
        //执行select获得要返回的傎
        function GetDynTableValue(sql) {        
            var res = "";
            $.ajax({
                url: '../appservice/formsave.ashx',
                type: 'post',
                async: false,
                cache: false,
                data: 'op=getdyntablevalue&sql_value=' + sql,
                error: function() {
                    showMsgBox("脚本中执行Sql出错"); //先这样提示
                },
                success: function(result) {
                    result = GetResultStr(result);
                    res = result;
                }
            });
            return res;
        }
        function GetSysGuid(format,toUpper) {
            var res = "";
            $.ajax({
                url: '../appservice/formsave.ashx',
                type: 'post',
                async: false,
                cache: false,
                data: 'op=getsysguid&format='+format +"&toupper="+toUpper ,
                error: function() {
                    showMsgBox("脚本中获取Guid出错"); //先这样提示
                },
                success: function(result) {
                    result = GetResultStr(result);
                    res = result;
                }
            });
            return res;
        }
        function GetSysDate(objname) {
            var res = "";
            $.ajax({
                url: '../appservice/formsave.ashx',
                type: 'post',
                async: false,
                cache: false,
                data: 'op=getsysdate&objname=' + objname,
                error: function() {
                    showMsgBox("脚本中获取服务器日期出错"); //先这样提示
                },
                success: function(result) {
                    result = GetResultStr(result);
                    res = result;
                }
            });
            return res;
        }
        function GetRMBNumber(v) {
            var res = "";
            $.ajax({
                url: '../appservice/formsave.ashx',
                type: 'post',
                async: false,
                cache: false,
                data: 'op=getrmb&str=' + v,
                error: function() {
                    showMsgBox("脚本中数字转人民币大写出错"); //先这样提示
                },
                success: function(result) {
                    result = GetResultStr(result);
                    res = result;
                }
            });
            return res;
        }
        //执行存程过程,得到返回值 JavaScript 调用WebService --- 执行存储过程
        function GetProcValue(procname, pname, pvalue) {          
            var res = "";
            $.ajax({
                url: '../appservice/formsave.ashx',
                type: 'post',
                async: false,
                cache: false,
                data: 'op=getfunctionvalue&procName=' + procname + '&pName=' + pname + '&pValue=' + pvalue,
                error: function() {
                    showMsgBox("出错了"); 
                },
                success: function(result) {
                    result = GetResultStr(result);
                    res = result;
                }
            });
            return res;
        }
   
// <%--menu--%>

        $(function() {
            //$('body').append($("<div id='mm1' class='easyui-menu' style='width: 120px; display: none'><div onclick='exp()'>导出</div></div>"));
            //var name = "menu";var r = $("#mm1");r[name]();
            $(document).bind('contextmenu', function(e) {
                var elm = arguments[0].target || e.srcElement;
                var a = getSelectedText();

                if (elm.tagName == "TEXTAREA" || elm.tagName == "INPUT"
        || ((a != null && a != "") && (elm.tagName == "TD" || elm.tagName == "DIV" || elm.tagName == "SPAN" || elm.tanagName == "A"))) return true;

                var sMenuId = "";
                sMenuId = $(elm).attr("menuid");
                if (!sMenuId) {
                    var comlistObj = $(elm).parents("select[dytype='37'], ul[dytype='17']");
                    sMenuId = comlistObj.attr("menuid");
                }
                if (!sMenuId) {
                    var tbGrid = $(elm).parents("table[dytype='35']");
                    sMenuId = tbGrid.attr("menuid");
                    if (!sMenuId) {
                        var dataGrid = $(elm).parents("div.panel");
                        dataGrid = dataGrid.find("table[dytype='18']");
                        sMenuId = dataGrid.attr("menuid");
                        if (!sMenuId) {
                            if (tbGrid.length || dataGrid.length) {//ff下可用样式解决
                                $("#mexcel").removeAttr("disabled");
                                $("#mword").attr("disabled", "disabled");
                                if (tbGrid.length) {
                                    $("#mexcel").attr("action", "DataTbGrid");
                                    $("#mexcel").attr("objid", tbGrid[0].id);
                                    $("#mexcel").attr("dskeys", $("#" + tbGrid[0].id).attr("dskeys"));
                                } else {
                                    $("#mexcel").attr("action", "DataGrid");
                                    $("#mexcel").attr("objid", dataGrid[0].id);
                                    $("#mexcel").attr("dskeys", $("#" + dataGrid[0].id).attr("dskeys"));
                                }
                            } else {
                                $("#mexcel").attr("disabled", true);
                                $("#mword").attr("disabled", false);
                            }
                        }
                    }
                }
                if (!sMenuId) {
                    sMenuId = $('#hFormMenuId').val();
                    if ($('#hDefRightMenu').val() == "0") {
                        if (!sMenuId) return false;
                    }
                    else {
                        if (!sMenuId)
                            sMenuId = "mm2";
                    }
                }
                //menuLeft = e.pageX;
                //menuTop = e.pageY;
                $('#' + sMenuId).menu('show', {
                    left: e.pageX,
                    top: e.pageY
                });
                return false;
            });
        });
        function expFile(obj) {
            var url = "";
            if (obj)
                url = "../HtmlPage/ExportDoc.aspx?type=3&np=1&action=" + $(obj).attr("action") + "&iid=" + $("#hIidPlus").val() + "&fid=" + $("#hfid").val() + "&input_index=" + $("#hinput_index").val() + "&objid=" + $(obj).attr("objid") + "&dskeys=" + encodeURIComponent($(obj).attr("dskeys")), "newwindow";
            else
                url = '../HtmlPage/PageRightMenuExport.aspx?iid=' + $("#hIidPlus").val() + '&input_index=' + $("#hInput_index").val() + '&fid=' + $("#hfid").val() + '&px=0&py=0';
            window.open(url); 
            //, "toolbar=no,location=no,directories=no,status=yes,menubar=no,resizable=yes,width=" + nWidth + ",height=" + nHeight + ",left=" + nLeft + ",top=" + nTop, true
        }
        function getSelectedText() {
            if (window.getSelection) {
                // This technique is the most likely to be standardized. 
                // getSelection() returns a Selection object, which we do not document. 
                return window.getSelection().toString();
            }
            else if (document.getSelection) {
                // This is an older, simpler technique that returns a string 
                return document.getSelection();
            }
            else if (document.selection) {
                // This is the IE-specific technique. 
                // We do not document the IE selection property or TextRange objects. 
                return document.selection.createRange().text;
            }
        }
  
// <%--查询图表,隐藏图表--%>
   
        function getFlashMovieObject(movieName) {
            if (document["ff_" + movieName])//firefox
            {
                return document["ff_" + movieName];
            }
            else {
                return document.document[movieName];
            }
        }
        function queryFChart(objid, keyword) {
            //var fcobj = getFlashMovieObject(objid);
            var pdiv = $("#div"+objid);
            var sendObj = {};
            sendObj.iid = $("#hIidPlus").val(); //"<%=IidPlus%>";
            sendObj.fid = $("#hfid").val(); //"<%=fid%>";
            sendObj.action = pdiv.attr("fctype");
            sendObj.input_index = $("#hinput_index").val(); //"<%=input_index%>";
            sendObj.objid = objid;
            sendObj.keyword = keyword;
            $.ajax({
                url: 'GetData.aspx',
                type: 'Post',
                async: false,
                cache: false,
                data: sendObj,
                dataType: 'text',
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    alert("查询图表时发生远程请求错误！");
                },
                success: function(result) {
                    result = GetResultStr(result);
                    if (result.indexOf(backErrMsg) > 0) {
                        alert("请求错误！请联系管理员查看日志。");
                        return;
                    }
                    var chart1 = new FusionCharts(pdiv.attr("strSWF"), objid, pdiv.attr("sw"), pdiv.attr("sh"), 0, 0);
                    chart1.setDataXML(result);
                    chart1.render("div" + objid);
                }
            });
        }

        function hideFcCharts() {
            //$("#div0fc5dc0b534b4ce9ad023c7efdd481b0").toggleClass("hidefc");
            $("div[fctype^='Fc']").each(function() {
                $(this).toggleClass("hidefc");
            });
        }
    
//<%--数据表修订记录,工具栏处理函数--%>
  
        var dyGridChLog = null;
        function getChangeLog() {
            var sendObj = {};
            sendObj.action = "ChangeLog";
            sendObj.iid = $("#hiid").val(); //"<%=iid%>";
            sendObj.fid = $("#hfid").val(); //"<%=fid%>";
            $.ajax({
                url: 'GetData.aspx',
                type: 'Post',
                async: false,
                cache: false,
                data: sendObj,
                dataType: 'text',
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    alert("获取修订记录时发生远程请求错误！");
                },
                success: function(result) {
                    result = GetResultStr(result);
                    if (result.indexOf(backErrMsg) > 0) {
                        alert("请求错误！请联系管理员查看日志。");
                        return;
                    }
                    dyGridChLog = JSON.parse(result);
                }
            });
        }
        function GetOneLogText(showLog, idField, rowData, rowField, curColField) {
            if (!showLog || dyGridChLog == null || idField == null || idField == undefined) return "";
            var curidVal = null;
            for (var key in rowData) {
                if (idField.toLowerCase() == key.toLowerCase()) {
                    curidVal = rowData[key];
                    break;
                }
            }
            if (curidVal == null || curidVal == undefined || curidVal == "") return "";
            var rowLog = dyGridChLog[curidVal];
            if (rowLog == undefined) return "";
            if (rowLog[curColField.toLowerCase()] == undefined) return "";
            return "title=\"" + dyGridChLog[curidVal][curColField.toLowerCase()] + "\"";
        }

        function EndEditDataGrid(curIdx) {
            var diffentIdx = true;
            if (GlobalDgGridLastIndex) {
                var arr = GlobalDgGridLastIndex.split("|");
                if (arr.length == 2) {
                    if (GlobalDgGridLastIndex == curIdx) {
                        diffentIdx = false;
                    } else {
                        $('#' + arr[0]).datagrid('endEdit', arr[1]);
                        GlobalDgGridLastIndex = "";
                    }
                }
            }
            return diffentIdx;
        }

        function GridAddRowFun(Id, strfields, viewh) {
            EndEditDataGrid();
            $('#' + Id).datagrid('appendRow', strfields);
            GlobalDgGridLastIndex = $('#' + Id).datagrid('getRows').length - 1;
            $('#' + Id).datagrid('selectRow', GlobalDgGridLastIndex);
            $('#' + Id).datagrid('beginEdit', GlobalDgGridLastIndex);
            GlobalDgGridLastIndex = Id + '|' + GlobalDgGridLastIndex;
            if (viewh) SetInitGridHeight(Id, '', viewh);
        }
        //function GridImpData(Id, impInfos, viewh) //ImpDataFromExcel

        function GridDelRow(Id, viewh) {
            var row = $('#' + Id).datagrid('getSelected');
            if (row) {
                var index = $('#' + Id).datagrid('getRowIndex', row);
                $('#' + Id).datagrid('deleteRow', index);
                SetInitGridHeight(Id, '', viewh);
            }
        }

        function GridUnDo(Id, viewh) {
            $('#' + Id).datagrid('rejectChanges');
            if (viewh) SetInitGridHeight(Id, '', viewh);
        }

        function MergeDataGridCell(id) {
            if(id == 'DyDataGrid2')
                $('#' + id).datagrid('mergeCells', { rowspan: 2, colspan: 1, index: 3, field: 'age' });
        }
//<%--供用户调用的函数--%>

        function QueryDataGridBody(id, keyword) {
            if (!keyword) keyword = "";
            var opts = $("#" + id).datagrid("options"); 
            if (opts.manualurl) opts.url = opts.manualurl;
            $("#" + id).datagrid({ pageNumber: 1, queryParams: { action: "DataGrid", fid: $("#hfid").val(), iid: $("#hIidPlus").val(), input_index: $("#hinput_index").val(), objid: id, keyword: keyword} });
            $("#" + id).attr("dskeys", keyword);
        }

        var resizeTimer = null;
        function SetInitGridHeight(id, data, defineH) {
            if (resizeTimer) clearTimeout(resizeTimer);
            resizeTimer = setTimeout(function() {
                //alert("SetInitGridHeight");

                var h = 33;
                var view = $("#" + id).datagrid("getPanel").find("div.datagrid-view");
                h = view.css("height");
                h = h.substring(0, h.length - 2);
                //if (data) {
                //    var dh = data.rows.length * 33;
                //    h = Math.max(dh, h);
                //}
                if (h < defineH)
                //resizeTimer = setTimeout(function() {
                    view.css("height", defineH + "px");
                //}, 500);
                $("#" + id).attr("canprint", 1);
            }, 300);
        }

        var dgresizeTimer = null;
        
        function SetDataGridSize(id) {
            //setTimeout(function() {
                //alert("SetDataGridSize");
                var pobj = $("#" + id).parents("td,body").eq(0);
                //alert(pobj.width() + "|" + pobj.height());
                var pw = pobj.width();
                var ph = pobj.height();

                var opts = $.data($("#" + id)[0], "datagrid").options;
                var chw = Math.abs(pw - opts.width) > 2;
                var chh = opts.oheight && (Math.abs(ph - opts.height) > 2);
                var sizepara = {};
                if(chw) {
                    sizepara.width = opts.owidth ? Math.round((pw * opts.owidth) / 100) : opts.width;
                    for (var r = 0; r < opts.columns.length; r++) {
                        for (var c = 0; c < opts.columns[r].length; c++) {
                            var rcol = opts.columns[r][c];
                            if (rcol.owidth && !rcol.hidden) {
                                rcol.width = Math.round((sizepara.width * rcol.owidth) / 100) + opts.eachAdd;
                                rcol.boxWidth = rcol.width - 8;
                            }
                        }
                    }
                }

                if (chh) sizepara.height = opts.oheight ? Math.round((ph * opts.oheight) / 100) : opts.height;
                if(chw || chh)
                    $("#" + id).datagrid('resize', sizepara);
                if (chw)
                    $("#" + id).datagrid("fixColumnSize");
            //}, 100);
        }
    
        //<%--cookie--%>

        //onStopResize会引起失效
        function DyTableSetCookie(tableid, field, width, boxwidth) {
            //fid iid input_inxdex rtid sctlid userid wiid
            var today = new Date();
            var expireDay = new Date();
            var msYear = 24 * 60 * 60 * 1000 * 365 * 100;
            expireDay.setTime(today.getTime() + msYear);
            var key = $("#hfid").val() + $("#hiid").val() + $("#hinput_index").val() + $("#hsctlid").val() + $("#huserid").val() + $("#hwiid").val() + tableid + field;
            document.cookie = key + "width=" + width + ";expires=" + expireDay.toGMTString();
            document.cookie = key + "boxwidth=" + boxwidth + ";expires=" + expireDay.toGMTString();
        }
        function DyTableGetCookie(Key) {
            var search = Key + "=";
            begin = document.cookie.indexOf(search);
            if (begin != -1) {
                begin += search.length;
                end = document.cookie.indexOf(";", begin);
                if (end == -1) end = document.cookie.length;
                return document.cookie.substring(begin, end);
            }
        }
        function SetTableWidth(tbId, fields) {
            //setTimeout(function() {
                //alert("SetTableWidth");
                var key = $("#hfid").val() + $("#hiid").val() + $("#hinput_index").val() + $("#hsctlid").val() + $("#huserid").val() + $("#hwiid").val() + tbId;
                var dyTable = $("#" + tbId);
                for (var i = 0; i < fields.length; i++) {
                    var cwidth = DyTableGetCookie(key + fields[i] + "width");
                    if (!cwidth) continue;
                    var colc = $.fn.datagrid.methods.getColumnOption(dyTable, fields[i]);
                    if (!colc) continue;
                    var cboxwidth = DyTableGetCookie(key + fields[i] + "boxwidth");
                    colc.width = parseInt(cwidth);
                    colc.boxWidth = parseInt(cboxwidth);
                }
                $.fn.datagrid.methods.fixColumnSize(dyTable);
            //}, 500);
        }

        function GetDateDisplay(val) {
            var n = val.indexOf(" ");
            if (n > 0)
                val = val.substring(0, n);
            return val;
        }
        
        function GetComboDisplay(val, vlist)
        {
            for(var i = 0; i < vlist.length; i++)
            {
                if(vlist[i].id == val)
                {
                    val = vlist[i].text;
                }
            }
            return val;
        }
        /*
        function GetCombotreeDisplay(value, rec) {
            var pos = rec.url.indexOf("objid=");
            var objid = rec.url.substr(pos + 6);
            if (!document.getElementById(objid)) {
                rec.onLoadSuccess = DisplyTreeLoaded;
                var _4c6 = $("<input type=\"text\" id=\"" + objid + "\">").appendTo($('body'));
                _4c6.combotree(rec);
                _4c6.next("span").css("display", "none");
            }
            $("#" + objid).combotree("setValue", value);
            value = $("#" + objid).combotree("getText");
            return value;
        }*/

        
//<%--DyDataGrid Excle2007导入--%>
        var gImpInfos = "";
        function ImpDataFromExcel(gridId, impInfos, viewH) {
            if (document.getElementById("UpLoadDataFileWin")) {
                $("#UpLoadDataFileWin").remove();
            }
            {
                var content = '<iframe frameborder="0"  src="UpLoadDataFile.aspx" style="width:100%;height:100%;"></iframe>';
                var temp = $('<div id="UpLoadDataFileWin" iconCls="" title="&nbsp;">' + content + '</div>');
                $('body').append(temp);

                jQuery.data($("#UpLoadDataFileWin")[0], "strfields", impInfos);
                jQuery.data($("#UpLoadDataFileWin")[0], "viewH", viewH);
                jQuery.data($("#UpLoadDataFileWin")[0], "gridId", gridId);
                gImpInfos = impInfos;
            }
            $('#UpLoadDataFileWin').window({
                width: 405,
                height: 250,
                modal: true,
                minimizable: false,
                maximizable: false,
                collapsible: false,
                onClose: function() {
                }
            });
        }
        function ShowImpData(values) {
            $('#UpLoadDataFileWin').window('close');
            if (!values) return;
            var viewH = jQuery.data($("#UpLoadDataFileWin")[0], "viewH");
            var gridId = jQuery.data($("#UpLoadDataFileWin")[0], "gridId");
            var opts = $('#' + gridId).datagrid('options');
            var impdatas = JSON.parse(values);
            if (opts.rownumbers && impdatas.rows.length > 200) {
                alert("数据编辑表存在“序号列”，不能导入超过200条的数据！");
                return;
            }
            var oldDatas = $('#' + gridId).datagrid('getData');
            impdatas.total = oldDatas.total;
            impdatas.rows = oldDatas.rows.concat(impdatas.rows);
            setTimeout(function() {
                //alert("beigin");
                $('#' + gridId).datagrid('loadData', impdatas);
                $('#' + gridId).attr("imped", 1);
                if (viewH != "0")
                    SetInitGridHeight(gridId, '', viewH);
            }, 100);
        }/*
        function ShowImpData(values) {
            $('#UpLoadDataFileWin').window('close');
            if (!values) return;
            var strfields = jQuery.data($("#UpLoadDataFileWin")[0], "strfields");
            var viewH = jQuery.data($("#UpLoadDataFileWin")[0], "viewH");
            var gridId = jQuery.data($("#UpLoadDataFileWin")[0], "gridId");
            var gridKey = strfields.split('|')[1];
            strfields = strfields.split('|')[0];
            var fpairArr = strfields.split(',');
            var fieldArr = [];
            for (var i = 0; i < fpairArr.length; i++) {
                var fieldn = fpairArr[i].split(':');
                if (fieldn.length == 2 && fieldn[0] && fieldn[0].toLowerCase() != gridKey.toLowerCase())
                    fieldArr.push(fieldn[0]);
            }
            var impdatas = JSON.parse(values);
            var i = 0;
            var oneLen = 0;
            function addRows() {
                for (; i < impdatas.total; i++) {
                    var fieldvals = '"' + gridKey + '":""';
                    for (var f = 0; f < fieldArr.length; f++) {
                        var valname = "name" + i + f;
                        var oneval = impdatas.rows[i][valname];
                        if (oneval)
                            oneval = oneval.replace('"', '\\"');
                        else
                            oneval = "";
                        fieldvals += ',"' + fieldArr[f] + '":' + '"' + oneval + '"';
                    }
                    if (fieldvals) {
                        $('#' + gridId).datagrid('appendRow', JSON.parse("{" + fieldvals + "}"));
                    }
                    oneLen++;
                    if (oneLen > 10) break;
                }
                if (viewH != "0" && impdatas.total == i)
                    SetInitGridHeight(gridId, '', viewH);
            }

            function addOnce() {
                oneLen = 0;
                addRows();
                setTimeout(addOnce, 200);
            }

            setTimeout(addOnce, 50);
        }*/

// <%--手动加载--%>
        function InitCmbList(did, cdata, isuserval, value) {
            if (!cdata) return;
            var opts = "";
            var cval = (isuserval ? value : $("#" + did).attr("oldval"));
            for (var i = 0; i < cdata.length; i++) {
                //if (cdata[i].id == val) {
                //    val = cdata[i].text;
                opts += '<option value="' + cdata[i].id + '"';
                if (cval == cdata[i].id) opts += ' selected=\"selected\"';
                opts += '>' + cdata[i].text + '</option>';
            }
            $('#' + did).html(opts).removeAttr("inited");
        }

        function SetCombStyle(cobj, did) {
            var panel = $.data(cobj, "combo").combo;
            panel.addClass("combo-text-" + did);
            var itext = panel.find("input.combo-text");
            itext.addClass("combo-text-" + did + "input");
            var CssName = $(cobj).attr("class");
            if (CssName) {
                panel.addClass(CssName);
                itext.addClass(CssName);
            }
            var tidx = $(cobj).attr("tabindex");
            if (tidx)
                itext.attr('tabindex', tidx);
            //if (cobj.id == "DyCombox2") {
                //var swidth = cobj.style.width.replace("px", "");
                itext.attr("style", "");
                panel.attr("style", "");
                //itext.css("width", "50px");
                //panel.css("width", "138px");
                //itext.css("width", "58px");
                //alert(swidth + "|" + panel.width() + "|" + itext.width());
            //}
        }

        function AddBigFrame(did) {
            var dobj = document.getElementById(did);
            var panobj = $.data(dobj, "combo").panel;
            //alert(panobj);
            panobj.bgiframe();
        }
        function InitCmbTree(did, cdata, isuserval, value) {
            var tjson = {
                data: cdata,
                //checkbox: false //$("#" + did).attr("checkbox"),
                multiple: $("#" + did).attr("multiple")?true:false,
                onClick: function(node) {
                    try {
                        var sch = $("#" + did).attr("onClick");
                        CalculateEuiControl(did);
                        if (sch) setTimeout(sch, 500);
                    } catch (e) { alert("自定义事件出错：" + e); }
                },
                onChange: function(newValue, oldValue) {
                    try {
                        var sch = $("#" + did).attr("onChange");
                        CalculateEuiControl(did);
                        if (sch) setTimeout(sch, 500);
                    } catch (e) { alert("自定义事件出错：" + e); }
                },
                onLoadSuccess: function(node, data) {
                    formCtrlValOp(did, '1', '', '', (isuserval ? value : GetInitVal(did)));
                    $("#" + did).each(function() {
                        SetCombStyle(this, did);
                        $(this).removeAttr("inited");
                    });
                },
                onShowPanel: function() {
                    AddBigFrame(did);
                }
            };
            $('#' + did).combotree(tjson);
        }
        
        function InitTree(did, cdata, isuserval, value) {
            var tjson = {
                //url: "GetData.aspx?action=dytree&fid=77f96d33-c0ff-47db-a1e8-87aaa0bd3a28&Iid=2012031900001|ee9ea9d9-6ac8-40cf-af60-b7b391582ed9|C1632346AA9A42D7A118A795FF82C61E|2012031900001007&input_index=0&objid=DyTree2",
                data: cdata,
                onClick: function(node) {
                    try {
                        var sch = $("#" + did).attr("onClick");
                        sch = GetAnonyMousFunBody(sch);
                        CalculateEuiControl(did);
                        if (sch) eval(sch + "(node);"); //setTimeout(sch, 500);
                    } catch (e) { alert("自定义事件出错：" + e); }
                },
                onLoadSuccess: function(node, data) {
                    $(this).removeAttr("inited");
                    formCtrlValOp(did, '1', '', '', (isuserval ? value : $("#" + did).attr("oldval")));
                    var sch = $("#" + did).attr("onLoadSuccess");
                    if (sch) eval(sch + "(node);");
                }
            };
            var arrFun = ["onBeforeExpand", "onExpand", "onBeforeCollapse", "onCollapse", "onLoadError", "onContextMenu", "onCheck", "onSelect"];
            for (var s = 0; s < arrFun.length; s++) {
                var strfun = $("#" + did).attr(arrFun[s]);
                if (!strfun) continue;
                switch (s) {
                    case 0:
                        tjson[arrFun[s]] = function(arg1, arg2) { try { var strfun = $("#" + did).attr("onBeforeExpand"); eval(strfun + "(arg1, arg2);"); } catch (e) { alert("自定义事件出错：" + e); } };
                        break;
                    case 1:
                        tjson[arrFun[s]] = function(arg1, arg2) { try { var strfun = $("#" + did).attr("onExpand"); eval(strfun + "(arg1, arg2);"); } catch (e) { alert("自定义事件出错：" + e); } };
                        break;
                    case 2:
                        tjson[arrFun[s]] = function(arg1, arg2) { try { var strfun = $("#" + did).attr("onBeforeCollapse"); eval(strfun + "(arg1, arg2);"); } catch (e) { alert("自定义事件出错：" + e); } };
                        break;
                    case 3:
                        tjson[arrFun[s]] = function(arg1, arg2) { try { var strfun = $("#" + did).attr("onCollapse"); eval(strfun + "(arg1, arg2);"); } catch (e) { alert("自定义事件出错：" + e); } };
                        break;
                    case 4:
                        tjson[arrFun[s]] = function(arg1, arg2) { try { var strfun = $("#" + did).attr("onLoadError"); eval(strfun + "(arg1, arg2);"); } catch (e) { alert("自定义事件出错：" + e); } };
                        break;
                    case 5:
                        tjson[arrFun[s]] = function(arg1, arg2) { try { var strfun = $("#" + did).attr("onContextMenu"); strfun = GetAnonyMousFunBody(strfun); eval(strfun + "(arg1, arg2);"); } catch (e) { alert("自定义事件出错：" + e); } };
                        break;
                    case 6:
                        tjson[arrFun[s]] = function(arg1, arg2) { try { var strfun = $("#" + did).attr("onCheck"); eval(strfun + "(arg1, arg2);"); } catch (e) { alert("自定义事件出错：" + e); } };
                        break;
                    case 7:
                        tjson[arrFun[s]] = function(arg1, arg2) { try { var strfun = $("#" + did).attr("onSelect"); eval(strfun + "(arg1, arg2);"); } catch (e) { alert("自定义事件出错：" + e); } };
                        break;
                }
            }
            $('#' + did).tree(tjson);
        }
        
        function GetInitVal(did) {
            var val1 = $("#" + did).attr("defVal");
            if (!val1)
                val1 = $("#" + did).attr("oldval");
            return val1;
        }
        
        function InitComboBox(did, cdata, isuserval, value) {
            var sch = $("#" + did).attr("onChange");
            //$("#" + did).removeAttr("onChange");
            $("#" + did).combobox({
                data: cdata,
                valueField: 'id',
                textField: 'text',
                onLoadSuccess: function() {
                    $("#" + did).each(function() {
                        setTimeout(function() { SetCombStyle(document.getElementById(did), did); }, 500);
                        var panel = $.data(this, "combo").combo;
                        var itext = panel.find("input.combo-text");
                        //itext.next("span").find("span").trigger("click");
                        $(this).removeAttr("inited");
                    });
                },
                value: isuserval ? value : GetInitVal(did),
                onShowPanel: function() {
                    AddBigFrame(did);
                }
            });
            $("#" + did).combo({
                onChange: function(newValue, oldValue) {
                    if ($("#" + did).attr("inited") == "0") return;
                    CalculateEuiControl(did);
                    if (sch) setTimeout(sch, 500);
                    //$("#" + did).trigger("change");
                }
            });
        }
        function InitDataBox(did, showpanel) {
            var dyObj = $("#" + did);
            var opobj = {
                onShowPanel: function() {
                    AddBigFrame(did);
                    SetTodayDateTime(did);
                }
            };
            if (dyObj.attr('HaveTime') == "1")
                dyObj.datetimebox(opobj);
            else
                dyObj.datebox(opobj); //
            var obj = dyObj.next();
            var istype = (dyObj.attr("style")).replace(/display: none;/gi, "");
            obj.attr('style', istype);
            var objinput = obj.find('input');
            var CssName = dyObj.attr("class");
            if (CssName) {
                obj.addClass(CssName);
                objinput.addClass(CssName);
            }
            objinput.attr('style', istype);
            //setTimeout(function() { objinput.css("width", (parseInt(objinput.css("width")) - 18) + "px"); }, 500);
            objinput.css("border-style", "none");
            //objinput.attr('readonly', 'true');
            objinput.bind("blur", CheckManulDateTime).bind("keyup", DateKeyUpFun);
            objinput.bind('click', function() { $.fn.combo.methods.showPanel($("#" + did)); });
            var tidx = dyObj.attr("tabindex");
            if (tidx)
                objinput.attr('tabindex', tidx);
            objinput.bind('click', function() { BindEvent(did); SetTodayDateTime(did); });
            obj.find(".combo-arrow:eq(0)").click(function() {
                //SetTodayDateTime(did)
            });
            if (showpanel)
                objinput.trigger("click");
            dyObj.removeAttr("inited");
            BindChangeFun(did);
        }
        
        function BindChangeFun(cid) {
            var sch = $("#" + cid).attr("onChange");
            $("#" + cid).combo({
                onChange: function(newValue, oldValue) {
                    if ($("#" + cid).attr("inited") == "0") return;
                    CalculateEuiControl(cid); 
                    if (sch) setTimeout(sch, 500);
                }
            });
        }
        
        function SetTodayDateTime(did) {
            var currbtn = $(".panel:visible a[class='datebox-current']");
            currbtn.attr("setcrr", 1);
            currbtn.one("click", function() {
                if (!currbtn.attr("setcrr")) return;
                currbtn.removeAttr("setcrr", 1);
                $(".panel:visible a.datebox-close").click();
                DoSetCurrDateTime(did);
            });
        }
        
        function DateKeyUpFun() {
            var e = arguments[0] || window.event;
            var src = e.srcElement || e.target; 
            if (src != undefined) {
                if (!(e.keyCode == 9 || e.keyCode == 22 || e.keyCode == 67 || e.keyCode == 22 || e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40))//tab键还有效
                {
                    $(src).attr("needValidate", 1);
                }
            }
            return true;
        }

        var dateBoxSrc = null; //设置焦点
        function CheckManulDateTime() {
            var e = arguments[0] || window.event;
            var src = e.srcElement || e.target;
            if (!src) return;
            var realobj = $(src).parent().prev();
            var tid = realobj.attr("id");
            var dBox = realobj.attr("HaveTime");
            var setChange = false;
            var v = $(src).val();

            v = v.replace(/^\s/, '');
            v = v.replace(/\s$/, '');

            if (!v) {
                setChange = true;
            }
            else if ($(src).attr("needValidate") == 1)//if(v.indexOf('年') < 0)
            {
                dateBoxSrc = src;
                var errMsg = "";
                if (dBox != "1") {
                    v = GetManulDateStr(v);
                    if (v.length >= 5)
                        setChange = true;
                    else
                        errMsg = "请输入正确的日期!(如：2012-10-20 2012/10/20 2012.10.20)";
                } else {
                    v = GetManualDateTimeStr(v);
                    if (v.length >= 5) {
                        setChange = true;
                    } else {
                        errMsg = "请输入正确的日期时间!(如：2012-10-20 9:10 2012/10/20 12:10 2012.10.20 2:56)";
                    }
                }

                if (errMsg) {
                    showMsgBox(errMsg, true);
                    setTimeout("dateBoxSrc.focus()", 500);
                }
            }
            if (setChange) {
                formCtrlValOp(tid, 1, '', '', v);
            }
            return true;
        }
        
        //检查必填
        function CheckRequired() {
            var fields = "";
            $('textarea[required="true"],input[required="true"],select[required="true"]').each(function() {
                if (!formCtrlValOp(this.id)) {
                    fields += "『" + $(this).attr("dename") + "』";
                }
            });
            return fields;
        }

        //富文本
        function InitEditor(eid, w, h, sval) {
            sval = GetRealDisVal(sval);
            var oFCKeditor = new FCKeditor(eid);
            oFCKeditor.BasePath = "../fckeditor/";
            oFCKeditor.Width = w;
            oFCKeditor.Height = h;
            //sval = decodeURIComponent(sval);
            $("#" + eid).val(sval);
            oFCKeditor.ReplaceTextarea();
            //oFCKeditor.Create();
            jQuery.data($("#" + eid)[0], "datas", sval);
        }

        function GetRealDisVal(sval) {
            sval = sval.replace(/&quot;/gi, "\"");
            sval = sval.replace(/&#039;/gi, "\'");
            return sval;
        }

        //上传控件
        function SetUpLoaderParas(Id) {
            var sendObj = {};
            sendObj.fid = $("#hfid").val();
            sendObj.iid = $("#hiid").val(); 
            sendObj.input_index = $("#hinput_index").val();
            sendObj.step = $("#hsctlid").val(); 
            sendObj.userid = $("#huserid").val();
            sendObj.wiid = $("#hwiid").val();
            sendObj.recordkey = AddReqHiddenKey(); 
            //sendObj.iidplus = $("#hIidPlus").val();
            $("#" + Id).uploadify("settings", "formData", sendObj);
        }

        function UpLoadUserFile(Id) {
            if (!$('#' + Id).val()) {
                alert("请选择要上传的文件！");
                return;
            }
            var datas = jQuery.data($('#' + Id)[0], 'datas');
            if (datas.fileTypeExts && !Check_FileType($('#' + Id).val(), datas.fileTypeExts)) {
                alert("请选择设定的文件类型" + (datas.fileTypeDesc ? (":" + datas.fileTypeDesc) : "") );
                return;
            }
            var options = {
                url: '../appservice/UploadControl.ashx',
                type: 'post',
                data: datas,
                success: function(result) {
                    result = GetResultStr(result);
                    result = result.replace(/^<pre[^>]*>/i, "").replace(/<\/pre>$/i, "");
                    if (datas.onPlainSuccess) {
                        eval(datas.onPlainSuccess + "(result)");
                    } else {
                        if (result != "true") {
                            alert(result);
                        } else {
                            alert("上传成功！");
                        }
                    }
                }, error: function(p1, p2, p3) {
                    if (datas.onPlainErr) {
                        eval(datas.onPlainErr + "(p1, p2, p3)");
                    } else {
                        alert("上传失败！");
                    }
                }
            }
            if (datas.surl) options.url = datas.surl;
            options.data.fid = $("#hfid").val();
            options.data.iid = $("#hiid").val();
            options.data.input_index = $("#hinput_index").val();
            options.data.step = $("#hsctlid").val();
            options.data.userid = $("#huserid").val();
            options.data.wiid = $("#hwiid").val();
            options.data.recordkey = AddReqHiddenKey(); 
            $("form").ajaxSubmit(options);
        }

        function Check_FileType(str, exts) {
            var pos = str.lastIndexOf(".");
            var lastname = str.substring(pos, str.length);
            if (exts.toLowerCase().indexOf(lastname) < 0) {
                return false;
            }
            else {
                return true;
            }
        }

        //主要因为需要一些额外的信息
        function AddReqHiddenKey() {
            var keyplus = $("#hUserPara").val();
            var hidinput = $('input[dytype=26],textarea[dytype=26]').filter(function() {
                return ($(this).css('visibility') == 'hidden' || $(this).css('display') == 'none');
            });
            for (var i = 0; i < hidinput.length; i++) {
                if (keyplus.length > 0) keyplus += "|";
                keyplus += "[" + hidinput[i].id + "][" + hidinput[i].value + "]";
            }
            return keyplus;
        }
        
        //图形显示
        function ShowDyPic(objid)
        {
            var sObj = $("#" + objid);
            var sendObj = {};
            sendObj.iid = $("#hIidPlus").val(); //"<%=IidPlus%>";
            sendObj.fid = $("#hfid").val(); //"<%=fid%>";
            sendObj.action = "DyPicture";
            sendObj.input_index = $("#hinput_index").val(); //"<%=input_index%>";
            sendObj.objid = objid;
            sendObj.dsourceId = sObj.attr("dsourceId");
            sendObj.setId = sObj.attr("setId");
            sendObj.tbItem = sObj.attr("tbItem");
            $.ajax({
                url: 'GetData.aspx',
                type: 'Post',
                async: false,
                cache: false,
                data: sendObj,
                dataType: 'text',
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                },
                success: function(result) {
                    result = GetResultStr(result);
                    if (result.indexOf(backErrMsg) > 0) {
                        //alert("请求错误！请联系管理员查看日志。");
                        return;
                    }
                    sObj.attr("src", result);
                }
            });
        }

// <%--打印--%>//
        function CanPrint() {
            var canp = true;
            $("table[dytype='18']").each(function() {
                if (!$(this).attr("canprint")) canp = false;
            });
            return canp;
        }