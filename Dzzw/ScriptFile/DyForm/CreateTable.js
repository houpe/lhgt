﻿// JScript 文件

// JScript 文件
///创建表格的方法
///动态操作表格的方法只实现创建规则的表格
///------------------------------------------------------
///表格同一行的各个控件的ID一样，只是在前面加上控件的类型
///如：button 其ID为：button+strId(其中strId为模拟产生的唯一编号0
///只能在tbody中加

///模拟生成一个唯一的编号(16位)
///

function GetGuid()
{
     var strGuid="";
     for (var i=1; i<= 32;i++)
     { 
         var n = Math.floor(Math.random()*16.0).toString(16);
         strGuid += n;
         if((i == 8)||(i == 12)||(i == 16)||(i == 20))
         {
            strGuid += "-";
         }
      }
      return strGuid;
}

///
///创建一个删除按钮
///strId按钮的ID
function CreateDeleteButton(strId)
{
     var objButton= document.createElement("input");
     
     objButton.setAttribute("type","button");
     objButton.setAttribute("value","删除");
     objButton.setAttribute("id",strId);
     objButton.setAttribute("class","input");
     objButton.attachEvent("onclick",DeleteRow); //关联的删除事件这里只删除相应的行(仅仅隐藏行)
     return objButton;
}

///
///创建一个下拉列表
///arrText下拉列表对应的TEXT列表
///arrValue下拉列表对应的VALUE列表
///dlpId下拉列表对应的VALUE列表的id
///selectedText被选中的文本
function CreateDropList(arrText,arrValue,dlpId,selectedText)
{
      var objSelet= document.createElement("select");
      objSelet.setAttribute("id","select"+dlpId);
        
      for(var i=0;i<arrText.length;i++)
      {
         var objOption = document.createElement("option");
         if(arrText[i]!="")
         {
             objOption.setAttribute("text",arrText[i]);
             objOption.setAttribute("value",arrValue[i]);     
             objSelet.options.add(objOption);
         }
      }
      
      ///设置第几个被选中
      for(var i=0;i<objSelet.options.length;i++)
      {
          if(objSelet.options[i].text==selectedText ||objSelet.options[i].value ==selectedText)
          {
              objSelet.options[i].selected=true;
              break;
          }
      }
     
      return objSelet;
}

///
///创建一行
///strId行ID
function CreateRow(rowId)
{
    var row = document.createElement("tr");
    row.setAttribute("id","tr*"+rowId); //加tr是为了区分和删除按钮的ID相同
    
    return row;
}

///
///创建一个单元格子
///cellName单元格对应的字段名称
///cellId单元格子ID
function CreateCell(cellName,cellId)
{
    var cell= document.createElement("td");
    cell.setAttribute("id",cellName+cellId);
    return cell;
}

///
///隐藏要删除的行(通过隐藏来实现删除)
///
function DeleteRow()
{
    var id= "tr*" + window.event.srcElement.id; //要隐藏对应行的ID
    
    if(window.confirm("确认删除?"))
    {
        document.getElementById(id).style.display="none";
    }
}

///
///
///返回下拉列表选中的文本
///objId为DropList的id
function GetDropListSelectedText(objId)
{
    var text="";
    var objSelect= document.getElementById("select"+objId);

    for(var i=0;i<objSelect.options.length;i++)
    {
        if(objSelect.options[i].selected==true)
        {
           text=objSelect.options[i].text;
           break;
        }
    }
    return text;
}


///
///
///返回下拉列表选中的值
///objId为DropList的id
function GetDropListSelectedValue(objId)
{
    var value="";
    var objSelect= document.getElementById("select"+objId);
    
    for(var i=0;i<objSelect.options.length;i++)
    {
        if(objSelect.options[i].selected)
        {
           value=objSelect.options[i].value;
           break;
        }
    }
    return value;
}

///
///创建一个文本筐
///textBoxId要创建文本框的id
///textValue文本框的值
function CreateTextBox(textBoxId,textValue)
{
     var objButton= document.createElement("input");
     
     objButton.setAttribute("type","text");   
     objButton.setAttribute("value",textValue);
     objButton.setAttribute("id","text"+textBoxId);
     objButton.attachEvent("onblur",CheckValue);
     return objButton;
}

///
///返回文本框的值
///strId文本框的id
function GetTextBoxValue(strId)
{
     return document.getElementById("text"+strId).value;
}

///
///返回表格的行数
///
///
function GetTableRowsCount(objId)
{
    return document.getElementById(objId).rows.length;
}

///
///检测单元格的值是否正确
///
///
function CheckValue()
{
    var obj = window.event.srcElement;
    var values = obj.value;//单元格的文本值
    var regNumber = "^[0-9]*[1-9][0-9]*$";
    var regFloat = "^((0-9)+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*))$";
    if(values=="")
    {
       alert("文本框的值不能为空!");
       //obj.foucs();
    }
    
    //if(!regNumber.exec(values) &&!regFloat.exec(values)) //不匹配
    //{  
       //alert("请输入非负数字!");
       //obj.foucs();
   // } 
}

