﻿//主要是一些参照方法，参照表单

$(function() {
    ShowSignature();
});

var signatureLoaded = false;

//签名
function OpenUserSignSelect(obj) {
    var userSignVal = document.getElementById("hUserSign").value;
    var stype = $('#hSignType').val();
    if (userSignVal == "" && stype == '0') {
        var bEmpty = false;
        $.ajax({
            type: 'POST',
            async: false,
            cache: false,
            url: '../appservice/formsave.ashx',
            data: 'op=getsignid&userid=' + $("#huserid").val(),
            error: function() {
                bEmpty = true;
            },
            success: function(result) {
                if (result == '') {
                    bEmpty = true;
                }
            }
        });
        if (bEmpty) {
            alert("您没有上传签名，无法签名！");
            return;
        }
        var signuser = $("#hRequserid").val();
        if (!signuser)
            signuser = $("#huserid").val();
        var url = '../HtmlPage/SelectUserSign.aspx?dy=1&userid=' + signuser + '&ctlname=' + obj.id + '&fn=' + Math.round(Math.random() * 10000);

        var w = 420, h = 200;
        OpenNewWin(url, 2, w, h, "target2");
    } else {
        if (stype == '1') {
            if (!DoSXSignature2(obj.id)) return; //'86596d6269e8430999596fa2fd385b52'
            var signRes = HaveSignature(obj.id);
            SetSignResult(obj.id, signRes);
        } else {
            var newVal = formCtrlValOp(obj.id);
            if (newVal == userSignVal) {
                if (confirm("已有签名，是否清空签名？")) {
                    SetSignResult(obj.id, "");
                }
            } else {
                SetSignResult(obj.id, userSignVal);
            }
        }
    }
}

//被SelectUserSign.aspx使用
function CloseDialog() {
    $("#target2").window("close");
}

//被SelectUserSign.aspx使用
function SetSignResult(sid, newValue) {
    var sobj = $("#" + sid);
    if (newValue != sobj.attr("newval")) {
        sobj.attr("ischange", true);
        sobj.attr("newval", newValue);
        var newsrc = sobj.attr("src");
        newsrc = newsrc.replace(/UserCode=[^&]*&/g, 'UserCode=' + newValue + '&');
        sobj.attr("src", newsrc);
        //sobj.trigger("change");
        //eval('try {alert("\u7B7E\u540D\u6539\u53D8\uFF01");} catch (e) {alert("\u81EA\u5B9A\u4E49\u4E8B\u4EF6\u51FA\u9519\uFF1A" + e); }');//sobj.attr("onchange")
        //var cstr = 'try {alert("\u7B7E\u540D\u6539\u53D8\uFF01");} catch (e) {alert("\u81EA\u5B9A\u4E49\u4E8B\u4EF6\u51FA\u9519\uFF1A" + e); }';
        var cstr = sobj.attr("changestr");
        if (cstr) {
            cstr = cstr.replace(/this/g, '$("#' + sid + '")[0]');
            setTimeout(cstr, 500);
        }
    }
}

function DoSXSignature2(signPosi_) {
    if (!HaveSignControl()) {
        alert("请先安装金格HTML签章！");
        return false;
    }
    if (HaveSignature(signPosi_)) {
        alert("此处已有签章，请选择另外位置。");
        return false;
    }

    document.all.SignatureControl.DivId = "signPosi" + signPosi_;                     //签章所在的层
    document.all.SignatureControl.FieldsList = "recordIds=标识;";  

    //设置、读取手写签名笔颜色
    document.all.SignatureControl.SetPositionRelativeTag(signPosi_, 0);        //设置签章位置是相对于哪个标记的什么位置
    //form1.SignatureControl.Position(44,97);                           //手写签名位置
    document.all.SignatureControl.SaveHistory = "False";                    //是否自动保存历史记录,true保存  false不保存  默认值false
    document.all.SignatureControl.UserName = "lyj";                         //文件版签章用户
    //form1.SignatureControl.WebCancelOrder = 1; 		    //签章撤消原则设置, 0无顺序 1先进后出  2先进先出  默认值0
    document.all.SignatureControl.WebSetFontOther(true, "", "2", "宋体", 11, "$000000", "False"); //设置签章样式
    document.all.SignatureControl.DefaultSignTimeFormat = "1";
    document.all.SignatureControl.RunSignature(false);
    //form1.SignatureControl.RunHandWrite();  //执行手写签名
    return true;
}

function HaveSignature(signPosi_) {
    var mLength = document.getElementsByName("iHtmlSignature").length;
    var j = 0;
    for (var i = 0; i < mLength; i++) {
        var vItem = document.getElementsByName("iHtmlSignature")[i];
        if (vItem.DivId == "signPosi" + signPosi_) {
            return vItem.SignatureId + "|" + $(vItem).offset().left + "|" + $(vItem).offset().top;
        }
    }
    return "";
}

function HaveSignControl() {
    if (document.all && document.all.SignatureControl && (document.all.SignatureControl.SaveHistory != undefined))
        return true;
    return false;
}

//http://oa.goldgrid.com:70/BBS/showtopic-1849.aspx 
function ShowSignature() {
    if ($('#hSignType').val() == "0" || !HaveSignControl()) {
        signatureLoaded = true;
        return;
    }

    $("img[dytype=40]").each(function() {
        $(this).wrap('<div id="signPosi' + $(this).attr("id") + '" style=""></div>');
    });

    document.all.SignatureControl.ShowSignature($('#DocumentID').val());

    $("img[dytype=40]").each(function() {
        var mLength = document.getElementsByName("iHtmlSignature").length;
        for (var i = mLength - 1; i >= 0; i--) {
            var vItem = document.getElementsByName("iHtmlSignature")[i];
            vItem.EnableMove = false;
            if (vItem.DivId == "signPosi" + $(this).attr("id")) {
                var singval = formCtrlValOp(this.id);
                //alert(singval + "  " + singval.indexOf(vItem.SignatureId));
                if (singval.indexOf(vItem.SignatureId) == -1)//+ "|"
                    vItem.DeleteSignature();
            }
        }
    });
    
    signatureLoaded = true;
}