﻿//横向画流向图
//divID：div的id值, startX：流程图起画的x坐标, startY：流程图起画的y坐标,
//textArr：各岗位的名称, stepMsgArr:各岗位展示的消息集合,currentStepNo:当前岗位编号;
//passColor：当前岗位显示颜色；stepWidth：岗位间的宽度,AixFlag:代表横向0还是纵向1
function DrawFlow(divID, startX, startY, textArr, stepMsgArr, currentStepNo, passColor, stepWidth, AixFlag) {
    //数组初始化
    var posXArr = new Array(textArr.length); //存储X值
    var posYArr = new Array(textArr.length); //存储Y值

    var i, nAixFlag = 0;
    if (stepWidth != null && stepWidth > 0) {
        stepWidth = 80;
    }
    if (AixFlag != null) {
        nAixFlag = AixFlag;
    }

    for (i = 0; i < posXArr.length; i++) {
        if (nAixFlag == 0) {//横向
            posXArr[i] = startX + stepWidth * i;
            posYArr[i] = startY;
        }
        else {//纵向
            posXArr[i] = startX;
            posYArr[i] = startY + stepWidth * i;
        }
    }

    //绘制各岗位节点
    for (i = 0; i < posXArr.length; i++) {
        //绘制岗位图片
        var htmltemp = $(document.createElement('img'));
        htmltemp.css("position", "absolute");
        htmltemp.css("left", posXArr[i] + "px");
        htmltemp.css("top", posYArr[i] + "px");
        var imgsrc;
        switch (i) {
            case 0:
                imgsrc = "../App_Themes/SkinFile/Images/stepBegion.ico";
                break;
            case 1:
                imgsrc = "../App_Themes/SkinFile/Images/step.ico";
                break;
            case (posXArr.length - 1):
                imgsrc = "../App_Themes/SkinFile/Images/stepEnd.ico";
                break;
        }
        htmltemp.attr("src", imgsrc);
        htmltemp.css("cursor", "pointer");
        htmltemp.attr("id", "img" + i);
        htmltemp.attr("msgInfo", textArr[i]);
        htmltemp.attr("alt", stepMsgArr[i]);
        htmltemp.bind("click", function() {
            alert(this.msgInfo);
        });
        $(divID).append(htmltemp);

        //绘制岗位图片下的名称
        htmltemp = $(document.createElement('span'));
        htmltemp.html(textArr[i])
        htmltemp.css("position", "absolute");
        htmltemp.css("left", (posXArr[i] - 5) + "px");
        htmltemp.css("top", (posYArr[i] + 30) + "px");
        //判读是否是当前岗位
        if (i == (currentStepNo - 1)) {
            htmltemp.css("color", passColor);
        }
        $(divID).append(htmltemp);

        //显示岗位的提示消息
        htmltemp = $(document.createElement('span'));
        htmltemp.html(stepMsgArr[i])
        htmltemp.css("position", "absolute");
        if (nAixFlag == 0) {//横向
            htmltemp.css("left", (posXArr[i] - 5) + "px");
            htmltemp.css("top", (posYArr[i] + 50) + "px");
            htmltemp.css("width", "50px");
        } else {
            htmltemp.css("left", (posXArr[i] + 60) + "px");
            htmltemp.css("top", posYArr[i] + "px");
            htmltemp.css("width", "300px");
        }
        htmltemp.css("border", "solid 1px");
        //判读是否是当前岗位
        if (i == (currentStepNo - 1)) {
            htmltemp.css("color", passColor);
        } else {
            htmltemp.css("color", "green");
        }
        $(divID).append(htmltemp);
    }

    //划箭头【包括匹配的偏移量】
    var line = new Line();
    for (i = 0; i < posXArr.length - 1; i++) {
        var corlorInit = "#808080"

        //判读是否是当前岗位
        if (i <= (currentStepNo - 1)) {
            corlorInit = passColor;
        }
        line.setColor(corlorInit);

        if (nAixFlag == 0) {//横向
            line.drawArrowLine(posXArr[i] + 30, posYArr[i] + 15, posXArr[i + 1] - 1, posYArr[i + 1] + 15);
        } else {
            line.drawArrowLine(posXArr[i] + 15, posYArr[i] + 40, posXArr[i + 1] + 15, posYArr[i + 1] - 1);
        }
    }
}
