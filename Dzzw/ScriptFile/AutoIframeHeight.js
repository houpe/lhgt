﻿//自适应框架的高度
function SetCwinHeight(cwin) {
    if (document.getElementById) {
        if (cwin && !window.opera) {
            if (cwin.document && cwin.document.body.scrollHeight) {
                cwin.height = cwin.document.body.scrollHeight;
                //cwin.width = cwin.document.body.scrollWidth;
            }
            else if (cwin.contentDocument && cwin.contentDocument.body.offsetHeight) {
                cwin.height = cwin.contentDocument.body.offsetHeight;
            }
        }
    }
}

//为iframe自适应高度
//onload="this.height=frmChild.document.body.scrollHeight";//本语名适合在同一站点的情况
function SetWinHeight(obj)
{
   var win=obj;
   if (document.getElementById)
   {                  
     if (win && !window.opera)
     {
       if (win.contentDocument && win.contentDocument.body.offsetHeight) 
       {
            win.height = win.contentDocument.body.offsetHeight;                     
       }
       else if(win.Document && win.Document.body.scrollHeight)
       {
            win.height = win.Document.body.scrollHeight;
       }
       else//跨站点的情况,办理流程时.
       {
            win.height="450px";
       }

     }
   }
   
}