﻿/***************************************附件上传***************************************/

/***************************************单个附件上传***************************************/
/*
例1：
<div style="text-align: center" upfile='1' id='Div4' isworkflow='1' showname="上 传" 
type='' taskpath="竣工验收信息/竣工验收信息相关附件" defultname="竣工验收确认申请表" successfun="UploadSuccess(this)">
<span></span></div>
    

*/

$(function() {
    try {
        UpFile();
        createSwf();
        UpFileCoord();
    } catch (e) { }
});
function UpFile() {
    $.each($("div[upfile='1']"), function(n, i) {
        var uid = $(i).attr("id");
        var _id = $.query.get("id") || "";
        var _table = $(i).attr("table") || "";
        var _sourceid = $(i).attr("sourceid") || "";
        var _field = $(i).attr("field") || "";
        var _successfun = $(i).attr("successfun");
        var _type = $(i).attr("type") || "";
        var _sort = $(i).attr("sort") || ""; ;
        var _iid = $.query.get("iid") || "";
        var _indexfiled = $.query.get("indexfiled") || "";
        var _indexvalue = $.query.get("indexvalue") || "";
        var _change = $.query.get("change") || "";
        var _value = $(i).attr("showname") || "";
        var _taskpath = $(i).attr("taskpath") || "";
        
        var _stepid = $.query.get("stepid") || "";
        var _isworkflow = $(i).attr("isworkflow") || _iid;
        var _defultname = $(i).attr("defultname") || "";
        if (!_value || _value == "") {
            _value = "上传";
        }
        try {

            $.jUploader({
                button: uid, // 这里设置按钮id
                upload: _value,
                defultname: _defultname,
                iid: _iid,
                taskpath: _taskpath,
                action: Common.GetRootPath() + 'Model/Handler/FileUp.ashx?stepid=' + _stepid + '&isworkflow=' + _isworkflow + '&defultname=' + _defultname + '&taskpath=' + _taskpath + '&tablename=' + _table + '&fielename=' + _field + '&indexfiled=' + _indexfiled + '&indexvalue=' + _indexvalue + '&iid=' + _iid + '&type=' + _type + '&id=' + _id + '&sort=' + _sort + '&change=' + _change, // 这里设置上传处理接口，这个加了参数test_cancel=1来测试取消
                onUpload: function(fileName) {

                },
                // 上传完成事件
                onComplete: function(fileName, response) {
                    if (response.success) {
                        $(this).attr("color", "greed");
                        if (_sourceid && _sourceid != "") {
                            $("#" + _sourceid).attr("src", response.fileUrl);
                            $("#title").val(fileName.substr(0, fileName.indexOf('.')));
                        }
                        else if (_type && _type != "") {
                            window.open(getRootPath() + response.fileUrl);
                        }
                        else if (_successfun) {
                            eval(_successfun);
                        }
                    } else {
                        alert('上传失败');
                    }
                }
            });
        }
        catch (e) {
            alert("没有引用JQuery/jquery.jUploader.js!");
        }
    });
}
//附件上传成功后方法
function UploadSuccess(obj) {     
    alert("上传成功");
    try {
        parent.bindResource();
    }
    catch (e)
    { }
}
/***************************************单个附件上传 END***************************************/


/*************************************单个附件上传 SWF Start*************************************/
function createSwf() {
    var _iid = $.query.get("iid") || "";
    var _wiid = $.query.get("wiid") || "";
    var _enabled = "1"; //attDelRight(_iid, _wiid);
    $.each($("[upfile='2']"), function(i) {
        var uid = $(this).attr("id");                    //控件ID
        var _id = $.query.get("id") || $.query.get("indexvalue");
        var _table = $(this).attr("table") || "";
        var _sourceid = $(this).attr("sourceid") || "";
        var _loadfunction = "waitting";
        var _loadedfunction = "commpled";
        var _field = $(this).attr("field") || "";
        var _successfun = $(this).attr("successfun");
        var _type = $(this).attr("type") || "";           //上传的文件类型

        var _indexfiled = $.query.get("indexfiled") || ""; //页面主键
        var _indexvalue = $.query.get("indexvalue") || ""; //主键值
        var _change = $.query.get("change") || "";        //Word文件转换成哪种类型 pdf,swf,htmltxt,tiff,jpeg,png
        var _value = $(this).attr("showname") || "";
        var _taskpath = $(this).attr("taskpath") || "";
        
        var _isworkflow = $(this).attr("isworkflow") || "";
        var _defultname = $(this).attr("defultname") || "";
        var _filetype = $(this).attr("filetype") || "";    //限制上传文件类型

        if (!_value || _value == "") {
            _value = "1";
        }
        if (_isworkflow == "" && _defultname == "") {
            _enabled = 1;
        }
        var _maxsize = GetConfig("ftp.maxsize");
        var _path = _taskpath + "/" + _defultname;
        var isupfile = "1"; //findAttFile(_iid, _path) ? "1" : "0";
        var userid = GetParam("userid");
        _indexvalue = GetParam(_indexvalue.replaceAll("{", "").replaceAll("}", ""));
        var upfileurl = Common.GetRootPath() + "Model/Handler/FileUp.ashx";
        var parameters = '?upfileurl=' + upfileurl + '&maxsize=' + _maxsize + '&isupfile=' + isupfile + '&enabled=' + _enabled + '&lablename=' + _value + '&filetype=' + _filetype + '&isworkflow=' + _isworkflow + '&defultname=' + _defultname + '&taskpath=' + _taskpath + '&tablename=' + _table + '&fielename=' + _field + '&indexfiled=' + _indexfiled + '&indexvalue=' + _indexvalue + '&iid=' + _iid + '&type=' + _type + '&id=' + _id + '&change=' + _change + '&userid=' + userid + "&loadfunction=" + _loadfunction + "&loadedfunction=" + _loadedfunction;
        var uid = $(this).attr("id");

        $(this).hide();

        var _swfpath = Common.GetRootPath() + "Script/Plug/Flex/FileUpSingle.swf";
        var _embed = "<embed src='FileUpSingle.swf' quality='high' bgcolor='#869ca7' name='FileUpSingle' align='middle' allowscriptaccess='sameDomain' flashvars='?" + parameters + "' type='application/x-shockwave-flash' wmode='transparent' pluginspage='http://www.macromedia.com/go/getflashplayer' /></embed>"
        var myswf = $("<object flag='objup' classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000' id='SWFUpload_" + uid + "' width='74' height='23'   data='" + _swfpath + parameters + "'    type='application/x-shockwave-flash' > <param name='movie' value='" + _swfpath + parameters + "'/><param name='FlashVars' value='" + parameters + "'/><param name='wmode' value='transparent'>" + _embed + "</object>");
        $(this).after(myswf);
    });
}

/*
win  2003 下能正常访问
<object id="SWFUpload_Div4" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
data="http://localhost/FrameWork/Script/Plug/Flex/FileUpSingle.swf?maxsize=10&isupfile=1&enabled=1&lablename=1&filetype=&stepid=&isworkflow=1&defultname=2&taskpath=1&tablename=&fielename=&indexfiled=&indexvalue=&iid=&type=&itemtype=&id=&sort=&usersign=&change=&tzs=undefined_&loadfunction=waitting&loadedfunction=commpled&upfileurl=http://localhost/FrameWork/Model/Handler/FileUp.ashx"
width="740" type="application/x-shockwave-flash" height="323">
<param name="movie" value="http://localhost/FrameWork/Script/Plug/Flex/FileUpSingle.swf?maxsize=10&isupfile=1&enabled=1&lablename=1&filetype=&stepid=&isworkflow=1&defultname=2&taskpath=1&tablename=&fielename=&indexfiled=&indexvalue=&iid=&type=&itemtype=&id=&sort=&usersign=&change=&tzs=undefined_&loadfunction=waitting&loadedfunction=commpled&upfileurl=http://localhost/FrameWork/Model/Handler/FileUp.ashx" />
<param name="wmode" value="transparent" />
<param name="FlashVars" value="?maxsize=10&isupfile=1&enabled=1&lablename=测试&filetype=&stepid=&isworkflow=1&defultname=测试&taskpath=1&tablename=&fielename=&indexfiled=&indexvalue=&iid=&type=&itemtype=&id=&sort=&usersign=&change=&tzs=undefined_&loadfunction=waitting&loadedfunction=commpled&upfileurl=http://localhost/FrameWork/Model/Handler/FileUp.ashx"/>
        
</object>

*/

/***************************************单个附件上传 SWF END***************************************/


function UpFileCoord() {
    $.each($("div[upfile='3']"), function(i) {
        var uid = this.id;
        
        var _successfun = this.successfun;
        var _iid = $.query.get("iid") || "";
        if(_iid=="" || _iid==null)
        {
        
            alert("如没有传递业务编号，则上传按钮不会显示");
            return;
        }
        var _value = this.showname || "";
        if (!_value || _value == "") {
            _value = "上传";
        }
        
        try {

            $.jUploader({
                button: uid, // 这里设置按钮id
                upload: _value,
                defultname: "",
                iid: _iid,
                taskpath: "",
                action: Common.GetRootPath() + 'Model/Handler/FileUpCoordInfo.ashx?iid=' + _iid, // 这里设置上传处理接口，这个加了参数test_cancel=1来测试取消
                onUpload: function(fileName) {
                   
                },
                // 上传完成事件
                onComplete: function(fileName, response) {
                    if (response.success) {
                        $(this).attr("color", "greed");
                        if (_successfun) {
                            eval(_successfun);
                        }
                    } else {
                        alert(response.msg);
                    }
                }
            });
        }
        catch (e) {
            alert("没有引用JQuery/jquery.jUploader.js!");
        }
    });
}

function commpled() {
     
}