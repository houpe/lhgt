﻿//获取报表配置信息
function ReportHtml(repartname) {
    var reportqueryid = "reportquery";
    $("#" + reportqueryid).css("margin-top", "10px")
    var json = TableData("config_report", "*", "  and rid='" + repartname + "'", "").toJson();
    if (json.length > 0) {
        for (var i = 0; i < json.length; i++) {
            for (var key in json[i]) {
                if (key != "id") {
                    $('<input id="' + key + '" type="hidden" value="' + json[i][key] + '" />').appendTo($("body"));
                }
            }
        }
        var rqueryfield = eval($("#rqueryfield").val());
        var rqueryfieldtext = eval($("#rqueryfieldtext").val());
        var rqueryfieldtype = eval($("#rqueryfieldtype").val());
        var rqueryfieldid = eval($("#rqueryfieldid").val());
        var dictionarys = eval($("#dictionarys").val());
        var rqueryfielddefult = eval($("#rqueryfielddefult").val());
        var rqueryfielddatatype = eval($("#rqueryfielddatatype").val());
        var datatable = eval($("#datatable").val());
        var datafield = eval($("#datafield").val());
        var rauto = eval($("#rauto").val());
        if (rqueryfield.length > 0 && rqueryfield[0] != "") {
            for (var r = 0; r < rqueryfield.length; r++) {
                $("#" + reportqueryid).show();
                var rtype = rqueryfieldtype[r];
                var data_table = datatable.length >= r ? datatable[r] : "";
                var data_field = datafield.length >= r ? datafield[r] : "";
                var data_rauto = rauto.length >= r ? rauto[r] : "";
                var query_fieldtext = rqueryfieldtext.length >= r ? rqueryfieldtext[r] : "";
                var query_fieldid = rqueryfieldid.length >= r ? rqueryfieldid[r] : "";
                var query_fielddatatype = rqueryfielddatatype.length >= r ? rqueryfielddatatype[r] : "";
                var query_field = rqueryfield.length >= r ? rqueryfield[r] : "";
                var query_dictionarys = dictionarys.length >= r ? dictionarys[r] : "";
                var query_fielddefult = rqueryfielddefult.length >= r ? rqueryfielddefult[r] : "";

                switch (rtype) {
                    case "select":
                        $("<span>" + query_fieldtext + "</span>").css("margin-left", "10px").appendTo($("#" + reportqueryid));
                        $("<div id='" + query_fieldid + "' fieldtype='" + query_fielddatatype + "' class='selectTree'  trueType='selectTree'  queryfield='" + query_field + "' dictionary='" + query_dictionarys + "' defultvalue='" + query_fielddefult + "'></div>  ").css("margin-left", "10px").appendTo($("#" + reportqueryid));
                        break;
                    default:
                        $("<span>" + query_fieldtext + "</span>").css("margin-left", "10px").appendTo($("#" + reportqueryid));
                        $("<input id='" + query_fieldid + "' datatable='" + data_table + "'  datafield='" + query_field + "' auto='" + data_rauto + "' type='text' fieldtype='" + query_fielddatatype + "' queryfield='" + query_field + "'  defultvalue='" + query_fielddefult + "' class='formText'/>  ").css("margin-left", "10px").appendTo($("#" + reportqueryid));
                        break;
                }
            }


            $("<input type='button' value='查询' class='formButton' onclick='ShowReport();'  />").css("margin-left", "10px").appendTo($("#" + reportqueryid));
            $("<input type='button' value='重置' class='formButton'/>").click(function() {
                $("[queryfield]").val("");
                if ($("[queryfield]").hasClass("selectTree"))
                    $("[queryfield]").resetValue();
                ShowReport();
            }).css("margin-left", "10px").appendTo($("#" + reportqueryid));
        }
        $('<input id="rwherevaluedynamic" type="hidden" value="" />').appendTo($("body"));
    }
    InitDropDownList("");


    ShowReport();
}

//展示报表
function ShowReport() {
    reportWhere();
    var rshowid = $("#rshowid").val();
    rshowid = rshowid ? "#" + rshowid : "body";
    $(rshowid).html("");
    var rbodycontent = $("#rbodycontent").val();
    var rtablehand = $("#rtablehand").val();
    var rtablename = $("#rtablename").val();

    var rfileds = $("#rfileds").val().replaceAll(',', ';');
    var rwherevalue = $("#rwherevalue").val().replaceAll(',', ';');
    var rwherevaluedynamic = $("#rwherevaluedynamic").val();
    var rsumfileds = $("#rsumfileds").val();
    var rwidths = eval($("#rwidths").val());
    var wherevalue = rwherevaluedynamic + rwherevalue;
    wherevalue = wherevalue.replaceAll(',', ';')
    //var json = TableData(rtablename, rfileds, wherevalue, "").toJson();/*2014-1-2 han注释掉*/
    var json = TableData(escape("(" + rtablename + ")"), rfileds, escape(wherevalue), escape("")).toJson();
    var contents = rbodycontent.split("{");
    for (var c = 0; c < contents.length; c++) {
        if (c > 0) {
            var cids = contents[c].split("}");
            var cValue = $("#" + cids[0]).val();
            if ($("#" + cids[0]).attr("dictionary")) {
                cValue = $("#" + cids[0]).find("option:selected").text();
            }
            rbodycontent = rbodycontent.replaceAll("{" + cids[0] + "}", cValue);
        }
    }
    $(rshowid).append("<div class='divcenter'><h1>" + rbodycontent + "</h1></div>");
    var rleftcontent = $("#rleftcontent").val();
    if (rleftcontent) {
        $(rshowid).append("<br/><div class='right'>" + rleftcontent + "</div>");
    }
    var tmpTable = $('<table id="tableReport" class="tabtj" cellspacing="0" border="1"  style="width:100%;border-collapse:collapse;"></table>');
    var tmpHead = $("<thead></thead>");
    $(tmpHead).append(rtablehand);
    var tmpFoot = $("<tfoot></tfoot>");
    if (json.length > 0) {

        var tmpBody = $("<tbody></tbody>");
        for (var i = 0; i < json.length; i++) {
            var rowClass = "Row";
            if (i % 2 == 0) {
                rowClass = "AlternatingRow";
            }
            var widths = "";
            if (rwidths && rwidths.length >= kindex) {
                widths = "width:" + rwidths[kindex];
            }
            var kindex = 0;
            var style = "";
            if (i > -1) {
                if (rwidths && rwidths.length >= kindex) {
                    style = 'style="width:' + rwidths[kindex] + '"';
                }
                kindex++;
            }
            var tmpRow = $("<tr style='height:25px' class='" + rowClass + "'><td " + style + ">" + (i + 1) + "</td></tr>");

            for (var key in json[i]) {
                style = "";
                if (i > -1) {
                    if (rwidths && rwidths.length >= kindex) {
                        style = 'style="width:' + rwidths[kindex] + '"';
                    }
                    kindex++;
                }
                $(tmpRow).append('<td ' + style + '>' + json[i][key].replace("0:00:00", "") + '</td>');
            }
            $(tmpBody).append(tmpRow);
        }
        $(tmpTable).append(tmpBody);

    }
    $(tmpHead).appendTo(tmpTable);
    $(tmpFoot).appendTo(tmpTable);
    $(tmpTable).appendTo($(rshowid));

}

/*构建动态报表where条件*/
function reportWhere() {
    var queryobjs = $("[queryfield]");
    var dynamicWhere = "";
    for (var oq = 0; oq < queryobjs.length; oq++) {
        var o_fieldtype = $(queryobjs[oq]).attr("fieldtype");
        var o_queryfield = $(queryobjs[oq]).attr("queryfield");
        var o_value = $(queryobjs[oq]).val();
        switch (o_fieldtype) {
            case "select":
                o_value = $(queryobjs[oq]).attr("relText");
                if (o_value == "请选择")
                    o_value = "";
                break;
            default:
                break;
        }
        if (o_value && o_value != "") {
            switch (o_fieldtype) {
                case "year":
                    dynamicWhere += " and " + o_queryfield + ">=to_date('" + o_value + "-01-01','yyyy-mm-dd')";
                    dynamicWhere += " and " + o_queryfield + "<=to_date('" + o_value + "-12-31','yyyy-mm-dd')";
                    break;

                case "quarter":
                    dynamicWhere += " and " + o_queryfield + ">=to_date('" + $("#year").val() + "-" + o_value + "-01','yyyy-mm-dd')";
                    if ($(queryobjs[oq]).val() != "10") {
                        dynamicWhere += " and " + o_queryfield + "<to_date('" + $("#year").val() + "-" + (eval(o_value) + 3) + "-01','yyyy-mm-dd')";
                    }
                    else if ($(queryobjs[oq]).val() == "10") {
                        dynamicWhere += " and " + o_queryfield + "<to_date('" + $("#year").val() + "-12-31','yyyy-mm-dd')";
                    }
                    break;
                case "select":
                    dynamicWhere += " and " + o_queryfield + " = '" + o_value + "'";
                    break;
                case "text":
                    dynamicWhere += " and " + o_queryfield + " like '%" + o_value + "%'";
                    break;
            }
        }
    }
    $("#rwherevaluedynamic").val(dynamicWhere);
    $("#hidWhere").val(dynamicWhere);
}
function htmlToExcel(divid) {
    reportWhere();
    var tablename = $("#rtablename").val();
    var datahead = $("#rtablehand").val();
    var showfield = $("#rfileds").val();
    var rwherevalue = $("#rwherevalue").val()
    var rwherevaluedynamic = $("#rwherevaluedynamic").val();
    var wherevalue = rwherevaluedynamic + rwherevalue;
    var rbodycontent = $("#rbodycontent").val();
    var contents = rbodycontent.split("{");
    datahead = datahead.replaceAll("td", "th");
    $.cookie("hander", datahead);
    for (var c = 0; c < contents.length; c++) {
        if (c > 0) {
            var cids = contents[c].split("}");
            var cValue = $("#" + cids[0]).val();
            if ($("#" + cids[0]).attr("dictionary")) {
                cValue = $("#" + cids[0]).find("option:selected").text();
            }
            rbodycontent = rbodycontent.replaceAll("{" + cids[0] + "}", cValue);
        }
    }
    var title = rbodycontent;

    //window.open(Common.GetRootPath() + "Model/Export/ExportExcel.aspx?tablename=" + Common.Encode(tablename) + "&title=" + Common.Encode(title) + "&hander=" + "&where=" + wherevalue + "&fileds=" + Common.Encode(showfield));

    post(Common.GetRootPath() + "Model/Export/ExportExcel.aspx",
    {
        tablename: Common.Encode(tablename),
        title: Common.Encode(title),
        hander: '',
        where: Common.Encode(wherevalue),
        fileds: Common.Encode(showfield)
    });
}

function post(URL, PARAMS) {
    var temp = document.createElement("form");
    temp.action = URL;
    temp.method = "post";
    temp.style.display = "none";
    for (var x in PARAMS) {
        var opt = document.createElement("textarea");
        opt.name = x;
        opt.value = PARAMS[x]; // alert(opt.name)                 
        temp.appendChild(opt);
    }
    document.body.appendChild(temp);
    temp.submit();
    return temp;
}