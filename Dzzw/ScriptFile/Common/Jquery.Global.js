﻿/*创 建 人：LiuY
创建时间：2013-3-14
案例说明：获取网站后台配置
function GetConfig(name) {
var str = GetServerData( "SystemConfig", "GetConfig", name,"c");
return str;
}

参数说明：类,方法,参数,命名空间（可选：默认:b:SbBusiness；c:ChCommon）,回调函数（可选）
*/
function GetServerData(clssname, method, parameters, assembly, callback) {
    var _val = "";
    if (!assembly) {
        assembly = "b";
    } alert(parameters);
    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: Common.GetServicePath() + "WebService/JqueryService.asmx/AjaxMethod",
        data: '{ asb: "' + assembly + '", cls: "' + clssname + '", mth: "' + method + '", param: "' + parameters + '" }',
        dataType: 'json',
        async: false,
        error: function (message) {
            alert(message.responseText);
        },
        success: function (data) {
            if (!callback || callback == undefined || callback == "") {
                _val = data.d;
            }
            else {
                callback(data.d);
            }
        }
    });
    return _val;
};

function GetServerDataJson(clssname, method, parameters, assembly, callback) {
    var _val = "";
    if (!assembly) {
        assembly = "b";
    }
    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: Common.GetServicePath() + "WebService/JqueryService.asmx/AjaxMethodJson",
        data: '{ asb: "' + assembly + '", cls: "' + clssname + '", mth: "' + method + '", param:"' + parameters + '"}',
        dataType: 'json',
        async: false,
        error: function (message) {
            alert(message.responseText);
        },
        success: function (data) {
            if (!callback || callback == undefined || callback == "") {
                _val = data.d;
            }
            else {
                callback(data.d);
            }
        }
    });
    return _val;
};

//请求service通用方法
function GetServiceData(method, parameters, callback, loadItemId) {
    var _val = "";
    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: Common.GetServicePath() + "WebService/JqueryService.asmx/" + method,
        data: parameters,
        dataType: 'json',
        async: false,
        error: function (message) {
            alert(message.responseText);
        },
        success: function (data) {
            if (!callback || callback == undefined || callback == "") {
                _val = data.d;
            }
            else {
                callback(loadItemId, data.d);
            }
        }
    });
    return _val;
};

//jquery实现对webservice的调用
function WebService(url, callback, pars) {
    $.ajax({
        data: pars,
        url: url,
        type: "POST",
        contentType: "application/json;utf-8",
        dataType: 'json',
        async: false,
        cache: false,
        success: function (json) {
            callback(json.d);
        },
        error: function (xml, status) {
            if (status == 'error') {
                try {
                    var json = eval('(' + xml.responseText + ')');
                    alert(json.Message + '\n' + json.StackTrace);
                } catch (e) { }
            } else {
                alert(status);
            }
        },
        beforeSend: function (xml) {
            if (!pars) xml.setRequestHeader("Content-Type", "application/json;utf-8")
        }
    });
}

//获取参数
function GetParam(type) {
    var result = "";
    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: Common.GetServicePath() + "WebService/JqueryService.asmx/ParamChange",
        data: "{param:'{" + type + "}'}",
        dataType: 'json',
        async: false,
        success: function (data) {
            result = data.d;
        }
    });
    return result;
}

//获取dataTable并返回json
function CommonData(clssname, method, parameters) {
    var json = GetServerData(clssname, method, parameters, "c");
    return json;
}

//解析Json格式并构建table数据(行政许可事项列表)
function createCommonTable(strDivId, json) {
    json = json.toJson(); //格式化
    var table = $("<table class='window_tab_list_all'></table>");
    for (var i = 0; i < json.length; i++) {
        o1 = json[i];
        var row = $("<tr></tr>");
        for (key in o1) {
            var td = $("<td></td>");
            td.text(o1[key].toString());
            td.appendTo(row);
        }
        row.appendTo(table);
    }
    table.appendTo($("#" + strDivId));
}

function BussinessHandle(clssname, method, parameters) {
    var str = GetServerData("Handle." + clssname, method, parameters, "b");
    return str;
}
function BussinessTools(clssname, method, parameters) {
    var str = GetServerData("Tools." + clssname, method, parameters, "b");
    return str;
}

//获取网站配置文件中的信息
function GetConfig(name) {
    var json = GetServerDataJson("ConfigHelper", "GetConfig", "{ configKey:'" + name + "'}", "c");
    return json;
}



//数据字典翻译成中文
function fieldCodeDataRender(r, n, value) {
    for (var i = 0, l = dictionaryCodeData.length; i < l; i++) {
        var o = dictionaryCodeData[i];
        if (o.value == value) {
            return o.text;
        }
    }
    return value;
}

//表格滚动方法
function scrollText() {
    var speed = 50; //数字越大速度越慢
    var tab = document.getElementById("demo");
    var tab1 = document.getElementById("demo1");
    var tab2 = document.getElementById("demo2");
    //tab2.innerHTML = tab1.innerHTML; //克隆demo1为demo2
    function Marquee() {
        if (tab2.offsetTop - tab.scrollTop <= 0)//当滚动至demo1与demo2交界时
            tab.scrollTop -= tab1.offsetHeight //demo跳到最顶端
        else {
            tab.scrollTop++
        }
    }
    var MyMar = setInterval(Marquee, speed);
    tab.onmouseover = function () { clearInterval(MyMar) }; //鼠标移上时清除定时器达到滚动停止的目的
    tab.onmouseout = function () { MyMar = setInterval(Marquee, speed) }; //鼠标移开时重设定时器
}

//设置相应html元素的内容
function LoadMsgText(loadItemId, strInfo) {
    if (strInfo == "") {//session过期则退出
        //LoginOut();
    }
    else {
        $("#" + loadItemId).html(strInfo);
    }
}

//注销
function LoginOut() {
    var loginPage = getCookie("loginPageUrl");
    window.document.location.href = loginPage;
}

//显示进度条
function ShowLoading() {
    //定时器2秒
    $.messager.progress({
        title: '请稍候',
        msg: '数据加载中...'
    });
}

//隐藏进度条
function HideLoading() {
    $.messager.progress('close');
}
//设置为主页
function SetPageHome() {
    this.style.behavior = 'url(#default#homepage)';
    this.setHomePage('http://shenpi.sbsm.gov.cn');
}

//打开弹出框
function clickUrl(strUrl) {
    window.open(strUrl);
}

//获取当前日期
function GetCurrentDate() {
    var date = new Date();
    this.year = date.getFullYear();
    this.month = date.getMonth() + 1;
    this.date = date.getDate();
    this.day = new Array("星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六")[date.getDay()];
    this.hour = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
    this.minute = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
    this.second = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();
    var currentTime = "今天是:" + this.year + "年" + this.month + "月" + this.date + "日 " + this.hour + ":" + this.minute + ":" + this.second + " " + this.day;
    return currentTime;
}
