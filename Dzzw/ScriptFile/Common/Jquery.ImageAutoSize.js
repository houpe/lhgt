﻿//限制图片大小
// $("图片组所在的容器").ImageAutoSize(限制最大宽,限制最大高);
jQuery.fn.ImageAutoSize = function(width, height) {
    $("img", this).each(function() {
        var image = $(this);
        if (image.width() > width) {
            image.width(width);
            image.height(width / image.width() * image.height());
        }
        if (image.height() > height) {
            image.height(height);
            image.width(height / image.height() * image.width());
        }
    });
}