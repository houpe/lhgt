﻿

function DropDownList() {
    var rootpath = Common.GetRootPath();
    this.objs;
    var _this = this;
    this.bind = function() {
        //此代码可以优化一下 减少循环
        for (var o = 0; o < _this.objs.length; o++) {
            var multiMode = $(_this.objs[o]).attr("multiMode");
            var dict = $(_this.objs[o]).attr("dictionary");

            if (dict && dict != "") {
                var _data = { treeNodes: [] };
                var _nodes = IniDictionarytData(dict);
                var _parentid = ShowPatentId(_nodes);

                var _firstnodes = { id: "", name: "清除选项", open: true, icon: rootpath + "Theme/Images/Icon/delete.png" };

                for (var i = _nodes.length; i > 0; i--) {
                    var _sonnodes = Common.query(_nodes, '@parentId="' + _nodes[i - 1].id + '"'); //查询是否有子项                    
                    if (_sonnodes == "" || _sonnodes.length == 0) {
                        _nodes[i] = { name: _nodes[i - 1].name, id: _nodes[i - 1].id, parentId: _nodes[i - 1].parentId, open: _nodes[i - 1].open, clickExpand: _nodes[i - 1].clickExpand, nocheck: _nodes[i - 1].nocheck, icon: rootpath + "Theme/Images/Icon/page.png" };
                    }
                    else {
                        _nodes[i] = { name: _nodes[i - 1].name, id: _nodes[i - 1].id, parentId: _nodes[i - 1].parentId, open: _nodes[i - 1].open, clickExpand: _nodes[i - 1].clickExpand, nocheck: _nodes[i - 1].nocheck, open: false };
                    }

                }
                _nodes[0] = _firstnodes;
                if (multiMode && multiMode == "1") {
                    _nodes.shift(); //移除最前一个元素并返回该元素值，数组中元素自动前移
                }
                _data.treeNodes = _nodes;
                $(_this.objs[o]).data("data", _data);
                $(_this.objs[o]).render();
            }
        }
    }
}

function ShowPatentId(_nodes) {
    var parentid = "-1";
    for (var i = 0; i < _nodes.length; i++) {
        var _nodesparentid = _nodes[i].parentId;
        if (_nodesparentid != parentid) {
            parentid = _nodesparentid;
            break;
        }
    }
    return parentid;
}


/**********************************************      附件管理模块Start      **************************************************************/
/*
eg: <a class="attach" extname="pdf" aid="e2c68e0a-99e9-46bb-9a02-bb4c9d321197" apath="ftpuser:ftpuser@192.168.0.110:21@2013\08\29\a702e18206674bb7be7253c011bf3adc.xls">我的档案</a>
*/
//附件初始化
function InitAttachment() {
    var objs = $(".attach");
    var OsVersion = BussinessTools("OSVersion", "ServiceOSVersion", "1");
    var loginname = "";
    for (var o = 0; o < objs.length; o++) {
        if (loginname == "") {
            loginname = GetParam("userid");
        }
        var extname = $(objs[o]).attr("extname").replace(".", "");
        var name = $(objs[o]).html();
        var spanclass = "icon_" + extname.toLocaleLowerCase();
        var spandel = '';
        var spanview = '';
        var apath = " apath = '" + $(objs[o]).attr("apath") + "' aid='" + $(objs[o]).attr("aid") + "'";
        var spandown = '<a href="javascript:;"><span class="icon_btn_down" ' + apath + ' onclick="FileDown(this)">下载</span></a>';
        if (loginname == GetConfig("adminUserid")) {
            spandel = '<a href="javascript:;"><span class="icon_delete" ' + apath + '  onclick="FileDelete(this)">删除</span></a>';
        }
        if (OsVersion == "win 2003") {
            spanview = '<a href="javascript:;"><span class="icon_view" ' + apath + '  onclick="FileView(this)">预览</span></a>';
        }
        var $menu = $('<div class="popupMenu">' +
				'<div class="popupMenu"><span class="' + spanclass + ' hand">' + name + '</span><br />' +
					'<div class="popupMenu_con icon_con">' + spanview + spandown + spandel +
				'</div></div>')
        $(objs[o]).after($menu);
        $(objs[o]).hide();
        $menu.render();
    }
}
function FileView(obj) {
    var apath = $(obj).attr("apath");
    if (apath != "") {
        var extend = Common.Extend(apath);
        var npath = apath.replace(extend, ".swf").replaceAll("\",\"/");
        npath = /\@[^\@]+$/.exec(npath).join("").replaceAll("@", "");
        var filepath = GetConfig("ftpService") + npath;
        window.open(filepath);
    }
    else {
        alert("找不到文件路径");
    }
}

function FileDown(obj) {
    var apath = $(obj).attr("apath");
    if (apath != "") {
        var npath = /\@[^\@]+$/.exec(apath).join("").replaceAll("@", "");
        var filepath = GetConfig("ftpService") + npath;         
        window.open(filepath);
    }
    else {
        alert("找不到文件路径");
    }
}
function FileDelete(obj) {
    if (window.confirm("确认删除")) {
        var aid = $(obj).attr("aid");
        var params = ",," + aid + ",{userid}";
        var msg = BussinessHandle("FtpFileHandle", "DelAttachment", params);
        if (msg == "") {
            $("[aid='" + aid + "']").parent().remove();
            alert("删除成功");
        }
        else {
            alert(msg);

        }
    }
}

/**********************************************      附件管理模块 End       **************************************************************/



function DropDownWhere(_this, obj) {
    var sorcevalue = $(obj).attr("relValue");
    var fromkey = $(_this).attr("fromkey");
    var where = " and " + fromkey + "='" + sorcevalue + "'";
    return where;

}

function IniDictionarytData(dictionary) {
    var match = Common.query(dictionaryCodeData, '@type="' + dictionary + '"');
    return match;
}

function InitForID() {
    var forids = $(document).find(":text[forid],div[forid]");
    for (var o = 0; o < forids.length; o++) {
        var _this = $(forids[o]);
        var forid = _this.attr("forid");

        if (forid != "") {
            _this.attr("changeforid", forid);
            _this.bind("change", function() {
                ChangeData(this);
            });

        }
    }
}
//function ChangeData(id) {
//    DropDownListBindData($("#" + id));
//}
function ChangeData(obj) {
    var objid = $(obj).attr("changeforid");
    DropDownListBindData($("#" + objid));
}



function DropDownListBindData(obj) {
    var _this = obj;
    var rootpath = Common.GetRootPath();
    var datatable = _this.attr("datatable");
    var datafieldshow = _this.attr("datafieldshow");
    var datafieldval = _this.attr("datafieldval");
    var dataparentfield = _this.attr("dataparentfield") == undefined ? "" : _this.attr("dataparentfield");
    var datawhere = _this.attr("datawhere") == undefined ? "" : _this.attr("datawhere");
    var fromid = _this.attr("fromid") == undefined ? "" : _this.attr("fromid");
    var multiMode = _this.attr("multiMode") == undefined ? "" : _this.attr("multiMode"); 
    if (datatable != "" && datafieldshow != "" && datafieldval != "") {
        var _data = { treeNodes: [] };
        var _parentfield = dataparentfield != "" ? ";" + dataparentfield : "";

        if (fromid != "") {
            datawhere = DropDownWhere(_this, $("#" + fromid)) + datawhere;
        }
        var queryfield = datafieldshow + ";" + datafieldval + _parentfield;
        if (datafieldshow == datafieldval) {
            queryfield = datafieldshow + _parentfield;
        }
        var _json = TableData(datatable + " a", queryfield, datawhere.replaceAll(">", "'")).toJson();
        
        if ( multiMode == "") {
            var _firstnodes = { id: "", name: "清除选项", open: true, icon: rootpath + "Theme/Images/Icon/delete.png" };
            _data.treeNodes[_data.treeNodes.length] = _firstnodes;             
        }
        for (var i = 0; i < _json.length; i++) {
            var id = _json[i][datafieldval];
            var name = _json[i][datafieldshow];
            var parentid = dataparentfield != "" ? _json[i][dataparentfield] : "0";
            var clexpand = _json[i]["isparent"];
            var clickExpand = "false";
            if (clexpand && clexpand == "1") {
                clickExpand = "true";
            }
            //查询是否有子项
            var _sonnodes = Common.query(_json, '@' + dataparentfield + '="' + id + '"');
            var _nodes;
            if (_sonnodes == "" || _sonnodes.length == 0) {
                _nodes = { id: id, name: name, parentId: parentid, open: true, clickExpand: clickExpand, icon: rootpath + "Theme/Images/Icon/page.png" };
            }
            else {
                _nodes = { id: id, name: name, parentId: parentid, open: true, clickExpand: clickExpand,open: false };
            }
            _data.treeNodes[_data.treeNodes.length] = _nodes;
        }
        _this.data("data", _data);
        _this.render();
    }
}

function DropDownListExt() {

    var objs = $(document).find("[datatable][datafieldshow][datafieldval]");
    for (var o = 0; o < objs.length; o++) {
        var fromid = $(objs[o]).attr("fromid");
        //if (!fromid) {
        DropDownListBindData($(objs[o]));
        //}

    }
    InitForID();
}

//绑定
function InitDropDownList() {
    var drownlist = new DropDownList();
    drownlist.objs = $(document).find("[dictionary]");
    drownlist.bind();
}

//初始化页面数据
function InitPageData(_callback, _where) {
    var _indexfield = $.query.get("indexfield");
    var _indexvalue = $.query.get("indexvalue");
    var msg = "";
    var _tables = GetPageTables();
    if (_tables.length > 0) {
        var _data = CommonData("PageHandle", "PageJson", _tables + "," + _indexfield + "," + _indexvalue + "," + _where);
        if (_data && _data != undefined && _data != "") {
            bindControl(_data, _callback);

            //绑定数据字典转值
            spanDictionary();
        }
    }
}
//判断是否有多行显示
function showMoreFieldRow(tablename) {
    var clonetype = $("[clone][table='" + tablename + "'][sort][indexfield]");
    var thisfields = new Array();
    if (clonetype.length > 0) {
        for (var i = 0; i < clonetype.length; i++) {
            thisfields[thisfields.length] = clonetype[i].indexfield;
        }
    }
    return thisfields;
}
function showMoreValueRow(tablename) {
    var clonetype = $("[clone][table='" + tablename + "'][sort][indexfield]");
    var thisvalues = new Array();
    if (clonetype.length > 0) {
        for (var i = 0; i < clonetype.length; i++) {
            thisvalues[thisvalues.length] = clonetype[i].indexvalue;
        }
    }
    return thisvalues;
}
//遍历数据行数
function jsonFindCount(data, field, val) {
    var count = 0;
    for (var i = 0; i < data.length; i++) {
        try {
            if (data[i][field] == val) {
                count++;
            }
        }
        catch (e) {

        }
    }
    return count;
}
//遍历数据只取符合条件的数据
function jsonFindVal(data, field, val) {
    var _json = [];
    for (var i = data.length - 1; i >= 0; i--) {
        try {
            if (data[i][field] == val) {
                _json.push(data[i]);
            }
        }
        catch (e) {

        }
    }
    return _json;
}


//绑定页面控件
function bindControl(strJson, callback) {
    var json = strJson.toJson(); ;
    var notexiststable = json.notexiststables;
    $("#div_notexists").html("");
    if (notexiststable != "") {//检验数据表不存在
        $("<div id='div_notexists'></div>").html(notexiststable + "数据表在数据库中不存在，请检查!").css("color", "red").appendTo($("body"));
        var table = notexiststable.split(",");
        for (var t = 0; t < table.length; t++) {
            setNotExistsControlStyle(table[t]);
        }
    }
    for (var t = 0; t < json.rows.length; t++) {
        if (!json.rows[t]) {
            return;
        }
        var _tables = json.rows[t].table;
        var _tablenames = json.rows[t].tablename;
        var _tcount = _tables.length;
        //判断是否有多行显示
        var showmorefield = showMoreFieldRow(_tablenames);
        var showmorevalue = showMoreValueRow(_tablenames);
        if (showmorefield.length > 0) {
            for (var _m = 0; _m < showmorefield.length; _m++) {
                var _showmorefield = showmorefield[_m];
                var _showmorevalue = showmorevalue[_m]
                var _tcount = jsonFindCount(_tables, _showmorefield, _showmorevalue);
                var _sjsonold = _tables;
                var _sJson = jsonFindVal(_sjsonold, _showmorefield, _showmorevalue);
                cloneArea(_tablenames, _tcount, _showmorefield, _showmorevalue);
                bindData(_sJson, _tcount, _tablenames, _showmorefield, _showmorevalue);
            }
        }
        else {
            bindData(_tables, _tcount, _tablenames);
        }
        var right = $.query.get("right");
        if (right && right == "1") {

        }
        else {
            var obj_defult = $(document).find("[defultvalue]");
            for (var s = 0; s < obj_defult.length; s++) {
                var truetype = $(obj_defult[s]).attr("truetype");
                var defultvalue = $(obj_defult[s]).attr("defultvalue");

                if (truetype != "selectTree") {
                    if ($(obj_defult[s]).val() == "" || $(obj_defult[s]).val() == null) {
                        $(obj_defult[s]).val(obj_defult[s].defult);
                    }
                }
                else {
                    if ($(obj_defult[s]).attr("relValue") == "") {
                        $(obj_defult[s]).attr("hidValue", defultvalue);
                        $(obj_defult[s]).render();
                        $(obj_defult[s]).setValue(defultvalue);
                    }
                }
            }
        }
    }
    if (callback && callback != null && callback != "") {
        callback();
    }
}

function bindData(_tables, _tcount, _tablenames, _showmorefield, _showmorevalue) {
    var _document = $(document);
    for (var i = 0; i < _tcount; i++) {
        var _tnames = _tablenames;
        var _sort = "";
        if (_showmorefield) {
            _sort = _tables[i].sort;
        }
        for (var key in _tables[i]) {
            var _objs = $(_document).find("input[name='" + key + "']");
            for (var k = 0; k < _objs.length; k++) {
                var _ids = _objs[k].id;
                var _value = _objs[k].attr("savevalue");
                if (_value == _tables[i][key]) {
                    $("#" + _ids).attr("checked", "checked");
                }
            }
            if (_showmorefield && _showmorefield != "") {
                //同一IID对应多行记录
                //                if (_showmorevalue && _showmorevalue != "") {
                //                    $(document).find("input[field='" + key + "'][table='" + _tnames + "'][sort='" + _sort + "'][indexfield='" + _showmorefield + "'][indexvalue='" + _showmorevalue + "'],select[field='" + key + "'][table='" + _tnames + "'][sort='" + _sort + "'][indexfield='" + _showmorefield + "'][indexvalue='" + _showmorevalue + "'],textarea[field='" + key + "'][table='" + _tnames + "'][sort='" + _sort + "'][indexfield='" + _showmorefield + "'][indexvalue='" + _showmorevalue + "']").val(StringReplace(_tables[i][key]));
                //                    $(document).find("span[field='" + key + "'][table='" + _tnames + "'][sort='" + _sort + "'][" + _showmorefield + "='" + _showmorevalue + "']").html(_tables[i][key].replace("0:00:00", ""));

                //                    var _valipt = $("input[readfield='" + key + "'][readtable='" + _tnames + "']").val();
                //                    var _valsel = $("select[readfield='" + key + "'][readtable='" + _tnames + "']").val();
                //                    var _valtxtarea = $("textarea[readfield='" + key + "'][readtable='" + _tnames + "']").val();
                //                    if (!_valipt && !_valipt && !_valtxtarea) {
                //                        $(document).find("input[readfield='" + key + "'][readtable='" + _tnames + "'][sort='" + _sort + "'][indexfield='" + _showmorefield + "'][indexvalue='" + _showmorevalue + "'],select[readfield='" + key + "'][readtable='" + _tnames + "'][sort='" + _sort + "'][indexfield='" + _showmorefield + "'][indexvalue='" + _showmorevalue + "'],textarea[readfield='" + key + "'][readtable='" + _tnames + "'][sort='" + _sort + "'][indexfield='" + _showmorefield + "'][indexvalue='" + _showmorevalue + "']").val(StringReplace(_tables[i][key]));
                //                    }

                //                }
                //                else {
                //                    var ipt = $(document).find("input[field='" + key + "'][table='" + _tnames + "'][sort='" + _sort + "'],select[field='" + key + "'][table='" + _tnames + "'][sort='" + _sort + "'],textarea[field='" + key + "'][table='" + _tnames + "'][sort='" + _sort + "']");
                //                    var ival = StringReplace(_tables[i][key]);
                //                    var iptdecryptype = ipt.attr("decryptype");
                //                    if (iptdecryptype && iptdecryptype != "") {
                //                        var _decryptypetext = Common.query(dictionaryCodeData, '@id="' + iptdecryptype + '"');
                //                        ival = Common.Decryp(ival, _decryptypetext[0].name);
                //                    }
                //                    ipt.val(ival);
                //                    $(document).find("span[field='" + key + "'][table='" + _tnames + "'][sort='" + _sort + "']").html(_tables[i][key].replace("0:00:00", ""));
                //                    var _valipt = $("input[readfield='" + key + "'][readtable='" + _tnames + "']").val();
                //                    var _valsel = $("select[readfield='" + key + "'][readtable='" + _tnames + "']").val();
                //                    var _valtxtarea = $("textarea[readfield='" + key + "'][readtable='" + _tnames + "']").val();
                //                    if (!_valipt && !_valipt && !_valtxtarea) {
                //                        $(document).find("input[readfield='" + key + "'][readtable='" + _tnames + "'][sort='" + _sort + "'],select[readfield='" + key + "'][readtable='" + _tnames + "'][sort='" + _sort + "'],textarea[readfield='" + key + "'][readtable='" + _tnames + "'][sort='" + _sort + "']").val(_tables[i][key].replace("0:00:00", "").replace(",", ";"));
                //                    }
                //                }
            }
            else {
                var ipt = $(_document).find("input[field='" + key + "'][table='" + _tnames + "'][onlysave!='1'],textarea[field='" + key + "'][table='" + _tnames + "'][onlysave!='1']");
                var dsel = $(_document).find("div[field='" + key + "'][table='" + _tnames + "'][onlysave!='1']");
                var img = $(_document).find("img[field='" + key + "'][table='" + _tnames + "']");
                var ival = StringReplace(_tables[i][key]);
                var iptdecryptype = ipt.attr("decryptype");
                var ipttype = ipt.attr("type");

                if (iptdecryptype && iptdecryptype != "") {
                    var _decryptypetext = Common.query(dictionaryCodeData, '@id="' + iptdecryptype + '"');
                    ival = Common.Decryp(ival, _decryptypetext[0].name);
                }

                //赋值
                if (dsel.length > 0) {
                    $(dsel).attr("hidValue", ival);
                    dsel.render();                     
                    dsel.setValue(ival.replace("，",","));
                }
                else if (img.length > 0) {                     
                    if (ival != "") {
                        $(img).attr("src", ival);
                    } 
                }
                else {
                    if (ipttype == "checkbox" || ipttype == "checkbox") {
                        if (ival == "1") {
                            ipt.attr("checked", "checked");
                        }
                        else {
                            ipt.removeAttr("checked");
                        }
                    }
                    else {
                        ipt.val(ival);
                    }
                }
                $(_document).find("span[field='" + key + "'][table='" + _tnames + "']").html(ival);
                var _valipt = $("input[readfield='" + key + "'][readtable='" + _tnames + "']").val();
                var _valsel = $("select[readfield='" + key + "'][readtable='" + _tnames + "']").val();
                var _valtxtarea = $("textarea[readfield='" + key + "'][readtable='" + _tnames + "']").val();
                if (!_valipt && !_valipt && !_valtxtarea) {
                    $(_document).find("input[readfield='" + key + "'][readtable='" + _tnames + "'],select[readfield='" + key + "'][readtable='" + _tnames + "'],textarea[readfield='" + key + "'][readtable='" + _tnames + "']").val(ival);
                }
            }
        }
    }
}

function spanDictionary() {

    var objs = $(document).find("span[dictionary]");
    for (var i = 0; i < objs.length; i++) {
        var v_val = $(objs[i]).html().replaceAll(" ", "");
        var v_text = dictionaryText(v_val);
        $(objs[i]).html(v_text);
    }
}

//翻译数据字典
function dictionaryText(val) {
    var v_val = val;
    var v_text = Common.query(dictionaryCodeData, '@id="' + v_val + '"');
    if (v_text && v_text != null && v_text.length > 0) {
        v_val = v_text[0].name;
    }
    return v_val;
}


//获取页面控件绑定的所有数据表
function GetPageTables() {
    var objs = $(document).find("input[table][notread!='1'],select[table],textarea[table][notread!='1'],span[table],div[table]");
    var tables = [];
    for (var i = 0; i < objs.length; i++) {
        if ($(objs[i]).attr("table") != null) {//去掉重复项
            if (tables.length <= 0 || $.inArray($(objs[i]).attr("table"), tables) == -1) {
                tables.push($(objs[i]).attr("table"));
            }
        }
    }
    var objsread = $(document).find("input[readtable],select[readtable],textarea[readtable]");
    for (var i = 0; i < objsread.length; i++) {
        if (objsread[i].attr("table") != null) {//去掉重复项
            if (tables.length <= 0 || $.inArray(objsread[i].readtable, tables) == -1) {
                tables.push(objsread[i].readtable);
            }
        }
    }
    return tables.join(";");
}

//获取页面下拉框控件绑定的数据字典
function GetPageDictionarys() {
    var objs = $(document).find("select");
    var dictionarys = [];
    for (var i = 0; i < objs.length; i++) {
        if ($(objs[i]).dictionary != null) {//去掉重复项
            if (dictionarys.length <= 0 || $.inArray($(objs[i]).dictionary, dictionarys) == -1) {
                dictionarys.push($(objs[i]).dictionary);
            }
        }
    }

    return dictionarys.join(";");
}


/****************************************************/
//保存数据
function SavePageData(_if, _iv, _where, _op_type) {
    var msg = "";
    var tables = [];
    //获取页面所配置的数据表   需要扩展单选框复选框下拉框

    //暂不考虑权限
    //var objs = $(document).find("[table][field][notread!='1'][writeflag='1',writeflag='2']");
    var objs = $(document).find("[table][field][notread!='1'][onlyshow!='1']");
    for (var i = 0; i < objs.length; i++) {
        var _table = $(objs[i]).attr("table");
        if ($(objs[i]).attr("table") != null) {//去掉重复项
            if (tables.length <= 0 || $.inArray(_table, tables) == -1) {
                tables.push(_table);
            }
        }
    }
    //LiuY  20130830 优化保存
    var S_tables = [], S_if = [], S_if = [], S_iv = [], S_where = [], S_fields = [], S_values = [], S_op_type = [];
    var ts = "[";
    for (var t = 0; t < tables.length; t++) {
        if (tables[t] && tables && [t] != "") {
            var newWhere = _where + InitWhere(tables[t]);
            var _objs = $(document).find("[table='" + tables[t] + "'][field][notread!='1'][onlyshow!='1']");
            var _objSons = $(document).find("[table='" + tables[t] + "'][field][sort][notread!='1'][onlyshow!='1']");

            var _maxsort = -1;
            if (_objSons && _objSons.length > 0) {
                _maxsort = 1;
            }
            if (_maxsort == -1) {
                var fields = [];
                var values = [];
                for (var j = 0; j < _objs.length; j++) {
                    var _encrytype = $(_objs[j]).attr("encrypttype");
                    _encrytype = _encrytype == undefined ? "" : _encrytype;
                    var _field = $(_objs[j]).attr("field");
                    var _savevalue = $(_objs[j]).attr("savevalue");
                    var _value = $(_objs[j]).val().replaceAll(",", ";");
                    var _controltype = $(_objs[j]).attr("controltype");
                    if (!_value) {
                        if (_controltype == "select") {
                            var _relValue = $(_objs[j]).attr("relValue"); //下拉框
                            if (_relValue) {
                                _value = _relValue.replaceAll(",", "，");
                            }
                        }
                        else if (_controltype == "span") {
                            try {
                                _value = $(_objs[j]).html().replaceAll(",", "，");
                            }
                            catch (e)
                            { }
                        }
                        else if (_controltype.indexOf("image") >= 0) {
                            _value = $(_objs[j]).attr("src");
                        }
                    }
                    if (_encrytype != "" && _value != "") {
                        var _encrytypetext = Common.query(dictionaryCodeData, '@id="' + _encrytype + '"');
                        _value = Common.Encry(_value, _encrytypetext[0].name);
                    }

                    if (_savevalue && _savevalue != "") {
                        _value = _savevalue.replaceAll(",", "，");
                    }

                    if (fields.length <= 0 || $.inArray(_field, fields) == -1) {
                        if (_op_type == "u") {
                            if (_controltype == "date" || (_controltype == "date" && _value != "")) {
                                fields.push(_field + " = to_date('" + _value + "';'yyyy-mm-dd')");
                            }
                            else {
                                fields.push(_field + " = '" + _value + "'");
                            }
                        }
                        else {
                            fields.push(_field);
                            if (_controltype == "date" && _value != "") {
                                _value = "to_date('" + _value + "';'yyyy-mm-dd')";
                            }
                            else if (_controltype == "text") {
                                _value = "'" + _value + "'";
                            }
                            else if (_controltype == "checkbox" || _controltype == "radio") {
                                if ($(_objs[j]).attr("checked") == "checked") {
                                    _value = "'1'";
                                }
                                else {
                                    _value = "0";
                                }
                            }
                            values.push(_value.replaceAll(",", "，"));
                        }
                    }
                }
                S_tables.push(tables[t]);
                S_if.push(_if);
                S_iv.push(_iv);
                S_where.push(newWhere);
                S_fields.push(fields.join(";"));
                S_values.push(values.join(";"));
                S_op_type.push(_op_type);
            }
            else {
                var _objType = $("[clone][table='" + tables[t] + "'][sort][indexvalue]");
                if (_objType && _objType.length > 0) {

                }
            }
        }
        msg += SetAllJsonServletExt(S_tables.join("^"), S_if.join("^"), S_iv.join("^"), S_where.join("^"), S_fields.join("^"), S_values.join("^"), S_op_type.join("^"));

    }
    return msg;
}

/****************************************************/
//设置数据表不存在的control样式
function setNotExistsControlStyle(table) {
    if (table != "") {
        $(document).find("[table='" + table + "']").css("border", "1px solid Red");
    }
}




 
function SetAllJsonServletExt(s_t, i_f, i_v, s_w, s_f, s_v, s_p) {
    var parameters = s_t + "," + i_f + "," + i_v + "," + s_w + "," + s_f + "," + s_v + "," + s_p;
    var msg = CommonData("PageHandle", "SetAllJsonServlet", parameters);
    if (msg == "") {
    }
    else {
        alert("保存失败：" + msg);
    }
    return msg;
}
function InitWhere(table) {
    var _where = "";
    var _objs = $(document).find("[table='" + table + "'][field][iswhere='1']");
    for (var i = 0; i < _objs.length; i++) {
        var _value = $(_objs[i]).val();
        var _field = $(_objs[i]).attr("field");
        var _type = $(_objs[i]).attr("fieldtype") == null ? "text" : $(_objs[i]).attr("fieldtype");
        if (_type == "date" && _value != "") {
            _where += " and " + _field + " = to_date('" + _value + "';'yyyy-mm-dd')";
        }
        else {
            _where += " and " + _field + " = '" + _value + "'";
        }
    }
    return _where;
}

//验证页面控件
function validateForm(containerId) {
    if (!containerId) {
        containerId = "body";
    }
    var valid = $(containerId).validationEngine({ returnIsValid: true });
    if (!valid) {
        _validmsg = "内容填写不正确，请检查";
    }
    return valid;
}


//web页面调用保存方法
function f_save() {
    var _indexfield = $.query.get("indexfield");
    var _indexvalue = $.query.get("indexvalue");
    var _validate = validateForm("#content");
    var msg = "";
    if (_validate) {
        try {
            var msg = SavePageData(_indexfield, _indexvalue, "", "u");
            if (msg == "") {
                top.Dialog.alert("保存成功", null, null, null, 0.6);
            }
        }
        catch (e) {
            msg = e;
        }
    }
    else {
        msg = _validmsg;
    }
    return msg;
}


function showOrHide(obj) {
    var title = $(obj).attr("title");
    if (title == "展开") {
        $(obj).parent().parent().find("[hide]").show();
        $(obj).removeClass("button_close").addClass("button_open").attr("title", "折叠");
    }
    else {
        $(obj).parent().parent().find("[hide]").hide();
        $(obj).addClass("button_close").removeClass("button_open").attr("title", "展开");
    }
}

function showMore() {
    var moreobjs = $("[showmore]");
    for (var i = 0; i < moreobjs.length; i++) {
        var moreobj = moreobjs[0];
        var _inputobj = $(moreobj).find("[hide]").hide();
        if (_inputobj && _inputobj.length > 0) {
            var spancontol = $('<span class="button_close" title="展开" onclick="showOrHide(this)"></span>');
            var showdiv = $("<div></div>");
            $(showdiv).append($(spancontol));
            $(showdiv).append($("<span class='button_tool_tip'>点击展开填写更详细内容</span>"));
            $(moreobj).append(showdiv);
        }
    }
}

function cloneArea(tablename, count, showmorefield, showmorevalue) {
    var cloneobj = $("[clone][table='" + tablename + "'][sort]");
    if (showmorevalue && showmorevalue != "") {
        cloneobj = $("[clone][table='" + tablename + "'][sort][indexfield='" + showmorefield + "'][indexvalue='" + showmorevalue + "']");
    }
    var clen = cloneobj.length;
    if (clen > 0) {
        var addaction = $('<div class="wid100_ right"><input type="button" value="删除" onclick="deleteRow(\'' + tablename + '\',\'' + showmorefield + '\',\'' + showmorevalue + '\')" class="formButton" /><input type="button" value="加行" onclick="addRow(\'' + tablename + '\',\'' + showmorefield + '\',\'' + showmorevalue + '\')" class="formButton" /></div>');
        $(cloneobj[0]).after(addaction);
    }
    for (var i = clen - 1; i >= 0; i--) {
        var zoresort = $(cloneobj[i]).attr("sort");
        if (i == 0) {
            $(cloneobj[0]).find("input,select,textarea").attr("sort", zoresort).attr(showmorefield, showmorevalue);
        }
        var max = Number(zoresort) + count - 2;
        for (var j = 0; j < max; j++) {
            var _clone = $(cloneobj[i]).clone().attr("sort", max - j + 1).attr(showmorefield, showmorevalue);
            var _objs = _clone.find("input,select,textarea").val("");
            for (var o = 0; o < _objs.length; o++) {
                $(_objs).attr("sort", max - j + 1);
            }

            $(cloneobj[i]).after(_clone);
        }
    }
}


function deleteRow(tablename, showmorefield, showmorevalue) {
    var maxSort = getMaxSort(tablename, showmorefield, showmorevalue);
    var msg = "";
    if (showmorevalue && showmorevalue != "") {
        msg = DeleteData(tablename, " and sort ='" + maxSort + "' and " + showmorefield + "='" + showmorevalue + "'");
    }
    else {
        msg = DeleteData(tablename, " and sort ='" + maxSort + "'");
    }
    if (msg == "") {
        if (showmorevalue && showmorevalue != "") {
            if (maxSort == 1) {
                $("[clone][table='" + tablename + "'][sort='" + maxSort + "'][indexfield='" + showmorefield + "'][indexvalue='" + showmorevalue + "']").find("input,select,textarea").val("");
            }
            else {
                $("[clone][table='" + tablename + "'][sort='" + maxSort + "'][indexfield='" + showmorefield + "'][indexvalue='" + showmorevalue + "']").remove();
            }
        }
        else {
            if (maxSort == 1) {
                $("[clone][table='" + tablename + "'][sort='" + maxSort + "']").find("input,select,textarea").val("");
            }
            else {
                $("[clone][table='" + tablename + "'][sort='" + maxSort + "']").remove();
            }
        }
    }
}

function addRow(tablename, showmorefield, showmorevalue) {
    var maxSort = getMaxSort(tablename, showmorefield, showmorevalue);
    var cloneobj = $("[clone][table='" + tablename + "'][sort='" + maxSort + "']");
    if (showmorevalue && showmorevalue != "") {
        cloneobj = $("[clone][table='" + tablename + "'][sort='" + maxSort + "'][indexfield='" + showmorefield + "'][indexvalue='" + showmorevalue + "']");
    }
    var _clone = $(cloneobj).clone();
    var _objs = _clone.find("input[type='text'],select,textarea").val("").attr("sort", maxSort + 1);
    if (showmorevalue && showmorevalue != "") {
        $(_objs).attr(showmorefield, showmorevalue);
    }
    $(_clone).attr("sort", maxSort + 1);
    $(cloneobj).after(_clone);
}

function getMaxSort(tablename, showmorefield, showmorevalue) {
    var cloneobj = $("[clone][table='" + tablename + "'][sort]");
    if (showmorevalue && showmorevalue != "") {
        cloneobj = $("[clone][table='" + tablename + "'][sort][indexfield='" + showmorefield + "'][indexvalue='" + showmorevalue + "']");
    }
    var _sort = -1;
    for (var i = 0; i < cloneobj.length; i++) {
        var _cSort = $(cloneobj[i]).attr("sort");
        if (_cSort && _sort < Number(_cSort)) {
            _sort = Number(_cSort);
        }
    }
    return _sort;
}

function StringReplace(str) {
    var newvar = str;
    newvar = newvar.replaceAll(",", "，").replaceAll(";", "；").replaceAll('"', '“').replaceAll('"', '”').replaceAll("'", "‘").replaceAll("'", "’").replaceAll("0:00:00", "");
    return newvar;
}
 