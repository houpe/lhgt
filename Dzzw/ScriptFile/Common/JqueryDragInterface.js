﻿//加载并变换div
function ChangeDiv(id) {
    //存储cookie里边的代码
    var arrCookieLeft = new Array();

    if (getCookie('ck_'+id) != "") {
        arrCookieLeft = getCookie('ck_'+id).split(",");
    }

    //存储初始加载的html
    var arrHtmlLeft = new Array();
    for (var i = 0; i < arrCookieLeft.length; i++) {
        var divId = arrCookieLeft[i];
        if (divId == "") return;
        arrHtmlLeft[i] = "<div id='" + divId + "'  style='margin:10px;width:320px;float:left; text-align:center'>" + $("#" + divId).html() + "</div>";
    }

    //先清空div的模板下内容
    if (arrCookieLeft.length > 0) {
        $("#"+id).html("");
    }
    //再按照顺序加在html内容
    for (var i = 0; i < arrHtmlLeft.length; i++) {
        $("#" + id).append(arrHtmlLeft[i]);
    }
}

//保存模块排序并写入Cookie (^_^我这里只有COOKIE保存.当然你可以保存在数据库里面)
function saveLayout(id) {
    setCookie('ck_' + id, getVales(id));
}

//每一列模块的id值,其实sortable这个函数里有一个serialize可以直接取到对应的序列值:格式如下:
// $('#left').sortable('serialize',{key:'leftmod[]'}) + '&' + $('#right').sortable('serialize',{key:'rightmod[]'})
function getVales(divParentId) {
    var strIds = "";
    $("#" + divParentId + ">div[id^='div_']").each(function(i) {

        if( strIds!="")
        {
            strIds += "," + this.id;
        }
        else
        {
            strIds = this.id;
        }
    });
    return strIds;
}