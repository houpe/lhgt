﻿//js获取url参数
function request(paras) {
    var url = location.href;
    var paraString = url.substring(url.indexOf("?") + 1, url.length).split("&");
    var returnValue;
    for (i = 0; i < paraString.length; i++) {
        if (paraString[i] != "") {
            if (paras.toString().toLowerCase() == paraString[i].substring(0, paraString[i].indexOf("=")).toLowerCase()) {
                returnValue = paraString[i].substring(paraString[i].indexOf("=") + 1, paraString[i].length)
            }
        }
    }
    if (typeof (returnValue) == "undefined") {
        return "";
    } else {
        return returnValue;
    }
}