﻿//使用第三方控件进行打印

//初始化控件参数
function InitPrint(flagType)
{
      DLPrinter.MarginLeft=20;
      DLPrinter.MarginRight=20;
      DLPrinter.MarginTop=20;
      DLPrinter.MarginBottom=20;
      //DLPrinter.CopyCount=1;//打印份数
      //DLPrinter.PageHeader="这是测试的页眉";
      //DLPrinter.PageFooter="这是测试的页脚";
      DLPrinter.IsLandScape=flagType;//1横向，2纵向
 }
 
 function SetPrintUrl(strUrl)
 {
    DLPrinter.ContentURL=strUrl;
 }

//打印预览
function DoPrintView(flagType)
{
    InitPrint(flagType);
    DLPrinter.PrintPreview();
}

//打印预览
function DoPrintAll(flagType)
{
    InitPrint(flagType)
    DLPrinter.Print();//打印
    //DLPrinter.PrintDirect();//.直接打印
}