﻿// JScript 文件


     function createXMLHttpRequest() 
     {
        if (window.ActiveXObject) //如果对ActiveX对象支持
        {
            xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        else if (window.XMLHttpRequest)//JavaScript对象创建
        {
            xmlHttp = new XMLHttpRequest();
        }
    }
   
  
   
    //回调函数，传递参数    
    function startRequest(BackFunctionName,SendValue) 
    {
        createXMLHttpRequest();

        //注册客户端回调函数
        xmlHttp.onreadystatechange = BackFunctionName;

        xmlHttp.open("POST", "../ManageInside/ResponesValue.aspx", true);
        //对url进行编码
        xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

        //把字符串发送服务器
        xmlHttp.send(SendValue); 
    }

     
     //获取部门名称  （服务器回调函数）
    function GetDepartName() 
    {
        if(xmlHttp.readyState == 4) //已经加载
        {
            if(xmlHttp.status == 200) //返回成功
            {
                var departName = document.getElementById("departMentList");
                departName.innerHTML = xmlHttp.responseText;//服务器返回的信息显示在页面上  
                GetAllUser();               
            }
            else//出现错误，显示错误状态
            {
                var msgValue = document.getElementById("departMentList");
                msgValue.innerHTML = "读取数据出错，请重试";
            }
        }
    }    
   
    //获取人员名单 （服务器回调函数）
    function GetUserName()
    {
       if(xmlHttp.readyState == 4) //已经加载
        {
            if(xmlHttp.status == 200) //返回成功
            {
                var userName = document.getElementById("userNameList");
                userName.innerHTML = xmlHttp.responseText;//服务器返回的信息显示在页面上   
                var oSoureSel = document.getElementById("userlist");  
                LookUpUserList();    
                document.getElementById("labCount").innerHTML = "剩余人数" + oSoureSel.options.length + "人";              
            }
            else//出现错误，显示错误状态
            {
                var msgValue = document.getElementById("userNameList");
                msgValue.innerHTML = "读取数据出错，请重试";
            }
        }        
    }
   //部门
   function GetD()
   {        
       startRequest(GetDepartName,"action=");
   }
   //用户
   function GetU()
   {
       var dpart = document.getElementById("Department1").value;      
       startRequest(GetUserName,"dpart="+dpart);  
       
   }
   //所有人员
   function GetAllUser()
   { 
       startRequest(GetUserName,"dpart=34");      
   }
   //初始化用户
   function initUser(obj)
   {  
	   startRequest(GetUserName,"dpart="+obj.value);  
   }
   function listFocus()
   {
       var userlist = document.getElementById("userlist")
       if(userlist.options.length > 0 )
       {
            userlist.options[0].selected = true;
       }
   }
   function sendFocus()
   {
       var sendlist = document.getElementById("sendlist");
       if(sendlist.options.length > 0 )
       {
            sendlist.options[0].selected = true;       
       }
   }
   
   //选择
   function chance()
   {    
       var oSoureSel = document.getElementById("userlist");
       var oTargetSel = document.getElementById("sendlist");   
       moveSelected(oSoureSel,oTargetSel,"1");
       document.getElementById("labCount").innerHTML = "剩余人数" + oSoureSel.options.length + "人";
       document.getElementById("labSendCount").innerHTML = "已经选择" + oTargetSel.options.length + "人";
       listFocus();
   }
   //全选
   function chanceAll()
   {    
       var oSoureSel = document.getElementById("userlist");
       var oTargetSel = document.getElementById("sendlist");     
       moveSelected(oSoureSel,oTargetSel,"3");
       document.getElementById("labCount").innerHTML = "剩余人数" + oSoureSel.options.length + "人";
       document.getElementById("labSendCount").innerHTML = "已经选择" + oTargetSel.options.length + "人";
       
   }
   //删除
   function Delete()
   {
       var oSoureSel = document.getElementById("userlist");
       var oTargetSel = document.getElementById("sendlist");   
       moveSelected(oTargetSel,oSoureSel,"2");
       document.getElementById("labCount").innerHTML = "剩余人数" + oSoureSel.options.length + "人";
       document.getElementById("labSendCount").innerHTML = "已经选择" + oTargetSel.options.length + "人";
       sendFocus();
   }
   //全删
   function DeleteAll()
   {
       var oSoureSel = document.getElementById("userlist");
       var oTargetSel = document.getElementById("sendlist");   
       moveSelected(oTargetSel,oSoureSel,"4");
       document.getElementById("labCount").innerHTML = "剩余人数" + oSoureSel.options.length + "人";
       document.getElementById("labSendCount").innerHTML = "已经选择" + oTargetSel.options.length + "人";
       
   }
   
    //判断当前数据是否在sendlist中,在,则移除
   function LookUpUserList()
   {
       var oSendList = document.getElementById("sendlist")
       var oUserList = document.getElementById("userlist");  
       
      
       for(var i=0;i<oUserList.options.length;i++)
       {
           for(var j=0;j<oSendList.options.length;j++)
           {
               if(oUserList.options[i]!=null)//为什么要加这个限制，好像多此一举，我也不清楚
               {
                   if(oUserList.options[i].value == oSendList.options[j].value)
                   {
                        oUserList.removeChild(oUserList.options[i]);  
                   }
               }
           }
       }
   }
   
   //移动
   function moveSelected(oSoureSel,oTargetSel,flag)
   {
        var arrSelValue = new Array();
        var arrSelText = new Array();
        var arrValueTextRelation = new Array();
        var index = 0;
        for(var i=0;i<oSoureSel.options.length;i++)
        {
            if(flag=="3"||flag=="4")
            {
                arrSelValue[index]=oSoureSel.options[i].value;
                arrSelText[index]=oSoureSel.options[i].text;
                arrValueTextRelation[arrSelValue[index]] = oSoureSel.options[i];
                index ++;
            }
            else if(oSoureSel.options[i].selected)
            {
                arrSelValue[index]=oSoureSel.options[i].value;
                arrSelText[index]=oSoureSel.options[i].text;
                arrValueTextRelation[arrSelValue[index]] = oSoureSel.options[i];
                index ++;
            }
        }
        
        for(var i=0;i<arrSelText.length;i++)
        {
            //追加
            var oOption=document.createElement("option");
            oTargetSel.appendChild(oOption);
            oOption.text=arrSelText[i];
            oOption.value=arrSelValue[i];
            
            //移除oSoureSel中的项
            oSoureSel.removeChild(arrValueTextRelation[arrSelValue[i]]);  
        }
        
        var SendList = document.getElementById("hidSendList");
        var SendNameList = document.getElementById("hidSendNameList");
        SendNameList.value="";
        SendList.value = "";
        
        if(flag=="1"||flag=="3")
        {            
            for(var i=0; i<oTargetSel.options.length;i++)
            {
                SendList.value +=oTargetSel.options[i].value + "|";  
                SendNameList.value += oTargetSel.options[i].text + ",";         
            }
        }  
        else
        {
            for(var i=0; i<oSoureSel.options.length;i++)
            {
                SendList.value +=oSoureSel.options[i].value + "|";  
                SendNameList.value += oSoureSel.options[i].text + ",";         
            }
        }      
   } 
   
  
   
   //检查上传文件是否为空，检查发送列表是否为空
   function checkNull()
   {
        var myfile = document.getElementById("myfile").value;
        var oTargetSel = document.getElementById("sendlist");
        if(myfile=="")
        {
            alert("请选择上传文件");
            return false;
        }   
        else if(oTargetSel.options.length==0)
        {
            alert("请选择用户，没有任何用户在列表里！")
            return false;
        }
        else
        {           
            return true;
        }
   }  
   //文件上传成功
   function upfileSucceed(fName)
   {
       var msg = document.getElementById("labmsg");
       msg.innerHTML = "《" + fName + "》上传成功";
   }   
   
   //获取选中的人员姓名
   function GetAllUserName()
   {
        var SendNameList = document.getElementById("hidSendNameList").value;
        window.parent.document.getElementById("sUserList").innerHTML = SendNameList.substr(0, SendNameList.length - 1);
        
        var SendList =document.getElementById("hidSendList").value;
        window.parent.document.getElementById("hidSendList").value = SendList;

        window.parent.CloseDivDlg();
   }
   
   
  
   
