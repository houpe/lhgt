﻿//防止SQL注入
function AntiSqlValid(strValue) {
    re = /select|update|delete|exec|count|or|and|'|"|=|;|>|<|%/i;
    //alert(re.test(strValue));
    if (re.test(strValue)) {
        //alert("请您不要在参数中输入特殊字符和SQL关键字！"); //注意中文乱码
        return false;
    }
    return true;
}

//密码验证回调
function validate() {
    var context = "";
    var user = $('#username').val();
    var password = $('#password').val();
    var checkcode = $('#txtCheckCode').val();

    //防sql注入
    if (!AntiSqlValid(user) || !AntiSqlValid(password)) {
        alert("请勿输入非法字符");
        return;
    }

    var args = user + ',' + password + ',' + checkcode;
    if (user != "" && password != "") {
        $('#btnLogin').attr("disabled", "true");
        $.ajax({
            type: "POST",
            url: "AjaxProcess/AjaxInfoProcess.ashx",
            data: "params=" + args,
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $("#btnLogin").removeAttr("disabled");
                alert("登录异常：" + textStatus + "  请求状态:" + XMLHttpRequest.status + " 状态：" + XMLHttpRequest.readyState);
            },
            success: function (msg) {
                validateResult(msg);
            }
        });
    }
    else {
        alert("请填写用户名和密码");
    }
}

//验证结果
function validateResult(result) {
    if (result != "") {
        alert("该用户不存在或密码错误！");
        $("#btnLogin").removeAttr("disabled");
    }
    else {
        var user = $('#username').val();
        var password = $('#password').val();

        //flag=2控制菜单是否可用，以及工具条的展示个数
        //window.location = "Index.aspx";
        window.location = "frmMainNew.aspx";
    }
}

//按回车登录
function SubmitKeyClick(button) {
    if (event.keyCode == 13) {
        event.keycode = 9;
        event.returnValue = false;
        $('#' + button).focus();
        $('#' + button).click();
    }
}