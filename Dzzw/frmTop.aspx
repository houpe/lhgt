﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frmTop.aspx.cs" Inherits="frmTop" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>顶头样式</title>
    <link href="css/style.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="JavaScript">
        var strCols_Current = top.fstMain.cols;
        function controltoolbar() {
            if (top.fstMain.cols == strCols_Current) {
                top.fstMain.cols = "0,*";
            }
            else {
                top.fstMain.cols = strCols_Current;
            }
        }
        top.fstMain.cols = strCols_Current;

        function button(text, event, img, button) {
            var output = "";
            output += '<TABLE height="30" border=0 cellspacing=0 cellpadding=0 class="button" event="' + event + '"';
            if (button != '')
                output += ' button="' + button + '"';
            output += '>';
            output += '	 <TR>';
            output += '		<TD><IMG SRC="images/button_left.gif" WIDTH="3" HEIGHT="30" BORDER=0 ALT=""></TD>';
            output += '		<TD class="buttonline">';
            if (img != '')
                output += '	<IMG SRC="' + img + '" BORDER=0 ALT="" hspace="1" align="absmiddle">';
            if (text != '')
                output += '	' + text + '&nbsp;';
            output += '	</TD>';
            output += '		<TD><IMG SRC="images/button_right.gif" WIDTH="3" HEIGHT="30" BORDER=0 ALT=""></TD>';
            output += '	 </TR>';
            output += '</TABLE>';
            document.write(output);
        }
        //添加用户退出功能 addby zhongjian 20091207
        function returnDefault() {
            window.parent.location = "Default.aspx";
            //self.parent.close();
        }
	
    </script>

</head>
<body leftmargin="0" topmargin="0" oncontextmenu="return false" onselectstart="return false"
    ondragstart="return false">
    <form id="form1" runat="server">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr height="30">
            <td class="title1">
                &nbsp;
            </td>
        </tr>
        <tr height="2">
            <td background="images/seperate_line_bg.gif">
                <img src="images/seperate_line_bg.gif" width="1" height="2" border="0" alt="">
            </td>
        </tr>
        <tr height="30">
            <td class="title2">
                <table height="100%" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="15">
                            <img src="images/seperate.gif" width="7" height="23" border="0" alt="" hspace="4">
                        </td>
                        <td>

                            <script>
                                button("功能导航菜单", "controltoolbar()", "images/ico/controltoolbar.gif", "down");
                                //button("Search", "window.open('about:blank')", "images/ico/search.gif");
                            </script>

                        </td>
                        <td align="right">
                            <span class="top_text">您好，<%=UserName %></span>&nbsp;&nbsp;&nbsp;&nbsp;<span><%=DateTime.Now.ToLongDateString() %></span>
                            <img alt="退出登录" onclick="javascript:returnDefault();" src="images/logout.jpg" style="text-align: right;
                                border: 0px;" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr height="2">
            <td background="images/seperate_line_bg.gif">
                <img src="images/seperate_line_bg.gif" width="1" height="2" border="0" alt="">
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
