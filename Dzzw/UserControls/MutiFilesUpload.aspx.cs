﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business.FlowOperation;
using Business.Common;
using WF_Business;
using System.Data;
using System.Text;

public partial class UserControls_MutiFilesUpload : PageBase
{
    //流程ID 
    public string iid
    {
        get
        {
            return Request["iid"];
        }
    }
    //岗位ID
    public string wiid
    {
        get
        {
            return Request["wiid"];
        }
    }

    //岗位ID
    public string ctlid
    {
        get
        {
            return Request["ctlid"];
        }
    }

    //岗位名称
    public string step
    {
        get
        {
            string strReturn = string.Empty;

            if (!string.IsNullOrEmpty(wiid))
            {
                strReturn = WorkFlowHandle.GetStepByWiid(long.Parse(wiid));
            }
            return strReturn;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //判断附件权限
            AttachmentFileUpload attachFile = new AttachmentFileUpload();
            int Flag = attachFile.GetAttachmentRight(iid, step);

            if (Flag == 0)
            {
                btnUpload.Visible = false;
            }
        }
    }

    /// <summary>
    /// 获取目录
    /// </summary>
    /// <returns></returns>
    public String GetTreeHTML()
    {
        WorkFlowResource wfr = new WorkFlowResource();
        DataTable dt = new DataTable();
        if ("-1".Equals(wiid))
        { dt = wfr.ListInstanceResource(iid); }
        else
        { dt = wfr.ListInstanceStepResource(wiid.ToString(), ctlid, iid.ToString()); }
        dt.Columns.Add(new DataColumn("ParentId"));
        dt.Columns.Add(new DataColumn("Site"));
        dt.Columns.Add(new DataColumn("Name"));
        DataView dv = dt.DefaultView;
        dv.Sort = "Path";
        for (int i = 0; i < dv.Count; i++)
        {
            string path = dv[i]["Path"].ToString();
            string[] tmps = path.Split('/');
            dv[i]["Site"] = i + 1;
            if (tmps.Length > 1)//不是顶节点
            {
                dv[i]["Name"] = tmps[tmps.Length - 1];
                string pre = path.Substring(0, path.LastIndexOf('/'));
                DataRow[] drs = dt.Select("Path='" + pre + "'");
                if (drs.Length > 0)
                    dv[i]["ParentId"] = drs[0]["Site"].ToString();
            }
            else
            {
                dv[i]["Name"] = dv[i]["Path"].ToString();
                dv[i]["ParentId"] = 0;
            }
        }
        StringBuilder sb = new StringBuilder();
        sb.Append("<select name='myfilePath' id='myfilePath' class='select'>\n");
        sb.Append("<option value='/'>/</option>\n");
        if ("-1".Equals(wiid))
        {
            printSubNodes(dt, sb, ""); 
        }
        else
        {
            string wid = WorkFlowResource.GetWidByIID(iid);
            printSubNodes(dt, sb, wid);
        }
        sb.Append("</select>\n");
        return sb.ToString();
    }
    /// <summary>
    /// 查找子节点
    /// </summary>
    /// <param name="dt">树形结构数据集</param>
    /// <param name="sb">返回字符串</param>
    /// <param name="wid">岗位ID</param>
    private void printSubNodes(DataTable dt, StringBuilder sb, string wid)
    {
        foreach (DataRow dr in dt.Rows)
        {
            string type = dr["type"].ToString();
            String strOutput = string.Empty;
            DataRow[] drs = dt.Select("ParentId='" + dr["Site"].ToString() + "'");
            if (drs.Length > 0)//有子节点，说明不是叶结点
            {
                strOutput = "<option value=\"" + dr["Path"].ToString() + "\">" + dr["Name"].ToString() + "</option>";
            }
            else
            {
                if (type == "0")//目录
                {
                    strOutput = "<option value=\"" + dr["Path"].ToString() + "\">" + dr["Name"].ToString() + "</option>";
                }
            }
            sb.Append(strOutput);
        }
    }
}