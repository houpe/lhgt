﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucLeftNavigation.ascx.cs"
    Inherits="UserControls_ucLeftNavigation" %>

<script type="text/javascript">
    //子菜单点击
    function addTab(strUrl, strTitle) {
        //显示进度条
        //ShowLoading();

        var varTemp = $("#divMain").tabs("exists", strTitle);
        if (varTemp == false) {//如果选项卡不存在
            $('#divMain').tabs('add', {
                title: strTitle,
                content: '<iframe src="' + strUrl + '" width="100%" height="100%" frameboder="0" id="' + strTitle + '"></iframe>',
                iconCls: 'icon-save',
                closable: true
            });

            //为子框架添加加载事件
            $("#" + strTitle).load(function() {
                HideLoading();
            });
        }
        else {//如果已经存在则选中对应的选项卡并刷新
            if (strUrl.indexOf("?") > 0) {
                strUrl += "&rdm=" + Math.random();
            }
            else {
                strUrl += "?rdm=" + Math.random();
            }
            $("#" + strTitle + "").attr("src", strUrl);
            $('#divMain').tabs('select', strTitle);

            //再关闭一次
            //HideLoading(); 
        }

        //为子框架添加加载事件
        //        var varIframe = $("#" + strTitle);
        //        if (varIframe.attachEvent) {
        //            alert(1);
        //            varIframe.attachEvent("onload", function() {
        //                alert("Local iframe is now loaded.");
        //            });
        //        } else {
        //            alert(2);
        //            varIframe.onload = function() {
        //                alert("Local");
        //            };
        //        }

        
    }
</script>

<div id="divMenu" class="easyui-accordion" fit="true" border="false" style="height: 500px">
    <%=Session["Menu"]%>
</div>
