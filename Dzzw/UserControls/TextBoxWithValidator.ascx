﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TextBoxWithValidator.ascx.cs"
    Inherits="UserControls_TextBoxWithValidator" %>
<asp:TextBox ID="txtTextBox" SkinID="InputGreen" runat="server" OnTextChanged="txtTextBox_TextChanged"></asp:TextBox>
<asp:RequiredFieldValidator ID="rfv" runat="server" ControlToValidate="txtTextBox"
    Display="Dynamic" Enabled="False" ErrorMessage="*"></asp:RequiredFieldValidator>
<asp:CustomValidator ID="cv" runat="server" ErrorMessage="*" ControlToValidate="txtTextBox"
    Display="Dynamic" Enabled="False"></asp:CustomValidator>
<asp:RegularExpressionValidator ID="rev" runat="server" ControlToValidate="txtTextBox"
    Display="Dynamic" Enabled="False" ErrorMessage="*"></asp:RegularExpressionValidator>