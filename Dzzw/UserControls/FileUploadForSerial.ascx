<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FileUploadForSerial.ascx.cs" Inherits="UserControls_FileUploadForSerial" %>
<style type="text/css">
thead,tbody
{
    font-size:12px;
}
.fu_list {
	width:600px;
	background:#ebebeb;
	font-size:12px;
}
.fu_list td {
	padding:5px;
	line-height:20px;
	background-color:#fff;
}
.fu_list table {
	width:100%;
	border:1px solid #ebebeb;
}
.fu_list thead td {
	background-color:#f4f4f4;
}
.fu_list b {
	font-size:14px;
}
a,a:hover
{
    color:#000000;
}
/*file容器样式*/
a.files {
	width:30px;
	height:30px;
	overflow:hidden;
	display:block;
	
	background:url(../images/fu_btn.gif) left top no-repeat;
	text-decoration:none;
}
a.path
{
    width:100px;
	height:30px;
	overflow:hidden;
	display:block;
	border:1px solid #BEBEBE;
	background:url(../images/fu_btn.gif) left top no-repeat;
	text-decoration:none;
}
a.files:hover,a.path:hover {
	background-color:#FFFFEE;
	background-position:0 -30px;
}
/*file设为透明，并覆盖整个触发面*/
a.files input,a.path input {
	margin-left:-350px;
	font-size:30px;
	cursor:pointer;
	filter:alpha(opacity=0);
	opacity:0;
}
/*取消点击时的虚线框*/
a.files, a.files input,a.path,a.path input {
	outline:none;/*ff*/
	hide-focus:expression(this.hideFocus=true);/*ie*/
}
.select
{
    height:30px; 
    font-size:14px;  
    border:1px solid #BEBEBE; 
}
.button
{
    width:80px;
    height:25px; 
    border:1px solid #BEBEBE; 
    background:#FFFFFF; 
}
</style>
<div id="attachment" style="display: block; width: 600px; height: 370px; background-color: #fff;
    text-align: center; border: 1px solid #97B4D4;">
    <div id="attachmentmove" style="border-top: 20px solid #97B4D4;">
    </div>
    <div>
        <form id="uploadForm" name="uploadForm" action="File.aspx?IID=<%=iid %>&STEP=<%=step %>&ctlid=<%=ctlid%>">
            <asp:Panel ID="Flagdiv" runat="server">
                <table border="0" cellspacing="1" class="fu_list">
                    <thead>
                        <tr>
                            <td colspan="2" style="height: 30px; text-align: center">
                                <span style="float: right"><a href="#" onclick="closethis()">关闭</a></span><b>上传附件</b></td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <table border="0" cellspacing="0" width="600px;">
                                    <thead>
                                        <tr>
                                            <td style="width: 200px;">
                                                文件名</td>
                                            <td style="width: 200px;">
                                                路径</td>
                                            <td style="width: 80px;">
                                                进度</td>
                                            <td style="width: 80px;">
                                                操作</td>
                                        </tr>
                                    </thead>
                                    <tbody id="idFileList">
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table style="width: 300px; border: none;">
                                    <tr>
                                        <td style="width: 200px;">
                                            选择目录：<%=GetTreeHTML()%></td>
                                        <td style="width: 100px; margin-left:0px; padding-left:0px;">
                                            <a href="javascript:void(0);" title="请选择文件" class="files" id="idFile"></a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" id="idMsg">
                                <input type="button" value="开始上传" id="idBtnupload" disabled="disabled" class="button" />
                                &nbsp;&nbsp;&nbsp;
                                <input type="button" value="全部取消" id="idBtndel" disabled="disabled" class="button" />
                            </td>
                        </tr>
                    </tbody>
                </table>
                <input type="hidden" value="" id="varinfo" name="varinfo" />
            </asp:Panel>
            <asp:Panel runat="server" ID="Flagdiv1" Visible="false">
                <div style="margin-top: 100px; z-index: 10000; text-align: center">
                    <table style="width: 386px; height: 55px; text-align: center" align="center">
                        <tr>
                            <td bgcolor="#FFFFCC" class="floatTip">
                                <br />
                                没有附件添加权限！请联系管理员！<a href="#" onclick="closethis()">关闭</a><br />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </form>
        <!----提交后用户提示代码----->
        <div id="idProcess" style="margin-top: -100px; z-index: 10000; display: none; text-align: center">
            <table style="width: 386px; height: 55px; text-align: center" align="center">
                <tr>
                    <td bgcolor="#FFFFCC" class="floatTip">
                        <img src="../images/loading.gif" alt="进度条" /><br />
                        <br />
                        上传中，请耐心等候！</td>
                </tr>
            </table>
        </div>
        <!----提交后用户提示代码----->

        <script type="text/javascript" defer="defer">
    function GetFileName(filename)
   {
          return filename.substring(filename.lastIndexOf('\\')+ 1);
         //GetFileSize(filename)+ 
   }
   function GetFileSize(filename) 
   { 
        var fso = new ActiveXObject("Scripting.FileSystemObject"); 
        if ("object" != typeof(fso)) return;
        var f = fso.GetFile(filename);
        if(f.size>1048576)
        {
            return  "("+Math.round(f.size/1048576*100)/100+"M)"
        }else
        {
            return "("+Math.round(f.size/1024*10)/10+"KB)";
        }
   }

   function   SetStatus(pgbID,   pgbValue)     
   {     
        if( lblObj   =   document.getElementById("uploadfile_file_"+pgbID))
        {
            lblObj.innerHTML   =   pgbValue;
         }
  }  

    String.prototype.trim= function()  
    {  
        // 用正则表达式将前后空格  
        // 用空字符串替代。  
        return this.replace(/(^\s*)|(\s*$)/g, "");  
    }
    var isEditing=false; 
    function editinplace (editid) 
    {	
	    //var isEditing = false;
		var obj=document.getElementById("edit_"+editid);
    		
	    if(!isEditing)
	    {//alert(isEditing);
		    isEditing = true;
		    var oldDivContent = obj.innerHTML.replace(/\r\n?/, "\n").trim();
	    //alert(isEditing);
			
		    obj.innerHTML="";
			
		    var txtContent = document.createElement("input");
		    txtContent.id="edit_input_"+editid;
		    txtContent.value=oldDivContent;
		    obj.appendChild(txtContent);
		    txtContent.focus();

		    var btnSave = document.createElement("input"); 
		    btnSave.type="button"; 
		    btnSave.value="确定"; 
		    btnSave.onclick=function()
		    { 
			    isEditing=false;
			    obj.innerHTML=document.getElementById("edit_input_"+editid).value;
			   document.getElementById(editid).SetFileName=obj.innerHTML;
		    }
		    obj.appendChild(btnSave); 

		    var btnCancel = document.createElement("input"); 
		    btnCancel.type="button"; 
		    btnCancel.value="取消"; 
		    btnCancel.onclick=function()
		    { 
			    isEditing=false;
			    obj.innerHTML = oldDivContent;
		    }
		    obj.appendChild(btnCancel); 
		}
    }
        </script>

        <script type="text/javascript" defer="defer">
var filecount=0;
var isIE = (document.all) ? true : false;
var $ = function (id) {
    return "string" == typeof id ? document.getElementById(id) : id;
};

var Class = {
  create: function() {
    return function() {
      this.initialize.apply(this, arguments);
    }
  }
}

var Extend = function(destination, source) {
	for (var property in source) {
		destination[property] = source[property];
	}
}

var Bind = function(object, fun) {
	return function() {
		return fun.apply(object, arguments);
	}
}

var Each = function(list, fun){
	for (var i = 0, len = list.length; i < len; i++) { fun(list[i], i); }
};

//文件上传类
var FileUpload = Class.create();
FileUpload.prototype = {
    //表单对象，文件控件存放空间
    initialize: function(form, folder, options) {

        this.Form = $(form); //表单
        this.Folder = $(folder); //文件控件存放空间
        this.Files = []; //文件集合

        this.SetOptions(options);

        this.FileName = this.options.FileName;
        this._FrameName = this.options.FrameName;
        this.Limit = this.options.Limit;
        this.Distinct = !!this.options.Distinct;
        this.ExtIn = this.options.ExtIn;
        this.ExtOut = this.options.ExtOut;

        this.onIniFile = this.options.onIniFile;
        this.onEmpty = this.options.onEmpty;
        this.onNotExtIn = this.options.onNotExtIn;
        this.onExtOut = this.options.onExtOut;
        this.onLimite = this.options.onLimite;
        this.onSame = this.options.onSame;
        this.onFail = this.options.onFail;
        this.onIni = this.options.onIni;

        if (!this._FrameName) {
            //为每个实例创建不同的iframe
            this._FrameName = "uploadFrame_" + Math.floor(Math.random() * 1000);
            //ie不能修改iframe的name
            var oFrame = isIE ? document.createElement("<iframe name=\"" + this._FrameName + "\">") : document.createElement("iframe");
            //为ff设置name
            oFrame.name = this._FrameName;
            oFrame.style.display = "none";
            //在ie文档未加载完用appendChild会报错
            document.body.insertBefore(oFrame, document.body.childNodes[0]);
        }

        //设置form属性，关键是target要指向iframe
        this.Form.target = this._FrameName;
        this.Form.method = "post";
        //注意ie的form没有enctype属性，要用encoding
        this.Form.encoding = "multipart/form-data";

        //整理一次
        this.Ini();
    },
    //设置默认属性
    SetOptions: function(options) {
        this.options = {//默认值
            FileName: "", //文件上传控件的name，配合后台使用
            FrameName: "", //iframe的name，要自定义iframe的话这里设置name
            Path: "",
            onIniFile: function() { }, //整理文件时执行(其中参数是file对象)
            onEmpty: function() { }, //文件空值时执行
            Limit: 0, //文件数限制，0为不限制
            onLimite: function() { }, //超过文件数限制时执行
            Distinct: true, //是否不允许相同文件
            onSame: function() { }, //有相同文件时执行
            ExtIn: [], //允许后缀名
            onNotExtIn: function() { }, //不是允许后缀名时执行
            ExtOut: [], //禁止后缀名，当设置了ExtIn则ExtOut无效
            onExtOut: function() { }, //是禁止后缀名时执行
            onFail: function() { }, //文件不通过检测时执行(其中参数是file对象)
            onIni: function() { } //重置时执行
        };
        Extend(this.options, options || {});
    },
    //整理空间
    Ini: function() {
        //整理文件集合
        this.Files = [];
        //整理文件空间，把有值的file放入文件集合
        if (typeof (this.Folder) != "undefined" && this.Folder != null) {
            Each(this.Folder.getElementsByTagName("input"), Bind(this, function(o) {
                if (typeof (o) != "undefined") {
                    if (o.type == "file") { o.value && this.Files.push(o); this.onIniFile(o); }
                }
            }));
            //插入一个新的file
            var file = document.createElement("input");
            file.name = this.FileName; file.type = "file"; file.onchange = Bind(this, function() { this.Check(file); this.Ini(); });
            if ($("path") != null && $("path") != undefined) {
                file.Path = $("path").value;
            }
            else {
                file.Path = "";
            }

            filecount = filecount + 1;
            file.onclick = function() {
                if ($("path") != null && $("path") != undefined) {
                    this.Path = $("path").value;
                }
                file.id = "file_" + filecount; file.SetFileName = "";
            };

            this.Folder.appendChild(file);
        }
        //执行附加程序
        this.onIni();
    },
    //检测file对象
    Check: function(file) {

        var FilesCopy = [];
        for (var i = 0; i < this.Files.length - 1; i++) {
            FilesCopy[i] = this.Files[i];
        }
        //检测变量
        var bCheck = true;
        //空值、文件数限制、后缀名、相同文件检测
        if (!file.value) {
            bCheck = false; this.onEmpty();
        } else if (this.Limit && this.Files.length >= this.Limit) {
            bCheck = false; this.onLimite();
        } else if (!!this.ExtIn.length && !RegExp("\.(" + this.ExtIn.join("|") + ")$", "i").test(file.value)) {
            //检测是否允许后缀名
            bCheck = false; this.onNotExtIn();
        } else if (!!this.ExtOut.length && RegExp("\.(" + this.ExtOut.join("|") + ")$", "i").test(file.value)) {
            //检测是否禁止后缀名
            bCheck = false; this.onExtOut();
        } else if (!!this.Distinct) {
            Each(FilesCopy, function(o) { if (o.value == file.value) { bCheck = false; } });
            if (!bCheck) { this.onSame(); }
        }
        //没有通过检测
        !bCheck && this.onFail(file);
    },
    //删除指定file
    Delete: function(file) {
        //移除指定file
        this.Folder.removeChild(file); this.Ini();
    },
    //删除全部file
    Clear: function() {
        //清空文件空间
        Each(this.Files, Bind(this, function(o) { this.Folder.removeChild(o); }));
        filecount = 0;
        this.Folder.style.display = "block";
        $("idProcess").style.display = "none";
        $("idMsg").style.display = "block";
        $("varinfo").value = "";
        this.Ini();
    }
}

var fu = new FileUpload("uploadForm", "idFile", { Limit: 20, //ExtIn: ["jpg", "gif"],
    onIniFile: function(file) { file.value ? file.style.display = "none" : this.Folder.removeChild(file); },
    onEmpty: function() { alert("请选择一个文件"); },
    onLimite: function() { alert("超过上传限制"); },
    onSame: function() { alert("已经有相同文件"); },
    onNotExtIn: function() { alert("只允许上传" + this.ExtIn.join("，") + "文件"); },
    onFail: function(file) { this.Folder.removeChild(file); },
    onIni: function() {
        //显示文件列表
        var arrRows = [];
        if (this.Files.length) {
            var oThis = this;
            Each(this.Files, function(o) {
                var a = document.createElement("a"); a.innerHTML = "取消"; a.href = "javascript:void(0);";
                a.onclick = function() { oThis.Delete(o); return false; };
                if (o.SetFileName == "")
                    o.SetFileName = GetFileName(o.value); 
                    //o.Path = $("myfilePath1").value; //针对path设置值，否则获取不到
                arrRows.push(["<div id=\"edit_" + o.id + "\" onclick=\"editinplace('" + o.id + "')\" >" + o.SetFileName + "</div>",  $("myfilePath1").value, "<div id='uploadfile_" + o.id + "'></div>", a]);
            });
        } else { arrRows.push(["<font color='gray'>没有添加文件</font>", "&nbsp;"]); }
        AddList(arrRows);
        //设置按钮
        $("idBtnupload").disabled = $("idBtndel").disabled = this.Files.length <= 0;
    }
});

$("idBtnupload").onclick = function() {
    //显示文件列表
    var arrRows = [];
    var pro_index = 1;
    Each(fu.Files,
	    function(o) {
    arrRows.push([o.SetFileName, $("myfilePath1").value, "<div id='uploadfile_file_" + pro_index + "'></div>"]);
	        pro_index = pro_index + 1;
	        $("varinfo").value = $("varinfo").value + ($("myfilePath1").value + "^" + o.SetFileName) + "|";
	    }
	);
    AddList(arrRows);

    fu.Folder.style.display = "none";
    $("idProcess").style.display = "";
    $("idMsg").style.display = "none";

    fu.Form.submit();
}

//用来添加文件列表的函数
function AddList(rows){
	//根据数组来添加列表
	var FileList = $("idFileList"), oFragment = document.createDocumentFragment();
	//用文档碎片保存列表
	if( FileList!=null)
	{
	    Each(rows, function(cells){
		    var row = document.createElement("tr");
		    Each(cells, function(o){
			    var cell = document.createElement("td");
			    if(typeof o == "string"){ cell.innerHTML = o; }else{ cell.appendChild(o); }
			    row.appendChild(cell);
		    });
		    oFragment.appendChild(row);
	    })
	    //ie的table不支持innerHTML所以这样清空table
	    while(FileList.hasChildNodes()){ FileList.removeChild(FileList.firstChild); }
	    FileList.appendChild(oFragment);
	}
}
$("idBtndel").onclick = function(){fu.Clear();}

//在后台通过window.parent来访问主页面的函数
function Finish(){ alert("上传成功！");window.opener.frames("ResourceView").location.reload();fu.Clear();closethis();}//location.href = location.href; 
function FinishCopy(){ alert("部分文件上传未成功！请重新上传该文件！");window.opener.frames("ResourceView").location.reload();fu.Clear();}//location.href = location.href; 
function closethis(){window.opener=null;window.close();}
        </script>

    </div>
</div>
