﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Business.Common;

public partial class UserControl_MsgPlate : System.Web.UI.UserControl
{
    /// <summary>
    /// 消息数据类型
    /// </summary>
    /// <value>The type of the MSG data.</value>
    public string MsgDataType
    {
        get
        {
            if (ViewState["MsgDataType"] != null)
            {
                return (string)ViewState["MsgDataType"];
            }
            else
            {
                return string.Empty;
            }
        }
        set
        {
            ViewState["MsgDataType"] = value;
        }
    }

    public string MsgSmallType
    {
        get
        {
            if (ViewState["MsgSmallType"] != null)
            {
                return (string)ViewState["MsgSmallType"];
            }
            else
            {
                return string.Empty;
            }
        }
        set
        {
            ViewState["MsgSmallType"] = value;
        }
    }

    public string UserId
    {
        get
        {
            if (ViewState["UserId"] != null)
            {
                return (string)ViewState["UserId"];
            }
            else
            {
                return string.Empty;
            }
        }
        set
        {
            ViewState["UserId"] = value;
        }
    }

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindDataGrid();
        }
    }


    /// <summary>
    /// Binds the data grid.
    /// </summary>
    public void BindDataGrid()
    {
        if (!string.IsNullOrEmpty(MsgDataType))
        {
            InfoManage imTemp = new InfoManage();
            DataTable dtSource = imTemp.GetInfoByUser(MsgDataType,UserId);
            if (dtSource != null)
            {
                this.DataGrid1.DataSource = dtSource;
                this.DataGrid1.DataBind();
            }
        }
    }
   
    //more
    /// <summary>
    /// Handles the PageIndexChanged event of the DataGrid1 control.
    /// </summary>
    /// <param name="source">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.DataGridPageChangedEventArgs"/> instance containing the event data.</param>
    private void DataGrid1_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
    {
        if (this.DataGrid1.Items.Count == 1 && this.DataGrid1.CurrentPageIndex > 0)
        {
            this.DataGrid1.CurrentPageIndex--;
        }
        this.DataGrid1.CurrentPageIndex = e.NewPageIndex;
        BindDataGrid();
    }
}
