﻿<%@ WebHandler Language="C#" Class="AutoInputProcess" %>

using System;
using System.Web;

public class AutoInputProcess : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/plain";

        string sName = context.Request["name"];
        string sInput = context.Request["inputData"];
        if (sInput.Length == 0)
            return;
        
        string sResult = "";
        string strWhere = string.Format(" keyvalue like '%{0}%'", sInput);

        System.Data.DataTable dtTable = Business.Admin.DictOperation.GetSysParams(sName, strWhere);
        
        for (int i = 0; i < dtTable.Rows.Count; i++)
        {
            sResult += dtTable.Rows[i]["keyvalue"] + ",";
        }
        if (sResult.Length > 0) //如果有匹配项 
            sResult = sResult.Substring(0, sResult.Length - 1); //去掉最后的“,”号 

        context.Response.Write(sResult);
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}