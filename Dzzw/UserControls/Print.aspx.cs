﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.IO;
public partial class Controls_User_Print : System.Web.UI.Page
{
    private string _PrintContent = string.Empty;
    
    /// <summary>
    /// 打印内容
    /// </summary>
    public string PrintContent
    {
        get { return _PrintContent.Replace("../../../..", "../../.."); }
    }

    /// <summary>
    /// 获取需要显示的参数信息
    /// </summary>
    public string AppearCtrlParamInfo
    {
        get
        {
            if( !string.IsNullOrEmpty(Request["appearCtrl"]))
            {
                return Request["appearCtrl"].Replace("_", "$");
            }
            else
            {
                return string.Empty;
            }
        }
    }

    /// <summary>
    /// 获取需要隐藏的参数信息
    /// </summary>
    public string HiddenCtrlParamInfo
    {
        get
        {
            if (!string.IsNullOrEmpty(Request["hiddenCtrl"]))
            {
                string strNew = Request["hiddenCtrl"].Replace("_", "$");
                return strNew.Replace("$Title", "_Title");
            }
            else
            {
                return string.Empty;
            }
        }

    }

    #region 构建打印内容
    /// <summary>
    /// 构建打印内容
    /// </summary>
    private void ConstructPrintContent()
    {
        string csName = "TextOnFocusScript";
        Type csType = this.GetType();
        ClientScriptManager cs = Page.ClientScript;

        // 检查是否脚本是否已经被注册
        if (!cs.IsStartupScriptRegistered(csType, csName))
        {
            string csText = @"
            var hkey_root,hkey_path,hkey_key
            hkey_root=""HKEY_CURRENT_USER""
            hkey_path=""\\Software\\Microsoft\\Internet Explorer\\PageSetup\\""
            //设置网页打印的页眉页脚为空
            function pagesetup_null()
            {
                try{
                var RegWsh = new ActiveXObject(""WScript.Shell"")
                hkey_key=""header"" 
                RegWsh.RegWrite(hkey_root+hkey_path+hkey_key,"""")
                hkey_key=""footer""
                RegWsh.RegWrite(hkey_root+hkey_path+hkey_key,"""")
                }catch(e){}
            }
            function printsetup()
            {
                bdhtml=window.document.body.innerHTML;
                WebBrowser.execwb(8,1); 
                window.document.body.innerHTML=bdhtml;
            }
            function printpreview() 
            {  
                pagesetup_null();
                bdhtml=window.document.body.innerHTML;
                sprnstr=""<!--STARTPRINT-->"";
                eprnstr=""<!--ENDPRINT-->"";
                prnhtml=bdhtml.substring(bdhtml.indexOf(sprnstr));
                prnhtml=prnhtml.substring(0,prnhtml.indexOf(eprnstr));
                window.document.body.innerHTML=prnhtml;
                WebBrowser.execwb(7,1); 
                window.document.body.innerHTML=bdhtml;                
            }
            function printit()
            {
                //if(confirm('确定打印吗?'))
                //{
                    bdhtml=window.document.body.innerHTML;
                    sprnstr=""<!--STARTPRINT-->"";
                    eprnstr=""<!--ENDPRINT-->"";
                    prnhtml=bdhtml.substring(bdhtml.indexOf(sprnstr));
                    prnhtml=prnhtml.substring(0,prnhtml.indexOf(eprnstr));
                    window.document.body.innerHTML=prnhtml;                    
                    //WebBrowser.execwb(6,6);
                    window.print();
                    window.document.body.innerHTML=bdhtml;
                //} 
            }

            //不用控件WebBrowser的打印方式
            function printitWithNoControl()
            {
                //if(confirm('确定打印吗?'))
                //{
                    bdhtml=window.document.body.innerHTML;
                    sprnstr=""<!--STARTPRINT-->"";
                    eprnstr=""<!--ENDPRINT-->"";
                    prnhtml=bdhtml.substring(bdhtml.indexOf(sprnstr));
                    prnhtml=prnhtml.substring(0,prnhtml.indexOf(eprnstr));
                    window.document.body.innerHTML=prnhtml;
                    window.print();
                    window.document.body.innerHTML=bdhtml;
                //} 
            }
            ";
            cs.RegisterStartupScript(csType, csName, csText, true);
        }

        //设置打印动作
        if (Request.Params["bWithActivex"] == "1")
        {
            this.button_print.Attributes.Add("onclick", "printitWithNoControl();return;");
            button_setup.Visible = false;
            button_preview.Visible = false;
        }
        else
        {
            this.button_print.Attributes.Add("onclick", "printit();return;");
            this.button_setup.Attributes.Add("onclick", "printsetup();return;");
            this.button_preview.Attributes.Add("onclick", "printpreview();return;");
        }
        

        if (Session["PrintContent"] != null)
        {
            _PrintContent = Session["PrintContent"].ToString();
        }

        int nEndLabel = _PrintContent.IndexOf("<!--CLEARSTART-->");
        if (nEndLabel > -1)
        {
            //去除datagrid中不需要打印的项
            string strEndClear = "<!--CLEAREND-->";
            _PrintContent = _PrintContent.Substring(0, nEndLabel) +
                _PrintContent.Substring(_PrintContent.IndexOf(strEndClear) + strEndClear.Length);

            //移除url链接的后标志位(消除链接的样式)
            _PrintContent = _PrintContent.Replace("</a>", "");
            StringBuilder sb = new StringBuilder();
            int start = 0;

            //清除url链接的前标志位
            int end = _PrintContent.IndexOf("<a");
            if (end > 0)
            {
                while (end > 0)
                {
                    sb.Append(_PrintContent.Substring(start, end - start));
                    start = _PrintContent.IndexOf(">", end) + 1;
                    end = _PrintContent.IndexOf("<a", start);
                }
                sb.Append(_PrintContent.Substring(start));
            }
            if (string.IsNullOrEmpty(sb.ToString()))
            {
                sb.Append(_PrintContent);
            }

            //移除行标记中的样式及属性（消除行特效）
            start = sb.ToString().IndexOf("<tr");
            while (start > 0)
            {
                end = sb.ToString().IndexOf(">", start);
                if (end > start + 3)
                {
                    sb.Remove(start + 3, end - start - 3);
                }
                start = sb.ToString().IndexOf("<tr", end);
            }
            _PrintContent = sb.ToString();

            //打开隐藏控件--add by lj
            //if (!string.IsNullOrEmpty(Request["appearCtrl"]))
            //{
            //    string[] arrStrAppearCtrl = Request["appearCtrl"].Split(',');
            //    foreach (string strCtrl in arrStrAppearCtrl)
            //    {
            //        int nStartIndex = _PrintContent.IndexOf(strCtrl);
            //        string strNew = _PrintContent.Substring(
            //        _PrintContent = _PrintContent.Replace("display:none", "display:block");
            //    }
            //}
        }
    } 
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ConstructPrintContent();
            if (!string.IsNullOrEmpty(Request["GridviewId"]))
            {
                this.Gridviewid.Value = Request["GridviewId"].ToString();
            }
        }        
    }
   
}
