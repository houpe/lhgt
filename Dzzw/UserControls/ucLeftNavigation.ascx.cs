﻿ 
// 创建人  ：LinJian
// 创建时间：2006-09-04
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Business.Menu;
using Business;

/// <!--
/// 功能描述  : 菜单导航控件
/// 创建人  : LinJian
/// 创建时间: 2006-09-04
/// -->
public partial class UserControls_ucLeftNavigation : System.Web.UI.UserControl
{
    private XtMenuOperation clsMenu = new XtMenuOperation();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            GenerateBodyScript();
        }
    }

    public void GenerateBodyScript()
    {
        Session["Menu"] = null;
        if (Session["Menu"] == null)
        {
            if (Session["UserID"] != null)
            {
                DataTable dtParent = clsMenu.GenerateParentMenu();

                int i = 0;
                string userid = Convert.ToString(Session["userid"]);
                string strHtml = string.Empty;

                for (i = 0; i < dtParent.Rows.Count; i++)
                {
                    DataTable dtChild = clsMenu.GenerateChildMenu(dtParent.Rows[i]["menu_parent_id"].ToString(), userid);

                    if (dtChild.Rows.Count > 0)
                    {
                        strHtml += string.Format(@"<div id='divParent{0}' title='{1}'>",i, dtParent.Rows[i]["menu_parent_desc"].ToString().Trim());
                        for (int j = 0; j < dtChild.Rows.Count; j++)
                        {
                            strHtml += string.Format(@"<div id=""divChild{0}{1}""><img src='images/ico/ico-01.gif'/><a href='#' onclick=""addTab('{2}','{3}')"" title='{3}'>{3}</a></div>",
                            i, j, dtChild.Rows[j]["linkhref"], dtChild.Rows[j]["menu_child_desc"].ToString().Trim());
                        }
                        strHtml += "</div>";
                    }
                    
                }

                if (dtParent.Rows.Count > 0)
                {
                    Session["Menu"] = strHtml;
                }
            }
        }
    }
}
