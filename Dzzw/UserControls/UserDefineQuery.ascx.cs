﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Business;
using Business.Common;

public partial class UserControls_UserDefineQuery : System.Web.UI.UserControl
{
    private string _tablename=string.Empty;
    private string _text = string.Empty;
    private string _strDisablefields = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public string DisabledFields
    {
        get
        {
            return _strDisablefields;
        }
        set
        {
            _strDisablefields = value;
        }
    }

    public string Text
    {
        get 
        {
            return _text;
        }
    }

    public string TableName
    {
        get 
        {
            return _tablename;
        }
        set
        {
            _tablename = value;
        }
    }

    public string Condition
    {
        get
        {
            return this.txtSelectedCondition.Text;
        }
    }

    
    public void BindData()
    {
        if (!string.IsNullOrEmpty(_tablename))
        {
            OracleSystemOperation oracleoperation = new OracleSystemOperation();
            DataTable dtTemp = oracleoperation.GetFieldsNameAndCommentFromTable(_tablename, "BLOB");

            foreach (DataRow row in dtTemp.Rows)
            {
                if (_strDisablefields == string.Empty || !_strDisablefields.Contains(row["COLUMN_NAME"].ToString()))
                {
                    ListItem listItem = new ListItem();
                    listItem.Value = row["COLUMN_NAME"].ToString();
                    listItem.Text = row["COMMENTS"].ToString();
                    this.ddlFieldQuery.Items.Add(listItem);                    
                }
            }
        }
    }


    protected void btnClick_Click(object sender, EventArgs e)
    {       
        this.txtSelectedCondition.Text = string.Empty;
    }
}
