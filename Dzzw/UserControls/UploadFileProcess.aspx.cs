﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Business.Common;

public partial class Controls_UploadFileProcess : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //获取改过的名称和所选路径组合成的字符串
        string varinfo = Request["varinfo"];
        //分解成数组
        string[] infoarray = varinfo.Split('|');
        //获取File文件并循环上传
        HttpFileCollection files = HttpContext.Current.Request.Files;
        for (int i = 0; i < files.Count - 1; i++)
        {
            Response.Write(string.Format("<script>window.parent.SetStatus('{0}','上传中...');</script>",i + 1));
            Response.Flush();
            try
            {
                //分解该文件的名称和路径
                string[] PathAndName = infoarray[i].Split('^');
                //读取文件
                AttachmentFileUpload ObjFile = new AttachmentFileUpload();
                if (ObjFile.readfile((HttpPostedFile)files[i]))
                {
                    //修正路径及文件名
                    PathAndName[1] = PathAndName[1].Replace(ObjFile.GetFileExt().ToString(), "");
                    if (PathAndName[0].Equals("/"))
                    {
                        PathAndName[0] = PathAndName[1];
                    }
                    else
                    {
                        PathAndName[0] = PathAndName[0] + "/" + PathAndName[1];
                    }
                    //插入数据库
                    if (!ObjFile.Update_St_Attachment(Request["IID"], PathAndName[0], PathAndName[1], 
                        Session["UserId"].ToString(), Request["step"].ToString()))
                    {
                        Response.Write("<script>window.parent.SetStatus('上传失败');</script>");
                        Response.Flush();
                    }
                }
            }
            catch
            {
                Response.Write("<script>window.parent.SetStatus('上传失败');</script>");
                Response.Flush();
            }
            Response.Write(string.Format("<script>window.parent.SetStatus('{0}','100%');</script>",i + 1));
            Response.Flush();
        }
        Response.Write("<script>window.parent.Finish();</script>");
        Response.Flush();
    }
}
