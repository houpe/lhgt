﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="JqueryDataGrid.ascx.cs" Inherits="UserControls_JqueryDataGrid" %>


<script type="text/javascript">
    function GetGridColAttr() {
        var hidValue = "";
        var tabGrid = $('#tabResult').datagrid('options');
        if (tabGrid.columns.length > 0) {
            if (tabGrid.columns[0].length > 0) {
                for (var i = 0; i < tabGrid.columns[0].length; i++) {
                    hidValue += tabGrid.columns[0][i].title + "☆" + tabGrid.columns[0][i].width;
                    if (i < tabGrid.columns[0].length - 1) {
                        hidValue += "★";
                    }
                }
            }
        }
        if (hidValue != null && hidValue != "") {
            return hidValue;
        }
        else {
            return "";
        }
    }

    //带入url根据查询的数据返回csv
    function bsuExportXls() {
        //如果页面中没有用于下载iframe，增加iframe到页面中
        if ($('#downloadXsl').length <= 0)
            $('body').append("<iframe id=\"downloadXsl\" style=\"display:none\"></iframe>");
        $('#downloadXsl').attr('src', '../UserControls/JqueryDataGridExportXls.aspx?flag=1' + paramInfo);
    }

</script>

<div id="tabTools">
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-ok',plain:true">全选</a>
    <a href="#" class="easyui-linkbutton" onclick="bsuExportXls()" data-options="iconCls:'icon-redo',plain:true">导出Excel</a>
</div>
<table id="tabResult">
</table>
