﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Calendar.ascx.cs" Inherits="Controls_Com_Calendar"
    ClassName="Controls_Com_Calendar" %>
<asp:TextBox ID="txtDateTime" runat="server" Width="135px"></asp:TextBox>
<asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtDateTime" Display="Dynamic"
    ErrorMessage="日期格式错误" Type="Date" MaximumValue="9999-12-31" MinimumValue="1000-1-1">
    *
</asp:RangeValidator>
<asp:RequiredFieldValidator ID="rvDateTime" runat="server" ControlToValidate="txtDateTime"
    Enabled="False" Display="Dynamic" ErrorMessage="时间不能为空">*
</asp:RequiredFieldValidator>
    
<asp:CustomValidator ID="cvTime" runat="server" ControlToValidate="txtDateTime" Enabled="false"
    ErrorMessage="CustomValidator" Display="Dynamic">
*
</asp:CustomValidator>