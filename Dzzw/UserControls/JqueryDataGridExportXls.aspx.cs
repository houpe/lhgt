﻿using Business.Common;
using Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WF_Business;
public partial class UserControls_JqueryDataGridExportXls : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //获取专题名称
            string tabName = Request["tabName"];
            string colNames = Request["colNames"];
            string queryWhere = Request["queryWhere"];
            string postSql = Request["postSql"];

            DataTable dtSource = new DataTable();
            if (!string.IsNullOrEmpty(postSql))
            {
                SysParams.OAConnection().RunSql(postSql, out dtSource);
            }
            else
            {
                OracleSystemOperation osoTemp = new OracleSystemOperation();
                dtSource = osoTemp.GetDataByCommonParams(tabName, colNames, queryWhere);
            }

            string strFileName = string.Format("{0}.xls", Guid.NewGuid());
            ExcelOperation.TableToExcelCustom(dtSource, strFileName);
        }
    }

    public override void VerifyRenderingInServerForm(System.Web.UI.Control control)
    {
    }
}