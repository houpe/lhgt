﻿<%@ WebHandler Language="C#" Class="JqueryDataGridHandler" %>

using System;
using System.Web;
using System.Data;

public class JqueryDataGridHandler : IHttpHandler
{
    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/plain";
        //获取专题名称
        string tabName = context.Request["tabName"];
        string colNames = context.Request["colNames"];
        string queryWhere = context.Request["queryWhere"];
        string postSql = context.Request["postSql"];

        DataTable dtSource = new DataTable();
        if (!string.IsNullOrEmpty(postSql))
        {
            WF_Business.SysParams.OAConnection().RunSql(postSql, out dtSource);
        }
        else
        {
            Business.Common.OracleSystemOperation osoTemp = new Business.Common.OracleSystemOperation();
            dtSource = osoTemp.GetDataByCommonParams(tabName, colNames, queryWhere);            
        }

        //获取页码和一页显示的记录数
        int pageNumber = Int32.Parse(context.Request["page"]);
        int pageSize = Int32.Parse(context.Request["rows"]);

        context.Response.Write(GenJson(dtSource, pageNumber, pageSize));
    }

    
    private string GenJson(DataTable dt, int pageNumber, int pageSize)
    {
        string jsonString = Common.JsonOperation.DataToJson(dt, pageNumber, pageSize);
        return jsonString;
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}