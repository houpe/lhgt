﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


public partial class UserControls_TextBoxWithValidator : UserControl, WebControls.IUserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    public enum ValidatingType
    {
        Required, Integer, Double, IDCard, Phone, RegularExpression, Postcode, Cczc, Passwords, UserId
    }


    #region 设置属性

    /// <summary>
    /// 设置文本录入的最大长度---add by lj
    /// </summary>
    public int TextMaxLength
    {
        get { return txtTextBox.MaxLength; }
        set { txtTextBox.MaxLength = value; }
    }

    public string Text
    {
        get { return txtTextBox.Text; }
        set { txtTextBox.Text = value; }
    }

    public TextBoxMode TextMode
    {
        get { return txtTextBox.TextMode; }
        set { txtTextBox.TextMode = value; }
    }

    public int Rows
    {
        get { return txtTextBox.Rows; }
        set { txtTextBox.Rows = value; }
    }

    public string Value
    {
        get { return txtTextBox.Text; }
        set { txtTextBox.Text = value; }
    }

    public bool ReadOnly
    {
        get { return txtTextBox.ReadOnly; }
        set { txtTextBox.ReadOnly = value;}
    }

    public bool Enabled
    {
        get { return txtTextBox.Enabled; }
        set { txtTextBox.Enabled = value; }
    }

    public string ErrorMessage
    {
        get { return cv.ErrorMessage; }
        set
        {
            cv.ErrorMessage = value;
            rfv.ErrorMessage = value;
            rev.ErrorMessage = value;
        }
    }

    /// <summary>
    /// 验证组
    /// </summary>
    public string ValidationGroup
    {
        get { return rfv.ValidationGroup; }
        set
        {
            cv.ValidationGroup = value;
            rfv.ValidationGroup = value;
            rev.ValidationGroup = value;
        }
    }

    /// <summary>
    /// 子控件的clientid
    /// </summary>
    public string ChildControlClientId
    {
        get
        {
            return txtTextBox.ClientID;
        }
    }

    public Unit Width
    {
        get { return txtTextBox.Width; }
        set { txtTextBox.Width = value; }
    }

    public Unit Height
    {
        get { return txtTextBox.Height; }
        set { txtTextBox.Height = value; }
    }

    public Unit BorderWidth
    {
        get { return txtTextBox.BorderWidth; }
        set { txtTextBox.BorderWidth = value; }
    }

    public string UniqueName
    {
        get { return txtTextBox.Attributes["uniqueName"]; }
        set { txtTextBox.Attributes["uniqueName"] = value; }
    }

    private string _strRelationControls;
    /// <summary>
    /// 设置关联的控件的ClientID,以","间隔
    /// </summary>
    public string RelationControls
    {
        get { return _strRelationControls; }
        set { _strRelationControls = value; }
    }

    /// <summary>
    /// 设置客户端onchange事件
    /// </summary>
    public string ClientOnChange
    {
        set
        {
            if (!string.IsNullOrEmpty(value))
            {
                txtTextBox.Attributes.Add("onchange", value);
            }
        }
    }

    /// <summary>
    /// 设置关联的标志
    /// </summary>
    public bool bSetRelation
    {
        set
        {
            if (value == true)
            {
                if (_strRelationControls != null)
                {
                    string[] arrStrCtrls = _strRelationControls.Split(',');
                    string strCtrlClient = txtTextBox.ClientID;
                    string strScript = "javascript:";

                    foreach (string strCtrlName in arrStrCtrls)
                    {
                        strScript += string.Format("document.getElementById('{0}').value=document.getElementById('{1}').value;",
                        strCtrlName, strCtrlClient);
                    }

                    txtTextBox.Attributes.Add("onchange", strScript);
                }
            }
        }
    }

    private string _strUpperRelationControls;

    public string UpperRelationControls
    {
        get
        {
            return _strUpperRelationControls;
        }
        set
        {
            _strUpperRelationControls = value;
        }
    }

    private string _strRelationControlUniqueName;

    public string RelationControlUniqueName
    {
        get
        {
            return _strRelationControlUniqueName;
        }
        set
        {
            _strRelationControlUniqueName = value;
        }
    }

    public bool BUpperRelationControls
    {
        set
        {
            if (value == true)
            {
                if (_strUpperRelationControls != null)
                {
                    string strScript = string.Empty;


                    string[] arrStrCtrls = _strUpperRelationControls.Split(',');
                    foreach (string strCtrlName in arrStrCtrls)
                    {
                        strScript += string.Format("document.getElementById('{0}').value=creat(getInputByUniqueName('{1}').value);",
                            strCtrlName, _strRelationControlUniqueName);
                    }
                    txtTextBox.Attributes.Add("onchange", strScript);
                }
            }
        }
    }

    public bool ValidateEmptyText
    {
        get { return cv.ValidateEmptyText; }
        set { cv.ValidateEmptyText = value; }
    }

    public string ValidationExpression
    {
        get { return rev.ValidationExpression; }
        set { rev.ValidationExpression = value; }
    }


    #endregion

    public event EventHandler<EventArgs> _textChanged;
    /// <summary>
    /// 捕获ONCHANGE事件
    /// </summary>
    public event EventHandler<EventArgs> OnTextChanged
    {
        add
        {
            _textChanged += value;
        }
        remove
        {
            _textChanged -= value;
        }
    }

    /// <summary>
    /// 第二个验证的类型
    /// </summary>
    public ValidatingType SecondValide
    {
        get { return (ValidatingType)ViewState["TextBoxWithValidator_SecondType"]; }
        set
        {
            ViewState["TextBoxWithValidator_SecondType"] = value;
            switch (value)
            {
                case ValidatingType.Required:
                    rfv.Enabled = true;
                    break;
                case ValidatingType.Integer:
                    cv.Enabled = true;
                    cv.ClientValidationFunction = "checkInteger";
                    break;
                case ValidatingType.Double:
                    cv.Enabled = true;
                    cv.ClientValidationFunction = "checkDouble";
                    break;
                case ValidatingType.IDCard:
                    cv.Enabled = true;
                    cv.ClientValidationFunction = "checkNumber";
                    break;
                case ValidatingType.Phone:
                    cv.Enabled = true;
                    cv.ClientValidationFunction = "checkPhoneNumber";
                    break;
                case ValidatingType.Postcode:
                    cv.Enabled = true;
                    cv.ClientValidationFunction = "checkPostcode";
                    break;
                case ValidatingType.Cczc:
                    cv.Enabled = true;
                    cv.ClientValidationFunction = "checkCczc";
                    break;
                case ValidatingType.RegularExpression:
                    rev.Enabled = true;
                    break;
            }
        }
    }

    /// <summary>
    /// 验证的类型
    /// </summary>
    public ValidatingType Type
    {
        get { return (ValidatingType)ViewState["TextBoxWithValidator_Type"]; }
        set { 
            ViewState["TextBoxWithValidator_Type"] = value;
            switch (value)
            {
                case ValidatingType.Required:
                    rfv.Enabled = true;
                    break;
                case ValidatingType.Integer:
                    cv.Enabled = true;
                    cv.ClientValidationFunction = "checkInteger";
                    break;
                case ValidatingType.Double:                    
                    cv.Enabled = true;
                    cv.ClientValidationFunction = "checkDouble";
                    break;
                case ValidatingType.IDCard:
                    cv.Enabled = true;
                    cv.ClientValidationFunction = "checkNumber";
                    break;
                case ValidatingType.Phone:
                    cv.Enabled = true;
                    cv.ClientValidationFunction = "checkPhoneNumber";
                    break;
                case ValidatingType.Postcode:
                    cv.Enabled = true;
                    cv.ClientValidationFunction = "checkPostcode";
                    break;
                case ValidatingType.Cczc:
                    cv.Enabled = true;
                    cv.ClientValidationFunction = "checkCczc";
                    break;
                case ValidatingType.Passwords:
                    cv.Enabled = true;
                    cv.ClientValidationFunction = "checkPasswords";
                    break;
                case ValidatingType.UserId:
                    cv.Enabled = true;
                    cv.ClientValidationFunction = "checkUserId";
                    break;
                case ValidatingType.RegularExpression:
                    rev.Enabled = true;
                    break;
            }
        }
    }

    protected void txtTextBox_TextChanged(object sender, EventArgs e)
    {
        if(sender!=null&&e!=null&&_textChanged!=null)
            _textChanged(sender, e);
    }
}
