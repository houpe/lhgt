﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MsgPlate.ascx.cs" Inherits="UserControl_MsgPlate" %>
<asp:DataGrid ID="DataGrid1" runat="server" BackColor="White" CellPadding="4" GridLines="None"
    AutoGenerateColumns="False" Width="100%" PageSize="15" ShowHeader="False">
    <SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#339966"></SelectedItemStyle>
    <ItemStyle ForeColor="#333333" BackColor="#E7F9F8" Font-Bold="False" Font-Italic="False" HorizontalAlign="Left"
        Font-Overline="False" Font-Strikeout="False" Font-Underline="False"></ItemStyle>
    <HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#E0E0E0"></HeaderStyle>
    <FooterStyle ForeColor="#333333" BackColor="White"></FooterStyle>
    <Columns>
        <asp:HyperLinkColumn DataTextField="TITLE" DataNavigateUrlField="ID"
            DataNavigateUrlFormatString="javascript:window.parent.openDivDlg('信息查看','ManageInside/detail.aspx?id={0}');" ItemStyle-CssClass="hline">
        </asp:HyperLinkColumn>
    </Columns>
    <PagerStyle HorizontalAlign="Right" ForeColor="White" BackColor="#E0E0E0" Mode="NumericPages"
        Visible="False"></PagerStyle>
    <AlternatingItemStyle BackColor="#F7FDFE" Font-Bold="False" Font-Italic="False" Font-Overline="False"
        Font-Strikeout="False" Font-Underline="False" />
</asp:DataGrid>
