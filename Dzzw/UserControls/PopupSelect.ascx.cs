﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using WebControls;
using Business.Admin;

public partial class UserControls_PopupSelect : System.Web.UI.UserControl, IUserControl
{
    #region 属性

    private const string POPUP_SELECT = "UserControls_PopupSelect_Script";

    private Hashtable DataTable
    {
        get
        {
            if (ViewState["PopupSelect_DataTable"] == null)
            {
                ViewState["PopupSelect_DataTable"] = new Hashtable();
            }
            return ViewState["PopupSelect_DataTable"] as Hashtable;
        }
    }

    public string RelationControlClientId
    {
        get { return ViewState["RelationControlClientId"] as string; }
        set { ViewState["RelationControlClientId"] = value; }
    }

    public string UserType
    {
        get { return ViewState["PopupSelect_UserType"] as string; }
        set { ViewState["PopupSelect_UserType"] = value; }
    }

    public string Name
    {
        get { return ViewState["PopupSelect_Name"] as string; }
        set { ViewState["PopupSelect_Name"] = value; }
    }

    public string Value
    {
        get { return txtValue.Text; }
        set
        {
            txtValue.Text = value;
            txtDisplay.Text = value;
        }
    }

    /// <summary>
    /// 查询条件
    /// </summary>
    public string Where
    {
        get { return ViewState["PopupSelect_Where"] as string; }
        set { ViewState["PopupSelect_Where"] = value; }
    }

    /// <summary>
    /// 设置成多行编辑
    /// </summary>
    public bool IsMultiLine
    {
        set
        {
            if (value)
            {
                this.txtDisplay.TextMode = TextBoxMode.MultiLine;
                this.txtValue.TextMode = TextBoxMode.MultiLine;
            }
        }
    }
    public bool ReadOnly
    {
        get { return btnSelect.Disabled; }
        set { btnSelect.Disabled = value; txtDisplay.Enabled = !value; }
    }

    public string Text
    {
        get { return txtDisplay.Text; }
        set { txtValue.Text = value; txtDisplay.Text = value; }
    }

    public Unit Width
    {
        get { return txtDisplay.Width; }
        set { txtDisplay.Width = value; }
    }

    public string DisplayClientID
    {
        get { return txtDisplay.ClientID; }
    }

    public string ValueClientID
    {
        get { return txtValue.ClientID; }
    } 

    #endregion

    #region 窗体加载
    /// <summary>
    /// 窗体加载
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtDisplay.Attributes.Add("guid", Guid.NewGuid().ToString());
            txtValue.Attributes.Add("guid", Guid.NewGuid().ToString());
            if (Name != null && Name != string.Empty)
            {
                DataTable dtTable = DictOperation.GetSysParams(Name, "");
                string[] values = Value.Split(',');
                List<string> displays = new List<string>();
                txtDisplay.Text = "";
                foreach (string value in values)
                {
                    foreach (DataRow row in dtTable.Rows)
                    {
                        if (value == row["KEYCODE"].ToString())
                        {
                            displays.Add(row["KEYVALUE"].ToString());
                        }
                    }
                }
                txtDisplay.Text = string.Join(",", displays.ToArray());
            }

            string displayGuid = txtDisplay.Attributes["guid"];
            string valueGuid = txtValue.Attributes["guid"];

            string strJs = string.Format("popupSelect('{0}','{1}','{2}','{3}','{4}','{5}')",
                Page.ResolveUrl("~/UserControls/PopupSelectPage.aspx"), displayGuid, valueGuid,
                UserType, Name, RelationControlClientId);
            btnSelect.Attributes.Add("onclick", strJs);

            if (!string.IsNullOrEmpty(RelationControlClientId))
            {
                txtDisplay.Width = 0;
                txtDisplay.Height = 0;
                txtValue.Width = 0;
                txtValue.Height = 0;
            }
        }
    } 
    #endregion
}
