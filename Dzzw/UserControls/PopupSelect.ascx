﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PopupSelect.ascx.cs" Inherits="UserControls_PopupSelect" %>
<style type="text/css">
    /* 提示框div块的样式 */
    #popup { position: absolute; width: 202px; color: #004a7e; font-size: 12px; font-family: Arial, Helvetica, sans-serif; left: 41px; top: 25px; background-color:White; }
    
    /* 显示提示框的边框 */
    #popup.show { border: 1px solid #004a7e; }
    
    /* 提示框的样式风格 */
    ul { list-style: none; margin: 0px; padding: 0px; color: #004a7e; }
    li.mouseOver { background-color: #004a7e; color: #FFFFFF; }
</style>
<script src="../ScriptFile/jquery.js" type="text/javascript"></script>
<script type="text/javascript">
    function popupSelect(url, displayGuid, valueGuid, usertype, name, otherClientId) {
        window.open(url + '?display=' + displayGuid + '&value=' + valueGuid +
                    '&usertype=' + usertype + '&name=' + name + '&otherClient=' + otherClientId,
                    '_blank', 'width=580,height=530,scrollbars=yes, left=200,top=100');
    }

    function clearDropList() {
        //清除提示内容 
        $("#option_ul").empty();
        $("#popup").removeClass("show");
    }

    //设置值
    function setValues(arrValues) {
        //显示提示框，传入的参数即为匹配出来的结果组成的数组 
        clearDropList(); //每输入一个字母就先清除原先的提示，再继续 
        $("#popup").addClass("show");
        var ctrlId = "<%=txtDisplay.ClientID%>";

        for (var i = 0; i < arrValues.length; i++)
        //将匹配的提示结果逐一显示给用户 
            $("#option_ul").append($("<li>" + arrValues[i] + "</li>"));

        $("#option_ul").find("li").click(function () {
            $("#" + ctrlId).val($(this).text());
            clearDropList();
        }).hover(
            function () { $(this).addClass("mouseOver"); },
            function () { $(this).removeClass("mouseOver"); }
            );
    }

    function showDropList() {
        var ctrlId = "<%=txtDisplay.ClientID%>";

        if ($("#" + ctrlId).val().length > 0) {
            //获取异步数据
            $.get("../UserControls/AutoInputProcess.ashx", {name:"<%=Name%>", inputData: $("#" + ctrlId).val() },
                function (data) {
                    var aResult = new Array();
                    if (data.length > 0) {
                        aResult = data.split(",");
                        setValues(aResult); //显示服务器结果 

                        //定位位置
                        var pos = $("#" + ctrlId).position();

                        $("#popup").css({ "left": pos.left, "top": (pos.top + 20)});
                    }
                    else
                        clearDropList();
                });
        }
        else
            clearDropList();  //无输入时清除提示框（例如用户按del键） 
    } 
</script>
<asp:TextBox ID="txtDisplay" runat="server" onkeyup="showDropList();"></asp:TextBox>
<asp:TextBox ID="txtValue" runat="server" BorderStyle="none" BorderWidth="0" Width="0"
    Height="0"></asp:TextBox>
<input id="btnSelect" type="button" value="选择" runat="server" class="NewButton" />
<div id="popup">
    <ul id="option_ul">
    </ul>
</div>
