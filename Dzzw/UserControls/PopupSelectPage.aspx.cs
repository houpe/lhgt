﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Business.Admin;
using SbBusiness.User;

public partial class PopupSelectPage : System.Web.UI.Page
{
    /// <summary>
    /// 窗体加载
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        string strUserTypeName = Request.Params["usertype"].Trim();

        if (!string.IsNullOrEmpty(strUserTypeName))//操作用户表
        {
            CustomGridView1.DataKeyNames = new string[] { "用户名称" };
        }
        else//操作数据字典
        {
            CustomGridView1.DataKeyNames = new string[] { "类别", "键值" };
        }
        
        if (!IsPostBack)
        {
            BindData();
        }

    }

    /// <summary>
    /// 完成事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void CustomGridView1_OnComplelteClick(object sender, WebControls.CustomGridViewEventArgs e)
    {
        if (e == null)
        {
            return;
        }

        if (e.MultiSelectKeys.Length == 0)
        {
            return;
        }

        string strUserTypeName = Request.Params["usertype"];

        if (string.IsNullOrEmpty(strUserTypeName))//获取数据字典的值
        {
            ProcessNameAndValue(e.MultiSelectKeys);
        }
        else//处理用户名
        {
            ProcesOnlyValue(e.MultiSelectKeys);
        }
    }

    /// <summary>
    /// 显示值对应的wenben 
    /// </summary>
    /// <param name="keys"></param>
    private void ProcessNameAndValue(DataKey[] keys)
    {
        string[] names = new string[keys.Length];
        string[] values = new string[keys.Length];
        for (int i = 0; i < keys.Length; i++)
        {
            DataKey key = keys[i];

            string[] strKeyInfo = CustomGridView1.DataKeyNames;

            names[i] = key.Values[strKeyInfo[0]].ToString();
            values[i] = key.Values[strKeyInfo[1]].ToString();
        }
        string script = @"
                <script type=""text/javascript"">
                    setParent('" + Request.Params["display"] + "','" + 
                                 Request.Params["value"] + "', '" + string.Join(",", names) + "' , '" + 
                                 string.Join(",", values)+ @"');
                    window.close();
                </script>
                ";
        Page.ClientScript.RegisterStartupScript(this.GetType(), "setParent", script);
    }

    /// <summary>
    /// 处理用户名
    /// </summary>
    /// <param name="keys"></param>
    private void ProcesOnlyValue(DataKey[] keys)
    {
        string[] usernames = new string[keys.Length];
        for (int i = 0; i < keys.Length; i++)
        {
            DataKey key = keys[i];
            usernames[i] = key.Value.ToString();
        }
        string username = string.Join(",", usernames);
        string script = @"
                <script type=""text/javascript"">
                    setParent('" + Request.Params["display"] + "','" + Request.Params["value"] + "', '"
                                 + username + "','', '" + Request.Params["otherClient"] + @"');
                    window.close();
                </script>
                ";
        Page.ClientScript.RegisterStartupScript(this.GetType(), "setParent", script);
    }

    /// <summary>
    /// 取消按钮
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void CustomGridView1_OnCancelClick(object sender, EventArgs e)
    {
        Response.Write("<script>window.close();</script>");
    }

    protected void CustomGridView1_OnLoadData(object sender, EventArgs e)
    {
        BindData();
    }

    /// <summary>
    /// 绑定数据
    /// </summary>
    private void BindData()
    {
        string strUserTypeName = Request.Params["usertype"].Trim();
        string name = Request.Params["name"];
        string query = txtSearch.Text;
        DataTable dtTable = null;

        if (string.IsNullOrEmpty(strUserTypeName))//获取数据字典的值
        {
            dtTable = DictOperation.GetSysParams(name, "");

            //更改列名
            dtTable.Columns["keyvalue"].ColumnName = "类别";
            dtTable.Columns["keycode"].ColumnName = "键值";

            CustomGridView1.HideColName = "name,imptime,exptime";
        }
        else//操作用户名
        {
            SysUserHandle userHandle = new SysUserHandle();
            SysUserTypeHandle sutoTemp = new SysUserTypeHandle();

            string strUserType = sutoTemp.GetUserTypeByName(strUserTypeName);
            dtTable = userHandle.GetUserName(strUserType, query);
        }
        CustomGridView1.DataSource = dtTable;
        CustomGridView1.RecordCount = dtTable.Rows.Count;
        //CustomGridView1.HideColName = "ID";
        CustomGridView1.DataBind();
    }

    /// <summary>
    /// 查询
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }
}
