﻿<%@ WebHandler Language="C#" Class="MutiUploadHandler" %>

using System;
using System.Web;
using System.IO;
using Business.Common;
using System.Web.SessionState;

public class MutiUploadHandler : IHttpHandler, IRequiresSessionState
{
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/plain";
        context.Response.Charset = "utf-8";

        HttpPostedFile file = context.Request.Files["Filedata"];
        //string uploadPath =
        //    HttpContext.Current.Server.MapPath("..\\FileUpload\\fileroot") + "\\";

        if (file != null)
        {
            //if (!Directory.Exists(uploadPath))
            //{
            //    Directory.CreateDirectory(uploadPath);
            //}
            //file.SaveAs(uploadPath + file.FileName);
            //string msg = "";
            //string swfpath = "";
            //Common.WordConvert.Change(uploadPath + file.FileName, "swf", ref msg);
            //Common.ConvertToSwf.ConvertFileToSwf(uploadPath + file.FileName, ref msg, ref swfpath);

            AttachmentFileUpload ObjFile = new AttachmentFileUpload();
            if (ObjFile.readfile(file))
            {
                //修正路径及文件名
                string strFileExt = file.FileName.Substring(file.FileName.LastIndexOf(".")+1);
                string strFileName = file.FileName.Substring(0,file.FileName.LastIndexOf("."));
                
                string strNodeName = string.Format("({0}){1}", strFileExt, strFileName);
                string strNodePath = "";
                if (context.Request["ml"].Equals("/"))
                {
                    strNodePath = strNodeName;
                }
                else//选择其他目录
                {
                    strNodePath = context.Request["ml"] + "/" + strNodeName;
                }
                
                
                //插入数据库
                if (!ObjFile.Update_St_Attachment(context.Request["IID"], strNodePath, strNodeName, context.Session["UserId"].ToString(), context.Request["step"]))
                {
                    //下面这句代码缺少的话，上传成功后上传队列的显示不会自动消失
                    context.Response.Write("1");
                }
            }
            context.Response.Write("0");
        }
        else
        {
            context.Response.Write("0");
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}