﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SerialGenerate.aspx.cs" Inherits="WebPubManager_SerialGenerate" %>

<html>
<head runat="server">
    <title>业务办理申请审核</title>
    <link href="../css/page.css" rel="stylesheet" type="text/css" />
</head>
<script type="text/javascript">
    function tosubmit(page) {
        var strUrl = "SerialGenerate.aspx";
        if (page == "" || page == null)
            strUrl += "?pagenow=" + document.all.jumpPage.value
        else
            strUrl += "?pagenow=" + page;

        strUrl += '&flag=<%=Request.Params["flag"]%>'

        window.location.href = strUrl;
    }

    function SubmitClient(paramId, paramResult, paramflowname) {
        if (confirm('确定执行此操作?')) {
            var url = "SerialResult.aspx?sid=" + paramId + "&resultFlag=" + paramResult + "&flowname=" + paramflowname;
            this.location = url;
        }
    }

    function query() {
        var paramId = document.getElementById("txtSeach").value;
        var url = "SerialGenerate.aspx?flag=query&username=" + paramId;
        this.location = url;
    }

    function keyEvent() {
        if (event.keyCode == 13) {
            //tab健转换焦点
            event.keyCode = 9;
            event.returnValue = false;
            query();
        }
    }
    //查询详细申请理由 addby zhongjian 20090924
    function ViewMessage(id) {
        var url = "SerialManager.aspx?id=" + id;
        window.open(url, "view", "top=200,left=200,toolbar=no,menubar=no,scrollbars=yes,location=no,status=no,width=600,height=300;");
    }
    //查询用户详细 addby zhongjian 20090924
    function UserMessage(id) {
        var url = "UserMassage.aspx?id=" + id;
        window.open(url, "view", "top=20,left=20,toolbar=no,menubar=no,scrollbars=yes,location=no,status=no,width=500,height=550;");
    }

    //设置tabcontrol按钮的背景颜色
    function SetBackColor(varCtrl) {
        if (varCtrl == "0") {
            document.getElementById("td1").style.backgroundImage = "url(../images/top_button_03.gif)";
            document.getElementById("td2").style.backgroundImage = "url(../images/top_button_05.gif)";
        }
        else {
            document.getElementById("td1").style.backgroundImage = "url(../images/top_button_05.gif)";
            document.getElementById("td2").style.backgroundImage = "url(../images/top_button_03.gif)";
        }
    }
</script>
<body>
    <form id="form1" runat="server">
    <table border="0" id="tabMenu">
        <tr>
            <td id="td1" runat="server" style="background-image: url(../images/top_button_05.gif);
                background-repeat: no-repeat; text-align: center; height: 35px; width: 93px;">
                <a href="SerialGenerate.aspx?flag=nosp">未审批</a>
            </td>
            <td id="td2" runat="server" style="background-image: url(../images/top_button_03.gif);
                background-repeat: no-repeat; text-align: center; height: 35px; width: 93px;">
                <a href="SerialGenerate.aspx?flag=ysp">已审批</a>
            </td>
            <td style="width: 200px; text-align: right;">
                用户名称查询：
            </td>
            <td style="text-align: center;">
                <input type="text" id="txtSeach" onkeydown="keyEvent()" />
            </td>
            <td>
                <input type="button" class="NewButton" value="查询" name="btnQuery" onclick="query()">
            </td>
        </tr>
    </table>
    <table width="100%" border="0" align="center">
        <tr>
            <td>
                <div class="tab_middle">
                    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="tab">
                        <tr>
                            <th>
                                用户id
                            </th>
                            <th>
                                用户名称
                            </th>
                            <th>
                                业务名称
                            </th>
                            <th>
                                申请理由
                            </th>
                            <th>
                                操作
                            </th>
                        </tr>
                        <%=strHtml%>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="4">
                        <tr>
                            <td align="right" colspan="8" height="20">
                                每页<%=pagesize%>行 共<%=mypage.RecordCount %>行 当前第<%=pagenow%>页 共<%=pagecount %>页
                                <%
                                    if (pagenow == 1)
                                    {
                                        Response.Write(" 首页 上一页");
                                    }
                                    else
                                    {
		
                                %>
                                <a href="javascript:tosubmit('1')">首页</a> <a href="javascript:tosubmit('<%=pagenow - 1%>')">
                                    上一页</a>
                                <%
                                    }
	
                                %>
                                <%
                                    if (pagenow == pagecount)
                                    {
                                        Response.Write("下一页 尾页");
                                    }
                                    else
                                    {
		
                                %>
                                <a href="javascript:tosubmit('<%=pagenow + 1%>')">下一页</a> <a href="javascript:tosubmit('<%=pagecount%>')">
                                    尾页</a>
                                <%
                                    }
	
                                %>
                                转到第<select name="jumpPage" id="jumpPage" onchange="tosubmit()">
                                    <%
                                        for (int i = 1; i <= pagecount; i++)
                                        {
                                            if (i == pagenow)
                                            {
			
                                    %>
                                    <option selected value="<%=i%>">
                                        <%=i%></option>
                                    <%
}
                                            else
                                            {
			
                                    %>
                                    <option value="<%=i%>">
                                        <%=i%></option>
                                    <%
                                        }
                                        }
	
                                    %>
                                </select>
                            页
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
