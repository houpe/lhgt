﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WorkPowerManager.aspx.cs"
    Inherits="WebPubManager_WorkPowerManager" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>办理事项权限管理</title>
    <link rel="stylesheet" type="text/css" href="../css/page.css" />

    <script language="javascript" type="text/javascript" src="../ScriptFile/Regex.js"></script>

</head>
<body>
    <form id="form1" runat="server">
        <div style="width: 100%">
            <div style="text-align: center; margin: 0px auto; width:600px; margin-top:10px;" class="tab">
                <div class="new_box02" style="height: 100px; border:1px; border-width:1px; border-style:solid;">
                    <div class="new_bar02">
                        用户审核设置
                    </div>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="text-align: left;">
                                <asp:CheckBox ID="cbxYhsz" runat="server" Text="注册用户不需预审" /><span style="color: Red">(勾选：不需要预审；不勾选：需要预审)</span>
                            </td>
                            <td style="width: 60px">
                                <asp:LinkButton ID="btnRefer" runat="server" OnClick="btnRefer_Click">保存设置</asp:LinkButton>
                            </td>
                        </tr>
                    </table>
                    <asp:HiddenField ID="HiddenFieldFlag" runat="server" />
                </div>


                <div class="new_box02" style="height: 100px;border:1px; border-width:1px; border-style:solid;margin-top:10px; height:200px;">
                    <div class="new_bar02">
                        留言回复权限配置
                    </div>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="text-align: left;">按流程配置：<asp:DropDownList ID="ddlWorkflow" runat="server" OnSelectedIndexChanged="ddlWorkflow_SelectedIndexChanged"
                                AutoPostBack="True">
                            </asp:DropDownList>
                            </td>
                            <td style="width: 60px">
                                <a href="../SystemManager/membermis.aspx?groupid=<%=ViewState["TypeID"].ToString()%>&groupname=<%= ViewState["FlowType"].ToString()%>"
                                    target="_self">设置权限</a>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left;">按部门配置：<asp:DropDownList ID="ddlDep" runat="server" OnSelectedIndexChanged="ddldep_SelectedIndexChanged"
                                AutoPostBack="True">
                            </asp:DropDownList>
                            </td>
                            <td style="width: 60px">
                                <a href="../SystemManager/membermis.aspx?groupid=<%=ViewState["DepID"].ToString()%>&groupname=<%=ViewState["DepName"].ToString()%>"
                                    target="_self">设置权限</a>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left;">留言咨询回复管理员配置：
                            </td>
                            <td style="width: 60px">
                                <a href="../SystemManager/membermis.aspx?groupid=MessageAdministrator&groupname=留言咨询回复管理员"
                                    target="_self">设置权限</a>
                            </td>
                        </tr>
                    </table>
                </div>

            </div>
        </div>
    </form>
</body>
</html>
