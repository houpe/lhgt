﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LawGuide.aspx.cs" Inherits="WebPubManager_LawGuide" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>办事指南</title>
    <link rel="stylesheet" type="text/css" href="../css/page.css" />
    <script src="../ScriptFile/AutoIframeHeight.js" type="text/javascript"></script>
    <script src="../ScriptFile/jquery.js" type="text/javascript"></script>
    <script type="text/jscript" language="javascript">
        //动态添加办事指南列表  addby zhongjian 20100115
        var fNum = 0;
        //页面加载时执行
        $(document).ready(function () {
            //获取行总数(已存在的信息)
            fNum = parseInt(document.getElementById("hidNumber").value);
            document.getElementById("hidCount").value = fNum;
        });
        //添加事件 addby zhongjian 20100115
        function addRow() {
            fNum += 1;
            //添加行
            var newTr = tbLawGuide.insertRow();
            //添加列
            var newTd0 = newTr.insertCell();
            var newTd1 = newTr.insertCell();
            var newTd2 = newTr.insertCell();
            //设置列内容和属性
            newTd0.innerHTML = "<input id='fTitle" + fNum + "' name='fTitle" + fNum + "' type='text' style='font-weight:bold;text-align:center' />";
            newTd1.innerHTML = "<textarea id='FContent" + fNum + "' name='FContent" + fNum + "' style='overflow:visible;width:98%;'></textarea>";
            newTd2.innerHTML = "<a href='#' onclick='javascript:deleteThisRow(this);'>移除</a>";
            //获取行总数(新添加的行包括已存在的)
            document.getElementById("hidCount").value = fNum;
        }
        //删除触发事件控件所在的行 addby zhongjian 20100115
        function deleteThisRow(targetControl) {
            if (targetControl.tagName == "TR")
                targetControl.parentNode.removeChild(targetControl);
            else
                deleteThisRow(targetControl.parentNode);
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div align="right" style="width: 98%">
        <input type="hidden" value="0" id="hidNumber" runat="server" />
        <input type="hidden" value="0" id="hidCount" runat="server" />
        <input id="btnAddRow" type="button" class="NewButton" value="添加条列" onclick="addRow();" />&nbsp;
        <asp:Button ID="btnSave" CssClass="input" runat="server" Text="保存" OnClick="btnSave_Click" />&nbsp;
    </div>
    <div>
        <span style="text-align: center; color: Red">提示：在移除或修改之后请注意保存。</span></div>
    <div id="mainDiv" style="overflow: auto; height: 800px; width:100%">
        <table width="97%" border="0" cellpadding="1" class="tab" cellspacing="0" id="tbLawGuide">
            <tr style="background-color: Azure">
                <td style="width: 10%">
                    <span style="text-align: center; font-weight: bold;">标题</span>
                </td>
                <td style="width: 80%">
                    <span style="text-align: center; font-weight: bold;">内容</span>
                </td>
                <td style="width: 10%">
                    <span style="text-align: center; font-weight: bold;">操作</span>
                </td>
            </tr>
            <%=BindLawGuide() %>
            <tr id="trDefault" runat="server">
                <td>
                    <input id="fTitle" name="fTitle0" type="text" style="font-weight: bold; text-align: center" />
                </td>
                <td align="left">
                    <textarea rows="2" cols="2" id="FContent" name="FContent0" style="overflow: visible;
                        width: 97%;"></textarea>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
