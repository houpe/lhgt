﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using Business;
using WF_Business;
using Business.Common;

public partial class WebPubManager_FileUpload : PageBase
{
    UploadFileClass ufcGloab = new UploadFileClass();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SetFileUpdatePerproty();

            /***************上移****************/
            if (Request["upId"] != null && Convert.ToInt32(Request["upNo"]) >= 1)
            {
                ufcGloab.UpdateSort(Request["upNo"], Request["flowid"], Request["upId"], 1);
            }

            /***************下移****************/
            if (Request["downId"] != null)
            {
                ufcGloab.UpdateSort(Request["downNo"], Request["flowid"], Request["downId"], 0);
            }
            BindData();
        }

        DeleteResource();
    }

    /// <summary>
    /// 删除附件
    /// </summary>
    private void DeleteResource()
    {
        if (!string.IsNullOrEmpty(Request["id"]) && Request["action"].CompareTo("delete") == 0)
        {
            ufcGloab.DeleteStoreFile(FileUpdate1.TableName, FileUpdate1.KeyId, Request["id"]);

            BindData();
        }
    }

    /// <summary>
    /// 设置文件上传属性
    /// </summary>
    private void SetFileUpdatePerproty()
    {
        //设置文件上传控件
        FileUpdate1.KeyId = "id";
        FileUpdate1.FileStoreFieldName = "RESOURCE_CONTENT";
        FileUpdate1.FileTypeFieldName = "RESOURCE_TYPE";
        FileUpdate1.TableName = "SYS_RESOURCE";

        FileUpdate1.StoreKeyFieldName = "USE_TYPE";

        FileUpdate1.StoreParamFieldName = "table_name";
        FileUpdate1.StoreParamValue = txtResourceName.ID;

        if (!string.IsNullOrEmpty(Request["DataType"]))
        {
            FileUpdate1.StoreKeyValue = Request["DataType"];
        }
        FileUpdate1.FlowIDValue = Request["flowid"];
        FileUpdate1.StoreResourceFieldName = "RESOURCE_NAME";        
        FileUpdate1.StoreResourceNameControlName = txtResourceName.ID;
        FileUpdate1.StoreResourceType = "insert";
    }

    /// <summary>
    /// 上传后触发事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void FileUpdate1_AfterUploadedEvent(object sender, EventArgs e)
    {
        //绑定数据
        BindData();
    }

    /// <summary>
    /// 绑定数据源,绑定datagrid
    /// </summary>
    private void BindData()
    {
        DataTable dtSource = ufcGloab.GetResourceInfo(string.Empty, string.Empty, Request["flowid"]);
        CustomGridView1.DataSource = dtSource;

        int nPage = Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings["pagesize"].ToString ());
        CustomGridView1.PageSize = nPage;
        CustomGridView1.RecordCount = dtSource.Rows.Count;
        CustomGridView1.DataBind();
    }

    /// <summary>
    /// 绑定数据
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void CustomGridView1_OnLoadData(object sender, EventArgs e)
    {
        BindData();
    }

    /// <summary>
    /// 行创建
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void CustomGridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            TableCellCollection dataTC = e.Row.Cells;

            HyperLink hlTemp = e.Row.FindControl("hlDelete") as HyperLink;

            if (hlTemp != null)
            {
                DataRowView dtr = (DataRowView)e.Row.DataItem;

                if (dtr != null)
                {
                    hlTemp.NavigateUrl = string.Format("FileUpload.aspx?id={0}&flowid={1}&DataType={2}&action=delete",
                        dtr["id"], Request["flowid"], Request["DataType"]);
                }
            }
        }
    }

    protected string GetUp(IDataItemContainer row)
    {
        string strHtml = string.Empty;
        if (DataBinder.Eval(row.DataItem, "ORDERDIELD").ToString() == "0")
        {
            strHtml = "";
        }
        else
        {
            strHtml = "<a href='FileUpload.aspx?upId=" + DataBinder.Eval(row.DataItem, "ID").ToString() + "&upNo=" + DataBinder.Eval(row.DataItem, "ORDERDIELD").ToString() + "&flowid=" + Request["flowid"] + "&DataType=" + Request["DataType"] + "'>上移</a>";
        }

        return strHtml;
    }

    protected string GetDown(IDataItemContainer row)
    {
        string strHtml = string.Empty;
        string strCount = UploadFileClass.GetMax(Request["flowid"]);
        if (strCount != null)
        {
            if (DataBinder.Eval(row.DataItem, "ORDERDIELD").ToString() == strCount)
            {
                strHtml = "";
            }
            else
            {
                strHtml = "<a href='FileUpload.aspx?downId=" + DataBinder.Eval(row.DataItem, "ID").ToString() + "&downNo=" + DataBinder.Eval(row.DataItem, "ORDERDIELD").ToString() + "&flowid=" + Request["flowid"] + "&DataType=" + Request["DataType"] + "'>下移</a>";
            }
        }
        return strHtml;
    }
}
