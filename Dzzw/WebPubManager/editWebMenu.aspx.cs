﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using Business.Menu;

public partial class WebPubManager_editWebMenu : System.Web.UI.Page
{
    private string strAction = "";
    private string strParentId = "";
    private string strId = "";
    private SysMenuOperation cMenu = new SysMenuOperation();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request["action"] != "")
            strAction = Request["action"];
        if (Request["parentId"] != "")
            strParentId = Request["parentId"];
        if (Request["id"] != "")
            strId = Request["id"];

        if (!IsPostBack)
        {
            DataTable dt = cMenu.GetMenu(strId);
            
            if (dt.Rows.Count > 0)
            {
                menuname.Text = dt.Rows[0]["MENU_TITLE"].ToString();
                menuurl.Text = dt.Rows[0]["MENU_URL"].ToString();
                orderfield.Text = dt.Rows[0]["ORDER_FIELD"].ToString();
                string strParent = dt.Rows[0]["PARENT_ID"].ToString();
                parentname.Text = cMenu.GetParentName(strParent);
                txtChUrl.Text = dt.Rows[0]["MENU_CHURL"].ToString();//添加数据维护地址 addby zhongjian 20091010
            }

            if (strAction != null && strParentId != null)
            {
                if (strAction != "" && strParentId != "")
                {
                    parentname.Text = cMenu.GetParentName(strParentId);
                }
            }

        }
    }

    protected void inputdata_Click(object sender, EventArgs e)
    {
        int order = 0;
        if (orderfield.Text == "")
        {
            order = 0;
        }
        else
        {
            order = Convert.ToInt32(orderfield.Text);
        }
        if (strId == "" || strId == null)
        {
            if (strAction != "" && strParentId != "")
            {
                if (menuname.Text != "")
                {
                    SysMenuOperation.InsertMenu(menuname.Text, menuurl.Text, strParentId, order,txtChUrl.Text);
                    Response.Redirect("WebMenuManager.aspx");
                }
            }
            else
            {
                if (menuname.Text != "")
                {
                    SysMenuOperation.InsertMenu(menuname.Text, menuurl.Text, "", order,txtChUrl.Text);
                    Response.Redirect("WebMenuManager.aspx");
                }
            }
        }
        else
        {
            if (menuname.Text != "")
            {
                SysMenuOperation.UpdateMenu(strId, menuname.Text, menuurl.Text, order,txtChUrl.Text);          
                Response.Redirect("WebMenuManager.aspx");
            }
        }
    }

    protected void editdata_Click(object sender, EventArgs e)
    {
        menuname.Text = "";
    }

    #region 返回菜单管理主界面
    protected void Button1_Click(object sender, EventArgs e)
    {
        Response.Redirect("WebMenuManager.aspx");
    }
    #endregion
}
