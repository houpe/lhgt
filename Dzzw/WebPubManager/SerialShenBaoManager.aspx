﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SerialShenBaoManager.aspx.cs"
    Inherits="WebPubManager_SerialShenBaoManager" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>业务申报表单详细信息</title>
    <link href="../css/page.css" rel="stylesheet" type="text/css" />
    <script src="../ScriptFile/jquery.js" type="text/javascript"></script>
</head>
<script type="text/javascript">
    //设置tabcontrol按钮的背景颜色,并判断是否进行过表单修改
    function SetBackColor(varCtrl, strUrl, strTabName) {
        currentUrl = strUrl;
        currentCtrlId = varCtrl.id;
        var varEnableSave = $("#saveform").attr("disabled");

        $("#frmChild").attr("src", strUrl);
        $("#tabMenu td[id ^= 'td']").attr("class", "tabcontrol_button03");
        $("#" + varCtrl.id).attr("class", "tabcontrol_button05");
    }

    //页面加载时执行
    $(document).ready(function () {
        $("#tabMenu td:first").click();
    });

    var striid = '<%= Request["iid"] %>';
    var strFlowName = '<%= Request["flowname"] %>';
    var strFlowType = '<%= Request["flowtype"] %>';
    var strUserName = '<%=Request["UserName"] %>';
    var strBusiness = '<%= Request["Business"] %>';


    function OpenReasonDiag(nType) {
        if (strUserName == "") {
            //strUserName == "无";
            alert("申请单位或个人申请人为空，请联系管理员核查");
            return;
        }

        //转向网页的地址;
        var url = "../office/OnlineReturn.aspx?iid=" + striid + "&flowname=" + strFlowName + "&flowtype=" + strFlowType + "&username=" + strUserName + "&childType=" + nType;

        var iWidth = 350;                          //弹出窗口的宽度;
        var iHeight = 300;                         //弹出窗口的高度;
        //获得窗口的垂直位置
        var iTop = (window.screen.availHeight - 30 - iHeight) / 2;
        //获得窗口的水平位置
        var iLeft = (window.screen.availWidth - 10 - iWidth) / 2;

        window.open(url, nType, 'height=' + iHeight + ',innerHeight=' + iHeight + ',width=' + iWidth + ',innerWidth=' + iWidth + ',top=' + iTop + ',left=' + iLeft + ',status=no,toolbar=no,menubar=no,location=no,resizable=yes,scrollbars=0,titlebar=no');
    }

    function goReturn() {
       //window.location.href = "../office/OnlineTask.aspx?Business=" + strBusiness;
       window.history.back(-1); 
    }
</script>
<body>
    <form id="form1" runat="server">
    <div style="text-align: center; margin: 0px auto; width: 100%">
        <table border="0" id="tabMenu" style="width: 100%; height: 30px; background-image: url(../App_Themes/SkinFile/Images/shenbao.gif);">
            <tr>
                <asp:Repeater ID="RepeaterWeb" runat="server">
                    <ItemTemplate>
                        <%#GetContent(Container)%>
                    </ItemTemplate>
                </asp:Repeater>
                <%--<td id="tdSccl" onclick='SetBackColor(this,"../WebPubManager/SubmitModule.aspx?flowname=<%=Server.UrlEncode(Request["flowname"])%>&iid=<%= Request["iid"] %>");'
                    class="tabcontrol_button03">
                    查看材料
                </td>--%>
                <td id="tdSccl" onclick='SetBackColor(this,"../WebPubManager/SbFileUploadNew.aspx?flowname=<%=Server.UrlEncode(Request["flowname"])%>&NewsId=<%= Request["iid"] %>&sbUserId=<%= Request["sbUserId"] %>");'
                    class="tabcontrol_button03">
                    查看材料
                </td>
                <td align="right">
                    <input type="button" id="btnBzbq" runat="server" class="NewButton" value="补正补齐" onclick="OpenReasonDiag(1)" />&nbsp;
                    <input type="button" id="btnBysl" runat="server" class="NewButton" value="不予受理" onclick="OpenReasonDiag(2)" />&nbsp;
                    <asp:Button ID="btnOk" runat="server" CssClass="input" Text="通过预审" OnClick="btnOk_Click"
                        OnClientClick=" return confirm('确定通过?')" />
                    &nbsp;
                    <input id="printform" class="NewButton" type="button" value="返回 " onclick="goReturn()" />
                </td>
            </tr>
        </table>
        <iframe name="frmChild" id="frmChild" width="100%" height="1100" frameborder="0" scrolling="no">
        </iframe>
    </div>
    </form>
</body>
</html>
