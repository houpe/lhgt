﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Yushen.aspx.cs" Inherits="WebPubManager_Yushen" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>预审开关设置</title>
    <link rel="stylesheet" type="text/css" href="../css/page.css" />
</head>
<body>
    <form id="Form1" name="queryForm" action="" runat="server">
    <div align="center">
        <controlDefine:MessageBox ID="msgAppear" runat="server" />
    </div>
    <div align="center">
        <table id="Table1" width="70%" border="0" class="box_middle_tab">
            <tr>
                <td style="height: 25px">
                    <div class="box_top_bar">
                        <div class="box_top_left">
                            <div class="box_top_right">
                                <div class="box_top_text">
                                    预审开关设置</div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="height: 25px">
                    <div class="box_middle4">
                        <table width="70%" border="0" cellpadding="0" cellspacing="0" class="box_middle_tab">
                            <tr>
                                <td style="text-align:right;">
                                    <asp:CheckBox ID="cbxYhsz" runat="server" Text="注册用户不需预审" />
                                </td>
                                <td style="color:Red">
                                   (勾选：不需要预审；不勾选：需要预审)
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <asp:Button ID="btnRefer" CssClass="input" runat="server" Text="提交" OnClick="btnRefer_Click" />
                                </td>
                            </tr>
                        </table>
                        <asp:HiddenField ID="HiddenFieldFlag" runat="server" />
                    </div>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
