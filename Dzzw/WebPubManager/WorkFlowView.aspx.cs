﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using System.Drawing;
using System.IO;
using Business.FlowOperation;

public partial class WebPubManager_WorkFlowView :PageBase
{
    protected WorkFlowPubSetting wfpsTemp = new WorkFlowPubSetting();
    protected string strWid = string.Empty;//流程WID

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
        }
    }

    /// <summary>
    /// 绑定显示办理流程图
    /// </summary>
    /// <!--addby zhongjian 20091209-->
    public string ShowWorkFlow()
    {
        string strHttp = string.Empty;
        strWid = wfpsTemp.GetWorkWid(Request["datatype"]);
        Common.ConfigHelper config = new Common.ConfigHelper(true);
        String strNewActivexServer = config.GetAppSettingsValue("ActivexDisplay");

        strHttp = string.Format("../../{0}?wid={1}&IsShowDialog=1", strNewActivexServer, strWid);

        return strHttp;
    }

}
