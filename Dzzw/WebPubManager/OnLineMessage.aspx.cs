﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using SbBusiness.PubInfo;

public partial class WebPubManager_OnLineMessage : PageBase
{
    private OnLineInfo OnLine = new OnLineInfo();
    private string strUserName = string.Empty;//用户名称
    private string strUserID = string.Empty;//用户ID
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindMessage();
        }
    }

    /// <summary>
    /// 绑定留言咨询信息
    /// </summary>
    /// <!--addby zhongjian 20091103-->
    protected void BindMessage()
    {
        if (Session["UserId"] != null)
        {
            strUserName = Session["UserName"].ToString();
            strUserID = Session["UserId"].ToString();
        }
        bool flag = OnLine.GetMessageAdmin(strUserID);//判断该用户是否为留言咨询管理员
        if (flag)
            hfAdminType.Value = "1";
        DataTable dtTemp = OnLine.GetMessageInfo(strUserID, "", "", "", "", "", flag);
        this.gvMessage.DataSource = dtTemp;
        this.gvMessage.RecordCount = dtTemp.Rows.Count;
        this.gvMessage.DataBind();
    }

    /// <summary>
    /// datagrid数据加载
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void recvgrid_OnLoadData(object sender, EventArgs e)
    {
        this.BindMessage();
    }

    /// <summary>
    /// 获取留言咨询管理员标识
    /// </summary>
    /// <returns></returns>
    public string GetAdminType()
    {
        return hfAdminType.Value;
    }

    protected void lbtnDelete_Click(object sender, EventArgs e)
    {
        LinkButton lbtnDelete = (LinkButton)sender;
        string strID=lbtnDelete.CommandArgument.ToString();
        OnLine.DeleteLYZX(strID);
        BindMessage();
    }
}
