﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="JsydCrxxList.aspx.cs" Inherits="WebPubManager_JsydCrxxList" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>建设用地出让信息列表</title>
    <link href="../css/page.css" rel="Stylesheet" type="text/css" />

    <script type="text/javascript">

        function CrxxPub(strIid) {
            window.open("CommonTabUpdate.aspx?iid=" + strIid + "&tabname=工业用地出让信息");
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div style="width: 98%; margin: 0px auto; text-align: center;">
            <table class="tab_top" width="98%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="18">
                        <img src="../images/tab_top_left.gif" width="17" height="35" alt="" />
                    </td>
                    <td width="89%" align="center">&nbsp;
                    </td>
                    <td>&nbsp;
                    </td>
                    <td align="right">
                        <img src="../images/tab_top_right.gif" width="5" height="35" alt="" />
                    </td>
                </tr>
            </table>
            <table style="width: 98%; border-top-width: 0px" class="tab">
                <tr>
                    <td>地块编号<asp:TextBox ID="txtDkbh" runat="server"></asp:TextBox>
                        地块坐落<asp:TextBox ID="txtDkzl" runat="server"></asp:TextBox>
                        <asp:Button ID="btnRefer" CssClass="input" runat="server" Text="查询" OnClick="btnRefer_Click" />
                        <asp:Button runat="server" ID="btnCrxxPub" Text="添加" OnClick="btnCrxxPub_Click" />
                    </td>
                </tr>
                <tr style="border-top-width: 0px">
                    <td style="border-top-width: 0px">
                        <controlDefine:CustomGridView ID="gridviewCar" runat="server" AllowPaging="True" Width="100%"
                            SkinID="List" HideTextLength="30" AutoGenerateColumns="False" ShowCmpAndCancel="False"
                            AllowOnMouseOverEvent="False" ShowItemNumber="True" AutoGenerateDeleteConfirm="false"
                            ShowHideColModel="None" OnRowCreated="gridviewCar_RowCreated" OnOnLoadData="recvgrid_OnLoadData">
                            <PagerSettings Visible="False" />
                            <Columns>
                                <asp:BoundField HeaderText="编号" DataField="iid" />
                                <asp:BoundField HeaderText="地块编号" DataField="DK_BH" />
                                <asp:BoundField HeaderText="地块坐落" DataField="DK_ZL" />
                                <asp:BoundField HeaderText="土地面积" DataField="DK_TDMJ" />
                                <asp:BoundField HeaderText="项目内容" DataField="DK_XMNR" />
                                <asp:BoundField HeaderText="土地用途" DataField="DK_TDYT" />
                                <asp:BoundField HeaderText="最新报价" DataField="zxbj" />
                                <asp:TemplateField HeaderText="编辑">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hyedit" runat="server" Text="编辑"></asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="删除">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hydel" runat="server" Text="删除"></asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </controlDefine:CustomGridView>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
