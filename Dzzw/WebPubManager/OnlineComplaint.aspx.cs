﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using SbBusiness.PubInfo;

public partial class WebPubManager_OnlineComplaint :PageBase
{
    private OnLineInfo OnLine = new OnLineInfo();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindComplaint();
        }
    }

    /// <summary>
    /// 绑定留言咨询信息
    /// </summary>
    /// <!--addby zhongjian 20091103-->
    protected void BindComplaint()
    {
        DataTable dtTemp = OnLine.GetComplaintInfo("");
        this.gvComplaint.DataSource = dtTemp;
        this.gvComplaint.RecordCount = dtTemp.Rows.Count;
        this.gvComplaint.DataBind();
    }

    /// <summary>
    /// datagrid数据加载
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void recvgrid_OnLoadData(object sender, EventArgs e)
    {
        this.BindComplaint();
    }

    protected void lbtnDelete_Click(object sender, EventArgs e)
    {
        LinkButton lbtnDelete = (LinkButton)sender;
        string strID = lbtnDelete.CommandArgument.ToString();
        OnLine.DeleteTsinfo(strID);
        BindComplaint();
    }
}
