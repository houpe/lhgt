﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Business;
using Business.Common;
using System.Configuration;
using SbBusiness.User;
using SbBusiness;

public partial class WebPubManager_UnitInfoUpdate : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //设置身份证类型的验证
            ddlZjlx.ValidatorName = cvZjhm.ClientID;
            Page.ClientScript.RegisterStartupScript(this.GetType(), "knh1", "<script>ddlChange('" + ddlZjlx.ddlClientID + "');ddlChange('UserType');</script>");
        }
    }

    /// <summary>
    /// 用户注册
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnRefer_Click(object sender, EventArgs e)
    {
        try
        {
            //检测用户是否存在过
            bool UserOldExist = SysUserHandle.CheckUserExist(txtUserId.Text);

            if (!UserOldExist)
            {
                string strUnitsID = string.Empty;//单位ID

                strUnitsID = SysUserHandle.InsertUnits(txtUnitsName.Text.Trim(), txtUnitsTel.Text.Trim(), txtUnitsAddress.Text.Trim(), txtUnitsFax.Text.Trim(), txtUnitsPostCode.Text.Trim(), txtLegal.Text.Trim(), txtOrganization.Text.Trim(), txtNature.Text.Trim());

                SysUserHandle.CreateUser(txtUserId.Text, txtUserName.Text, txtUserPwd.Text, int.Parse(ConfigurationManager.AppSettings["bsqxspSystemFlag"]), txtTel.Text, txtMobile.Text, txtAddress.Text, ddlZjlx.Value, txtZjhm.Text, txtCz.Text, txtEmail.Text, txtYZBM.Text,1,"", strUnitsID,"","","");

                Common.WindowAppear.WriteAlert(this.Page, "添加成功!");
            }
            else
            {
                labMsg.Text = "用户名已存在，请重新输入！";
            }
        }
        catch (Exception ex)
        {
            Common.WindowAppear.WriteAlert(this, ex.Message);
        }
    }
}