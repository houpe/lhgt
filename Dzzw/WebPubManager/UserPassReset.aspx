﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UserPassReset.aspx.cs" Inherits="WebPubManager_UserPassReset" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>用户密码查询</title>
    <link href="../css/page.css" rel="Stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div style="text-align: center">
        <table class="window_tab">
            <tr>
                <td>
                    用户ID：<asp:TextBox runat="server" ID="txtUserId"></asp:TextBox>
                    &nbsp; 用户名：<asp:TextBox runat="server" ID="txtUserName"></asp:TextBox>
                </td>
                <td style="text-align: left;">
                    <asp:Button ID="btnOk" CssClass="input" Text="查询" runat="server" OnClick="btnOk_Click" />
                </td>
            </tr>
        </table>
        <br />
        <table style="width: 100%; border-top-width: 0px" class="tab">
            <tr style="border-top-width: 0px">
                <td style="border-top-width: 0px">
                    <controlDefine:CustomGridView ID="CustomGridView1" runat="server" AllowPaging="false" AutoGenerateColumns="false"
                        Width="800" ShowHideColModel="None" DivFstClass-DivButtonClass="input" DivScdClass-DivButtonClass="input"
                        DivTrdClass-DivButtonClass="input" OnOnLoadData="CustomGridView1_OnLoadData">
                        <Columns>
                            <asp:BoundField DataField="userid" HeaderText="用户id" />
                            <asp:BoundField DataField="username" HeaderText="用户名" />
                            <asp:BoundField DataField="query_pass" HeaderText="密码 " />
                            <asp:TemplateField HeaderText="重置密码">
                                <itemtemplate>
                                   <a href='<%# DataBinder.Eval(Container.DataItem,"ID", "UserPassReset.aspx?action=reset&id={0}") %>' onclick="return confirm('确定要重置该用户的密码?')">重置密码</a>
                                </itemtemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerSettings Visible="False" />
                        <HeaderStyle CssClass="top_table" Height="30px" Width="50px" />
                        <RowStyle CssClass="inputtable" />
                    </controlDefine:CustomGridView>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
