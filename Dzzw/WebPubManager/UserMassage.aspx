﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UserMassage.aspx.cs" Inherits="WebPubManager_UserMassage" %>

<%@ Register Src="../UserControls/TextBoxWithValidator.ascx" TagName="TextBoxWithValidator"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControls/GeneralSelect.ascx" TagName="GeneralSelect" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>用户信息查看</title>
    <script type="text/javascript">
        //关闭子窗口刷新父窗口
        function windowClose1() {
            if (!window.opener.closed) {
                window.opener.location.href = window.opener.location.href;
                window.close();
            }
        } 

    </script>
    <link rel="stylesheet" type="text/css" href="../css/page.css" />
    <script type="text/javascript" src="../ScriptFile/Regex.js"></script>
</head>
<body>
    <form id="Form1" runat="server">
    <div style="width: 90%; margin: 0px auto; text-align: center;">
        <div class="box_top_bar">
            <div class="box_top_left">
                <div class="box_top_right">
                    <div class="box_top_text">
                        用户信息查看</div>
                </div>
            </div>
        </div>
        <div class="box_middle2">
            <table width="90%" border="0" cellpadding="0" cellspacing="0" class="box_middle_tab">
                <tr>
                    <td style="width: 244px; height: 26px;">
                        用户ID：
                    </td>
                    <td style="width: 510px; height: 26px;">
                        <asp:Label ID="lblUserID" runat="server" Text=" "></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="width: 244px; height: 26px;">
                        用户名：
                    </td>
                    <td style="width: 510px; height: 26px;">
                        <asp:Label ID="lblUserName" runat="server" Text=" "></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="width: 244px; height: 26px;">
                        用户类型：
                    </td>
                    <td style="width: 510px; height: 26px;">
                        <asp:Label ID="lblUserType" runat="server" Text=" "></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="width: 244px; height: 26px;">
                        证件类型：
                    </td>
                    <td style="width: 510px; height: 26px;">
                        <uc2:GeneralSelect ID="ddlZjlx" runat="server" ValidateEmpty="true" Name="证件类型" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 244px; height: 26px;">
                        证件号码：
                    </td>
                    <td style="width: 510px; height: 26px;">
                        <asp:Label ID="lblZjhm" runat="server" Text=" "></asp:Label>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 244px; height: 26px;">
                        联系电话：
                    </td>
                    <td style="width: 510px; height: 26px;">
                        <asp:Label ID="lblTel" runat="server" Text=" "></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="width: 244px; height: 26px;">
                        传 真：
                    </td>
                    <td style="width: 510px; height: 26px;">
                        <asp:Label ID="lblCz" runat="server" Text=" "></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="width: 244px; height: 26px;">
                        手机号码：
                    </td>
                    <td style="width: 510px; height: 26px;">
                        <asp:Label ID="lblMobile" runat="server" Text=" "></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="width: 244px; height: 26px;">
                        邮 件：
                    </td>
                    <td style="width: 510px; height: 26px;">
                        <asp:Label ID="lblEmail" runat="server" Text=" "></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="width: 244px; height: 26px;">
                        邮政编码：
                    </td>
                    <td style="width: 510px; height: 26px;">
                        <asp:Label ID="lblYZBM" runat="server" Text=" &nbsp; "></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="width: 244px; height: 26px;">
                        联系地址：
                    </td>
                    <td style="width: 510px; height: 26px;">
                        <asp:Label ID="lblAddress" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="width: 244px; height: 26px;">
                        所在单位：
                    </td>
                    <td style="width: 510px; height: 26px;">
                        <asp:Label ID="lblGrssdw" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input id="btnClose" type="button" value="关闭" class="NewButton" onclick="windowClose1();" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </form>
</body>
</html>
