﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Business.FlowOperation;

public partial class WebPubManager_WorkFlowPub :PageBase
{
    private WorkFlowPubSetting wpsGloab = new WorkFlowPubSetting();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Params["action"] == "delete")
        {
            wpsGloab.DeletePubWorkFlow(Request.Params["id"]);
        }

        if (!IsPostBack)
        {
            GridviewDatabind();
        }
    }

    #region 数据绑定
    /// <summary>
    /// Gridviews the databind.
    /// </summary>
    private void GridviewDatabind()
    {
        DataTable dt = wpsGloab.GetPubWorkFlow("");
        gvFlows.DataSource = dt;
        gvFlows.RecordCount = dt.Rows.Count;
        gvFlows.DataBind();
    }
    #endregion

    /// <summary>
    /// datagrid数据加载
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void recvgrid_OnLoadData(object sender, EventArgs e)
    {
        this.GridviewDatabind();
    }

    #region 行创建
    /// <summary>
    /// Handles the RowCreated event of the gvFlows control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
    protected void gridviewCar_RowCreated(object sender, GridViewRowEventArgs e)
    {
        switch (e.Row.RowType)
        {
            case DataControlRowType.DataRow:
                DataRowView drv = e.Row.DataItem as DataRowView;
                if (drv == null)
                {
                    break;
                }
                HyperLink hyedit = e.Row.FindControl("hyedit") as HyperLink;
                if (hyedit != null)
                {
                    //hyedit.NavigateUrl = "WorkFlowPubAdd.aspx?id=" + drv["id"];
                    //添加流程名称传值edit by zhongjian 20091010
                    hyedit.NavigateUrl = "WorkFlowManager.aspx?id=" + drv["id"] + "&flowname=" + drv["FLOWTYPE"];
                    
                }

                HyperLink hydel = e.Row.FindControl("hydel") as HyperLink;
                if (hydel != null)
                {
                    hydel.NavigateUrl = "WorkFlowPub.aspx?action=delete&id=" + drv["id"];
                }
                break;
        }
    }
    #endregion
    /// <summary>
    /// 绑定发布状态
    /// </summary>
    /// <param name="row"></param>
    /// <returns></returns>
    public string BindPubType(IDataItemContainer row)
    {
        string strReurn = string.Empty;
        strReurn = DataBinder.Eval(row.DataItem, "ispub").ToString();
        if (strReurn=="1")
        {
            strReurn = "已发布";
        }
        else
        {
            strReurn = "未发布";
        }
        return strReurn;
    }
}
