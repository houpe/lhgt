﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ProcessEdit.aspx.cs" Inherits="WebPubManager_ProcessEdit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>添加表单</title>
    <link rel="stylesheet" type="text/css" href="../css/page.css" />
    <script  type="text/javascript" src="../ScriptFile/Regex.js"></script>
</head>
<script type="text/javascript">
    function onRefer() {
        var eOtherName = document.getElementById("<%= txtOtherName.ClientID %>");
        if (eOtherName.value == "") {
            alert("表单别名不允许为空");
            eOtherName.focus();
            return false;
        }

        return true;
    }
</script>
<body>
    <form id="Form1" name="queryForm" action="" runat="server">
    <div align="center">
        <controlDefine:MessageBox ID="msgAppear" runat="server" />
    </div>
    <div align="center">
        <table id="Table1" width="70%" border="0" class="box_middle_tab">
            <tr>
                <td style="height: 25px">
                    <div class="box_top_bar">
                        <div class="box_top_left">
                            <div class="box_top_right">
                                <div class="box_top_text">
                                    添加表单</div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="box_middle4" style="height:200px">
                        <table width="70%" border="0" cellpadding="0" cellspacing="0" class="box_middle_tab">
                            <tr>
                                <td>
                                    流程名称：
                                </td>
                                <td style="text-align: left;">
                                    <asp:Label ID="LabelFlowName" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    表单别名：
                                </td>
                                <td style="text-align: left;">
                                    <asp:TextBox ID="txtOtherName" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    表单名称：
                                </td>
                                <td style="text-align: left; display: <%= GetDropDown() %>">
                                    <asp:DropDownList ID="ddlFlow" runat="server">
                                    </asp:DropDownList>
                                </td>
                                <td style="text-align: left; display: <%= GetLable() %>">
                                    <asp:Label ID="LabelBiaodan" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:Button ID="btnRefer" CssClass="input" runat="server" Text="提交" OnClick="btnRefer_Click"
                                        OnClientClick="return onRefer ();" />
                                    <asp:Button ID="btnReturn" CssClass="input" runat="server" Text="返回" OnClick="btnReturn_Click"
                                        CausesValidation="False" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
