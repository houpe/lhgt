﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using Business;
using System.Configuration;
using Business.FlowOperation;
using SbBusiness.Wsbs;

public partial class WebPubManager_CommonTabUpdate : PageBase
{
    DataTable dtTable = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
        }
    }

    /// <summary>
    /// 生成TAB按钮
    /// </summary>
    /// <param name="row"></param>
    /// <returns></returns>
    /// <!--addby zhongjian 20091229-->
    protected string GetContent()
    {
        string strHtml = string.Empty;

        string strServername = Request.Url.Authority;
        string strNewwebdisplay = ConfigurationManager.AppSettings["NewWebDisplay"];
        string strIId = Request["iid"];
        string strTitle = Request["tabname"];

        SerialInstance sioTemp = new SerialInstance();
        string strFid = sioTemp.GetFirstFidByWname(strTitle);

        //默认添加权限打开参数--针对webservice2
        if (strNewwebdisplay.IndexOf("?") == -1)
        {
            strNewwebdisplay += "?readonly=false";
        }
        else
        {
            strNewwebdisplay += "&readonly=false";
        }

        //其他表的保存
        strHtml = string.Format(" <div id='divSjb' title='{4}' icon='icon-search' closable='false' style='padding: 2px; overflow: hidden;'><iframe src=\"http://{0}{1}&fid={2}&userid=-1&iid={3}&step=-1\" width='100%' height='100%' frameboder='0'  id='frmSjb' name='frmSjb'></iframe></div>", strServername, strNewwebdisplay, strFid, strIId, strTitle);

        return strHtml;
    }
}
