﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UnitsEdit.aspx.cs" Inherits="WebPubManager_UnitsEdit" %>

<%@ Register Src="../UserControls/TextBoxWithValidator.ascx" TagName="TextBoxWithValidator"
    TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>单位编辑</title>
    <link rel="stylesheet" type="text/css" href="../css/page.css" />
    <script type="text/javascript" src="../ScriptFile/Regex.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div style=" width:80%;  margin: 0px auto;text-align:center;">
        <div class="box_top_bar">
            <div class="box_top_left">
                <div class="box_top_right">
                    <div class="box_top_text">
                        单位编辑</div>
                </div>
            </div>
        </div>
        <div class="box_middle" style="width: 100%; height: auto">
            <table width="70%" border="0" cellpadding="0" cellspacing="0" class="box_middle_tab">
                <tr>
                    <td>
                        单位名称：
                    </td>
                    <td style="text-align:left">
                        <asp:TextBox ID="txtUnitsname" runat="server" Width="290px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        单位地址：
                    </td>
                    <td style="text-align:left">
                        <asp:TextBox ID="txtUnitsaddress" runat="server" Width="290px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        单位性质：
                    </td>
                    <td style="text-align:left">
                        <asp:TextBox ID="txtNature" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        单位电话：
                    </td>
                    <td style="text-align:left">
                        <asp:TextBox ID="txtUnitsTel" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        邮政编码：
                    </td>
                    <td style="text-align:left">
                        <uc1:TextBoxWithValidator ID="txtPostcode" runat="server" Type="Postcode" ErrorMessage="请输入正确的邮政编码">
                        </uc1:TextBoxWithValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        单位传真：
                    </td>
                    <td style="text-align:left">
                        <asp:TextBox ID="txtFax" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        法人代表：
                    </td>
                    <td style="text-align:left">
                        <asp:TextBox ID="txtlegal" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        组织代码：
                    </td>
                    <td style="text-align:left">
                        <asp:TextBox ID="txtOrganization" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Label ID="labMsg" ForeColor="red" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Button ID="btnRefer" CssClass="input" runat="server" Text="提交" OnClick="btnRefer_Click"
                            OnClientClick="onRefer ();" />
                        <asp:Button ID="btnReturn" CssClass="input" runat="server" Text="返回" OnClick="btnReturn_Click"
                            CausesValidation="False" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </form>
</body>
</html>
