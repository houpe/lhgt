﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OnLineMsgList.aspx.cs" Inherits="WebPubManager_OnLineMsgList" %>

<%@ Register Src="../UserControls/GeneralSelect.ascx" TagName="GeneralSelect" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>留言回复权限配置</title>
    <link rel="stylesheet" type="text/css" href="../css/page.css" />

    <script language="javascript" type="text/javascript" src="../ScriptFile/Regex.js"></script>

</head>
<body>
    <form id="form1" runat="server">
    <div align="center">
        <table id="Table1" width="80%" border="0" class="box_middle_tab">
            <tr>
                <td style="height: 25px">
                    <div class="box_top_bar">
                        <div class="box_top_left">
                            <div class="box_top_right">
                                <div class="box_top_text">
                                    留言回复权限配置</div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="height: 25px">
                    <div class="box_middle4">
                        <table width="70%" border="0" cellpadding="0" cellspacing="0" class="box_middle_tab">
                            <tr>
                                <td>
                                    按流程事项配置：
                                </td>
                                <td style="text-align: left;">
                                    <asp:DropDownList ID="ddlControl" runat="server" OnSelectedIndexChanged="ddlControl_SelectedIndexChanged"
                                        AutoPostBack="True">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <a href="../SystemManager/membermis.aspx?groupid=<%=GetFlowID()%>&groupname=<%=GetFlowName()%>"
                                        target="_self">设置权限人员</a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    按审批部门配置：
                                </td>
                                <td style="text-align: left;">
                                    <%--<uc2:GeneralSelect ID="ddlFlowName" runat="server" IsParamsData="false" Name="投诉部门" />--%>
                                    <asp:DropDownList ID="ddlDep" runat="server" OnSelectedIndexChanged="ddldep_SelectedIndexChanged"
                                        AutoPostBack="True">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <a href="../SystemManager/membermis.aspx?groupid=<%=GetDepID()%>&groupname=<%=GetDepName()%>"
                                        target="_self">设置权限人员</a>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="text-align: left;">
                                    &nbsp;留言咨询回复管理员：
                                </td>
                                <td>
                                    <a href="../SystemManager/membermis.aspx?groupid=MessageAdministrator&groupname=留言咨询回复管理员"
                                        target="_self">设置权限人员</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
