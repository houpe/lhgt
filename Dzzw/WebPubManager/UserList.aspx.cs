﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using Business;
using Business.Common;
using Business.Admin;
using SbBusiness.User;

public partial class WebPubManager_UserList :PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string strId = Request.Params["id"];
        string strXml = string.Empty;

        if (Request.Params["action"] == "delete")
        {
            SysUserHandle.DelUserInfoById(strId);//删除用户
        }
        if (Request.Params["action"] == "reset")
        {
            //先更新密码
            SysUserHandle.UpdatePassword(strId);
        }

        GridviewDatabind();

    }

    #region 数据绑定
    /// <summary>
    /// Gridviews the databind.
    /// </summary>
    private void GridviewDatabind()
    {
        string strUnitsID = Request.Params["unitsid"];
        string strUnitsName = Request.Params["unitsname"];
        if (!string.IsNullOrEmpty(strUnitsName))
        {
            txtUnitsName.Text = strUnitsName;
        }
        if (!string.IsNullOrEmpty(strUnitsID))
        {
            btnReturn.Visible = true;
        }
        else
        {
            btnReturn.Visible = false;
        }
        lblAdd.Text = string.Format("<a href='UserEdit.aspx?unitsid={0}&unitsname={1}'>添加用户</a>", strUnitsID, strUnitsName);

        DataTable dt = SysUserHandle.GetUnitUser(txtUserID.Text.Trim(), txtUserName.Text.Trim(), txtUnitsName.Text.Trim(), strUnitsID, ddlGly.SelectedValue);
        gridviewCar.DataSource = dt;
        gridviewCar.RecordCount = dt.Rows.Count;
        gridviewCar.DataBind();
    }
    #endregion 

    /// <summary>
    /// datagrid数据加载
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void recvgrid_OnLoadData(object sender, EventArgs e)
    {
        this.GridviewDatabind();
    }

    #region 行创建
    /// <summary>
    /// 行创建
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridviewCar_RowCreated(object sender, GridViewRowEventArgs e)
    {
        switch (e.Row.RowType)
        {
            case DataControlRowType.DataRow:

                DataRowView drv = e.Row.DataItem as DataRowView;
                if (drv == null)
                {
                    break;
                }
                HyperLink hyedit = e.Row.FindControl("hyedit") as HyperLink;
                if (hyedit != null)
                {
                    hyedit.NavigateUrl = "UserEdit.aspx?id=" + drv["id"] + "&unitsid=" + drv["unitid"] + "&unitsname=" + drv["unitsname"];
                }

                HyperLink hydel = e.Row.FindControl("hydel") as HyperLink;
                if (hydel != null)
                {
                    hydel.Attributes.Add("OnClick", "return confirm('确定要删除该用户?')");
                    hydel.NavigateUrl = "UserList.aspx?action=delete&id=" + drv["id"];
                }

                LinkButton lbtnUnitsAdmin = e.Row.FindControl("lbtnUnitsAdmin") as LinkButton;
                if (lbtnUnitsAdmin != null)
                {
                    int nAdmin = Convert.IsDBNull(drv["unitsadmin"]) ? 0 : Convert.ToInt32(drv["unitsadmin"]);
                    if (!Convert.IsDBNull(drv["usertype"]))
                    {
                        lbtnUnitsAdmin.Visible = Convert.ToInt32(drv["usertype"]) == 1 ? true : false;
                    }
                    else
                    {
                        lbtnUnitsAdmin.Visible = false;
                    }

                    lbtnUnitsAdmin.Text = nAdmin == 1 ? "取消管理员" : "设为管理员";
                    lbtnUnitsAdmin.ToolTip = drv["unitsadmin"].ToString();
                    lbtnUnitsAdmin.CommandArgument = drv["ID"].ToString();

                    if (nAdmin == 1)
                    {
                        e.Row.BackColor = Color.Silver;
                    }
                }

                break;
        }
    }
    #endregion

    /// <summary>
    /// 设置或取消单位管理员
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbtnUnitsAdmin_Click(object sender, EventArgs e)
    {
        LinkButton lbtn = (LinkButton)sender;
        string strID = lbtn.CommandArgument;
        string strUnitsAdmin = lbtn.ToolTip;
        if (strUnitsAdmin == "1")
        {
            SysUserHandle.SaveUserType(strID, "0");
            lbtn.Text = "设为管理员";
        }
        else
        {
            SysUserHandle.SaveUserType(strID, "1");
            lbtn.Text = "取消管理员";
        }

    }
    protected void btnRefer_Click(object sender, EventArgs e)
    {
        GridviewDatabind();
    }

    protected void btnReturn_Click(object sender, EventArgs e)
    {
        Response.Redirect("UnitsList.aspx");
    }
}
