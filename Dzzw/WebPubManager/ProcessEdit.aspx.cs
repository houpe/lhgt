﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using Business;
using System.Configuration;
using SbBusiness.Wsbs;

public partial class WebPubManager_ProcessEdit : System.Web.UI.Page
{
    private XtProcess cProcess = new XtProcess();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LabelFlowName.Text = Request["datatype"];

            if (!string.IsNullOrEmpty(Request["id"]))
            {
                DataTable dtSource = cProcess.GetContentId(Request["id"]);

                if (dtSource.Rows.Count > 0)
                {
                    txtOtherName.Text = dtSource.Rows[0]["TABLEANOTHERNAME"].ToString();
                    LabelBiaodan.Text = dtSource.Rows[0]["TABLENAME"].ToString();
                }
            }

            ddlFlow.DataSource = cProcess.GetTablesInSerial(Request["flowid"]);
            ddlFlow.DataTextField = "bm";
            ddlFlow.DataValueField = "res_value";
            ddlFlow.DataBind();
        }
    }
       
    protected void btnRefer_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["id"]))
        {
            XtProcess.UpdateIsFlag(Request["id"], txtOtherName.Text, Request["flowid"]);
        }
        else
        {
            XtProcess.InsertProcess(LabelFlowName.Text, ddlFlow.SelectedItem.Text,
                txtOtherName.Text, ddlFlow.SelectedValue, Request["flowid"]);//添加时,增加流程ID edit by zhongjian 20091021
        }

        Response.Redirect("Process.aspx?flowid=" + Request["flowid"] + "&datatype=" + Request["datatype"]);
    }

    protected string GetDropDown()
    {
        if (!string.IsNullOrEmpty(Request["id"]))
            return "none";
        else
            return "";
    }

    protected string GetLable()
    {
        if (!string.IsNullOrEmpty(Request["id"]))
            return "";
        else
            return "none";
    }

    #region 返回菜单管理主界面
    protected void btnReturn_Click(object sender, EventArgs e)
    {
        Response.Redirect("Process.aspx?flowid=" + Request["flowid"] + "&datatype=" + Request["datatype"]);
    }
    #endregion
}
