﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UserEdit.aspx.cs" Inherits="WebPubManager_UserEdit" %>

<%@ Register Src="../UserControls/GeneralSelect.ascx" TagName="GeneralSelect" TagPrefix="uc2" %>
<%@ Register Src="../UserControls/TextBoxWithValidator.ascx" TagName="TextBoxWithValidator"
    TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>用户信息编辑</title>
    <link rel="stylesheet" type="text/css" href="../css/page.css" />
    <script language="javascript" type="text/javascript" src="../ScriptFile/Regex.js"></script>
</head>
<script type="text/javascript">
    function onRefer() {
        var eUserId = document.getElementById("<%=txtUserId.ChildControlClientId %>");
        var eUserPwd = document.getElementById("<%=txtUserPwd.ChildControlClientId %>");
        var eUserRePwd = document.getElementById("<%=txtUserRePwd.ClientID %>");
        var eHidden = document.getElementById("<%=HiddenFieldFlag.ClientID %>");
        var eZjhm = document.getElementById("<%=txtZjhm.ClientID %>");

        eHidden.value = "";
        if (eUserId.value == "") {
            alert("用户名不能为空");
            eHidden.value = "0";
        }
        if (eUserPwd.value == "") {
            alert("密码不能为空");
            eHidden.value = "1";
        }
        if (eUserPwd.value != eUserRePwd.value) {
            alert("两次密码输入不一致密码");
            eHidden.value = "2";
        }
        //        if (eZjhm.value == "")
        //        {
        //            alert ("证件号码不能为空");
        //            eHidden.value = "3";
        //        }  
    }
    function SetCusIsValid(objcon) {
        var sourceid = objcon.id.replace("ddlZjlx_ddlControl", "cvZjhm");
        var objsource = document.getElementById(sourceid);
        if (objcon.value == "军官证" || objcon.value == "其它") {
            objsource.enabled = false;
        }
        else {
            objsource.enabled = true;
        }
    }
</script>
<body>
    <form id="Form1" runat="server">
    <div align="center">
        <controlDefine:MessageBox ID="msgAppear" runat="server" />
    </div>
    <div style="width: 60%; margin: 0px auto; text-align: center;">
        <div class="box_top_bar">
            <div class="box_top_left">
                <div class="box_top_right">
                    <div class="box_top_text">
                        用户编辑</div>
                </div>
            </div>
        </div>
        <div class="box_middle2"  style="width: 100%; height: auto; text-align:center;">
            <table width="90%" border="0" cellpadding="0" cellspacing="0" class="box_middle_tab">
                <tr id="trUnitsName" runat="server">
                    <td>
                        单位名称：
                    </td>
                    <td style="text-align:left">
                        <asp:Label ID="lblUntisName" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        用户名：
                    </td>
                    <td style="text-align:left">
                        <uc1:TextBoxWithValidator ID="txtUserId" runat="server" Type="UserId" ErrorMessage="必须输入以字母开头的6-16个字母、数字或下划线">
                        </uc1:TextBoxWithValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        姓 名：
                    </td>
                    <td style="text-align:left">
                        <asp:TextBox ID="txtUserName" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr id="trUserPwd" runat="server">
                    <td>
                        登录密码：
                    </td>
                    <td style="text-align:left">
                        <uc1:TextBoxWithValidator ID="txtUserPwd" runat="server" Type="Passwords" TextMode="Password"
                            ErrorMessage="请输入6-20个字母、数字、下划线"></uc1:TextBoxWithValidator>
                    </td>
                </tr>
                <tr id="trUserRePwd" runat="server">
                    <td>
                        重复密码：
                    </td>
                    <td style="text-align:left">
                        <asp:TextBox ID="txtUserRePwd" runat="server" TextMode="Password"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        证件类型：
                    </td>
                    <td style="text-align:left">
                        <uc2:GeneralSelect ID="ddlZjlx" runat="server" ClientOnChangeScript="SetCusIsValid(this)"
                            ValidateEmpty="true" Name="证件类型" />
                    </td>
                </tr>
                <tr>
                    <td>
                        证件号码：
                    </td>
                    <td style="text-align:left">
                        <asp:TextBox ID="txtZjhm" runat="server"></asp:TextBox>
                        <asp:CustomValidator ID="cvZjhm" runat="server" ControlToValidate="txtZjhm" ErrorMessage="请输入正确的证件号码"
                            Display="Dynamic" ClientValidationFunction="checkNumberNew" ValidateEmptyText="true"></asp:CustomValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        联系电话：
                    </td>
                    <td style="text-align:left">
                        <asp:TextBox ID="txtTel" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        传 真：
                    </td>
                    <td style="text-align:left">
                        <asp:TextBox ID="txtCz" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        手机号码：
                    </td>
                    <td style="text-align:left">
                        <uc1:TextBoxWithValidator ID="txtMobile" runat="server" Type="Phone" ErrorMessage="请输入正确的手机号码">
                        </uc1:TextBoxWithValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        邮 件：
                    </td>
                    <td style="text-align:left">
                        <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        邮政编码：
                    </td>
                    <td style="text-align:left">
                        <uc1:TextBoxWithValidator ID="txtYZBM" runat="server" Type="Postcode" ErrorMessage="请输入正确的邮政编码">
                        </uc1:TextBoxWithValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        联系地址：
                    </td>
                    <td style="text-align:left">
                        <asp:TextBox ID="txtAddress" runat="server" Width="290px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Label ID="labMsg" ForeColor="red" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Button ID="btnRefer" CssClass="input" runat="server" Text="提交" OnClick="btnRefer_Click"
                            OnClientClick="onRefer ();" />
                        <asp:Button ID="btnReturn" CssClass="input" runat="server" Text="返回" OnClick="btnReturn_Click"
                            CausesValidation="False" />
                    </td>
                </tr>
            </table>
            <asp:HiddenField ID="HiddenFieldFlag" runat="server" />
            <asp:HiddenField ID="hfUsertype" runat="server" />
            <asp:HiddenField ID="hidUnitsID" runat="server" />
        </div>
    </div>
    </form>
</body>
</html>
