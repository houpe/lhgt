﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WorkFlowPubEdit.aspx.cs"
    Inherits="WebPubManager_WorkFlowPubEdit" %>

<%@ Register Src="../UserControls/GeneralSelect.ascx" TagName="GeneralSelect" TagPrefix="uc2" %>
<%@ Register Src="../UserControls/TextBoxWithValidator.ascx" TagName="TextBoxWithValidator"
    TagPrefix="uc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>流程发布信息管理</title>
    <link rel="stylesheet" type="text/css" href="../css/page.css" />

    <script type="text/javascript" src="../ScriptFile/Regex.js"></script>

    <script type="text/jscript">
    function SetCheck(ChValue)
    {
        if(ChValue)
        {
            return true;
        }
        else
        {        
            return false;
        }
    }
    function GetConfirm()
    {
        if(confirm('系统提示：此流程已发布，确定要覆盖吗？')==true)
        {            
            //替换新版本方法在后台btnRePub_Click事件当中实现
            //当用户选择确定时,调用后台btnRePub_Click事件 addby zhongjian 20091020
            document.getElementById("btnRePub").click();//btnReturn
        }
    }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField ID="hfPub" runat="server" />
    <div align="right">
        <asp:Button ID="btnSave" CssClass="input" runat="server" Text="保存" OnClick="btnSave_Click" />&nbsp;
        <asp:Button ID="btnPub" CssClass="input" runat="server" Text="发布" OnClick="btnPub_Click" />&nbsp;
        <asp:Button ID="btnCancelPub" CssClass="input" runat="server" Visible="false" Text="取消发布" OnClick="btnCancelPub_Click" />&nbsp;
        <asp:Button ID="btnDelete" CssClass="input" runat="server" Text="删除" OnClick="btnDelete_Click"
            OnClientClick="return confirm('是否确认删除此流程？')" />&nbsp;
        <asp:Button ID="btnReturn" CssClass="input" runat="server" Text="返回" OnClick="btnReturn_Click"
            CausesValidation="False" />
        <asp:HiddenField ID="hfFlag" runat="server" Value="0" />
       <%-- <asp:Button ID="btnRePub" CssClass="input" runat="server" Text="重新发布" CausesValidation="False"
            OnClick="btnRePub_Click" Width="0" />--%>
    </div>
    <span>
        <asp:Label ID="lblMessage" Visible="false" runat="server" ForeColor="Red" Text="权限设定【注：打勾则代表允许匿名查看】"></asp:Label></span>
    <br />
    <table width="100%" border="0" cellpadding="1" cellspacing="0" class="tab">
        <tr>
            <td style="width: 50%" valign="top">
                <div class="new_box02">
                    <div class="new_bar02">
                        业务信息</div>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                事项名称：
                            </td>
                            <td style="text-align: left;">
                                <uc1:TextBoxWithValidator ID="txtFlowType" runat="server" Type="Required" Width="250px">
                                </uc1:TextBoxWithValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                内部审批流程：
                            </td>
                            <td style="text-align: left;">
                                <uc2:GeneralSelect ID="ddlFlowName" runat="server" IsParamsData="false" Name="所有流程"
                                    Width="250px" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                流程排序：
                            </td>
                            <td style="text-align: left;">
                                <uc1:TextBoxWithValidator ID="txtFlowNum" runat="server" Width="50px"></uc1:TextBoxWithValidator>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="new_box02">
                    <div class="new_bar02">
                        基本状态</div>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                发布状态：
                            </td>
                            <td style="text-align: left;">
                                <asp:Label ID="lblisPub" runat="server" Text="" ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                发布时间：
                            </td>
                            <td style="text-align: left;">
                                <asp:Label ID="lblPubTime" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                创建时间：
                            </td>
                            <td style="text-align: left;">
                                <asp:Label ID="lblCreateTime" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                修改时间：
                            </td>
                            <td style="text-align: left;">
                                <asp:Label ID="lblEditTime" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
            <td valign="top" style="width: 50%;">
                <div class="new_box02">
                    <div class="new_bar02">
                        权限设定【注：打勾则代表允许匿名查看】
                    </div>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <asp:Repeater ID="RepeaterWeb" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <%# DataBinder.Eval(Container.DataItem, "menu_title")%>：
                                    </td>
                                    <td style='text-align: left;'>
                                        <asp:CheckBox ID="cbWorkFlow" runat="server" Text="匿名查看" ToolTip='<%# DataBinder.Eval(Container.DataItem, "menu_title")%>'
                                            Checked='<%# SetCheck(Container,"menu_type")%>' />

                                            <asp:CheckBox ID="cbAppear" runat="server" Text="显示" ToolTip='<%# DataBinder.Eval(Container.DataItem, "menu_title")%>' Checked='<%# SetCheck(Container,"MENU_APPEAR")%>' />
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                        <tr>
                            <td>
                                材料校验：
                            </td>
                            <td style="text-align: left;">
                                <asp:CheckBox ID="cbCheck" runat="server" Text="上传材料是否校验" />
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td valign="top" style="width: 50%; display: none">
                <div class="new_box02">
                    <div class="new_bar02">
                        接口地址设置
                    </div>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="width: 80px">
                                启用接口：
                            </td>
                            <td style="text-align: left;">
                                <asp:CheckBox ID="cbInterfaceType" runat="server" Text="" />
                                <span style="color: Red">(勾选：启用；不勾选：不启用)</span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                接口地址：
                            </td>
                            <td style="text-align: left;">
                                <asp:TextBox ID="txtInterfaceUrl" runat="server" Width="98%"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>

    <script src="../ScriptFile/AutoHeightAndWidthDouble.js" type="text/javascript"></script>
    </form>
</body>
</html>
