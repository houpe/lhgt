﻿using Business.Admin;
using Common;
using SbBusiness.User;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class WebPubManager_AddSysName : System.Web.UI.Page
{
    private SysUserTypeHandle userOperation = new SysUserTypeHandle();
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void btnCommit_Click(object sender, EventArgs e)
    {
        if (userOperation.CheckSysName(txtUserTypeName.Text))
        {
            WindowAppear.WriteAlert(Page, "系统名已存在");
            return;
        }

        if (!string.IsNullOrEmpty(txtUserTypeName.Text))
        {
            if (userOperation.InsertSysName(txtUserTypeName.Text))
            {
                Response.Write("<script language='javascript'>alert('插入成功');window.opener.location.href=window.opener.location.href;window.opener=null;window.close();</script>");
            }
            else
            {
                Response.Write("<script language='javascript'>alert('插入失败')</script>");
            }
        }

    }
}
