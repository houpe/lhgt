﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UnitInfoUpdate.aspx.cs" Inherits="WebPubManager_UnitInfoUpdate" %>

<%@ Register Src="../UserControls/GeneralSelect.ascx" TagName="GeneralSelect" TagPrefix="uc2" %>
<%@ Register Src="../UserControls/TextBoxWithValidator.ascx" TagName="TextBoxWithValidator"
    TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>单位信息维护</title>
    <link href="../css/page.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../ScriptFile/Regex.js"></script>
</head>
<body>
    <script type="text/javascript">
        function onRefer() {
            var eUserId = document.getElementById("<%=txtUserId.ChildControlClientId %>");
            var eUserPwd = document.getElementById("<%=txtUserPwd.ChildControlClientId %>");
            var eUserRePwd = document.getElementById("<%=txtUserRePwd.ClientID %>");
            var eZjhm = document.getElementById("<%=txtZjhm.ClientID %>");
            var eUnitsName = document.getElementById("<%=txtUnitsName.ClientID %>");

            eHidden.value = "";
            if (eUserId.value == "") {
                alert("用户名不能为空");
                eHidden.value = "0";
            }
            if (eUserPwd.value == "") {
                alert("密码不能为空");
                eHidden.value = "1";
            }
            if (eUserPwd.value != eUserRePwd.value) {
                alert("两次密码输入不一致密码");
                eHidden.value = "2";
            }
            if (eZjhm.value == "") {
                alert("证件号码不能为空");
                eHidden.value = "3";
            }
            if (eUnitsName.value == "") {
                alert("单位名称不能为空");
                eHidden.value = "4";
            }
        }

        function SetCusIsValid(objcon) {
            var sourceid = objcon.id.replace("ddlZjlx_ddlControl", "cvZjhm");
            var objsource = document.getElementById(sourceid);
            if (objcon.value == "军官证" || objcon.value == "其它") {
                objsource.enabled = false;
            }
            else {
                objsource.enabled = true;
            }
        }

    </script>
    <form id="form1" runat="server">
    <div style="width: 100%; margin: 0px auto; text-align: center;">
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr id="trUnits" style="display: block">
                <td>
                    <fieldset style="width: 100%; text-align: center">
                        <legend style="font-weight: bold; font-size: 12;">单位信息</legend>
                        <table class="inputtable" style="width: 100%;" cellpadding="8" cellspacing="0">
                            <tr>
                                <td>
                                    单位名称
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtUnitsName"  runat="server"></asp:TextBox>
                                </td>
                                <td>
                                    组织代码
                                </td>
                                <td align="left" colspan="3">
                                    <asp:TextBox ID="txtOrganization"  runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    法人代表或负责人
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtLegal"  runat="server"></asp:TextBox>
                                </td>
                                <td>
                                    单位电话
                                </td>
                                <td align="left" colspan="3">
                                    <asp:TextBox ID="txtUnitsTel"  runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    单位类型
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtNature"  runat="server"></asp:TextBox>
                                </td>
                                <td>
                                    传&nbsp;&nbsp;&nbsp;&nbsp;真
                                </td>
                                <td align="left" colspan="3">
                                    <asp:TextBox ID="txtUnitsFax"  runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    单位地址
                                </td>
                                <td align="left" colspan="3">
                                    <asp:TextBox ID="txtUnitsAddress" Width="95%" runat="server"></asp:TextBox>
                                </td>
                                <td >
                                    邮编
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtUnitsPostCode" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <td>
                    <fieldset style="width: 100%; text-align: center">
                        <legend style="font-weight: bold; font-size: 12;">
                            <asp:Label ID="lblFiel" runat="server" Text="经办人信息"></asp:Label></legend>
                        <table class="inputtable" style="width: 100%;" cellpadding="8" cellspacing="0">
                            <tr>
                                <td>
                                    用 户 名
                                </td>
                                <td align="left">
                                    <uc1:TextBoxWithValidator ID="txtUserId"  runat="server" Type="UserId">
                                    </uc1:TextBoxWithValidator>
                                </td>
                                <td>
                                    姓&nbsp;&nbsp;&nbsp;&nbsp;名
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtUserName" Width="180px" runat="server"></asp:TextBox>
                                </td>
                                <td style="width: 50px">
                                    性&nbsp;&nbsp;&nbsp;&nbsp;别
                                </td>
                                <td align="left">
                                    <uc2:GeneralSelect ID="ddlSex" Width="50px" runat="server" ValidateEmpty="false"
                                        Name="性别" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    登录密码
                                </td>
                                <td align="left">
                                    <uc1:TextBoxWithValidator ID="txtUserPwd"  runat="server" Type="Passwords"
                                        TextMode="Password"></uc1:TextBoxWithValidator>
                                </td>
                                <td>
                                    重复密码
                                </td>
                                <td align="left" colspan="3">
                                    <asp:TextBox ID="txtUserRePwd"  runat="server" TextMode="Password"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    证件类别
                                </td>
                                <td align="left">
                                    <uc2:GeneralSelect ID="ddlZjlx"  runat="server" ClientOnChangeScript="SetCusIsValid(this)"
                                        ValidateEmpty="true" Name="证件类型" />
                                </td>
                                <td>
                                    证件号码
                                </td>
                                <td align="left" colspan="3">
                                    <asp:TextBox ID="txtZjhm"  runat="server"></asp:TextBox>
                                    <asp:CustomValidator ID="cvZjhm" runat="server" ControlToValidate="txtZjhm" ErrorMessage="请输入正确的证件号码"
                                        Display="Dynamic" ClientValidationFunction="checkNumberNew" ValidateEmptyText="true"></asp:CustomValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    联系电话
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtTel"  runat="server"></asp:TextBox>
                                </td>
                                <td>
                                    传&nbsp;&nbsp;&nbsp;&nbsp;真
                                </td>
                                <td align="left" colspan="3">
                                    <asp:TextBox ID="txtCz"  runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    手机号码
                                </td>
                                <td align="left">
                                    <uc1:TextBoxWithValidator ID="txtMobile"  runat="server" Type="Phone">
                                    </uc1:TextBoxWithValidator>
                                </td>
                                <td>
                                    电子邮件
                                </td>
                                <td align="left" colspan="3">
                                    <asp:TextBox ID="txtEmail"  runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr id="trAddress" style="display: none">
                                <td>
                                    联系地址
                                </td>
                                <td align="left" colspan="3">
                                    <asp:TextBox ID="txtAddress" Width="100%" runat="server"></asp:TextBox>
                                </td>
                                <td style="">
                                    邮编
                                </td>
                                <td align="left">
                                    <uc1:TextBoxWithValidator ID="txtYZBM" Width="50px" runat="server" Type="Postcode">
                                    </uc1:TextBoxWithValidator>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="labMsg" ForeColor="red" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Button ID="btnRefer" SkinID="LightGreen" runat="server" Text="提交" OnClick="btnRefer_Click"
                        OnClientClick="onRefer ();" />
                    <input type="button" value="关闭" class="NewButton" onclick="javascript:window.close()" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
