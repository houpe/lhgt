﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Process.aspx.cs" Inherits="WebPubManager_Process" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>添加表单</title>
    <link href="../css/page.css" rel="Stylesheet" type="text/css" />
    <script type="text/javascript">
        //打开下载页面方法 addby zhongjian 20090923
        function DownLoad(strID) {
            window.location.href = "AppearOtherResouce.aspx?type=Material&id=" + strID;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div align="right">
            <asp:Button ID="btnAddForms" CssClass="input" runat="server" Text="添加表单" OnClick="btnAddForms_Click" />&nbsp;
            <asp:Button ID="btnAddMaterial" CssClass="input" runat="server" Text="添加材料" OnClick="btnAddMaterial_Click" />&nbsp;
            <asp:Button ID="btnReturn" CssClass="input" runat="server" Text="返  回" OnClick="btnReturn_Click"
                CausesValidation="False" />
        </div>
        <br />
        <table width="100%" border="0" cellpadding="1" cellspacing="0" class="tab">
            <tr>
                <td valign="top">
                    <div class="new_bar02">
                        表单管理
                    </div>
                    <controlDefine:CustomGridView ID="gridviewCar" runat="server" AllowPaging="True"
                        SkinID="List" Width="100%" HideTextLength="30" AutoGenerateColumns="False" ShowCmpAndCancel="False"
                        AllowOnMouseOverEvent="False" ShowItemNumber="true" AutoGenerateDeleteConfirm="True"
                        ShowHideColModel="None" OnRowCreated="gridviewCar_RowCreated" OnOnLoadData="recvgrid_OnLoadData">
                        <PagerSettings Visible="False" />
                        <Columns>
                            <asp:BoundField HeaderText="流程名" DataField="FLOWNAME" />
                            <asp:BoundField HeaderText="表单名" DataField="TABLENAME" />
                            <asp:BoundField HeaderText="表单别名" DataField="TABLEANOTHERNAME" />
                            <asp:TemplateField HeaderText="操作">
                                <ItemTemplate>
                                    <%#GetFromUp(Container)%>
                                    <%#GetFromDown(Container)%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="数据配置">
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyData" runat="server" Text="数据配置"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="编辑">
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyedit" runat="server" Text="编辑"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="删除">
                                <ItemTemplate>
                                    <a href='Process.aspx?action=deletefrom&id=<%# DataBinder.Eval(Container.DataItem,"id") %>&datatype=<%# DataBinder.Eval(Container.DataItem,"FLOWNAME") %>&flowid=<%= GetFlow() %>'>
                                        删除</a>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </controlDefine:CustomGridView>
                </td>
            </tr>
            <tr>
                <td valign="top">
                    <div class="new_bar02">
                        材料管理
                    </div>
                    <controlDefine:CustomGridView ID="gridviewMaterial" runat="server" AllowPaging="false"
                        SkinID="List" AutoGenerateColumns="false" Width="100%" HideTextLength="30" ShowCmpAndCancel="False"
                        AllowOnMouseOverEvent="False" ShowItemNumber="true" ShowHideColModel="None"
                        OnRowCreated="gridviewMaterial_RowCreated" OnOnLoadData="gridviewMaterial_OnLoadData">
                        <PagerSettings Visible="False" />
                        <Columns>
                            <asp:BoundField HeaderText="附件名称" DataField="modulename" />
                            <asp:TemplateField HeaderText="附件模板">
                                <ItemTemplate>
                                    <a href="#" onclick="DownLoad('<%# DataBinder.Eval(Container.DataItem, "ID")%>')">
                                        <%#GetFileName(Container)%></a>
                                    <!--为防止弹出空白页面-->
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="操作">
                                <ItemTemplate>
                                    <%#GetUp(Container)%>
                                    <%#GetDown(Container)%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="编辑附件">
                                <ItemTemplate>
                                    <a href='CailiaoFileUpload.aspx?id=<%# DataBinder.Eval(Container.DataItem,"id") %>&datatype=<%# DataBinder.Eval(Container.DataItem,"type") %>'
                                        target="_blank">上传</a>&nbsp;&nbsp;&nbsp;
                                    <asp:HyperLink ID="hyEmpty" runat="server" Text="删除"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="修改材料名称">
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyedit" runat="server" Text="编辑"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="删除材料">
                                <ItemTemplate>
                                    <a href='Process.aspx?action=deleteMaterial&id=<%# DataBinder.Eval(Container.DataItem,"id") %>&datatype=<%# DataBinder.Eval(Container.DataItem,"type") %>&flowid=<%= GetFlow() %>'
                                        onclick="return confirm('确定要删除该材料?')">删除</a>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </controlDefine:CustomGridView>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
