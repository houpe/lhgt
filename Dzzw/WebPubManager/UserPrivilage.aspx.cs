﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Business.Admin;
using SbBusiness.User;

public partial class WebPubManager_UserPrivilage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Params["action"] == "delete")
        {
            SysUserTypeHandle sutoTemp = new SysUserTypeHandle();
            sutoTemp.DeleteMenuUserType(Request.Params["id"]);
        }
        if (!IsPostBack)
        {
            BindData();
        }
    }

    private void BindData()
    {
        SysUserTypeHandle suhTemp = new SysUserTypeHandle();
        DataTable dtTable = suhTemp.GetUserTypes();

        CustomGridView1.DataSource = dtTable;
        CustomGridView1.HideColName = "ID";
        CustomGridView1.RecordCount = dtTable.Rows.Count;
        CustomGridView1.DataBind();
    }

    protected void CustomGridView1_OnLoadData(object sender, EventArgs e)
    {
        BindData();
    }

    protected void CustomGridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        switch (e.Row.RowType)
        {
            case DataControlRowType.DataRow:
                DataRowView dtrv = e.Row.DataItem as DataRowView;
                if (dtrv == null)
                {
                    break;
                }
                break;
        }
    }
}
