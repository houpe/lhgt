﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using Business.Common;
using Business.Admin;

public partial class Common_SbFileUploadNew : System.Web.UI.Page
{
    UploadFileClass ufcGloab = new UploadFileClass();

    //流程ID 
    public string iid
    {
        get
        {
            return Request["NewsId"];
        }
    }
    //流程类别
    public string flowname
    {
        get
        {
            return Request["flowname"];
        }
    }
    //申报用户id
    public string SbUserId
    {
        get
        {
            return Request["sbUserId"];
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //判断附件权限
            GenerateAllFuJian();
        }
    }

    /// <summary>
    /// 生成附件
    /// </summary>
    public void GenerateAllFuJian()
    {
        StringBuilder html = new StringBuilder();
        html.Append("<table class='window_tab_list' style='color: dodgerblue; border-color: #4fbff9; width:100%;border-collapse:collapse;' align='center' border=\"1\"><tr class=\"title\" align=\"center\">");
        html.Append("<th style='height:25px;width:50px'>序号</th><th width=\"300px\">类别</th><th>附件名称</th><th width='80'>格式</th></tr>");
        DataTable dt = DictOperation.GetSysParams("发布类别", string.Format("type='{0}'", flowname));

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            //绘制种类行
            if (dt.Rows[i]["keyvalue"].ToString() == System.Configuration.ConfigurationManager.AppSettings["dtshAttType"])
            {
                html.Append("<tr style='background-color:Silver;font-weight:bold; height:25px'>");
            }
            else
            {
                html.Append("<tr style='background-color:#ECF2FB;font-weight:bold; height:25px'>");
            }

            html.Append(string.Format("<td>{1}</td><td>{0}</td><td></td><td></td>", dt.Rows[i]["keyvalue"],i+1));

            html.Append("</tr>");

            //获取上传的附件
            string strWhere = string.Format(" (地信申报信息ID='{0}' or (IS_TEMPLET=1 and USERID='{2}')) and MODULE_ID='{1}'", iid, dt.Rows[i]["keycode"], SbUserId);
            DataTable dtchild = ufcGloab.GetAttachmentResourceInfo(strWhere);

            //绘制附件所对应的行            
            for (int j = 0; j < dtchild.Rows.Count; j++)
            {
                string strRowSpan = "";

                if (j==0)//第一行设置占用行数，其他行少<td></td>
                {
                    strRowSpan = string.Format("<td rowspan='{0}'></td>", dtchild.Rows.Count);
                }

                string strBackColor = "";
                if (dtchild.Rows[j]["IS_TEMPLET"].ToString() == "1")
                {
                    strBackColor = "color:red";
                }

                html.Append(string.Format("<tr style='height:25px;background-color:#fff;'>{1}<td style='{2}'>({0})</td>", j + 1, strRowSpan, strBackColor));
                html.Append(string.Format("<td style='{2}'><a href='AppearOtherResouce.aspx?type=AttAchment&id={1}' style='{2}'>{0}</a></td>", dtchild.Rows[j]["filename"], dtchild.Rows[j]["id"], strBackColor));
                html.Append(string.Format("<td  style='{1}'>{0}</td>", dtchild.Rows[j]["fileTYPE"],strBackColor));

                html.Append("</tr>");
            }

        }
        html.Append("</table>");

        this.context.InnerHtml = html.ToString();
    }
}