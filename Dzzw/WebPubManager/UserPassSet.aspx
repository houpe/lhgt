﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UserPassSet.aspx.cs" Inherits="WebPubManager_UserPassSet" %>

<%@ Register Src="../UserControls/TextBoxWithValidator.ascx" TagName="TextBoxWithValidator"
    TagPrefix="uc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>用户密码重置</title>
    <link rel="stylesheet" type="text/css" href="../css/page.css" />
    <script language="javascript" type="text/javascript" src="../ScriptFile/Regex.js"></script>
    <base target="_self" />
</head>
<script language="javascript" type="text/javascript">
    //在修改密码时判断两密码输入密码是否一致 add by zhongjian 20090927
    function onRefer() {
        //添加错误处理,因为在showModalDialog对话框模式弹出时,密码输入框隐藏,对象可能找不到 edit by zhongjian 20091019
        try {
            var eUserPwd = document.getElementById("<%=txtUserPwd.ChildControlClientId %>");
            var eUserRePwd = document.getElementById("<%=txtUserRePwd.ClientID %>");
            var eHidden = document.getElementById("<%=hfFlag.ClientID %>");
            eHidden.value = "";
            if (eUserPwd.value == "") {
                alert("密码不能为空");
                eHidden.value = "1";
            }
            if (eUserPwd.value != eUserRePwd.value) {
                alert("两次密码输入不一致密码");
                eHidden.value = "1";
            }
        }
        catch (Error) {

        }
        //refreshParent();
    }

    //关闭子窗口刷新父窗口
    function windowClose() {
        window.close();
    }
    //add by zhongjian 20090927
    function refreshParent() {
        window.opener.location.href = window.opener.location.href;
        if (window.opener.progressWindow) {
            window.opener.progressWindow.close();
        }
        window.close();
    }    
</script>
<body>
    <form id="form1" runat="server">
    &nbsp;<div style="text-align: center">
        <br />
        <table style="width: 100%" class="tab">
            <tr>
                <th colspan="2" style="height: 27px">
                    用户密码重置
                </th>
            </tr>
            <tr>
                <td style="width: 20%; height: 30px">
                    选择密码重置类型
                </td>
                <td>
                    <asp:RadioButtonList ID="rblUserPwdType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="rblUserPwdType_SelectedIndexChanged">
                        <asp:ListItem Value="0" Selected="True">同用户名</asp:ListItem>
                        <asp:ListItem Value="1">随机生成</asp:ListItem>
                        <asp:ListItem Value="2">指定密码</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr id="trUserPwd" runat="server" visible="false">
                <td style="width: 244px; height: 26px;">
                    输入密码
                </td>
                <td style="width: 510px; height: 26px;">
                    <uc1:TextBoxWithValidator ID="txtUserPwd" runat="server" Type="Passwords" TextMode="Password"
                        ErrorMessage="请输入6-20个字母、数字、下划线"></uc1:TextBoxWithValidator>
                </td>
            </tr>
            <tr id="trUserRePwd" runat="server" visible="false">
                <td style="width: 244px; height: 26px;">
                    确认密码
                </td>
                <td style="width: 510px; height: 26px;">
                    <asp:TextBox ID="txtUserRePwd" runat="server" TextMode="Password"></asp:TextBox>
                </td>
            </tr>
        </table>
        <br />
        <table style="width: 100%;" class="tab">
            <tr>
                <td colspan="4">
                    <asp:Button ID="btnOK" runat="server" Text="确定" OnClientClick="onRefer();" CssClass="input"
                        OnClick="btnOK_Click" />
                    &nbsp;
                    <input id="btnClose" type="button" value="取消" class="NewButton" onclick="windowClose();" />
                </td>
            </tr>
        </table>
        <asp:HiddenField ID="hfFlag" runat="server" />
    </div>
    </form>
</body>
</html>
