﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using Business;

using Business.Common;

public partial class WebPubManager_CailiaoFileUpload : System.Web.UI.Page
{
    UploadFileClass ufcGloab = new UploadFileClass();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SetFileUpdatePerproty();
        }
    }

    /// <summary>
    /// 设置文件上传属性
    /// </summary>
    private void SetFileUpdatePerproty()
    {
        //设置文件上传控件
        FileUpdate1.KeyId = "id";
        if (!string.IsNullOrEmpty(Request["id"]))
        {
            FileUpdate1.KeyIdValue = Request["id"];
        }
        FileUpdate1.FileStoreFieldName = "FILEDATA";
        FileUpdate1.FileTypeFieldName = "FILETYPE";
        FileUpdate1.TableName = "XT_SERIAL_MODULE";

        FileUpdate1.StoreKeyFieldName = "TYPE";
        if (!string.IsNullOrEmpty(Request["datatype"]))
        {
            FileUpdate1.StoreKeyValue = Request["datatype"];
        }
        FileUpdate1.StoreResourceFieldName = "FILENAME";
        FileUpdate1.StoreResourceType = "update";
        FileUpdate1.StoreResourceNameControlName = txtFileName.ID;
    }

    /// <summary>
    /// 上传后触发事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void FileUpdate1_AfterUploadedEvent(object sender, EventArgs e)
    {
        ClientScript.RegisterStartupScript(this.GetType(), "scr", "<script>window.opener.location.href=window.opener.location.href;window.close();</script>");
    }
}
