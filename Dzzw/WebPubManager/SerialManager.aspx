﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SerialManager.aspx.cs" Inherits="WebPubManager_SerialManager" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>业务权限申请详细信息</title>
    <link href="../css/page.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
   //关闭子窗口刷新父窗口
    function windowClose1()
    { 
        if (!window.opener.closed) 
	        {	           
	            window.opener.location.href = window.opener.location.href;
	            window.close();
	        }
    } 

    </script>

</head>
<body>
    <form id="form1" runat="server">
   <div style="text-align: center; margin: 0px auto;" class="page_box">

        <table style="width: 100%" class="tab">
            <tr>
                <th colspan="4" style="height: 27px">
                    业务权限申请详细信息
                </th>
            </tr>
            <tr>
                <td style="width: 20%; height: 30px">
                    用户ID
                </td>
                <td style="width: 25%">
                    <asp:Label ID="lblUserID" runat="server" Text=""></asp:Label>
                </td>
                <td style="width: 25%">
                    申请时间
                </td>
                <td style="width: 30%">
                    <asp:Label ID="lblSerialTime" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 20%; height: 30px">
                    用户名称
                </td>
                <td style="width: 25%">
                    <asp:Label ID="lblUserName" runat="server" Text=""></asp:Label>
                </td>
                <td style="width: 25%">
                    业务名称
                </td>
                <td style="width: 30%">
                    <asp:Label ID="lblSerialName" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr style="height: 100px">
                <td>
                    申请理由
                </td>
                <td colspan="3" style="text-align: left; vertical-align: top">
                    <asp:TextBox ID="txtContents" runat="server" TextMode="MultiLine" Width="98%" Height="100px"
                        ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width: 20%; ">
                    附件信息
                </td>
                <td colspan="3" style="text-align: left; vertical-align: top">
                    &nbsp;
                    <asp:Label ID="lblFileAtt" runat="server" Width="98%"></asp:Label>
                    &nbsp;
                </td>
            </tr>
        </table>
        <br />
        <table style="width: 100%;" class="tab">
            <tr>
                <td colspan="4">
                    <input id="btnClose" type="button" value="关闭" class="NewButton" onclick="windowClose1();" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
