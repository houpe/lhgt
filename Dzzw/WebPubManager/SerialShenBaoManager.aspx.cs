﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using Business;
using System.Configuration;
using Business.Common;
using SbBusiness.Wsbs;

public partial class WebPubManager_SerialShenBaoManager : PageBase
{
    ShenBaoSubmit clsSubmit = new ShenBaoSubmit();
    DataTable dtTable = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindSubmitFlag();
            BindTitleMenu();
        }
    }

    /// <summary>
    /// 绑定TAB菜单 
    /// </summary>
    /// <!--addby zhongjian 20091229-->
    protected void BindTitleMenu()
    {
        string strFlowName = Request["FlowName"];
        dtTable = clsSubmit.GetTableanName(strFlowName);
        RepeaterWeb.DataSource = dtTable;
        RepeaterWeb.DataBind();
    }

    /// <summary>
    /// 生成TAB按钮
    /// </summary>
    /// <param name="row"></param>
    /// <returns></returns>
    /// <!--addby zhongjian 20091229-->
    protected string GetContent(IDataItemContainer row)
    {
        string strHtml = string.Empty;
        string strServername = Request.Url.Authority;
        string strNewwebdisplay = ConfigurationManager.AppSettings["NewWebDisplay"];
        string strIId = Request["iid"];
        string strTitle = (string)DataBinder.Eval(row.DataItem, "TABLEANOTHERNAME");
        string strFid = (string)DataBinder.Eval(row.DataItem, "FID");
        strHtml = string.Format("<td id='td{2}' onclick='SetBackColor(this,\"http://{0}{1}?fid={2}&userid=-1&iid={3}&step=-1\",\"{5}\");' class='tabcontrol_button03'>{4}</td>",
            strServername, strNewwebdisplay, strFid, strIId, strTitle, Server.UrlEncode(strTitle));


        return strHtml;
    }

    protected void btnOk_Click(object sender, EventArgs e)
    {
        //设置预审通过标志
        clsSubmit.SetYsFlag(Request["iid"],3);
        ////发送消息 update by zhongjian 20100324
        clsSubmit.SetMsg(Request["iid"], "预审通过");
        btnBzbq.Visible = false;
        btnBysl.Visible = false;
        btnOk.Visible = false;
    }

    /// <summary>
    /// 绑定流程状态
    /// </summary>
    /// <!--addby zhongjian 20100323-->
    protected void BindSubmitFlag()
    {
        dtTable = clsSubmit.GetSerialSubmitflag(Request["iid"]);
        string strSubmitFlag = dtTable.Rows[0]["submitflag"].ToString();
        if (strSubmitFlag == "3")
        {
            btnBzbq.Visible = false;
            btnBysl.Visible = false;
            btnOk.Visible = false;
        }
    }
}
