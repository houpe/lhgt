﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using Business.FlowOperation;
using Business.Menu;

public partial class WebPubManager_WorkFlowManager :PageBase
{
    private WorkFlowPubSetting wfpsTemp = new WorkFlowPubSetting();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindWorkFlowList();
        }
    }

    /// <summary>
    /// 绑定流程信息
    /// </summary>
    /// <!--addby zhongjian 20091010-->
    private void BindWorkFlowList()
    {
        string strParentID = ConfigurationManager.AppSettings["bsznId"];

        SysMenuOperation smoTemp = new SysMenuOperation();
        DataTable dtTable = smoTemp.GetSysMenuByParentid(strParentID, "");
        RepeaterWeb.DataSource = dtTable;
        RepeaterWeb.DataBind();
    }

    /// <summary>
    /// 绑定标题以及内容页面
    /// </summary>
    /// <param name="row"></param>
    /// <returns></returns>
    /// <!--addby zhongjian 20091010-->
    protected string GetContent(IDataItemContainer row)
    {
        string strHtml = string.Empty;
        string strFlowID = Request["id"];
        string strWorkName = Request["flowname"];
        string strTitle = (string)DataBinder.Eval(row.DataItem, "menu_title");
        string strChUrl = (string)DataBinder.Eval(row.DataItem, "menu_churl");

        strChUrl = strChUrl.Replace("~", string.Format("http://{0}:{1}{2}", Request.ServerVariables["SERVER_NAME"], 
            Request.ServerVariables["SERVER_PORT"], Request.ApplicationPath));
        if (strTitle == ConfigurationManager.AppSettings["bsznName"])
        {
            strHtml = string.Format(@"<td id='td{1}' onclick='SetBackColor(this);' style='background-image: url(../images/top_button_03.gif);background-repeat: no-repeat; text-align: center; height: 35px; width: 93px;'><a href='{0}?type={1}&datatype={2}&flowid={3}' target='ifmWorkFlow'>{1}</a></td>", strChUrl, strTitle, strWorkName, strFlowID);
        }
        else
        {
            if (strChUrl.IndexOf("?") >= 0)//如果url中已经包含了参数
            {
                strHtml = string.Format(@"<td id='td{1}' onclick='SetBackColor(this);' style='background-image: url(../images/top_button_03.gif);background-repeat: no-repeat; text-align: center; height: 35px; width: 93px;'><a href='{0}&datatype={2}&flowid={3}' target='ifmWorkFlow'>{1}</a></td>", strChUrl, strTitle, strWorkName, strFlowID);
            }
            else
            {
                strHtml = string.Format(@"<td id='td{1}' onclick='SetBackColor(this);' style='background-image: url(../images/top_button_03.gif);background-repeat: no-repeat; text-align: center; height: 35px; width: 93px;'><a href='{0}?datatype={2}&flowid={3}' target='ifmWorkFlow'>{1}</a></td>", strChUrl, strTitle, strWorkName, strFlowID);
            }
        }
        return strHtml;
    }
}
