﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OnLineCptMessage.aspx.cs"
    Inherits="WebPubManager_OnLineCptMessage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>在线投诉回复</title>
    <link href="../css/page.css" rel="Stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
    function GetFocus()
    {
        var ctrl=document.getElementById("txtNotesFk"); 
        ctrl.focus();    
    }
    function Return()
    {
        window.location.href="OnlineComplaint.aspx";
    }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table style="width: 800px;" class="tabbox">
            <tr class="tab_top" style="text-align: center;">
                <td style="font-weight: bold; font-size: 14px">
                    在线投诉管理
                </td>
            </tr>
            <tr>
                <td>
                    <table width="800px" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td align="left">
                                <span style='font-weight: bold; color: Blue;'>投诉日期：<asp:Label ID="lblData" runat="server"
                                    Text=""></asp:Label></span>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td align="right">
                                <a href="#" onclick="GetFocus()">回复</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr style="border: 1px solid #74a4c0;">
                            <td>
                                <span style='font-weight: bold; color: Blue'>被投诉部门：
                                    <asp:Label ID="lblDep" runat="server" Text=""></asp:Label></span>
                            </td>
                            <td>
                                <span style='font-weight: bold; color: Blue'>被投诉类型：</span> <span style='color: Blue'>
                                    <asp:Label ID="lblType" runat="server" Text=""></asp:Label></span>
                            </td>
                            <td>
                                <span style='font-weight: bold; color: Blue'>投诉人员：</span> <span style='color: Blue'>
                                    <asp:Label ID="lblName" runat="server" Text=""></asp:Label></span>
                            </td>
                            <td>
                                <span style='font-weight: bold; color: Blue'>被投诉案件编号：</span> <span style='color: Blue'>
                                    <asp:Label ID="lblSerial" runat="server" Text=""></asp:Label></span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <span style='font-weight: bold; color: Blue'>投诉建议：</span><br />
                                <span style='color: Blue'>
                                    <asp:Label ID="lblNotes" runat="server" Text=""></asp:Label></span>
                            </td>
                        </tr>
                    </table>
                    <!--绑定回复信息-->
                    <div>
                        <%=BindMessageFK()%>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtNotesFk"
                            Display="None" ErrorMessage="请认真填写回复内容!"></asp:RequiredFieldValidator>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <br />
    <div style="margin-left: 10px; width: 95%">
        投诉回复：
        <div>
            <asp:TextBox ID="txtNotesFk" runat="server" TextMode="MultiLine" Height="88px" Width="100%"></asp:TextBox></div>
        <div style="text-align: center;">
            <asp:Button ID="btnOK" CssClass="input" runat="server" Text="确定" OnClick="btnOK_Click" />
            &nbsp;<input id="Button2" type="button" value="返回" class="NewButton" onclick="Return()" />
        </div>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="List"
            ShowMessageBox="True" ShowSummary="False" />
    </div>
    <br />
    </form>
</body>
</html>
