﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddSysName.aspx.cs" Inherits="WebPubManager_AddSysName" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>添加系统类型</title>
    <link href="../css/page.css" rel="Stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
   <div style="width: 60%; margin: 0px auto; text-align: center;">
       
        <table border="0" cellpadding="0" cellspacing="0" class="tab">
            <tr>
                <td align="right">
                    系统名称
                </td>
                <td align="left">
                    <asp:TextBox id="txtUserTypeName" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnCommit" runat="server" Text="提交" OnClick="btnCommit_Click" CssClass="input" />
                </td>
                <td>
                    <input type="button" id="btnCancel" class="NewButton" value="取消"
                        onclick="Javascript:window.close();" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
