﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ProcessData.aspx.cs" Inherits="WebPubManager_ProcessData" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>表单数据配置</title>
    <link rel="stylesheet" type="text/css" href="../css/page.css" />
    <script type="text/javascript">
        function bindSouseSelect() {
            var lst = window.document.getElementById("lbUserData");
            var val = "";
            for (i = 0; i < lst.options.length; i++) {
                if (lst.options[i].selected == true) {
                    if (val == "") {
                        val = lst.options[i].value;
                    }
                    else {
                        val += "," + lst.options[i].value;
                    }
                }
            }
            alert(val);
        }
    </script>
</head>
<body style=" background-color:White;">
    <form id="form1" runat="server">
    <div style="text-align: center;">
        <controlDefine:MessageBox ID="msgAppear" runat="server" />
    </div>
    <div style="vertical-align: top; text-align: center;">
        <div style="text-align: right; width: 80%">
            <asp:Button ID="btnRefer" CssClass="input" runat="server" Text="提交" OnClick="btnRefer_Click"
                OnClientClick="return onRefer ();" />&nbsp;&nbsp;
            <asp:Button ID="btnDelete" runat="server" Text="删除" CssClass="input" OnClick="btnDelete_Click" />&nbsp;&nbsp;
            <asp:Button ID="btnReturn" CssClass="input" runat="server" Text="返回" OnClick="btnReturn_Click" />
        </div>
        <br />
        <div class="box_top_bar">
            <div class="box_top_left">
                <div class="box_top_right">
                    <div class="box_top_text">
                        表单数据初始值配置</div>
                </div>
            </div>
        </div>
        <div class="box_middle">
            <table border="0" cellpadding="0" cellspacing="0" class="box_middle_tab">
                <tr>
                    <td>
                        <%=Request["othername"]%><br />
                        <asp:ListBox ID="lbProcessData" runat="server" Height="320px" Width="200px" AutoPostBack="true"
                            OnSelectedIndexChanged="lbProcessData_SelectedIndexChanged"></asp:ListBox>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlSouseData" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSouseData_SelectedIndexChanged">
                            <asp:ListItem Selected="True" Value="0">用户</asp:ListItem>
                            <asp:ListItem Value="1">单位</asp:ListItem>
                        </asp:DropDownList>
                        <br />
                        <asp:ListBox ID="lbUserData" runat="server" Height="320px" AutoPostBack="true" Width="200px"
                            OnSelectedIndexChanged="lbUserData_SelectedIndexChanged"></asp:ListBox>
                    </td>
                    <td>
                        已配置的数据<br />
                        <asp:ListBox ID="lblDataList" runat="server" Height="320px" Width="180px" OnSelectedIndexChanged="lblDataList_SelectedIndexChanged"
                            SelectionMode="Multiple"></asp:ListBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        已选目标值<br />
                        <asp:TextBox ID="txtProcessData" runat="server" Width="180px" ReadOnly="true"></asp:TextBox>
                    </td>
                    <td>
                        已选来源值<br />
                        <asp:TextBox ID="txtUserData" runat="server" Width="180px" ReadOnly="true"></asp:TextBox>
                    </td>
                    <td>
                        提示：可以选择多个已配置的数据进行删除
                    </td>
                </tr>
            </table>
        </div>
        <asp:HiddenField ID="hdfStableName" runat="server" />
        <asp:HiddenField ID="hdfScolName" runat="server" />
        <asp:HiddenField ID="hdfTtableName" runat="server" />
        <asp:HiddenField ID="hdfTcolName" runat="server" />
        <asp:HiddenField ID="hdfSql" runat="server" />

    </div>
    </form>
</body>
</html>
