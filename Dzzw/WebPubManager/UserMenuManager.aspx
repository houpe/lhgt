﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UserMenuManager.aspx.cs"
    Inherits="WebPubManager_UserMenuManager" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>用户菜单管理</title>
    <link href="../css/page.css" rel="Stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div style="width: 80%; margin: 0px auto; text-align: center;">
        <controlDefine:CustomGridView ID="CustomGridView1" runat="server" AllowPaging="false"
            AutoGenerateColumns="false" Width="100%" ShowHideColModel="None" OnOnLoadData="CustomGridView1_OnLoadData"
            SkinID="List" OnRowCreated="CustomGridView1_RowCreated">
            <Columns>
                <asp:BoundField DataField="parentmenu" HeaderText="父菜单名"></asp:BoundField>
                <asp:BoundField DataField="childmenu" HeaderText="子菜单名"></asp:BoundField>
                <asp:BoundField DataField="menu_url" HeaderText="路径"></asp:BoundField>
                <asp:TemplateField HeaderText="查看权限">
                    <ItemTemplate>
                        <asp:CheckBox ID="ckbAuth" runat="server" Text="可查看"></asp:CheckBox>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerSettings Visible="False" />
        </controlDefine:CustomGridView>
        <asp:Button ID="btnSave" runat="server" Text="保存" OnClick="btnSave_Click" CssClass="input" />
        <asp:Button ID="btnReturn" runat="server" Text="返回" OnClick="btnReturn_Click" CssClass="input" />
    </div>
    </form>
</body>
</html>
