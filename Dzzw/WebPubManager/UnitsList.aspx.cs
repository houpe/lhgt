﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using Business.Common;
using Business.Admin;

public partial class WebPubManager_UnitsList :PageBase
{
    private SysUnitsOperation cUser = new SysUnitsOperation();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DeleteUnits();
            GridviewDatabind();
        }
    }

    /// <summary>
    /// 删除单位信息
    /// </summary>
    /// <!--addby zhongjian 20100412-->
    private void DeleteUnits()
    {
        string strId = Request.Params["id"];
        string strXml = string.Empty;
        if (Request.Params["action"] == "delete")
        {
            SysUnitsOperation.DeleteUnitsInfo(strId);//删除单位
        }
    }

    #region 数据绑定
    /// <summary>
    /// 绑定单位信息
    /// </summary>
    /// <!--addby zhongjian 20100412-->
    private void GridviewDatabind()
    {
        DataTable dt = cUser.GetUnits("", txtUnitsName.Text.Trim(), txtOrganization.Text.Trim(), txtNature.Text.Trim(), txtLegal.Text.Trim());
        gridviewCar.DataSource = dt;
        gridviewCar.RecordCount = dt.Rows.Count;
        gridviewCar.DataBind();
    }
    #endregion

    /// <summary>
    /// datagrid数据加载
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void recvgrid_OnLoadData(object sender, EventArgs e)
    {
        this.GridviewDatabind();
    }

    #region 行创建
    /// <summary>
    /// Handles the RowCreated event of the gridviewCar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
    protected void gridviewCar_RowCreated(object sender, GridViewRowEventArgs e)
    {
        switch (e.Row.RowType)
        {
            case DataControlRowType.DataRow:

                DataRowView drv = e.Row.DataItem as DataRowView;
                if (drv == null)
                {
                    break;
                }
                HyperLink hyedit = e.Row.FindControl("hyedit") as HyperLink;
                if (hyedit != null)
                {
                    hyedit.NavigateUrl = "UnitsEdit.aspx?id=" + drv["id"];
                }
                HyperLink hyuser = e.Row.FindControl("hyuser") as HyperLink;
                if (hyuser != null)
                {
                    hyuser.NavigateUrl = "UserList.aspx?unitsid=" + drv["id"] + "&unitsname=" + drv["unitsname"] + "";
                }
                HyperLink hydel = e.Row.FindControl("hydel") as HyperLink;
                if (hydel != null)
                {
                    hydel.Attributes.Add("OnClick", "return confirm('确定要删除该单位信息吗?该单位下的用户也将被删除!')");
                    hydel.NavigateUrl = "UnitsList.aspx?action=delete&id=" + drv["id"];
                }
                break;
        }
    }
    #endregion

    protected void btnRefer_Click(object sender, EventArgs e)
    {
        GridviewDatabind();
    }
}
