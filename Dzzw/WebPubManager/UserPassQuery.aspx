﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UserPassQuery.aspx.cs" Inherits="WebPubManager_UserPassQuery" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>用户密码查询</title>
    <link href="../css/page.css" rel="Stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div style="width: 99%; margin: 0px auto; text-align: center;">
        <table class="window_tab">
            <tr>
                <td>
                    用户ID：<asp:TextBox runat="server" ID="txtUserId"></asp:TextBox>
                    &nbsp; 用户名：<asp:TextBox runat="server" ID="txtUserName"></asp:TextBox>
                </td>
                <td style="text-align: left;">
                    <asp:Button ID="btnOk" CssClass="input" Text="查询" runat="server" OnClick="btnOk_Click" />
                </td>
            </tr>
        </table>
        <br />
        <controlDefine:CustomGridView ID="CustomGridView1" runat="server" AllowPaging="false"
            AutoGenerateColumns="false" Width="100%" ShowHideColModel="None"  SkinID="List" 
            OnOnLoadData="CustomGridView1_OnLoadData">
            <Columns>
                <asp:BoundField DataField="userid" HeaderText="用户id" />
                <asp:BoundField DataField="username" HeaderText="用户名" />
                <asp:BoundField DataField="query_pass" HeaderText="密码 " />
            </Columns>
            <PagerSettings Visible="False" />
            <HeaderStyle CssClass="top_table" Height="30px" Width="50px" />
            <RowStyle CssClass="inputtable" />
        </controlDefine:CustomGridView>
    </div>
    </form>
</body>
</html>
