﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using System.Collections.Generic;
using System.Text;
using Business.FlowOperation;

public partial class WebPubManager_WorkFlowPubEdit : System.Web.UI.Page
{
    public List<string> lstMenuType = new List<string>();//菜单是否匿名的值
    public List<string> lstMenuAppear = new List<string>();//菜单是否显示值

    private WorkFlowPubSetting wfpsTemp = new WorkFlowPubSetting();
    public string strFlowID = string.Empty;//流程ID
    public string strFlowName = string.Empty;//流程名称
    public string strFlowOldID = string.Empty;//上一个已发布流程ID
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindWorkFlowMenu();//绑定事项信息菜单
            BindMaxNum();//绑定排序值
            if (!string.IsNullOrEmpty(Request["id"]))
            {
                DataTable dtSource = wfpsTemp.GetAllPubWorkFlow(Request["id"]);
                if (dtSource.Rows.Count > 0)    
                {
                    txtFlowNum.Text = dtSource.Rows[0]["flownum"].ToString();
                    txtFlowType.Text = dtSource.Rows[0]["flowtype"].ToString();
                    ViewState["ddlDefaultValue"] = dtSource.Rows[0]["flowname"].ToString();
                    cbCheck.Checked = dtSource.Rows[0]["ischeck"].ToString()=="0"?false:true;
                    lblisPub.Text = dtSource.Rows[0]["pubtext"].ToString();
                    lblCreateTime.Text = dtSource.Rows[0]["CREATETIME"].ToString();
                    lblEditTime.Text = dtSource.Rows[0]["EDITTIME"].ToString();
                    lblPubTime.Text = dtSource.Rows[0]["PUBTIME"].ToString();
                    string strIsSave = dtSource.Rows[0]["issave"].ToString();
                    string strIsPub = dtSource.Rows[0]["ispub"].ToString();
                    //暂时取消在本页面对接口地址的设置 update by zhongjian 20091223
                    cbInterfaceType.Checked = dtSource.Rows[0]["INTERFACETYPE"].ToString() == "0" ? false : true;
                    txtInterfaceUrl.Text = dtSource.Rows[0]["INTERFACEURL"].ToString();
                    if (strIsSave == "1" && strIsPub=="0")
                    {
                        lblMessage.Visible = true;
                        lblMessage.Text = "【注：当前数据已保存但未发布】";
                    }
                }
            }
        }

        if (ViewState["ddlDefaultValue"] != null)
        {
            ddlFlowName.Text = ViewState["ddlDefaultValue"].ToString();
        }
    }

    /// <summary>
    /// 绑定事项信息菜单
    /// </summary>
    /// <!--addby zhongjian 20091012-->
    private void BindWorkFlowMenu()
    {
        string strParentID = ConfigurationManager.AppSettings["bsznId"];//网上办事流程的ID  
        DataTable dtTable = wfpsTemp.GetSysMenuByParentid(strParentID);
        RepeaterWeb.DataSource = dtTable;
        RepeaterWeb.DataBind();
    }

    /// <summary>
    /// 绑定菜单权限设置
    /// </summary>
    /// <param name="IDataItemContainer"></param>
    /// <returns></returns>
    /// <!--addby zhongjian 20091012-->
    public bool SetCheck(IDataItemContainer row,string strFieldName)
    {
        bool flag = false;
        string strMenuName = "";//流程名称
        string strType = "";//菜单设置
        string strMenuTitle= (string)DataBinder.Eval(row.DataItem, "menu_title");
        DataTable dtTable = BindWorkFlow();
        if (dtTable.Rows.Count > 0)
        {
            for (int i = 0; i < dtTable.Rows.Count; ++i)
            {
                strMenuName = dtTable.Rows[i]["menu_name"].ToString();
                strType = dtTable.Rows[i][strFieldName].ToString();
                if (strMenuName == strMenuTitle)
                {
                    if (strType == "1")
                    {
                        if (strFieldName == "menu_type")
                        {
                            lstMenuType.Add(strMenuName);
                        }
                        else if (strFieldName == "MENU_APPEAR")
                        {
                            lstMenuAppear.Add(strMenuName);
                        }

                        flag = true;
                    }
                    else
                    {
                        flag = false;
                    }
                }
            }
        }
        return flag;
    }

    /// <summary>
    /// 绑定排序最大值
    /// </summary>
    /// <!--addby zhongjian 20092027-->
    public void BindMaxNum()
    {
        txtFlowNum.Text=wfpsTemp.GetDefineMax();
    }

   
    /// <summary>
    /// 获取要绑定的子菜单信息
    /// </summary>
    /// <returns></returns>
    /// <!--addby zhongjian 20091014-->
    public DataTable BindWorkFlow()
    {
        DataTable dtTable = new DataTable();
        string strPub = "0";//发布标识
        if (Request["id"] != null)
        {
            strFlowID = Request["id"];
        }
        if (Request["ispub"] != null)
        {
            strPub = Request["ispub"];
        }
        dtTable = wfpsTemp.GetWorkFlowSet(strFlowID);
        hfFlag.Value = strPub;//发布的标识
        if (strPub == "1")
        {
            this.btnCancelPub.Visible = true;
        }

        return dtTable;
    }

   
    /// <summary>
    /// 取消发布
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnCancelPub_Click(object sender, EventArgs e)
    {
        if (Request["id"] != null)
            strFlowID = Request["id"];
        wfpsTemp.PubWorkFlow(strFlowID, "0");//取消发布
        Return();
    }

    /// <summary>
    /// 保存流程
    /// </summary>
    /// <param name="strPub">发布标识(0:不发布；1：发布)</param>
    /// <!--addby zhongjian 20091014-->
    public void SaveWorkFlow(string strPub)
    {
        string strAction = string.Empty;
        if (Request["id"] != null)
        {
            strFlowID = Request["id"];
            int strCheck = 0;
            if (cbCheck.Checked)
                strCheck = 1;
            int strInserfaceType = 0;
            if (cbInterfaceType.Checked)
                strInserfaceType = 1;
            //修改流程信息
            wfpsTemp.UpdateWorkFlow(strFlowID, ddlFlowName.Text.Trim(), txtFlowType.Text.Trim(), txtFlowNum.Text.Trim(), strPub, "", strCheck, strInserfaceType,txtInterfaceUrl.Text.Trim());
            wfpsTemp.SaveWorkFlowSet(strFlowID);


            for (int i = 0; i < RepeaterWeb.Items.Count; ++i)
            {
                CheckBox cbWorkFlow = (CheckBox)RepeaterWeb.Items[i].FindControl("cbWorkFlow");
                CheckBox cbAppear = (CheckBox)RepeaterWeb.Items[i].FindControl("cbAppear");
                if (cbWorkFlow.Checked)
                {
                    lstMenuType.Add(cbWorkFlow.ToolTip);
                }
                else
                {
                    lstMenuType.Remove(cbWorkFlow.ToolTip);
                }

                if (cbAppear.Checked)
                {
                    lstMenuAppear.Add(cbAppear.ToolTip);
                }
                else
                {
                    lstMenuAppear.Remove(cbAppear.ToolTip);
                }
            }

            if (lstMenuType.Count > 0)
            {
                wfpsTemp.SetWorkFlow(lstMenuType, lstMenuAppear,strFlowID);
            }
        }
    }

    /// <summary>
    /// 返回流程列表
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnReturn_Click(object sender, EventArgs e)
    {
        Return();
    }

    /// <summary>
    /// 保存流程设置修改等操作
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtFlowType.Text))
        {
            string strAction = Request["action"];
            if (strAction == "insert")
            {
                //检查流程名称
                bool strFlag =false;
                strFlag = wfpsTemp.CheckFlowName(ddlFlowName.Text.Trim(), txtFlowType.Text.Trim(), "0");
                if (strFlag)
                {
                    string strMsg = "流程名称或办理事项有重复！";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "", string.Format(" <script> alert('{0}'); </script> ", strMsg));  
                }
                else
                {
                    int strCheck = 0;
                    if (cbCheck.Checked)//判断上次材料是否显示
                        strCheck = 1;
                    int strInserfaceType = 0;
                    if (cbInterfaceType.Checked)//判断是否启用接口
                        strInserfaceType = 1;
                    wfpsTemp.InsertWorkFlow(ddlFlowName.Text.Trim(), txtFlowType.Text.Trim(), txtFlowNum.Text.Trim(), lstMenuType, "0", strCheck, strInserfaceType, txtInterfaceUrl.Text.Trim(), lstMenuAppear);
                    Return();
                }
            }
            else
            {
                SaveWorkFlow("0"); //保存流程
                Return();
            }
        }
    }

   

    /// <summary>
    /// 替换之前发布版本,重新发布
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// <!--addby zhongjian 20091020 根据用户选择由前台JS调用-->
    protected void btnRePub_Click(object sender, EventArgs e)
    {
        //发布前再检查是否已经存在此流程的已发布版本
        strFlowOldID = wfpsTemp.GetWorkFlowId(ddlFlowName.Text.Trim(), "", "1", "0");        
        wfpsTemp.PubWorkFlow(strFlowOldID, "0");//先注销已存在的版本
        SaveWorkFlow("1"); //保存流程  
        wfpsTemp.PubWorkFlow(strFlowID, "1");//发布流程
        //设置整个流程的发布标识
        strFlowName = Request["flowname"];
        wfpsTemp.SetCanSync(strFlowID, strFlowName);
        Return();
    }

    /// <summary>
    /// 整体流程保存并发布
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnPub_Click(object sender, EventArgs e)
    {
        bool strMessage = false;
        if (!string.IsNullOrEmpty(txtFlowType.Text))
        {
            string strAction = Request["action"];
            if (strAction == "insert")
            {
                //检查流程名称
                strMessage = wfpsTemp.CheckFlowName(ddlFlowName.Text.Trim(), "", "1");
                if (strMessage)
                {
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "<script>alert('流程名称或办理事项有重复！');</script>");
                }
                else
                {
                    int strCheck = 0;
                    if (cbCheck.Checked)
                        strCheck = 1;
                    int strInserfaceType = 0;
                    if (cbInterfaceType.Checked)
                        strInserfaceType = 1;
                    wfpsTemp.InsertWorkFlow(ddlFlowName.Text.Trim(), txtFlowType.Text.Trim(), txtFlowNum.Text.Trim(), lstMenuType, "1", strCheck, strInserfaceType,txtInterfaceUrl.Text.Trim(),lstMenuAppear);
                    //如果不存在，直接发布流程
                    wfpsTemp.PubWorkFlow(strFlowID, "1");//发布流程
                    Return();
                }
            }
            else
            {
                if (Request["id"] != null)
                {
                    strFlowID = Request["id"];
                    //发布前再检查是否已经存在此流程的已发布版本
                    strFlowOldID = wfpsTemp.GetWorkFlowId(ddlFlowName.Text.Trim(), "", "1", "0");
                    if (string.IsNullOrEmpty(strFlowOldID))
                    {
                        SaveWorkFlow("1"); //保存流程  
                        //如果不存在，直接发布流程
                        wfpsTemp.PubWorkFlow(strFlowID, "1");//发布流程
                        //设置整个流程的发布标识
                        strFlowName = Request["flowname"];
                        wfpsTemp.SetCanSync(strFlowID, strFlowName);
                        Return();
                    }
                    else
                    {
                        //当已存在此流程的发布版本时，提示用户是否替换。
                        //注：调用前台JS方法目的是为了获取confirm返回值。
                        //如果直接写在这里，会将代码执行完之后才弹出JS对话框，因此达不到提示的功能效果。
                        LiteralControl li = new LiteralControl();
                        li.Text = "<script>GetConfirm();</script>";
                        Controls.Add(li);
                    }
                }
            }
        }
    }

    /// <summary>
    /// 删除流程(逻辑删除)
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        if (Request["id"] != null)
        {
            strFlowID = Request["id"];
        }
        wfpsTemp.UpdateWorkFlow(strFlowID, "", "", "", "", "1", 0, 0, "");
        Return();
    }
    /// <summary>
    /// 返回流程列表
    /// </summary>
    public void Return()
    {
        //跳出上一级框架 edit by zhongjian 20091010
        Response.Write("<script>parent.location.href='WorkFlowPub.aspx';</script>");
    }
}
