﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using Business;
using WF_Business;
using SbBusiness.Wsbs;

using Business.Common;

public partial class WebPubManager_Process : System.Web.UI.Page
{
    private XtProcess cProcess = new XtProcess();
    private UploadFileClass upFile = new UploadFileClass();
    string strType = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request["datatype"] != null)
            strType = Request["datatype"];

        //删除表单
        if (Request["action"] == "deletefrom")
        {
            XtProcess.DeleteProcess(Request["id"]);
        }
        //删除材料
        if (Request["action"] == "deleteMaterial")
        {
            UploadFileClass.DeleteModule(Request["id"]);
        }
        //清空材料附件
        if (Request["action"] == "empty")
        {
            UploadFileClass.EmptyModule(Request["id"]);
        }

        if (!IsPostBack)
        {                     
            BindFromUpDown();
            BindMaterialUpDown();
            BindFroms();
            BindModule();
        }
    }    

    protected string GetFlow()
    {
        string strFlow = Request["flowid"];
        return strFlow;
    }

    #region 返回菜单管理主界面
    protected void btnReturn_Click(object sender, EventArgs e)
    {
        Response.Redirect("WorkFlowPubEdit.aspx?id=" + Request["flowid"]);
        //为跳出上一级框架 edit by zhongjian 20091010
        //Response.Write("<script>parent.location.href='WorkFlowPub.aspx';</script>");
    }
    #endregion

    #region 表单内容

    /// <summary>
    /// 表单数据绑定
    /// </summary>
    private void BindFroms()
    {
        DataTable dt = cProcess.GetContent(Request["flowid"]);
        gridviewCar.DataSource = dt;
        gridviewCar.RecordCount = dt.Rows.Count;
        gridviewCar.DataBind();
    }

    /// <summary>
    /// 表单数据加载
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void recvgrid_OnLoadData(object sender, EventArgs e)
    {
        this.BindFroms();
    }

    /// <summary>
    /// 表单行创建
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridviewCar_RowCreated(object sender, GridViewRowEventArgs e)
    {
        switch (e.Row.RowType)
        {
            case DataControlRowType.DataRow:

                DataRowView drv = e.Row.DataItem as DataRowView;
                if (drv == null)
                {
                    break;
                }
                HyperLink hyedit = e.Row.FindControl("hyedit") as HyperLink;
                if (hyedit != null)
                {
                    hyedit.NavigateUrl = "ProcessEdit.aspx?id=" + drv["id"] + "&datatype=" + Request["datatype"] + "&flowid=" + Request["flowid"];
                }
                HyperLink hyData = e.Row.FindControl("hyData") as HyperLink;
                if (hyedit != null)
                {
                    hyData.NavigateUrl = "ProcessData.aspx?id=" + drv["id"] + "&othername=" + drv["tableanothername"] + "&datatype=" + Request["datatype"] + "&flowid=" + Request["flowid"];
                }
                break;
        }
    }

    /// <summary>
    /// 表单上移
    /// </summary>
    /// <param name="row"></param>
    /// <returns></returns>
    /// <!--addby zhongjian 20091026-->
    protected string GetFromUp(IDataItemContainer row)
    {
        string strHtml = string.Empty;
        if (DataBinder.Eval(row.DataItem, "ORDERDIELD").ToString() == "0")
        {
            strHtml = "";
        }
        else
        {
            strHtml = "<a href='Process.aspx?fromupId=" + DataBinder.Eval(row.DataItem, "ID").ToString() + "&fromupNo=" + DataBinder.Eval(row.DataItem, "ORDERDIELD").ToString() + "&datatype=" + Request["datatype"] + "&flowid=" + Request["flowid"] + "'>上移</a>";
        }

        return strHtml;
    }

    /// <summary>
    /// 表单下移
    /// </summary>
    /// <param name="row"></param>
    /// <returns></returns>
    /// <!--addby zhongjian 20091026-->
    protected string GetFromDown(IDataItemContainer row)
    {
        string strHtml = string.Empty;
        string strCount = XtProcess.GetProcessMax(Request["flowid"]);
        if (strCount != null)
        {
            if (DataBinder.Eval(row.DataItem, "ORDERDIELD").ToString() == strCount)
            {
                strHtml = "";
            }
            else
            {
                strHtml = "<a href='Process.aspx?fromdownId=" + DataBinder.Eval(row.DataItem, "ID").ToString() + "&fromdownNo=" + DataBinder.Eval(row.DataItem, "ORDERDIELD").ToString() + "&datatype=" + Request["datatype"] + "&flowid=" + Request["flowid"] + "'>下移</a>";
            }
        }
        return strHtml;
    }
    /// <summary>
    /// 表单列表排序
    /// </summary>
    /// <!--添加表单排序的功能 addby zhongjian 20091026-->
    protected void BindFromUpDown()
    {
        //============
        /***************上移****************/
        if (Request["FromupId"] != null && Convert.ToInt32(Request["FromupNo"]) >= 1)
        {
            int NewNo = Convert.ToInt32(Request["FromupNo"]) - 1;
            string strId = "select id from XT_PROCESS where ORDERDIELD=" + NewNo + "and flowid ='" + Request["flowid"] + "'";
            string strUpId = SysParams.OAConnection().GetValue(strId);
            string UpparNo = "update XT_PROCESS set ORDERDIELD=" + Convert.ToInt32(Request["FromupNo"]) + " where id='" + strUpId + "'";
            int i = SysParams.OAConnection().RunSql(UpparNo);
            string UpparNonew = "update XT_PROCESS set ORDERDIELD=" + NewNo + " where id = '" + Request["FromupId"] + "'";
            int j = SysParams.OAConnection().RunSql(UpparNonew);
        }

        /***************下移****************/
        if (Request["FromdownId"] != null)
        {
            int NewNo = Convert.ToInt32(Request["FromdownNo"]) + 1;
            string strId = "select id from XT_PROCESS where ORDERDIELD=" + NewNo + "and flowid ='" + Request["flowid"] + "'";
            string strUpId = SysParams.OAConnection().GetValue(strId);
            string UpparNo = "update XT_PROCESS set ORDERDIELD=" + Convert.ToInt32(Request["FromdownNo"]) + " where id='" + strUpId + "'";
            int i = SysParams.OAConnection().RunSql(UpparNo);
            string UpparNonew = "update XT_PROCESS set ORDERDIELD=" + NewNo + " where id = '" + Request["FromdownId"] + "'";
            int j = SysParams.OAConnection().RunSql(UpparNonew);
        }
    }

    /// <summary>
    /// 添加表单
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnAddForms_Click(object sender, EventArgs e)
    {
        string strUrl=string.Format("ProcessEdit.aspx?flowid={0}&datatype={1}",Request["flowid"],Request["datatype"]);
        this.Response.Redirect(strUrl);
    }
    #endregion

    #region 材料内容
    /// <summary>
    /// 添加材料
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnAddMaterial_Click(object sender, EventArgs e)
    {
        string strUrl = string.Format("../WebPubManager/SubmitModuleEdit.aspx?flowid={0}&datatype={1}", Request["flowid"], Request["datatype"]);
        this.Response.Redirect(strUrl);
    }

    /// <summary>
    /// 绑定材料数据
    /// </summary>
    /// <!--addby zhongjian 20091217-->
    protected void BindModule()
    {
        DataTable dt = upFile.GetModuleType(Request["flowid"]);
        gridviewMaterial.DataSource = dt;
        gridviewMaterial.RecordCount = dt.Rows.Count;
        gridviewMaterial.DataBind();
    }

    /// <summary>
    /// 材料数据加载
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridviewMaterial_OnLoadData(object sender, EventArgs e)
    {
        this.BindModule();
    }

    /// <summary>
    /// 材料行创建
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridviewMaterial_RowCreated(object sender, GridViewRowEventArgs e)
    {
        switch (e.Row.RowType)
        {
            case DataControlRowType.DataRow:

                DataRowView drv = e.Row.DataItem as DataRowView;
                if (drv == null)
                {
                    break;
                }

                HyperLink hyEmpty = e.Row.FindControl("hyEmpty") as HyperLink;
                if (hyEmpty != null)
                {
                    hyEmpty.Attributes.Add("OnClick", "return confirm('确定要清空该材料附件?')");
                    hyEmpty.NavigateUrl = "Process.aspx?action=empty&flowid=" + Request["flowid"] + "&id=" + drv["id"] + "&datatype=" + strType;
                }

                HyperLink hyedit = e.Row.FindControl("hyedit") as HyperLink;
                if (hyedit != null)
                {
                    hyedit.NavigateUrl = "SubmitModuleEdit.aspx?flowid=" + Request["flowid"] + "&id=" + drv["id"] + "&datatype=" + strType;
                }
                break;
        }
    }

    /// <summary>
    /// 当附件模板为空时不显示查询结果(隐藏.点的显示)
    /// </summary>
    /// <param name="row">当前行</param>
    /// <returns>返回要显示的信息</returns>
    /// <!--addby zhongjian 20090923-->
    public string GetFileName(IDataItemContainer row)
    {
        string strName = DataBinder.Eval(row.DataItem, "FILENAME").ToString();
        if (strName == null || strName == "" || strName == ".")
        {
            return "";
        }
        else
        {
            strName = strName + "." + DataBinder.Eval(row.DataItem, "FILETYPE").ToString();
            return strName;
        }
    }

    /// <summary>
    /// 材料上移
    /// </summary>
    /// <param name="row"></param>
    /// <returns></returns>
    /// <!--addby zhongjian 20091018-->
    protected string GetUp(IDataItemContainer row)
    {
        string strHtml = string.Empty;
        if (DataBinder.Eval(row.DataItem, "ORDERDIELD").ToString() == "0")
        {
            strHtml = "";
        }
        else
        {
            strHtml = "<a href='Process.aspx?upId=" + DataBinder.Eval(row.DataItem, "ID").ToString() + "&upNo=" + DataBinder.Eval(row.DataItem, "ORDERDIELD").ToString() + "&flowid=" + Request["flowid"] + "&datatype=" + Request["datatype"] + "'>上移</a>";
        }

        return strHtml;
    }

    /// <summary>
    /// 材料下移
    /// </summary>
    /// <param name="row"></param>
    /// <returns></returns>
    ///  <!--addby zhongjian 20091018-->
    protected string GetDown(IDataItemContainer row)
    {
        string strHtml = string.Empty;
        string strCount = UploadFileClass.GetModuleMax(Request["flowid"]);
        if (strCount != null)
        {
            if (DataBinder.Eval(row.DataItem, "ORDERDIELD").ToString() == strCount)
            {
                strHtml = "";
            }
            else
            {
                strHtml = "<a href='Process.aspx?downId=" + DataBinder.Eval(row.DataItem, "ID").ToString() + "&downNo=" + DataBinder.Eval(row.DataItem, "ORDERDIELD").ToString() + "&flowid=" + Request["flowid"] + "&datatype=" + Request["datatype"] + "'>下移</a>";
            }
        }
        return strHtml;
    }

    /// <summary>
    /// 材料列表排序
    /// </summary>
    /// <!--添加排序的功能 addby zhongjian 20091018-->
    protected void BindMaterialUpDown()
    {
        //============
        /***************上移****************/
        if (Request["upId"] != null && Convert.ToInt32(Request["upNo"]) >= 1)
        {
            int NewNo = Convert.ToInt32(Request["upNo"]) - 1;
            string strId = "select id from XT_SERIAL_MODULE where ORDERDIELD=" + NewNo + "and flowid ='" + Request["flowid"] + "'";
            string strUpId = SysParams.OAConnection().GetValue(strId);
            string UpparNo = "update XT_SERIAL_MODULE set ORDERDIELD=" + Convert.ToInt32(Request["upNo"]) + " where id='" + strUpId + "'";
            int i = SysParams.OAConnection().RunSql(UpparNo);
            string UpparNonew = "update XT_SERIAL_MODULE set ORDERDIELD=" + NewNo + " where id = '" + Request["upId"] + "'";
            int j = SysParams.OAConnection().RunSql(UpparNonew);
        }

        /***************下移****************/
        if (Request["downId"] != null)
        {
            int NewNo = Convert.ToInt32(Request["downNo"]) + 1;
            string strId = "select id from XT_SERIAL_MODULE where ORDERDIELD=" + NewNo + "and flowid ='" + Request["flowid"] + "'";
            string strUpId = SysParams.OAConnection().GetValue(strId);
            string UpparNo = "update XT_SERIAL_MODULE set ORDERDIELD=" + Convert.ToInt32(Request["downNo"]) + " where id='" + strUpId + "'";
            int i = SysParams.OAConnection().RunSql(UpparNo);
            string UpparNonew = "update XT_SERIAL_MODULE set ORDERDIELD=" + NewNo + " where id = '" + Request["downId"] + "'";
            int j = SysParams.OAConnection().RunSql(UpparNonew);
        }
    }
    #endregion
}
