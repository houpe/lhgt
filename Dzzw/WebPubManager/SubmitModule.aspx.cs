﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using Business;
using System.Configuration;
using Business.Common;

public partial class WebPubManager_SubmitModule : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            GridviewDatabind();            
        }        
    }

    #region 数据绑定
    /// <summary>
    /// 绑定在线申报上传的资源信息
    /// </summary>
    /// <!--addby zhongjian 20090831-->
    protected void GridviewDatabind()
    {
        UploadFileClass upFile = new UploadFileClass();
        DataTable dt = upFile.GetCailiaoInfo(Request["flowname"],Request["iid"]);
        gridviewCar.DataSource = dt;
        gridviewCar.RecordCount = dt.Rows.Count;
        gridviewCar.DataBind();
    }
    #endregion 

    /// <summary>
    /// datagrid数据加载
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void recvgrid_OnLoadData(object sender, EventArgs e)
    {
        this.GridviewDatabind();
    }

    #region 行创建
    //
    protected void gridviewCar_RowCreated(object sender, GridViewRowEventArgs e)
    {
        switch (e.Row.RowType)
        {
            case DataControlRowType.DataRow:

                DataRowView drv = e.Row.DataItem as DataRowView;
                if (drv == null)
                {
                    break;
                }
                break;
        }
    }
    #endregion
    
    /// <summary>
    /// 当附件模板为空时不显示查询结果(隐藏.点的显示)
    /// </summary>
    /// <param name="row">当前行</param>
    /// <returns>返回要显示的信息</returns>
    /// <!--addby zhongjian 20090831-->
    public string GetFileName(IDataItemContainer row)
    {
        string strName = DataBinder.Eval(row.DataItem, "mudulefilename").ToString();
        if (strName == null || strName == "" || strName == ".")
        {
            return "";
        }
        else
        {
            return strName;
        }
    }

    /// <summary>
    /// 当附件上传材料名称为空时不显示查询结果(隐藏.点的显示)
    /// </summary>
    /// <param name="row">当前行</param>
    /// <returns>返回要显示的信息</returns>
    /// <!--addby zhongjian 20090831-->
    public string GetAttName(IDataItemContainer row)
    {
        string strAttName = DataBinder.Eval(row.DataItem, "attachfilename").ToString();
        if (strAttName == null || strAttName == "" || strAttName == ".")
        {
            strAttName = "";
        }
        return strAttName;
    }

}
