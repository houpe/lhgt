﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Business;
using Business.Admin;
using System.Drawing;
using SbBusiness.User;

public partial class WebPubManager_RegisterUserSp : PageBase
{
    protected void BingData()
    {
        DataTable dt = SysUserHandle.GetRegisterUser(rblSp.SelectedValue, txtSeach.Text);
        gvData.DataSource = dt;
        gvData.RecordCount = dt.Rows.Count;
        gvData.DataBind();
    }

    /// <summary>
    /// datagrid数据加载
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvData_OnLoadData(object sender, EventArgs e)
    {
        this.BingData();
    }

    #region 行创建
    protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
    {
        switch (e.Row.RowType)
        {
            case DataControlRowType.DataRow:
                DataRowView dtrv = e.Row.DataItem as DataRowView;
                if (dtrv == null)
                {
                    break;
                }

                string strUserFlag = dtrv["ISVALID"].ToString();

                HyperLink hyuser = e.Row.FindControl("hyuser") as HyperLink;
                HyperLink hySptg = e.Row.FindControl("hySptg") as HyperLink;
                HyperLink hyDel = e.Row.FindControl("hyDel") as HyperLink;

                if (hyuser != null)
                {
                    hyuser.Text = dtrv["userid"].ToString();
                    hyuser.NavigateUrl = "#";
                    hyuser.Attributes.Add("onclick", string.Format("UserMessage('{0}')", dtrv["userid"]));

                    hyDel.Visible = false;
                }

                if (strUserFlag.CompareTo("1") == 0)
                {
                    hySptg.NavigateUrl = "#";
                    hySptg.BackColor = Color.Silver;
                    hySptg.Text = "已通过";
                }
                else
                {
                    if (hySptg != null)
                    {
                        hySptg.NavigateUrl = "#";
                        hySptg.Attributes.Add("onclick", string.Format("SubmitClient('{0}',1)", dtrv["id"]));
                    }

                    if (hyDel != null)
                    {
                        hyDel.Visible = true;
                        hyDel.NavigateUrl = "#";
                        hyDel.Attributes.Add("onclick", string.Format("SubmitClient('{0}',0)", dtrv["id"]));
                    }
                }
                break;
        }
    }
    #endregion

    /// <summary>
    /// 更新操作
    /// </summary>
    protected void UpdateOperation()
    {
        if (!string.IsNullOrEmpty(Request["sid"]))
        {
            if (Request["resultFlag"] == "1")
            {
                SysUserHandle.SetRegUserValide(Request["sid"]);
            }
            else//删除操作
            {
                SysUserHandle.DelUserInfoById(Request["sid"]);
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            UpdateOperation();
            BingData();
        }
    }

    protected void btnQuery_Click(object sender, EventArgs e)
    {
        BingData();
    }
}