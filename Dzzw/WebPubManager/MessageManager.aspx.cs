﻿ 
// 创建人  ：zhongjian 
// 创建时间：2009年11月23日
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Business;
using WF_Business;

public partial class WebPubManager_MessageManager :PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindMessageList();
        }
    }

    /// <summary>
    /// 绑定消息步骤名称
    /// </summary>
    public void BindMessageList()
    {
        DataTable dtTable = GetSelectOptions("");
        ddlControl.Items.Clear();
        foreach (DataRow row in dtTable.Rows)
        {
            ddlControl.Items.Add(new ListItem(row["step_name"].ToString(), row["step_name"].ToString()));
        }
        ddlControl_SelectedIndexChanged(null, null);
    }

    /// <summary>
    /// 获取所有流程名称(目前xt_request_step内容来自管理员手工录入)
    /// </summary>
    /// <param name="strParamsName">消息步骤名称</param>
    /// <returns></returns>
    /// <!--addby zhongjian 20091124-->
    public DataTable GetSelectOptions(string strStepName)
    {
        string strSql = string.Empty;
        DataTable dtOut;
        strSql = "select id,step_no,step_name,step_msg from xt_request_step where 1=1 ";
        if (!string.IsNullOrEmpty(strStepName))
            strSql += string.Format("and step_name='{0}'", strStepName);
        strSql += string.Format(" order by step_no", strStepName);
        SysParams.OAConnection().RunSql(strSql, out dtOut);
        return dtOut;
    }

    protected void ddlControl_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtTemp = GetSelectOptions(ddlControl.SelectedItem.Text);
        if (dtTemp.Rows.Count > 0)
        {
            txtStepNO.Text = dtTemp.Rows[0]["step_no"].ToString();
            txtStepMsg.Text = dtTemp.Rows[0]["step_msg"].ToString();
        }
    }
    protected void btnOK_Click(object sender, EventArgs e)
    {
        SaveMessage();
    }

    protected void SaveMessage()
    {
        string strSql =string.Format(@"update xt_request_step set step_no='{0}',step_msg='{1}' where step_name='{2}' "
                                    ,txtStepNO.Text.Trim(),txtStepMsg.Text.Trim(),ddlControl.SelectedItem.Text);
        SysParams.OAConnection().RunSql(strSql);
    }
}
