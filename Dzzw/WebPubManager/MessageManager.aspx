﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MessageManager.aspx.cs" Inherits="WebPubManager_MessageManager" %>

<%@ Register Src="../UserControls/GeneralSelect.ascx" TagName="GeneralSelect" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>通知消息管理</title>
    <link rel="stylesheet" type="text/css" href="../css/page.css" />
    <script type="text/javascript" src="../ScriptFile/Regex.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div style="width: 99%; margin: 0px auto; text-align: center;">
        <div class="box_top_bar">
            <div class="box_top_left">
                <div class="box_top_right">
                    <div class="box_top_text">
                        通知消息管理</div>
                </div>
            </div>
        </div>
        <div class="box_middle4">
            <table width="90%" border="0" cellpadding="0" cellspacing="0" class="box_middle_tab">
                <tr>
                    <td>
                        消息步骤名称：
                    </td>
                    <td style="text-align: left;">
                        <asp:DropDownList ID="ddlControl" runat="server" OnSelectedIndexChanged="ddlControl_SelectedIndexChanged"
                            AutoPostBack="True" Width="80px">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        通知消息步骤：
                    </td>
                    <td style="text-align: left;">
                        <asp:TextBox ID="txtStepNO" runat="server" Width="80px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        通知消息内容：
                    </td>
                    <td style="text-align: left;">
                        <asp:TextBox ID="txtStepMsg" runat="server" TextMode="MultiLine" Height="150px" Width="350px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        提示说明：
                    </td>
                    <td style="text-align: left;">
                        1、{0}：代表用户名称；{1}代表流程名称；<br />
                        2、为消息内容的准确性，特约定除用户注册步骤只出现{0}以外，<br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;其它步骤应同时出现{0}和{1}。<br />
                        3、如果该步骤不需要发送消息，请将内容设置为空。<br />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Button ID="btnOK" runat="server" Text="保存" CssClass="input" OnClick="btnOK_Click" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </form>
</body>
</html>
