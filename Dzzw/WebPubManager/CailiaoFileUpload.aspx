﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CailiaoFileUpload.aspx.cs"
    Inherits="WebPubManager_CailiaoFileUpload" %>

<%@ Register Src="../UserControls/FileUploadForCommon.ascx" TagName="FileUpdate" TagPrefix="uc1" %>
<%@ Register Src="../UserControls/GeneralSelect.ascx" TagName="GeneralSelect" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>文件上传</title>
    <link href="../css/page.css" rel="Stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <br />
    <div style="text-align: center; width: 100%; margin: 0px auto;">
        <table style="text-align: left; width: 100%;" class="tab">
            <tr>
                <td>
                    表格名称
                </td>
                <td style="text-align: left;">
                    <asp:TextBox ID="txtFileName" SkinID="LongInputGreen" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    上传文件
                </td>
                <td style="text-align: left;">
                    <uc1:FileUpdate ID="FileUpdate1" OnAfterUploadedEvent="FileUpdate1_AfterUploadedEvent"
                        runat="server" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
