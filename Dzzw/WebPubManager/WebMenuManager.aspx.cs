﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using Business.Menu;

public partial class WebPubManager_WebMenuManager : System.Web.UI.Page
{
    private SysMenuOperation cW = new SysMenuOperation();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Params["action"] == "delete")
        {
            SysMenuOperation.DeleteMenu(Request.Params["id"]);
        }

        if (!IsPostBack)
        {
            GridviewDatabind();
        }
    }

    #region 数据绑定
    //
    private void GridviewDatabind()
    {    
        DataTable dt = cW.GetMenusSorted("");
        gridviewCar.DataSource = dt;
        gridviewCar.RecordCount = dt.Rows.Count;
        gridviewCar.DataBind();
    }
    #endregion 
    
    /// <summary>
    /// datagrid数据加载
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void recvgrid_OnLoadData(object sender, EventArgs e)
    {
        this.GridviewDatabind();
    }

    #region 行创建
    //
    protected void gridviewCar_RowCreated(object sender, GridViewRowEventArgs e)
    {
        switch (e.Row.RowType)
        {
            case DataControlRowType.DataRow:

                DataRowView drv = e.Row.DataItem as DataRowView;
                if (drv == null)
                {
                    break;
                }
                HyperLink hyedit = e.Row.FindControl("hyedit") as HyperLink;
                if (hyedit != null)
                {
                    hyedit.NavigateUrl = "editWebMenu.aspx?id=" + drv["id"];
                }

                HyperLink hydel = e.Row.FindControl("hydel") as HyperLink;
                if (hydel != null)
                {
                    if (string.IsNullOrEmpty(drv["child"].ToString()) || drv["child"].ToString() == "0")
                    {
                        hydel.Attributes.Add("OnClick", "return confirm('确定要删除该菜单?')");
                        hydel.NavigateUrl = "WebMenuManager.aspx?action=delete&id=" + drv["id"];
                    }
                    else
                    {
                        hydel.Visible = false;
                    }
                }

                HyperLink hyadd = e.Row.FindControl("hyadd") as HyperLink;
                if (hyadd != null)
                {
                    if (string.IsNullOrEmpty(drv["menu_url"].ToString()))
                    {
                        hyadd.NavigateUrl = "editWebMenu.aspx?action=add&parentId=" + drv["id"];
                    }
                    else
                    {
                        hyadd.Visible = false;
                    }
                }
                break;
        }
    }
    #endregion
}
