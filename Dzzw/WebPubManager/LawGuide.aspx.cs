﻿/**
 * 添加办事指南为动态表格形式
 * addby zhongjian 20100115
 * **/
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using System.Collections.Generic;
using System.Text;
using Business.FlowOperation;

public partial class WebPubManager_LawGuide : PageBase
{
    private WorkFlowPubSetting wfpsTemp = new WorkFlowPubSetting();

    /// <summary>
    /// 存储办事指南信息
    /// </summary>
    /// <!--addby zhongjian 20100118-->
    protected DataTable tbVita
    {
        get
        {
            return (DataTable)this.ViewState["tbVita"];
        }
        set
        {
            this.ViewState["tbVita"] = value;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ViewState["datatype"] = Request["datatype"];
            ViewState["flowid"] = Request["flowid"];
            //获取办事指南
            tbVita= wfpsTemp.GetLawGuide(ViewState["flowid"].ToString());
            hidNumber.Value = tbVita.Rows.Count.ToString();

        }
    }

    /// <summary>
    /// 保存办事指南
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// <!--addby zhongjian 20100118-->
    protected void btnSave_Click(object sender, EventArgs e)
    {
        int iNum = int.Parse(hidCount.Value);
        if(ViewState["flowid"]!=null)//先删除以前的信息，再添加新的信息。
            wfpsTemp.DeleteLawGuide(ViewState["flowid"].ToString());
        for(int i=0;i<=iNum;++i)
        {
            string strTitle = string.Empty;
            string strContent = string.Empty;
            if (Request.Form["fTitle" + i + ""] != null)
                strTitle = Request.Form["fTitle" + i + ""].ToString();
            if (Request.Form["FContent" + i + ""] != null)
                strContent = Request.Form["FContent"+i+""].ToString();
            if (strTitle.Length > 80 || strContent.Length > 1400)
            {
                string strErrMsg=string.Format("<script>alert('第{0}项：{1}，字符过大，将不做保存。')</script>",i,strTitle);
                Page.ClientScript.RegisterStartupScript(GetType(), "ok", strErrMsg);
                continue;
            }
            if (!string.IsNullOrEmpty(strTitle) && ViewState["datatype"] != null && ViewState["flowid"] != null)
                wfpsTemp.SaveLawGuide(ViewState["datatype"].ToString(), ViewState["flowid"].ToString(), strTitle, strContent, i.ToString());
        }
        //让页面重新加载（刷新本页面）
        Page.ClientScript.RegisterStartupScript(GetType(), "reload", "<script>location.replace(location.href);</script>");
    }

    /// <summary>
    /// 绑定已经存在的办事指南
    /// </summary>
    /// <returns></returns>
    /// <!--addby zhongjian 20100118-->
    protected string BindLawGuide()
    {
        string strHtml=string.Empty;
        if (ViewState["flowid"] != null)
        {
            DataTable dtTemp = tbVita;
            for (int i = 0; i < dtTemp.Rows.Count; ++i)
            {
                trDefault.Visible = false;
                string strTitle = dtTemp.Rows[i]["ftitle"].ToString();
                string strContent = dtTemp.Rows[i]["fcontent"].ToString();
                strHtml += string.Format(@"<tr >
                        <td>
                            <input id='fTitle' name='fTitle{2}' type='text'  value='{0}' style='font-weight:bold;text-align:center'/>
                        </td>
                        <td align='left'>
                            <textarea id='FContent' name='FContent{2}' style='overflow:visible;width:98%;'>{1}</textarea>
                        </td>
                        <td>
                            <a href='#' onclick='javascript:deleteThisRow(this);'>移除</a>
                        </td>
                    </tr>", strTitle, strContent, i);
            }
        }

        return strHtml;
    }
}
