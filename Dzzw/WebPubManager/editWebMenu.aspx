﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="editWebMenu.aspx.cs" Inherits="WebPubManager_editWebMenu" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>申报菜单管理</title>
    <link rel="stylesheet" type="text/css" href="../css/page.css" />
</head>
<script type="text/javascript">
    //判断是否是正整数
    function IsNum(s) {
        if (s != null) {
            var r, re;
            re = /\d*/i; //\d表示数字,*表示匹配多个数字
            r = s.match(re);
            return (r == s) ? true : false;
        }
        return false;
    }
    function Save() {
        var eOrder = document.getElementById("<%= orderfield.ClientID %>");

        if (eOrder.value != "") {
            if (!IsNum(eOrder.value)) {
                alert("菜单顺序必须为正整数");
                eOrder.focus();
                return false;
            }
        }
        else {
            return true;
        }

    }
</script>
<body>
    <form id="Form1" name="queryForm" action="" runat="server">
    <div align="center">
        <controlDefine:MessageBox ID="msgAppear" runat="server" />
    </div>
    <div align="center">
        <table id="Table1" width="90%" border="0" class="box_middle_tab">
            <tr>
                <td style="height: 25px">
                    <div class="box_top_bar">
                        <div class="box_top_left">
                            <div class="box_top_right">
                                <div class="box_top_text">
                                    添加菜单</div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="box_middle" style="height:300px; width:100%">
                        <table width="70%" border="0" cellpadding="0" cellspacing="0" class="box_middle_tab">
                            <tr>
                                <td style="width: 374px; height: 28px;">
                                    菜单名称
                                </td>
                                <td style="width: 270px; height: 28px;">
                                    <asp:TextBox MaxLength="100" size="50" ID="menuname" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 374px; height: 28px;">
                                    菜单路径
                                </td>
                                <td style="width: 270px; height: 28px;">
                                    <asp:TextBox size="50" ID="menuurl" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 374px; height: 28px;">
                                    菜单顺序
                                </td>
                                <td style="width: 270px; height: 28px;">
                                    <asp:TextBox MaxLength="100" size="50" ID="orderfield" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 374px; height: 28px;">
                                    <asp:Label ID="LabelParent" runat="server" Text="父菜单名称">
                                    </asp:Label>
                                </td>
                                <td style="width: 270px; height: 28px;">
                                    <asp:TextBox MaxLength="100" size="50" ID="parentname" runat="server" Enabled="false">
                                    </asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 374px; height: 28px;">
                                    <asp:Label ID="lblChUrl" runat="server" Text="数据维护地址">
                                    </asp:Label>
                                </td>
                                <td style="width: 270px; height: 28px;">
                                    <asp:TextBox size="50" ID="txtChUrl" runat="server">
                                    </asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="height: 40px">
                                    <asp:Button ID="inputdata" class="NewButton" Text="保存" runat="server" OnClick="inputdata_Click"
                                        OnClientClick="return Save ();" />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:Button ID="editdata" Text="重写" class="NewButton" runat="server" OnClick="editdata_Click" />
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:Button ID="Button1" Text="返回" class="NewButton" runat="server" OnClick="Button1_Click" />
                                </td>
                                <td style="height: 40px">
                                </td>
                            </tr>
                        </table>
                        <asp:TextBox ID="parentid" runat="server" Width="118px" Visible="False"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
