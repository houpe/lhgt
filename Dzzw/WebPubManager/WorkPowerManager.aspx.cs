﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Business;
using Business.FlowOperation;
using Business.Admin;

public partial class WebPubManager_WorkPowerManager :PageBase
{
    private WorkFlowPubSetting wfpsTemp = new WorkFlowPubSetting();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DataTable dtSource = SysSettingOperation.GetSetting("注册用户不需预审");

            if (dtSource.Rows.Count > 0)
            {
                cbxYhsz.Checked = dtSource.Rows[0]["value"].ToString() == "1" ? true : false;               
            }
            BindFlowType();
            BindDepList();
        }
    }

    /// <summary>
    /// 保存注册用户预审设置
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnRefer_Click(object sender, EventArgs e)
    {
        int nYhsz = cbxYhsz.Checked ? 1 : 0;
        SysSettingOperation.StoreSetting(cbxYhsz.Text, nYhsz);
    }

    /// <summary>
    /// 绑定所有流程别名
    /// </summary>
    public void BindFlowType()
    {
        DataTable dtTable = wfpsTemp.GetPubWorkFlow();
        ddlWorkflow.Items.Clear();
        foreach (DataRow row in dtTable.Rows)
        {
            ddlWorkflow.Items.Add(new ListItem(row["flowname"].ToString(), row["id"].ToString()));
        }
        //设置默认值
        ViewState["FlowType"] = ddlWorkflow.SelectedItem.Text;
        ViewState["TypeID"] = ddlWorkflow.SelectedItem.Value;
    }

    /// <summary>
    /// 绑定咨询部门
    /// </summary>
    public void BindDepList()
    {
        DataTable dtTable = RequestFlowOperation.GetDepList();
        ddlDep.Items.Clear();
        foreach (DataRow row in dtTable.Rows)
        {
            ddlDep.Items.Add(new ListItem(row["depart_name"].ToString(), row["departid"].ToString()));
        }
        //设置默认值
        ViewState["DepID"] = ddlDep.SelectedItem.Value;
        ViewState["DepName"] = ddlDep.SelectedItem.Text;
    }

  
    protected void ddlWorkflow_SelectedIndexChanged(object sender, EventArgs e)
    {
        ViewState["FlowType"] = ddlWorkflow.SelectedItem.Text;
        ViewState["TypeID"] = ddlWorkflow.SelectedItem.Value;
    }

  
    protected void ddldep_SelectedIndexChanged(object sender, EventArgs e)
    {
        ViewState["DepID"] = ddlDep.SelectedItem.Value;
        ViewState["DepName"] = ddlDep.SelectedItem.Text;
    }

}
