﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Business;
using Business.Admin;

public partial class WebPubManager_Yushen : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DataTable dtSource = SysSettingOperation.GetSetting("注册用户不需预审");

            if (dtSource.Rows.Count > 0)
            {
                cbxYhsz.Checked = dtSource.Rows[0]["value"].ToString() == "1" ? true : false;
            }
        }
    }
    protected void btnRefer_Click(object sender, EventArgs e)
    {
        int nYhsz = cbxYhsz.Checked ? 1 : 0;

        SysSettingOperation.StoreSetting(cbxYhsz.Text, nYhsz);
    }

}
