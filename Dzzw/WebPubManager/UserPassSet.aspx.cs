﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using Business;
using System.Data.Common;
using System.Data.OracleClient;
using Common;
using Business.Common;
using Business.Admin;
using SbBusiness.User;
public partial class WebPubManager_UserPassSet : System.Web.UI.Page
{
    public string strId = "";
    string strXml = string.Empty;
    string strExMsg = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Params["id"] != null)
        {
            strId = Request.Params["id"];
        }
    }

    protected void btnOK_Click(object sender, EventArgs e)
    {
        string strSql = string.Empty;
        if (hfFlag.Value == "1")
        {
            return;
        }
        if (rblUserPwdType.SelectedValue == "0")//与UserID相同
        {
            //先更新密码
            SysUserHandle.UpdatePassword(strId);         
        }
        else if (rblUserPwdType.SelectedValue == "1")//随机密码
        {
            SysUserHandle.UpdatePassSet(strId);
        }
        else if (rblUserPwdType.SelectedValue == "2")//指定密码
        {
            string strUserPwd = txtUserRePwd.Text.Trim().ToString();
            SysUserHandle.UpdatePassSet(strId, strUserPwd);
        }

        //在showModalDialog打开页面时,刷新父页面并关闭 addby zhongjian 20091019
        this.Response.Write("<script>window.returnValue = 'True';</script>");//设置返回值为True,在父页面刷新
        this.Response.Write("<script>window.close();</script>");
    }

    protected void rblUserPwdType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rblUserPwdType.SelectedValue == "0" || rblUserPwdType.SelectedValue == "1")
        {
            this.trUserPwd.Visible = false;
            this.trUserRePwd.Visible = false;
        }
        if (rblUserPwdType.SelectedValue == "2")
        {
            this.trUserPwd.Visible = true;
            this.trUserRePwd.Visible = true;
        }
    }
   

}
