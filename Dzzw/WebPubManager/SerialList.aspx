﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SerialList.aspx.cs" Inherits="WebPubManager_SerialList" %>

<%@ Register Src="../UserControls/GeneralSelect.ascx" TagName="GeneralSelect" TagPrefix="uc2" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>业务管理权限配置</title>
    <link rel="stylesheet" type="text/css" href="../css/page.css" />
    <script type="text/javascript" src="../ScriptFile/Regex.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div style="text-align: center; margin: 0px auto;" class="page_box">
        <div class="box_top_bar">
            <div class="box_top_left">
                <div class="box_top_right">
                    <div class="box_top_text">
                        业务管理权限配置</div>
                </div>
            </div>
        </div>
        <div class="box_middle4">
            <table width="70%" border="0" cellpadding="0" cellspacing="0" class="box_middle_tab">
                <tr>
                    <td>
                        流程名称：
                    </td>
                    <td style="text-align: left;">
                        <%--<uc2:GeneralSelect ID="ddlFlowName" runat="server" IsParamsData="false" Name="所有流程" />--%>
                        <asp:DropDownList ID="ddlControl" runat="server" OnSelectedIndexChanged="ddlControl_SelectedIndexChanged"
                            AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <a href="../SystemManager/membermis.aspx?groupid=<%=GetFlowID()%>&groupname=<%=GetFlowName()%>"
                            target="_self">权限设置</a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </form>
</body>
</html>
