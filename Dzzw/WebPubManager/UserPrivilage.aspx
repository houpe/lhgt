﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UserPrivilage.aspx.cs" Inherits="WebPubManager_UserPrivilage" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>人员权限管理</title>
    <link href="../css/page.css" rel="Stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div style="text-align: center">
        <table style="width: 100%; border-top-width: 0px" class="tab">
            <tr style="border-top-width: 0px">
                <td style="border-top-width: 0px">
                    <controlDefine:CustomGridView ID="CustomGridView1" runat="server" AllowPaging="false" AutoGenerateColumns="false"
                        Width="100%" SkinID="List" DivFstClass-DivButtonClass="input" DivScdClass-DivButtonClass="input"
                        DivTrdClass-DivButtonClass="input" ShowHideColModel="None" OnOnLoadData="CustomGridView1_OnLoadData">
                        <Columns>
                            <asp:BoundField DataField="系统代号" HeaderText="系统代号"></asp:BoundField>
                            <asp:BoundField DataField="用户类型名" HeaderText="用户类型名"></asp:BoundField>
                            <asp:BoundField DataField="用户类型代码" HeaderText="用户类型代码"></asp:BoundField>
                            <asp:BoundField DataField="系统名称" HeaderText="系统名称"></asp:BoundField>
                            <asp:TemplateField HeaderText="菜单权限">
                                <ItemTemplate>
                                    <asp:HyperLink ID="hlEdit" runat="server" NavigateUrl='<%# DataBinder.Eval(Container.DataItem,"ID", "UserMenuManager.aspx?usertype_id={0}") %>'>编辑</asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>                        
                            <asp:TemplateField HeaderText="删除菜单">
                                <itemtemplate>
                                    <a href='<%# DataBinder.Eval(Container.DataItem,"ID", "UserPrivilage.aspx?action=delete&id={0}") %>' onclick="return confirm('确定要删除该菜单?')">删除</a>
                                </itemtemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerSettings Visible="False" />
                    </controlDefine:CustomGridView>
                    <input type="button" id="btnAddSysName" class="NewButton" onclick="Javascript:window.open('AddSysName.aspx','添加系统名称','height=100,width=300,top=200px,left=200px,toolbar =no,menubar=no, scrollbars=yes, resizable=no, location=no, status=yes');"
                        value="添加系统" />
                    &nbsp;&nbsp;&nbsp;
                    <input type="button" id="btnAddUserType" class="NewButton" onclick="Javascript:window.open('AddUserTypeName.aspx','添加类型名称','height=100,width=300,top=200px,left=200px,toolbar =no,menubar=no, scrollbars=yes, resizable=no, location=no, status=yes');"
                        value="添加类型" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
