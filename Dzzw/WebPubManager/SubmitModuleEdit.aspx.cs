﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using Business;
using System.Configuration;
using Business.Common;

public partial class WebPubManager_SubmitModuleEdit : PageBase
{
    private UploadFileClass upload = new UploadFileClass();

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request["id"]))
            {
                DataTable dt = upload.GetModuleId(Request["id"]);

                if (dt.Rows.Count > 0)
                {
                    txtModuleName.Text = dt.Rows[0]["modulename"].ToString();
                    //LabelFlowName.Text = dt.Rows[0]["type"].ToString();

                    
                }
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LabelFlowName.Text = Request["datatype"];
            txtModuleName.Name = LabelFlowName.Text;
            
            SetFileUpdatePerproty();
            CheckBoxUpload.Attributes["onclick"] = "CheckUpload()";
        }
    }

    /// <summary>
    /// 设置文件上传属性
    /// </summary>
    private void SetFileUpdatePerproty()
    {
        //设置文件上传控件
        FileUpdate1.KeyId = "id";
        FileUpdate1.FileStoreFieldName = "FILEDATA";
        FileUpdate1.FileTypeFieldName = "FILETYPE";
        FileUpdate1.TableName = "XT_SERIAL_MODULE";

        FileUpdate1.StoreKeyFieldName = "TYPE";
        if (!string.IsNullOrEmpty(Request["datatype"]))
        {
            FileUpdate1.StoreKeyValue = Request["datatype"];
        }
        FileUpdate1.StoreResourceFieldName = "FILENAME";
        FileUpdate1.StoreParamFieldName = "MODULENAME";
        FileUpdate1.StoreParamValue = txtModuleName.ID;
        FileUpdate1.FlowIDValue = Request["flowid"];

        FileUpdate1.StoreResourceType = "insert";
        FileUpdate1.StoreResourceNameControlName = txtModuleName.ID;
    }

    protected void btnRefer_Click(object sender, EventArgs e)
    {
        string strId = Request["id"];

        if (strId == null || strId == "")
        {
            string strMax = UploadFileClass.GetModuleMax(Request["flowid"]);
            int intMax = 0;
            if(!string.IsNullOrEmpty(strMax))
                intMax=int.Parse(strMax) + 1;
            strMax = intMax.ToString();

            UploadFileClass.InsertModule(txtModuleName.Text, LabelFlowName.Text, Request["flowid"], strMax);
        }
        else
        {
            UploadFileClass.UpdateModule(strId, txtModuleName.Text, LabelFlowName.Text, Request["flowid"]);
        }

        Response.Redirect("Process.aspx?flowid=" + Request["flowid"] + "&datatype=" + Request["datatype"]);
    }

    /// <summary>
    /// 上传后触发事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void FileUpdate1_AfterUploadedEvent(object sender, EventArgs e)
    {
        Response.Redirect("Process.aspx?flowid=" + Request["flowid"] + "&datatype=" + Request["datatype"]);
    }

    #region 返回菜单管理主界面
    protected void btnReturn_Click(object sender, EventArgs e)
    {
        Response.Redirect("Process.aspx?flowid=" + Request["flowid"] + "&datatype=" + Request["datatype"]);
    }
    #endregion
}
