﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using Business;
using System.Configuration;
using Common;
using Business.Common;
using Business.Admin;
using SbBusiness.User;

public partial class WebPubManager_UserEdit :PageBase
{
    private SysUserHandle cUser = new SysUserHandle();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //设置身份证类型的验证
            ddlZjlx.ValidatorName = cvZjhm.ClientID;
            Page.ClientScript.RegisterStartupScript(this.GetType(), "knh1", "<script>ddlChange('" + ddlZjlx.ddlClientID + "');</script>");

            if (!string.IsNullOrEmpty(Request["id"]))
            {
                DataTable dt = cUser.GetSysUserById(Request["id"]);

                if (dt.Rows.Count > 0)
                {
                    txtUserId.Text =BasicOperate.GetDataBaseString(dt.Rows[0]["USERID"]);
                    txtUserName.Text = BasicOperate.GetDataBaseString(dt.Rows[0]["USERNAME"]);
                    trUserPwd.Visible = false;
                    trUserRePwd.Visible = false;
                    ddlZjlx.Value = dt.Rows[0]["IDTYPE"].ToString();
                    txtZjhm.Text = dt.Rows[0]["IDNUMBER"].ToString();
                    txtTel.Text = dt.Rows[0]["TEL"].ToString();
                    txtCz.Text = dt.Rows[0]["FAX"].ToString();
                    txtMobile.Text = dt.Rows[0]["MOBILE"].ToString();
                    txtEmail.Text = dt.Rows[0]["EMAIL"].ToString();
                    txtYZBM.Text = dt.Rows[0]["POSTCODE"].ToString();
                    txtAddress.Text = dt.Rows[0]["ADDRESS"].ToString();
                    hfUsertype.Value = dt.Rows[0]["type"].ToString();//用户类型
                }
            }
            //单位ID和单位名称
            if ((!string.IsNullOrEmpty(Request["unitsid"])) && (!string.IsNullOrEmpty(Request["unitsname"])))
            {
                hidUnitsID.Value = Request["unitsid"];
                lblUntisName.Text = Request["unitsname"];
            }
            else
            {
                trUnitsName.Visible = false;
            }
        }
    }

    protected void btnRefer_Click(object sender, EventArgs e)
    {
        //获取操作者ip地址
        string myIP = Common.IPOperation.GetIpAddress();

        string strId = Request["id"];
        string strCszcUserType = ConfigurationManager.AppSettings["csyhSystemFlag"];
        if (string.IsNullOrEmpty(strId))
        {
            if (HiddenFieldFlag.Value == "0")
            {
                return;
            }
            else if (HiddenFieldFlag.Value == "1")
            {
                return;
            }
            else if (HiddenFieldFlag.Value == "2")
            {
                return;
            }
            else if (HiddenFieldFlag.Value == "3")
            {
                return;
            }
            else
            {
                bool userExist = SysUserHandle.CheckUserExist(txtUserId.Text);
                if (!userExist)
                {
                    SysUserHandle.CreateUser(txtUserId.Text, txtUserName.Text, txtUserPwd.Text,
                        int.Parse(strCszcUserType), txtTel.Text, txtMobile.Text,
                        txtAddress.Text, ddlZjlx.Value, txtZjhm.Text, txtCz.Text, txtEmail.Text,
                        txtYZBM.Text,1,"", hidUnitsID.Value,"","","");

                    Response.Redirect("UserList.aspx?unitsid=" + hidUnitsID.Value + "&unitsname=" + lblUntisName.Text + "");
                }
                else
                {
                    labMsg.Text = "用户名已存在，请重新输入！";
                }
            }
        }
        else
        {
            strCszcUserType = hfUsertype.Value;//在作修改时,应该保存原有的用户类型 addby zhongjian 20090904
            SysUserHandle.UpdateUser(strId, txtUserId.Text, txtUserName.Text,
                int.Parse(strCszcUserType), txtTel.Text, txtMobile.Text,
                txtAddress.Text, ddlZjlx.Value, txtZjhm.Text, txtCz.Text, txtEmail.Text,
                txtYZBM.Text);
        }
    }

    #region 返回菜单管理主界面
    protected void btnReturn_Click(object sender, EventArgs e)
    {
        Response.Redirect("UserList.aspx?unitsid=" + hidUnitsID.Value + "&unitsname=" + lblUntisName.Text + "");
    }
    #endregion
}
