﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Business.Admin;
using SbBusiness.User;

public partial class WebPubManager_UserPassReset : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Params["action"] == "reset")
        {
            SysUserHandle.UpdatePassword(Request.Params["id"]);
        }

        if (!IsPostBack)
        {
            BindData();
        }
    }

    /// <summary>
    /// 绑定数据
    /// </summary>
    private void BindData()
    {
        DataTable dtSource = SysUserHandle.QueryPassByUserInfo(txtUserId.Text, txtUserName.Text);

        if (dtSource != null)
        {
            this.CustomGridView1.DataSource = dtSource;
            this.CustomGridView1.RecordCount = dtSource.Rows.Count;
            this.CustomGridView1.DataBind();
        }
    }

    protected void btnOk_Click(object sender, EventArgs e)
    {
        BindData();
    }

    protected void CustomGridView1_OnLoadData(object sender, EventArgs e)
    {
        BindData();
    }


    #region 行创建
    //
    protected void gridviewCar_RowCreated(object sender, GridViewRowEventArgs e)
    {
        switch (e.Row.RowType)
        {
            case DataControlRowType.DataRow:
                DataRowView dtrv = e.Row.DataItem as DataRowView;
                if (dtrv == null)
                {
                    break;
                }
                break;
        }
    }
    #endregion
}
