﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OnLineMsgManager.aspx.cs"
    Inherits="WebPubManager_OnLineMsgManager" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>留言咨询回复</title>
    <link href="../css/page.css" rel="Stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
    function GetFocus()
    {
        var ctrl=document.getElementById("txtNotesFk"); 
        ctrl.focus();  
    }
    //返回
    function Return()
    {
        window.location.href="OnLineMessage.aspx";
    }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table style="width: 96%;" class="tabbox">
            <tr class="tab_top" style="text-align: center;">
                <td style="font-weight: bold; font-size: 14px">
                    留言咨询管理
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td align="left" valign="middle" style="width: 60%;">
                                <span style='font-weight: bold; color: Blue'>留言日期：<asp:Label ID="lblData" runat="server"
                                    Text=""></asp:Label></span>
                            </td>
                            <td align="right" valign="middle" style="width: 40%">
                                <a href="#" onclick="GetFocus()">回复</a>&nbsp;
                                <asp:LinkButton ID="lbtnForward" runat="server" OnClick="lbtnForward_Click">转发</asp:LinkButton>&nbsp;
                                <asp:LinkButton ID="lbtnOK" runat="server" OnClick="lbtnOK_Click" Visible="false">审核通过</asp:LinkButton>
                                &nbsp;<asp:LinkButton ID="lbtnCancel" runat="server" OnClick="lbtnCancel_Click" Visible="false">撤消</asp:LinkButton>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr style="border: 1px solid #74a4c0;">
                            <td align="left" valign="middle" style="width: 60%;">
                                <span style='font-weight: bold; color: Blue'>留言主题：
                                    <asp:Label ID="lblTitle" runat="server" Text=""></asp:Label></span><br />
                            </td>
                            <td align="right" valign="middle" style="width: 40%">
                                <asp:DropDownList ID="ddlDep" runat="server" OnSelectedIndexChanged="ddldep_SelectedIndexChanged"
                                    AutoPostBack="True" Visible="false">
                                </asp:DropDownList>
                                <asp:DropDownList ID="ddlControl" runat="server" OnSelectedIndexChanged="ddlControl_SelectedIndexChanged"
                                    AutoPostBack="True" Visible="true">
                                </asp:DropDownList>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <span style='font-weight: bold; color: Blue'>留言内容：</span> <span style='color: Blue'>
                                    <asp:Label ID="lblNotes" runat="server" Text=""></asp:Label></span>
                            </td>
                        </tr>
                    </table>
                    <!--绑定回复信息-->
                    <div>
                        <%=BindMessageFK()%>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <br />
    <div style="margin-left: 10px; width: 95%">
        留言回复：
        <div>
            <asp:TextBox ID="txtNotesFk" runat="server" TextMode="MultiLine" Height="88px" Width="100%"></asp:TextBox></div>
        <div style="text-align: center;">
            <asp:Button ID="btnOK" CssClass="input" runat="server" Text="确定" OnClick="btnOK_Click" />&nbsp;
            <input id="Button2" type="button" value="返回" class="NewButton" onclick="Return()" />
        </div>
    </div>    
    <br />
    <asp:HiddenField ID="hfShowType" runat="server" />
    </form>
</body>
</html>
