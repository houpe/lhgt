﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SubmitModuleEdit.aspx.cs"
    Inherits="WebPubManager_SubmitModuleEdit" %>

<%@ Register Src="../UserControls/FileUploadForCommon.ascx" TagName="FileUpdate" TagPrefix="uc1" %>
<%@ Register src="../UserControls/PopupSelect.ascx" tagname="PopupSelect" tagprefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>添加材料</title>
    <link rel="stylesheet" type="text/css" href="../css/page.css" />
</head>
<script language="javascript" type="text/javascript">
    function CheckUpload() {
        var eUpload = document.getElementById("<%= CheckBoxUpload.ClientID %>");
        var eFile = document.getElementById("File");
        if (eUpload.checked) {
            eFile.style.display = "block";
        }
        else {
            eFile.style.display = "none";
        }
    }
</script>
<body>
    <form id="Form1" runat="server">
    <div align="center">
        <controlDefine:MessageBox ID="msgAppear" runat="server" />
    </div>
    <div align="center" style=" margin:0px auto;">
        <div class="box_top_bar">
            <div class="box_top_left">
                <div class="box_top_right">
                    <div class="box_top_text">
                        添加材料</div>
                </div>
            </div>
        </div>
        <div class="box_middle3">
            <table width="70%" border="0" cellpadding="0" cellspacing="0" class="box_middle_tab">
                <tr>
                    <td>
                        流程名称：
                    </td>
                    <td style="text-align: left;">
                        <asp:Label ID="LabelFlowName" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        材料名称：
                    </td>
                    <td style="text-align: left;">
                        <uc2:PopupSelect ID="txtModuleName" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox ID="CheckBoxUpload" runat="server" Text="是否上传附件" />
                    </td>
                    <td id="File" style="text-align: left; display: none;">
                        <uc1:FileUpdate ID="FileUpdate1" OnAfterUploadedEvent="FileUpdate1_AfterUploadedEvent"
                            runat="server" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Button ID="btnRefer" CssClass="input" runat="server" Text="提交" OnClick="btnRefer_Click" />
                        <asp:Button ID="btnReturn" CssClass="input" runat="server" Text="返回" OnClick="btnReturn_Click" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </form>
</body>
</html>
