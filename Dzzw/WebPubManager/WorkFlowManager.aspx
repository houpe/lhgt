﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WorkFlowManager.aspx.cs"
    Inherits="WebPubManager_WorkFlowManager" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>流程信息管理</title>
    <link rel="stylesheet" type="text/css" href="../css/page.css" />
    <script src="../ScriptFile/AutoIframeHeight.js" type="text/javascript"></script>
    <script src="../ScriptFile/jquery.js" type="text/javascript"></script>
    <script type="text/javascript">
        //设置tabcontrol按钮的背景颜色
        function SetBackColor(varCtrl) {
            $("#tabMenu td[id ^= 'td']").css("background-image", "url(../images/top_button_03.gif)");
            $("#tabMenu td[id ^= 'td']").find("a").css("font-weight", "");
            //$("#tabMenu td").not($("#" + varCtrl.id)[0]).find("a").css("font-weight", "");//包括了所有不等于当前控件id的td

            $("#" + varCtrl.id).find("a").css("font-weight", "bolder");
            $("#" + varCtrl.id).css("background-image", "url(../images/top_button_05.gif)");
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div style="width: 99%; margin: 0px auto; text-align: center;">
            <table border="0" id="tabMenu">
                <tr>
                    <td id="tdLcqx" onclick='SetBackColor(this);' style="background-image: url(../images/top_button_05.gif); background-repeat: no-repeat; text-align: center; height: 35px; width: 93px;">
                        <a href="../WebPubManager/WorkFlowPubEdit.aspx?flowname=<%= Request["flowname"] %>&id=<%=Request["id"] %>&action=<%=Request["action"] %>&ispub=<%=Request["ispub"] %>"
                            target="ifmWorkFlow">事项信息</a>
                    </td>
                    <asp:Repeater ID="RepeaterWeb" runat="server">
                        <ItemTemplate>
                            <%#GetContent(Container)%>
                        </ItemTemplate>
                    </asp:Repeater>
                </tr>
            </table>

            <iframe name="ifmWorkFlow" src='../WebPubManager/WorkFlowPubEdit.aspx?flowname=<%= Request["flowname"] %>&id=<%=Request["id"] %>&action=<%=Request["action"] %>&ispub=<%=Request["ispub"] %>'
                width="100%" height="600" onload="SetWinHeight(this);" frameborder="0" scrolling="no" />

        </div>
    </form>
</body>
</html>
