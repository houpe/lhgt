﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Business;
using System.IO;
using Business.Common;
using SbBusiness.User;

public partial class WebPubManager_AppearOtherResouce :PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindData();
        }
    }

    /// <summary>
    /// 附件数据绑定
    /// </summary>
    /// <!--update by zhongjian 20091221-->
    private void BindData()
    {
        DataTable dtStoreInfo = new DataTable();

        if (!string.IsNullOrEmpty(Request["id"]))
        {
            UploadFileClass uploadFileClass = new UploadFileClass();
            string strFlag=Request["type"];
            string strID=Request["id"];
            if (strFlag == "SerialAtt")//业务预审附件信息
            {
                SysUserRightRequest urrTemp = new SysUserRightRequest();
                dtStoreInfo = urrTemp.GetUserSerialAtt(strID);
            }
            else if (strFlag == "Material")//在线申报:材料附件信息
                dtStoreInfo = uploadFileClass.GetCailiaoStoreFile("XT_SERIAL_MODULE", strID);
            else if (strFlag == "AttAchment")//外网在线申报:上传的材料附件信息(或样本批准图片)
                dtStoreInfo = uploadFileClass.GetCailiaoStoreFile("xt_serial_attachment", strID);
            else//表格下载附件信息
                dtStoreInfo = uploadFileClass.GetStoreFile("SYS_RESOURCE", strID);
        }

        if (dtStoreInfo.Rows.Count > 0)
        {
            string strFileName = dtStoreInfo.Rows[0][2].ToString();//文件名称
            string strFileType = dtStoreInfo.Rows[0][3].ToString();//文件类型

            if (!string.IsNullOrEmpty(strFileType))
            {
                Response.ClearContent();
                //文件数据
                byte[] bArr = dtStoreInfo.Rows[0][1] as Byte[];

                strFileType = strFileType.ToLower();
                if (strFileType.CompareTo("jpg") == 0 || strFileType.CompareTo("gif") == 0 ||
                    strFileType.CompareTo("png") == 0 || strFileType.CompareTo("bmp") == 0)
                {
                    ImageView1.Images = bArr;
                    ImageView1.Visible = true;

                    //Response.Clear();
                    //Response.Buffer = true;
                    //Response.Charset = "gb2312";
                    //Response.ContentEncoding = System.Text.Encoding.GetEncoding("GB2312");
                    //Response.AppendHeader("content-disposition", "attachment;filename=\"" + System.Web.HttpUtility.UrlEncode(strFileName, System.Text.Encoding.UTF8) + "." + strFileType + "\"");
                    //Response.ContentType = "image/"+strFileType;
                    //Response.BinaryWrite(bArr);
                }
                else
                {
                    /////控制生成的文件名字 edit by zhongjian 20090910
                    Response.Clear();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Charset = "gb2312";
                    Response.ContentEncoding = System.Text.Encoding.GetEncoding("GB2312");
                    Response.AppendHeader("content-disposition", "attachment;filename=\"" + System.Web.HttpUtility.UrlEncode(strFileName, System.Text.Encoding.UTF8) + "." + strFileType + "\"");
                    if (strFileType.CompareTo("xls") == 0)
                    {
                        Response.ContentType = "application/vnd.ms-excel";
                        Response.BinaryWrite(bArr);
                    }
                    else if (strFileType.CompareTo("doc") == 0)
                    {
                        Response.ContentType = "application/msword";
                        Response.BinaryWrite(bArr);
                    }
                    //针对office 2007 update by zhongjian 20100409
                    else if (strFileType.CompareTo("docx") == 0 || strFileType.CompareTo("xlsx") == 0 || strFileType.CompareTo("pptx") == 0)
                    {
                        Response.BinaryWrite(bArr);
                    }
                    else if (strFileType.CompareTo("ppt") == 0)
                    {
                        Response.ContentType = "application/vnd.ms-powerpoint";
                        Response.BinaryWrite(bArr);
                    }
                    else if (strFileType.CompareTo("rtf") == 0)
                    {
                        Response.ContentType = "application/rtf";
                        Response.BinaryWrite(bArr);
                    }
                    else if (strFileType.CompareTo("pdf") == 0)
                    {
                        Response.ContentType = "application/pdf";
                        Response.BinaryWrite(bArr);
                    }
                    else if (strFileType.CompareTo("zip") == 0)
                    {
                        Response.ContentType = "application/zip";
                        Response.BinaryWrite(bArr);
                    }
                    else
                    {
                        Response.ContentType = "application/x-msdownload;";
                        Response.BinaryWrite(bArr);
                    }

                    Response.Flush();
                    Response.End();
                }

                
            }
        }
    }
}
