﻿ 
// 创建人  ：zhongjian 
// 创建时间：2009年11月17日
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WF_Business;
using Business;
using Business.FlowOperation;

public partial class WebPubManager_OnLineMsgList : System.Web.UI.Page
{
    public string strFlowName = string.Empty;//流程名称
    public string strFlowID = string.Empty;//流程ID
    public string strDepName = string.Empty;//部门名称
    public string strDepID = string.Empty;//部门key值

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindFlowName();
            BindDepList();
        }
    }
    /// <summary>
    /// 绑定所有流程名称
    /// </summary>
    public void BindFlowName()
    {
        DataTable dtTable = GetSelectOptions("");
        ddlControl.Items.Clear();
        foreach (DataRow row in dtTable.Rows)
        {
            ddlControl.Items.Add(new ListItem(row["KEYVALUE"].ToString(), row["KEYCODE"].ToString()));
        }
        //设置默认值
        strFlowName = ddlControl.SelectedItem.Text;
        strFlowID = ddlControl.SelectedItem.Value;
    }

    /// <summary>
    /// 获取所有流程名称
    /// </summary>
    /// <param name="strParamsName">流程名称</param>
    /// <returns></returns>
    /// <!--addby zhongjian 20091009-->
    public DataTable GetSelectOptions(string strParamsName)
    {
        string strSql = string.Empty;
        strSql = "select flowname keyvalue,id keycode from xt_workflow_define where ispub='1' and isdelete='0'";
        DataTable dtOut;
        SysParams.OAConnection().RunSql(strSql, out dtOut);
        return dtOut;
    }

    /// <summary>
    /// 绑定咨询部门
    /// </summary>
    public void BindDepList()
    {
        DataTable dtTable = RequestFlowOperation.GetDepList();
        ddlDep.Items.Clear();
        foreach (DataRow row in dtTable.Rows)
        {
            ddlDep.Items.Add(new ListItem(row["depart_name"].ToString(), row["departid"].ToString()));
        }
        //设置默认值
        strDepID = ddlDep.SelectedItem.Value;
        strDepName = ddlDep.SelectedItem.Text;
    }

    protected void ddlControl_SelectedIndexChanged(object sender, EventArgs e)
    {
        strFlowName = ddlControl.SelectedItem.Text;
        strFlowID = ddlControl.SelectedItem.Value;
    }

    protected void ddldep_SelectedIndexChanged(object sender, EventArgs e)
    {
        strDepID = ddlDep.SelectedItem.Value;
        strDepName = ddlDep.SelectedItem.Text;
    }

    public string GetFlowName()
    {
        return strFlowName;
    }

    public string GetFlowID()
    {
        return strFlowID;
    }

    public string GetDepID()
    {
        return strDepID;
    }

    public string GetDepName()
    {
        return strDepName;
    }

}
