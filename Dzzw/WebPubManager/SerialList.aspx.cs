﻿ 
// 创建人  ：zhongjian 
// 创建时间：2009年9月28日
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WF_Business;
using Business;

public partial class WebPubManager_SerialList : System.Web.UI.Page
{
    public string strFlowName = "";
    public string strFlowID = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindFlowName();
        }
    }
    /// <summary>
    /// 绑定所有流程名称
    /// </summary>
    public void BindFlowName()
    {
        DataTable dtTable = GetSelectOptions("");
        ddlControl.Items.Clear();
        foreach (DataRow row in dtTable.Rows)
        {
            ddlControl.Items.Add(new ListItem(row["KEYVALUE"].ToString(),row["KEYCODE"].ToString()));
        }
        //设置默认值
        strFlowName = ddlControl.SelectedItem.Text;
        strFlowID = ddlControl.SelectedItem.Value;
    }
    /// <summary>
    /// 获取所有流程名称
    /// </summary>
    /// <param name="strParamsName">流程名称</param>
    /// <returns></returns>
    /// <!--addby zhongjian 20091009-->
    public DataTable GetSelectOptions(string strParamsName)
    {
        string strSql = "select WNAME keyvalue,wid keycode from st_workflow t where rot=0";
        DataTable dtOut;
        SysParams.OAConnection().RunSql(strSql, out dtOut);
        return dtOut;
    }
    protected void ddlControl_SelectedIndexChanged(object sender, EventArgs e)
    {
        strFlowName = ddlControl.SelectedItem.Text;
        strFlowID = ddlControl.SelectedItem.Value;
    }
    public string GetFlowName()
    {
        return strFlowName;
    }
    public string GetFlowID()
    {
        return strFlowID;
    }
}
