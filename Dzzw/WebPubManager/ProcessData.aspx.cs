﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using Business;
using Business.Common;
using SbBusiness.Wsbs;

public partial class WebPubManager_ProcessData :PageBase
{
    private XtProcess cProcess = new XtProcess();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindProcessDataList();
            BindSouseDataList("0");
            BindDataList();
        }
    }

    /// <summary>
    /// 绑定表单字段内容(目标字段)
    /// </summary>
    /// <!--addby zhongjian 20091119-->
    protected void BindProcessDataList()
    {
        DataTable dtTemp = cProcess.GetProcessDataList(Request["id"]);
        string strText = string.Empty;//表单字段内容
        string strValue = string.Empty;//表单字段值
        lbProcessData.Items.Clear();
        for (int i = 0; i < dtTemp.Rows.Count; ++i)
        {
            strText = dtTemp.Rows[i]["name"].ToString();
            strValue = dtTemp.Rows[i]["db_field"].ToString();
            lbProcessData.Items.Add(new ListItem(strText, strValue));
            hdfTtableName.Value = dtTemp.Rows[i]["db_table"].ToString();
        }
    }

    /// <summary>
    /// 绑定用户字段内容(来源字段)
    /// </summary>
    /// <param name="strFlag">标识(0:用户表;1:部门表)</param>
    /// <!--addby zhongjian 20091119-->
    protected void BindSouseDataList(string strFlag)
    {
        DataTable dtTemp;
        string strText = string.Empty;//用户字段内容
        string strValue = string.Empty;//用户字段值
        string strSouseTable = string.Empty;//来源数据表名
        if (strFlag == "1")
            strSouseTable="SYS_UNITS";
        else
            strSouseTable = "SYS_USER";

        OracleSystemOperation osoTemp = new OracleSystemOperation();
        dtTemp = osoTemp.GetSouseDataList(strSouseTable);
        lbUserData.Items.Clear();
        for (int i = 0; i < dtTemp.Rows.Count; ++i)
        {
            strText = dtTemp.Rows[i]["comments"].ToString();
            strValue = dtTemp.Rows[i]["column_name"].ToString();
            lbUserData.Items.Add(new ListItem(strText, strValue));
            hdfStableName.Value = dtTemp.Rows[i]["table_name"].ToString();
        }
    }

    /// <summary>
    /// 绑定已配置数据信息
    /// </summary>
    /// <!--addby zhongjian 20091125-->
    protected void BindDataList()
    {
        DataTable dtTemp;
        string strText = string.Empty;//用户字段内容
        string strValue = string.Empty;//用户字段值

        XtFormDefaultOperation xfdoTemp = new XtFormDefaultOperation();
        dtTemp = xfdoTemp.GetDataList(Request["othername"].ToString(), Request["datatype"].ToString());
        lblDataList.Items.Clear();
        for (int i = 0; i < dtTemp.Rows.Count; ++i)
        {
            strText = dtTemp.Rows[i]["target_tabcol_name"].ToString() + "------" + dtTemp.Rows[i]["source_tabcol_comment"].ToString();
            strValue = dtTemp.Rows[i]["id"].ToString();
            lblDataList.Items.Add(new ListItem(strText, strValue));
        }
    }


    protected void btnRefer_Click(object sender, EventArgs e)
    {
        if (txtProcessData.Text.Trim() == "")
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "ok", "<script>window.alert('目标值不能为空！')</script>");
            return;
        }
        if (txtUserData.Text.Trim() == "")
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "ok", "<script>window.alert('来源值不能为空！')</script>");
            return;
        }
        //构建存储用户信息SQL
        if (ddlSouseData.SelectedValue == "0")
            hdfSql.Value = string.Format("select {0} {1} from {2} where 1=1 ", hdfScolName.Value, hdfTcolName.Value, hdfStableName.Value);

        //构建存储部门信息SQL
        if (ddlSouseData.SelectedValue == "1")
            hdfSql.Value = string.Format("select a.{0} {1} from SYS_UNITS a,sys_user b where a.id=b.unitid ", hdfScolName.Value, hdfTcolName.Value);

        XtFormDefaultOperation xfdoTemp = new XtFormDefaultOperation();
        xfdoTemp.InsertFormDefault(Request["datatype"], Request["flowid"], Request["othername"], hdfStableName.Value, hdfScolName.Value, txtUserData.Text, hdfTtableName.Value, hdfTcolName.Value, txtProcessData.Text, hdfSql.Value);
        BindDataList();
    }

    protected void btnReturn_Click(object sender, EventArgs e)
    {
        Response.Redirect("Process.aspx?flowid=" + Request["flowid"] + "&datatype=" + Request["datatype"]);
    }

    protected void lbProcessData_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtProcessData.Text = lbProcessData.SelectedItem.Text;
        hdfTcolName.Value = lbProcessData.SelectedItem.Value;
    }

    protected void lbUserData_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtUserData.Text = lbUserData.SelectedItem.Text;
        hdfScolName.Value = lbUserData.SelectedItem.Value;
    }

    protected void ddlSouseData_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindSouseDataList(ddlSouseData.SelectedValue);
    }

    protected void lblDataList_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        if (lblDataList.SelectedIndex >= 0)
        {
            XtFormDefaultOperation xfdoTemp = new XtFormDefaultOperation();

            for (int i = 0; i < lblDataList.Items.Count; ++i)
            {
                if (lblDataList.Items[i].Selected)
                {
                    //删除表单数据配置
                    xfdoTemp.DeleteData(lblDataList.Items[i].Value);
                }
            }
            BindDataList();
        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "ok", "<script>window.alert('请选择你要删除的表单数据配置！')</script>");
        }
    }
}
