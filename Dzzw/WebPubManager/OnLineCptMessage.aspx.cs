﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using SbBusiness.PubInfo;

public partial class WebPubManager_OnLineCptMessage : PageBase
{
    OnLineInfo OnLine = new OnLineInfo();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindCptMessage();
            if (Request["type"] != null && Request["delid"] != null)
            {
                if (Request["type"].ToString() == "delete")
                {
                    OnLine.DeleteComplaint(Request["delid"].ToString());
                }
            }
        }
        BindMessageFK();
    }
    /// <summary>
    /// 绑定投诉回馈信息
    /// </summary>
    /// <!--addby zhongjian 20091103-->
    protected void BindCptMessage()
    {
        string strID = Request["id"];
        DataTable dtTemp = OnLine.GetComplaintInfo(strID);
        if (dtTemp.Rows.Count > 0)
        {
            lblDep.Text = dtTemp.Rows[0]["tsbm"].ToString();
            lblType.Text = dtTemp.Rows[0]["tslx"].ToString();
            lblName.Text = dtTemp.Rows[0]["tsr"].ToString();
            lblData.Text = dtTemp.Rows[0]["tssj"].ToString();
            lblSerial.Text = dtTemp.Rows[0]["iid"].ToString();
            lblNotes.Text = dtTemp.Rows[0]["tsnr"].ToString();
        }
    }

    /// <summary>
    /// 绑定在线投诉回馈信息
    /// </summary>
    /// <!--addby zhongjian 20091103-->
    protected string BindMessageFK()
    {
        string strHTML = string.Empty;
        string strData = string.Empty;//回复时间
        string strNotes = string.Empty;//回复内容
        string strUserName = string.Empty;//回复人员名称
        string strUserID = string.Empty;//回复人员ID
        string strFKID = string.Empty;//回复ID
        string strID = Request["id"];
        DataTable dtTemp = OnLine.GetComplaintFKInfo(strID);
        if (dtTemp.Rows.Count > 0)
        {
            for (int i = 0; i < dtTemp.Rows.Count; ++i)
            {
                strData = dtTemp.Rows[i]["fk_date"].ToString();
                strUserName = dtTemp.Rows[i]["user_name"].ToString();
                strNotes = dtTemp.Rows[i]["fk_notes"].ToString();
                strUserID = dtTemp.Rows[i]["fk_userid"].ToString();
                strFKID = dtTemp.Rows[i]["id"].ToString();

                strHTML += "<div class='new_bar02' style='border:1px solid #89b9e2'><table width='98%' border='0' cellpadding='0' cellspacing='0'><tr>";
                strHTML += string.Format("<td align='left' style='width: 25%'><span style='font-weight:bold;'>回复时间：</span>{0}</td>", strData);
                strHTML += string.Format("<td align='left' style='width: 30%'><span style='font-weight:bold;'>回复人员：{0}</span></td>", strUserName);
                if (strUserID == Session["UserId"].ToString())
                {
                    strHTML += string.Format("<td align='right' style='width: 20%'><a href='OnLineCptMessage.aspx?id={0}&type=delete&delid={1}' onclick=\"return confirm('确定要删除吗？')\">删除</a></td>", strID, strFKID);
                }
                else
                {
                    strHTML += string.Format("<td align='right' style='width: 20%'></td>");

                }
                strHTML += "</tr></table></div>";
                strHTML += string.Format("<table width='100%' cellpadding='0' cellspacing='0'>");
                strHTML += string.Format("<tr><td><span style='font-weight:bold;'>回复内容：</span>{0}</td></tr></table>", strNotes);
            }
        }
        return strHTML;
    }

    protected void btnOK_Click(object sender, EventArgs e)
    {
        string strID = string.Empty;
        string strUserID = string.Empty;
        if (Request["id"] != null)
            strID = Request["id"].ToString();
        if (Session["UserId"] != null)
            strUserID = Session["UserId"].ToString();
        OnLine.InsertComplaint(strID, strUserID, txtNotesFk.Text.Trim());
        txtNotesFk.Text = "";
    }

}
