﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using SbBusiness.User;

public partial class WebPubManager_AddUserTypeName : System.Web.UI.Page
{
    private SysUserTypeHandle userOperation = new SysUserTypeHandle();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindData();
        }
    }

    private void BindData()
    {
        this.ddlSysName.DataSource = userOperation.GetSysName();
        this.ddlSysName.DataValueField = "系统名称";
        this.ddlSysName.DataBind();
    }

    protected void btnCommit_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(ddlSysName.Text))
        {
            Hashtable table = new Hashtable();
            table.Add("SYSCODE", userOperation.GetSysCodeBySysName(this.ddlSysName.Text));
            table.Add("USERTYPENAME", this.txtUserTypeName.Text);
            table.Add("SYSNAME", this.ddlSysName.Text);

            if (userOperation.InsertUserType(table))
            {
                Response.Write("<script language='javascript'>alert('插入成功');window.opener.location.href=window.opener.location.href;window.opener=null;window.close();</script>");
            }
            else
            {
                Response.Write("<script language='javascript'>alert('插入失败')</script>");
            }
        }
    }
}
