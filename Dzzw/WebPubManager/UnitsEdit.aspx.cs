﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Configuration;
using Business;
using SbBusiness.User;

public partial class WebPubManager_UnitsEdit :PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindUnitsInfo();
        }
    }

    /// <summary>
    /// 绑定单位信息
    /// </summary>
    /// <!--addby zhongjian 20100412-->
    protected void BindUnitsInfo()
    {
        if (!string.IsNullOrEmpty(Request["id"]))
        {
            DataTable dt = SysUserHandle.GetUnitsInfo(Request["id"]);

            if (dt.Rows.Count > 0)
            {
                txtUnitsname.Text = BasicOperate.GetDataBaseString(dt.Rows[0]["unitsname"]);
                txtUnitsTel.Text = BasicOperate.GetDataBaseString(dt.Rows[0]["unitstel"]);
                txtUnitsaddress.Text = BasicOperate.GetDataBaseString(dt.Rows[0]["unitsaddress"]);
                txtPostcode.Text = BasicOperate.GetDataBaseString(dt.Rows[0]["postcode"]);
                txtFax.Text = BasicOperate.GetDataBaseString(dt.Rows[0]["fax"]);
                txtlegal.Text = BasicOperate.GetDataBaseString(dt.Rows[0]["legal"]);
                txtOrganization.Text = BasicOperate.GetDataBaseString(dt.Rows[0]["organization"]);
                txtNature.Text = BasicOperate.GetDataBaseString(dt.Rows[0]["nature"]);
            }
        }
    }

    protected void btnRefer_Click(object sender, EventArgs e)
    {
        //获取操作者ip地址
        string myIP = Common.IPOperation.GetIpAddress();

        string strId = Request["id"];
        if (string.IsNullOrEmpty(strId))
        {
            SysUserHandle.AddUnitsInfo(txtUnitsname.Text, txtUnitsTel.Text, txtUnitsaddress.Text, txtFax.Text, txtPostcode.Text,
                txtlegal.Text, txtOrganization.Text, txtNature.Text);
        }
        else
        {
            SysUserHandle.UpdateUnitsInfo(strId, txtUnitsname.Text, txtUnitsTel.Text, txtUnitsaddress.Text, txtFax.Text, txtPostcode.Text,
                txtlegal.Text, txtOrganization.Text, txtNature.Text);

        }
        Response.Redirect("UnitsList.aspx");
    }

    protected void btnReturn_Click(object sender, EventArgs e)
    {
        Response.Redirect("UnitsList.aspx");
    }

   

}
