﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FlowLinkEdit.aspx.cs" Inherits="WebPubManager_FlowLinkEdit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>其它连接编辑</title>
    <link href="../css/page.css" rel="Stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        function getSrc() {
            var Src = window.location.protocol + "//" + window.location.host;
            document.getElementById("hidHref").value = Src;
            document.form1[0].submit();
        }    
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div style="text-align: center">
        <div class="box_top_bar">
            <div class="box_top_left">
                <div class="box_top_right">
                    <div class="box_top_text">
                        其它连接编辑</div>
                </div>
            </div>
        </div>
        <div class="box_middle3">
            <table width="80%" border="0" cellpadding="0" cellspacing="0" class="box_middle_tab">
                <tr>
                    <td>
                        流程名称：
                    </td>
                    <td style="text-align: left;">
                        <asp:Label ID="lblFlowName" Width="300px" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        连接标题：
                    </td>
                    <td style="text-align: left;">
                        <asp:TextBox ID="txtTitle" Width="300px" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        连接地址：
                    </td>
                    <td style="text-align: left;">
                        <asp:TextBox ID="txtLinkHref" Width="300px" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        图片名称：
                    </td>
                    <td style="text-align: left;">
                        <asp:TextBox ID="txtImgSrc" ReadOnly="true" Width="300px" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        上传图片：
                    </td>
                    <td style="text-align: left;">
                        <input id="FileImage" style="width: 307px; height: 22px;" runat="server" type="file" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Button ID="btnRefer" CssClass="input" runat="server" Text="提交" OnClick="btnRefer_Click" />
                        <asp:Button ID="btnReturn" CssClass="input" runat="server" Text="返回" OnClick="btnReturn_Click"
                            CausesValidation="False" />
                    </td>
                </tr>
            </table>
        </div>
        <input id="hidHref" type="hidden" runat="server" />
    </div>
    </form>
</body>
</html>
