﻿using SbBusiness;
using SbBusiness.Wsbs;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WebPubManager_JsydCrxxList : System.Web.UI.Page
{
    private JsydCrOperation jcoTemp = new JsydCrOperation();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DeleteJsydCrxx();
            GridviewDatabind();
        }
    }

    /// <summary>
    /// 删除单位信息
    /// </summary>
    /// <!--addby zhongjian 20100412-->
    private void DeleteJsydCrxx()
    {
        string strId = Request.Params["id"];
        if (Request.Params["action"] == "delete")
        {
            jcoTemp.DeleteJsydCrxx(strId);//删除单位
        }
    }

    #region 数据绑定
    /// <summary>
    /// 绑定单位信息
    /// </summary>
    /// <!--addby zhongjian 20100412-->
    private void GridviewDatabind()
    {
        string strWhere = string.Empty;
        if(!string.IsNullOrEmpty(txtDkbh.Text))
        {
            strWhere += string.Format(" and DK_BH like '%{0}%'",txtDkbh.Text);
        }
        if (!string.IsNullOrEmpty( txtDkzl.Text))
        {
            strWhere += string.Format(" and DK_ZL like '%{0}%'", txtDkzl.Text);
        }
        DataTable dtSource = jcoTemp.GetNewestDkxx(strWhere, "");

        gridviewCar.DataSource = dtSource;
        gridviewCar.RecordCount = dtSource.Rows.Count;
        gridviewCar.DataBind();
    }
    #endregion

    /// <summary>
    /// 出让信息发布
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnCrxxPub_Click(object sender, EventArgs e)
    {
        SerialInstance sioTemp = new SerialInstance();
        long nIID = sioTemp.GetIIDWithEnterprise("", "", "");
        Common.WindowAppear.ExecuteScript(this.Page, string.Format("CrxxPub('{0}')", nIID));
    }

    /// <summary>
    /// datagrid数据加载
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void recvgrid_OnLoadData(object sender, EventArgs e)
    {
        this.GridviewDatabind();
    }

    #region 行创建
    /// <summary>
    /// Handles the RowCreated event of the gridviewCar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
    protected void gridviewCar_RowCreated(object sender, GridViewRowEventArgs e)
    {
        switch (e.Row.RowType)
        {
            case DataControlRowType.DataRow:

                DataRowView drv = e.Row.DataItem as DataRowView;
                if (drv == null)
                {
                    break;
                }
                HyperLink hyedit = e.Row.FindControl("hyedit") as HyperLink;
                if (hyedit != null)
                {
                    hyedit.NavigateUrl = string.Format("CommonTabUpdate.aspx?iid={0}&tabname=工业用地出让信息", drv["iid"]);
                }
                HyperLink hydel = e.Row.FindControl("hydel") as HyperLink;
                if (hydel != null)
                {
                    hydel.Attributes.Add("OnClick", "return confirm('确定要删除该信息吗?')");
                    hydel.NavigateUrl = "JsydCrxxList.aspx?action=delete&id=" + drv["id"];
                }
                break;
        }
    }
    #endregion

    protected void btnRefer_Click(object sender, EventArgs e)
    {
        GridviewDatabind();
    }
}