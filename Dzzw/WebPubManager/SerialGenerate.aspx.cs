﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using WF_Business;
using Business;
using Business.Common;

public partial class WebPubManager_SerialGenerate : PageBase
{
    public int pagesize = 10;//页面记录数
    public int pagenow = 1;//当前页
    public int pagecount = 0;//总页数
    public string strHtml = "";
    public PageOperation mypage = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        //定义分页变量
        string mpage = Request["pagenow"];
        string strUserID = "";//用户ID
        string strUserName = "";//用户名称
        
        if (!string.IsNullOrEmpty(mpage))
        {
            pagenow = Int32.Parse(mpage); ;
            if (pagenow < 1)
                pagenow = 1;
        }

        string sql = string.Empty;
        DataTable rs1;

        string strFlag = string.Empty;

        if (Request.Params["flag"] != null)
        {
            strFlag = Request.Params["flag"];
        }
        if (Session["UserId"] != null)
        {
            strUserName = Session["UserName"].ToString();
            strUserID = Session["UserId"].ToString();
        }

        if (strFlag.CompareTo("ysp") == 0)//查看审批通过的用户
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "function", "<script>SetBackColor(0);</script>");

            sql = @"select a.id,a.userid,b.username,a.serial_name,a.notes,a.is_pass,b.id sysid  from sys_user_serial a left join  sys_user b on a.userid = b.userid where a.is_pass=1 and   serial_name not in (select flowname from xt_workflow_define a where a.ispub = '0')";
        }
        else if (strFlag.CompareTo("query") == 0)//查询标志
        {
            string strQueryUserName = Request["username"];

            sql = @"select a.id,a.userid,b.username,a.serial_name,a.notes,a.is_pass,b.id sysid   from sys_user_serial a left join  sys_user b on a.userid = b.userid where serial_name not in
       (select flowname from xt_workflow_define a where a.ispub = '0')";
            if (!string.IsNullOrEmpty(strQueryUserName))
            {
                sql += string.Format(" and b.username like '%{0}%'", strQueryUserName);
            }
        }
        else//默认
        {
            sql = @"select a.id,a.userid,b.username,a.serial_name,a.notes,a.is_pass,b.id sysid   from sys_user_serial a left join sys_user b on a.userid = b.userid where (a.is_pass=0 or a.is_pass is null) and serial_name not in (select flowname from xt_workflow_define a where a.ispub = '0') ";
        }

        if (strUserName != "管理员")//不是管理员时,应加上用户预审权限 addby zhongjian 20091009 
        {
            sql += string.Format(" and a.serial_name in (select wname from st_workflow where wid in( select gid from ST_USER_GROUP where userid='{0}'))", strUserID);
        }
        sql += " order by a.userid";

        try
        {
            SysParams.OAConnection().RunSql(sql, out rs1);
            mypage = new PageOperation(rs1, pagenow, pagesize);
            pagecount = mypage.PageCount;
            System.Collections.Generic.List<System.Data.DataRow> listwork = mypage.CurrentPageList;

            for (int i = 0; i < listwork.Count; i++)
            {
                DataRow myArray = listwork[i];
                //新增查看用户信息和申请理由 addby zhongjian 20090927
                string UserMessage = "";//用户信息
                string viewMessage = "";//申请理由
                string strMassage = myArray["notes"].ToString();
                if (strMassage.Length >= 20)
                {
                    strMassage = strMassage.Substring(0, 20) + "...";
                    myArray["notes"] = strMassage;
                }
                UserMessage = string.Format("<a href='#' onclick=\"UserMessage('{0}')\">{1}</a>", myArray["sysid"].ToString(), myArray["userid"]);
                viewMessage = string.Format("<a href='#' onclick=\"ViewMessage('{0}')\">{1}</a>", myArray["id"].ToString(), strMassage);

                strHtml+="<tr bgcolor=\"#ffffff\" onmouseover=\"this.style.backgroundColor='#caf7fd'\" onMouseOut=\"this.style.backgroundColor='#ffffff'\">\r\n";

                strHtml+="<td align=\"center\">" + UserMessage + "</td>\r\n";
                strHtml+="<td align=\"center\">" + myArray["username"] + "</td>\r\n";
                strHtml+="<td align=\"center\">" + myArray["serial_name"] + "</td>\r\n";
                strHtml+="<td align=\"center\">" + viewMessage + "</td>\r\n";//新增显示申请理由 addby zhongjian20090924

                string strUserFlag = myArray["is_pass"].ToString();

                if (strFlag.CompareTo("ysp") == 0)//查询已审批
                {
                    strHtml+="<td style='background-color:Green'>已通过</td>\r\n";
                }
                else if (strFlag.CompareTo("query") == 0)//查询标志
                {
                    if (!string.IsNullOrEmpty(strUserFlag))
                    {
                        if (strUserFlag.CompareTo("1") == 0)
                        {
                            strHtml+="<td  style='background-color:Green'>已通过</td>\r\n";
                        }
                        else
                        {
                            strHtml+="<td  style='background-color:red'>未通过</td>\r\n";
                        }
                    }
                    else
                    {
                        strHtml+="<td ><a href='#' onclick='SubmitClient(\"" + myArray["id"] + "\",1,\"" + myArray["serial_name"] + "\")'>允许通过</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href='#' onclick='SubmitClient(\"" + myArray["id"] + "\",0,\"" + myArray["serial_name"] + "\")'>删除</a></td>\r\n";
                    }
                }
                else//默认
                {
                    strHtml+="<td ><a href='#' onclick='SubmitClient(\"" + myArray["id"] + "\",1,\"" + myArray["serial_name"] + "\")'>允许通过</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href='#' onclick='SubmitClient(\"" + myArray["id"] + "\",0,\"" + myArray["serial_name"] + "\")'>删除</a></td>\r\n";
                }

                strHtml+="</tr>\r\n";
            }
        }
        catch (System.Exception ex)
        {
            Common.WindowAppear.WriteAlert(Page, ex.Message);
        }
	
    }
}
