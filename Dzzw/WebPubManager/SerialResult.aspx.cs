﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WF_Business;
using Business;

public partial class WebPubManager_SerialResult : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string strId = Request.Params["sid"];
            string strResult = Request.Params["resultFlag"];
            string strFlowName = Request.Params["flowname"];
            UpdateUserFlag(strId, strResult, strFlowName);
        }
    }

    /// <summary>
    /// 审批通过业务申请的用户
    /// </summary>
    /// <param name="strSysId">办理权限申请id</param>
    /// <param name="strResult">审核状态位</param>
    /// <param name="strFlowName">流程名称</param>
    private void UpdateUserFlag(string strSysId, string strResult, string strFlowName)
    {
        try
        {
            string sql = string.Empty;
            string sqluser = string.Empty;

            if (strResult.CompareTo("1") == 0)//通过
            {
                sql = string.Format(@"update sys_user_serial t set is_pass='1' where ID='{0}' ",
                    strSysId);
                SysParams.OAConnection().RunSql(sql);

                int nType = Convert.ToInt32(ConfigurationManager.AppSettings["bsqxspSystemFlag"]);
                sqluser = string.Format(@"update sys_user set type =" + nType + 
                    "where userid = (select userid from Sys_User_Serial where id ='{0}') ", strSysId);
                SysParams.OAConnection().RunSql(sqluser);

                //获取用户信息
                sqluser = string.Format(@"select userid,username,mobile from sys_user where userid = (select userid from Sys_User_Serial where id ='{0}')",
                    strSysId);

                DataTable dtReturn = new DataTable();
                SysParams.OAConnection().RunSql(sqluser,out dtReturn);
                string strUserName = "", strMobile = "", strUserID = "";
                if (dtReturn.Rows.Count > 0)
                {
                    strUserName = dtReturn.Rows[0]["username"].ToString();
                    strMobile = dtReturn.Rows[0]["mobile"].ToString();
                    strUserID = dtReturn.Rows[0]["userid"].ToString(); //添加用户ID 
                }

                //sqluser = string.Format(@"select serial_name from sys_user_serial where id ='{0}'",strSysId);
                //string strName = SysParams.OAConnection().GetValue(sqluser);

                //流程别名
                sqluser = string.Format(@"select flowtype from xt_workflow_define where ispub=1 and isdelete=0 and flowname='{0}'",
                    strFlowName);
                string strFlowType = SysParams.OAConnection().GetValue(sqluser);

                //获取消息内容
                string strStepMsg = "", strStepN0 = "";
                sqluser = string.Format(@"select step_msg,step_no from xt_request_step where step_name='权限开通'",
                    strSysId);
                SysParams.OAConnection().RunSql(sqluser,out dtReturn);
                if (dtReturn.Rows.Count > 0)
                {
                    strStepMsg = dtReturn.Rows[0]["step_msg"].ToString();
                    //获取信息步骤码
                    strStepN0 = dtReturn.Rows[0]["step_no"].ToString();
                }
                

                if (!string.IsNullOrEmpty(strStepMsg))//当消息内容设置为空时，将不再发送消息。update by zhongjian 20091229
                {
                    //整理消息内容:strStepMsg中{0}:代表用户名称;{1}:代表事项名称; addby zhongjian 20091207
                    try
                    {
                        strStepMsg = string.Format(strStepMsg, strUserName, strFlowType);
                    }
                    catch { }
                    sql = string.Format(@"Insert into xt_messagebox(MESSAGETEXT,PHONENO,USERNAME,USERID) 
                    values ('{2}','{1}','{0}','{3}')",//{0},您好，您的{2}业务申报权限已被审批通过
                        strUserName, strMobile, strStepMsg, strUserID);
                    SysParams.OAConnection().RunSql(sql);

                }
                //修改用户表信息提示步骤 addby zhongjian 20091122
                sql = string.Format(@"update sys_user set stepno='{0}' where userid='{1}'", strStepN0,strUserID);
                SysParams.OAConnection().RunSql(sql);
            }
            else
            {
                sql = string.Format(@"delete from sys_user_serial where ID='{0}' ", strSysId); ;
                SysParams.OAConnection().RunSql(sql);
            }

            //保存短信信息

            string strScript = "<script>this.location = 'SerialGenerate.aspx';</script>";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "scriptBack", strScript);
        }
        catch (Exception e)
        {
            Common.WindowAppear.WriteAlert(Page, e.Message);
        }
    }    
}
