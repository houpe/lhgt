﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Business;

using Business.Common;
using System.Text;
using SbBusiness.User;

public partial class WebPubManager_SerialManager : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Databinder();
            BindSerialAtt();
        }
    }

    /// <summary>
    /// 绑定权限申请信息
    /// </summary>
    /// <!-- addby zhongjian 20090923-->
    private void Databinder()
    {
        DataTable dtTemp = new DataTable();
        string SerialID = Request["id"];

        SysUserRightRequest urrTemp = new SysUserRightRequest();
        dtTemp = urrTemp.GetUserSerial(SerialID);
        if (dtTemp.Rows.Count > 0)
        {
            lblUserID.Text = dtTemp.Rows[0]["userid"].ToString();
            lblSerialTime.Text = dtTemp.Rows[0]["serial_time"].ToString();
            lblUserName.Text = dtTemp.Rows[0]["username"].ToString();
            lblSerialName.Text = dtTemp.Rows[0]["serial_name"].ToString();
            txtContents.Text = dtTemp.Rows[0]["notes"].ToString();
        }
    }

    /// <summary>
    /// 绑定流程权限申请时的附件信息
    /// </summary>
    /// <!--addby zhongjian 20091221-->
    private void BindSerialAtt()
    {
        if (!string.IsNullOrEmpty(Request["id"]))
        {
            string strFileName = string.Empty;
            string strID = string.Empty;
            string strFileType = string.Empty;
            string strPath = string.Empty;
            StringBuilder strFileAtt = new StringBuilder();

            SysUserRightRequest urrTemp = new SysUserRightRequest();
            DataTable dtSource = urrTemp.GetSerialAtt(Request["id"]);
            if (dtSource.Rows.Count > 0)
            {
                for (int i = 0; i < dtSource.Rows.Count; ++i)
                {
                    strFileName = dtSource.Rows[i]["filename"].ToString();
                    strFileType = dtSource.Rows[i]["filetype"].ToString();
                    strID = dtSource.Rows[i]["id"].ToString();
                    strFileName = strFileName + "." + strFileType + ";";
                    strPath = string.Format("<a href='AppearOtherResouce.aspx?type=SerialAtt&id={0}'>{1}</a>&nbsp;&nbsp;", strID, strFileName);
                    strFileAtt.Append(strPath);
                }
                lblFileAtt.Text = strFileAtt.ToString();
            }
        }
    }
}
