﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using SbBusiness.PubInfo;

public partial class WebPubManager_OnLineFeedback : System.Web.UI.Page
{
    private OnLineInfo OnLine = new OnLineInfo();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindFeedback();
        }
    }

    /// <summary>
    /// 绑定服务质量反馈信息
    /// </summary>
    /// <!--addby zhongjian 20091103-->
    protected void BindFeedback()
    {
        DataTable dtTemp = OnLine.GetFeedbackInfo("");
        this.gvFeedback.DataSource = dtTemp;
        this.gvFeedback.RecordCount = dtTemp.Rows.Count;
        this.gvFeedback.DataBind();
    }

    /// <summary>
    /// datagrid数据加载
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void recvgrid_OnLoadData(object sender, EventArgs e)
    {
        this.BindFeedback();
    }

    protected void lbtnDelete_Click(object sender, EventArgs e)
    {
        LinkButton lbtnDelete = (LinkButton)sender;
        string strID = lbtnDelete.CommandArgument.ToString();
        OnLine.DeleteFwfk(strID);
        BindFeedback();
    }
}
