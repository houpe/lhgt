﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Business;
using Business.FlowOperation;

public partial class WebPubManager_FlowLink : PageBase
{
    private WorkFlowPubSetting wfpsTemp = new WorkFlowPubSetting();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request["type"] != null && Request["id"] != null)//删除连接信息
            {
                if (Request["type"].ToString() == "delete")
                {
                    wfpsTemp.DeleteFlowLink(Request["id"]);
                }
            }
            BindFlowLink();
        }
    }

    /// <summary>
    /// 绑定连接信息
    /// </summary>
    /// <!--addby zhongjian 20100310-->
    private void BindFlowLink()
    {
        DataTable ds = wfpsTemp.GetFlowLink("", Request["flowid"]);
        if (ds != null)
        {
            gvFlows.DataSource = ds;
            gvFlows.RecordCount = ds.Rows.Count;
            gvFlows.DataBind();
        }
    }

    /// <summary>
    /// datagrid数据加载
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void recvgrid_OnLoadData(object sender, EventArgs e)
    {
        this.BindFlowLink();
    }

    #region 行创建
    /// <summary>
    /// Handles the RowCreated event of the gvFlows control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
    protected void gridviewCar_RowCreated(object sender, GridViewRowEventArgs e)
    {
        switch (e.Row.RowType)
        {
            case DataControlRowType.DataRow:
                DataRowView drv = e.Row.DataItem as DataRowView;
                if (drv == null)
                {
                    break;
                }
                break;
        }
    }
    #endregion

    protected string GetImage(IDataItemContainer row)
    {
        string strHtml = string.Empty;
        string strLinkHref = DataBinder.Eval(row.DataItem, "LINKEHREF").ToString();
        string strTitle = DataBinder.Eval(row.DataItem, "title").ToString();
        string strImgSrc = DataBinder.Eval(row.DataItem, "imgsrc").ToString();
        if (!string.IsNullOrEmpty(strImgSrc))
        {
            //strImgSrc = Server.MapPath("~/image/uploadimg/" + strImgSrc);
            strHtml += string.Format(@"<a href='{0}' target='_blank'><img alt='{1}' style='border:0' src='{2}' /></a>",
                                        strLinkHref, strTitle,strImgSrc);
        }

        return strHtml;
    }
}
