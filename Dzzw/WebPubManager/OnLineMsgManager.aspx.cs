﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using WF_Business;
using Business;
using SbBusiness.PubInfo;
using Business.FlowOperation;

public partial class WebPubManager_OnLineMsgManager :PageBase
{
    private OnLineInfo OnLine = new OnLineInfo();
    public string strFlowName = string.Empty;//流程名称
    public string strFlowID = string.Empty;//流程ID
    public string strDepName = string.Empty;//部门名称
    public string strDepID = string.Empty;//部门key值
    public string strMsgType = string.Empty;//咨询类型
    public string strID = string.Empty;//留言ID
    public string strUserID = string.Empty;//留言ID

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindMessage();
            BindFlowName();
            BindDepList();
            if (Request["type"] != null && Request["delid"]!=null)
            {
                if (Request["type"].ToString() == "delete")
                {
                    OnLine.DeleteMessage(Request["delid"].ToString());
                }
            }
            BindAdmin();
        }
        BindMessageFK();
    }

    /// <summary>
    /// 绑定留言咨询信息
    /// </summary>
    /// <!--addby zhongjian 20091103-->
    protected void BindMessage()
    {
        strID = Request["id"];
        DataTable dtTemp = OnLine.GetMessageInfo("",strID,"","","","",false);
        if (dtTemp.Rows.Count > 0)
        {
            lblData.Text = dtTemp.Rows[0]["createdate"].ToString();
            lblNotes.Text = dtTemp.Rows[0]["notes"].ToString();
            lblTitle.Text = dtTemp.Rows[0]["zt"].ToString();

            ViewState["DepName"] = dtTemp.Rows[0]["slbmname"].ToString();
            ViewState["FlowName"] = dtTemp.Rows[0]["flow"].ToString(); 

            //添加咨询类型判断 addby zhongjian 20091118
            strMsgType = dtTemp.Rows[0]["msgtype"].ToString();
            if (strMsgType == "0")//咨询类型(0:咨询事项,1:咨询部门)
            {
                ddlControl.Visible = true;
                ddlDep.Visible = false;
            }
            else
            {
                ddlControl.Visible = false;
                ddlDep.Visible = true;
            }

            //添加显示类型判断 addby zhongjian 20091204
            //显示类型(1:已显示,0:未显示)
            hfShowType.Value = dtTemp.Rows[0]["msshowtype"].ToString();
        }
    }

    /// <summary>
    /// 绑定留言咨询回馈信息
    /// </summary>
    /// <!--addby zhongjian 20091103-->
    protected string BindMessageFK()
    {
        string strHTML = string.Empty;
        string strData=string.Empty;//回复时间
        string strNotes=string.Empty;//回复内容
        string strUserName=string.Empty;//回复人员名称
        string strUserID = string.Empty;//回复人员ID
        string strFKID = string.Empty;//回复ID
        strID = Request["id"];
        DataTable dtTemp = OnLine.GetMessageFKInfo(strID);
        if (dtTemp.Rows.Count > 0)
        {
            for (int i = 0; i < dtTemp.Rows.Count; ++i)
            {
                strData = dtTemp.Rows[i]["fk_date"].ToString();
                strUserName = dtTemp.Rows[i]["user_name"].ToString();
                strNotes = dtTemp.Rows[i]["fk_notes"].ToString();
                strUserID = dtTemp.Rows[i]["fk_userid"].ToString();
                strFKID = dtTemp.Rows[i]["id"].ToString();

                strHTML += "<div class='new_bar02' style='border:1px solid #89b9e2'><table width='98%' border='0' cellpadding='0' cellspacing='0'><tr>";
                strHTML += string.Format("<td align='left' style='width: 25%'><span style='font-weight:bold;'>回复时间：</span>{0}</td>", strData);
                strHTML += string.Format("<td align='left' style='width: 30%'><span style='font-weight:bold;'>回复人员：{0}</span></td>", strUserName);
                if (strUserID == Session["UserId"].ToString())
                {
                    strHTML += string.Format("<td align='right'  valign='middle' style='width: 20%'><a href='OnLineMsgManager.aspx?id={0}&type=delete&delid={1}' onclick=\"return confirm('确定要删除吗？')\">删除</a></td>", strID, strFKID);
                }
                else
                {
                    strHTML += string.Format("<td align='right' style='width: 20%'></td>");

                }
                strHTML += "</tr></table></div>";
                strHTML += string.Format("<table width='100%' border='0' cellpadding='0' cellspacing='0'>");                
                strHTML += string.Format("<tr><td><span style='font-weight:bold;'>回复内容：</span>{0}</td></tr></table>", strNotes);
            }
        }
        return strHTML;
    }

    protected void btnOK_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtNotesFk.Text.Trim()))
        {
            if (Request["id"] != null)
                strID = Request["id"].ToString();
            if (Session["UserId"] != null)
                strUserID = Session["UserId"].ToString();
            OnLine.InsertMessage(strID, strUserID, txtNotesFk.Text.Trim());
            txtNotesFk.Text = "";

        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "", " <script> alert('请认真回复留言咨询！'); </script>");
        }
    }

    /// <summary>
    /// 绑定所有流程名称
    /// </summary>
    /// <!--addby zhongjian 20091118-->
    public void BindFlowName()
    {
        DataTable dtTable = GetSelectOptions("");
        ddlControl.Items.Clear();
        ddlControl.Items.Add(new ListItem("请选择事项流程...", "0"));
        foreach (DataRow row in dtTable.Rows)
        {
            ddlControl.Items.Add(new ListItem(row["KEYVALUE"].ToString(), row["KEYCODE"].ToString()));
        }
        //设置默认值
        strFlowName = ddlControl.SelectedItem.Text;
        strFlowID = ddlControl.SelectedItem.Value;
    }

    /// <summary>
    /// 获取所有流程名称
    /// </summary>
    /// <param name="strParamsName">流程名称</param>
    /// <returns></returns>
    /// <!--addby zhongjian 20091118-->
    public DataTable GetSelectOptions(string strParamsName)
    {
        string strSql = string.Empty;
        strSql = "select flowname keyvalue,id keycode from xt_workflow_define where ispub='1' and isdelete='0'";
        DataTable dtOut;
        SysParams.OAConnection().RunSql(strSql, out dtOut);
        return dtOut;
    }

    /// <summary>
    /// 绑定咨询部门
    /// </summary>
    /// <!--addby zhongjian 20091118-->
    public void BindDepList()
    {
        DataTable dtTable = RequestFlowOperation.GetDepList();
        ddlDep.Items.Clear();
        ddlDep.Items.Add(new ListItem("请选择审批部门...", "0"));
        foreach (DataRow row in dtTable.Rows)
        {
            ddlDep.Items.Add(new ListItem(row["depart_name"].ToString(), row["departid"].ToString()));
        }
        //设置默认值
        strDepID = ddlDep.SelectedItem.Value;
        strDepName = ddlDep.SelectedItem.Text;
    }

    protected void ddlControl_SelectedIndexChanged(object sender, EventArgs e)
    {
        strFlowName = ddlControl.SelectedItem.Text;
        strFlowID = ddlControl.SelectedItem.Value;
    }

    protected void ddldep_SelectedIndexChanged(object sender, EventArgs e)
    {
        strDepID = ddlDep.SelectedItem.Value;
        strDepName = ddlDep.SelectedItem.Text;
    }

    /// <summary>
    /// 留言咨询转发
    /// </summary>
    /// <!--addby zhongjian 20091118-->
    public void SetForward()
    {
        string strReturn = "1";
        if (Request["id"] != null)
            strID = Request["id"].ToString();
        if (ddlControl.Visible)
        {
            strFlowName = ddlControl.SelectedItem.Text;
            strFlowID = ddlControl.SelectedItem.Value;
            if (strFlowID == "0" || strFlowID=="")
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "", " <script> alert('请选择你要转发的事项流程！'); </script>");
                strReturn = "0";
            }
            if (strFlowName == ViewState["FlowName"].ToString())
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "", " <script> alert('请选择其它事项流程！'); </script>");
                strReturn = "0";
            }
        }
        else
        {
            strDepID = ddlDep.SelectedItem.Value;
            strDepName = ddlDep.SelectedItem.Text;
            if (strDepID == "0" || strDepID == "")
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "", " <script> alert('请选择你要转发的审批部门！'); </script>");
                strReturn = "0";
            }
            if (strDepName == ViewState["DepName"].ToString())
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "", " <script> alert('请选择其它审批部门！'); </script>");
                strReturn = "0";
            }
        }
        if (strReturn == "1")
        {
            OnLine.UpdateMessage(strID, ddlControl.Visible, strFlowName, strDepID);
            this.Response.Redirect("OnLineMessage.aspx");
        }
    }

    protected void lbtnForward_Click(object sender, EventArgs e)
    {
        SetForward();
    }

    /// <summary>
    /// 审核通过
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbtnOK_Click(object sender, EventArgs e)
    {
        string strID = string.Empty;
        if (Request["id"] != null)
            strID = Request["id"].ToString();
        SetMsgShow(strID);
        lbtnOK.Visible = false;
        lbtnCancel.Visible = true;
    }

    /// <summary>
    /// 撤消通过
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbtnCancel_Click(object sender, EventArgs e)
    {
        string strID = string.Empty;
        if (Request["id"] != null)
            strID = Request["id"].ToString();
        OnLine.SetMsgShow(strID, "0");
        lbtnOK.Visible = true;
        lbtnCancel.Visible = false;
    }

    /// <summary>
    /// 留言管理员审核
    /// </summary>
    /// <param name="strID">留言ID</param>
    /// <!--addby zhongjian 20091202-->
    public void SetMsgShow(string strID)
    {
        OnLine.SetMsgShow(strID,"1");
    }

    /// <summary>
    /// 判断是否留言咨询管理员
    /// </summary>
    /// <!--addby zhongjian 20091202-->
    public void BindAdmin()
    {
        string strAdminType = string.Empty;
        if (Request["AdminType"] != null)
        {
            strAdminType = Request["AdminType"].ToString();
            if (strAdminType == "1")//为管理员时
            {
                if (hfShowType.Value == "0" || hfShowType.Value == "")
                {
                    lbtnOK.Visible = true;
                    lbtnCancel.Visible = false;
                }
                else
                {
                    lbtnOK.Visible = false;
                    lbtnCancel.Visible = true;
                }
            }
            else//非管理员都不显示
            {
                lbtnOK.Visible = false;
                lbtnCancel.Visible = false;
            }
        }
    }

    /// <summary>
    /// 获取流程名称
    /// </summary>
    /// <returns></returns>
    public string GetFlowName()
    {
        return strFlowName;
    }

    /// <summary>
    /// 获取流程ID
    /// </summary>
    /// <returns></returns>
    public string GetFlowID()
    {
        return strFlowID;
    }

    /// <summary>
    /// 获取部门KEY值
    /// </summary>
    /// <returns></returns>
    public string GetDepID()
    {
        return strDepID;
    }

    /// <summary>
    /// 获取部门名称
    /// </summary>
    /// <returns></returns>
    public string GetDepName()
    {
        return strDepName;
    }

    /// <summary>
    /// 获取咨询类型
    /// </summary>
    /// <returns></returns>
    public string GetMsgType()
    {
        return strMsgType;
    }
    
}
