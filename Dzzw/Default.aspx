﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>电子政务系统登录</title>
    <style type="text/css">
        td
        {
            font-size: 12px;
            color: #007AB5;
        }
        input
        {
            border: 0px;
            height: 26px;
            color: #007AB5;
        }
    </style>

    <script src="ScriptFile/jquery.js" type="text/javascript"></script>
    <script src="ScriptFile/loginValide.js" type="text/javascript"></script>

    <script type="text/javascript">
        function EnterKeyClick() {
            if (event.keyCode == 13) {
                event.keyCode = 9;
                event.returnValue = false;
                var btnLogin = document.getElementById('btnLogin');
                btnLogin.click();
            }
        }
    </script>
</head>
<body bgcolor="#9CDCF9"  onkeydown="EnterKeyClick()">
    <form id="form1" runat="server">
    <table width="681" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-top: 200px">
        <tr>
            <td width="353" height="259" align="center" valign="bottom" background="images/login/login_1.gif">
                <table width="90%" border="0" cellspacing="3" cellpadding="0">
                    <tr>
                        <td align="right" valign="bottom" style="color: #05B8E4">
                        </td>
                    </tr>
                </table>
            </td>
            <td width="195" background="images/login/login_2.gif">
                <table width="100%" height="106" border="0" align="center" cellpadding="2" cellspacing="0">
                    <tr>
                        <td height="50" colspan="2" align="left">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td height="30" align="left">
                            用户名称
                        </td>
                        <td>
                            <input id="username" type="text" style="background: url(images/login/login_6.gif) repeat-x;
                                border: solid 1px #27B3FE; height: 18px; background-color: #FFFFFF" size="17" />
                        </td>
                    </tr>
                    <tr>
                        <td height="30" align="left">
                            登陆密码
                        </td>
                        <td>
                            <input id="password" type="password" style="background: url(images/login/login_6.gif) repeat-x;
                                border: solid 1px #27B3FE; height: 18px; background-color: #FFFFFF" size="17" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            验证码：
                        </td>
                        <td>
                            <asp:TextBox ID="txtCheckCode" runat="server" Style="background: url(images/login/login_6.gif) repeat-x;
                                border: solid 1px #27B3FE; height: 18px; width: 50px; background-color: #FFFFFF"></asp:TextBox><img
                                    alt="img" style="height: 18px; vertical-align: middle" src="CheckCode.aspx" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <input type="button" style="background: url(images/login/login_5.gif) no-repeat"
                                id="btnLogin" value=" 登  陆 " onclick="validate();">
                            <input type="reset" name="Submit" style="background: url(images/login/login_5.gif) no-repeat"
                                value=" 重  置 ">
                        </td>
                    </tr>
                </table>
            </td>
            <td width="133" background="images/login/login_3.gif">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td height="161" colspan="3" background="images/login/login_4.gif">
            </td>
        </tr>
    </table>

    </form>
</body>
</html>
