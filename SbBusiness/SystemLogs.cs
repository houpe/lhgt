﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Text.RegularExpressions;
using Common;
using System.Net;
using Business;
using WF_Business;

namespace SbBusiness
{
    /// <summary>
    /// 日志操作类
    /// </summary>
    public class SystemLogs
    {
        /// <summary>
        /// 添加操作日志
        /// </summary>
        /// <param name="strUserID">操作人</param>
        /// <param name="strOptype">操作类型(add,update,delete)</param>
        /// <param name="strRemark">操作描述</param>
        /// <param name="strOpSql">操作SQL语句</param>
        public static void AddSystemLogs(string strUserID, string strOptype,string strRemark,string strOpSql)
        {
            //添加操作日志
            string strSql = string.Empty;
            string myIP = string.Empty;
            strOpSql = strOpSql.Replace("'", "");
            try
            {
                System.Net.IPAddress[] addressList = Dns.GetHostByName(Dns.GetHostName()).AddressList;
                //获取登录者ip地址
                myIP = addressList[0].ToString();
                strSql = string.Format(@"insert into xt_operationlogs (USERID,OPTTYPE,REMARK,IP,USERMEMO) values 
                                    ('{0}','{1}','{2}','{3}','{4}')", strUserID, strOptype, strRemark, myIP, strOpSql);

                SysParams.OAConnection().RunSql(strSql);
            }
            catch (Exception ex)
            {
                strRemark = " 外网在写入操作日志时发生错误: " + ex.ToString();

                Common.Log.ILogger ilogger = Common.Log.LoggerFactory.GetLogger();
                ilogger.WriteInfo("OfficeWeb", "ShowError", strRemark);
            }
        }

        /// <summary>
        /// 添加系统日志
        /// </summary>
        /// <param name="czy"></param>
        /// <param name="czdz"></param>
        /// <param name="czsjid"></param>
        /// <param name="strIp">Ip地址</param>
        public void Inputlog(string czy, string czdz, string czsjid, string strIp)
        {
            string strSql = string.Format(@"insert into sys_log(czy,czrq,czdz,czsjid,ip) 
                values('{0}',sysdate,'{1}','{2}','{3}')", czy, czdz, czsjid, strIp);

            SysParams.OAConnection().RunSql(strSql);
        }

        /// <summary>
        /// 添加系统日志
        /// </summary>
        /// <param name="czy"></param>
        /// <param name="czdz"></param>
        /// <param name="czsjid"></param>
        public void Inputlog(string czy, string czdz, string czsjid)
        {
            Inputlog(czy, czdz, czsjid, string.Empty);
        }

        ///<summary>
        /// 获取日志
        ///</summary>
        public DataTable GetLog()
        {
            return GetLog(string.Empty, string.Empty);
        }

        /// <summary>
        /// 获取时间段内的操作日志
        /// </summary>
        /// <param name="startData"></param>
        /// <param name="endData"></param>
        /// <returns></returns>
        public DataTable GetLog(string startData, string endData)
        {
            string strSql = @"select t.id,z.username 操作员,t.czrq 操作日期,t.czdz 操作动作,
                t.czsjid 操作信息ID from wy_log t,sys_user z where t.czy=z.userid";
            if (!string.IsNullOrEmpty(startData))
            {
                strSql += string.Format(" and czrq>=to_date('{0}','yyyy-MM-dd')", startData);
            }
            if (!string.IsNullOrEmpty(endData))
            {
                strSql += string.Format(" and czrq<to_date('{0}','yyyy-MM-dd')", endData);
            }
            strSql += " order by czrq desc ";

            DataTable dtTemp = new DataTable();
            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }

    }
}
