﻿using Business;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using WF_Business;

namespace SbBusiness.Wsbs
{
    public class XtFormDefaultOperation
    {
        /// <summary>
        /// 获取已经配置的数据信息
        /// </summary>
        /// <param name="strOtherName">表单别名</param>
        /// <returns></returns>
        /// <!--addby zhongjian 20091125-->
        public DataTable GetDataList(string strOtherName, string strFlowName)
        {
            DataTable dtTemp;
            string strSql = string.Format(@" select id,flowname,flowid,flowothername,source_tab_name,source_tabcol_name,source_tabcol_comment,
                            target_tab_name,target_tabcol_name,target_tabcol_comment,LOGIC_SQL from xt_form_default 
                            where flowothername='{0}' and flowname='{1}'", strOtherName, strFlowName);
            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }


        /// <summary>
        /// 获取表单初始设定
        /// </summary>
        /// <param name="strFlowName">流程名称</param>
        /// <param name="strOtherName">表单别名</param>
        /// <returns></returns>
        /// <!--addby zhongjian 20091120-->
        public DataTable GetFlowDefault(string strFlowName, string strOtherName)
        {
            string strSql = string.Format(@" select id,can_sync,sync_type,flowname,flowid,source_tab_name,source_tabcol_name,source_tabcol_comment,target_tab_name,target_tabcol_name,target_tabcol_comment,logic_sql,flowothername 
                            from xt_form_default where 1=1 ");
            if (!string.IsNullOrEmpty(strFlowName))
                strSql += string.Format(" and flowname='{0}'", strFlowName);
            if (!string.IsNullOrEmpty(strOtherName))
                strSql += string.Format(" and flowothername='{0}'", strOtherName);

            DataTable dtTemp;
            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }

        /// <summary>
        /// 添加初始化值设置内容
        /// </summary>
        /// <param name="strFlowName">流程名</param>
        /// <param name="strFlowID">对应xt_workflow_define表的id</param>
        /// <param name="strOtherName">表单别名</param>
        /// <param name="strSTableName">源表名</param>
        /// <param name="strSColName">源表列名</param>
        /// <param name="strSValue">源表列注释名</param>
        /// <param name="strTTableName">目标表名</param>
        /// <param name="strTColName">目标表列名</param>
        /// <param name="strTValue">目标表列注释名</param>
        /// <param name="strSqlData">构建提取数据SQL</param>
        /// <!--addby zhongjian 20091119-->
        public void InsertFormDefault(string strFlowName, string strFlowID, string strOtherName, string strSTableName, string strSColName, string strSValue,
            string strTTableName, string strTColName, string strTValue, string strSqlData)
        {
            string strSql = string.Empty;
            string strID = string.Empty;
            //判断该配置是否存在
            strSql = string.Format(@"select id from xt_form_default where flowid='{0}' and flowothername='{1}' and target_tabcol_name ='{2}' "
                                 , strFlowID, strOtherName, strTColName);
            strID = SysParams.OAConnection().GetValue(strSql);
            if (string.IsNullOrEmpty(strID))
            {
                //添加数据配置
                strSql = string.Format(@"insert into xt_form_default(flowname,flowid,flowothername,source_tab_name,source_tabcol_name,source_tabcol_comment,
                            target_tab_name,target_tabcol_name,target_tabcol_comment,LOGIC_SQL ) VALUES ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}')"
                    , strFlowName, strFlowID, strOtherName, strSTableName, strSColName, strSValue, strTTableName, strTColName, strTValue, strSqlData);
                SysParams.OAConnection().RunSql(strSql);
            }
            else
            {
                //修改数据配置
                strSql = string.Format(@"update xt_form_default set flowname='{0}',flowid='{1}',flowothername='{2}',source_tab_name='{3}',
                            source_tabcol_name='{4}',source_tabcol_comment='{5}',target_tab_name='{6}',target_tabcol_name='{7}',
                            target_tabcol_comment='{8}',LOGIC_SQL ='{9}'  where id='{10}'"
                    , strFlowName, strFlowID, strOtherName, strSTableName, strSColName, strSValue, strTTableName, strTColName, strTValue, strSqlData, strID);
                SysParams.OAConnection().RunSql(strSql);
            }
        }

        /// <summary>
        /// 删除表单数据配置
        /// </summary>
        /// <param name="strID">配置ID</param>
        /// <!--addby zhongjian 20091125-->
        public void DeleteData(string strID)
        {
            string strSql = string.Format(@"delete from xt_form_default where id='{0}'", strID);
            SysParams.OAConnection().RunSql(strSql);
        }
    }
}
