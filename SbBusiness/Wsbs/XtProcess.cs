﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Text.RegularExpressions;
using Common;
using Business;
using WF_Business;

namespace SbBusiness.Wsbs
{
    /// <summary>
    /// XT_PROCESS操作类
    /// </summary>
    public class XtProcess
    {
        /// <summary>
        /// 获取表单数据字段内容列表
        /// </summary>
        /// <param name="strID">表单ID</param>
        /// <returns></returns>
        /// <!--addby zhongjian 20091119-->
        public DataTable GetProcessDataList(string strID)
        {
            DataTable dtTemp;
            string strSql = string.Format(@"select name,db_table,db_field 
                            from st_form_input where fid=(select fid from XT_PROCESS WHERE ID='{0}')", strID);
            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }

        /// <summary>
        /// 根据流程ID取一个流程中的所有表格
        /// </summary>
        /// <param name="strName">流程ID</param>
        /// <returns></returns>
        public DataTable GetTablesInSerial(string strFlowID)
        {
            DataTable dtTemp;

            //针对2.0版本
            string strSql = string.Format(@"select distinct t.res_value, t.path bm
              from st_static_resource t, st_workflow b, xt_workflow_define c
             where t.wid = b.wid
               and t.res_value is not null
               and b.rot = 0
               and b.wname = c.flowname and c.id='{0}' 
                and t.res_value not in (select fid from XT_PROCESS where flowid ='{0}')", strFlowID);

            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }

        /// <summary>
        /// 根据流程名获取已发布的表单
        /// </summary>
        /// <param name="strName">流程名称</param>
        /// <returns></returns>
        public DataTable GetProcess(string strName)
        {
            string strSql = string.Format(@"select a.TABLEANOTHERNAME, a.FID, a.tablename, a.flowname from XT_PROCESS a, xt_workflow_define b
                                             where a.flowid = b.id
                                               and b.ispub = '1'
                                               and b.isdelete = '0'
                                               and b.flowname like '%{0}%'
                                             ORDER BY a.orderdield", strName);
            DataTable dtTemp;
            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }

        /// <summary>
        /// 删除一个表单
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static void DeleteProcess(string id)
        {
            string strSql = string.Format("delete from XT_PROCESS where id='{0}'", id);

            SysParams.OAConnection().RunSql(strSql);
        }

      
        /// <summary>
        /// 根据流程名获取相关内容
        /// </summary>
        /// <param name="strFlowID">流程ID</param>
        /// <returns></returns>
        /// <!--edit zhongjian 20091026-->
        public DataTable GetContent(string strFlowID)
        {
            DataTable dtTemp;
            string strSql = string.Format(@"select rownum,t.* from XT_PROCESS t 
                    where flowid = '{0}' order by orderdield ", strFlowID);

            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }

        /// <summary>
        /// 获取表单排序最大值
        /// </summary>
        /// <param name="strFlowID">流程ID</param>
        /// <returns></returns>
        /// <!--add by zhongjian 20091026-->
        public static string GetProcessMax(string strFlowID)
        {
            string strCount = "";
            if (!string.IsNullOrEmpty(strFlowID))
            {
                string strSql = string.Format(@"select max(ORDERDIELD) from XT_PROCESS where flowid='{0}'", strFlowID);
                strCount = SysParams.OAConnection().GetValue(strSql);
            }
            return strCount;
        }

        /// <summary>
        /// 增加一个表单
        /// </summary>
        /// <param name="strFlowName">流程名</param>
        /// <param name="strTableName">表名</param>
        /// <param name="strTableNotherName">表别名</param>
        /// <param name="strFid">The STR fid.</param>
        /// <param name="strFlowID">流程ID</param>
        /// <!--editby zhongjian 20091021-->
        public static void InsertProcess(string strFlowName, string strTableName,
            string strTableNotherName, string strFid, string strFlowID)
        {
            string strCount = GetProcessMax(strFlowID);
            if (string.IsNullOrEmpty(strCount))
                strCount = "0";
            int intCount = int.Parse(strCount) + 1;
            strCount = intCount.ToString();
            if (!string.IsNullOrEmpty(strFlowName))
            {
                string strSql = string.Format(@"Insert into XT_PROCESS (FLOWNAME,TABLENAME,
                    TABLEANOTHERNAME,fid,flowid,orderdield) VALUES ('{0}','{1}','{2}','{3}','{4}','{5}')", strFlowName,
                    strTableName, strTableNotherName, strFid, strFlowID, strCount);

                SysParams.OAConnection().RunSql(strSql);
            }
        }

        /// <summary>
        /// 根据id获取相关内容
        /// </summary>
        /// <param name="strId">The STR id.</param>
        /// <returns></returns>
        public DataTable GetContentId(string strId)
        {

            DataTable dtTemp;
            string strSql = string.Format(@"select * from XT_PROCESS 
                    where ID = '{0}'", strId);

            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }

        /// <summary>
        /// 修改一个表单
        /// </summary>
        /// <param name="strId">The STR id.</param>
        /// <param name="strTableNotherName">Name of the STR table nother.</param
        /// <param name="strFlowID">流程ID</param>
        /// <!--editby zhongjian 20091021-->
        public static void UpdateIsFlag(string strId, string strTableNotherName, string strFlowID)
        {
            string strSql = string.Format(@"update XT_PROCESS set TABLEANOTHERNAME='{1}',flowid='{2}' where ID='{0}'",
                strId, strTableNotherName, strFlowID);

            SysParams.OAConnection().RunSql(strSql);
        }

       

    }
}
