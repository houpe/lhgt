﻿using Business;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using WF_Business;

namespace SbBusiness
{
    /// <summary>
    /// 建设用地出让信息操作类
    /// </summary>
    public class JsydCrOperation
    {
        /// <summary>
        /// 获取建设用地最新的信息（涵盖最新报价）
        /// </summary>
        /// <param name="strWhere">拓展查询条件</param>
        /// <param name="strUserId">用户id</param>
        /// <returns></returns>
        public DataTable GetNewestDkxx(string strWhere, string strUserId)
        {
            DataTable dtTemp;
            string strSql = @"select iid,DK_BH,DK_ZL,DK_TDMJ,DK_XMNR,DK_TDYT,(select max(BJJE) from XT_JSYD_BJXX a where a.iid=b.iid) zxbj,Get_Between_DateValue(bmxx_bmkssj,b.bmxx_bmjzrq,sysdate) bmsj,Get_Between_DateValue(b.jmxx_jmkssj,b.jmxx_jmjzrq,sysdate) jjsj,JMXX_GPQSJ,id
";
            if (!string.IsNullOrEmpty(strUserId))
            {
                strSql += string.Format(@" ,(select count(a.id) from xt_jsyd_bmxx a where a.iid=b.iid and a.userid='{0}') bmgs", strUserId);
            }
            strSql += " from ut_jsyd_crxx b where 1=1 ";

            if (!string.IsNullOrEmpty(strWhere))
            {
                strSql += strWhere;
            }

            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }

        /// <summary>
        /// 根据iid获取最新的地块信息
        /// </summary>
        /// <param name="strIID"></param>
        /// <returns></returns>
        public DataTable GetNewestDkxxById(string strIID)
        {
            DataTable dtTemp;
            string strSql = @"select b.*,(select max(BJJE) from XT_JSYD_BJXX a where a.iid=b.iid) zxbj from ut_jsyd_crxx b where 1=1 ";
            if (!string.IsNullOrEmpty(strIID))
            {
                strSql += string.Format(@" and b.iid='{0}'", strIID);
            }

            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }

        /// <summary>
        /// 根据id删除出让地块信息
        /// </summary>
        /// <param name="strId"></param>
        public void DeleteJsydCrxx(string strId)
        {
            string strSql = string.Format(@"delete from ut_jsyd_crxx b where id='{0}' ",strId);
            SysParams.OAConnection().RunSql(strSql);
        }

        /// <summary>
        /// 添加地块报价
        /// </summary>
        /// <param name="strUserId"></param>
        /// <param name="strBaoJia"></param>
        /// <param name="strIid"></param>
        public void AddDkxxBaoJia(string strUserId,string strBaoJia,string strIid)
        {
            string strSql = string.Format(@"insert into xt_jsyd_bjxx b(iid,userid,bjje) values('{0}','{1}','{2}') ", strIid, strUserId, strBaoJia);
            SysParams.OAConnection().RunSql(strSql);
        }

        /// <summary>
        /// 添加报名信息
        /// </summary>
        /// <param name="strUserId">用户id</param>
        /// <param name="strIid">地块编号</param>
        public void AddDkxxBaoMing(string strUserId, string strIid)
        {
            string strSql = string.Format(@"insert into xt_jsyd_bmxx b(iid,userid) values('{0}','{1}') ", strIid, strUserId);
            SysParams.OAConnection().RunSql(strSql);
        }
    }
}
