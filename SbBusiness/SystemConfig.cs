
using Business;
// 创建人  ：linjian
// 创建时间：2006-10-01
using System;
using System.Configuration;

namespace SbBusiness
{
    /// <summary>
    /// 系统参数类
    /// </summary>
    public static class SystemConfig
    {
        private static string _DateFormat;

        /// <summary>
        /// WebService链接地址
        /// </summary>
        public static string Format
        {
            get { return _DateFormat; }
        }

        private static int _PageSize;

        /// <summary>
        /// datagrid单个页面显示的记录行数
        /// </summary>
        public static int PageSize
        {
            get { return _PageSize; }
        }

        private static int _IsInternet;

        /// <summary>
        /// 判断是否是外网系统(主要针对物业系统设置的标志)
        /// </summary>
        public static int IsInternet
        {
            get { return _IsInternet; }
        }

        private static int _menuAppearMaxCount;

        /// <summary>
        /// 父菜单显示的最大个数
        /// </summary>
        public static int MenuAppearMaxCount
        {
            get { return _menuAppearMaxCount; }
        }

        private static string _adminUser;

        /// <summary>
        /// 系统的超级管理员
        /// </summary>
        public static string AdminUser
        {
            get { return _adminUser; }
        }

        private static string[] _uploadFileExtension;

        /// <summary>
        /// 上传文件后缀
        /// </summary>
        public static string[] UploadFileExtension
        {
            get { return _uploadFileExtension; }
        }

        private static string _webSystemFlag;
        /// <summary>
        /// 外网业务申报系统标志
        /// </summary>
        public static string WebSystemFlag
        {
            get { return _webSystemFlag; }
        }

        private static string _cszcUserType;
        /// <summary>
        /// 初始注册用户
        /// </summary>
        public static string CszcUserType
        {
            get { return _cszcUserType; }
        }

        private static string _blqxsphUserType;
        /// <summary>
        /// 办事权限审批后用户
        /// </summary>
        public static string BlqxsphUserType
        {
            get { return _blqxsphUserType; }
        }

        private static string _zcyhysName;
        /// <summary>
        /// 注册用户预审设置键名
        /// </summary>
        public static string ZcyhysName
        {
            get { return _zcyhysName; }
        }
    
        private static string _excelOutPath;
        /// <summary>
        /// excel的导出路径
        /// </summary>
        public static string ExcelOutPath
        {
            get { return _excelOutPath; }
        }

        private static string _netoWsUrl;
        /// <summary>
        /// 工作流引擎路径
        /// </summary>
        public static string NetoWsUrl
        {
            get { return _netoWsUrl; }
        }

        private static string _NoSubmitDay;
        /// <summary>
        /// 尚未提交事项期限
        /// </summary>
        public static string NoSubmitDay
        {
            get { return _NoSubmitDay; }
        }

        private static string _BuZhengSubmitDay;
        /// <summary>
        /// 补齐补证事项期限
        /// </summary>
        public static string BuZhengSubmitDay
        {
            get { return _BuZhengSubmitDay; }
        }

        private static string _loginPage;
        /// <summary>
        /// 登录首页
        /// </summary>
        public static string LoginPage
        {
            get { return _loginPage; }
        }

        private static string _interfaceSerialName;
        /// <summary>
        /// 做接口的流程名称
        /// </summary>
        public static string InterfaceSerialName
        {
            get { return _interfaceSerialName; }
        }

        /// <summary>
        /// 加载系统配置
        /// </summary>
        static SystemConfig()
        {
            //时间格式
            _DateFormat = ConfigurationManager.AppSettings["date.format"];

            //datagrid单个页面显示的记录行数
            _PageSize = BasicOperate.GetDataBaseInt( ConfigurationManager.AppSettings["pagesize"],true);

            //判断是否是外网
            _IsInternet = BasicOperate.GetDataBaseInt(ConfigurationManager.AppSettings["IsInternet"],true);

            //父菜单显示的最大个数
            _menuAppearMaxCount = BasicOperate.GetDataBaseInt(ConfigurationManager.AppSettings["MenuAppearMaxCount"], true);

            //超级管理员用户
            _adminUser = ConfigurationManager.AppSettings["adminUser"];

            //上传文件后缀
            _uploadFileExtension = ConfigurationManager.AppSettings["UploadFileFormat"].Split(',');

            //获取各系统标识
            _webSystemFlag = ConfigurationManager.AppSettings["webSystemFlag"];

            //获取各用户标识
            _cszcUserType = ConfigurationManager.AppSettings["csyhSystemFlag"];
            _blqxsphUserType = ConfigurationManager.AppSettings["bsqxspSystemFlag"];
            _zcyhysName = ConfigurationManager.AppSettings["zcyhysName"];

            //获取到处excel路径
            _excelOutPath = ConfigurationManager.AppSettings["ExcelOutPath"];

            //工作流引擎路径
            _netoWsUrl = ConfigurationManager.AppSettings["NetoWsUrl"];

            //尚未提交事项期限
            _NoSubmitDay = ConfigurationManager.AppSettings["NoSubmitDay"];

            //补齐补证事项期限
            _BuZhengSubmitDay = ConfigurationManager.AppSettings["BuZhengSubmitDay"];

            //登录首页
            _loginPage = ConfigurationManager.AppSettings["loginPage"];

            //接口业务流程名
            _interfaceSerialName = ConfigurationManager.AppSettings["SerialFlowType"];
        }
    }
}