
using System;
using System.Collections.Generic;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Text.RegularExpressions;
using Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Business;
using WF_Business;
using WF_DataAccess;
using System.Data.OracleClient;
using System.Configuration;

namespace SbBusiness.User
{
    /// <summary>
    /// 用户管理
    /// </summary>
    public class SysUserHandle
    {
        /// <summary>
        /// 验证是否存在指定的用户名和密码。
        /// </summary>
        /// <param name="userName">要验证的用户的名称。</param>
        /// <param name="password">指定的用户的密码。</param>
        /// <returns>如果指定的用户名和密码有效，则为 true；否则为 false。</returns>        
        public static bool ValidateUser(string userName, string password)
        {
            if (!(string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password)))
            {
                return CheckEncodePassword(userName, password);
            }
            return false;
        }

        /// <summary>
        /// 验证是否存在指定的用户名和密码。(密码为加密后的)
        /// </summary>
        /// <param name="userName">要验证的用户的名称。</param>
        /// <param name="password">指定的用户的密码。</param>
        /// <returns>如果指定的用户名和密码有效，则为 true；否则为 false。</returns>
        public static bool ValidateUserPassword(string userName, string password)
        {
            if (!(string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password)))
            {
                string strDatabasePass = GetPassword(userName);
                return (strDatabasePass == password);
            }
            return false;
        }

        /// <summary>
        /// 添加新用户
        /// </summary>
        /// <param name="userID">登陆帐号</param>
        /// <param name="userName">用户名称</param>
        /// <param name="userPwd">密码</param>
        /// <param name="type">用户类型</param>
        /// <param name="tel">电话</param>
        /// <param name="mobile">手机</param>
        /// <param name="address">地址</param>
        /// <param name="strZjlb">证件类别</param>
        /// <param name="strZjhm">证件号码</param>
        /// <param name="strCz">传真</param>
        /// <param name="strEmail">email.</param>
        /// <param name="strYzbm">邮政编码</param>
        /// <param name="isValid">是否有效</param>
        /// <param name="strUserType">用户类型</param>
        /// <param name="strUnitsID">单位ID</param>
        /// <param name="strSex">性别</param>
        /// <param name="strUnitsAdmin">单位人员标识(0:成员;1:管理员)</param>
        /// <param name="strGrssdw">个人所属单位</param>
        /// <!--添加一些单位关联信息 addby zhongjian 20100125-->
        public static void CreateUser(string userID,string userName, string userPwd, int type, string tel,
             string mobile, string address,string strZjlb,string strZjhm,string strCz,
            string strEmail,string strYzbm,int isValid,string strUserType,string strUnitsID,string strSex,string strUnitsAdmin,string strGrssdw)
        {
            //userName = userName.ToUpperInvariant();//不再将用户名称转为大写保存
            string strPass = Encode.Md5(userPwd);

            //检查用户是否存在
            if (CheckUserExist(userID, type.ToString()))
            {
                throw new DBConcurrencyException("用户名已存在！");
            }
            //创建用户
            string strSql = @"insert into sys_user(userID,username,userPwd,query_pass,type,tel,mobile,address,IDTYPE,IDNUMBER,FAX,EMAIL,POSTCODE,isValid,SYNC_TYPE,USERTYPE,UNITID,sex,UNITSADMIN,grssdw) 
                values (:userID,:username,:userPwd,:querypass,:type,:tel,:mobile,:address,:zjlb,:zjhm,:cz,
                :email,:yzbm,:isValid,1,:usertype,:unitsid,:sex,:unitsadmin,:grssdw)";
           
            IDataAccess ida = SysParams.OAConnection(false);
            IDataParameter[] iDataPrams = DataFactory.GetParameter(DataFactory.DefaultDbType, 19);
            iDataPrams[0].ParameterName = "userID";
            iDataPrams[0].DbType = DbType.String;
            iDataPrams[0].Value = userID;

            iDataPrams[1].ParameterName = "username";
            iDataPrams[1].DbType = DbType.String;
            iDataPrams[1].Value = userName;

            iDataPrams[2].ParameterName = "userPwd";
            iDataPrams[2].DbType = DbType.String;
            iDataPrams[2].Value = strPass;

            iDataPrams[3].ParameterName = "querypass";
            iDataPrams[3].DbType = DbType.String;
            iDataPrams[3].Value = userPwd;

            iDataPrams[4].ParameterName = "type";
            iDataPrams[4].DbType = DbType.Int32;
            iDataPrams[4].Value = type;

            iDataPrams[5].ParameterName = "tel";
            iDataPrams[5].DbType = DbType.String;
            iDataPrams[5].Value = tel;

            iDataPrams[6].ParameterName = "mobile";
            iDataPrams[6].DbType = DbType.String;
            iDataPrams[6].Value = mobile;

            iDataPrams[7].ParameterName = "zjlb";
            iDataPrams[7].DbType = DbType.String;
            iDataPrams[7].Value = strZjlb;

            iDataPrams[8].ParameterName = "zjhm";
            iDataPrams[8].DbType = DbType.String;
            iDataPrams[8].Value = strZjhm;

            iDataPrams[9].ParameterName = "cz";
            iDataPrams[9].DbType = DbType.String;
            iDataPrams[9].Value = strCz;

            iDataPrams[10].ParameterName = "email";
            iDataPrams[10].DbType = DbType.String;
            iDataPrams[10].Value = strEmail;

            iDataPrams[11].ParameterName = "yzbm";
            iDataPrams[11].DbType = DbType.String;
            iDataPrams[11].Value = strYzbm;

            iDataPrams[12].ParameterName = "address";
            iDataPrams[12].DbType = DbType.String;
            iDataPrams[12].Value = address;

            iDataPrams[13].ParameterName = "isValid";
            iDataPrams[13].DbType = DbType.Int32;
            iDataPrams[13].Value = isValid;

            iDataPrams[14].ParameterName = "usertype";
            iDataPrams[14].DbType = DbType.String;
            iDataPrams[14].Value = strUserType;

            iDataPrams[15].ParameterName = "unitsid";
            iDataPrams[15].DbType = DbType.String;
            iDataPrams[15].Value = strUnitsID;

            iDataPrams[16].ParameterName = "sex";
            iDataPrams[16].DbType = DbType.String;
            iDataPrams[16].Value = strSex;

            iDataPrams[17].ParameterName = "unitsadmin";
            iDataPrams[17].DbType = DbType.String;
            iDataPrams[17].Value = strUnitsAdmin;

            iDataPrams[18].ParameterName = "grssdw";
            iDataPrams[18].DbType = DbType.String;
            iDataPrams[18].Value = strGrssdw;

            ida.RunSql(strSql, ref iDataPrams);
        }

        /// <summary>
        /// 修改用户
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="userName"></param>
        /// <param name="userPwd"></param>
        /// <param name="type"></param>
        /// <param name="tel"></param>
        /// <param name="mobile"></param>
        /// <param name="address"></param>
        /// <param name="strZjlb"></param>
        /// <param name="strZjhm"></param>
        /// <param name="strCz"></param>
        /// <param name="strEmail"></param>
        /// <param name="strYzbm"></param>
        /// <param name="isValid"></param>
        /// <param name="strUserType"></param>
        /// <param name="strUnitsID"></param>
        /// <param name="strUnitsAdmin"></param>
        /// <param name="strSex">性别</param>
        /// <!--addby zhongjian 20100130-->
        public static void UpdateUserInfo(string userID, string userName, string userPwd, int type, string tel,
             string mobile, string address, string strZjlb, string strZjhm, string strCz,string strEmail, string strYzbm,
             int isValid, string strUserType, string strUnitsID, string strUnitsAdmin,string strSex)
        {
            //检查用户是否存在
            if (CheckUserExist(userID, type.ToString()))
            {
                throw new DBConcurrencyException("用户名已存在！");
            }
            //创建用户
            string strSql = string.Format(@"update sys_user set username=:username,query_pass=:querypass,type=:type,tel=:tel,mobile=:mobile,address=:address,IDTYPE=:zjlb,IDNUMBER=:zjhm,FAX=:cz,EMAIL=:email,POSTCODE=:yzbm,isValid=:isValid,
                USERTYPE=:usertype,UNITID=:unitsid,UNITSADMIN=:unitsadmin,sex=:Sex where userID='{0}'", userID);


            IDataAccess ida = SysParams.OAConnection(false);
            IDataParameter[] iDataPrams = DataFactory.GetParameter(DataFactory.DefaultDbType, 16);
            iDataPrams[0].ParameterName = "username";
            iDataPrams[0].DbType = DbType.String;
            iDataPrams[0].Value = userName;

            iDataPrams[1].ParameterName = "querypass";
            iDataPrams[1].DbType = DbType.String;
            iDataPrams[1].Value = userPwd;

            iDataPrams[2].ParameterName = "type";
            iDataPrams[2].DbType = DbType.Int32;
            iDataPrams[2].Value = type;

            iDataPrams[3].ParameterName = "tel";
            iDataPrams[3].DbType = DbType.String;
            iDataPrams[3].Value = tel;

            iDataPrams[4].ParameterName = "mobile";
            iDataPrams[4].DbType = DbType.String;
            iDataPrams[4].Value = mobile;

            iDataPrams[5].ParameterName = "zjlb";
            iDataPrams[5].DbType = DbType.String;
            iDataPrams[5].Value = strZjlb;

            iDataPrams[6].ParameterName = "zjhm";
            iDataPrams[6].DbType = DbType.String;
            iDataPrams[6].Value = strZjhm;

            iDataPrams[7].ParameterName = "cz";
            iDataPrams[7].DbType = DbType.String;
            iDataPrams[7].Value = strCz;

            iDataPrams[8].ParameterName = "email";
            iDataPrams[8].DbType = DbType.String;
            iDataPrams[8].Value = strEmail;

            iDataPrams[9].ParameterName = "yzbm";
            iDataPrams[9].DbType = DbType.String;
            iDataPrams[9].Value = strYzbm;

            iDataPrams[10].ParameterName = "address";
            iDataPrams[10].DbType = DbType.String;
            iDataPrams[10].Value = address;

            iDataPrams[11].ParameterName = "isValid";
            iDataPrams[11].DbType = DbType.Int32;
            iDataPrams[11].Value = isValid;

            iDataPrams[12].ParameterName = "usertype";
            iDataPrams[12].DbType = DbType.String;
            iDataPrams[12].Value = strUserType;

            iDataPrams[13].ParameterName = "unitsid";
            iDataPrams[13].DbType = DbType.String;
            iDataPrams[13].Value = strUnitsID;

            iDataPrams[14].ParameterName = "unitsadmin";
            iDataPrams[14].DbType = DbType.String;
            iDataPrams[14].Value = strUnitsAdmin;

            iDataPrams[15].ParameterName = "Sex";
            iDataPrams[15].DbType = DbType.String;
            iDataPrams[15].Value = strSex;

            ida.RunSql(strSql, ref iDataPrams);
        }

        /// <summary>
        /// 修改用户信息
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="userID">登陆帐号</param>
        /// <param name="userName">用户名称</param>
        /// <param name="type">用户类型</param>
        /// <param name="tel">电话</param>
        /// <param name="mobile">手机</param>
        /// <param name="address">地址</param>
        /// <param name="strIdType">证件类别</param>
        /// <param name="strIdNumber">证件号码</param>
        /// <param name="strFax">传真</param>
        /// <param name="strEmail">email.</param>
        /// <param name="strPostCode">邮政编码</param>
        public static void UpdateUser(string id, string userID, string userName, int type, string tel,
             string mobile, string address, string strIdType, string strIdNumber, string strFax,
            string strEmail, string strPostCode)
        {
            try
            {
                string strSql = string.Format(@"update sys_user set userID = '{1}', username = '{2}',
                     type = '{3}',tel = '{4}',mobile = '{5}',address = '{6}',IDTYPE = '{7}',
                     IDNUMBER = '{8}',FAX = '{9}',EMAIL = '{10}',POSTCODE = '{11}',CAN_SYNC=1 where id = '{0}'",
                     id, userID, userName, type, tel, mobile, address, strIdType, strIdNumber, strFax,
                     strEmail, strPostCode);

                SysParams.OAConnection().RunSql(strSql);
                //如果是地信中心用户信息则更改标志位
                strSql = string.Format(@"update sys_user t set NEED_POST =1 where t.SYNC_TYPE=2 
                    and userid='{0}'", userID);
                SysParams.OAConnection().RunSql(strSql);
            }
            catch
            {
                throw new DBConcurrencyException("修改新用户失败！");
            }
        }


        /// <summary>
        /// 检查加密后密码是否匹配
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <param name="password">密码</param>
        /// <returns>密码匹配返回true,否则返回false</returns>
        /// <!--
        /// 创建人  : LiKun
        /// 创建时间: 2006-11-24
        /// -->
        public static bool CheckEncodePassword(string userName, string password)
        {
            string strDatabasePass = GetPassword(userName);
            return (strDatabasePass == Encode.Md5(password));
        }

        /// <summary>
        /// 检验密码是否匹配
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <param name="password">密码</param>
        /// <returns>密码匹配返回true,否则返回false</returns>
        /// <!--
        /// 创建人  : LiKun
        /// 创建时间: 2006-09-07
        /// -->
        public static bool CheckPassword(string userName, string password)
        {
            string strDatabasePass = GetPassword(userName);
            return (strDatabasePass == password);
        }

        /// <summary>
        /// 获取密码。
        /// </summary>
        /// <param name="userName">用户名。</param>
        /// <returns>数据库中对应用户密码</returns>
        private static string GetPassword(string userName)
        {
            string result = string.Empty;
            if (!string.IsNullOrEmpty(userName))
            {
              
                //屏蔽将用户度转为大写之后再验证 edit by zhongjian 20090924
                string strSql = string.Format(@"select userpwd from sys_user where userid ='{0}' and isvalid='1'",userName);
                result = SysParams.OAConnection().GetValue(strSql);
            }
            return result;
        }

        /// <summary>
        /// 获取获取用户所属行政区ID。
        /// </summary>
        /// <param name="userName">用户名。</param>
        /// <returns>数据库中对应的数据字典</returns>
        /// <!--
        /// 创建人  : LinJian
        /// 创建时间: 2006-09-04
        /// -->
        public static string GetSectionId(string userName)
        {
            string result = string.Empty;
            if (!string.IsNullOrEmpty(userName))
            {
                string strSql = @"select region_id from sys_user where userid = upper(:username)";

                result = SysParams.OAConnection().GetValue(strSql);
            }
            return result;
        }

        /// <summary>
        /// 检测同一系统下用户是否存在
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <param name="sysType">系统类型</param>
        /// <returns>用户存在则返回true,否则返回false</returns>
        public static bool CheckUserExist(string userName, string sysType)
        {
            if (!string.IsNullOrEmpty(userName))
            {
                string strSql = string.Empty;
                string result;

                strSql = string.Format(@"select userid from sys_user a join sys_usertype b on a.type=b.usertypecode and a.userid ='{0}' and trim(b.syscode)='{1}'",userName,sysType);//屏蔽登录时校验用户名的大小写

                result = SysParams.OAConnection().GetValue(strSql);
                if (0 == string.Compare(result, userName, true))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// 检测用户是否存在
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <returns>用户存在则返回true,否则返回false</returns>  
        public static bool CheckUserExist(string userName)
        {
            string strSql = string.Empty;
            strSql = string.Format("select * from sys_user a where a.userid = '{0}'", userName);

            DataTable dt = new DataTable();
            SysParams.OAConnection().RunSql(strSql, out dt);
            if (dt.Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 检查密码的强度是否符合要求。
        /// </summary>
        /// <param name="pass">需要检验的密码。</param>
        /// <param name="minLength">密码的最小长度。</param>
        /// <param name="minSpecial">密码中必须包含的特殊字符的最小数量。</param>
        /// <param name="regular">用于计算密码的正则表达式。</param>
        /// <returns></returns>
        /// <!--
        /// 创建人  : LiKun
        /// 创建时间: 2007-4-28
        /// -->
        public static bool CheckPasswordStrong(string pass, int minLength, int minSpecial, string regular)
        {
            bool result = false;
            if (pass.Length >= minLength)
            {
                int num = 0;
                for (int i = 0; i < pass.Length; i++)
                {
                    if (!char.IsLetterOrDigit(pass, i))
                    {
                        num++;
                    }
                }
                if (num >= minSpecial)
                {
                    if (regular.Length > 0)
                    {
                        result = Regex.IsMatch(pass, regular);
                    }
                    else
                    {
                        result = true;
                    }
                }
            }
            
            return result;
        }

        /// <summary>
        /// 获取用户类型
        /// </summary>
        /// <param name="userID">登陆账号</param>
        /// <returns>用户类型</returns>
        /// <!--
        /// 创建人  : dchen
        /// 创建时间: 2007-07-07
        /// -->
        public static string GetUserType(string userID)
        {
            string strSql = "select type from sys_user where userID='" + userID + "'";

            return SysParams.OAConnection().GetValue(strSql);
        }

       

        /// <summary>
        /// 检查用户是否通过审核
        /// </summary>
        /// <param name="userID">登陆用户名</param>
        /// <returns></returns>
        /// <!--
        /// 创建人  : cd
        /// 创建时间: 2007-07-16
        /// -->
        public static bool UserIsvalid(string userID)
        {
            string strSql = "select isvalid from sys_user where userID='" + userID + "'";
            try
            {
                string result = SysParams.OAConnection().GetValue(strSql);
                if (result == "1")
                {
                    return true;
                }
            }
            catch
            {
                throw;
            }
            return false;
        }

        
      
        /// 根据用户类型以及用户名字一部分获取用户名字
        public DataTable GetUserName(string usertype, string query)
        {
            string strSql = "select UserName 用户名称 from sys_User where TYPE='" + usertype +
                        "' and username like '%" + query + "%'";
            DataTable dtTemp = new DataTable();
            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }

       
        /// 根据用户类型获取用户名字
        /// 根据用户名获取用户id
        /// </summary>
        /// <param name="name">用户名</param>
        /// <returns></returns>
        public string GetUserIDByUserName(string name)
        {
            string strSql = "select USERID from sys_user where USERNAME='" + name + "'";

            return SysParams.OAConnection().GetValue(strSql);
        }

        /// <summary>
        /// 根据用户id重置用户密码与用户ID相同
        /// </summary> 
        /// <param name="id">用户ID</param>
        /// <returns></returns>
        public static void UpdatePassword(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                string strSql = string.Format("update sys_user set query_pass =userid,USERPWD=fn_md5(userid) where id = '{0}'", id);

                SysParams.OAConnection().RunSql(strSql);

                //如果是地信中心用户信息则更改标志位
                strSql = string.Format(@"update sys_user t set NEED_POST =1 where t.SYNC_TYPE=2 
                    and id='{0}'", id);

                SysParams.OAConnection().RunSql(strSql);
            }
        }
       
        /// <summary>
        /// 根据用户ID获取用户名
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string GetUserNameByUserID(string id)
        {
            string strSql = "select USERNAME from sys_user where USERID='" + id + "'";

            return SysParams.OAConnection().GetValue(strSql);
        }

        /// <summary>
        /// 添加系统用户
        /// </summary>
        /// <param name="txtUserID"></param>
        /// <param name="txtUserName"></param>
        /// <param name="pwd"></param>
        /// <param name="txtUserPwd"></param>
        /// <param name="ddlType"></param>
        public void AddSysUser(string txtUserID, string txtUserName, string pwd, string txtUserPwd, string ddlType)
        {
            string id = Guid.NewGuid().ToString().Replace("-", "");
            string sql = "INSERT INTO SYS_USER(id,userid, username, userpwd,query_pass, type, isvalid, can_sync)"
                + " VALUES('{0}','{1}','{2}','{3}',{4},'{5}',1,1)";
            sql = String.Format(sql, id, txtUserID, txtUserName, pwd, txtUserPwd, ddlType);
            SysParams.OAConnection().RunSql(sql);

        }
        /// <summary>
        /// 更新系统用户
        /// </summary>
        /// <param name="pwd"></param>
        /// <param name="id"></param>
        /// <param name="txtUserName"></param>
        /// <param name="txtUserID"></param>
        /// <param name="txtUserPwd"></param>
        /// <param name="ddlType"></param>
        public void UpdateSysUser(string pwd, string id, string txtUserName, string txtUserID, string txtUserPwd, string ddlType)
        {
            if (String.IsNullOrEmpty(pwd))
            {
                pwd = SysParams.OAConnection().GetValue("SELECT userpwd FROM sys_user WHERE id='" + id + "'");
            }
            string sql = "UPDATE SYS_USER SET username='{0}', userpwd='{1}', type='{2}',query_pass='{3}',userid='{4}',can_sync=1 WHERE id='" + id + "'";
            sql = String.Format(sql, txtUserName, pwd, ddlType, txtUserPwd, txtUserID);
            SysParams.OAConnection().RunSql(sql);
        }

        /// <summary>
        /// 获取系统用户
        /// </summary>
        /// <param name="txtId"></param>
        /// <param name="txtUserName"></param>
        /// <returns></returns>
        public string GetSysUserByName(string txtId, string txtUserName)
        {
            string sql = "SELECT UserName FROM SYS_User WHERE UserName='" + txtUserName + "'";
            if (!String.IsNullOrEmpty(txtId))
                sql += " AND Id<>'" + txtId + "'";
            return SysParams.OAConnection().GetValue(sql);
        }
       
        /// <summary>
        /// 获取单位内部用户
        /// </summary>
        /// <param name="strUnitid"></param>
        /// <returns></returns>
        public DataTable GetUserInUnit(string strUnitid)
        {
            string strSql = string.Format("select * from sys_user where UNITID='{0}' and UNITSADMIN='0'", strUnitid);
            DataTable dtTemp = new DataTable();
            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }

       

        /// <summary>
        /// 验证用户名存在
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool CheckUserName(string name)
        {
            string strSql = @"select userID from sys_user where username='" + name + "'";

            string result = string.Empty;
            try
            {
                result = SysParams.OAConnection().GetValue(strSql);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return (result != string.Empty);
        }


        /// <summary>
        /// 获取指定用户的密码
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string GetPWDByUserID(string id)
        {
            string strSql = "select USERPWD from sys_user where UserID='" + id + "'";
            return SysParams.OAConnection().GetValue(strSql);
        }

        /// <summary>
        /// 更改用户密码
        /// </summary>
        /// <param name="id"></param>
        ///  <param name="newPWD">新密码</param>
        /// <returns></returns>
        public bool ModifyPWD(string id,string newPWD)
        {
            string strSql = string.Format(@"update sys_user set USERPWD='{0}',query_pass='{1}' 
                where UserID='{2}'",Encode.Md5(newPWD),newPWD,id) ;
            return SysParams.OAConnection().RunSql(strSql) > 0 ? true : false;

            //添加操作日志
            //string strRemark = string.Format("修改密码");
            //SystemLogs.AddSystemLogs(id, "update", strRemark, strSql);
        }


        /// <summary>
        /// 获取用户所属的系统
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public DataTable GetUserTypeRegionByUserID(string id)
        {
            string strSql = string.Format(@"select type,region_id,
                WyNewPackage.GetItemTypeName('区属',region_id) area
                from sys_user where USERID='{0}'",id);
            DataTable dtTemp = new DataTable();
            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }
        /// <summary>
        /// 获取所有用户信息
        /// </summary>
        /// <returns></returns>
        public DataTable GetAllUsers(string userid,string username)
        {
            string strSql = "select * from sys_user  where 1=1";
            if (userid != string.Empty)
                strSql += " and userid like '%" + userid + "%'";
            if (username != string.Empty)
                strSql += " and username like '%" + username + "%'";
            strSql += " order by rowid";
            DataTable dtTemp = new DataTable();
            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }
        
        /// <summary>
        /// 获取除此用户之外的所有用户人信息
        /// </summary>
        public DataTable GetAllUserExclusionByUser(bool bPromise)
        {
            string strSql = @"select t.*,(case when IS_PROMISE=1 then '通过' when IS_PROMISE=-1 
                then '未通过' end) spjg from sys_user t where IS_PROMISE=0";

            if (bPromise)
            {
                strSql = @"select t.*,(case when IS_PROMISE=1 then '通过' when IS_PROMISE=-1 
                then '未通过' end) spjg from sys_user t where IS_PROMISE!=0";
            }
            DataTable dtTemp = new DataTable();
            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }

        /// <summary>
        /// 审核通过
        /// </summary>
        /// <param name="name"></param>
        public static void UpdataPassData(string name)
        {
            string strSql = "update sys_user set IS_PROMISE=1,ISVALID=1 where USERID='" + name + "'";
            SysParams.OAConnection().RunSql(strSql);
        }

        /// <summary>
        /// 审核不通过
        /// </summary>
        /// <param name="name"></param>
        public static void UpdataUnPassData(string name)
        {
            string strSql = "update sys_user set IS_PROMISE=-1 where USERID='" + name + "'";
            SysParams.OAConnection().RunSql(strSql);
        }

        /// <summary>
        /// 通过UserID删除用户信息
        /// </summary>
        /// <param name="id"></param>
        public static void DelUserInfoByUserID(string id)
        {
            string strSql = "delete from sys_user where USERID='" + id + "'";
            SysParams.OAConnection().RunSql(strSql);
        }

        /// <summary>
        /// 删除用户信息
        /// </summary>
        /// <param name="id"></param>
        public static void DelUserInfoById(string id)
        {
            string strSql = "delete from sys_user where id='" + id + "'";
            SysParams.OAConnection().RunSql(strSql);
        }

        #region 获取用户名查密码
        /// <summary>
        /// 根据用户id或用户名查密码
        /// </summary>
        /// <param name="strUserId"></param>
        /// <param name="strUserName"></param>
        /// <returns></returns>
        public static DataTable QueryPassByUserInfo(string strUserId, string strUserName)
        {
            string strSql =@"select userid,username,query_pass from sys_user";

            if (!string.IsNullOrEmpty(strUserId))
            {
                strSql += string.Format(" and userid='{0}'", strUserId.ToUpper());
            }

            if (!string.IsNullOrEmpty(strUserName))
            {
                strSql += string.Format(" and username like '%{0}%'", strUserName);
            }
            DataTable dtTemp = new DataTable();
            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }
        #endregion

        /// <summary>
        /// 根据用户id获取所能操作的业务名称
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        public static DataTable GetSerialName(string userid)
        {
            string strSql = string.Format("select SERIAL_NAME from SYS_USER_SERIAL where is_pass=1 and UserID='{0}'", userid);

            DataTable dtTemp = new DataTable();
            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }

        /// <summary>
        /// 根据用户id获取用户所有信息
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        public static DataTable GetUser(string userid)
        {
            string strSql = string.Format("select * from SYS_USER where UserID='{0}'", userid);
            DataTable dtTemp = new DataTable();
            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }

        /// <summary>
        /// 获取系统用户
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public DataTable GetSysUserById(string id)
        {
            DataTable dt;
            string strSql = "SELECT * FROM sys_User WHERE id='" + id + "'";
            SysParams.OAConnection().RunSql(strSql, out dt);
            return dt;
        }

        /// <summary>
        /// 检测用户是否存在过
        /// </summary>
        /// <param name="UserName">用户名称</param>
        /// <returns> 存在过则返回true,否则返回false</returns>
        /// <!--addby zhongjian 20091020-->
        public static bool CheckUserOldExist(string UserName)
        {
            if (!string.IsNullOrEmpty(UserName))
            {
                string strSql = string.Empty;
                string result;

                strSql = string.Format(@"select userid from sys_user_old where userid ='{0}'", UserName);
                result = SysParams.OAConnection().GetValue(strSql);

                if (0 == string.Compare(result, UserName, true))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// 获取用户密码
        /// </summary>
        /// <param name="strUserID">用户ID</param>
        /// <returns></returns>
        /// <!--addby zhonngjian 20091126-->
        public string GetUserPWD(string strUserID)
        {
            string strSql = string.Format(@"select userpwd from sys_user where userid='{0}'", strUserID);

            return SysParams.OAConnection().GetValue(strSql);
        }

        /// <summary>
        /// 设置用户类型
        /// </summary>
        /// <param name="strID">用户唯一标识id</param>
        /// <param name="strUserType">用户类型</param>
        public static void SaveUserType(string strID, string strUserType)
        {
            string strSql = string.Format("update sys_user set unitsadmin='{1}' where id='{0}'", strID, strUserType);
            SysParams.OAConnection().RunSql(strSql);
        }

        /// <summary>
        /// 获取用户信息
        /// </summary>
        /// <param name="strUserID">用户ID</param>
        /// <param name="strUserName">用户名称</param>
        /// <param name="UnitsName">单位名称</param>
        /// <param name="UnitsID">单位ID</param>
        /// <returns></returns>
        /// <!--addby zhongjian 20100326-->
        public static DataTable GetUnitUser(string strUserID, string strUserName, string UnitsName, string UnitsID, string strAdmin)
        {
            DataTable dtTemp = new DataTable();
            string strSql = string.Format(@"select a.*,
                                                   case
                                                     when a.usertype = '1' then
                                                      (select unitsname
                                                         from sys_units b
                                                        where b.id = a.unitid
                                                          and rownum < 2)
                                                     else
                                                      ''
                                                   end unitsname
                                              from sys_user a where 1=1 ");
            if (!string.IsNullOrEmpty(strUserID))
                strSql += string.Format(" and userid like '%{0}%'", strUserID);
            if (!string.IsNullOrEmpty(strUserName))
                strSql += string.Format(" and username like '%{0}%'", strUserName);
            if (!string.IsNullOrEmpty(UnitsName))
                strSql += string.Format(" and a.unitid in (select id from sys_units where unitsname like '%{0}%')", UnitsName);
            if (!string.IsNullOrEmpty(UnitsID))
                strSql += string.Format(" and a.unitid ='{0}'", UnitsID);
            if (!string.IsNullOrEmpty(strAdmin))
            {
                strSql += string.Format(" and a.UNITSADMIN ='{0}'", strAdmin);
            }
            strSql += string.Format(" order by unitsname");
            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }

        /// <summary>
        /// 添加单位信息内容
        /// </summary>
        /// <param name="strUnitsName">单位名称</param>
        /// <param name="strUnitsTel">单位电话</param>
        /// <param name="strUnitsAddress">单位地址</param>
        /// <param name="strFax">传真</param>
        /// <param name="strPostCode">邮编</param>
        /// <param name="strOrganization">组织代码</param>
        /// <param name="strLegal">法人代表或负责人 </param>
        /// <param name="strNature">单位性质</param>
        /// <returns></returns>
        public static string InsertUnits(string strUnitsName, string strUnitsTel, string strUnitsAddress, string strFax, string strPostCode,
            string strOrganization, string strLegal, string strNature)
        {
            string strNewId = string.Empty;
            string strSql = string.Format(@"insert into sys_units (UNITSNAME,UNITSTEL,UNITSADDRESS,FAX,POSTCODE,Organization,Legal,Nature) 
                    values(:unitsname,:unitstel,:unitsaddress,:fax,:postcode,:organization,:legal,:nature)  returning id into :newId");


            IDataAccess ida = SysParams.OAConnection(false);

            IDataParameter[] iDataPrams = new OracleParameter[9];
            iDataPrams[0] = new OracleParameter("unitsname", OracleType.VarChar,255);
            iDataPrams[0].Value = strUnitsName;

            iDataPrams[1] = new OracleParameter("unitstel", OracleType.VarChar, 255);
            iDataPrams[1].Value = strUnitsTel;

            iDataPrams[2] = new OracleParameter("unitsaddress", OracleType.VarChar, 255);
            iDataPrams[2].Value = strUnitsAddress;

            iDataPrams[3] = new OracleParameter("fax", OracleType.VarChar, 255);
            iDataPrams[3].Value = strFax;

            iDataPrams[4] = new OracleParameter("postcode", OracleType.VarChar, 255);
            iDataPrams[4].Value = strPostCode;

            iDataPrams[5] = new OracleParameter("organization", OracleType.VarChar, 255);
            iDataPrams[5].Value = strOrganization;

            iDataPrams[6] = new OracleParameter("legal", OracleType.VarChar, 255);
            iDataPrams[6].Value = strLegal;

            iDataPrams[7] = new OracleParameter("nature", OracleType.VarChar, 255);
            iDataPrams[7].Value = strNature;

            iDataPrams[8] = new OracleParameter("newId", OracleType.VarChar, 255);
            iDataPrams[8].Direction = ParameterDirection.Output;
          
            ida.RunSql(strSql, ref iDataPrams);

            strNewId = iDataPrams[8].Value.ToString();
            return strNewId;
        }

        /// <summary>
        /// 修改单位信息
        /// </summary>
        /// <param name="strUnitsName"></param>
        /// <param name="strUnitsTel"></param>
        /// <param name="strUnitsAddress"></param>
        /// <param name="strFax"></param>
        /// <param name="strPostCode"></param>
        /// <param name="strOrganization"></param>
        /// <param name="strLegal"></param>
        /// <param name="strNature"></param>
        /// <param name="strUnitsID"></param>
        /// <returns></returns>
        /// <!--addby zhongjian 20100130-->
        public static bool UpdateUnits(string strUnitsName, string strUnitsTel, string strUnitsAddress, string strFax, string strPostCode,
            string strOrganization, string strLegal, string strNature,string strUnitsID)
        {
            string strSql = string.Format(@"update sys_units set UNITSNAME='{0}',UNITSTEL='{1}',UNITSADDRESS='{2}',FAX='{3}',POSTCODE='{4}',
                Organization='{5}',Legal='{6}',Nature='{7}' where id='{8}'", strUnitsName, strUnitsTel, strUnitsAddress, strFax, strPostCode, strOrganization, strLegal, strNature,                                           strUnitsID);
            return SysParams.OAConnection().RunSql(strSql) > 0 ? true : false;
        }

        /// <summary>
        /// 获取用户信息
        /// </summary>
        /// <param name="strUnitsID">用户ID</param>
        /// <returns></returns>
        /// <!--addby zhongjian 20100130-->
        public static DataTable GetUnitsInfo(string strUnitsID)
        {
            string strSql = string.Format(@"select UNITSNAME,UNITSTEL,UNITSADDRESS,FAX,POSTCODE,Organization,Legal,Nature from sys_units 
                where id='{0}'", strUnitsID);
            DataTable dtTemp = new DataTable();
            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }

        /// <summary>
        /// 判断单位名称是否存在过
        /// </summary>
        /// <param name="strUnitName"></param>
        /// <returns></returns>
        public static string IsExistUnitsInfo(string strUnitName)
        {
            string strSql = string.Format(@"select UNITSNAME from sys_units 
                where UNITSNAME='{0}'", strUnitName.Trim());
            return SysParams.OAConnection().GetValue(strSql);
        }

        /// <summary>
        /// 添加单位信息
        /// </summary>
        /// <param name="strunitsname">单位名称</param>
        /// <param name="strunitstel">单位电话</param>
        /// <param name="strunitsaddress">单位地址</param>
        /// <param name="strfax">传真</param>
        /// <param name="strpostcode">邮编</param>
        /// <param name="strlegal">法人代表</param>
        /// <param name="strorganization">组织代码</param>
        /// <param name="strnature">单位性质</param>
        public static void AddUnitsInfo(string strunitsname, string strunitstel, string strunitsaddress, string strfax, string strpostcode, string strlegal, string strorganization, string strnature)
        {
            string strSql = string.Format(@"insert into sys_units(unitsname,unitstel,unitsaddress,fax,postcode,legal,organization,nature)
 values ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}')",
                            strunitsname, strunitstel, strunitsaddress, strfax, strpostcode, strlegal, strorganization, strnature);
            SysParams.OAConnection().RunSql(strSql);
        }

        /// <summary>
        /// 修改单位信息
        /// </summary>
        /// <param name="strID">单位id</param>
        /// <param name="strunitsname">单位名称</param>
        /// <param name="strunitstel">单位电话</param>
        /// <param name="strunitsaddress">单位地址</param>
        /// <param name="strfax">传真</param>
        /// <param name="strpostcode">邮编</param>
        /// <param name="strlegal">法人代表</param>
        /// <param name="strorganization">组织代码</param>
        /// <param name="strnature">单位性质</param>
        public static void UpdateUnitsInfo(string strID, string strunitsname, string strunitstel, string strunitsaddress, string strfax, string strpostcode, string strlegal, string strorganization, string strnature)
        {
            string strSql = string.Format(@"update sys_units set unitsname='{0}',unitstel='{1}',unitsaddress='{2}',fax='{3}',postcode='{4}',
                                                legal='{5}',organization='{6}',nature='{7}' where id='{8}'", strunitsname, strunitstel,
                                            strunitsaddress, strfax, strpostcode, strlegal, strorganization, strnature, strID);
            SysParams.OAConnection().RunSql(strSql);
        }

        /// 设置注册用户为有效
        /// </summary>
        /// <param name="strId">用户id</param>
        public static void SetRegUserValide(string strId)
        {
            //对于地图审核默认设置其状态为已办理权限（以后可以去掉）
            string strType = ConfigurationManager.AppSettings["bsqxspSystemFlag"];

            string sql = string.Format(@"update sys_user t set ISVALID='1',is_promise=1,CAN_SYNC=1,type='{1}' where ID='{0}' ",
                strId, strType);
            SysParams.OAConnection().RunSql(sql);

            //获取用户信息
            sql = string.Format(@"select userid,username,MOBILE from sys_user where id = '{0}'", strId);
            DataTable dtReturn = new DataTable();
            SysParams.OAConnection().RunSql(sql, out dtReturn);
            string strUserName = "", strMobile = "", strUserID = "";
            if (dtReturn.Rows.Count > 0)
            {
                strUserName = dtReturn.Rows[0]["username"].ToString();
                strMobile = dtReturn.Rows[0]["mobile"].ToString();
                strUserID = dtReturn.Rows[0]["userid"].ToString(); //添加用户ID 
            }

            //获取消息内容
            string strStepMsg = "", strStepN0 = "";
            sql = string.Format(@"select step_msg,step_no from xt_request_step where step_name='用户注册'", strId);
            SysParams.OAConnection().RunSql(sql, out dtReturn);
            if (dtReturn.Rows.Count > 0)
            {
                strStepMsg = dtReturn.Rows[0]["step_msg"].ToString();
                //获取信息步骤码
                strStepN0 = dtReturn.Rows[0]["step_no"].ToString();
            }

            //当消息内容设置为空时，将不再发送消息。
            if (!string.IsNullOrEmpty(strStepMsg))
            {
                //整理消息内容:strStepMsg中{0}:代表用户名称;{1}:代表事项名称; 
                try
                {
                    strStepMsg = string.Format(strStepMsg, strUserName);
                }
                catch { }

                //{0},您好，您的{2}业务申报权限已被审批通过
                sql = string.Format(@"Insert into xt_messagebox(MESSAGETEXT,PHONENO,USERNAME,USERID)  values ('{2}','{1}','{0}','{3}')",
                    strUserName, strMobile, strStepMsg, strUserID);
                SysParams.OAConnection().RunSql(sql);

            }
        }

        // 获取注册用户信息
        /// </summary>
        /// <param name="strValide"></param>
        /// <param name="strUserName"></param>
        /// <returns></returns>
        public static DataTable GetRegisterUser(string strValide, string strUserName)
        {
            string sql = @"select s.id, userid, USERNAME, mobile,(select UNITSNAME from sys_units a where a.id=UNITID) unitname, ISVALID,to_char(CREATEDATE,'yyyy-mm-dd hh24:mi') txtcreatedate from sys_user s where 1=1";

            if (strValide == "0")//查看审批通过的用户
            {
                sql += @" and ISVALID =1";
            }

            if (!string.IsNullOrEmpty(strUserName))//查询标志
            {
                sql += string.Format(" and USERNAME like '%{0}%'", strUserName);
            }

            sql += "order by createdate desc,userid";
            DataTable dt = new DataTable();
            SysParams.OAConnection().RunSql(sql, out dt);
            return dt;
        }

        /// <summary>
        /// 根据用户ID重置用户密码为随机的6位密码
        /// </summary>
        /// <param name="id">用户ID</param>
        /// <!--addby zhongjian 20090927-->
        public static void UpdatePassSet(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                string strSql = string.Format("update sys_user set query_pass =fun_pass(6),USERPWD=fn_md5(fun_pass(6)) where id = '{0}'", id);

                SysParams.OAConnection().RunSql(strSql);

                //如果是地信中心用户信息则更改标志位
                strSql = string.Format(@"update sys_user t set NEED_POST =1 where t.SYNC_TYPE=2 
                    and id='{0}'", id);
                SysParams.OAConnection().RunSql(strSql);
            }
        }
        /// <summary>
        /// 根据用户ID重置用户密码为指定密码
        /// </summary>
        /// <param name="id">用户ID</param>
        /// <param name="strUserPwd">指定的密码</param>
        /// <!--addby zhongjian 20090927-->
        public static void UpdatePassSet(string id, string strUserPwd)
        {
            if (!string.IsNullOrEmpty(id))
            {
                string strSql = string.Format("update sys_user set query_pass ='{1}',USERPWD=fn_md5('{1}') where id = '{0}'", id, strUserPwd);

                SysParams.OAConnection().RunSql(strSql);

                //如果是地信中心用户信息则更改标志位
                strSql = string.Format(@"update sys_user t set NEED_POST =1 where t.SYNC_TYPE=2 
                    and id='{0}'", id);

                SysParams.OAConnection().RunSql(strSql);
            }
        }
    }
}