﻿
// 创建人  ：LinJian
// 创建时间：2006-09-04
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.Security;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Business;
using WF_Business;
using WF_DataAccess;

namespace SbBusiness.Menu
{
    /// <!--
    /// 功能描述  : 菜单操作类
    /// 创建人  : LinJian
    /// 创建时间: 2006-09-04
    /// -->
    public class ClsMenuOperation : Page
    {
        /// <summary>
        /// 根据系统标识生成菜单
        /// </summary>
        /// <returns></returns>
        public  DataTable GenerateParentMenu(string userID)
        {
            string strSql = @"select a.* from sys_menu a join sys_usertype_menu_relation b on a.ID=b.Menu_ID join sys_usertype c on c.id=b.usertype_id join sys_user d on d.Type=c.UserTypeCode  where a.parent_id is null and d.userID=:userID order by ORDER_FIELD asc";

            IDataAccess ida = SysParams.OAConnection(false);
            IDataParameter[] iDataPrams = DataFactory.GetParameter(DataFactory.DefaultDbType, 1);
            iDataPrams[0].ParameterName = "userID";
            iDataPrams[0].DbType = DbType.String;
            iDataPrams[0].Value = userID;

            return ida.RunSql(strSql, ref iDataPrams);
        }

        /// <summary>
        /// 根据系统标识父菜单ID生成菜单
        /// </summary>
        /// <param name="userID">用户ID</param>
        /// <param name="strParentId">父菜单ID</param>
        /// <returns></returns>
        /// <!--
        /// 功能描述  : 菜单的生成与权限挂钩
        /// 修改人  : cd
        /// 修改时间: 2007-07-10
        /// -->
        public  DataTable GenerateChildMenu(string userID, string strParentId)
        {
            string strSql = @"select a.* from sys_menu a join sys_usertype_menu_relation b on a.ID=b.Menu_ID join sys_usertype c on c.id=b.usertype_id join sys_user d on d.Type=c.UserTypeCode  where a.parent_id =:parentid and d.userID=:userID order by order_field";
            
            IDataAccess ida = SysParams.OAConnection(false);
            IDataParameter[] iDataPrams = DataFactory.GetParameter(DataFactory.DefaultDbType, 2);
            iDataPrams[0].ParameterName = "parentid";
            iDataPrams[0].DbType = DbType.String;
            iDataPrams[0].Value = strParentId;

            iDataPrams[1].ParameterName = "userID";
            iDataPrams[1].DbType = DbType.String;
            iDataPrams[1].Value = userID;

            return ida.RunSql(strSql, ref iDataPrams);
        }


        /// 创建人  : Wu Hansi
        /// 创建时间: 2007-07-09
        /// 修改人  : Wu Hansi
        /// 修改时间: 2007-07-09
        /// 修改描述: 描述                 
        /// <summary>
        /// 得到排序好的所有菜单
        /// </summary>
        /// <returns></returns>
        public  DataTable GetMenusSorted()
        {
            string strSql = @"select t2.parent_id sortid,null parentMenu, t2.menu_title childMenu,t2.id, t2.menu_url, null child
                            from sys_menu t1, sys_menu t2 
                            where t1.ID = t2.parent_id UNION 
                            select t3.id sortid,menu_title parentMenu,null childMenu,id, menu_url,
                            (select count(*) from sys_menu t4 where t4.parent_id=t3.id) child
                            from sys_menu t3 
                            where t3.parent_id is null order by sortid desc,menu_url desc
                            ";
            DataTable dtTemp;
            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }

        /// <summary>
        /// 根据条件得到排序好的所有菜单
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public  DataTable GetMenusSorted(string str)
        {
            string strSql = @"select t2.parent_id sortid,null parentMenu, t2.menu_title childMenu,t2.id, t2.menu_url, null child
                            from sys_menu t1, sys_menu t2 
                            where t1.ID = t2.parent_id  and t2.menu_title like '%" +str + @"%' UNION 
                            select t3.id sortid,menu_title parentMenu,null childMenu,id, menu_url,
                            (select count(*) from sys_menu t4 where t4.parent_id=t3.id) child
                            from sys_menu t3 
                            where t3.parent_id is null and t3.menu_title like '%" + str + @"%' order by sortid desc,menu_url desc
                            ";
            DataTable dtTemp;
            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }
        /// <summary>
        /// 得到排序好的，带是否拥有权限的标志
        /// </summary>
        /// <param name="usertype_id"></param>
        /// <returns></returns>
        public  DataTable GetMenusSortedWithAuth(string usertype_id)
        {
            string strSql = @"select t2.parent_id sortid,null parentmenu, t2.menu_title childmenu,t2.id,t2.menu_url,
                            (select count(*) from sys_usertype_menu_relation s1 where 
                            s1.usertype_id='" + usertype_id + @"' and s1.menu_id=t2.id) auth
                            from sys_menu t1, sys_menu t2 where t1.ID = t2.parent_id
                            UNION 
                            select t3.id sortid,menu_title parentmenu,null childmenu,id,menu_url,
                            (select count(*) from sys_usertype_menu_relation s1 where 
                            s1.usertype_id='" + usertype_id + @"' and s1.menu_id=t3.id) auth
                            from sys_menu t3, sys_usertype_menu_relation s1
                            where t3.parent_id is null order by sortid desc, menu_url desc
                            ";
            DataTable dtTemp;
            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }

        /// <summary>
        /// 删除现有的菜单
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public  int DeleteMenu(string id)
        {
            string strSql = string.Format(@"Delete from sys_menu where id = '{0}'",id);

            return SysParams.OAConnection().RunSql(strSql);
        }

        /// <summary>
        /// 增加一个用户类型-菜单关系
        /// </summary>
        /// <param name="usertype_id"></param>
        /// <param name="menu_id"></param>
        /// <returns></returns>
        public  bool InsertUsertypeMenuRelation(string usertype_id, string menu_id)
        {
            string strSql = @"Insert into sys_usertype_menu_relation (usertype_id, menu_id) VALUES ('" + 
                            usertype_id + "','" + menu_id + "')";
            int nReturn = SysParams.OAConnection().RunSql(strSql);
            return nReturn > 0 ? true : false;
        }

        /// <summary>
        /// 删除一个用户类型-菜单关系
        /// </summary>
        /// <param name="usertype_id"></param>
        /// <param name="menu_id"></param>
        /// <returns></returns>
        public bool RemoveUsertypeMenuRelation(string usertype_id, string menu_id)
        {
            string strSql = string.Format(@"Delete from sys_usertype_menu_relation where 
                usertype_id ='{0}' and menu_id='{1}' ",usertype_id,menu_id);

            int nReturn = SysParams.OAConnection().RunSql(strSql);
            return nReturn > 0 ? true : false;
        }

        
        /// <summary>
        /// 根据菜单id获取父菜单id
        /// </summary>
        /// <param name="strMenuId">菜单id</param>
        /// <returns></returns>
        public  string GetParentId(string strMenuId)
        {
            string strSql =string.Format(@"select PARENT_ID from SYS_MENU where id='{0}'",strMenuId);
            
            return SysParams.OAConnection().GetValue(strSql);
        }

        /// <summary>
        ///获取特定菜单下的子菜单
        /// </summary>
        /// <param name="strParentMenuName">Name of the STR parent menu.</param>
        /// <returns></returns>
        public DataTable GetChildMenus(string strParentMenuName)
        {
            string strSql = string.Format(@"select * from SYS_MENU where parent_id in 
                (select id from sys_menu where menu_title='{0}') order by order_field", strParentMenuName);

            DataTable dtTemp = new DataTable();
            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }

        /// <summary>
        /// 根据流程和菜单获取下面有权限的菜单
        /// </summary>
        /// <param name="strParentMenuName"></param>
        /// <returns></returns>
        public DataTable GetChildMenus(string strParentMenuName,string strFlowName)
        {
            string strSql = string.Format(@"select *
  from SYS_MENU
 where parent_id in (select id from sys_menu where menu_title = '{0}')
   and menu_title in (select menu_name
                        from XT_WORKFLOW_SET a, xt_workflow_define b
                       where a.flowid = b.id
                         and b.ispub = '1'
                         and a.menu_appear = '1'
                         and b.flowname = '{1}')
 order by order_field", strParentMenuName, strFlowName);
            DataTable dtTemp;
            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }

        /// <summary>
        /// 获取个人信息管理菜单(非单位管理员)
        /// </summary>
        /// <returns></returns>
        /// <!--addby zhongjian 20100125-->
        public DataTable GetChildMenusPersonal()
        {
            string strSql = string.Format(@"select * from SYS_MENU where parent_id in 
                (select id from sys_menu where menu_title='个人信息管理') order by order_field");
            DataTable dtTemp;
            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }

        /// <summary>
        /// 根据用户ID判断该用户是否单位管理员用户
        /// </summary>
        /// <param name="UserID">用户ID</param>
        /// <returns></returns>
        /// <!--addby zhongjian 20100125-->
        public DataTable SelectUnitsAdmin(string UserID)
        {
            string strSql = string.Format(@"select * from sys_user where usertype='1' and unitid is not null
                                            and unitsadmin='1' and userid='{0}'", UserID);
            DataTable dtTemp;
            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }

        /// <summary>
        /// 根据流程别名获取该流程拓展信息
        /// </summary>
        /// <param name="strFlowType">流程别名</param>
        /// <returns></returns>
        /// <!--addby zhongjian 20100308-->
        public DataTable GetWorkflowDefine(string strFlowType)
        {
            string strSql = string.Format(@"select flowname, flowtype, id, interfaceurl, interfacetype
                                              from xt_workflow_define
                                             where ispub = 1
                                               and isdelete = 0
                                               and flowtype = '{0}'", strFlowType);
            DataTable dtTemp;
            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }
    }
}
