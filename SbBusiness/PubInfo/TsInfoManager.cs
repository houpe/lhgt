﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using Business;
using WF_Business;

namespace SbBusiness.PubInfo
{
    /// <summary>
    /// 
    /// </summary>
    public class TsInfoManager
    {
        /// <summary>
        /// Inserts the ts info.
        /// </summary>
        /// <param name="strTsr">The STR TSR.</param>
        /// <param name="strBtsr">The STR BTSR.</param>
        /// <param name="strTssj">The STR TSSJ.</param>
        /// <param name="strIid">The STR iid.</param>
        /// <param name="Tsnr">The TSNR.</param>
        /// <returns></returns>
        public void InsertTsInfo(string strTsr,string strBtsr,string strTssj,string strIid,string Tsnr)
        {
            string strSql = string.Format(@"insert into XT_TSINFO t(TSR,BTSR,TSSJ,IID,TSNR) 
                values('{0}','{1}',to_date('{2}','yyyy-mm-dd'),'{3}','{4}')",strTsr,strBtsr,strTssj,
                strIid,Tsnr);

            SysParams.OAConnection().RunSql(strSql);
        }


        /// <summary>
        ///获取投诉信息
        /// </summary>
        /// <param name="strWid">The STR wid.</param>
        /// <param name="strTsr">投诉人.</param>
        /// <param name="strBtsr">被投诉人.</param>
        /// <param name="strFromTssj">起始投诉时间.</param>
        /// <param name="strToTssj">结束投诉时间.</param>
        /// <returns></returns>
        public DataTable GetTsInfo(string strWid, string strTsr, string strBtsr, string strFromTssj, string strToTssj)
        {
            string strSql = "select rownum 序号,t.* from XT_TSINFO t ";
            if (!string.IsNullOrEmpty(strWid))
            {
                strSql += string.Format(" where t.iid in (select iid from st_instance b where b.wid='{0}' and b.isdelete <>1)", strWid);
            }
            if (!string.IsNullOrEmpty(strTsr))
            {
                strSql += string.Format(" and TSR like '%{0}%'", strTsr);
            }
            if (!string.IsNullOrEmpty(strBtsr))
            {
                strSql += string.Format(" and BTSR like '%{0}%'", strBtsr);
            }
            if (!string.IsNullOrEmpty(strFromTssj))
            {
                strSql += string.Format(" and TSSJ >= to_date('{0}','yyyy-mm-dd')", strFromTssj);
            }
            if (!string.IsNullOrEmpty(strToTssj))
            {
                strSql += string.Format(" and TSSJ <= to_date('{0}','yyyy-mm-dd')", strToTssj);
            }

            DataTable dt;
            SysParams.OAConnection().RunSql(strSql, out dt);
            return dt;
        }

        /// <summary>
        /// Gets the ts info.
        /// </summary>
        /// <param name="strID">The STR ID.</param>
        /// <returns></returns>
        public DataTable GetTs(string strID)
        {
            string strSql = "select * from XT_TSINFO t ";
            if (!string.IsNullOrEmpty(strID))
            {
                strSql += string.Format(" where t.ID = '{0}'", strID);
            }

            DataTable dt;
            SysParams.OAConnection().RunSql(strSql, out dt);
            return dt;
        }

        /// <summary>
        /// UpdateTsInfo the ts info.
        /// </summary>
        /// <param name="strId">The STR ID.</param>
        /// <param name="strTsr">The STR TSR.</param>
        /// <param name="strBtsr">The STR BTSR.</param>
        /// <param name="strTssj">The STR TSSJ.</param>
        /// <param name="strIid">The STR iid.</param>
        /// <param name="Tsnr">The TSNR.</param>
        /// <returns></returns>
        public void UpdateTsInfo(string strId, string strTsr, string strBtsr, string strTssj, string strIid, string Tsnr)
        {
            string strSql = string.Format(@"Update XT_TSINFO set TSR = '{1}',BTSR = '{2}',
               TSSJ = to_date('{3}','yyyy-mm-dd'),IID = '{4}',TSNR = '{5}' where id='{0}'", 
               strId, strTsr, strBtsr, strTssj,strIid, Tsnr);

            SysParams.OAConnection().RunSql(strSql);
        }
        
        /// <summary>
        /// UpdateTsInfo the ts info.
        /// </summary>
        /// <param name="strId">The STR ID.</param>
        /// <returns></returns>
        public void DeleteTsInfo(string strId)
        {
            string strSql = string.Format(@"Delete from XT_TSINFO where id='{0}'",strId);

            SysParams.OAConnection().RunSql(strSql);
        }
    }
}
