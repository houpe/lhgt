﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using SbBusiness;
using Business;
using WF_Business;

namespace SbBusiness.PubInfo
{
    /// <summary>
    /// 发布信息管理
    /// </summary>
    public class InfoManage
    {
        /// <summary>
        /// 根据流程的大小类获取信息
        /// </summary>
        /// <param name="Type">消息类型</param>
        /// <param name="strSmalType">流程名称</param>
        /// <returns></returns>
        public DataTable GetInfoType(string Type,string strSmalType)
        {
            string strSql = @"select * from (select t.id as id,(case when length(t.title)>14 then 
                Substr(t.title,0,14) || '...' else t.title end ) as title,t.time,t.type  
                from xt_other t where 1 = 1  ";
            if (!string.IsNullOrEmpty(Type))
            {
                strSql += string.Format(" and type ='{0}'", Type);
            }
            if (!string.IsNullOrEmpty(strSmalType))
            {
                strSql += string.Format(" and SMALLTYPE ='{0}'", strSmalType);
            }
            strSql += " order by t.time desc ) where rownum<=5 ";

            DataTable dtTemp;
            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }

      
        /// <summary>
        /// 读取办事指南内容
        /// </summary>
        /// <param name="strContentType">办事指南类别</param>
        /// <param name="strWid">获取流程id</param>
        /// <returns></returns>
        public DataTable GetPubContentInfo(string strContentType, string strWid)
        {
            string strSql = string.Format(@"select * from xt_other where type='{0}' and 
            SMALLTYPE=(select wname from st_workflow a where a.wid='{1}')",
                strContentType, strWid);

            DataTable dtTemp;
            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }

        /// <summary>
        /// 根据条件获取公示内容
        /// </summary>
        /// <param name="strContentType">公示信息类型</param>
        /// <param name="strWid">流程id</param>
        /// <param name="strTitle">标题头</param>
        /// <param name="strQssj">起始时间</param>
        /// <param name="strZzsj">终止时间</param>
        /// <returns></returns>
        public DataTable GetPubContentInfo(string strContentType, string strWid,
            string strTitle,string strQssj,string strZzsj)
        {
            string strSql = string.Format(@"select * from xt_other where type='{0}' and 
            SMALLTYPE=(select wname from st_workflow a where a.wid='{1}') ",
                strContentType, strWid);

            if (!string.IsNullOrEmpty(strTitle))
            {
                strSql += string.Format(" and title like '%{0}%'",strTitle);
            }
            if (!string.IsNullOrEmpty(strQssj))
            {
                strSql += string.Format(" and time>=to_date('{0}','yyyy-mm-dd')", strQssj);
            }
            if (!string.IsNullOrEmpty(strZzsj))
            {
                strSql += string.Format(" and time<=to_date('{0}','yyyy-mm-dd')", strZzsj);
            }

            DataTable dtTemp;
            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }

        /// <summary>
        /// Gets the pub info.
        /// </summary>
        /// <param name="strId">The STR id.</param>
        /// <returns></returns>
        public DataTable GetPubInfoById(string strId)
        {
            string strSql = @"select * from xt_other t where 1 = 1  ";
            if (!string.IsNullOrEmpty(strId))
            {
                strSql += string.Format(" and id ='{0}'", strId);
            }

            DataTable dtTemp;
            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }

        /// <summary>
        /// 读取办事指南内容
        /// </summary>
        /// <param name="strWid">流程名称</param>
        /// <returns></returns>
        /// <!--addby zhongjian 20100118-->
        public DataTable GetPubContentInfo(string strWid)
        {
            string strSql = string.Format(@"select id,filename,flowid,ftitle,fcontent,forder
                                  from xt_lawguide
                                 where filename = (select wname from st_workflow where wid='{0}') order by forder", strWid);

            DataTable dtTemp;
            SysParams.OAConnection().RunSql(strSql, out dtTemp);
            return dtTemp;
        }

    }
}
